package in.junctiontech.school;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.common.Gc;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Jaydevi Bhade on 07-06-2017.
 */

public class ChangeAppColorFragment extends
        Fragment {

    private SharedPreferences sp;
 //   private Button changeColorPrimaryDark;
    private SeekBar seekBar_red, seekBar_green, seekBar_blue;
    private TextView tv_color_primary;
    private int redValue = 0, greenValue = 0, blueValue = 0;
    private int colorIs;
    private ProgressDialog progressbar;
    private LinearLayout snackbar;
    private MainDatabase mDb;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) return null;

        mDb = MainDatabase.getDatabase(getActivity().getApplicationContext());
        View convertView = inflater.inflate(R.layout.layout_app_colour_setting, container, false);
        snackbar =  convertView.findViewById(R.id.ll_change_app_color_activity);
        tv_color_primary =  convertView.findViewById(R.id.tv_change_app_color_color_primary);
        seekBar_red =  convertView.findViewById(R.id.seekBar_red);
        seekBar_green =  convertView.findViewById(R.id.seekBar_green);
        seekBar_blue =  convertView.findViewById(R.id.seekBar_blue);

        (convertView.findViewById(R.id.btn_change_app_color_save)).setOnClickListener(v -> saveAppColor());
        (convertView.findViewById(R.id.btn_change_app_color_set_default)).setOnClickListener(v -> {
            redValue = 230;
            greenValue = 74;
            blueValue = 25;
            seekBar_red.setProgress(redValue);
            seekBar_green.setProgress(greenValue);
            seekBar_blue.setProgress(blueValue);
            setColorApp();
        });

        return convertView;
    }


    private void saveAppColor() {
       // SharedPreferences spMain = Prefs.with(getActivity()).getSharedPreferences();
        SchoolDetailsEntity generalSettingDataObj = mDb.schoolDetailsModel().getSchoolDetails();
        if (generalSettingDataObj != null && !generalSettingDataObj.Id.isEmpty()) {
            progressbar.show();

            final JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("Id", generalSettingDataObj.Id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final JSONObject param = new JSONObject();
            try {
                param.put("themeColor", colorIs);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, getActivity())
                    + Gc.ERPAPIVERSION + "schoolDetail/schoolDetails" + "/" + job_filter;
          //  Log.e("ok", url);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
           //     Log.e("response",job.toString());
                String code = job.optString("code");
                Intent intent = new Intent();
                progressbar.dismiss();
                switch (code) {
                    case "200":
                        /* ******* Successfully Save Color to server *******/
                        sp.edit().putInt(Config.APP_COLOR, colorIs).apply();
                        getActivity().setResult(RESULT_OK, intent);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        /* **********************************************/
                        break;
                    case "304":
                        /* ******* Successfully Save Color to server *******/
                        sp.edit().putInt(Config.APP_COLOR, colorIs).apply();
                        getActivity().setResult(RESULT_OK, intent);
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        /* **********************************************/
                        break;
                    default:

                        Snackbar snackbarObj = Snackbar.make(snackbar, (getString(R.string.data_added_successfully)),
                                Snackbar.LENGTH_LONG).setAction("", v -> {
                        });
                        snackbarObj.getView().setBackgroundColor(getResources()
                                .getColor(android.R.color.holo_red_dark));
                        snackbarObj.show();
                }
            }, error -> {
                Snackbar snackbarObj = Snackbar.make(snackbar, (error.toString()),
                        Snackbar.LENGTH_LONG).setAction("", v -> {
                });
                snackbarObj.getView().setBackgroundColor(getResources()
                        .getColor(android.R.color.holo_red_dark));
                snackbarObj.show();
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("APPKEY", Gc.APPKEY);
                    headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getActivity()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getActivity()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID", Gc.id(getActivity()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getActivity()));
                    headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getActivity()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getActivity()));
                    headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getActivity()));
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    return param.toString().getBytes(StandardCharsets.UTF_8);
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

           /* final JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("Id", generalSettingDataObj.Id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final Map<String, Object> param = new LinkedHashMap<>();
            param.put("themeColor", colorIs + "");

            final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
            param_main.put("data", new JSONObject(param));
            param_main.put("filter", job_filter);

            Log.e("BasicDetailResult1", new JSONObject(param_main).toString());
            DbHandler.longInfo(spMain.getString("HostName", "Not Found") + "GeneralSettingApi.php?" + "databaseName=" +
                    spMain.getString("organization_name", "") + "&data=" + (new JSONObject(param_main)));


            String basicDetailUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,getActivity())
                    + Config.applicationVersionNameValue
                    + "GeneralSettingApi.php?" + "databaseName=" +
                    Gc.getSharedPreference(Gc.ERPDBNAME,getActivity()) + "&data=" + (new JSONObject(param_main));

            Log.e("SchoolColorUrl", basicDetailUrl);

            StringRequest request = new StringRequest(Request.Method.PUT,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    basicDetailUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("SchoolColorRes", s);
                            //   progressbar.cancel();
                            progressbar.dismiss();
                            try {
                                JSONObject jsonObjectUpload = new JSONObject(s);
                                if (jsonObjectUpload.optString("code").equalsIgnoreCase("201")||
                                        jsonObjectUpload.optString("code").equalsIgnoreCase("200")) {
                                  *//*  ******* Successfully Save Color to server *******//*
                                    sp.edit()
                                            .putInt(Config.APP_COLOR, colorIs)
                                            .apply();
                                    Intent intent = new Intent();
                                    getActivity().setResult(RESULT_OK, intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                    *//* **********************************************//*
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.failed_to_upload),
                                            Toast.LENGTH_SHORT).show();
                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            (getString(R.string.data_added_successfully) ) ,
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources()
                                            .getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("SchoolColorError", volleyError.toString());
                    progressbar.cancel();

                    Config.responseVolleyErrorHandler(getActivity(), volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("data", new JSONObject(param).toString());

                    DbHandler.longInfo(new JSONObject(params).toString());
                    Log.e("SchoolColorPost", new JSONObject(params).toString());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);*/
        }
    }

    private void setColorApp() {
        // FOR NAVIGATION VIEW ITEM TEXT COLOR
        colorIs = Color.rgb(redValue, greenValue, blueValue);
      //  Log.e("colorIs", colorIs + "");
        tv_color_primary.setBackgroundColor(colorIs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(colorIs);
            getActivity().getWindow().setNavigationBarColor(colorIs);
        }
        ((AnotherActivity) getActivity()).changeToolbarColor(colorIs);
        // getActivity(). getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0000ff")));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getActivity().getApplicationContext().getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        colorIs = sp.getInt(Config.APP_COLOR, -1684967);
      //  Log.e("colorIs", colorIs + " ritu");
        redValue = Color.red(colorIs);
        greenValue = Color.green(colorIs);
        blueValue = Color.blue(colorIs);
        seekBar_red.setProgress(redValue);
        seekBar_green.setProgress(greenValue);
        seekBar_blue.setProgress(blueValue);
        tv_color_primary.setBackgroundColor(colorIs);
        seekBar_red.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                redValue = progress;
                setColorApp();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar_green.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                greenValue = progress;
                setColorApp();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar_blue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                blueValue = progress;
                setColorApp();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        setColorApp();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
