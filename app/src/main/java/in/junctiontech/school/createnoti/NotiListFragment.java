package in.junctiontech.school.createnoti;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.Communicator.NotificationComm;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;

/**
 * Created by JAYDEVI BHADE on 10/10/2016.
 */

public class NotiListFragment extends
        Fragment implements SearchView.OnQueryTextListener, NotificationComm{
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private ArrayList<NotificationEvent> listData;
    private NotiListAdapter adapter;
    private SwipeRefreshLayout swipe_refresh_list;
    private SharedPreferences sp;
    private RelativeLayout  page_demo;
    private int  appColor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       listData = new ArrayList<NotificationEvent>();
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor = ((CreateNotiActivity)getContext()).getAppColor();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data = (RecyclerView) convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available = (TextView) convertView.findViewById(R.id.tv_view_all_data_not_data_available);

        swipe_refresh_list =(SwipeRefreshLayout)convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);

        /*********************************    Demo Screen *******************************/

        page_demo =(RelativeLayout)convertView.findViewById(R.id.rl_view_all_list_item_demo);
        if (sp.getBoolean("DemoNotificationPage",true)){
            page_demo.setVisibility(View.VISIBLE);
        }else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoNotificationPage",false).commit();
            }
        });
        /****************************************************************/




        setHasOptionsMenu(true);
        return convertView;
    }
    private void refreshClassList() {
        ((CreateNotiActivity) getActivity()).refreshNotificationList();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshClassList();
            }
        });
        if (listData.size() == 0) {
            tv_view_all_data_not_data_available.setText(R.string.no_notifications_available);
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
        }else tv_view_all_data_not_data_available.setVisibility(View.GONE);
        setupRecycler();
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new NotiListAdapter(getContext(), listData,appColor);
        recycler_view_list_of_data.setAdapter(adapter);
    }

    @Override
    public void setData(ArrayList<NotificationEvent> data) {

        this.listData = data;
        Log.e("listData",listData.size()+" iiii");

        adapter.updateList(listData);
        if (listData.size() == 0) {
            tv_view_all_data_not_data_available.setText(R.string.no_notifications_available);
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
        }else tv_view_all_data_not_data_available.setVisibility(View.GONE);
    }
    public void removeObject(int pos) {

adapter.removeObject(pos);
    }
    @Override
    public void updateNotification(NotificationEvent notification) {

    }


    public void setFilter(ArrayList<NotificationEvent> classListData) {
        adapter.updateList(classListData);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        setFilter(listData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<NotificationEvent> filteredModelList = filter(listData, newText);
        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<NotificationEvent> filter(ArrayList<NotificationEvent> models, String query) {
        query = query.toLowerCase();final ArrayList<NotificationEvent> filteredModelList = new ArrayList<>();

        for (NotificationEvent model : models) {

            if (model.getDescription().toLowerCase().contains(query)||
                    model.getDate().toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    public void closeSwipe() {
        if (swipe_refresh_list.isRefreshing()) swipe_refresh_list.setRefreshing(false);
    }

}
