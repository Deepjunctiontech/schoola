package in.junctiontech.school.createnoti;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;


public class CreateNotiActivity extends AppCompatActivity {

    private ArrayList<NotificationEvent> notificationListData;
    private SharedPreferences sp;
    private String dbName;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private Gson gson;
    private Type type;
    private CreateNotiFragment fragmentCreateNotifi;
    private NotiListFragment fragment_ListOfNotifi;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private boolean isDialogShowing = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView tv_create_student_current_session;
    private boolean newNotificationAdded = false;
    private boolean logoutAlert = false;
    private FrameLayout snackbar_create_notification;
    private int colorIs = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //   LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_create_student2);

        snackbar_create_notification = (FrameLayout) findViewById(R.id.snackbar_view_pager);
        tv_create_student_current_session = (TextView) findViewById(R.id.tv_create_student_current_session);

        tv_create_student_current_session.startAnimation(AnimationUtils.
                loadAnimation(this, R.anim.blink));
        tv_create_student_current_session.setText(getString(R.string.for_session) + " : "
                + sp.getString(Config.SESSION, ""));

        dbName = sp.getString("organization_name", "");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        notificationListData = new ArrayList<>();

        gson = new Gson();
        type = new TypeToken<NotificationEvent>() {
        }.getType();


        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle(getString(R.string.create_notice));
                        fragmentCreateNotifi.refreshPage();
                      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
                            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        }*/
                        break;

                    case 1:
                        getSupportActionBar().setTitle(getString(R.string.notification_list));
                       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
                            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        }*/
                        break;


                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        getNotificationListFromServer();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (notificationListData.size() == 0)
                        getNotificationListFromServer();
                }
            }
        };
        setColorApp();
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        tabLayout.setBackgroundColor(colorIs);
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void setupViewPager(ViewPager viewPager) {
        CreateNotiActivity.ViewPagerAdapter adapter = new CreateNotiActivity.ViewPagerAdapter(getSupportFragmentManager());
        fragmentCreateNotifi = new CreateNotiFragment();
        fragment_ListOfNotifi = new NotiListFragment();
        adapter.addFragment(fragmentCreateNotifi, getString(R.string.create));
        adapter.addFragment(fragment_ListOfNotifi, getString(R.string.list));

        viewPager.setAdapter(adapter);

    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(android.R.drawable.ic_menu_add);
        tabLayout.getTabAt(1).setIcon(android.R.drawable.ic_menu_my_calendar);

      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setTitle(getString(R.string.create_class));
            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);

        }*/

    }

    public void getNotificationListFromServer() {
       /* if (Config.checkInternet(this)) {*/
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "NotificationApi.php?databaseName=" +
                dbName +
                "&action=join";
        Log.e("urlNotiFet", url);

        StringRequest request = new StringRequest(Request.Method.GET,
                url
                   /* sp.getString("HostName", "Not Found") + "NotificationApi.php?databaseName=" +
                            dbName +
                            "&action=join"*/

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getNotificationData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                NotificationEvent obj = gson.fromJson(s, type);
                                notificationListData = obj.getResult();
                                fragment_ListOfNotifi.setData(notificationListData);
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(CreateNotiActivity.this).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateNotiActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar_create_notification);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar_create_notification);

                            fragment_ListOfNotifi.closeSwipe();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("NotificationData", volleyError.toString());
                progressbar.cancel();
                fragment_ListOfNotifi.closeSwipe();
                Config.responseVolleyErrorHandler(CreateNotiActivity.this, volleyError, snackbar_create_notification);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
      /*  } else {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        }*/
    }

    public void updateNotification(NotificationEvent obj) {
        viewPager.setCurrentItem(0);
        fragmentCreateNotifi.updateNotification(obj);
        getSupportActionBar().setTitle(getString(R.string.update_notification));
    }

    public void updateOldNotification(final NotificationEvent notificationObjUpdate,
                                      final String description,
                                      final String link, final boolean switchStatus,
                                      final String date) {
        /*if (Config.checkInternet(this)) {*/
        progressbar.show();
        final Map<String, String> param = new LinkedHashMap<String, String>();
        try {
            param.put("Description", URLEncoder.encode( description  ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param.put("Link_url", link.replaceAll(" ", "+"));
        if (switchStatus)
            param.put("Status", "Active");
        else param.put("Status", "Inactive");
        param.put("Date", date);


        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Notify_id", "" + notificationObjUpdate.getNotifyId() + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String updateNotiUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "NotificationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("updateNotiUrl", updateNotiUrl);

        Log.e("UpdateNotification", new JSONObject(param_main).toString());

        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateNotiUrl

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateNotificationRes", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                newNotificationAdded = true;

                                int pos = notificationListData.indexOf(notificationObjUpdate);
                                notificationObjUpdate.setDescription(description);
                                notificationObjUpdate.setLinkUrl(link);
                                notificationObjUpdate.setDate(date);

                                if (switchStatus)
                                    notificationObjUpdate.setStatus("Active");
                                else notificationObjUpdate.setStatus("Inactive");

                                notificationListData.set(pos, notificationObjUpdate);

                                fragment_ListOfNotifi.setData(notificationListData);
                                Toast.makeText(CreateNotiActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                fragmentCreateNotifi.refreshPage();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(CreateNotiActivity.this).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateNotiActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar_create_notification);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(CreateNotiActivity.this, volleyError, snackbar_create_notification);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

    }

    public void refreshNotificationList() {
        getNotificationListFromServer();
    }

    public int getAppColor() {
        return colorIs;
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (newNotificationAdded)
            setResult(RESULT_OK, new Intent(this, AdminNavigationDrawerNew.class));
        else setResult(RESULT_CANCELED, new Intent(this, AdminNavigationDrawerNew.class));
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (newNotificationAdded)
            setResult(RESULT_OK, new Intent(this, AdminNavigationDrawerNew.class));
        else setResult(RESULT_CANCELED, new Intent(this, AdminNavigationDrawerNew.class));
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

        super.onBackPressed();
    }

    public void addNotification(final String description, final String linkUrl,
                                final String status, final String date) throws JSONException {
        /*if (Config.checkInternet(this)) {*/

        progressbar.show();
        final JSONObject param = new JSONObject();


        param.put("Description", description);
        param.put("Link_url", linkUrl);
        param.put("Status", status);
        param.put("Date", date);
        Log.e("varsha", param.toString());

        String addNotiUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "NotificationApi.php?databaseName="
                + dbName;

        Log.d("addNotiUrl", addNotiUrl);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, addNotiUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("ResultNotification", jsonObject.toString());
                        progressbar.cancel();


                        if (jsonObject.optInt("code") == 201) {
                            newNotificationAdded = true;

                            int id = jsonObject.optInt("result");
                            notificationListData.add(new NotificationEvent((id),
                                    description,
                                    linkUrl,
                                    status,
                                    date,
                                    false
                            ));
                            fragment_ListOfNotifi.setData(notificationListData);
                            if (fragmentCreateNotifi != null)
                                fragmentCreateNotifi.clearFields();
                            Toast.makeText(CreateNotiActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                ||
                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                            if (!logoutAlert & !(CreateNotiActivity.this).isFinishing()) {
                                Config.responseVolleyHandlerAlert(CreateNotiActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                logoutAlert = true;
                            }
                        } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                            Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar_create_notification);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("ResultNotificationErr", volleyError.toString());
                        progressbar.cancel();
                        Config.responseVolleyErrorHandler(CreateNotiActivity.this, volleyError, snackbar_create_notification);

                    }
                });

  /*          StringRequest request = new StringRequest(Request.Method.POST,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    addNotiUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("data", (new JSONObject(param)).toString());

                    //  params.put("className", et_create_class_new_class_name.getText().toString());
                    Log.e("NotificationEvent", new JSONObject(param).toString());
                    return params;
                }
            };*/

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    public void deleteNotification(final NotificationEvent notificationObj) {

        progressbar.show();

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Notify_id", notificationObj.getNotifyId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        //  param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String deleteNotiUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "NotificationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("deleteNotiUrl", deleteNotiUrl);

        Log.e("deleteNotification", new JSONObject(param_main).toString());

        StringRequest request = new StringRequest(Request.Method.DELETE,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                deleteNotiUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("deleteNotificationRes", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                newNotificationAdded = true;
                                int pos = notificationListData.indexOf(notificationObj);
                                notificationListData.remove(notificationObj);
                                // fragment_ListOfNotifi.setData(notificationListData);
                                fragment_ListOfNotifi.removeObject(pos);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(CreateNotiActivity.this).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateNotiActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar_create_notification);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar_create_notification);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("deleteResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(CreateNotiActivity.this, volleyError, snackbar_create_notification);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

    }

}
