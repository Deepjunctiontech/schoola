package in.junctiontech.school.createnoti;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.Communicator.NotificationComm;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;

/**
 * Created by JAYDEVI BHADE on 10/10/2016.
 */

public class CreateNotiFragment extends
        Fragment implements NotificationComm {
    private EditText et_description, et_link_url;
    private SwitchCompat switch_compact_status_of_notifi;
    private Button btn_last_date_of_notification, btn_add_notification_to_list;
    private NotificationEvent notificationObj;
    private Calendar cal;
    private boolean addNotifi = true;
    private NotificationEvent notificationObjUpdate;
    private int  appColor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cal = Calendar.getInstance(TimeZone.getDefault());
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 7);
        appColor = ((CreateNotiActivity)getContext()).getAppColor();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_create_notification, container, false);

        ((TextView) convertView.findViewById(R.id.tv_create_notification_linturl)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_create_notification_description)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_create_notification_last_Active_date)).setTextColor(appColor);


        et_description = (EditText) convertView.findViewById(R.id.et_create_notification_new_description);
        et_link_url = (EditText) convertView.findViewById(R.id.et_create_notification_new_link_url);
        switch_compact_status_of_notifi = (SwitchCompat) convertView.findViewById(R.id.switch_compact_status_of_notifi);
        /************************************   For change color ***********************************************************/
        switch_compact_status_of_notifi.setTextColor(appColor);
       Config.setSwitchCompactColor(switch_compact_status_of_notifi,appColor);
        /***********************************************************************************************/
        btn_last_date_of_notification = (Button) convertView.findViewById(R.id.
                btn_create_notification_last_date_of_notification);


        btn_add_notification_to_list = (Button) convertView.findViewById(R.id.btn_add_notification_to_list);
        btn_add_notification_to_list.setBackgroundColor(appColor);
        btn_add_notification_to_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!et_description.getText().toString().trim().equalsIgnoreCase("")) {
                    Config.hideKeyboard(getContext());
                    if (addNotifi) {
// for adding new NotificationEvent

                        String status = "";
                        if (switch_compact_status_of_notifi.isChecked())
                            status = "Active";
                        else status = "Inactive";

                        try {
                            ((CreateNotiActivity) getActivity()).addNotification(et_description.getText().toString().trim(),
                                    et_link_url.getText().toString().trim(),
                                    status,
                                    btn_last_date_of_notification.getText().toString().trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        // for updating old NotificationEvent

                        ((CreateNotiActivity) getActivity()).updateOldNotification(
                                notificationObjUpdate
                                , et_description.getText().toString().trim(),
                                et_link_url.getText().toString().trim(),
                                switch_compact_status_of_notifi.isChecked(),
                                btn_last_date_of_notification.getText().toString());

                    }


//                    et_link_url.setText("");
//                    et_description.setText("");
//                    switch_compact_status_of_notifi.setChecked(true);


                } else
                    et_description.setError(getString(R.string.please_write_some_data_to_add_notification));

            }
        });
        btn_last_date_of_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListener, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                Calendar calCurrent = Calendar.getInstance();
                calCurrent.set( calCurrent.get(Calendar.YEAR),
                        calCurrent.get(Calendar.MONTH),calCurrent.get(Calendar.DAY_OF_MONTH));
                datePicker.getDatePicker().setMinDate(calCurrent.getTimeInMillis());

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        return convertView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btn_last_date_of_notification.setText(cal.get(Calendar.YEAR) + "-" +
                (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH));
    }

    public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_last_date_of_notification.setText(year + "-" + new_month + "-" + new_day);
            // Toast.makeText(getContext(), year + "-" + new_month + "-" + new_day, Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void setData(ArrayList<NotificationEvent> data) {

    }

    @Override
    public void updateNotification(NotificationEvent notification) {
        btn_add_notification_to_list.setText(getString(R.string.update));

        et_description.setText(notification.getDescription());
        et_link_url.setText(notification.getLinkUrl());
        if (notification.getStatus().equalsIgnoreCase("Active"))
            switch_compact_status_of_notifi.setChecked(true);
        else switch_compact_status_of_notifi.setChecked(false);

        btn_last_date_of_notification.setText(notification.getDate());
        String date[] = notification.getDate().split("-");
        cal.set(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1,
                Integer.parseInt(date[2]));
        addNotifi = false;
        this.notificationObjUpdate = notification;
        Log.e("rituNotId",notification.getNotifyId()+" ritu");
    }

    public void refreshPage() {
        if (!btn_add_notification_to_list.getText().equals(getString(R.string.save))) {
            btn_add_notification_to_list.setText(getString(R.string.save));
            et_description.setText("");
            et_link_url.setText("");
            switch_compact_status_of_notifi.setChecked(true);

            addNotifi=true;
        }

    }

    public void clearFields() {
        if (et_description!=null)
            et_description.setText("");

        if (et_link_url!=null)
        et_link_url.setText("");

        if (switch_compact_status_of_notifi!=null)
        switch_compact_status_of_notifi.setChecked(true);
    }
}
