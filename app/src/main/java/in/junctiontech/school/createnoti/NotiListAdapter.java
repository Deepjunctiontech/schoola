package in.junctiontech.school.createnoti;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;

/**
 * Created by JAYDEVI BHADE on 9/29/2016.
 */

public class NotiListAdapter extends RecyclerView.Adapter<NotiListAdapter.MyViewHolder> {
    private ArrayList<NotificationEvent> notificationList;
    private Context context;
    private  int appColor =0;

    public NotiListAdapter(Context context, ArrayList<NotificationEvent> notificationList, int appColor) {
        this.notificationList = notificationList;
        this. appColor= appColor;
        this.context = context;
    }

    public void updateList(ArrayList<NotificationEvent> data) {
        this.notificationList = data;
        notifyDataSetChanged();
    }

    @Override
    public NotiListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_create_notification, parent, false);
        return new NotiListAdapter.MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final NotiListAdapter.MyViewHolder holder, int position) {
        final NotificationEvent notificationObj = notificationList.get(position);
        holder.tv_item_notification_date.setText(context.getString(R.string.date) + " : " + notificationObj.getDate());
        holder.tv_item_notification_one_line_notification.setText(notificationObj.getDescription());
        holder.tv_item_notification_total_notification.setText(notificationObj.getDescription());
        holder.tv_item_notification_link.setText(notificationObj.getLinkUrl());
        holder.tv_item_notification_last_active_date.setText(notificationObj.getDate());

        holder.tv_item_notification_status.setText(notificationObj.getStatus());

        holder.ll_create_notification_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notificationObj.isOnClickValve()) {
                    // if true i.e. currently open so close it
                    holder.ll_item_view_full_notification.setVisibility(View.VISIBLE);
                    holder.tv_item_notification_one_line_notification.setVisibility(View.INVISIBLE);
                    notificationObj.setOnClickValve(false);
                } else {

                    // if false i.e. currently close so open it
                    notificationObj.setOnClickValve(true);

                    holder.ll_item_view_full_notification.setVisibility(View.GONE);
                    holder.tv_item_notification_one_line_notification.setVisibility(View.VISIBLE);

                    //Toast.makeText(context,"")

                }

//                holder.ll_create_notification_description.setOnLongClickListener(new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View v) {
//                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
//                        final String[] aa = new String[]{
//                                context.getString(R.string.delete)
//                        };
//                        alert.setItems(aa, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
////                                if (which == 0) {
////
////                                    ((CreateNotificationActivity) context).updateNotification(notificationObj);
////
////                                } else
//                                if (which == 0) {
//                                    ((CreateNotificationActivity) context).deleteNotification(notificationObj);
//                                }
//
//
//                            }
//                        });
//
//                        alert.show();
//
//
//                        return false;
//                    }
//                });


            }
        });

        holder.tv_create_notification_notifi_number.setTextColor(Color.WHITE);
        holder.tv_create_notification_notifi_number.setText((position+1) + "");
        holder.tv_create_notification_notifi_number.setBackgroundResource(R.drawable.gridborder);

       // animate(holder);
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {

        return notificationList.size();
    }

    public void removeObject(int pos) {
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_notification_date, tv_item_notification_one_line_notification,
                tv_item_notification_total_notification, tv_item_notification_status,
                tv_item_notification_link, tv_item_notification_last_active_date,
                tv_create_notification_notifi_number;
        LinearLayout ll_item_view_full_notification, ll_create_notification_description
        ;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_create_notification_notifi_number = (TextView) itemView.findViewById(R.id.month_date);
            tv_item_notification_date = (TextView) itemView.findViewById(R.id.tv_item_notification_date);
            tv_item_notification_date.setTextColor(appColor);
            tv_item_notification_one_line_notification = (TextView) itemView.findViewById(R.id.tv_item_notification_one_line_notification);

            tv_item_notification_total_notification = (TextView) itemView.findViewById(R.id.tv_item_notification_total_notification);
            tv_item_notification_status = (TextView) itemView.findViewById(R.id.tv_item_notification_status);

            tv_item_notification_link = (TextView) itemView.findViewById(R.id.tv_item_notification_link);
            tv_item_notification_last_active_date = (TextView) itemView.findViewById(R.id.tv_item_notification_last_active_date);
            ll_item_view_full_notification = (LinearLayout) itemView.findViewById(R.id.ll_item_view_full_notification);
            ll_create_notification_description = (LinearLayout) itemView.findViewById(R.id.ll_create_notification_description);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final String[] aa = new String[]{
                            context.getString(R.string.update),
                            context.getString(R.string.delete)
                    };
                    alert.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                                if (which == 0) {

                                    ((CreateNotiActivity) context).
                                            updateNotification(notificationList.get(getLayoutPosition()));

                                } else  if (which == 1) {
                                ((CreateNotiActivity) context).deleteNotification(notificationList.get(getLayoutPosition()));
                            }


                        }
                    });

                    alert.show();


                    return false;
                }
            });

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }


}
