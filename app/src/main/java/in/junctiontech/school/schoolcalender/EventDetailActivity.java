package in.junctiontech.school.schoolcalender;

import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

public class EventDetailActivity extends AppCompatActivity {

    private RecyclerView rv_school_calendar_events;
    private EventsCalendarAdapter eventsCalendarAdapter;
    private TextView  rv_event_details_event_title, rv_event_details_event_subtitle;
    private ArrayList<SchoolCalenderEntity>  eventList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        eventList = (ArrayList<SchoolCalenderEntity>) getIntent().getSerializableExtra("eventList");
        initViews();
    }

    private void initViews() {
        rv_school_calendar_events = (RecyclerView) findViewById(R.id.rv_event_details_event_list);
        rv_event_details_event_title = (TextView) findViewById(R.id.rv_event_details_event_title);
        rv_event_details_event_subtitle = (TextView) findViewById(R.id.rv_event_details_event_subtitle);

        Calendar calObj = (Calendar) getIntent().getSerializableExtra("CalObj");
        rv_event_details_event_subtitle.setText(( new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH).format(calObj.getTime())));
        setupEventRecycler( );
        ((LinearLayout)findViewById(R.id.ll_event_detail_header)).setBackgroundColor(getIntent().getIntExtra("monthColor",getResources().getColor(R.color.bottomTabSelector)));
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getIntent().getIntExtra("monthColor", getResources().getColor(R.color.bottomTabSelector)));
            getWindow().setNavigationBarColor(getIntent().getIntExtra("monthColor", getResources().getColor(R.color.bottomTabSelector)));
        }
        }

    private void setupEventRecycler( ) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        // Attach the layout manager to the recycler view
        rv_school_calendar_events.setLayoutManager(gridLayoutManager);
        // Attach the layout manager to the recycler view
        rv_school_calendar_events.setLayoutManager(layoutManager);

        eventsCalendarAdapter = new EventsCalendarAdapter(this,eventList ,eventList.size()==0, true);
        rv_school_calendar_events.setAdapter(eventsCalendarAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
