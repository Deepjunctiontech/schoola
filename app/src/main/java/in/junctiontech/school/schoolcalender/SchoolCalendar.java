package in.junctiontech.school.schoolcalender;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 27-05-2017.
 *  * Updated by Deep on 10-05-2018
 */

public class SchoolCalendar implements Serializable {
    ArrayList<SchoolCalendar> result;
    String CalendarId,Title,Color, startdate,StartTime,enddate, EndTime,CalendarStatus,Date,Username;

    public ArrayList<SchoolCalendar> getResult() {
        return result;
    }

    public void setResult(ArrayList<SchoolCalendar> result) {
        this.result = result;
    }

    public String getCalendarId() {
        return CalendarId;
    }

    public void setCalendarId(String calendarId) {
        CalendarId = calendarId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getCalendarStatus() {
        return CalendarStatus;
    }

    public void setCalendarStatus(String calendarStatus) {
        CalendarStatus = calendarStatus;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}
