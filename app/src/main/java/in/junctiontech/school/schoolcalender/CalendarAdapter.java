package in.junctiontech.school.schoolcalender;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

/**
 * Created by JAYDEVI BHADE on 25-05-2017.
 *  * Updated by Deep on 10-05-2018
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {

    private Context context;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);


    private static final String TAG = CalendarAdapter.class.getSimpleName();

    private List<Date> monthlyDates;
    private Calendar currentDate;
    private ArrayList<SchoolCalenderEntity> allEvents;
    private int currentMonthDate =0;
    private int animationView ;
    private boolean animate= true;
    private int lastClickedPosition = 0;
/*
    public CalendarAdapter(Context context, int lengthAdapter) {
        this.context = context;
        this.lengthAdapter = lengthAdapter;
    }*/

    public CalendarAdapter(SchoolCalendarActivity1 schoolCalendarActivity1,
                           List<Date> monthlyDates, Calendar currentDate,
                           ArrayList<SchoolCalenderEntity> allEvents, int animationView) {
        this.animate= true;
        this.context = schoolCalendarActivity1;
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        this.allEvents = allEvents;
        this.currentMonthDate = Calendar.getInstance(Locale.ENGLISH).get(Calendar.DAY_OF_MONTH);
        this.animationView = animationView;
    }


    @Override
    public CalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar_layout, parent, false);
        return new CalendarAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final CalendarAdapter.MyViewHolder holder, final int position) {

        Date mDate = monthlyDates.get(position);
        final Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);

        final int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) ;
      //  int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH);
       // int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        Calendar calStartDate = Calendar.getInstance();
        Calendar calEndDate = Calendar.getInstance();

        holder.tv_item_calendar_event_color.setVisibility(View.INVISIBLE);

        if (allEvents!=null && allEvents.size()>position && allEvents.get(position)!=null)
            holder.tv_item_calendar_event_color.setVisibility(View.VISIBLE);

       /* for (SchoolCalendar schObj : allEvents   ) {
            try {
                Date  startEventDateObj = formatter.parse(schObj.getStartdate());
                Date  endEventDateObj = formatter.parse(schObj.getEnddate());
                calStartDate.setTime(startEventDateObj);
                calEndDate.setTime(endEventDateObj);


                if (dateCal.compareTo(calStartDate)>=0 && dateCal.compareTo(calEndDate)<=0) {
                    holder.tv_item_calendar_event_color.setVisibility(View.VISIBLE);
                    break;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/




    //    Log.e("RITU",dayValue+" r "+displayMonth+" r "+displayYear+" r "+currentMonth+" r " + currentYear);

        holder.itemViewHolder.setBackground(null);
        if (displayMonth == currentMonth && displayYear == currentYear) {
            holder.tv_item_calendar_date.setTextColor(Color.parseColor("#000000"));

            if ( currentMonthDate == dayValue) {
                holder.itemViewHolder.setBackground(context.getResources().getDrawable(R.drawable.circle_transparent_dark_border));
            }

///*//            holder.tv_item_calendar_date.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////
////                       Log.e("holder.calendar_date", dateCal.getTime() + "");
////                       currentMonthDate = dayValue;
////                       animate = false;
////
////                       notifyDataSetChanged();
////
////                       ((SchoolCalendarActivity1) context).openEventList(dateCal);
////
////                }
////            });




//            holder.itemViewHolder.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    Log.e(" holder.itemViewHolder",dateCal.getTime()+ "");
//                    currentMonthDate = dayValue;
//                    animate = false;
//
//                    notifyDataSetChanged();
//
//                    ((SchoolCalendarActivity1) context).openEventList(dateCal);
//                    return false;
//                }
//            });*/

        } else {
            holder.tv_item_calendar_date.setTextColor(Color.parseColor("#727272"));
        }


        //Add day to calendar

        holder.tv_item_calendar_date.setText(String.valueOf(dayValue));
        //Add events to the calendar


       /* if (position== 0 && getItemCount()==7){
            holder.itemViewHolder.performClick();
        }
*/


        if (animate)
        holder.itemViewHolder.startAnimation(AnimationUtils.loadAnimation(context, animationView));
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return monthlyDates.size();
    }

    public void updateCalendar(List<Date> monthlyDates, Calendar currentDate, ArrayList<SchoolCalenderEntity> allEvents, int animationView) {
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        this.allEvents = allEvents;
        this.animationView = animationView;
        this.animate= true;
        this.notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_calendar_date, tv_item_calendar_event_color;
        private View itemViewHolder;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_calendar_date = (TextView) itemView.findViewById(R.id.tv_item_calendar_date);
            tv_item_calendar_event_color = (TextView) itemView.findViewById(R.id.tv_item_calendar_event_color);
            this.itemViewHolder = itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Date mDate = monthlyDates.get(getLayoutPosition());
                    final Calendar dateCal = Calendar.getInstance();
                    dateCal.setTime(mDate);

                    Log.e(" holder.itemViewHolder",dateCal.getTime()+ "");
                    currentMonthDate = dateCal.get(Calendar.DAY_OF_MONTH);
                    animate = false;

                    notifyDataSetChanged();

                    ((SchoolCalendarActivity1) context).openEventList(dateCal);
                }
            });
        }
    }

}






