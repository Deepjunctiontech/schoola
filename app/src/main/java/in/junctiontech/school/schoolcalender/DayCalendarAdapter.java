package in.junctiontech.school.schoolcalender;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

/**
 * Created by JAYDEVI BHADE on 25-05-2017.
 *  * Updated by Deep on 10-05-2018
 */

public class DayCalendarAdapter extends RecyclerView.Adapter<DayCalendarAdapter.MyViewHolder> {

    private Context context;
    private int itemCount = 24;
    private ArrayList<SchoolCalenderEntity> eventList = new ArrayList<>();
    private int animationView ;

    public DayCalendarAdapter(Context context, int itemCount, ArrayList<SchoolCalenderEntity> eventList, int animationView) {
        this.context = context;
        this.itemCount = itemCount;
        this.eventList = eventList;
        this.animationView = animationView;
    }


    @Override
    public DayCalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_day_calendar_layout, parent, false);
        return new DayCalendarAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final DayCalendarAdapter.MyViewHolder holder, final int position) {

        if (position == 0) {
            holder.tv_item_day_calendar_time.setText("12 am");
            holder.tv_item_day_calendar_content1.setText("");
            holder.tv_item_day_calendar_content2.setText("");
            for (int i = 0; i < eventList.size(); i++) {
                SchoolCalenderEntity eventObj = eventList.get(i);
                if (i == 0) {
                    holder.tv_item_day_calendar_content1.setText(eventObj == null || eventObj.Title == null ? "" : eventObj.Title);
                    holder.tv_item_day_calendar_content1.setTextColor(Color.parseColor(eventObj == null || eventObj.Color == null ? "#727272" : eventObj.Color));

                } else if (i == 1) {
                    holder.tv_item_day_calendar_content2.setText(eventObj == null || eventObj.Title == null ? "" : eventObj.Title);
                    holder.tv_item_day_calendar_content2.setTextColor(Color.parseColor(eventObj == null || eventObj.Color == null ? "#727272" : eventObj.Color));
                    break;
                }

            }

        } else if (position < 12)
            holder.tv_item_day_calendar_time.setText((position) + "");
        else if (position == 12)
            holder.tv_item_day_calendar_time.setText((position) + " pm");
        else if (position > 12)
            holder.tv_item_day_calendar_time.setText((position - 12) + "");

        holder.itemViewHolder.startAnimation(AnimationUtils.loadAnimation(context, animationView));

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_day_calendar_time, tv_item_day_calendar_content1,
                tv_item_day_calendar_content2;
        private View itemViewHolder;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_day_calendar_time = (TextView) itemView.findViewById(R.id.tv_item_day_calendar_time);
            tv_item_day_calendar_content1 = (TextView) itemView.findViewById(R.id.tv_item_day_calendar_content1);
            tv_item_day_calendar_content2 = (TextView) itemView.findViewById(R.id.tv_item_day_calendar_content2);
            this.itemViewHolder = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((SchoolCalendarActivity1) context).openEventDetails(eventList);
                }
            });


        }
    }

}






