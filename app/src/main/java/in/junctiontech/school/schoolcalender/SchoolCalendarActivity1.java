package in.junctiontech.school.schoolcalender;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

public class SchoolCalendarActivity1 extends AppCompatActivity {

    private static final String TAG = "RITU";
    private RecyclerView rv_school_calendar;
    private CalendarAdapter calendarAdapter;
    private WeekCalendarAdapter weekCalendarAdapter;
    private TextView lastView;
    private TextView tv_school_calendar_week_padding, tv_school_calendar_week_padding2;
    private RecyclerView rv_school_calendar_events;
    private DayCalendarAdapter dayCalendarAdapter;
    private EventsCalendarAdapter eventsCalendarAdapter;
    private int whichSelected = 0;
    private Calendar monthCalendar;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private TextView currentDate;
    private float x1, x2;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private LinearLayout snackbar, ll_school_calendar_week_day;
    private ProgressDialog progressbar;
    ArrayList<SchoolCalendar> eventsList = new ArrayList<>();
    ArrayList<SchoolCalenderEntity> mEventsList = new ArrayList<>();
    private RelativeLayout rl_school_calendar_header;
    private int[] colourArray;
    //private Calendar currentSelectedDate = Calendar.getInstance();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int animationView = R.anim.from_right_to_left;
    MainDatabase mDb ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_calendar);
        sp = Prefs.with(this).getSharedPreferences();
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        mDb = MainDatabase.getDatabase(this);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        colourArray = getResources().getIntArray(R.array.color_array_calendar_background);

        monthCalendar = Calendar.getInstance(Locale.ENGLISH);
        initViews();

        setSelectedMonth();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (mEventsList.size() == 0)
                        getCalenderData();
                }
            }
        };
    }

    private void getCalenderData(){
//        mEventsList = (ArrayList<SchoolCalenderEntity>) mDb.schoolCalenderModel().getAllCalenderEvents();

        setUpCalendarAdapter(whichSelected == 0 ? true : false);
    }

    private void getCalendarDataFromServer() {
        mEventsList = new ArrayList<>();
        progressbar.show();
        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("monthYear", ((monthCalendar.get(Calendar.MONTH)) + 1) + "-" + monthCalendar.get(Calendar.YEAR));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();

        param_main.put("filter", job_filter);


        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "CalendarApi.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param_main);

        Log.e("FetchCalendarApiUrl", url);

        StringRequest request = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getCalendarData", s + "");
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                SchoolCalendar obj = gson.fromJson(s, new TypeToken<SchoolCalendar>() {
                                }.getType());
                                eventsList = new ArrayList<>();
                                eventsList = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if ( isFinishing()) {
                                    Config.responseVolleyHandlerAlert(SchoolCalendarActivity1.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));

                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            setUpCalendarAdapter(whichSelected == 0 ? true : false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getCalendarApiData", volleyError.toString());
                progressbar.cancel();

                setUpCalendarAdapter(whichSelected == 0 ? true : false);
                Config.responseVolleyErrorHandler(SchoolCalendarActivity1.this,volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

    }

    private void initViews() {

        rl_school_calendar_header = (RelativeLayout) findViewById(R.id.rl_school_calendar_header);
        ll_school_calendar_week_day = (LinearLayout) findViewById(R.id.ll_school_calendar_week_day);
        snackbar = (LinearLayout) findViewById(R.id.ll_school_calendar_activity);
        currentDate = (TextView) findViewById(R.id.tv_school_calendar_select_date);
        rv_school_calendar = (RecyclerView) findViewById(R.id.rv_school_calendar);
        rv_school_calendar_events = (RecyclerView) findViewById(R.id.rv_school_calendar_events);
        tv_school_calendar_week_padding = (TextView) findViewById(R.id.tv_school_calendar_week_padding);
        tv_school_calendar_week_padding2 = (TextView) findViewById(R.id.tv_school_calendar_week_padding2);
        ((TextView) findViewById(R.id.tv_school_calendar_month)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whichSelected!=0) {
                    whichSelected = 0;

                    performClick((TextView) v, View.GONE);
                    setUpCalendarAdapter(true);
                    // setupMonthRecycler(31);
                    setupWeekRecycler(0, new ArrayList<SchoolCalenderEntity>());
                }

            }
        });
        ((TextView) findViewById(R.id.tv_school_calendar_week)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whichSelected!=1) {
                    whichSelected = 1;
                    performClick((TextView) v, View.VISIBLE);
                    setUpCalendarAdapter(false);
                    //setupMonthRecycler(7);
                    //  setupWeekRecycler(24);
                }
            }
        });
        ((TextView) findViewById(R.id.tv_school_calendar_day)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (whichSelected!=2) {
                   whichSelected = 2;
                   performClick((TextView) v, View.VISIBLE);
                   setUpCalendarAdapter(false);
                   // setupMonthRecycler(7);
                   //   setupDayRecycler(24);
               }
            }
        });
        //  perFormMonthClick();

        currentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePicker = new DatePickerDialog(SchoolCalendarActivity1.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                animationView = R.anim.from_right_to_left;
                                monthCalendar.set(year, monthOfYear, dayOfMonth);

                                setSelectedMonth();
                                perFormMonthClick();
                            }
                        }, monthCalendar.get(Calendar.YEAR),
                        monthCalendar.get(Calendar.MONTH),
                        monthCalendar.get(Calendar.DAY_OF_MONTH));


                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        rv_school_calendar.setOnTouchListener(myViewTouchListener);
        rv_school_calendar_events.setOnTouchListener(myViewTouchListener);
        snackbar.setOnTouchListener(myViewTouchListener);
    }


    public View.OnTouchListener myViewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent touchevent) {
            switch (touchevent.getAction()) {

                case MotionEvent.ACTION_DOWN: {
                    x1 = touchevent.getX();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    x2 = touchevent.getX();

                    //if left to right sweep event on screen
                    if (x1+150 < x2) {
                        animationView = R.anim.from_left_to_come;
                        if (whichSelected == 0) {
                            monthCalendar.add(Calendar.MONTH, -1);
                            setUpCalendarAdapter(whichSelected == 0);
                        } else {
                            monthCalendar.add(Calendar.DAY_OF_YEAR, -7);
                            setUpCalendarAdapter(whichSelected == 0);
                        }
                        //   Toast.makeText(SchoolCalendarActivity1.this, "Left to Right Swap", Toast.LENGTH_LONG).show();
                    }

                    // if right to left sweep event on screen
                    if (x1 > x2+150) {
                        animationView = R.anim.from_right_to_left;
                        if (whichSelected == 0) {
                            monthCalendar.add(Calendar.MONTH, 1);

                            setUpCalendarAdapter(whichSelected == 0);
                        } else {
                            monthCalendar.add(Calendar.DAY_OF_YEAR, +7);
                            setUpCalendarAdapter(whichSelected == 0);
                        }
                        //  Toast.makeText(SchoolCalendarActivity1.this, "Right to Left Swap ", Toast.LENGTH_LONG).show();
                    }

                    break;
                }
            }
            return false;
        }
    };

    private void perFormMonthClick() {
        ((TextView) findViewById(R.id.tv_school_calendar_month)).performClick();
    }

    private void setupEventRecycler(Calendar calEventOpenDateObj) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_school_calendar_events.setLayoutManager(layoutManager);

        ArrayList<SchoolCalenderEntity> eventCalList = getEventListOfDate(calEventOpenDateObj);
        eventsCalendarAdapter = new EventsCalendarAdapter(this, eventCalList, eventCalList.size() == 0, false);

        rv_school_calendar_events.setAdapter(eventsCalendarAdapter);

    }

    public void performClick(TextView v, int visibility) {

        if (lastView != null) lastView.setTextColor(getResources().getColor(R.color.black));
        else
            ((TextView) findViewById(R.id.tv_school_calendar_month)).setTextColor(getResources().getColor(R.color.black));
        v.setTextColor(getResources().getColor(R.color.white));

        lastView = v;
        tv_school_calendar_week_padding.setVisibility(visibility);
        tv_school_calendar_week_padding2.setVisibility(visibility);
    }


    private void setupWeekRecycler(int itemCount, ArrayList<SchoolCalenderEntity> weekEventList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        // Attach the layout manager to the recycler view
        rv_school_calendar_events.setLayoutManager(gridLayoutManager);
        // Attach the layout manager to the recycler view
        rv_school_calendar_events.setLayoutManager(layoutManager);
        weekCalendarAdapter = new WeekCalendarAdapter(this, itemCount, weekEventList, animationView);
        rv_school_calendar_events.setAdapter(weekCalendarAdapter);

    }

    public ArrayList<SchoolCalenderEntity> getEventListOfDate(Calendar calEventOpenDateObj) {
        Calendar calStartDate = Calendar.getInstance();
        Calendar calEndDate = Calendar.getInstance();

        if (calEventOpenDateObj == null) calEventOpenDateObj = Calendar.getInstance();
        calEventOpenDateObj.set(Calendar.HOUR_OF_DAY, 0);
        calEventOpenDateObj.set(Calendar.MINUTE, 0);
        calEventOpenDateObj.set(Calendar.SECOND, 0);
        calEventOpenDateObj.set(Calendar.MILLISECOND, 0);

        ArrayList<SchoolCalenderEntity> eventCalList = new ArrayList<>();
        for (SchoolCalenderEntity schObj : mEventsList) {
            try {
                Date startEventDateObj = (new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)).parse(schObj.startdate);
                Date endEventDateObj = (new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)).parse(schObj.enddate);
                calStartDate.setTime(startEventDateObj);
                calEndDate.setTime(endEventDateObj);

                if (calEventOpenDateObj.compareTo(calStartDate) >= 0 && calEventOpenDateObj.compareTo(calEndDate) <= 0) {
                    eventCalList.add(schObj);

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        return eventCalList;
    }

    private void setupDayRecycler(int itemCount, ArrayList<SchoolCalenderEntity> dayEventList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        // Attach the layout manager to the recycler view
        rv_school_calendar_events.setLayoutManager(layoutManager);

        dayCalendarAdapter = new DayCalendarAdapter(this, itemCount, dayEventList, animationView);
        rv_school_calendar_events.setAdapter(dayCalendarAdapter);


    }

    public void openEventList(Calendar eventOpenDateObj) {
        if (eventOpenDateObj.get(Calendar.MONTH) != monthCalendar.get(Calendar.MONTH)) {
            monthCalendar.set(eventOpenDateObj.get(Calendar.YEAR),eventOpenDateObj.get(Calendar.MONTH),eventOpenDateObj.get(Calendar.DAY_OF_MONTH));

            setUpCalendarAdapter(whichSelected == 0);
        }
        monthCalendar = eventOpenDateObj;

        if (whichSelected == 0) setupEventRecycler(eventOpenDateObj);
        else if (whichSelected == 1)
            ((TextView) findViewById(R.id.tv_school_calendar_day)).performClick();
        else if (whichSelected == 2) setUpCalendarAdapter(false);
    }

    public void openEventDetails(ArrayList<SchoolCalenderEntity> eventList) {
        startActivity(new Intent(this, EventDetailActivity.class)
                .putExtra("eventList", eventList)
                .putExtra("monthColor", colourArray[monthCalendar.get(Calendar.MONTH)])
                .putExtra("CalObj", monthCalendar == null ? Calendar.getInstance() : monthCalendar)
        );
    }


    private void setUpCalendarAdapter(boolean isMonth) {
        progressbar.show();
        List<Date> dayValueInCells = new ArrayList<Date>();
        ArrayList<SchoolCalenderEntity> listOfEvents = new ArrayList<>();
        Calendar mCal = (Calendar) monthCalendar.clone();
        mCal.set(Calendar.HOUR_OF_DAY, 0);
        mCal.set(Calendar.MINUTE, 0);
        mCal.set(Calendar.SECOND, 0);
        mCal.set(Calendar.MILLISECOND, 0);

        if (isMonth) {
            mCal.set(Calendar.DAY_OF_MONTH, 1);
            int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
            mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);


            while (dayValueInCells.size() < 42) {
                dayValueInCells.add(mCal.getTime());
                ArrayList<SchoolCalenderEntity> localList = getEventListOfDate(mCal);
                listOfEvents.add(localList.size() == 0 ? null : localList.get(0));

                mCal.add(Calendar.DAY_OF_MONTH, 1);
            }
        } else {

            mCal.set(Calendar.WEEK_OF_MONTH, monthCalendar == null ? Calendar.WEEK_OF_MONTH + 1 : monthCalendar.get(Calendar.WEEK_OF_MONTH));
            int firstDayOfTheWeek = mCal.get(Calendar.DAY_OF_WEEK) - 1;
            mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheWeek);
            //  mCal.set(Calendar.MONTH, mCal.get(Calendar.MONTH) - 1);

            if (whichSelected == 1) {

                while (dayValueInCells.size() < 7) {
                    dayValueInCells.add(mCal.getTime());

                    ArrayList<SchoolCalenderEntity> localList = getEventListOfDate(mCal);
                    listOfEvents.add(localList.size() == 0 ? null : localList.get(0));

                    mCal.add(Calendar.DAY_OF_MONTH, 1);
                }
           /*           For setting of week adapter   * */
                setupWeekRecycler(24, listOfEvents);
            } else if (whichSelected == 2) {
                ArrayList<SchoolCalenderEntity> localList = getEventListOfDate(monthCalendar);

                while (dayValueInCells.size() < 7) {
                    dayValueInCells.add(mCal.getTime());

                    ArrayList<SchoolCalenderEntity> aa = getEventListOfDate(mCal);
                    listOfEvents.add(aa.size() == 0 ? null : aa.get(0));

                    mCal.add(Calendar.DAY_OF_MONTH, 1);
                }
           /*           For setting of week adapter   * */
                setupDayRecycler(24, localList.size() == 0 ? new ArrayList<SchoolCalenderEntity>() : localList);
            }
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());
        if (calendarAdapter == null) {
            calendarAdapter = new CalendarAdapter(this, dayValueInCells, monthCalendar, listOfEvents, animationView);

            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
            // Attach the layout manager to the recycler view
            rv_school_calendar.setLayoutManager(gridLayoutManager);
            rv_school_calendar.setAdapter(calendarAdapter);

        } else if (currentDate.getText().toString().equalsIgnoreCase(formatter.format(monthCalendar.getTime())))
            calendarAdapter.updateCalendar(dayValueInCells, monthCalendar, listOfEvents, animationView);

        setSelectedMonth();

        // calendarAdapter = new CalendarAdapter(this, itemCount);
        progressbar.dismiss();
    }

    private void setSelectedMonth() {
        if (!currentDate.getText().toString().equalsIgnoreCase(formatter.format(monthCalendar.getTime()))) {
            getCalenderData();
            currentDate.setText(formatter.format(monthCalendar.getTime()));
            tv_school_calendar_week_padding.setBackgroundColor(colourArray[monthCalendar.get(Calendar.MONTH)]);
            rl_school_calendar_header.setBackgroundColor(colourArray[monthCalendar.get(Calendar.MONTH)]);
            ll_school_calendar_week_day.setBackgroundColor(colourArray[monthCalendar.get(Calendar.MONTH)]);
            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(colourArray[monthCalendar.get(Calendar.MONTH)]);
                getWindow().setNavigationBarColor(colourArray[monthCalendar.get(Calendar.MONTH)]);
            }
        }

    }


    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

    }
}
