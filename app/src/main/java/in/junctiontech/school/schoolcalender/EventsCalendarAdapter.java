package in.junctiontech.school.schoolcalender;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

/**
 * Created by JAYDEVI BHADE on 26-05-2017.
 * Updated by Deep on 10-05-2018
 */

public class EventsCalendarAdapter extends RecyclerView.Adapter<EventsCalendarAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SchoolCalenderEntity> eventList;
    private boolean isListSizeZero;
    private boolean isListForEventDetails;

    public EventsCalendarAdapter(Context context, ArrayList<SchoolCalenderEntity> eventList, boolean isListSizeZero, boolean isListForEventDetails) {
        this.context = context;
        if (eventList.size()==0)
            eventList.add(new SchoolCalenderEntity());
        this.eventList = eventList;
        this.isListSizeZero = isListSizeZero;
        this.isListForEventDetails = isListForEventDetails;
    }


    @Override
    public EventsCalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar_event_layout, parent, false);
        return new EventsCalendarAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final EventsCalendarAdapter.MyViewHolder holder, final int position) {
        SchoolCalenderEntity eventObj = eventList.get(position);
        if (isListSizeZero) {
            holder.tv_item_calendar_event_dots.setVisibility(View.GONE);
            holder.tv_item_calendar_event_data.setPadding(20, 0, 0, 0);
            holder.tv_item_calendar_event_data.setText(context.getString(R.string.no_events_available));
        } else {
            holder.tv_item_calendar_event_dots.setBackgroundColor(Color.parseColor((eventObj.Color == null || eventObj.Color == "") ? "#E64A19" : eventObj.Color));
            holder.tv_item_calendar_event_data.setText(
                    eventObj.Title == null ? "" : eventObj.Title);


            if (isListForEventDetails){
                holder.tv_item_calendar_event_data.setTextColor(Color.parseColor((eventObj.Color == null || eventObj.Color == "") ? "#E64A19" : eventObj.Color));

                holder.ll_item_calendar_event_data.setVisibility(View.VISIBLE);
                holder.tv_item_calendar_event_data_date.setText(
                        context.getString(R.string.starts) +" : "+
                                (  eventObj.startdate == null ? "" : eventObj.startdate)
                                +" "+ (eventObj.StartTime == null ? "" : eventObj.StartTime)
                        +"\n"+
                                context.getString(R.string.ends) +" :   "+
                                (eventObj.enddate == null ? "" : eventObj.enddate)
                                +" "+ (eventObj.EndTime == null ? "" : eventObj.EndTime)
                );
            }
        }

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_calendar_event_dots, tv_item_calendar_event_data,
                tv_item_calendar_event_data_date;
        private LinearLayout ll_item_calendar_event_data;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_calendar_event_dots = (TextView) itemView.findViewById(R.id.tv_item_calendar_event_dots);
            tv_item_calendar_event_data = (TextView) itemView.findViewById(R.id.tv_item_calendar_event_data);
            tv_item_calendar_event_data_date = (TextView) itemView.findViewById(R.id.tv_item_calendar_event_data_date);
            ll_item_calendar_event_data = (LinearLayout) itemView.findViewById(R.id.ll_item_calendar_event_data);

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.bottom_up));


        }
    }

}






