package in.junctiontech.school.schoolcalender;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;

/**
 * Created by JAYDEVI BHADE on 25-05-2017.
 *  * Updated by Deep on 10-05-2018
 */

public class WeekCalendarAdapter extends RecyclerView.Adapter<WeekCalendarAdapter.MyViewHolder> {
    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    private Context context;
    private int itemCount=24;
    private int animationView;
    private ArrayList<SchoolCalenderEntity> allEvents = new ArrayList<>();

    public WeekCalendarAdapter(Context context, int itemCount, ArrayList<SchoolCalenderEntity> allEvents, int animationView) {
        this.context = context;
        this.allEvents  = allEvents;
        this.itemCount= itemCount;
        this.animationView = animationView;
    }


    @Override
    public WeekCalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_week_calendar_layout, parent, false);
        return new WeekCalendarAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final WeekCalendarAdapter.MyViewHolder holder, final int position) {


      if (position ==0) {
          holder.tv_item_week_calendar_time.setText("12 am");
          holder.tv_item_week_calendar_sun.setText("");
          holder.tv_item_week_calendar_mon.setText("");
          holder.tv_item_week_calendar_tue.setText("");
          holder.tv_item_week_calendar_wed.setText("");
          holder.tv_item_week_calendar_thr.setText("");
          holder.tv_item_week_calendar_fri.setText("");
          holder.tv_item_week_calendar_sat.setText("");

          for (int i=0;i<allEvents.size();i++ ) {
              SchoolCalenderEntity eventObj = allEvents.get(i);
              switch (i){
                  case 0:

                      holder.tv_item_week_calendar_sun.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_sun.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));
                      break;
                  case 1:  holder.tv_item_week_calendar_mon.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_mon.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;
                  case 2:  holder.tv_item_week_calendar_tue.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_tue.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;
                  case 3: holder.tv_item_week_calendar_wed.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_wed.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;
                  case 4: holder.tv_item_week_calendar_thr.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_thr.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;
                  case 5: holder.tv_item_week_calendar_fri.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_fri.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;
                  case 6:holder.tv_item_week_calendar_sat.setText(eventObj==null||eventObj.Title==null?"":eventObj.Title);
                      holder.tv_item_week_calendar_sat.setTextColor(Color.parseColor( eventObj==null||eventObj.Color==null?"#727272":eventObj.Color ));

                      break;

              }

          }

      } else if (position <12)
            holder.tv_item_week_calendar_time.setText((position) + "");
        else if (position ==12)
            holder.tv_item_week_calendar_time.setText((position ) + " pm");
        else  if (position >12)
            holder.tv_item_week_calendar_time.setText((position-12 ) + "");

        holder.itemViewHolder.startAnimation(AnimationUtils.loadAnimation(context, animationView));

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return itemCount;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_week_calendar_time, tv_item_week_calendar_sat,
                tv_item_week_calendar_sun,tv_item_week_calendar_fri,tv_item_week_calendar_thr,
                tv_item_week_calendar_wed, tv_item_week_calendar_tue,tv_item_week_calendar_mon;
        private View view_item_week_calendar_time;
        private View itemViewHolder;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemViewHolder = itemView;



            tv_item_week_calendar_time = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_time);
            tv_item_week_calendar_sun = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_sun);
            tv_item_week_calendar_mon = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_mon);
            tv_item_week_calendar_tue = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_tue);
            tv_item_week_calendar_wed = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_wed);
            tv_item_week_calendar_thr = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_thr);
            tv_item_week_calendar_fri = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_fri);
            tv_item_week_calendar_sat = (TextView) itemView.findViewById(R.id.tv_item_week_calendar_sat);



        }
    }

}






