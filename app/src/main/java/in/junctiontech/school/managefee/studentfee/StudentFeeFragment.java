package in.junctiontech.school.managefee.studentfee;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.managefee.payment.Fees;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.RegisteredStudent;

/**
 * Created by JAYDEVI BHADE on 2/3/2017.
 */

public class StudentFeeFragment extends Fragment {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private Spinner sp_class, sp_student, sp_distance_list;
    private Button btn_assign_new_fee, btn_update;
    private RecyclerView rv_assigned_fee_list;
    private TextView tv_fee_not_available;
    private ArrayList<RegisteredStudent> registeredStudentList = new ArrayList<>();
    private boolean logoutAlert;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>(),
            sectionIdList = new ArrayList<>(),
            classIdList= new ArrayList<>();
    private ArrayAdapter<String> classAdapter;
    private ArrayList<String> studentAdmissionNameList = new ArrayList<>(),
            studentAdmissionIdList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private Calendar cal;
    private LinearLayout snackbar;
    private LinearLayout ll_fee_list;
    private ArrayList<Fees> feesList = new ArrayList<>();
    private ArrayList<Fees> transportList = new ArrayList<>();
    private LinearLayout ll_account_selection;
    private ArrayAdapter<String> distanceAdapter;
    private ArrayList<String> distanceList = new ArrayList<>();
    private EditText et_distance_amount;
    private TextInputLayout til_distance_amount;
    private Button btn_effective_date_month_year;
    private int  appColor =0;
    private TextView  tv_fee_payment_total_session,tv_fee_payment_total_monthly;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_fee_payment, container, false);
        ll_account_selection = (LinearLayout) view.findViewById(R.id.ll_fee_payment_account_selection);
        ( view.findViewById(R.id.ll_fee_payment_date_amount_view)).setVisibility(View.GONE);
        ( view.findViewById(R.id.ll_fee_payment_effective_date_view)).setVisibility(View.VISIBLE);
        btn_effective_date_month_year= (Button)view.findViewById(R.id.btn_fee_payment_effective_date_month_year);

        ll_fee_list = (LinearLayout) view.findViewById(R.id.ll_fee_payment_student_fee_list);
        TextView tv_fee_payment_spinner_name=   (TextView) view.findViewById(R.id.tv_fee_payment_spinner_name);
        tv_fee_payment_spinner_name.setText(getString(R.string.distance));
        tv_fee_payment_spinner_name.setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_sp_fee_payment_class)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_sp_fee_payment_student)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_fee_payment_name_due_or_paid)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_fee_payment_spinner_name)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_btn_fee_payment_effective_date_month_year)).setTextColor(appColor);
        (  view.findViewById(R.id.ll_fee_payment_total_session)).setVisibility(View.VISIBLE);
        (  view.findViewById(R.id.ll_fee_payment_total_monthly)).setVisibility(View.VISIBLE);

        snackbar = (LinearLayout) view.findViewById(R.id.ll_fee_payment);
        sp_class = (Spinner) view.findViewById(R.id.sp_fee_payment_class);
        sp_student = (Spinner) view.findViewById(R.id.sp_fee_payment_student);
        sp_distance_list = (Spinner) view.findViewById(R.id.sp_fee_payment_account_list);

        tv_fee_payment_total_session = (TextView) view.findViewById(R.id.tv_fee_payment_total_session);
        tv_fee_payment_total_monthly = (TextView) view.findViewById(R.id.tv_fee_payment_total_monthly);

        sp_distance_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    til_distance_amount.setVisibility(View.VISIBLE);
                    et_distance_amount.setText(transportList.get(position - 1).getStudentFeeAmount());
            }   else if (position==0){
                    til_distance_amount.setVisibility(View.GONE);
                }
                calculateAmount();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, sectionIdList.get(position))
                        .putString(Config.LAST_USED_CLASS_ID,classIdList.get(position))
                        .commit();
                resetStudentListSectionWise(sectionIdList.get(position));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getFeeSrtucture();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_update = (Button) view.findViewById(R.id.btn_fee_payment_pay_fee);
        btn_update.setText(getString(R.string.update_student_fee));
        tv_fee_not_available = (TextView) view.findViewById(R.id.tv_fee_payment_no_dues);
        tv_fee_not_available.setText(getString(R.string.fees_not_available));
        et_distance_amount = (EditText) view.findViewById(R.id.et_fee_payment_distance_amount);
        til_distance_amount = (TextInputLayout) view.findViewById(R.id.til_fee_payment_distance_amount);

        et_distance_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
calculateAmount();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_update.setBackgroundColor(appColor);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStudentFee();
            }
        });

        btn_effective_date_month_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListener, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));


                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        return view;
    }

    public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            cal.set(year, monthOfYear, dayOfMonth);
            Date d = new Date();
            d.setYear(cal.get(Calendar.YEAR));
            d.setMonth(cal.get(Calendar.MONTH));
            d.setDate(cal.get(Calendar.DAY_OF_MONTH));

            btn_effective_date_month_year.setText(Config.monthYear.format(d)+" "+year);
 }
    };

    private void updateStudentFee() {
        if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0 && feesList.size() > 0) {
            JSONArray jsonArrayFeesUpdate = new JSONArray();
            for (Fees feesObj : feesList) {
                if (!feesObj.getStudentFeeAmount().trim().isEmpty()) {
                    if (Double.parseDouble(feesObj.getStudentFeeAmount()) != 0) {
                        Map<String, Object> paramFee = new LinkedHashMap<String, Object>();
                        paramFee.put("FeeId", feesObj.getFeeId());
                        paramFee.put("StudentFeeAmount", feesObj.getStudentFeeAmount());
                        paramFee.put("Assign", "Yes");

                        jsonArrayFeesUpdate.put(new JSONObject(paramFee));
                    }
                }

            }
            if (sp_distance_list.getSelectedItemPosition() > 0) {

                if (!et_distance_amount.getText().toString().equalsIgnoreCase("")
                        && Double.parseDouble(et_distance_amount.getText().toString()) != 0
                        ) {

                    Map<String, Object> paramFee = new LinkedHashMap<String, Object>();
                    paramFee.put("FeeId", transportList.get(sp_distance_list.getSelectedItemPosition() - 1).getFeeId());
                    paramFee.put("StudentFeeAmount", et_distance_amount.getText().toString());
                    paramFee.put("Assign", "Yes");

                    jsonArrayFeesUpdate.put(new JSONObject(paramFee));

                }

            }
            if (jsonArrayFeesUpdate.length() > 0) {
                try {
                    updateFee(jsonArrayFeesUpdate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Snackbar.make(snackbar,
                        R.string.fees_not_available,
                        Snackbar.LENGTH_LONG)
                        .setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
            }

        } else {
            showSnack();

        }
    }

    private void updateFee(JSONArray jsonArrayFeesUpdate) throws JSONException {
        progressbar.show();
        final JSONObject param = new JSONObject();
        param.put("admissionId", studentAdmissionIdList.get(sp_student.getSelectedItemPosition()));
        param.put("session", sp.getString(Config.SESSION, ""));
        param.put("remarks", "");
        param.put("effectiveFrom",btn_effective_date_month_year.getText().toString());
        param.put("fees", jsonArrayFeesUpdate);

        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "UpdateFees.php?databaseName=" +
                dbName;

        Log.e("studentUpdateFeeUrl", studentFetUrl);
        Log.e("param", param.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, studentFetUrl,param
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("getStudentUpdateData", jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optInt("code") == 200 || jsonObject.optInt("code") == 201) {



                                Snackbar snackbarObj = Snackbar.make(
                                        snackbar,

                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("",
                                        new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else
                                Config.responseSnackBarHandler( jsonObject.optString("message"),snackbar);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStudentUpdateData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, getTag());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        cal = Calendar.getInstance();
        appColor = Config.getAppColor(getActivity(),false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");

                    if (!(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                            ||
                            sp.getString("user_type", "").equalsIgnoreCase("Student")

                    )) {
                        if (classListData.size() == 0)
                            getClassListFromServer();

                        if (registeredStudentList.size() == 0)
                            getRegisteredStudentListFromServer();


                    }


                }
            }
        };


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            if (classListData.size() == 0)
                getClassListFromServer();
            if (registeredStudentList.size() == 0)
                getRegisteredStudentListFromServer();


            btn_effective_date_month_year.setText((new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH)).format((cal.getTime())) );
        }
    }

    private void getClassListFromServer() {

        progressbar.show();

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");

        Log.d("fetchClassList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
                                }.getType());
                                classListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            setUpClassSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

        //}
    }

    private void setUpClassSpinner() {
        if (sp_class != null) {
          /* class spinner */
            classSectionNameList = new ArrayList<>();
            sectionIdList = new ArrayList<>();
            classIdList= new ArrayList<>();

            int lastUsedSectionIdPos = 0;
            int jj = 0;
            for (CreateClass clsObj : classListData) {

                for (CreateClass secObj : clsObj.getSectionList()) {
                    classIdList.add(clsObj.getClassId());
                    classSectionNameList.add(clsObj.getClassName() + " " + secObj.getSectionName());
                    sectionIdList.add(secObj.getSectionId());

                    if (sp.getString(Config.LAST_USED_SECTION_ID, "").
                            equalsIgnoreCase(secObj.getSectionId())) {
                        lastUsedSectionIdPos = jj ;

                    }
                    jj++;
                }

            }

            classAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, classSectionNameList);
          classAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_class.setAdapter(classAdapter);

            if (classSectionNameList.size() > 0) sp_class.setSelection(lastUsedSectionIdPos);
            else showSnack();

        }
    }

    private void  showSnack() {
        Snackbar snackbarObj =null;
        if (classSectionNameList.size() == 0){
            snackbarObj    = Snackbar.make(
                    snackbar,

                    getString(R.string.classes_not_available),
                    Snackbar.LENGTH_LONG).setAction( getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                Toast.makeText(getActivity(),"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                            }else {startActivity(new Intent(getActivity(), ClassSectionActivity.class));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });

        } else if (studentAdmissionIdList.size() == 0)  snackbarObj    = Snackbar.make(
                snackbar,

                getString(R.string.students_not_available),
                Snackbar.LENGTH_LONG).setAction( getString(R.string.create_student),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!sp.getBoolean(Config.CREATE_STUDENT_PERMISSION, false)) {
                            Toast.makeText(getActivity(), "Permission not available : CREATE STUDENT !", Toast.LENGTH_SHORT).show();
                        } else {
//                            startActivity(new Intent(getActivity(), CreateStudentActivity.class));

                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                    }
                });
        else if (feesList.size() == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,

                    getString(R.string.fees_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.class_fee),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CLASS_FEE_PERMISSION, false)) {
                                Toast.makeText(getActivity(), "Permission not available : CLASS FEE !", Toast.LENGTH_SHORT).show();
                            } else {   startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "addClassFee"));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
        } else if (distanceList.size() <= 1) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.transport_fee) + " " + getString(R.string.not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.class_fee),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) { if (!sp.getBoolean(Config.TRANSPORT_FEE_PERMISSION, false)) {
                            Toast.makeText(getActivity(), "Permission not available : TRANSPORT FEE !", Toast.LENGTH_SHORT).show();
                        } else {
                            startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "addTransportFee"));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
        }
        if (snackbarObj!=null) {
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }
    }


    public void getRegisteredStudentListFromServer() {

        progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Session", sp.getString(Config.SESSION, ""));


        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "StudentRegistrationApi.php?databaseName=" +
                dbName + "&action=studentParentGet&data=" + new JSONObject(param);

        Log.e("studentFetUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStudentData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                RegisteredStudent obj = gson.fromJson(s, new TypeToken<RegisteredStudent>() {
                                }.getType());
                                registeredStudentList = obj.getResult();
                                if (sectionIdList.size() > 0 && !(getActivity()).isFinishing())
                                    resetStudentListSectionWise(sectionIdList.get(sp_class.getSelectedItemPosition()));

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStudentData", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
        // }
    }

    private void resetStudentListSectionWise(String sectionId) {
        studentAdmissionIdList = new ArrayList<>();
        studentAdmissionNameList = new ArrayList<>();

        for (RegisteredStudent registeredStudentObj : registeredStudentList) {
            if (registeredStudentObj.getSectionId().equalsIgnoreCase(sectionId)) {
                studentAdmissionNameList.add(registeredStudentObj.getStudentName() + " (" +
                        registeredStudentObj.getAdmissionNo() + ")");
                studentAdmissionIdList.add(registeredStudentObj.getAdmissionId());
            }
        }
        if (studentAdmissionIdList.size() == 0) {
            showFeeList();
        }
        studentAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, studentAdmissionNameList);
        studentAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_student.setAdapter(studentAdapter);
    }



    public void getFeeSrtucture() {

        if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0) {
            progressbar.show();
            Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("admissionId", studentAdmissionIdList.get(sp_student.getSelectedItemPosition()));
            param.put("session", sp.getString(Config.SESSION, ""));

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));


            String studentFetUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +
                    "UpdateFees.php?databaseName=" +
                    dbName + "&data=" + new JSONObject(param1);

            Log.e("studentFeeUrl", studentFetUrl);

            StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                    studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getStudentData", s);
                            progressbar.cancel();
                            feesList = new ArrayList<>();
                            transportList = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                    Fees obj = gson.fromJson(s, new TypeToken<Fees>() {
                                    }.getType());
                                    Fees feesObj = obj.getResult();
                                    feesList = feesObj.getFees();
                                    transportList = feesObj.getTransportfees();


                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(getActivity()).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                               else
                                    Config.responseSnackBarHandler(jsonObject.optString("message"),snackbar);


                                showFeeList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getStudentData", volleyError.toString());
                    progressbar.cancel();

                    Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, getTag());
        } else {
            if (classSectionNameList.size() == 0)
                Snackbar.make(snackbar,
                        R.string.sections_not_available,
                        Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).show();
            else Snackbar.make(snackbar,
                    R.string.students_not_available,
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        }

    }

    private void showFeeList() {
        if (ll_fee_list != null) {

            if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0) {
                progressbar.show();
                boolean isDue = true;
                ll_fee_list.setVisibility(View.VISIBLE);


                ll_fee_list.removeAllViews();


                if (feesList.size() == 0) {
                   showSnack();
                } else {
                    for (final Fees obj : feesList) {
                        View view = getActivity().getLayoutInflater().inflate(R.layout.item_add_manage_fee, null);

                        String next = "<font color='#727272'><br>" + getString(R.string.actual_amount_is) + " :"
                                + obj.getActualAmount() + "</font>";

                        TextView tv_item_fee_name=  (TextView) view.findViewById(R.id.tv_item_fee_name);
                        tv_item_fee_name.setTextColor(appColor);
                        tv_item_fee_name.setText(Html.fromHtml(obj.getFeeTypeName().toUpperCase() +
                                        next));
                        final EditText et_item_amount = (EditText) view.findViewById(R.id.et_item_amount);
                        et_item_amount.setText(obj.getStudentFeeAmount());

                        et_item_amount.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                obj.setStudentFeeAmount(s.toString().trim());
                                calculateAmount();
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                        view.setPadding(5, 0, 5, 0);
                        ll_fee_list.addView(view);

                        isDue = false;


                    }


                    if (isDue) {
                        tv_fee_not_available.setVisibility(View.VISIBLE);
                        Snackbar.make(snackbar,
                                getString(R.string.fees_not_available) + " " + getString(R.string.to_pay) + " !",
                                Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
                    } else tv_fee_not_available.setVisibility(View.GONE);
                }


                int transportAssignedPos = 0;
                int pos = 0;
                et_distance_amount.setText("");
                distanceList = new ArrayList<>();
                distanceList.add(getString(R.string.select));
                for (Fees transObj : transportList) {
                    distanceList.add(transObj.getDistanceKM()
                            + "\n" + getString(R.string.actual_amount_is) + " : " +
                            transObj.getActualAmount());
                    if (transObj.getAssign().equalsIgnoreCase("Yes"))
                        transportAssignedPos = pos + 1;
                    pos++;
                }

                if (transportList.size() > 0) {
                    til_distance_amount.setVisibility(View.VISIBLE);
                    ll_account_selection.setVisibility(View.VISIBLE);
                    distanceAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1, distanceList);
                  distanceAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
                    sp_distance_list.setAdapter(distanceAdapter);
                    sp_distance_list.setSelection(transportAssignedPos);
                    if (transportAssignedPos > 0)
                        et_distance_amount.setText(transportList.get(transportAssignedPos - 1).getStudentFeeAmount());
                } else {
                    ll_account_selection.setVisibility(View.GONE);
                    til_distance_amount.setVisibility(View.GONE);
                    showSnack();
                }

                progressbar.dismiss();
            } else {
                ll_fee_list.setVisibility(View.GONE);
                til_distance_amount.setVisibility(View.GONE);
                ll_account_selection.setVisibility(View.GONE);

               showSnack();
            }
        }
        calculateAmount();
    }

    private void calculateAmount() {
        double monthPay = 0;
        double sessionPay = 0;


        for (Fees obj : feesList) {

                if (obj.getStudentFeeAmount() != null && !obj.getStudentFeeAmount().equalsIgnoreCase("")  ) {


                        Log.e("Frequency", obj.getFrequency() + "   " + obj.getStudentFeeAmount());
                        if (obj.getFrequency().equalsIgnoreCase("Session"))
                            sessionPay = sessionPay + (long) Double.parseDouble(obj.getStudentFeeAmount());
                        else if (obj.getFrequency().equalsIgnoreCase("Monthly"))
                            monthPay = monthPay + (long) Double.parseDouble(obj.getStudentFeeAmount());



                }

        }

        tv_fee_payment_total_monthly.setText(
                +(monthPay + ((
                        sp_distance_list.getSelectedItemPosition() > 0) ?
                        Double.parseDouble(et_distance_amount.getText().toString().trim().isEmpty() ? "0"
                                : et_distance_amount.getText().toString().trim())
                        : 0
                )) + "");
        tv_fee_payment_total_session.setText(sessionPay + "");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (progressbar.isShowing())
            progressbar.dismiss();
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());


        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));


        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDetach() {

        super.onDetach();
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());

    }
}

