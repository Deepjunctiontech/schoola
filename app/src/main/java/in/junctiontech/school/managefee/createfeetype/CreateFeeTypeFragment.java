package in.junctiontech.school.managefee.createfeetype;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 2/3/2017.
 */

public class CreateFeeTypeFragment extends Fragment implements SearchView.OnQueryTextListener{
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private LinearLayout snackbar;
    private RadioButton rb_monthly, rb_annual;
    private EditText et_fee_type_name;
    private RecyclerView rv_fee_type_list;
    private Button btn_add_fee_type;
    private ProgressDialog progressbar;
    private boolean logoutAlert = false;
    private ArrayList<FeeType> feeTypeList = new ArrayList<>();
    private FeeListAdapter adapter;
    private boolean checkingKey = false;
    private boolean isFeeNameAlreadyExist = false;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private RelativeLayout  page_demo;
    private int  appColor = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.layout_create_fee_type, container, false);
        snackbar = (LinearLayout) view.findViewById(R.id.ll_snackbar_create_fee_type);
//        rb_monthly = (RadioButton) view.findViewById(R.id.rb_create_fee_frequency_monthly);
//        rb_annual = (RadioButton) view.findViewById(R.id.rb_create_fee_frequency_annual);

//        rb_monthly.setBackgroundTintList(ColorStateList.valueOf(appColor));
//        rb_annual.setBackgroundTintList(ColorStateList.valueOf(appColor));
//        ((TextView)view.findViewById(R.id.tv_create_fee_type_title)).setTextColor(appColor);
       // ((TextInputLayout)view.findViewById(R.id.til_create_fee_type_type_name)).setBackgroundTintList(ColorStateList.valueOf(appColor));
//        et_fee_type_name = (EditText) view.findViewById(R.id.et_create_fee_fee_type_name);
        et_fee_type_name.setFilters(new InputFilter[] { Config.filter, new InputFilter.LengthFilter(getResources().getInteger(R.integer.normal_text)) });
        rv_fee_type_list = (RecyclerView) view.findViewById(R.id.recycler_view_fee_type_list);
//        btn_add_fee_type= (Button) view.findViewById(R.id.btn_create_fee_add_fee_type);
        btn_add_fee_type.setBackgroundColor(appColor);
        (btn_add_fee_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_fee_type_name.getText().toString().trim().equalsIgnoreCase(""))
                {
                   et_fee_type_name.setError(getString(R.string.name_can_not_be_blank));
                }else
                    try {
                        addFeeTypeToServer(et_fee_type_name.getText().toString().trim(),false/*save or not*/);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        });

        et_fee_type_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().equalsIgnoreCase(""))
                    checkFeeTypeToServer(
                            s.toString().trim(),false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        page_demo =(RelativeLayout)view.findViewById(R.id.rl_create_fee_type_demo);
        if (sp.getBoolean("DemoCreateFeeTypePage",true)){
            page_demo.setVisibility(View.VISIBLE);
        }else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoCreateFeeTypePage",false).commit();
            }
        });
        /* ***************************************************************/
        setHasOptionsMenu(true);
        return view;
    }


    private void checkFeeTypeToServer(final String feeName, final boolean saveOrNot) {
        if (feeName.trim().contains(" ")) {
            et_fee_type_name.setError( getString(R.string.space_not_allowed));
            Snackbar snackbarObj= Snackbar.make(snackbar,
                  R.string.space_not_allowed,
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();

        } else {et_fee_type_name.setError(null);
            Map<String, String> param = new LinkedHashMap<String, String>();
            try {
                param.put("FeeType", URLEncoder.encode( feeName,"utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));


            String feeCheck = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "FeeType.php?" +
                    "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
            Log.e("feeCheck", feeCheck);

            StringRequest request = new StringRequest(Request.Method.GET,
                    feeCheck
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {

                            DbHandler.longInfo(s);
                            Log.e("feeCheck", s);
                            checkingKey = false;
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                et_fee_type_name.setError(null);
                                if (jsonObject.optInt("code") == 200) {
                                    isFeeNameAlreadyExist = true;
                                    if (progressbar != null && progressbar.isShowing() && !getActivity().isFinishing()) {
                                        progressbar.cancel();
                                    }
                                    et_fee_type_name.setError(getString(R.string.fee_name_already_exist));

                                /*if (updateClass) {
                                    *//* update class*//*
                                    if (saveOrNot)
                                        Toast.makeText(ClassSectionActivity.this,
                                                getString(R.string.class_name_already_exist), Toast.LENGTH_SHORT).show();
                                } else {
                                    *//* add class *//*
                                    fragmentCreateClass.setClassNameAlreadyRegister(true);
                                }
*/

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(getActivity()).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                else {
                                    isFeeNameAlreadyExist = false;

                               /* if (updateClass) {
                                    *//* update class*//*
                                    if (progressbar.isShowing()) {
                                        progressbar.cancel();
                                        updateClassNameToServer(updateClassObj, className, false);
                                    }

                                    if (saveOrNot)
                                        updateClassNameToServer(updateClassObj, className, true);
                                } else {*/


                                    if (progressbar.isShowing()) {
                                        progressbar.cancel();
                                        btn_add_fee_type.performClick();
                                    }
                                    if (saveOrNot)
                                        addFeeTypeToServer(feeName, true);
/*
                                }*/
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("ClassKey", volleyError.toString());
                    checkingKey = false;
                    isFeeNameAlreadyExist = false;
                    if (progressbar.isShowing())
                        progressbar.cancel();

                    Config.responseVolleyErrorHandler(getActivity(), volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            request.setTag("FEE_NAME");
            AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");
        }
    }

    public void addFeeTypeToServer(String feeName, boolean saveOrNot) throws JSONException {
        if (saveOrNot && !isDetached()) {

            progressbar.show();
            if (!checkingKey) {
                if (isFeeNameAlreadyExist || feeName.trim().contains(" ")) {
                    Snackbar snackbarObj= Snackbar.make(snackbar,
                            isFeeNameAlreadyExist ?R.string.fee_name_already_exist:R.string.space_not_allowed,
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                    snackbarObj.show();

                } else {
                    final JSONObject param = new JSONObject();
                    if (rb_monthly.isChecked())
                        param.put("Frequency", "Monthly");
                    else   param.put("Frequency", "Session");

                    param.put("FeeType", feeName.trim().toUpperCase());
                    param.put("Status", "Active");
                    param.put("CreatedBy", sp.getString("loggedUserName", "Admin"));


                    String addClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "FeeType.php?databaseName=" + dbName;
                    Log.d("addClassFeeUrl", addClassUrl);
                    Log.d("param", param.toString());

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, addClassUrl,param    ,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    DbHandler.longInfo(jsonObject.toString());
                                    Log.e("ResultClassFee", jsonObject.toString());
                                    progressbar.cancel();

                                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                            getAllFeeType();
                                            clearText();

                                            Toast.makeText(getActivity(),
                                                    jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !(getActivity()).isFinishing()) {
                                                Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                            Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                        else {
                                            alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();
                                        }



                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("ResultClass", volleyError.toString());
                            Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

                            progressbar.cancel();
                        }
                    })  ;

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request.setTag(getTag());
                    AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

                }


            }

        } else {
            checkFeeTypeToServer(feeName,true);

        }

    }

    private void clearText() {
        et_fee_type_name.setText("");
        rb_monthly.setChecked(true);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor = Config.getAppColor(getActivity(),false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (feeTypeList.size() == 0)
                        getAllFeeType();
                }
            }
        };

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            setupRecycler();
            getAllFeeType();
        }
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        rv_fee_type_list.setLayoutManager(layoutManager);
        adapter = new FeeListAdapter(getActivity(), feeTypeList,appColor);
        rv_fee_type_list.setAdapter(adapter);
    }

    private void getAllFeeType() {

        progressbar.show();

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?databaseName=" + dbName /*+
                "&action=join&session=" + sp.getString(Config.SESSION, "")*/;

        Log.e("FetchAllFeeTypeUrl", url);

        StringRequest request = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getFeeTypeData", s + "");
                        progressbar.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                FeeType obj = gson.fromJson(s, new TypeToken<FeeType>() {
                                }.getType());
                                feeTypeList = obj.getResult();
                               if (adapter!=null)
                                adapter.updateList(feeTypeList);
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getFeeTypeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

        /*}*/
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        progressbar.dismiss();
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter!=null)
                       adapter.updateList(feeTypeList);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<FeeType> filteredModelList = filter(feeTypeList, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<FeeType> filter(ArrayList<FeeType> models, String query) {
        query = query.toLowerCase();final ArrayList<FeeType> filteredModelList = new ArrayList<>();

        for (FeeType model : models) {

            if (model.getFeeType().toLowerCase().contains(query)
                    ||
                    model.getFrequency().toLowerCase().contains(query)
                    ||
                    model.getStatus().toLowerCase().contains(query)
                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDetach() {
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());
        super.onDetach();
    }
}
