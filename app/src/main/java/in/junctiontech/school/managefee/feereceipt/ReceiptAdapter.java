package in.junctiontech.school.managefee.feereceipt;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.managefee.payment.FeeReceipt;

/**
 * Created by JAYDEVI BHADE on 8/3/2017.
 */

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.MyViewHolder> {
    private ArrayList<FeeReceipt> feesArrayList;
    private Context context;
    private int colorIs;

    public ReceiptAdapter(Context context, ArrayList<FeeReceipt> feesArrayList, int colorIs) {
        this.feesArrayList = feesArrayList;
        this.context = context;
        this.colorIs = colorIs;

    }

    public void updateList(ArrayList<FeeReceipt> data) {
        this.feesArrayList = data;
        notifyDataSetChanged();
    }

    @Override
    public ReceiptAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_section_view, parent, false);
        return new ReceiptAdapter.MyViewHolder(view);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final ReceiptAdapter.MyViewHolder holder, int position) {
        final FeeReceipt feeObj = feesArrayList.get(position);

        String next = "<font color='#727272'><br>" + (context.getString(R.string.date)) + " : " + feeObj.getTransactionDate() + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(
                context.getString(R.string.ammount)+" : "+
                feeObj.getTransactionAmount().toUpperCase() +
                next));


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return feesArrayList.size();
    }

    public void removeObject(int pos) {
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;


        public MyViewHolder(final View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(colorIs);
            ( itemView.findViewById(R.id.iv_item_icon)).setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((FeeReceiptActivity)context).getFeeListOfPaid(feesArrayList.get(getLayoutPosition()));
                }
            });
            tv_item_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemView.performClick();
                }
            });
            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }


}

