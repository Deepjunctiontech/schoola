package in.junctiontech.school.managefee.payment;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 7/3/2017.
 */

public class Fees implements Serializable{
ArrayList<Fees> fees;
    ArrayList<FeeReceipt> PaidFeeReciept;
    String FeeTypeName,FeeId,FeeTypeFrequency, MonthYear, FeeAmount, AmountBalance, FeeStatus,Frequency;
          int  AmountPaid;

    Fees result;

    public Fees(String feeTypeName, String amountBalance, String monthYear, boolean neglet ) {
        this.FeeTypeName= feeTypeName;
        this.AmountBalance= amountBalance;
        this.MonthYear= monthYear;
    }

    public Fees(String feeTypeName, String amount, String distance) {
        this.FeeTypeName= feeTypeName;
        this.FeeAmount= amount;
        this.Distance= distance;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public ArrayList<Fees> getFees() {
        return fees;
    }

    public void setFees(ArrayList<Fees> fees) {
        this.fees = fees;
    }

    public ArrayList<FeeReceipt> getPaidFeeReciept() {
        return PaidFeeReciept;
    }

    public void setPaidFeeReciept(ArrayList<FeeReceipt> paidFeeReciept) {
        PaidFeeReciept = paidFeeReciept;
    }

    public String getFeeTypeName() {
        return FeeTypeName;
    }

    public void setFeeTypeName(String feeTypeName) {
        FeeTypeName = feeTypeName;
    }

    public String getFeeId() {
        return FeeId;
    }

    public void setFeeId(String feeId) {
        FeeId = feeId;
    }

    public String getFeeTypeFrequency() {
        return FeeTypeFrequency;
    }

    public void setFeeTypeFrequency(String feeTypeFrequency) {
        FeeTypeFrequency = feeTypeFrequency;
    }

    public String getMonthYear() {
        return MonthYear;
    }

    public void setMonthYear(String monthYear) {
        MonthYear = monthYear;
    }

    public String getFeeAmount() {
        return FeeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        FeeAmount = feeAmount;
    }

    public String getAmountBalance() {
        return AmountBalance;
    }

    public void setAmountBalance(String amountBalance) {
        AmountBalance = amountBalance;
    }

    public String getFeeStatus() {
        return FeeStatus;
    }

    public void setFeeStatus(String feeStatus) {
        FeeStatus = feeStatus;
    }

    public int getAmountPaid() {
        return AmountPaid;
    }

    public void setAmountPaid(int amountPaid) {
        AmountPaid = amountPaid;
    }

    public Fees getResult() {
        return result;
    }

    public void setResult(Fees result) {
        this.result = result;
    }



    String Assign, StudentFeeAmount, ActualAmount;

    public String getAssign() {
        return Assign;
    }

    public void setAssign(String assign) {
        Assign = assign;
    }

    public String getStudentFeeAmount() {
        return StudentFeeAmount;
    }

    public void setStudentFeeAmount(String studentFeeAmount) {
        StudentFeeAmount = studentFeeAmount;
    }

    public String getActualAmount() {
        return ActualAmount;
    }

    public void setActualAmount(String actualAmount) {
        ActualAmount = actualAmount;
    }

    String Distance,DistanceKM;

  ArrayList<Fees> transportfees;

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getDistanceKM() {
        return DistanceKM;
    }

    public void setDistanceKM(String distanceKM) {
        DistanceKM = distanceKM;
    }

    public ArrayList<Fees> getTransportfees() {
        return transportfees;
    }

    public void setTransportfees(ArrayList<Fees> transportfees) {
        this.transportfees = transportfees;
    }
}
