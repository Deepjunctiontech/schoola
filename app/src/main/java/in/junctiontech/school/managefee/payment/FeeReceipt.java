package in.junctiontech.school.managefee.payment;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 7/3/2017.
 */

public class FeeReceipt implements Serializable {
    String TransactionId, TransactionDate, TransactionAmount,TransactionRemarks;

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getTransactionAmount() {
        return TransactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        TransactionAmount = transactionAmount;
    }

    public String getTransactionRemarks() {
        return TransactionRemarks;
    }

    public void setTransactionRemarks(String transactionRemarks) {
        TransactionRemarks = transactionRemarks;
    }
}
