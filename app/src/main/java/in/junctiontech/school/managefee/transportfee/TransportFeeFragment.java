package in.junctiontech.school.managefee.transportfee;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.managefee.classfee.SectionFee;
import in.junctiontech.school.managefee.createfeetype.FeeListAdapter;
import in.junctiontech.school.managefee.createfeetype.FeeType;
import in.junctiontech.school.managefee.payment.Fees;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by JAYDEVI BHADE on 4/3/2017.
 */

public class TransportFeeFragment extends Fragment {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private LinearLayout snackbar;

    private EditText et_fee_type_name;
    private RecyclerView rv_fee_type_list;
    private Button btn_add_fee_type, btn_fee_type_update;
    private ProgressDialog progressbar;
    private boolean logoutAlert = false;

    private FeeListAdapter adapter;
    private boolean checkingKey = false;
    private boolean isFeeNameAlreadyExist = false;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private ArrayList<SectionFee> sectionnalFeeList = new ArrayList<>();
    private LinearLayout ll_fee_list;

    private ArrayList<MasterEntry> distanceListData = new ArrayList<>();
    private ArrayList<Fees> finalTransportFeeList = new ArrayList<>();
    private String transportFeeTypeId = "";
    private Spinner sp_distance_unit;
    private ArrayAdapter<String> unitAdapter;
    private String[] unitList;
    private  String sectionId = "";
    private int  appColor =0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.layout_create_fee_type, container, false);
        ((RadioGroup) view.findViewById(R.id.gr_fee_frequency)).setVisibility(View.GONE);
//        ((TextView) view.findViewById(R.id.tv_create_fee_type_title)).setTextColor(appColor);

        btn_fee_type_update = (Button) view.findViewById(R.id.btn_fee_type_update);
        btn_fee_type_update.setVisibility(View.VISIBLE);
        ((TextInputLayout) view.findViewById(R.id.til_create_fee_type_type_name)).setHint(getString(R.string.enter_new_distance));
//        et_fee_type_name = (EditText) view.findViewById(R.id.et_create_fee_fee_type_name);

        et_fee_type_name.setInputType(InputType.TYPE_CLASS_NUMBER);
//        sp_distance_unit = (Spinner) view.findViewById(R.id.sp_create_fee_type_distance_unit);
        sp_distance_unit.setVisibility(View.VISIBLE);

//        btn_add_fee_type = (Button) view.findViewById(R.id.btn_create_fee_add_fee_type);

        btn_add_fee_type.setBackgroundColor(appColor);rv_fee_type_list = (RecyclerView) view.findViewById(R.id.recycler_view_fee_type_list);
        snackbar = (LinearLayout) view.findViewById(R.id.ll_snackbar_create_fee_type);

        ll_fee_list = (LinearLayout) view.findViewById(R.id.ll_fee_type_fee_list);
        (btn_add_fee_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (!et_fee_type_name.getText().toString().trim().equalsIgnoreCase(""))
                   try {
                       addFeeTypeToServer(et_fee_type_name.getText().toString().trim(), false/*save or not*/);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               else et_fee_type_name.setError(getString(R.string.name_can_not_be_blank));
            }
        });
        et_fee_type_name.setHint("");
        et_fee_type_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().equalsIgnoreCase(""))
                    checkFeeTypeToServer(
                            s.toString().trim(), false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_fee_type_update.setBackgroundColor(appColor);
        btn_fee_type_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classListData.size()==0 || sectionId.equalsIgnoreCase("")){
                    showSnack();
                }else if (transportFeeTypeId.equalsIgnoreCase(""))
                    getTransportIdFromServer(true/* for saving*/);
                else try {
                        updateTransportFee();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        });
        return view;
    }

    private void updateTransportFee() throws JSONException {
        progressbar.show();
        final JSONArray jsonArraySection = new JSONArray();
        final JSONArray jsonArraySectionIdList = new JSONArray();

        for (final Fees distanceFeeObj : finalTransportFeeList) {

            Map<String, Object> paramSection = new LinkedHashMap<String, Object>();

            if (distanceFeeObj.getFeeAmount().equalsIgnoreCase("")
                    ||
                    Double.parseDouble(distanceFeeObj.getFeeAmount()) == 0) {
                paramSection.put("Amount", "0");
            } else paramSection.put("Amount", distanceFeeObj.getFeeAmount());

            paramSection.put("FeeType", transportFeeTypeId);
            paramSection.put("Distance", distanceFeeObj.getDistance());
            jsonArraySection.put(new JSONObject(paramSection));


        }


        final JSONObject param = new JSONObject();
        param.put("Session", sp.getString(Config.SESSION, ""));
        param.put("SectionId", jsonArraySectionIdList);
        param.put("Fees", jsonArraySection);

        if (jsonArraySection.length() > 0) {

            String addFeeUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +
                    "BulkClassFee.php?databaseName=" + dbName;
            Log.e("addClassFeeUrl", addFeeUrl);
            Log.e("param", param.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,    addFeeUrl,param
                       ,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("FEE", jsonObject.toString());
                            progressbar.cancel();

                                if (jsonObject.optInt("code") == 200) {

                                    Toast.makeText(getActivity(), getString(R.string.fee_inserted_successfully), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(getActivity()).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else {
                                  /*  alert.setMessage(jsonObject.optString("message"));


                                    if (!getActivity().isFinishing())
                                        alert.show();*/
                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            jsonObject.optString("message"),
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("FEE", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                }
            })  ;

            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag(getTag());
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            Snackbar.make(snackbar,
                    getString(R.string.fees_not_available) + " !",
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        }


    }


    private void checkFeeTypeToServer(final String feeName, final boolean saveOrNot) {
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryValue", feeName + unitList[sp_distance_unit.getSelectedItemPosition()]);
        param.put("MasterEntryName", "Distance");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("feeCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("feeCheck", s);
                        checkingKey = false;
                        if (progressbar.isShowing())
                            progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            et_fee_type_name.setError(null);
                            if (jsonObject.optInt("code") == 200) {
                                isFeeNameAlreadyExist = true;
                                if (progressbar.isShowing()) {
                                    progressbar.cancel();
                                }
                                et_fee_type_name.setError(getString(R.string.distance_already_exist));

                                /*if (updateClass) {
                                    *//* update class*//*
                                    if (saveOrNot)
                                        Toast.makeText(ClassSectionActivity.this,
                                                getString(R.string.class_name_already_exist), Toast.LENGTH_SHORT).show();
                                } else {
                                    *//* add class *//*
                                    fragmentCreateClass.setClassNameAlreadyRegister(true);
                                }
*/

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                isFeeNameAlreadyExist = false;

                               /* if (updateClass) {
                                    *//* update class*//*
                                    if (progressbar.isShowing()) {
                                        progressbar.cancel();
                                        updateClassNameToServer(updateClassObj, className, false);
                                    }

                                    if (saveOrNot)
                                        updateClassNameToServer(updateClassObj, className, true);
                                } else {*/


                                if (saveOrNot)
                                    addFeeTypeToServer(feeName, true);


/*
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ClassKey", volleyError.toString());
                if (progressbar.isShowing())
                    progressbar.cancel();
                checkingKey = false;
                isFeeNameAlreadyExist = false;
                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");

    }

    public void addFeeTypeToServer(String feeName, boolean saveOrNot) throws JSONException {
        progressbar.show();
        if (saveOrNot) {

            progressbar.show();
            if (!checkingKey) {
                if (isFeeNameAlreadyExist) {
                    alert.setMessage(getString(R.string.distance_already_exist));
                    alert.show();

                } else {
                    final JSONObject param = new JSONObject();

                    param.put("MasterEntryValue", feeName.trim().toUpperCase() + unitList[sp_distance_unit.getSelectedItemPosition()]);
                    param.put("MasterEntryStatus", "Active");
                    param.put("MasterEntryName", "Distance");
                    param.put("Grade", "");
                    param.put("GradeRange", "");


                    String addClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "MasterEntryApi.php?databaseName=" + dbName;

                    Log.d("addDistanceFeeUrl", addClassUrl);
                    Log.d("param", param.toString());

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, addClassUrl,param
 ,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    DbHandler.longInfo(jsonObject.toString());
                                    Log.e("ResultDistanceFee", jsonObject.toString());
                                    progressbar.cancel();

                                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                            getDistanceListFromServer();
                                            clearText();

                                            Toast.makeText(getActivity(),
                                                    jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !(getActivity()).isFinishing()) {
                                                Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                            Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                        else {
                                           /* alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();*/
                                            Snackbar snackbarObj= Snackbar.make(snackbar,
                                                    jsonObject.optString("message"),
                                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                }
                                            });
                                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                            snackbarObj.show();

                                        }




                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("ResultDistance", volleyError.toString());
                            Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                            progressbar.cancel();
                        }
                    })  ;

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request.setTag(getTag());
                    AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

                }


            }

        } else {
            checkFeeTypeToServer(feeName, true);

        }

    }

    private void clearText() {
        et_fee_type_name.setText("");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor = Config.getAppColor(getActivity(),false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        getDistanceListFromServer();
        getTransportIdFromServer(false);
        getClassListFromServer();
        getDistanceListFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");

                    getDistanceListFromServer();

                    if (transportFeeTypeId.equalsIgnoreCase(""))
                        getTransportIdFromServer(false);

                    if (classListData.size() == 0)
                        getClassListFromServer();

                    if (sectionnalFeeList.size() == 0)
                        getAllFeeTypeSectionWise();
                }
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            getTransportIdFromServer(false);
            getDistanceListFromServer();
            getClassListFromServer();

            unitList = getActivity().getResources().getStringArray(R.array.distance_list);
            unitAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, unitList);
            unitAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_distance_unit.setAdapter(unitAdapter);
        }
    }

    private void getAllFeeTypeSectionWise() {

        if (classListData.size() > 0) {

            for (int cls = 0; cls < classListData.size(); cls++) {
                CreateClass obj = classListData.get(cls);
                ArrayList<CreateClass> secList = obj.getSectionList();
                if (secList.size() > 0) {
                    sectionId = secList.get(0).getSectionId();
                    break;
                }

            }


            progressbar.show();
            sectionnalFeeList = new ArrayList<>();
            final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
            param_main.put("SectionId", sectionId);

            final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
            param.put("filter", new JSONObject(param_main));

            String classWiseFetFee = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "ClassFee.php?databaseName=" +
                    dbName +
                    "&data=" + new JSONObject(param);

            Log.d("classWiseFetFee", classWiseFetFee);

            StringRequest request = new StringRequest(Request.Method.GET,
                    classWiseFetFee

                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getFeeData", s);

                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                    SectionFee obj = gson.fromJson(s, new TypeToken<SectionFee>() {
                                    }.getType());
                                    sectionnalFeeList = obj.getResult();


                                }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(getActivity()).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else {
                               /* for (MasterEntry obj : feeTypeList)
                                    obj.setAmount("");*/


                                   /* if (!getActivity().isFinishing()) {
                                        alert.setMessage(getString(R.string.fees_not_available));
                                        alert.show();

                                    }*/
                                    Snackbar snackbarObj= Snackbar.make(snackbar,
                                            getString(R.string.fees_not_available),
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();

                                }


                                setFeeList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            progressbar.dismiss();
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("FeeData", volleyError.toString());
                    progressbar.cancel();
                    setFeeList();
                    Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag(getTag());
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
        }else {
          showSnack();
        }

    }

    private void  showSnack() {
        Snackbar snackbarObj = Snackbar.make(
                snackbar,

                getString(R.string.classes_not_available),
                Snackbar.LENGTH_LONG).setAction( getString(R.string.create_class),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                            Toast.makeText(getActivity(),"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                        }else { startActivity(new Intent(getActivity(), ClassSectionActivity.class));
                        getActivity().finish();
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                    }
                });
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));

        snackbarObj.show();
    }

    private void getClassListFromServer() {

        progressbar.show();

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");

        Log.d("fetchClassList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
                                }.getType());
                                classListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            getAllFeeTypeSectionWise();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

        //}
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        progressbar.dismiss();


    }

    private void setFeeList() {
        if (ll_fee_list != null) {
            ll_fee_list.removeAllViews();

            finalTransportFeeList = new ArrayList<Fees>();
            for (MasterEntry distanceObj : distanceListData) {

                boolean isAvailableInList = false;
                for (SectionFee sectionFeeObj : sectionnalFeeList) {
                    if (sectionFeeObj.getDistance() != null && !sectionFeeObj.getDistance().equalsIgnoreCase("")
                            && distanceObj.getMasterEntryId().equalsIgnoreCase(sectionFeeObj.getDistance())
                            ) {
                        finalTransportFeeList.add(new Fees(distanceObj.getMasterEntryValue(),
                                sectionFeeObj.getAmount(),
                                sectionFeeObj.getDistance()));
                        isAvailableInList = true;
                        break;
                    }
                }

                if (!isAvailableInList) {
                    finalTransportFeeList.add(new Fees(distanceObj.getMasterEntryValue(),
                            "0",
                            distanceObj.getMasterEntryId()));

                }
            }

            refreshList();
        }
    }

    public void refreshList(){
        if (ll_fee_list != null) {
            ll_fee_list.removeAllViews();

            for (final Fees distanceFeeObj : finalTransportFeeList) {
                if (distanceFeeObj.getDistance() != null && !distanceFeeObj.getDistance().equalsIgnoreCase("")) {
                    final View view = getActivity().getLayoutInflater().inflate(R.layout.item_add_manage_fee, null);
                    TextView tv_item_fee_name = (TextView) view.findViewById(R.id.tv_item_fee_name);
                    tv_item_fee_name.setTextColor(appColor);
                    tv_item_fee_name.setText(distanceFeeObj.getFeeTypeName());
                    tv_item_fee_name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder alertUpdate = new AlertDialog.Builder(getActivity(),
                                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

                            View view_update = getActivity().getLayoutInflater().inflate(R.layout.item_update, null);
                            TextInputLayout text_input_name = (TextInputLayout) view_update.findViewById(R.id.text_input_name);
                            text_input_name.setHint(getString(R.string.enter_new_distance));
                            final EditText et_name = (EditText) view_update.findViewById(R.id.et_name);

                            et_name.setInputType(InputType.TYPE_CLASS_NUMBER);


                            final Spinner spinner_distance_unit = (Spinner) view_update.findViewById(R.id.spinner_distance_unit);
                            spinner_distance_unit.setVisibility(View.VISIBLE);
                            int pos = 0;
                            et_name.setText(distanceFeeObj.getFeeTypeName());
                            for (String unitName : unitList) {

                                if (distanceFeeObj.getFeeTypeName().contains(unitName)) {
                                    spinner_distance_unit.setSelection(pos);
                                    et_name.setText(distanceFeeObj.getFeeTypeName().replace(unitName, ""));
                                    break;
                                }
                                pos++;
                            }


                            alertUpdate.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (et_name.getText().toString().trim().length() == 0) {
                                        Toast.makeText(getActivity(), getString(R.string.name_can_not_be_blank), Toast.LENGTH_SHORT).show();
                                    } else {
                                        checkDistanceNameToServer(distanceFeeObj, et_name.getText().toString()
                                                + unitList[spinner_distance_unit.getSelectedItemPosition()]);
                                    }

                                }
                            });
                            alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_maps_directions_car));
                            alertUpdate.setTitle(getString(R.string.update)+" "+getString(R.string.distance));
                            alertUpdate.setView(view_update);
                            alertUpdate.setCancelable(false);
                            alertUpdate.show();
                        }
                    });


                    EditText et_item_amount = (EditText) view.findViewById(R.id.et_item_amount);
                    et_item_amount.setText(distanceFeeObj.getFeeAmount());
                    et_item_amount.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            distanceFeeObj.setFeeAmount(s.toString().trim());
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    ll_fee_list.addView(view);
                }
            }
        }
    }

    private void updateDistanceNameToServer(final Fees distanceFeeObj, final String distanceName) {
        progressbar.show();

        final Map<String, String> param = new LinkedHashMap<String, String>();

        param.put("MasterEntryValue", distanceName);

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("MasterEntryId", distanceFeeObj.getDistance());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String updateClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("MasterEntryApi", updateClassUrl);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateClassUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResultMaster", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                int objPos = finalTransportFeeList.indexOf(distanceFeeObj);
                                distanceFeeObj.setFeeTypeName(distanceName);
                                finalTransportFeeList.set(objPos, distanceFeeObj);
                                refreshList();

                                Toast.makeText(getActivity(),
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                               /* alert.setMessage(jsonObject.optString("message"));
                                alert.show();*/

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }

    private void checkDistanceNameToServer(final Fees distanceFeeObj, final String distanceName) {

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryName", "Distance");
        param.put("MasterEntryValue", distanceName);
        param.put("MasterEntryStatus", "Active");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        String classCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("DistanceKey", classCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                classCheck
                //192.168.1.151/apicpanel/RegistrationApi
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                //  http://apicpanel.zeroerp.com/RegistrationApi.php?
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("DistanceKey", s);
                        checkingKey = false;
                        if (progressbar.isShowing()) {
                            progressbar.cancel();
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                               Snackbar snackbarObj= Snackbar.make(snackbar,
                                        R.string.distance_already_exist,
                                        Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                updateDistanceNameToServer(distanceFeeObj, distanceName);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("DistanceKey", volleyError.toString());


                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag(Config.CHECK_CLASS_NAME);
        // AppRequestQueueController.getInstance(this).cancelRequestByTag(Config.CHECKMAILID);
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(Config.CHECK_CLASS_NAME);
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, Config.CHECK_CLASS_NAME);


    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());

    }

    public void getDistanceListFromServer() {
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("MasterEntryName", "Distance");

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param);

        Log.d("fetchDistanceList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getDistanceData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                distanceListData = new ArrayList<>();
                                distanceListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                           // setFeeList();

                            getAllFeeTypeSectionWise();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getDistanceData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }

    public void getTransportIdFromServer(final boolean saveOrNot) {
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("FeeType", "TRANSPORT");

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param);

        Log.d("fetchTransportId", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getTransportId", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                FeeType obj = gson.fromJson(s, new TypeToken<FeeType>() {
                                }.getType());
                                if (obj.getResult().size() > 0)
                                    transportFeeTypeId = obj.getResult().get(0).getFeeTypeID();

                                if (!transportFeeTypeId.equalsIgnoreCase("") && saveOrNot)
                                    btn_fee_type_update.performClick();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getTransportId", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }
}
