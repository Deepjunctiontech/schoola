package in.junctiontech.school.managefee.feereceipt;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.managefee.payment.FeeConfirmationActivity;
import in.junctiontech.school.managefee.payment.FeeReceipt;
import in.junctiontech.school.managefee.payment.Fees;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.RegisteredStudent;
import in.junctiontech.school.models.SchoolSession;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;

/**
 * Created by JAYDEVI BHADE on 8/3/2017.
 */

public class FeeReceiptActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private LinearLayout ll_student_fee_list, ll_account_details;
    private RelativeLayout snackbar;
    private RecyclerView recycler_fee_list;
    private ArrayList<FeeReceipt> studentFeeList = new ArrayList<>();
    private ReceiptAdapter adapter;
    private Spinner sp_class, sp_student, sp_session_list;
    private ArrayList<RegisteredStudent> registeredStudentList = new ArrayList<>();
    private ArrayList<RegisteredStudent> studentRegisteredObjList = new ArrayList<>();
    private boolean logoutAlert;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>(),
            sectionIdList = new ArrayList<>(),
            classIdList = new ArrayList<>();
    private ArrayAdapter<String> classAdapter, adapterSession;
    private ArrayList<String> studentAdmissionNameList = new ArrayList<>(),
            studentAdmissionIdList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private Calendar cal;
    private ArrayList<PaidFeeReceipt> paidTransactionFeeDetailList;
    private ArrayList<FeeReceipt> feesTransactionList = new ArrayList<>();
    private RegisteredStudent registerStudent;
    private boolean selectedStudentFeeShown = true;
    private int colorIs = 0;
    private boolean clearTop;
    private ArrayList<String> sessionList = new ArrayList<>();

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_spinner_fee_receipt_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_spinner_fee_receipt_student)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_fee_receipt_session_title)).setTextColor(colorIs);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_receipt_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sp_class = (Spinner) findViewById(R.id.spinner_fee_receipt_class);
        sp_student = (Spinner) findViewById(R.id.spinner_fee_receipt_student);
        sp_session_list = (Spinner) findViewById(R.id.sp_fee_receipt_session_list);

        sp = Prefs.with(this).getSharedPreferences();
        cal = Calendar.getInstance();
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setColorApp();
        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        /***********************************  Session spinner setup  **************************************************/
        if (!sp.getString(Config.SESSION_DATA, "").equalsIgnoreCase("")) {

            final SchoolSession schoolSessionObj = gson.fromJson(sp.getString(Config.SESSION_DATA, ""),
                    new TypeToken<SchoolSession>() {
                    }.getType());

            sessionList = new ArrayList<>();
            ArrayList<SchoolSession> schoolSessionArrayList = schoolSessionObj.getSessionList();
            int sessionPos = 0, i = 0;
            for (SchoolSession schoolSession : schoolSessionArrayList) {
                if (sp.getString(Config.SESSION, "").equalsIgnoreCase(schoolSession.getSession()))
                    sessionPos = i;
                sessionList.add(schoolSession.getSession());
                i++;
            }
                            /* ----------- Spinner Fill Session List--------------*/
            adapterSession = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, sessionList);
            sp_session_list.setAdapter(adapterSession);
            if (sessionList.size()>= sessionPos) sp_session_list.setSelection(sessionPos);
        }


/*************************************************************************************/



        snackbar = (RelativeLayout) findViewById(R.id.ll_fee_receipt);
        //  if (classListData.size() == 0)
        clearTop = getIntent().getBooleanExtra("clearTop", false);

        if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student")

                ) {
              /* for parent and student*/
            ((LinearLayout) findViewById(R.id.ll_fee_receipt_select_class_student)).setVisibility(View.GONE);

            if (!sp.getString("StudentData", "").equalsIgnoreCase("")) {
                RegisteredStudent registerStudentList = gson.fromJson(sp.getString("StudentData", ""),
                        new TypeToken<RegisteredStudent>() {
                        }.getType());
                registerStudent = registerStudentList.getResult().get(0);
                getStudentFeeStructure();
            }
        } else {
             /* for admin */
            getClassListFromServer();
            getRegisteredStudentListFromServer();
        }

        //  if (registeredStudentList.size() == 0)

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");

                    if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                            ||
                            sp.getString("user_type", "").equalsIgnoreCase("Student")

                            ) {
                        getStudentFeeStructure();
                    } else {
                        if (classListData.size() == 0)
                            getClassListFromServer();

                        if (registeredStudentList.size() == 0)
                            getRegisteredStudentListFromServer();
                    }
                }
            }
        };


        recycler_fee_list = (RecyclerView) findViewById(R.id.rv_fee_receipt_list);
        setupRecycler();
        sp_session_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getStudentFeeStructure();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getStudentFeeStructure();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, sectionIdList.get(position))
                        .putString(Config.LAST_USED_CLASS_ID, classIdList.get(position))
                        .commit();
                resetStudentListSectionWise(sectionIdList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getStudentFeeStructure() {
        boolean requestSend = true;
        Map<String, String> param = new LinkedHashMap<String, String>();
        if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student")

                ) {
            param.put("admissionId", sp.getString("loggedUserID", "not found"));

        } else {

            if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0) {
                requestSend = true;
                param.put("admissionId", studentAdmissionIdList.get(sp_student.getSelectedItemPosition()));
            } else requestSend = false;
        }


        if (requestSend) {

            progressbar.show();

            if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

                    ) {
                param.put("admissionId", sp.getString("loggedUserID", "not found"));

            } else {
                param.put("admissionId", studentAdmissionIdList.get(sp_student.getSelectedItemPosition()));
            }

            param.put("session", sp_session_list.getSelectedItemPosition()!=-1?sp_session_list.getSelectedItem().toString():sp.getString(Config.SESSION, ""));
            param.put("monthYear", cal.get(Calendar.DAY_OF_MONTH) + "-" +
                    (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR));

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));

            String studentFetUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +
                    "FeesPayment.php?databaseName=" +
                    dbName + "&data=" + new JSONObject(param1);

            Log.e("studentFeeUrl", studentFetUrl);

            StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                    studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getStudentData", s);
                            progressbar.cancel();
                            feesTransactionList = new ArrayList<>();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                    Fees obj = gson.fromJson(s, new TypeToken<Fees>() {
                                    }.getType());
                                    Fees feesObj = obj.getResult();
                                    //  feesList = feesObj.getFees();
                                    feesTransactionList = feesObj.getPaidFeeReciept();


                                }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & ! isFinishing()) {
                                        Config.responseVolleyHandlerAlert(FeeReceiptActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else  Config.responseSnackBarHandler( jsonObject.optString("message"),snackbar);

                                showFeeReceiptList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getStudentData", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(FeeReceiptActivity.this,volleyError,snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag("getFeePayment");
            AppRequestQueueController.getInstance(FeeReceiptActivity.this).cancelRequestByTag("getFeePayment");
            AppRequestQueueController.getInstance(FeeReceiptActivity.this).addToRequestQueue(request, "getFeePayment");
        } else {
            showSnack();
        }


    }

    private void showFeeReceiptList() {
        adapter.updateList(feesTransactionList);
        showSnack();

    }

    private void resetStudentListSectionWise(String sectionId) {
        studentAdmissionIdList = new ArrayList<>();
        studentAdmissionNameList = new ArrayList<>();
        studentRegisteredObjList = new ArrayList<>();
        int pos = 0;
        int i = 0;
        String studentAdmissionId = getIntent().getStringExtra("studentAdmissionId");

        Log.e("studentAdmissionId", studentAdmissionId + " " + clearTop + " " + sectionId);

        for (RegisteredStudent registeredStudentObj : registeredStudentList) {
            if (registeredStudentObj.getSectionId().equalsIgnoreCase(sectionId)) {

                studentRegisteredObjList.add(registeredStudentObj);
                studentAdmissionNameList.add(registeredStudentObj.getStudentName() + " (" +
                        registeredStudentObj.getAdmissionNo() + ")");
                studentAdmissionIdList.add(registeredStudentObj.getAdmissionId());
                Log.e("MAtch",pos+" ritu"+" "+studentAdmissionId+" "+registeredStudentObj.getAdmissionId());
                if (clearTop) {
                    if (studentAdmissionId.equalsIgnoreCase(registeredStudentObj.getAdmissionId())) {
                        pos = i;
                        Log.e("MAtch",pos+" ritu"+" "+studentAdmissionId+" "+registeredStudentObj.getAdmissionId()+" matched");

                    }
                }
                i++;
            }
        }


        studentAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, studentAdmissionNameList);
        studentAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_student.setAdapter(studentAdapter);

        if (studentAdmissionNameList.size() == 0) {
            feesTransactionList = new ArrayList<>();
            showFeeReceiptList();
        }

        if (clearTop && selectedStudentFeeShown && studentAdmissionNameList.size() > 0) {
            sp_student.setSelection(pos);
         //   selectedStudentFeeShown = false;
        }
    }

    private void getClassListFromServer() {

        progressbar.show();

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");

        Log.d("fetchClassList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
                                }.getType());
                                classListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(FeeReceiptActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            setUpClassSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(FeeReceiptActivity.this,volleyError,snackbar);            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(FeeReceiptActivity.this).addToRequestQueue(request);

        //}
    }

    private void setUpClassSpinner() {
          /* class spinner */
        classSectionNameList = new ArrayList<>();
        sectionIdList = new ArrayList<>();

        int lastUsedSectionIdPos = 0;
        String sectionIdStudent = "";
        if (clearTop) sectionIdStudent = getIntent().getStringExtra("studentSectionId");

        int clsPos = 0, i = 0;
        for (CreateClass clsObj : classListData) {

            for (CreateClass secObj : clsObj.getSectionList()) {
                classIdList.add(clsObj.getClassId());
                classSectionNameList.add(clsObj.getClassName() + " " + secObj.getSectionName());
                sectionIdList.add(secObj.getSectionId());


                if (clearTop && sectionIdStudent.equalsIgnoreCase(secObj.getSectionId())) {
                    clsPos = i;
                }

                if (sp.getString(Config.LAST_USED_SECTION_ID, "").
                        equalsIgnoreCase(secObj.getSectionId())) {
                    lastUsedSectionIdPos = i;

                }
                i++;
            }

        }


        classAdapter = new ArrayAdapter<String>(FeeReceiptActivity.this,
                android.R.layout.simple_list_item_1, classSectionNameList);
        classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_class.setAdapter(classAdapter);
        if (classSectionNameList.size() > 0) {
            sp_class.setSelection(clsPos);
            if (!clearTop) {
                sp_class.setSelection(lastUsedSectionIdPos);

            } else if (sectionIdList.size() >= sp_class.getSelectedItemPosition())
                resetStudentListSectionWise(sectionIdList.get(sp_class.getSelectedItemPosition()));
        } else showSnack();
    }

    public void getRegisteredStudentListFromServer() {

        progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Session", sp.getString(Config.SESSION, ""));


        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "StudentRegistrationApi.php?databaseName=" +
                dbName + "&action=studentParentGet&data=" + new JSONObject(param);

        Log.e("studentFetUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStudentData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                RegisteredStudent obj = gson.fromJson(s, new TypeToken<RegisteredStudent>() {
                                }.getType());
                                registeredStudentList = obj.getResult();

                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(FeeReceiptActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            if (sectionIdList.size() > 0 && !isFinishing())
                                resetStudentListSectionWise(sectionIdList.get(sp_class.getSelectedItemPosition()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStudentData", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(FeeReceiptActivity.this,volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(FeeReceiptActivity.this).addToRequestQueue(request);
        // }
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_fee_list.setLayoutManager(layoutManager);
        adapter = new ReceiptAdapter(this, studentFeeList, colorIs);
        recycler_fee_list.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(FeeReceiptActivity.this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(FeeReceiptActivity.this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void openFeeReceipt(String transactionId, String date, ArrayList<PaidFeeReceipt> paidTransactionFeeDetailList) {

        ArrayList<Fees> feesList = new ArrayList<>();
        for (PaidFeeReceipt paidFeeReceiptObj : paidTransactionFeeDetailList) {
            feesList.add(new Fees(paidFeeReceiptObj.FeeName,
                    paidFeeReceiptObj.AmountPaid,
                    paidFeeReceiptObj.getMonthYear(), true));
        }
        if (feesList.size() > 0) {

            if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

                    ) {
                registerStudent.setAdmissionId(sp.getString("loggedUserID", "not found"));
                startActivity(new Intent(this, FeeConfirmationActivity.class)
                        .putExtra("isNotPaid", false)
                        .putExtra("feeList", feesList)
                        .putExtra("date", date/*cal.get(Calendar.DAY_OF_MONTH) + "/" +
                                cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR)*/)
                        .putExtra("transactionId", transactionId)
                        .putExtra("registerStudentObj", registerStudent));
            } else {
                startActivity(new Intent(this, FeeConfirmationActivity.class)
                        .putExtra("isNotPaid", false)
                        .putExtra("feeList", feesList)
                        .putExtra("date", date/*cal.get(Calendar.DAY_OF_MONTH) + "/" +
                                cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.YEAR)*/)
                        .putExtra("transactionId", transactionId)
                        .putExtra("registerStudentObj",
                                studentRegisteredObjList.get(sp_student.getSelectedItemPosition())
                                /*registeredStudentList.get(sp_student.getSelectedItemPosition())*/
                        ));

            }

            overridePendingTransition(R.anim.enter, R.anim.nothing);
        } else {
            Snackbar.make(snackbar,
                    getString(R.string.no_fee_available),
                    Snackbar.LENGTH_LONG).setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        }


    }

    private ArrayList<RegisteredStudent> filter(ArrayList<RegisteredStudent> models, String query) {
        query = query.toLowerCase();
        final ArrayList<RegisteredStudent> filteredModelList = new ArrayList<>();

        for (RegisteredStudent model : models) {

            if (model.getAdmissionId().toLowerCase().contains(query)
                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void getFeeListOfPaid(final FeeReceipt feeReceiptObj) {
        progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("TransactionId", feeReceiptObj.getTransactionId());

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "FeesPayment.php?databaseName=" +
                dbName + "&action=reciept" + "&data=" + new JSONObject(param1);

        Log.e("studentPaidFeeUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStudentPaidData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {

                                PaidFeeReceipt obj = gson.fromJson(s, new TypeToken<PaidFeeReceipt>() {
                                }.getType());
                                PaidFeeReceipt feesObj = obj.getResult();
                                paidTransactionFeeDetailList = feesObj.getFeePaidDetails();

                                openFeeReceipt(feeReceiptObj.getTransactionId(), feeReceiptObj.getTransactionDate(), paidTransactionFeeDetailList);
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(FeeReceiptActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                paidTransactionFeeDetailList = new ArrayList<>();

                                Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                }).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStudentData", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(FeeReceiptActivity.this,volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag("getFeePayment");
        AppRequestQueueController.getInstance(FeeReceiptActivity.this).cancelRequestByTag("getFeePayment");
        AppRequestQueueController.getInstance(FeeReceiptActivity.this).addToRequestQueue(request, "getFeePayment");
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (clearTop) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (clearTop) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    private void showSnack() {
        if (classSectionNameList.size() == 0 && !(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student"))) {

            Snackbar snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.classes_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                Toast.makeText(FeeReceiptActivity.this,"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                            }else {
                            startActivity(new Intent(FeeReceiptActivity.this, ClassSectionActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();

        } else if (studentAdmissionIdList.size() == 0 && !(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student"))) {

            Snackbar snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.students_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_student),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_STUDENT_PERMISSION, false)) {
                                Toast.makeText(FeeReceiptActivity.this, "Permission not available : CREATE STUDENT !", Toast.LENGTH_SHORT).show();
                            } else {
//                                startActivity(new Intent(FeeReceiptActivity.this, CreateStudentActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        } else if (feesTransactionList.size() == 0) {
            Snackbar snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.fees_not_available),
                    Snackbar.LENGTH_LONG).setAction("",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
            snackbarObj.setDuration(2000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }

    }

}
