package in.junctiontech.school.managefee.feeaccount;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.managefee.payment.FeeAccount;

/**
 * Created by JAYDEVI BHADE on 03-04-2017.
 */

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.MyViewHolder> {
    private final FeeAccountFragment fragment;

    private ArrayList<FeeAccount> dataList;
    private Context context;
    private int appColor;

    public AccountListAdapter(Context context, ArrayList<FeeAccount> dataList,
                              FeeAccountFragment fragment, int appColor) {
        this.dataList = dataList;
        this.appColor = appColor;

        this.context = context;
        this.fragment = fragment;
    }


    public void updateList(ArrayList<FeeAccount> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public AccountListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final AccountListAdapter.MyViewHolder holder, final int position) {
        final FeeAccount feeAccountObj = dataList.get(position);

        String next = "<font color='#727272'>" + "( " +
                feeAccountObj.getAccountTypeName() + " )" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(feeAccountObj.getAccountName().toUpperCase() +
                " " + next));

        holder.tv_designation.setText(context.getString(R.string.opening_balance) + " : " + feeAccountObj.getOpeningBalance() + "\n"
                +
                context.getString(R.string.account_balance) + " : " +
                (
                        ((long)Double.parseDouble(feeAccountObj.getAccountBalance()) +((long)Double.parseDouble(feeAccountObj.getOpeningBalance()))
                        )
        ));
     //   Log.e("RRRRRR",  " : " +(Double.parseDouble(feeAccountObj.getAccountBalance()) +(Double.parseDouble(feeAccountObj.getOpeningBalance()))));
        /*Log.e("RRRRRR",   " : " + ((long)Double.parseDouble(feeAccountObj.getAccountBalance()))  );
        Log.e("RRRRRR",   " : " + ((long)Double.parseDouble(feeAccountObj.getOpeningBalance())));

        Log.e("RRRRRR",   " : " +  feeAccountObj.getAccountBalance() );
        Log.e("RRRRRR",   " : " +  feeAccountObj.getOpeningBalance() );*/


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name, tv_designation;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(appColor);

            tv_designation = (TextView) itemView.findViewById(R.id.tv_designation);
            tv_designation.setAllCaps(true);
            tv_designation.setVisibility(View.VISIBLE);


            // itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.updateAccount(dataList.get(getLayoutPosition()));
                }
            });
        }
    }

    public void setFilter(ArrayList<FeeAccount> gradeList) {
        this.dataList = new ArrayList<>();
        this.dataList.addAll(gradeList);
        notifyDataSetChanged();
    }

}


