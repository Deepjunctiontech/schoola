package in.junctiontech.school.managefee;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import in.junctiontech.school.ChangeAppColorFragment;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.exam.createexamtype.CreateExamTypeFragment;
import in.junctiontech.school.exam.examlist.FilledExamListFragment;
import in.junctiontech.school.exam.graderange.GradeRangeFragment;
import in.junctiontech.school.exam.graderange.MaxMarkGradeMappingFragment;
import in.junctiontech.school.exam.grades.CreateGradesFragment;
import in.junctiontech.school.managefee.classfee.ClassFeeFragment;
import in.junctiontech.school.managefee.createfeetype.CreateFeeTypeFragment;
import in.junctiontech.school.managefee.feeaccount.FeeAccountFragment;
import in.junctiontech.school.managefee.feereceipt.FeeReceiptActivity;
import in.junctiontech.school.managefee.payment.FeePaymentFragment;
import in.junctiontech.school.managefee.reminder.FeeReminderFragment;
import in.junctiontech.school.managefee.studentfee.StudentFeeFragment;
import in.junctiontech.school.managefee.transportfee.TransportFeeFragment;
import in.junctiontech.school.menuActions.aboutus.WebViewFragment;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.tranport.fragments.StopListFragment;
import in.junctiontech.school.tranport.fragments.VehicleListFragment;

public class AnotherActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private int colorIs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sp = Prefs.with(this).getSharedPreferences();

       /* if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // If the screen is now in landscape mode, we can show the
            // dialog in-line with the list so we don't need this activity.
            finish();
            return;
        }*/
        setColorApp();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (getIntent().getStringExtra("id")) {
            case "createFeeType":
                getSupportActionBar().setTitle(getString(R.string.create_fee_type));
                CreateFeeTypeFragment createFeeTypeFragment = new CreateFeeTypeFragment();
                ft.replace(R.id.selectedChoice, createFeeTypeFragment, "createFeeType");
                ft.commit();


                break;

            case "addClassFee":
                getSupportActionBar().setTitle(getString(R.string.class_fee));
                ClassFeeFragment classFeeFragment = new ClassFeeFragment();

                ft.replace(R.id.selectedChoice, classFeeFragment, "addClassFee");
                ft.commit();

                break;

            case "addTransportFee":
                getSupportActionBar().setTitle(getString(R.string.transport_fee));
                TransportFeeFragment transportFeeFragment = new TransportFeeFragment();
                ft.replace(R.id.selectedChoice, transportFeeFragment, "addTransportFee");
                ft.commit();


                break;

            case "updateStudentFee":
                getSupportActionBar().setTitle(getString(R.string.update_student_fee));
                StudentFeeFragment updateStudentFeeFragment = new StudentFeeFragment();
                ft.replace(R.id.selectedChoice, updateStudentFeeFragment, "updateStudentFee");
                ft.commit();
                break;

            case "paymentFee":
                if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                        ||
                        sp.getString("user_type", "").equalsIgnoreCase("Student"))
                    getSupportActionBar().setTitle(getString(R.string.due_fee));
                else getSupportActionBar().setTitle(getString(R.string.fee_payment));
                FeePaymentFragment paymentFragment = new FeePaymentFragment();
                ft.replace(R.id.selectedChoice, paymentFragment, "paymentFee");
                ft.commit();

                break;

            case "feeReminder":
                getSupportActionBar().setTitle(getString(R.string.reminder));
                FeeReminderFragment feeReminderFragment = new FeeReminderFragment();
                ft.replace(R.id.selectedChoice, feeReminderFragment, "feeReminder");
                ft.commit();
                break;


            /**** For student Promotion and transfer class  *****/

            case "transferClass":
                getSupportActionBar().setTitle(getString(R.string.change_class));
//                ChangeClassFragment changeClassFragment = new ChangeClassFragment();
//                ft.replace(R.id.selectedChoice, changeClassFragment, "transferClass");
//                ft.commit();

                break;
            case "promoteStudent":
                getSupportActionBar().setTitle(getString(R.string.promotion_to_next_class));
//                PromotionFragment promotionFragment = new PromotionFragment();
//                ft.replace(R.id.selectedChoice, promotionFragment, "promoteStudent");
                ft.commit();

                break;

            case "feeAccount":
                getSupportActionBar().setTitle(getString(R.string.fee_account));
                FeeAccountFragment feeAccountFragment = new FeeAccountFragment();
                ft.replace(R.id.selectedChoice, feeAccountFragment, "feeAccount");
                ft.commit();

                break;

            case "createGrades":
                getSupportActionBar().setTitle(getString(R.string.create_scholastic_grades));
                CreateGradesFragment createGradesFragment = new CreateGradesFragment();
                ft.replace(R.id.selectedChoice, createGradesFragment, "createGrades");
                ft.commit();

                break;
            case "GradesRange":
                getSupportActionBar().setTitle(getString(R.string.scholastic_grade_marks_setup));
                GradeRangeFragment gradeRangeFragment = new GradeRangeFragment();
                ft.replace(R.id.selectedChoice, gradeRangeFragment, "GradeRange");
                ft.commit();
                break;

            case "GradeResultMapping":
                getSupportActionBar().setTitle(getString(R.string.grade_range_for));
                MaxMarkGradeMappingFragment markGradeMappingFragment = new MaxMarkGradeMappingFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isNewEntry", getIntent().getBooleanExtra("isNewEntry", false));
                bundle.putSerializable("dataList", getIntent().getSerializableExtra("dataList"));
                markGradeMappingFragment.setArguments(bundle);
                ft.replace(R.id.selectedChoice, markGradeMappingFragment, getIntent().getStringExtra("maxMarks"));
                ft.commit();

                break;

            case "examList":
                getSupportActionBar().setTitle(getString(R.string.exam_results));
                FilledExamListFragment examListFragment = new FilledExamListFragment();
                ft.replace(R.id.selectedChoice, examListFragment, "examList");
                ft.commit();

                break;
            case "examType":
                getSupportActionBar().setTitle(getString(R.string.exam_type));
                CreateExamTypeFragment createExamTypeFragment = new CreateExamTypeFragment();
                ft.replace(R.id.selectedChoice, createExamTypeFragment, "examType");
                ft.commit();

                break;


            case "aboutUs":
                getSupportActionBar().setTitle(getString(R.string.about_us));
                WebViewFragment webViewFragment = new WebViewFragment();
                ft.replace(R.id.selectedChoice, webViewFragment, getIntent().getStringExtra("url"));
                ft.commit();

                break;

            case "changeAppColor":

                getSupportActionBar().setTitle(getString(R.string.app_color));
                ChangeAppColorFragment changeAppColorFragment = new ChangeAppColorFragment();

                ft.replace(R.id.selectedChoice, changeAppColorFragment, "changeAppColor");
                ft.commit();
                break;

             case "addVehicle":
                getSupportActionBar().setTitle(getString(R.string.add_vehicle));
                 VehicleListFragment vehicleListFragment = new VehicleListFragment();
                ft.replace(R.id.selectedChoice, vehicleListFragment,"addVehicle");
                ft.commit();

                break;

             case "addStop":
                 getSupportActionBar().setTitle(getString(R.string.add_stop));
                 StopListFragment stopListFragment = new StopListFragment();
                 ft.replace(R.id.selectedChoice, stopListFragment,"addStop");
                 ft.commit();
                break;



           /* case "feeReceipt":
                getSupportActionBar().setTitle(getString(R.string.fee_reminder));
                FeeReceiptFragment feeReceiptFragment = new FeeReceiptFragment();
                ft.replace(R.id.selectedChoice, feeReceiptFragment,"feeReceipt");
                ft.commit();

                break;*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (getIntent().getStringExtra("id").equalsIgnoreCase("paymentFee")
                &&
                (getIntent().getStringExtra("id").equalsIgnoreCase("paymentFee")
                        && (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                        ||
                        sp.getString("user_type", "").equalsIgnoreCase("Student"))

                )) {
            getMenuInflater().inflate(R.menu.menu_fee_receipt, menu);
            return true;
        } else return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_fee_receipt:
                startActivity(new Intent(this, FeeReceiptActivity.class));
                overridePendingTransition(R.anim.nothing, R.anim.enter);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getIntent().getBooleanExtra("clearTop", false)) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra("clearTop", false)) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        super.onBackPressed();
    }

    public void changeToolbarColor(int colorIs) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    private void setColorApp() {

         colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        changeToolbarColor(colorIs);


    }

    public int  getAppColor() {
        return colorIs;
    }
}
