package in.junctiontech.school.managefee.feereceipt;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 8/3/2017.
 */

public class PaidFeeReceipt implements Serializable{
    String TransactionDate, TotalAmountPaid, FeeName,AmountPaid,MonthYear;
PaidFeeReceipt result;
    ArrayList<PaidFeeReceipt> FeePaidDetails;

    public PaidFeeReceipt getResult() {
        return result;
    }

    public void setResult(PaidFeeReceipt result) {
        this.result = result;
    }

    public String getMonthYear() {
        return MonthYear;
    }

    public void setMonthYear(String monthYear) {
        MonthYear = monthYear;
    }

    public String getAmountPaid() {
        return AmountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        AmountPaid = amountPaid;
    }

    public String getFeeName() {
        return FeeName;
    }

    public void setFeeName(String feeName) {
        FeeName = feeName;
    }

    public String getTotalAmountPaid() {
        return TotalAmountPaid;
    }

    public void setTotalAmountPaid(String totalAmountPaid) {
        TotalAmountPaid = totalAmountPaid;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public ArrayList<PaidFeeReceipt> getFeePaidDetails() {
        return FeePaidDetails;
    }

    public void setFeePaidDetails(ArrayList<PaidFeeReceipt> feePaidDetails) {
        FeePaidDetails = feePaidDetails;
    }
}
