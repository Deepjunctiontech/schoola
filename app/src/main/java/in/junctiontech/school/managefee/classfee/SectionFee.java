package in.junctiontech.school.managefee.classfee;

import java.io.Serializable;
import java.util.ArrayList;

import in.junctiontech.school.managefee.createfeetype.FeeType;

/**
 * Created by JAYDEVI BHADE on 11/3/2017.
 */

public class SectionFee implements Serializable {

    String FeeId,FeeStatus, Session, SectionId, FeeTypeID, Amount, Distance,MasterEntryValue,SectionName,
            ClassName,FeeType,Frequency;


    ArrayList<SectionFee> result;

    public SectionFee(String feeTypeID, String feeType,String amount, String distance, String frequency) {
        this.FeeTypeID= feeTypeID;
        this.FeeType = feeType;
        this.Amount= amount;
        this.Distance=distance;
        this.Frequency=frequency;

    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getFeeId() {
        return FeeId;
    }

    public void setFeeId(String feeId) {
        FeeId = feeId;
    }

    public String getFeeStatus() {
        return FeeStatus;
    }

    public void setFeeStatus(String feeStatus) {
        FeeStatus = feeStatus;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getFeeTypeID() {
        return FeeTypeID;
    }

    public void setFeeTypeID(String feeTypeID) {
        FeeTypeID = feeTypeID;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getMasterEntryValue() {
        return MasterEntryValue;
    }

    public void setMasterEntryValue(String masterEntryValue) {
        MasterEntryValue = masterEntryValue;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getFeeType() {
        return FeeType;
    }

    public void setFeeType(String feeType) {
        FeeType = feeType;
    }

    public ArrayList<SectionFee> getResult() {
        return result;
    }

    public void setResult(ArrayList<SectionFee> result) {
        this.result = result;
    }
}
