package in.junctiontech.school.managefee.createfeetype;

import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 3/3/2017.
 */

public class FeeType {
    private String FeeTypeID, FeeType, Frequency, generationDate, Status, CreatedOn, CreatedBy,
            UpdatedBy,UpdatedOn;
    private ArrayList<FeeType> result;

    public String getFeeTypeID() {
        return FeeTypeID;
    }

    public void setFeeTypeID(String feeTypeID) {
        FeeTypeID = feeTypeID;
    }

    public String getFeeType() {
        return FeeType;
    }

    public void setFeeType(String feeType) {
        FeeType = feeType;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(String generationDate) {
        this.generationDate = generationDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getUpdatedOn() {
        return UpdatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        UpdatedOn = updatedOn;
    }

    public ArrayList<in.junctiontech.school.managefee.createfeetype.FeeType> getResult() {
        return result;
    }

    public void setResult(ArrayList<in.junctiontech.school.managefee.createfeetype.FeeType> result) {
        this.result = result;
    }
}
