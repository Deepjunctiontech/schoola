package in.junctiontech.school.managefee.reminder;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 2/3/2017.
 */

public class FeeReminderFragment extends Fragment {
    // private Button btn_fee_reminder_due_date;
    private EditText et_fee_reminder_message_content,et_fee_reminder_message_title;
    // private Calendar cal;
    private LinearLayout snackbar;

    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;

    private ProgressDialog progressbar;
    private boolean logoutAlert = false;
    private int appColor = 0;
    private CheckBox ck_reminder_student, ck_reminder_parent, ck_reminder_staff;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.layout_fee_reminder, container, false);
        snackbar = (LinearLayout) view.findViewById(R.id.ll_fee_reminder);

        ((TextView) view.findViewById(R.id.tv_fee_reminder_message_title)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_fee_reminder_message_content)).setTextColor(appColor);

        ck_reminder_student = (CheckBox) view.findViewById(R.id.ck_reminder_student);
        ck_reminder_parent = (CheckBox) view.findViewById(R.id.ck_reminder_parent);
        ck_reminder_staff = (CheckBox) view.findViewById(R.id.ck_reminder_staff);

        et_fee_reminder_message_title = (EditText) view.findViewById(R.id.et_fee_reminder_message_title);
        et_fee_reminder_message_content = (EditText) view.findViewById(R.id.et_fee_reminder_message_content);
        Button btn_fee_reminder = (Button) view.findViewById(R.id.btn_fee_reminder_send_notification);
        btn_fee_reminder.setBackgroundColor(appColor);
        btn_fee_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_fee_reminder_message_content.getText().toString().trim().equalsIgnoreCase("")) {
                    try {
                        sendNotification();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Snackbar.make(snackbar,
                            (getString(R.string.please_write_some_text) + " " +
                                    getString(R.string.to_send_notification)),
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                }
            }
        });
          /* btn_fee_reminder_due_date = (Button) view.findViewById(R.id.btn_fee_reminder_due_date);
     (btn_fee_reminder_due_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListener, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                Calendar calCurrent = Calendar.getInstance();

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        btn_fee_reminder_due_date.setText(cal.get(Calendar.YEAR) + "-" +
                (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH));*/

        return view;
    }

    private void sendNotification() throws JSONException {
        if (ck_reminder_student.isChecked() || ck_reminder_parent.isChecked() || ck_reminder_staff.isChecked()) {
            final JSONObject param = new JSONObject();
            param.put("Session", sp.getString(Config.SESSION, ""));
            //   param.put("DueDate", btn_fee_reminder_due_date.getText().toString());
            param.put("Message", et_fee_reminder_message_content.getText().toString().trim());
            param.put("Title", et_fee_reminder_message_title.getText().toString().trim().isEmpty()?"Reminder":et_fee_reminder_message_title.getText().toString().trim());
            JSONArray receiverType = new JSONArray();
            if (ck_reminder_student.isChecked())
                receiverType.put("student");
            if (ck_reminder_parent.isChecked())
                receiverType.put("parent");
            if (ck_reminder_staff.isChecked())
                receiverType.put("staff");

            param.put("receiverType", receiverType);
            String addFeeUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "FeeReminder.php?databaseName=" + dbName+"&action=reminder";

            Log.e("addFeeReminderUrl", addFeeUrl);
            Log.e("param", param.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, addFeeUrl, param
                    ,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("FeeReminder", jsonObject.toString());
                            progressbar.cancel();

                            if (jsonObject.optInt("code") == 200) {

                                // alert.setMessage(getString(R.string.fee_inserted_successfully));
                                Toast.makeText(getActivity(), getString(R.string.send_successfully), Toast.LENGTH_SHORT).show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else {
                                alert.setMessage(jsonObject.optString("message"));


                                if (!getActivity().isFinishing())
                                    alert.show();
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("FeeReminder", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(getActivity(), volleyError, snackbar);
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag(getTag());
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
        } else {
            Snackbar.make(snackbar,
                "Select at least one from : student, parent, staff !",
                    Snackbar.LENGTH_LONG).setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        }
    }


  /*  public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_fee_reminder_due_date.setText(year + "-" + new_month + "-" + new_day);
            // Toast.makeText(getContext(), year + "-" + new_month + "-" + new_day, Toast.LENGTH_LONG).show();
        }
    };*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //cal = Calendar.getInstance(TimeZone.getDefault());
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor = Config.getAppColor(getActivity(), false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (progressbar.isShowing())
            progressbar.dismiss();

        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {


        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());
        super.onDetach();
    }
}
