package in.junctiontech.school.managefee.classfee;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.managefee.createfeetype.FeeType;
import in.junctiontech.school.models.CreateClass;

/**
 * Created by JAYDEVI BHADE on 4/3/2017.
 */

public class ClassFeeFragment extends Fragment {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private boolean logoutAlert = false;
    private LinearLayout snackbar;
    private LinearLayout ll_fee_list;
    /*  private CheckBox ck_for_all_sections;*/
    private Spinner spinner_manage_fee_select_class, sp_create_class_fee_section;
    private ArrayAdapter<String> classAdapter;
    private ArrayList<String> classNameList = new ArrayList<>(), classIdList = new ArrayList<>();
    private ArrayList<String> sectionIdList = new ArrayList<>(), sectionNameList = new ArrayList<>();
    private ArrayList<Boolean> isSectionSelected = new ArrayList<>();
    private ArrayList<FeeType> feeTypeList = new ArrayList<>();
    /* private RecyclerView rv_class_fee_section_list;*/
    /*  private ArrayList<CreateClass> customSectionIdList = new ArrayList<>();*/
    private ArrayList<SectionFee> sectionnalFeeList = new ArrayList<>();
    private ArrayAdapter<String> sectionAdapter;
    private boolean isDialogComesFirstTime = true;
    private int appColor = 0;
    private TextView tv_manage_fee_total_session, tv_manage_fee_total_monthly;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor = Config.getAppColor(getActivity(),false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (classListData.size() == 0)
                        getClassListFromServer();

                    if (feeTypeList.size() == 0)
                        getFeeTypeFromServer();

                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.fragment_create_fee, container, false);
        snackbar = (LinearLayout) view.findViewById(R.id.activity_manage_class_fees);


        ((TextView) view.findViewById(R.id.tv_create_class_fee_class)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_create_class_fee_section)).setTextColor(appColor);

        spinner_manage_fee_select_class = (Spinner) view.findViewById(R.id.sp_create_class_fee_class);
        sp_create_class_fee_section = (Spinner) view.findViewById(R.id.sp_create_class_fee_section);

        tv_manage_fee_total_session = (TextView) view.findViewById(R.id.tv_manage_fee_total_session);
        tv_manage_fee_total_monthly = (TextView) view.findViewById(R.id.tv_manage_fee_total_monthly);

      /*  ck_for_all_sections = (CheckBox) view.findViewById(R.id.ck_for_all_sections);
        rv_class_fee_section_list = (RecyclerView) view.findViewById(R.id.rv_class_fee_section_list);  */

        /*ck_for_all_sections.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    rv_class_fee_section_list.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                            R.anim.from_left_to_in));
                    rv_class_fee_section_list.setVisibility(View.GONE);

                } else {
                    rv_class_fee_section_list.setVisibility(View.VISIBLE);
                    rv_class_fee_section_list.startAnimation(AnimationUtils.loadAnimation(getActivity(),
                            R.anim.from_right_to_left));

                }

            }
        });*/


        ll_fee_list = (LinearLayout) view.findViewById(R.id.ll_class_fee_fee_list);

        spinner_manage_fee_select_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* ck_for_all_sections.setChecked(true);*/
                /*fetchClassFeeData
                        (sectionIdList.get(position));*/
                setSectionList(classIdList.get(position));
                sp.edit().
                        putString(Config.LAST_USED_CLASS_ID, classIdList.get(position))
                        .commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_create_class_fee_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* ck_for_all_sections.setChecked(true);*/
                if (position == 0) {

                    Toast.makeText(getActivity(),
                            getString(R.string.all_sections_of_class_will_have_same_fees),
                            Toast.LENGTH_SHORT).show();

                  /*  AlertDialog.Builder alertClass = new AlertDialog.Builder(getActivity(),
                            AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alertClass.setMessage(getString(R.string.all_sections_of_class_will_have_same_fees)+" !");
                    alertClass.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });


                    alertClass.setCancelable(false);
                    alertClass.show();*/

                    fetchClassFeeData
                            (sectionIdList.get(position + 1));
                } else {
                    sp.edit().
                            putString(Config.LAST_USED_SECTION_ID, sectionIdList.get(position))
                            .commit();
                    fetchClassFeeData
                            (sectionIdList.get(position));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button btn_manage_fee_add_new_fee= (Button) view.findViewById(R.id.btn_manage_fee_add_new_fee);
        btn_manage_fee_add_new_fee.setBackgroundColor(appColor);
        btn_manage_fee_add_new_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classNameList.size() == 0) showSnack();
                else {

                    try {
                        addClassesFee();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        return view;
    }

    private void showSnack() {
        Snackbar snackbarObj = null;
        if (classNameList.size() == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,

                    getString(R.string.classes_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getActivity(), ClassSectionActivity.class));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
                        }
                    });
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        } else if (feeTypeList.size() == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,

                    getString(R.string.fee_type) + " " + getString(R.string.not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_fee_type),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_FEE_TYPE_PERMISSION, false)) {
                                Toast.makeText(getActivity(), "Permission not available : CREATE FEE TYPE!", Toast.LENGTH_SHORT).show();
                            } else {   startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "createFeeType"));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }

    }


    private void addClassesFee() throws JSONException {


        final JSONArray jsonArraySection = new JSONArray();
        final JSONArray jsonArraySectionIdList = new JSONArray();

        if (sp_create_class_fee_section.getSelectedItemPosition() == 0) {
            for (int sec = 1; sec < sectionIdList.size(); sec++) {
                jsonArraySectionIdList.put(sectionIdList.get(sec));
            }
        } else {
            jsonArraySectionIdList.put(sectionIdList.get(sp_create_class_fee_section.getSelectedItemPosition()));

        }

        for (final SectionFee sectionFeeObj : sectionnalFeeList) {
            if (sectionFeeObj.getDistance() != null && sectionFeeObj.getDistance().equalsIgnoreCase("")
                    && !sectionFeeObj.getFeeType().equalsIgnoreCase("TRANSPORT")
                    && sectionFeeObj.getAmount() != null /*&& !sectionFeeObj.getAmount().equalsIgnoreCase("")
                    &&
                    Double.parseDouble(sectionFeeObj.getAmount()) != 0*/) {

                Map<String, Object> paramSection = new LinkedHashMap<String, Object>();

                if (sectionFeeObj.getAmount().equalsIgnoreCase("")
                        ||
                        Double.parseDouble(sectionFeeObj.getAmount()) == 0) {
                    paramSection.put("Amount", "0");
                } else paramSection.put("Amount", sectionFeeObj.getAmount());

                paramSection.put("FeeType", sectionFeeObj.getFeeTypeID());

                jsonArraySection.put(new JSONObject(paramSection));


            }
        }


        final JSONObject param = new JSONObject();
        param.put("Session", sp.getString(Config.SESSION, ""));
        param.put("SectionId", jsonArraySectionIdList);
        param.put("Fees", jsonArraySection);

        if (jsonArraySection.length() > 0 && jsonArraySectionIdList.length() > 0)
            if (jsonArraySectionIdList.length() > 1) {
                AlertDialog.Builder alertClass = new AlertDialog.Builder(getActivity(),
                        AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                alertClass.setMessage(getString(R.string.you_want_to_save_fees_for_all_the_section_in_this_class) + " ?");
                alertClass.setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addClassFeeViaPost(param);
                    }
                });
                alertClass.setNegativeButton(getString(R.string.edit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertClass.setCancelable(false);
                alertClass.show();
            } else
                addClassFeeViaPost(param);
        else {
            if (jsonArraySectionIdList.length() == 0)
                Snackbar.make(snackbar,
                        getString(R.string.select_any_section) + " !",
                        Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).show();
            else Snackbar.make(snackbar,
                    getString(R.string.fees_not_available) + " !",
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        }


    }



    private void addClassFeeViaPost(final JSONObject param) {
        progressbar.show();
        String addFeeUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "BulkClassFee.php?databaseName=" + dbName;
        Log.e("addClassFeeUrl", addFeeUrl);
        Log.e("param", param.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,  addFeeUrl ,param   ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("FEE", jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optInt("code") == 200) {

                                alert.setMessage(getString(R.string.fee_inserted_successfully));
                                Toast.makeText(getActivity(), getString(R.string.fee_inserted_successfully), Toast.LENGTH_SHORT).show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {


                                if (!getActivity().isFinishing()) {
                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            jsonObject.optString("message"),
                                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                }
                            }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("FEE", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        })  ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {

            if (feeTypeList.size() == 0)
                getFeeTypeFromServer();

            if (classListData.size() == 0)
                getClassListFromServer();

        }
    }

    private void fetchClassFeeData(String sectionId) {
        progressbar.show();
        sectionnalFeeList = new ArrayList<>();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("SectionId", sectionId);

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String classWiseFetFee = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassFee.php?databaseName=" +
                dbName +
                "&data=" + new JSONObject(param);

        Log.d("classWiseFetFee", classWiseFetFee);

        StringRequest request = new StringRequest(Request.Method.GET,
                classWiseFetFee

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getFeeData", s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {

                                SectionFee obj = gson.fromJson(s, new TypeToken<SectionFee>() {
                                }.getType());
                                sectionnalFeeList = obj.getResult();


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                               /* for (MasterEntry obj : feeTypeList)
                                    obj.setAmount("");*/

                                showSnack();
                              /*  if (!getActivity().isFinishing()) {


                                    Snackbar snackbarObj= Snackbar.make(snackbar,
                                            R.string.fees_not_available,
                                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                }
*/

                            }

                            setFeeList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        progressbar.dismiss();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("FeeData", volleyError.toString());
                progressbar.cancel();
                setFeeList();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setFeeList() {
        if (ll_fee_list != null) {
            ll_fee_list.removeAllViews();

            ArrayList<SectionFee> notAvailFee = new ArrayList<>();

            for (FeeType feeTypeObj : feeTypeList) {

                boolean isAvail = false;
                for (SectionFee sectionFeeObj : sectionnalFeeList) {
                    if (feeTypeObj.getFeeTypeID().equalsIgnoreCase(sectionFeeObj.getFeeTypeID())) {
                        sectionFeeObj.setFrequency(feeTypeObj.getFrequency());
                        isAvail = true;
                        break;
                    }

                }
                if (!isAvail) {
                    notAvailFee.add(new SectionFee(feeTypeObj.getFeeTypeID(),
                            feeTypeObj.getFeeType(), "0", "",feeTypeObj.getFrequency()));
                }
            }


            sectionnalFeeList.addAll(notAvailFee);

            for (final SectionFee sectionFeeObj : sectionnalFeeList) {

                if (sectionFeeObj.getDistance() != null && sectionFeeObj.getDistance().equalsIgnoreCase("")
                        && !sectionFeeObj.getFeeType().equalsIgnoreCase("TRANSPORT")) {
                    View view = getActivity().getLayoutInflater().inflate(R.layout.item_add_manage_fee, null);
                    TextView  tv_item_fee_name=   (TextView) view.findViewById(R.id.tv_item_fee_name);
                    tv_item_fee_name.setText(sectionFeeObj.getFeeType());
                    tv_item_fee_name.setTextColor(appColor);

                    EditText et_item_amount = (EditText) view.findViewById(R.id.et_item_amount);
                    et_item_amount.setText(sectionFeeObj.getAmount());
                    et_item_amount.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            sectionFeeObj.setAmount(s.toString().trim().equalsIgnoreCase("")?"0":s.toString().trim());
                            calculateAmount();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    ll_fee_list.addView(view);
                }
            }

calculateAmount();
       /* for (final MasterEntry obj : feeTypeList) {
            if (!obj.getMasterEntryId().equalsIgnoreCase("20")) {
                View view = getActivity().getLayoutInflater().inflate(R.layout.item_add_manage_fee, null);
                ((TextView) view.findViewById(R.id.tv_item_fee_name)).setText(obj.getMasterEntryValue());
                EditText et_item_amount = (EditText) view.findViewById(R.id.et_item_amount);
                et_item_amount.setText(obj.getAmount());
                et_item_amount.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        obj.setAmount(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                ll_fee_list.addView(view);
            }


        }*/

        }
    }

    private void calculateAmount() {
        double monthPay = 0;
        double sessionPay = 0;


        for (SectionFee obj : sectionnalFeeList) {
            if (obj.getAmount() != null /*&& obj.getFeeStatus().equalsIgnoreCase("Due")*/) {
                if (obj.getDistance() != null && obj.getDistance().equalsIgnoreCase("")
                        && !obj.getFeeType().equalsIgnoreCase("TRANSPORT")
                          ) {

                    if (!obj.getAmount().equalsIgnoreCase("") ) {
                        Log.e("Frequency",obj.getFrequency()+ "   "+ obj.getAmount());
                        if ( obj.getFrequency().equalsIgnoreCase("Session"))
                            sessionPay = sessionPay + (long) Double.parseDouble(obj.getAmount());
                        else  if ( obj.getFrequency().equalsIgnoreCase("Monthly"))
                            monthPay = monthPay + (long) Double.parseDouble(obj.getAmount());
                    }



                }
            }
        }

        tv_manage_fee_total_monthly.setText(
                +monthPay + ""  );
        tv_manage_fee_total_session.setText(  sessionPay+"");
    }

    public void getFeeTypeFromServer() {

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Status", "Active");
        param.put("FeeType!", "TRANSPORT");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        String feeFetchUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param1));

        Log.d("FetchAllFeeTypeUrl", feeFetchUrl);


        StringRequest request = new StringRequest(Request.Method.GET,
                feeFetchUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntry", s);
                        progressbar.cancel();

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                FeeType obj = gson.fromJson(s, new TypeToken<FeeType>() {
                                }.getType());
                                feeTypeList = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MasterEntry", volleyError.toString());
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", (new JSONObject(param1)).toString());
                //  Log.d("loginjson", new JSONObject(param).toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }

    private void getClassListFromServer() {

        progressbar.show();

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");

        Log.d("fetchClassList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
                                }.getType());
                                classListData = obj.getResult();

                            }   else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);

                            setUpClassSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

        //}
    }

    private void setUpClassSpinner() {
        /* class spinner */
        this.classNameList = new ArrayList<>();
        this.classIdList = new ArrayList<>();
        this.isSectionSelected = new ArrayList<>();

        int lastUsedClassIdPos = 0;
        int jj = 0;

        for (int cls = 0; cls < classListData.size(); cls++) {
            CreateClass obj = classListData.get(cls);
            ArrayList<CreateClass> secList = obj.getSectionList();
            if (secList.size() > 0) {
                this.classNameList.add(obj.getClassName());
                this.classIdList.add(obj.getClassId());

                if (sp.getString(Config.LAST_USED_CLASS_ID, "").
                        equalsIgnoreCase(obj.getClassId())) {
                    lastUsedClassIdPos = jj;

                }
                jj++;
                this.isSectionSelected.add(false);
            }

        }


        classAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, this.classNameList);
       classAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_manage_fee_select_class.setAdapter(classAdapter);

        if (this.classNameList.size() > 0)
            spinner_manage_fee_select_class.setSelection(lastUsedClassIdPos);
        else showSnack();


    }

    public void setSectionList(String classID) {
        sectionIdList = new ArrayList<>();
        sectionNameList = new ArrayList<>();
        sectionIdList.add(getString(R.string.all_section));
        sectionNameList.add(getString(R.string.all_section));
        int lastUsedSectionIdPos = 0;
        int jj = 0;
        for (CreateClass obj : classListData) {
            if (obj.getClassId().equalsIgnoreCase(classID) && obj.getSectionList().size() > 0) {
                for (CreateClass secObj : obj.getSectionList()) {
                    sectionIdList.add(secObj.getSectionId());
                    sectionNameList.add(secObj.getSectionName());
              /*      customSectionIdList.add(new CreateClass(secObj.getSectionId(),
                            secObj.getSectionName(),
                            false));*/

                    if (sp.getString(Config.LAST_USED_SECTION_ID, "").
                            equalsIgnoreCase(secObj.getSectionId())) {
                        lastUsedSectionIdPos = jj;

                    }
                    jj++;
                }
                break;
            }

        }

        sectionAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, this.sectionNameList);
       sectionAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_create_class_fee_section.setAdapter(sectionAdapter);

        if (this.sectionNameList.size() > 1) {

            sp_create_class_fee_section.setSelection(lastUsedSectionIdPos + 1);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (progressbar.isShowing())
            progressbar.dismiss();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDetach() {


        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());
        super.onDetach();
    }
}
