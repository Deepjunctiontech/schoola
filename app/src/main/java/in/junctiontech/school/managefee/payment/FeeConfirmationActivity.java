package in.junctiontech.school.managefee.payment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.managefee.feereceipt.FeeReceiptActivity;
import in.junctiontech.school.models.GeneralSettingData;
import in.junctiontech.school.models.RegisteredStudent;

public class FeeConfirmationActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private TextView tv_receipt_date, tv_receipt_number, tv_receipt_name, tv_receipt_subtext,
    /*tv_receipt_balance_total,*/ tv_receipt_paying_total/*, tv_receipt_paying_amount*/;
    private Button btn_confirm_and_pay_now;
    private LinearLayout ll_fee_receipt_items;
    private CircleImageView civ_fee_receipt_recipient;
    private ArrayList<Fees> feeList = new ArrayList<>();
    private RegisteredStudent registeredStudentObj;
    private LinearLayout snackbar;
    private boolean logoutAlert;
    private FeeAccount accountObj;
    private TextView tv_school_name;
    private LinearLayout root;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_receipt);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sp = Prefs.with(this).getSharedPreferences();
        intViews();
        setColorApp();
        root = (LinearLayout) findViewById(R.id.ll_fee_receipt_for_pdf); //LinearLayout is root view of my UI(xml) file.
        //  root.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        snackbar = (LinearLayout) findViewById(R.id.activity_fee_receipt);

        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        accountObj = (FeeAccount) getIntent().getSerializableExtra("accountObj");
        tv_school_name = (TextView) findViewById(R.id.tv_school_name);
        if (getIntent().getBooleanExtra("isNotPaid", false)) {
            btn_confirm_and_pay_now.setVisibility(View.VISIBLE);
            btn_confirm_and_pay_now.setText(getString(R.string.confirm) +" & "+getString(R.string.pay_now) );
            tv_school_name.setVisibility(View.GONE);
        } else {
            tv_receipt_number = (TextView) findViewById(R.id.tv_fee_receipt_recipient_receipt_number);
            tv_receipt_number.setVisibility(View.VISIBLE);
            tv_receipt_number.setText(getString(R.string.receipt_number) + " :" +
                    getIntent().getStringExtra("transactionId"));


            GeneralSettingData oo1 = gson.fromJson(sp.getString(Config.GENERAL_SETTING, ""),
                    new TypeToken<GeneralSettingData>() {
                    }.getType());
            GeneralSettingData generalSettingDataObj = oo1.getResult().get(0);
            if (generalSettingDataObj != null && generalSettingDataObj.getSchoolName() != null)
                tv_school_name.setText(generalSettingDataObj.getSchoolName());
        }

        feeList = (ArrayList<Fees>) getIntent().getSerializableExtra("feeList");

        setUpFeeList();
        tv_receipt_date.setText(getString(R.string.date) + " : " +
                getIntent().getStringExtra("date"));

        registeredStudentObj = (RegisteredStudent) getIntent().getSerializableExtra("registerStudentObj");
        tv_receipt_name.setText(registeredStudentObj.getStudentName()/* + " S/O " + registeredStudentObj.getFatherName()*/);
        tv_receipt_subtext.setText(getString(R.string.admission_number) + " : " +
                registeredStudentObj.getAdmissionId() + "\n" +
                registeredStudentObj.getPositionName());
        //  setProfileImage();

     /*   ( (Button)findViewById(R.id.btn_print_receipt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new PdfGenerationTask().execute();

            }
        });*/

    }

    private void setColorApp() {

         int colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_confirm_and_pay_now.setBackgroundColor(colorIs);
    }


    /**
     * Background task to generate pdf from users content
     *
     *
     */
    private class PdfGenerationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {



            // repaint the user's text into the page

            // LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            //   root.setDrawingCacheEnabled(true);
            //View content = findViewById(R.id.activity_fee_receipt);

            // crate a page description


            // do final processing of the page
            PdfDocument  document  = new PdfDocument();
            int pageNumber = 2;
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(root.getWidth(),
                    root.getHeight() , pageNumber).create();

            // create a new page from the PageInfo
            PdfDocument.Page page = document.startPage(pageInfo);

            root.draw(page.getCanvas());
            document.finishPage(page);

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
            String pdfName = registeredStudentObj.getStudentName() + "_" +
                    registeredStudentObj.getAdmissionId() + "_"
                    + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

            File outputFile = /*new File("/sdcard/PDFDemo_AndroidSRC/", pdfName);*/ new File(Environment.getExternalStorageDirectory().toString() +
                    "/" + Config.FOLDER_NAME + "/",
                    pdfName);

            try {
                outputFile.createNewFile();
                OutputStream out = new FileOutputStream(outputFile);
                document.writeTo(out);
                document.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return outputFile.getPath();
        }

        @Override
        protected void onPostExecute(String filePath) {
            if (filePath != null) {
Log.e("PDF_Path",filePath);
                Toast.makeText(getApplicationContext(),
                        "Pdf saved at " + filePath, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Error in Pdf creation" + filePath, Toast.LENGTH_SHORT)
                        .show();
            }

        }

    }

    private void setProfileImage() {
  /*      Log.e("ProfileUrlNAviga", sp.getString(Config.PROFILE_URL, ""));

        if (takePermission())
            if (!sp.getString(Config.PROFILE_URL, "").equalsIgnoreCase("")) {

                if (!((Activity) this).isFinishing())
                    Glide
                            .with(AdminNavigationDrawer.this)
                            .load(sp.getString(Config.PROFILE_URL, ""))
                            .asBitmap()
                            .toBytes(Bitmap.CompressFormat.JPEG, 80)
                            .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                            .into(new SimpleTarget<byte[]>() {
                                @Override
                                public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                                    new AsyncTask<Void, Void, Void>() {
                                        @Override
                                        protected Void doInBackground(Void... params) {
                                            if (takePermission()) {

                                                File dir = targetProfileImage.getParentFile();
                                                try {
                                                    if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                                        File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME);
                                                        if (!f.exists()) {
                                                            f.mkdirs();
                                                            //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
                                                            Log.e("Directory Created", "");
                                                        }

                                                    }
                                                    BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(targetProfileImage));
                                                    s.write(resource);
                                                    s.flush();
                                                    s.close();


                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }


                                            return null;
                                        }


                                        @Override
                                        protected void onPostExecute(Void dummy) {




                                        }
                                    }.execute();

                                }
                            })
                            ;


            }*/
    }

    private void intViews() {
        tv_receipt_date = (TextView) findViewById(R.id.tv_fee_receipt_recipient_receipt_date);

        tv_receipt_number = (TextView) findViewById(R.id.tv_fee_receipt_recipient_receipt_number);
        tv_receipt_name = (TextView) findViewById(R.id.tv_fee_receipt_recipient_name);
        tv_receipt_subtext = (TextView) findViewById(R.id.tv_fee_receipt_recipient_subtext);
        // tv_receipt_balance_total = (TextView) findViewById(R.id.tv_fee_receipt_recipient_balance_total);
        tv_receipt_paying_total = (TextView) findViewById(R.id.tv_fee_receipt_recipient_paying_total);
       // tv_receipt_paying_amount = (TextView) findViewById(R.id.tv_fee_receipt_recipient_paying_amount);
        btn_confirm_and_pay_now = (Button) findViewById(R.id.btn_fee_payment_confirm_and_pay_fee);
        civ_fee_receipt_recipient = (CircleImageView) findViewById(R.id.civ_fee_receipt_recipient);

        if (getIntent().getBooleanExtra("isNotPaid", false)) {
            ((LinearLayout) findViewById(R.id.ll_fee_receipt_logo_school_name)).setVisibility(View.GONE);
            btn_confirm_and_pay_now.setVisibility(View.VISIBLE);

           // ((Button) findViewById(R.id.btn_fee_receipt_recipient_pay_now)).setVisibility(View.INVISIBLE);
        } else {
            setLogo();
           // ((Button) findViewById(R.id.btn_fee_receipt_recipient_pay_now)).setVisibility(View.INVISIBLE);
        }
        ll_fee_receipt_items = (LinearLayout) findViewById(R.id.ll_fee_receipt_items);


        btn_confirm_and_pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    payFees();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setLogo() {

        File targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                sp.getString("organization_name", "") + "_logo" + ".jpg");


        try {
            civ_fee_receipt_recipient.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(targetLocation)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void payFees() throws JSONException {
        progressbar.show();
        JSONArray jsonArrayFeeList = new JSONArray();
        for (final Fees obj : feeList) {

            if (obj.getAmountBalance() == null ||
                    obj.getAmountBalance().equalsIgnoreCase("")
                    || obj.getAmountBalance().equalsIgnoreCase("0")
                    ||
                    Long.parseLong(obj.getAmountBalance()) < 0
                    )
                ;
            else {
                final Map<String, String> paramFee = new LinkedHashMap<String, String>();
                paramFee.put("FeeId", obj.getFeeId());
                paramFee.put("MonthYear", obj.getMonthYear());
                paramFee.put("AmountBalance", obj.getFeeAmount());
                paramFee.put("AmountPay", obj.getAmountBalance());
                jsonArrayFeeList.put(new JSONObject(paramFee));
            }
        }

        final JSONObject param = new JSONObject();
        param.put("admissionId", registeredStudentObj.getAdmissionId());
        param.put("feespay", jsonArrayFeeList);
        param.put("session", sp.getString(Config.SESSION, ""));


            String insertAttStaff = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "FeesPayment.php?databaseName=" + dbName;

            Log.d("insertFee", insertAttStaff);
            Log.d("param", param.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,  insertAttStaff,param
                    ,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("insertFee",jsonObject.toString());
                            progressbar.cancel();

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    try {
                                        paymentConfirmation(jsonObject.optJSONObject("result").optString("token"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & ! isFinishing()) {
                                        Config.responseVolleyHandlerAlert(FeeConfirmationActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else {

                                    alert.setMessage(jsonObject.optString("message"));
                                    if (!isFinishing())
                                        alert.show();
                                }



                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("saveAtt", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(FeeConfirmationActivity.this,volleyError,snackbar);
                }
            })  ;

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);


    }

    private void paymentConfirmation(String token) throws JSONException {
        progressbar.show();
        final JSONObject param = new JSONObject();
        param.put("admissionId", registeredStudentObj.getAdmissionId());
        param.put("action", "confirmPayment");
        param.put("token", token);
        param.put("session", sp.getString(Config.SESSION, ""));
        param.put("account", accountObj.getAccountId());
        param.put("DOP", getIntent().getStringExtra("date"));
        param.put("remark", "");
        param.put("userName", sp.getString("loggedUserName", ""));


            String insertAttStaff = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "FeesPayment.php?databaseName=" + dbName;

            Log.d("insertFee", insertAttStaff);
            Log.d("param", param.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, insertAttStaff,param
                    ,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("insertFee", jsonObject.toString());
                            progressbar.cancel();

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    btn_confirm_and_pay_now.setEnabled(false);

                                    Toast.makeText(FeeConfirmationActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                    Snackbar.make(snackbar,
                                            jsonObject.optString("message"),
                                            Snackbar.LENGTH_LONG).setAction(R.string.fee_receipt, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    }).show();
                                    Intent intent = new Intent(FeeConfirmationActivity.this,
                                            FeeReceiptActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("studentAdmissionId", registeredStudentObj.getAdmissionId());
                                    intent.putExtra("studentSectionId", registeredStudentObj.getSectionId());
                                    intent.putExtra("clearTop", true);
                                    startActivity(intent
                                    );
                                    finish();
                                    overridePendingTransition(R.anim.nothing, R.anim.enter);


                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & ! isFinishing()) {
                                        Config.responseVolleyHandlerAlert(FeeConfirmationActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else {

                                    alert.setMessage(jsonObject.optString("message"));
                                    if (!isFinishing())
                                        alert.show();
                                }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("saveAtt", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(FeeConfirmationActivity.this,volleyError,snackbar);
                }
            })  ;

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    public void setUpFeeList() {
        double totalAmountBalance = 0;


        for (final Fees obj : feeList) {

            if (obj.getAmountBalance() == null ||
                    obj.getAmountBalance().equalsIgnoreCase("")
                    || obj.getAmountBalance().equalsIgnoreCase("0")
                    ||
                    Long.parseLong(obj.getAmountBalance()) < 0
                    )
                ;
            else {

                View view = getLayoutInflater().inflate(R.layout.item_fee_receipt, null);

                ((TextView) view.findViewById(R.id.tv_item_fee_number))
                        .setText(obj.getMonthYear());
                ((TextView) view.findViewById(R.id.tv_item_fee_name))
                        .setText(obj.getFeeTypeName());
              /*  ((TextView) view.findViewById(R.id.tv_item_fee_amount))
                        .setText(obj.getFeeAmount());*/
                ((TextView) view.findViewById(R.id.tv_item_fee_total_amount))
                        .setText(obj.getAmountBalance());


                totalAmountBalance = totalAmountBalance +((long) Double.parseDouble(obj.getAmountBalance()));

                ll_fee_receipt_items.addView(view);

            }
        }
        // tv_receipt_balance_total.setText(getString(R.string.total) + "\n" + totalAmountBalance);
        tv_receipt_paying_total.setText(getString(R.string.total) + " : " + totalAmountBalance + "");
        //tv_receipt_paying_amount.setText(getString(R.string.total) + " : " + totalAmountBalance + "");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_fee_receipt, menu);
        if (getIntent().getBooleanExtra("isNotPaid", false)) {
            getMenuInflater().inflate(R.menu.menu_fee_receipt, menu);
            return true;
        } else {
            getMenuInflater().inflate(R.menu.menu_save, menu);
            return true;
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_fee_receipt:
                startActivity(new Intent(this, FeeReceiptActivity.class));
                overridePendingTransition(R.anim.nothing, R.anim.enter);
                return true;

            case R.id.action_save:
                if (takePermission())
                    new PdfGenerationTask(  ).execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean takePermission() {

        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);

            }
            return false;
        } else return true;

    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {
        //   boolean res = grantResults[0] == PackageManager.PERMISSION_GRANTED;

        if (permsRequestCode == Config.IMAGE_LOGO_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //denied
                    Log.e("denied", permissions[0]);
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        Log.e("allowed", permissions[0]);
                        setLogo();

                    } else {
                        //set to never ask again
                        Log.e("set to never ask again", permissions[0]);
                        //do something here.
                        showRationale(getString(R.string.permission_denied), getString(
                                R.string.permission_denied_external_storage));
                    }
                }
            }

        }


    }

    private void showRationale(String permission, String permission_denied_location) {
        AlertDialog.Builder alertLocationInfo = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, Config.LOCATION);
            }
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }


    @Override
    protected void onResume() {
        setLogo();

        super.onResume();
    }
}
