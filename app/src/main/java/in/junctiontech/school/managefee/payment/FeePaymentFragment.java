package in.junctiontech.school.managefee.payment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.RegisteredStudent;

/**
 * Created by JAYDEVI BHADE on 4/3/2017.
 */

public class FeePaymentFragment extends Fragment {
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private AlertDialog.Builder alert;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private LinearLayout ll_student_fee_list, ll_account_details;
    private LinearLayout snackbar;
    private Spinner sp_fee_payment_class, sp_fee_payment_student,
            sp_fee_payment_account_list;
    private ArrayList<RegisteredStudent> registeredStudentList = new ArrayList<>();
    private boolean logoutAlert;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>(),
            sectionIdList = new ArrayList<>(),
            classIdList = new ArrayList<>();
    private ArrayAdapter<String> classAdapter;
    private ArrayList<String> studentAdmissionNameList = new ArrayList<>(),
            studentAdmissionIdList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private Calendar cal;
    private Button btn_fee_payment_month_year, btn_fee_payment_pay_fee;
    private Object feeSrtucture;
    private ArrayList<Fees> feesList = new ArrayList<>();
    private ArrayList<FeeReceipt> paidFeeReceiptList = new ArrayList<>();
    private ArrayList<FeeAccount> accountListData = new ArrayList<>();
    private ArrayList<String> accountHolderNameList = new ArrayList<>();
    private ArrayAdapter<String> accountAdapter;
    private boolean inputNumberIsGreaterToMaxNumber = false;
    private TextView tv_paid_amount,tv_fee_payment_total_amount;
    private double totalAmount = 0;
    private double payingAmount = 0;
    private RegisteredStudent registerStudent;
    private TextView tv_fee_payment_no_dues;
    private ArrayList<RegisteredStudent> studentAdmissionRegisterObjList = new ArrayList<>();
    private int appColor = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_fee_payment, container, false);


        ((TextView) view.findViewById(R.id.tv_sp_fee_payment_student)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_sp_fee_payment_class)).setTextColor(appColor);
        ((TextView) view.findViewById(R.id.tv_fee_payment_spinner_name)).setTextColor(appColor);

        btn_fee_payment_pay_fee = (Button) view.findViewById(R.id.btn_fee_payment_pay_fee);
        btn_fee_payment_pay_fee.setBackgroundColor(appColor);
        if ((sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
           sp.getString("user_type", "").equalsIgnoreCase("Student")

        )) {
            ( view.findViewById(R.id.ll_fee_payment_account_selection)).setVisibility(View.GONE);
            ( view.findViewById(R.id.ll_fee_payment_total_amount)).setVisibility(View.GONE);
            btn_fee_payment_pay_fee.setVisibility(View.GONE);
            ( view.findViewById(R.id.ll_fee_payment_fragment_select_class_student)).setVisibility(View.GONE);
            TextView tv_fee_payment_name_due_or_paid = (TextView) view.findViewById(R.id.tv_fee_payment_name_due_or_paid);
            tv_fee_payment_name_due_or_paid.setText(getString(R.string.due_amount));
            tv_fee_payment_name_due_or_paid.setTextColor(appColor);
        }else {
            boolean permission = getActivity().getIntent().getBooleanExtra("isPermissionForPayment", false);
            ( view.findViewById(R.id.ll_fee_payment_account_selection)).setVisibility(permission ? View.VISIBLE : View.GONE);
            btn_fee_payment_pay_fee.setVisibility(permission ? View.VISIBLE : View.GONE);
            ( view.findViewById(R.id.ll_fee_payment_total_amount)).setVisibility(View.VISIBLE);
        }

        tv_fee_payment_no_dues = (TextView) view.findViewById(R.id.tv_fee_payment_no_dues);
        ll_account_details = (LinearLayout) view.findViewById(R.id.ll_account_details);
        ll_student_fee_list = (LinearLayout) view.findViewById(R.id.ll_fee_payment_student_fee_list);
        snackbar = (LinearLayout) view.findViewById(R.id.ll_fee_payment);

        tv_fee_payment_total_amount = (TextView) view.findViewById(R.id.tv_fee_payment_total_amount);
        tv_paid_amount = (TextView) view.findViewById(R.id.tv_fee_payment_paid_amount);
        sp_fee_payment_account_list = (Spinner) view.findViewById(R.id.sp_fee_payment_account_list);
        sp_fee_payment_class = (Spinner) view.findViewById(R.id.sp_fee_payment_class);
        sp_fee_payment_student = (Spinner) view.findViewById(R.id.sp_fee_payment_student);
        sp_fee_payment_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getFeeSrtucture();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_fee_payment_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, sectionIdList.get(position))
                        .putString(Config.LAST_USED_CLASS_ID, classIdList.get(position))
                        .commit();
                resetStudentListSectionWise(sectionIdList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_fee_payment_pay_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputNumberIsGreaterToMaxNumber) {
                    Snackbar.make(snackbar,
                            getString(R.string.should_not_be_greater_than_balance_ammount),
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                } else if (accountListData.size() == 0 || sp_fee_payment_account_list.getSelectedItemPosition() == 0) {

                    sp_fee_payment_account_list.setFocusable(true);
                    ((TextView) sp_fee_payment_account_list.getSelectedView()).setError(
                            getString(R.string.select_account)
                    );
                    if (accountListData.size() > 0) {
                        Snackbar.make(snackbar,
                                getString(R.string.select_account),
                                Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
                    } else showSnack();
                } else {

                    payFee();
                }
            }
        });

        btn_fee_payment_month_year = (Button) view.findViewById(R.id.btn_fee_payment_month_year);
        (btn_fee_payment_month_year).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListener, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));


                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        setHasOptionsMenu(true);
        return view;
    }

    private void payFee() {
        Log.e("payingAmount", payingAmount + "  ritu");
        if (payingAmount == 0) {
            //  tv_fee_payment_no_dues.setVisibility(View.VISIBLE);
            Snackbar.make(snackbar,
                    getString(R.string.fees_not_available) + " " + getString(R.string.to_pay) + " !",
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }).show();
        } else {


            if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

                    ) {
                if (registerStudent != null) {
                    registerStudent.setAdmissionId(sp.getString("loggedUserID", "not found"));
                    startActivity(new Intent(getActivity(), FeeConfirmationActivity.class)
                            .putExtra("isNotPaid", true)
                            .putExtra("feeList", feesList)
                            .putExtra("date", (new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH)).format((Calendar.getInstance(Locale.ENGLISH)).getTime()) )
                                    /*cal.get(Calendar.DAY_OF_MONTH) + "/" +
                                    ( cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.YEAR)*/
                            .putExtra("accountObj",
                                    accountListData.get(sp_fee_payment_account_list.getSelectedItemPosition() - 1))
                            .putExtra("registerStudentObj", registerStudent));

                }

            } else {

Log.e("Position",sp_fee_payment_student.getSelectedItemPosition()+" ritu");
                startActivity(new Intent(getActivity(), FeeConfirmationActivity.class)
                        .putExtra("isNotPaid", true)
                        .putExtra("feeList", feesList).putExtra("date",
                                (new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH)).format((Calendar.getInstance(Locale.ENGLISH)).getTime())
                                /*cal.get(Calendar.DAY_OF_MONTH) + "/" +
                                ( cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.YEAR)*/)
                        .putExtra("accountObj",
                                accountListData.get(sp_fee_payment_account_list.getSelectedItemPosition() - 1))
                        .putExtra("registerStudentObj",
                                studentAdmissionRegisterObjList.get(sp_fee_payment_student.getSelectedItemPosition())));

            }

            getActivity().overridePendingTransition(R.anim.nothing, R.anim.enter);
        }

    }

    private void resetStudentListSectionWise(String sectionId) {
        studentAdmissionIdList = new ArrayList<>();
        studentAdmissionNameList = new ArrayList<>();
        studentAdmissionRegisterObjList = new ArrayList<RegisteredStudent>();

        Log.e("ritu", sectionId + " ritu");

        for (RegisteredStudent registeredStudentObj : registeredStudentList) {
            if (registeredStudentObj.getSectionId().equalsIgnoreCase(sectionId)) {
                studentAdmissionNameList.add(registeredStudentObj.getStudentName() + " (" +
                        registeredStudentObj.getAdmissionNo() + ")");
                studentAdmissionIdList.add(registeredStudentObj.getAdmissionId());

                studentAdmissionRegisterObjList.add(registeredStudentObj);

                Log.e("ritu", registeredStudentObj.getStudentName() + " (" +
                        registeredStudentObj.getAdmissionNo() + ")" +registeredStudentObj.getAdmissionId() + " ritu");

            }
        }

        if (studentAdmissionIdList.size() == 0) {
            getFeeSrtucture();
        }

        studentAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, studentAdmissionNameList);
       studentAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_fee_payment_student.setAdapter(studentAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        cal = Calendar.getInstance();
        appColor = Config.getAppColor(getActivity(),false);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert.setCancelable(false);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");

                    if (!(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                            ||
                            sp.getString("user_type", "").equalsIgnoreCase("Student")

                    )) {
                        if (classListData.size() == 0)
                            getClassListFromServer();

                        if (registeredStudentList.size() == 0)
                            getRegisteredStudentListFromServer();


                    }
                    if (accountListData.size() == 0)
                        getAccountListFromServer();

                }
            }
        };

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            if (!(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

            )) {

                if (classListData.size() == 0)
                    getClassListFromServer();
                if (registeredStudentList.size() == 0)
                    getRegisteredStudentListFromServer();


            } else {
                getFeeSrtucture();

                if (!sp.getString("StudentData", "").equalsIgnoreCase("")) {
                    RegisteredStudent registerStudentList = gson.fromJson(sp.getString("StudentData", ""),
                            new TypeToken<RegisteredStudent>() {
                            }.getType());
                    registerStudent = registerStudentList.getResult().get(0);

                }

            }


            getAccountListFromServer();
            Date d = new Date();
            d.setYear(cal.get(Calendar.YEAR));
            d.setMonth(cal.get(Calendar.MONTH));
            d.setDate(cal.get(Calendar.DAY_OF_MONTH));
            btn_fee_payment_month_year.setText(Config.monthYear.format(d) + " " + cal.get(Calendar.YEAR));

        }
    }

    public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            cal.set(year, monthOfYear, dayOfMonth);

            Date d = new Date();
            d.setYear(cal.get(Calendar.YEAR));
            d.setMonth(cal.get(Calendar.MONTH));
            d.setDate(cal.get(Calendar.DAY_OF_MONTH));

            btn_fee_payment_month_year.setText(Config.monthYear.format(d) + " " + year);

            getFeeSrtucture();
            // Toast.makeText(getContext(), year + "-" + new_month + "-" + new_day, Toast.LENGTH_LONG).show();
        }
    };


    private void getClassListFromServer() {

        progressbar.show();

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");

        Log.d("fetchClassList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
                                }.getType());
                                classListData = obj.getResult();
                                setUpClassSpinner();
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

        //}
    }

    private void setUpClassSpinner() {
        if (sp_fee_payment_class != null) {
          /* class spinner */
            classSectionNameList = new ArrayList<>();
            sectionIdList = new ArrayList<>();

            classIdList = new ArrayList<>();

            int lastUsedSectionIdPos = 0;
            int jj = 0;
            for (CreateClass clsObj : classListData) {

                for (CreateClass secObj : clsObj.getSectionList()) {
                    classIdList.add(clsObj.getClassId());
                    classSectionNameList.add(clsObj.getClassName() + " " + secObj.getSectionName());
                    sectionIdList.add(secObj.getSectionId());

                    if (sp.getString(Config.LAST_USED_SECTION_ID, "").
                            equalsIgnoreCase(secObj.getSectionId())) {
                        lastUsedSectionIdPos = jj;

                    }
                    jj++;
                }

            }


            classAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, classSectionNameList);
           classAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_fee_payment_class.setAdapter(classAdapter);


            if (classSectionNameList.size() > 0) {

                sp_fee_payment_class.setSelection(lastUsedSectionIdPos);
            }
        }
    }

    public void getRegisteredStudentListFromServer() {

        progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Session", sp.getString(Config.SESSION, ""));


        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "StudentRegistrationApi.php?databaseName=" +
                dbName + "&action=studentParentGet&data=" + new JSONObject(param);

        Log.e("studentFetUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStudentData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                RegisteredStudent obj = gson.fromJson(s, new TypeToken<RegisteredStudent>() {
                                }.getType());
                                registeredStudentList = obj.getResult();
                                if (sectionIdList.size() > 0)
                                    resetStudentListSectionWise(sectionIdList.get(sp_fee_payment_class.getSelectedItemPosition()));

                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStudentData", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
        // }
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));


        if (!(sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student")

        )) {
            if (registeredStudentList.size() == 0)
                getRegisteredStudentListFromServer();

            if (classListData.size() == 0)
                getClassListFromServer();


        } else {
            getFeeSrtucture();

            if (!sp.getString("StudentData", "").equalsIgnoreCase("")) {
                RegisteredStudent registerStudentList = gson.fromJson(sp.getString("StudentData", ""),
                        new TypeToken<RegisteredStudent>() {
                        }.getType());
                registerStudent = registerStudentList.getResult().get(0);

            }

        }

        if (accountListData.size() == 0)
            getAccountListFromServer();


        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void getFeeSrtucture() {
        feesList = new ArrayList<>();
        paidFeeReceiptList = new ArrayList<>();
        boolean requestSend = true;
        Map<String, String> param = new LinkedHashMap<String, String>();
        if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("Student")

                ) {
            param.put("admissionId", sp.getString("loggedUserID", "not found"));

        } else {


            if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0) {
                requestSend = true;
                param.put("admissionId", studentAdmissionIdList.get(sp_fee_payment_student.getSelectedItemPosition()));

            } else requestSend = false;
        }


        if (requestSend) {
            progressbar.show();

            param.put("session", sp.getString(Config.SESSION, ""));
            param.put("monthYear", cal.get(Calendar.DAY_OF_MONTH) + "-" +
                    (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR));


            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));


            String studentFetUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +
                    "FeesPayment.php?databaseName=" +
                    dbName + "&data=" + new JSONObject(param1);

            Log.e("studentFeeUrl", studentFetUrl);

            StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                    studentFetUrl
                  /*  sp.getString("HostName", "Not Found") +
                            "StudentRegistrationApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                   /* sp.getString("HostName", "Not Found") +
                            "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getStudentData", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                    Fees obj = gson.fromJson(s, new TypeToken<Fees>() {
                                    }.getType());
                                    Fees feesObj = obj.getResult();
                                    feesList = feesObj.getFees();
                                    paidFeeReceiptList = feesObj.getPaidFeeReciept();

                                    showFeeList();

                                    Log.e("feeList", feesList.size() + " ritu");
                                    Log.e("paidFeeReceiptList", paidFeeReceiptList.size() + " ritu");

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(getActivity()).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                else {
                                    paidFeeReceiptList = new ArrayList<>();
                                    showFeeList();
                                    Snackbar.make(snackbar,
                                            jsonObject.optString("message"),
                                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    }).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getStudentData", volleyError.toString());
                    progressbar.cancel();

                    Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            request.setTag("getFeePayment");
            AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("getFeePayment");
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "getFeePayment");
        } else {

            showFeeList();
            showSnack();
        }

    }

    private void showFeeList() {
        if (ll_student_fee_list != null) {
            progressbar.show();
            payingAmount = 0;
            totalAmount = 0;
            boolean requestSend = true;
            if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

                    ) {
                requestSend = true;
            } else {

                if (classSectionNameList.size() > 0 && studentAdmissionIdList.size() > 0) {
                    requestSend = true;
                } else requestSend = false;
            }


            if (requestSend) {

                boolean isDue = true;
                ll_account_details.setVisibility(View.VISIBLE);


                ll_student_fee_list.removeAllViews();

                ArrayList<String> tempMonthYear = new ArrayList<>();

                if (feesList.size() == 0) {
                    ll_account_details.setVisibility(View.GONE);
                    Snackbar.make(snackbar,
                            getString(R.string.fees_not_available),
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                } else {
                    for (final Fees objTop : feesList) {
                        if (!tempMonthYear.contains(objTop.getMonthYear())) {
                            tempMonthYear.add(objTop.getMonthYear());
                            boolean temp = true;
                            for (final Fees obj : filter(feesList, objTop.getMonthYear())) {

                                if (obj.getFeeStatus().equalsIgnoreCase("Due")) {

                                    View view = getActivity().getLayoutInflater().inflate(R.layout.item_add_manage_fee, null);
                                    if (temp) {
                                        TextView tv_item_month_name = (TextView) view.findViewById(R.id.tv_item_month_name);
                                        (tv_item_month_name).setText(
                                                (obj.getMonthYear()==null || obj.getMonthYear().equalsIgnoreCase(""))?
                                                        obj.getFeeTypeFrequency() :obj.getMonthYear()

                                        );
                                        (tv_item_month_name).setVisibility(View.VISIBLE);


                                        temp = false;
                                    }
                                    TextView tv_item_fee_name = (TextView) view.findViewById(R.id.tv_item_fee_name);
                                    tv_item_fee_name.setText(obj.getFeeTypeName());
                                    tv_item_fee_name.setTextColor(appColor);
                                    final EditText et_item_amount = (EditText) view.findViewById(R.id.et_item_amount);
                                    et_item_amount.setText(obj.getAmountBalance());
                                    String max_marks = "";
                                    if (obj.getAmountBalance().equalsIgnoreCase(""))
                                        max_marks = "0";
                                    else max_marks = obj.getAmountBalance();

                                    if (obj.getAmountBalance() != null) {
                                        if (!obj.getAmountBalance().equalsIgnoreCase("")) {
                                            totalAmount = totalAmount + (long)Double.parseDouble(obj.getAmountBalance());
                                            payingAmount = payingAmount +(long) Double.parseDouble(obj.getAmountBalance());
                                        }
                                    }
                                    if ((sp.getString("user_type", "").equalsIgnoreCase("Parent")
                                            ||
                                            sp.getString("user_type", "").equalsIgnoreCase("Student")

                                    )) {
                                        et_item_amount.setTextColor(getResources().getColor(R.color.black));
                                        et_item_amount.setEnabled(false);
                                    }

                                    final String finalMax_marks = max_marks;
                                    obj.setAmountBalance(max_marks);
                                    et_item_amount.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                                            if ((s + "").equalsIgnoreCase(""))
                                                s = "0";

                                            if ((long)Double.parseDouble(finalMax_marks) < Double.parseDouble(s + "")) {
                                                et_item_amount.setError(getString(R.string.should_not_be_greater_than_balance_ammount) + " i.e. " + finalMax_marks);
                                                inputNumberIsGreaterToMaxNumber = true;
                                            } else {

                                                obj.setAmountBalance(s.toString());
                                                inputNumberIsGreaterToMaxNumber = false;
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {
                                            calculateAmount();
                                        }
                                    });

                                    view.setPadding(5, 0, 5, 0);
                                    ll_student_fee_list.addView(view);

                                    isDue = false;

                                }
                            }
                        }
                    }
                }

                tv_fee_payment_total_amount.setText(
                        +payingAmount+""  );
                tv_paid_amount.setText(  totalAmount+"");
                if (payingAmount == 0 && isDue) {
                    tv_fee_payment_no_dues.setVisibility(View.VISIBLE);
                    Snackbar.make(snackbar,
                            getString(R.string.fees_not_available) + " " + getString(R.string.to_pay) + " !",
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                } else tv_fee_payment_no_dues.setVisibility(View.GONE);

            } else {
                ll_account_details.setVisibility(View.GONE);
                calculateAmount();
                showSnack();
            }

            progressbar.dismiss();
        }
    }

    private void showSnack() {
        if (
                sp.getString("user_type", "").equalsIgnoreCase("Parent")
                        ||
                        sp.getString("user_type", "").equalsIgnoreCase("Student")

                ) ;
        else {

            if (classSectionNameList.size() == 0) {

                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.classes_not_available),
                        Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                    Toast.makeText(getActivity(),"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                                }else {
                                    startActivity(new Intent(getActivity(), ClassSectionActivity.class));
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
                                }
                            }
                        });
                snackbarObj.setDuration(5000);
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                snackbarObj.show();

            } else if (studentAdmissionIdList.size() == 0) {

                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.students_not_available),
                        Snackbar.LENGTH_LONG).setAction(getString(R.string.create_student),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!sp.getBoolean(Config.CREATE_STUDENT_PERMISSION, false)) {
                                    Toast.makeText(getActivity(), "Permission not available : CREATE STUDENT !", Toast.LENGTH_SHORT).show();
                                } else {
//                                    startActivity(new Intent(getActivity(), CreateStudentActivity.class))

                                    ;
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                            }
                        });
                snackbarObj.setDuration(5000);
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                snackbarObj.show();
            } else if (accountListData.size() == 0) {

                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.account_list_not_available),
                        Snackbar.LENGTH_LONG).setAction(getString(R.string.fee_account),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!sp.getBoolean(Config.CREATE_FEE_ACCOUNT_PERMISSION, false)) {
                                    Toast.makeText(getActivity(), "Permission not available : CREATE FEE ACCOUNT !", Toast.LENGTH_SHORT).show();
                                } else {       startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "feeAccount"));
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                            }
                        });
                snackbarObj.setDuration(5000);
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                snackbarObj.show();
            }
        }
    }

    private void calculateAmount() {
        double pay = 0;
        for (Fees obj : feesList) {
            if (obj.getAmountBalance() != null && obj.getFeeStatus().equalsIgnoreCase("Due")) {
                if (!obj.getAmountBalance().equalsIgnoreCase("")) {
                    pay = pay + (long)Double.parseDouble(obj.getAmountBalance());
                }
            }
        }
        payingAmount = pay;
        tv_fee_payment_total_amount.setText(
                +pay + ""  );
        tv_paid_amount.setText(  totalAmount+"");
    }

    private ArrayList<Fees> filter(ArrayList<Fees> models, String query) {
        query = query.toLowerCase();
        final ArrayList<Fees> filteredModelList = new ArrayList<>();

        for (Fees model : models) {

            if (model.getMonthYear().equalsIgnoreCase(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void getAccountListFromServer() {
        progressbar.show();

        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "ManageAccount.php?databaseName=" +
                dbName;

        Log.e("ManageAccountUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,

                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getManageAccount", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                FeeAccount obj = gson.fromJson(s, new TypeToken<FeeAccount>() {
                                }.getType());
                                accountListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401")) {
                                showSnack();
                            }

                            setUpAccountSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getManageAccount", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setUpAccountSpinner() {
        if (sp_fee_payment_account_list != null) {
         /* Account spinner */
            accountHolderNameList = new ArrayList<>();
            accountHolderNameList.add(getString(R.string.select));

            if (sp.getString("user_type", "").equalsIgnoreCase("Parent")
                    ||
                    sp.getString("user_type", "").equalsIgnoreCase("Student")

                    ) {
                for (FeeAccount feeAccount : accountListData) {

                    accountHolderNameList.add(feeAccount.getAccountName());


                }

            } else {
                for (FeeAccount feeAccount : accountListData) {

                    accountHolderNameList.add(feeAccount.getAccountName() + "\n" +
                            getString(R.string.balance) + " : " +
                            (
                                    (long) Double.parseDouble(feeAccount.getAccountBalance()) +
                                            (long) Double.parseDouble(feeAccount.getOpeningBalance())
                            ));


                }
            }


            accountAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, accountHolderNameList);
           accountAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_fee_payment_account_list.setAdapter(accountAdapter);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (progressbar.isShowing())
            progressbar.dismiss();

        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {

        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag(getTag());
        super.onDetach();
    }
}
