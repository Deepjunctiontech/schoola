package in.junctiontech.school.managefee.feeaccount;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.managefee.payment.FeeAccount;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by JAYDEVI BHADE on 03-04-2017.
 */

public class FeeAccountFragment extends
        Fragment implements SearchView.OnQueryTextListener {

    private RecyclerView recycler_view_school_class_list;
    private ArrayList<FeeAccount> accountListData = new ArrayList<>();
 /*   private ArrayList<String> accountManagedByList = new ArrayList<>();*/

    private ArrayList<String> accountTypeList = new ArrayList<>();

    private RelativeLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;

    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private AccountListAdapter adapter;
    private ArrayList<MasterEntry> accountTypeListData = new ArrayList<>();
    /*  private ArrayList<MasterEntry> managedByListData = new ArrayList<>();*/
    private Calendar cal;
    private FloatingActionButton fab_fragment_add;
    private int  appColor =0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_list_and_one_button, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        snackbar = (RelativeLayout) convertView.findViewById(R.id.ll_fragment_list_and_one_button);
        fab_fragment_add = (FloatingActionButton) convertView.findViewById(R.id.fab_fragment_add);

        fab_fragment_add.setBackgroundTintList( ColorStateList.valueOf(appColor));
        fab_fragment_add
                .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewAccount();


            }
        });
        recycler_view_school_class_list = (RecyclerView) convertView.findViewById(R.id.rv_fragment_list);

        setHasOptionsMenu(true);
        return convertView;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view_school_class_list.setLayoutManager(layoutManager);
        adapter = new AccountListAdapter(getActivity(), accountListData, FeeAccountFragment.this,appColor);
        recycler_view_school_class_list.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appColor = Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        cal = Calendar.getInstance(TimeZone.getDefault());
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setupRecycler();
        getAccountListFromServer();
        // getManagedByListFromServer();
        getAccountTypeListFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");


                    if (accountListData.size() == 0)
                        getAccountListFromServer();

//                    if (managedByListData.size() == 0)
//                    getManagedByListFromServer();

                    if (accountTypeListData.size() == 0)
                        getAccountTypeListFromServer();
                }
            }
        };
    }

    private void getAccountTypeListFromServer() {
        progressbar.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryName", "AccountType");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));
        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "MasterEntryApi.php?databaseName="
                + dbName +
                "&data=" + (new JSONObject(param1));

        Log.e("MasterEntryApi", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,

                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntryApi", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                accountTypeListData = obj.getResult();

                                 /* account type spinner */
                                for (int i = 0; i < accountTypeListData.size(); i++) {
                                    accountTypeList.add(accountTypeListData.get(i).getMasterEntryValue());
                                }


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else  if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler( getString(R.string.account_type_not_available),snackbar);


                            setUpAccountSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getManageAccount", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void getAccountListFromServer() {
        progressbar.show();

        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "ManageAccount.php?databaseName=" +
                dbName;

        Log.e("ManageAccountUrl", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,

                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getManageAccount", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                FeeAccount obj = gson.fromJson(s, new TypeToken<FeeAccount>() {
                                }.getType());
                                accountListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401")) {

                                Snackbar  snackbarObj = Snackbar.make(
                                        snackbar,
                                        getString(R.string.account_list_not_available),
                                        Snackbar.LENGTH_LONG).setAction(getString(R.string.fee_account),
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                fab_fragment_add.performClick();
                                            }
                                        });

                                snackbarObj.setDuration(5000);
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }

                            setUpAccountSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getManageAccount", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setUpAccountSpinner() {
        adapter.updateList(accountListData);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter != null)
                            adapter.updateList(accountListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<FeeAccount> filteredModelList = filter(accountListData, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<FeeAccount> filter(ArrayList<FeeAccount> models, String query) {
        query = query.toLowerCase();
        final ArrayList<FeeAccount> filteredModelList = new ArrayList<>();

        for (FeeAccount model : models) {

            if (model.getAccountName().toLowerCase().contains(query)
                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    public void updateAccount(final FeeAccount feeAccount) {
        if (/*accountManagedByList.size() > 0 &&*/ accountTypeList.size() > 0) {
            progressbar.show();

            android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(getActivity());
            alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_edit));
            alertUpdate.setTitle(getString(R.string.fee_account));
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_fee_account, null);
            final EditText et_account_name = (EditText) view.findViewById(R.id.et_fee_account_account_name);
            final EditText et_opening_balance = (EditText) view.findViewById(R.id.et_fee_account_opening_balance);

           /* final Spinner sp_managed_by = (Spinner) view.findViewById(R.id.sp_fee_account_managed_by);*/
            final Spinner sp_account_type = (Spinner) view.findViewById(R.id.sp_fee_account_account_type);
            final Button btn_account_start_date = (Button) view.findViewById(R.id.btn_fee_account_account_start_date);


            et_account_name.setText(feeAccount.getAccountName());
            et_opening_balance.setText(feeAccount.getOpeningBalance());

          /*  ArrayAdapter<String> accountManagedByAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, accountManagedByList);
            sp_managed_by.setAdapter(accountManagedByAdapter);

            for (int i= 0;i<= managedByListData.size();i++){
                if (managedByListData.get(i).getMasterEntryId().equalsIgnoreCase(feeAccount.getManagedBy())){
                    sp_managed_by.setSelection(i);
                    break;
                }
            }
*/
            ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, accountTypeList);
            accountTypeAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_account_type.setAdapter(accountTypeAdapter);


            for (int i = 0; i <= accountTypeListData.size(); i++) {
                if (accountTypeListData.get(i).getMasterEntryId().equalsIgnoreCase(feeAccount.getAccountType())) {
                    sp_account_type.setSelection(i);
                    break;
                }
            }

            Date d = new Date();
            d.setYear(cal.get(Calendar.YEAR));
            d.setMonth(cal.get(Calendar.MONTH));
            d.setDate(cal.get(Calendar.DAY_OF_MONTH));
            btn_account_start_date.setText((new SimpleDateFormat("dd-M-")).format(d) + cal.get(Calendar.YEAR));
            btn_account_start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    cal.set(year, monthOfYear, dayOfMonth);

                                    Date d = new Date();
                                    d.setYear(cal.get(Calendar.YEAR));
                                    d.setMonth(cal.get(Calendar.MONTH));
                                    d.setDate(cal.get(Calendar.DAY_OF_MONTH));
                                    btn_account_start_date.setText((new SimpleDateFormat("dd-M-")).format(d) + cal.get(Calendar.YEAR));

                                }
                            }, cal.get(Calendar.YEAR),
                            cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH));

                    datePicker.setCancelable(false);
                    datePicker.setTitle(getString(R.string.select_date));
                    datePicker.show();
                }
            });


            alertUpdate.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (et_account_name.getText().toString().trim().equalsIgnoreCase("")) {
                        Snackbar.make(snackbar,
                                getString(R.string.blank_entry_not_allowed),
                                Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
                    } else {
                        checkAccountNameToServer(et_account_name.getText().toString().trim(),
                                et_opening_balance.getText().toString().trim(),
                                feeAccount.getAccountBalance(),
                                sp_account_type.getSelectedItemPosition(),
                                /*sp_managed_by.getSelectedItemPosition()*/
                                feeAccount.getManagedBy(),
                                btn_account_start_date.getText().toString(),

                                false/* false for update*/,
                                feeAccount.getAccountId());
                    }

                }
            });
            alertUpdate.setView(view);
            alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertUpdate.setCancelable(false);
            alertUpdate.show();
            progressbar.cancel();
        } else {
            Snackbar.make(snackbar,
                    getString(R.string.data_not_available),
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* if (managedByListData.size() == 0)
                        getManagedByListFromServer();*/

                    if (accountTypeListData.size() == 0)
                        getAccountTypeListFromServer();
                }
            }).show();
        }
    }

    public void addNewAccount() {
        if (/*accountManagedByList.size() > 0 &&*/ accountTypeList.size() > 0) {


            android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(getActivity());
            alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_edit));
            alertUpdate.setTitle(getString(R.string.fee_account));
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_fee_account, null);
            final EditText et_account_name = (EditText) view.findViewById(R.id.et_fee_account_account_name);
            final EditText et_opening_balance = (EditText) view.findViewById(R.id.et_fee_account_opening_balance);

         /*   final Spinner sp_managed_by = (Spinner) view.findViewById(R.id.sp_fee_account_managed_by);*/
            final Spinner sp_account_type = (Spinner) view.findViewById(R.id.sp_fee_account_account_type);
            final Button btn_account_start_date = (Button) view.findViewById(R.id.btn_fee_account_account_start_date);

          /*  ArrayAdapter<String> accountManagedByAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, accountManagedByList);
            sp_managed_by.setAdapter(accountManagedByAdapter);*/

            ArrayAdapter<String> accountTypeAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, accountTypeList);
          accountTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_account_type.setAdapter(accountTypeAdapter);

            Date d = new Date();
            d.setYear(cal.get(Calendar.YEAR));
            d.setMonth(cal.get(Calendar.MONTH));
            d.setDate(cal.get(Calendar.DAY_OF_MONTH));
            btn_account_start_date.setText((new SimpleDateFormat("dd-M-")).format(d) + cal.get(Calendar.YEAR));
            btn_account_start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    cal.set(year, monthOfYear, dayOfMonth);

                                    Date d = new Date();
                                    d.setYear(cal.get(Calendar.YEAR));
                                    d.setMonth(cal.get(Calendar.MONTH));
                                    d.setDate(cal.get(Calendar.DAY_OF_MONTH));
                                    btn_account_start_date.setText((new SimpleDateFormat("dd-M-")).format(d) + cal.get(Calendar.YEAR));

                                }
                            }, cal.get(Calendar.YEAR),
                            cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH));

                    datePicker.setCancelable(false);
                    datePicker.setTitle(getString(R.string.select_date));
                    datePicker.show();
                }
            });


            alertUpdate.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (et_account_name.getText().toString().trim().equalsIgnoreCase("")) {
                        Snackbar.make(snackbar,
                                getString(R.string.blank_entry_not_allowed),
                                Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
                    } else {
                        checkAccountNameToServer(et_account_name.getText().toString().trim(),
                                et_opening_balance.getText().toString().trim(),
                                /*et_opening_balance.getText().toString().trim()*/"0",
                                sp_account_type.getSelectedItemPosition(),
                                /*sp_managed_by.getSelectedItemPosition()*/
                                3 + ""/* 3 is User type of Accountant type */,
                                btn_account_start_date.getText().toString(),
                                true, "");
                    }

                }
            });
            alertUpdate.setView(view);
            alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertUpdate.setCancelable(false);
            alertUpdate.show();
        } else {
            Snackbar.make(snackbar,
                    getString(R.string.data_not_available),
                    Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if (managedByListData.size() == 0)
                        getManagedByListFromServer();
*/
                    if (accountTypeListData.size() == 0)
                        getAccountTypeListFromServer();
                }
            }).show();
        }
    }

    public void getDateToString(final String accountName, final String openingBalance, final String accountBalance,
                                final int accountTypeSelectedItemPosition, final String managedByAccountantId/*managedBySelectedItemPosition*/,
                                String date, final boolean saveOrUpdate, final String accountId) {
        progressbar.show();
        String convertDateUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "Conversion.php?type=dateToString&date=" +
                date;
        Log.d("convertDateUrl", convertDateUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
                convertDateUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String date) {
                        DbHandler.longInfo(date);
                        Log.e("dateStrResult", date);
                        //   progressbar.dismiss();

                        if (saveOrUpdate){
                            try {
                                addNewAccountToServer(accountName, openingBalance, accountBalance, accountTypeSelectedItemPosition,
                                        managedByAccountantId, date);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else
                            updateAccountDetailToServer(accountName, openingBalance, accountBalance, accountTypeSelectedItemPosition,
                                    managedByAccountantId, date, accountId);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }

    private void updateAccountDetailToServer(String accountName, String openingBalance,
                                             String accountBalance, int accountTypeSelectedItemPosition,
                                             String /*managedBySelectedItemPosition*/
                                                     managedByAccountantId, String date,
                                             String accountId) {

        progressbar.show();
        final Map<String, String> param = new LinkedHashMap<String, String>();
        try {
            param.put("AccountName", URLEncoder.encode( accountName.replaceAll(" ", "_") ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param.put("ManagedBy", /*managedByListData.get(managedBySelectedItemPosition).getMasterEntryId()*/
                managedByAccountantId + "");
        param.put("AccountStatus", "Active");
        param.put("AccountType", accountTypeListData.get(accountTypeSelectedItemPosition).getMasterEntryId());
        param.put("OpeningBalance", openingBalance.trim() == "" ? "0.00" : openingBalance);
        param.put("AccountBalance", accountBalance.trim() == "" ? "0.00" : accountBalance);
        param.put("AccountDate", date);

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("AccountId", accountId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        Log.e("UpdateResultClass", new JSONObject(param_main).toString());

        String updateClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ManageAccount.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("updateUrl", updateClassUrl);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateClassUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResult", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200 || jsonObject.optInt("code") == 201) {
                                Snackbar snackbarObj = Snackbar.make(
                                        snackbar,

                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("",
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                            }
                                        });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();

                                getAccountListFromServer();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler(  jsonObject.optString("message"),snackbar);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResult", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }


    private void addNewAccountToServer(String accountName, String openingBalance, String accountBalance,
                                       int accountTypeSelectedItemPosition, String /*managedBySelectedItemPosition*/
                                               managedByAccountantId,
                                       String date) throws JSONException {

        final JSONObject param = new JSONObject();


        param.put("AccountName", accountName );
        param.put("ManagedBy", /*managedByListData.get(managedBySelectedItemPosition).getMasterEntryId()*/
                managedByAccountantId + "");
        param.put("AccountStatus", "Active");
        param.put("AccountType", accountTypeListData.get(accountTypeSelectedItemPosition).getMasterEntryId());
        param.put("OpeningBalance", openingBalance.trim().equalsIgnoreCase("") || openingBalance.trim() == null ? "0.00" : openingBalance);
        param.put("AccountBalance", accountBalance/*openingBalance.trim() .equalsIgnoreCase("") || openingBalance.trim() == null ? "0.00" : openingBalance*/);
        param.put("AccountDate", date);

        String stuFeeAddUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ManageAccount.php?databaseName=" + dbName;
        Log.e("url", stuFeeAddUrl);
        Log.e("param", param.toString());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,    stuFeeAddUrl  ,param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("ManageAccount", jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optInt("code") == 200 || jsonObject.optInt("code") == 201) {
                                Snackbar snackbarObj = Snackbar.make(
                                        snackbar,

                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("",
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                            }
                                        });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();

                                getAccountListFromServer();

                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler(  jsonObject.optString("message"),snackbar);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ResultRegister", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        })  ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }


    public void checkAccountNameToServer(final String accountName, final String openingBalance, final String accountBalance,
                                         final int accountTypeSelectedItemPosition,
                                         final String managedByAccountantId/*managedBySelectedItemPosition*/,
                                         final String date, final boolean saveOrUpdate, final String accountId) {

        progressbar.show();
        Map<String, String> param = new LinkedHashMap<String, String>();

        try {
            param.put("AccountName", URLEncoder.encode(  accountName.replaceAll(" ","_") ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ManageAccount.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("markCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("markCheck", s);

                        if (progressbar.isShowing())
                            progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if ((jsonObject.optInt("code") == 200 || jsonObject.optInt("code") == 201)
                                    ) {

                                FeeAccount obj = gson.fromJson(s, new TypeToken<FeeAccount>() {
                                }.getType());
                                ArrayList<FeeAccount> result = obj.getResult();
                                if (!saveOrUpdate && result.size() > 0 && result.get(0).getAccountId().equalsIgnoreCase(accountId)) {
                                    getDateToString(accountName, openingBalance,accountBalance, accountTypeSelectedItemPosition,
                                            managedByAccountantId, date, saveOrUpdate,accountId);
                                                                 } else {

                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            getString(R.string.account_name) + " " + getString(R.string.already_exist),
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();


                                }

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                           else {
                                getDateToString(accountName, openingBalance,accountBalance, accountTypeSelectedItemPosition,
                                        managedByAccountantId, date, saveOrUpdate,accountId);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("markKey", volleyError.toString());
                if (progressbar.isShowing())
                    progressbar.cancel();
                checkingKey = false;
                isNameAlreadyExist = false;
                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


   /* public void getManagedByListFromServer() {
        progressbar.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryName", "UserType");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));
        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "MasterEntryApi.php?databaseName="
                + dbName +
                "&data=" + (new JSONObject(param1));

        Log.e("MasterEntryApi", studentFetUrl);

        StringRequest request = new StringRequest(Request.Method.GET,

                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntryApi", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                managedByListData = obj.getResult();

                                   *//* Managed by type spinner *//*
                                for (int i = 0; i < managedByListData.size(); i++) {
                                    accountManagedByList.add(managedByListData.get(i).getMasterEntryValue());
                                }


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {


                                AlertDialog.Builder alertLogout = new AlertDialog.Builder(getActivity(),
                                        AlertDialog.THEME_TRADITIONAL);
                                alertLogout.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                                alertLogout.setTitle(getString(R.string.information));
                                alertLogout.setMessage(jsonObject.optString("message") + "\n" +
                                        getString(R.string.contact_message_to_junction_tech));
                                alertLogout.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                            *//* ***** clear SharedPreference*//*
                                        Prefs.with(getActivity()).getSharedPreferences().edit().clear().apply();


                                        *//********  delete MySchool Images Folder  *******//*
                                        File fileOrDirectory = new File(Environment.getExternalStorageDirectory().toString() +
                                                "/" + Config.FOLDER_NAME);

                                        AdminNavigationDrawer.deleteRecursive(fileOrDirectory);
                                        *//*****   delete Database   ****//*
                                        DbHandler.getInstance(getActivity()).deleteDatabase();

                                        Intent intent = new Intent(getActivity(), FirstScreen.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        getActivity().finish();
                                        getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
                                    }
                                });
                                alertLogout.setCancelable(false);
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    alertLogout.show();
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("401")) {
                                Snackbar.make(snackbar,
                                        getString(R.string.account_type_not_available),
                                        Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                }).show();
                            }

                            setUpAccountSpinner();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getManageAccount", volleyError.toString());
                progressbar.cancel();

                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    Snackbar.make(snackbar,
                            R.string.internet_not_available_please_check_your_internet_connectivity,
                            Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }*/

}
