package in.junctiontech.school.managefee.createfeetype;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 3/3/2017.
 */

public class FeeListAdapter extends RecyclerView.Adapter<FeeListAdapter.MyViewHolder> {
    private final ProgressDialog progressbar;
    private ArrayList<FeeType> feeList;
    private Context context;
    private SharedPreferences sp;
    private int appColor;

    public FeeListAdapter(Context context, ArrayList<FeeType> feeList, int appColor) {
        this.feeList = feeList;
        this.context = context;
        this.appColor = appColor;

        sp = Prefs.with(context).getSharedPreferences();
        progressbar = new ProgressDialog(context);
        progressbar.setCancelable(false);
        progressbar.setMessage(context.getString(R.string.please_wait));
    }

    public void updateList(ArrayList<FeeType> feeList) {
        this.feeList = feeList;
        notifyDataSetChanged();
    }

    @Override
    public FeeListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final FeeListAdapter.MyViewHolder holder, final int position) {
        final FeeType classObj = feeList.get(position);
        String next = "<font color='#727272'>" + "( " + classObj.getFrequency() + " )" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(classObj.getFeeType().toUpperCase() +
                " " + next));
        if (classObj.getStatus() != null) {
            holder.tv_designation.setText(classObj.getStatus());
            if (classObj.getStatus().equalsIgnoreCase("Active"))
                holder.tv_designation.setTextColor(context.getResources().getColor(android.R.color.holo_green_dark));
            else
                holder.tv_designation.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));

        }


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return feeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name, tv_designation;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(appColor);
            tv_designation = (TextView) itemView.findViewById(R.id.tv_designation);
            tv_designation.setAllCaps(true);
            tv_designation.setVisibility(View.VISIBLE);
            tv_designation.setGravity(Gravity.RIGHT);

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateFeeType(feeList.get(getLayoutPosition()));
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final String[] aa = new String[]{/*context.getString(R.string.update) + " " +
                            context.getString(R.string.fee_type) + " : " +
                            feeList.get(getLayoutPosition()).getFeeType()
                            ,

                            context.getString(R.string.set) + " " + context.getString(R.string.frequency) + " " +
                                    ((feeList.get(getLayoutPosition()).getFrequency().equalsIgnoreCase("Monthly"))
                                            ? context.getString(R.string.annual) : context.getString(R.string.monthly))
                            ,*/

                            context.getString(R.string.set) + " " +
                                    ((feeList.get(getLayoutPosition()).getStatus().equalsIgnoreCase("Active"))
                                            ? context.getString(R.string.inactive) : context.getString(R.string.active))
                            ,
                            context.getString(R.string.delete) + " " +
                                    context.getString(R.string.fee_type) + " : " +
                                    feeList.get(getLayoutPosition()).getFeeType()

                    };
                    alert.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            switch (which) {
                             /*   case 0:
                                    updateFeeType(feeList.get(getLayoutPosition()));
                                    break;
                                case 1:
                                    updateFeeToServer(feeList.get(getLayoutPosition()),
                                            ((feeList.get(getLayoutPosition()).getFrequency().equalsIgnoreCase("Monthly"))
                                                    ? "Annual" : "Monthly")
                                            ,2*//*2 for fee frequency update*//*);
                                    break;*/

                                case 0:
                                    updateFeeToServer(feeList.get(getLayoutPosition()),
                                            ((feeList.get(getLayoutPosition()).getStatus().equalsIgnoreCase("Active"))
                                                    ? "Inactive" : "Active")
                                            ,3/*3 for fee status update*/);
                                    break;

                                case 1:
                                    deleteFeeTypeFromServer(feeList.get(getLayoutPosition()));
                                    break;
                            }


                        }
                    });

                    alert.show();
                    return false;
                }
            });

        }
    }

    private void deleteFeeTypeFromServer(final FeeType feeTypeObj) {
        progressbar.show();

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("FeeTypeID", feeTypeObj.getFeeTypeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        //  param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String deleteUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?databaseName=" + sp.getString("organization_name", "") +
                "&data=" + (new JSONObject(param_main));

        Log.e("deleteUrl", deleteUrl);

        StringRequest request = new StringRequest(Request.Method.DELETE,
                deleteUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("deleteResultFee", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optString("code") .equalsIgnoreCase("200")) {

                            feeList.remove(feeTypeObj);

                                updateList(feeList);
                                Toast.makeText(context,
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                            }else
                                Toast.makeText(context,
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("deleteResultFee", volleyError.toString());
                progressbar.cancel();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    Toast.makeText(context,
                            context.getString(R.string.some_error_occurred_try_again_later),
                            Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(context).addToRequestQueue(request);
    }

    private void updateFeeType(final FeeType feeTypeObj) {
        android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(context, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setIcon(context.getResources().getDrawable(R.drawable.ic_edit));
        alertUpdate.setTitle(context.getString(R.string.update) + " " +
                context.getString(R.string.fee_type));

        final EditText et_name = new EditText(context);
        et_name.setText(feeTypeObj.getFeeType());

        RadioGroup radioGroup= new RadioGroup(context);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);

        final RadioButton rbMonthly = new RadioButton(context);
        rbMonthly.setText(context.getString(R.string.monthly));

        rbMonthly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    feeTypeObj.setFrequency("Monthly");
                else   feeTypeObj.setFrequency("Session");
            }
        });

        RadioButton rbAnnual = new RadioButton(context);
        rbAnnual.setText(context.getString(R.string.session));

        radioGroup.addView(rbMonthly);
        radioGroup.addView(rbAnnual);

        if(feeTypeObj.getFrequency().equalsIgnoreCase("Monthly"))
            rbMonthly.setChecked(true);
        else rbAnnual.setChecked(true);



        radioGroup.setPadding(0,10,0,10);

        LinearLayout linearLayout= new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        linearLayout.addView(et_name);
        linearLayout.addView(radioGroup);
        linearLayout.setPadding(15,10,15,10);

        alertUpdate.setView(linearLayout);
        alertUpdate.setCancelable(false);


        alertUpdate.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_name.getText().toString().trim().length() == 0) {
                    Toast.makeText(context, context.getString(R.string.name_can_not_be_blank), Toast.LENGTH_SHORT).show();
                } else {

                    checkFeeNameToServer(feeTypeObj, et_name.getText().toString().trim());

                    updateFeeToServer(feeTypeObj
                            ,rbMonthly.isChecked()
                            ? "Monthly" : "Session",2/*2 for fee frequency update*/);
                }

            }
        });
        alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertUpdate.show();
    }

    private void checkFeeNameToServer(final FeeType feeTypeObj, final String newFeeName) {
        progressbar.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("FeeType", newFeeName);

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + sp.getString("organization_name", "");
        Log.e("feeCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("feeCheck", s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                Toast.makeText(context,
                                        context.getString(R.string.fee_name_already_exist),
                                        Toast.LENGTH_SHORT).show();

                                progressbar.cancel();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {

                                Toast.makeText(context,
                                        context.getString(R.string.some_error_occurred_try_again_later),
                                        Toast.LENGTH_SHORT).show();

                                progressbar.cancel();
                            } else {

                                updateFeeToServer(feeTypeObj, newFeeName,1/*1 for fee name update*/);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ClassKey", volleyError.toString());

                if (progressbar.isShowing())
                    progressbar.cancel();

                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    Toast.makeText(context,
                            context.getString(R.string.internet_not_available_please_check_your_internet_connectivity),
                            Toast.LENGTH_SHORT).show();


                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(context).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(context).addToRequestQueue(request, "FEE_NAME");

    }

    private void updateFeeToServer(final FeeType feeTypeObj, final String newName, final int reqNumber) {
        progressbar.show();
        final Map<String, String> param = new LinkedHashMap<String, String>();

       switch (reqNumber){
           case 1:
               try {
                   param.put("FeeType", URLEncoder.encode(  newName.toUpperCase().replaceAll(" ", "_"),"utf-8"));
               } catch (UnsupportedEncodingException e) {
                   e.printStackTrace();
               }
               param.put("Frequency", feeTypeObj.getFrequency());
               param.put("Status",feeTypeObj.getStatus());
               break;
           case 2:
               param.put("Frequency", newName);
               param.put("FeeType", feeTypeObj.getFeeType());
               break;
           case 3:
               param.put("Status", newName);
               param.put("FeeType", feeTypeObj.getFeeType());
               break;
       }



        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("FeeTypeID", feeTypeObj.getFeeTypeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        Log.e("UpdateResultFee", new JSONObject(param_main).toString());

        String updateSectionUrl=sp.getString("HostName", "Not Found")
                +sp.getString(Config.applicationVersionName, "Not Found")
                + "FeeType.php?databaseName=" + "&databaseName=" + sp.getString("organization_name", "") +
                "&data=" + (new JSONObject(param_main));

        Log.d("updateFeeUrl",updateSectionUrl);

        StringRequest request = new StringRequest(Request.Method.PUT,
                updateSectionUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResultFee", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optString("code") .equalsIgnoreCase("200")) {

                                switch (reqNumber){
                                    case 1:

                                      feeList.get(feeList.indexOf(feeTypeObj)).setFeeType(newName);

                                        break;
                                    case 2:

                                        feeList.get(feeList.indexOf(feeTypeObj)).setFrequency(newName);
                                        break;
                                    case 3:
                                        feeList.get(feeList.indexOf(feeTypeObj)).setStatus(newName);
                                        break;
                                }

                                notifyDataSetChanged();
                                Toast.makeText(context,
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                            }else
                                Toast.makeText(context,
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultFee", volleyError.toString());
                progressbar.cancel();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    Toast.makeText(context,
                           context.getString(R.string.some_error_occurred_try_again_later),
                            Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(context).addToRequestQueue(request);
    }


    public void setFilter(ArrayList<FeeType> feeList) {
        this.feeList = new ArrayList<>();
        this.feeList.addAll(feeList);
        notifyDataSetChanged();
    }


}

