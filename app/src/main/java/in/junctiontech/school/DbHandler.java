package in.junctiontech.school;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.admin.msgpermission.ChatPermission;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.exam.examlist.ExamData;
import in.junctiontech.school.exam.examlist.PreviousResultData;
import in.junctiontech.school.models.Attendance;
import in.junctiontech.school.models.ChatData;
import in.junctiontech.school.models.ChatList;
import in.junctiontech.school.models.HomeworkEvaluations;
import in.junctiontech.school.models.StudentData;

/**
 * Created by JAYDEVI BHADE  on 10/1/2015.
 */
public class DbHandler extends SQLiteOpenHelper {
    private static Context context;
    private SharedPreferences sharedPreferences;
    private static DbHandler mInstance = null;
    private static final String DATABASE_NAME = "School";
    private static final int DATABASE_VERSION = 12;
    SQLiteDatabase dbRead;
    SQLiteDatabase dbwrite;
    private Object allClassAndSection;
    private ArrayList<String> groupIdsOFPreviousChat;
    static boolean onUpgradeRun = false;

    public static DbHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbHandler(context);

        }
        DbHandler.context = context;
        return mInstance;
    }

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.sharedPreferences = Prefs.with(context).getSharedPreferences();


    }

    public static boolean checkDataBaseVersion(Context context) {
        SQLiteDatabase db = new DbHandler(context).getDbReadObject();
        Log.e("RITU", onUpgradeRun + "ritu");
        db.close();
        if (onUpgradeRun) {
            onUpgradeRun = false;
            return true;
        } else return false;
    }

//    public DbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//        this.context = context;
//        this.sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
//    }

    public SQLiteDatabase getDbReadObject() {
        if (dbRead == null || !dbRead.isOpen())
            dbRead = super.getReadableDatabase();

        return dbRead;
    }

    public SQLiteDatabase getDbWriteObject() {
        if (dbwrite == null || !dbwrite.isOpen())
            dbwrite = super.getWritableDatabase();

        return dbwrite;
    }

    public static final String EXAM_RESULT_CREATE_TABLE = "CREATE TABLE ExamResult(examName TEXT," +
            "class_id TEXT," +
            "subjectid TEXT," +
            "section_id TEXT," +
            "student_id TEXT," +
            "student_name TEXT," +
            "grade TEXT," +
            "obtained_marks TEXT," +
            "maximum_marks TEXT," +
            "result TEXT," +
            "date TEXT," +
            "sending_Status TEXT," +
            "PRIMARY KEY(examName,subjectid,student_id,date)," +
            "FOREIGN KEY (examName) REFERENCES ExamType(examName)" +
            "FOREIGN KEY (subjectid) REFERENCES Subjects(subjectid)" +
            "FOREIGN KEY (student_id) REFERENCES Student_Record(student_id)" +
            ")";
    public static final String EXAM_RESULT_CREATE_TABLE_COPY = "CREATE TABLE ExamResultCopy(examName TEXT," +
            "class_id TEXT," +
            "subjectid TEXT," +
            "section_id TEXT," +
            "student_id TEXT," +
            "student_name TEXT," +
            "grade TEXT," +
            "obtained_marks TEXT," +
            "maximum_marks TEXT," +
            "result TEXT," +
            "date TEXT," +
            "sending_Status TEXT," +
            "PRIMARY KEY(examName,subjectid,student_id,date)," +
            "FOREIGN KEY (examName) REFERENCES ExamType(examName)" +
            "FOREIGN KEY (subjectid) REFERENCES Subjects(subjectid)" +
            "FOREIGN KEY (student_id) REFERENCES Student_Record(student_id)" +
            ")";

    public static final String GROUP_RECORD_CREATE_TABLE = "CREATE TABLE GroupRecord(groupId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "groupName TEXT," +
            "groupAdmin TEXT," +
            "groupAdminType TEXT," +
            "createdAt TEXT," +
            "groupMembers TEXT," +
            "groupMembersType TEXT" +
            ")";
    public static final String GROUP_MESSAGE_CREATE_TABLE = "CREATE TABLE GroupMsg(msgId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "groupId INTEGER," +
            "userID TEXT," +
            "userType TEXT," +
            "userName TEXT," +
            "msg TEXT," +
            "createdAtMsg TEXT," +
            "status TEXT," +
            "FOREIGN KEY (groupId) REFERENCES GroupRecord(groupId)" +
            ")";
    public static final String STUDENT_ATTENDANCE_CREATE_TABLE = "CREATE TABLE StudentAttendance(student_id TEXT," +
            "section_id TEXT," +
            "subjectId TEXT," +
            "date TEXT," +
            "present_status TEXT," +
            "sending_status TEXT," +
            "PRIMARY KEY(student_id,section_id,date,subjectId)," +
            "FOREIGN KEY (student_id) REFERENCES Student_Record(student_id)" +
            "FOREIGN KEY (section_id) REFERENCES Section_Name(section_id)" +
            ")";

    /* for location map*/
    public static final String LOCATION_MAP_CREATE_TABLE = "CREATE TABLE map_data(" +
            "route_name" + " TEXT," +
            "route_url" + " TEXT," +
            "start_lat" + " TEXT," +
            "start_long" + " TEXT," +
            "destination_lat" + " TEXT," +
            "destination_long" + " TEXT," +
            "start_title" + " TEXT," +
            "start_snippet" + " TEXT," +
            "destination_title" + " TEXT," +
            "destination_snippet" + " TEXT," +
            "waypoint_address" + " TEXT," +
            "waypoint_time" + " TEXT," +
            "waypoint_lat" + " TEXT," +
            "waypoint_long" + " TEXT" +
            ")";


    /* for current user location map*/
    public static final String CURRENT_LOCATION_USER_CREATE_TABLE = "CREATE TABLE currentLocationData(" +
            "latitude" + " TEXT," +
            "longitude" + " TEXT," +
            "date" + " TEXT," +
            "time" + " TEXT" +
            ")";


    /* for Staff list who is user  */
    public static final String USER_STAFF_CREATE_TABLE = "CREATE TABLE UserStaffList(TeacherID TEXT," +
            "TeacherName TEXT," +
            "UserType TEXT," +
            "PRIMARY KEY(TeacherID)" +
            ")";
    public static final String School_Class_CREATE_TABLE = "CREATE TABLE School_Class(class_id TEXT," +
            "class_no TEXT," +
            "PRIMARY KEY(class_id)" +
            ")";
    public static final String Section_Name_CREATE_TABLE = "CREATE TABLE Section_Name(section_id TEXT," +
            "section TEXT," +
            "class_id TEXT," +
            "PRIMARY KEY(section_id,class_id)," +
            "FOREIGN KEY (class_id) REFERENCES School_Class(class_id)" +
            ")";

    public static final String Student_Record_CREATE_TABLE = "CREATE TABLE Student_Record(student_id TEXT," +
            "student_name TEXT," +
            "class_id TEXT," +
            "section_id TEXT," +
            "PRIMARY KEY(student_id)," +
            "FOREIGN KEY (section_id) REFERENCES Section_Name(section_id)" +
            "FOREIGN KEY (class_id) REFERENCES School_Class(class_id)" +
            ")";
    public static final String Homework_CREATE_TABLE = "CREATE TABLE Homework(class_id TEXT," +
            "section_id TEXT," +
            "date TEXT," +
            "subjectid TEXT," +
            "subjectname TEXT," +
            "work TEXT," +
            "homeworkEvaluation TEXT," +
            "sending_work_status TEXT," +
            "PRIMARY KEY(class_id,section_id,date,subjectid)," +
            "FOREIGN KEY (subjectid) REFERENCES Subjects(subjectid)" +
            "FOREIGN KEY (section_id) REFERENCES Section_Name(section_id)" +
            "FOREIGN KEY (class_id) REFERENCES School_Class(class_id)" +
            ")";

    public static final String Subjects_CREATE_TABLE = "CREATE TABLE Subjects(class_id TEXT," +
            "section_id TEXT," +
            "subjectid TEXT," +
            "subjectname TEXT," +
            "PRIMARY KEY(class_id,section_id,subjectid)," +
            "FOREIGN KEY (section_id) REFERENCES Section_Name(section_id)" +
            "FOREIGN KEY (class_id) REFERENCES School_Class(class_id)" +
            ")";
    public static final String Message_CREATE_TABLE = "CREATE TABLE Message(msg TEXT," +
            "senderStudentID TEXT," +
            "sent_DateTime TEXT," +
            "delivered_DateTime TEXT," +
            "read_DateTime TEXT," +
            "status TEXT" +
            ")";
    public static final String TeacherList_CREATE_TABLE = "CREATE TABLE TeacherList(TeacherID TEXT," +
            "TeacherName TEXT," +
            "PRIMARY KEY(TeacherID)" +
            ")";
    public static final String ResultList_CREATE_TABLE = "CREATE TABLE ResultList(ResultID TEXT," +
            "ResultName TEXT," +
            "PRIMARY KEY(ResultID)" +
            ")";
    public static final String ExamType_CREATE_TABLE = "CREATE TABLE ExamType(examName TEXT," +
            "PRIMARY KEY(examName)" +
            ")";
    public static final String ParentSubject_CREATE_TABLE = "CREATE TABLE ParentSubject(subjectid TEXT," +
            "subjectname TEXT," +
            "PRIMARY KEY(subjectid)" +
            ")";
    public static final String Feedback_CREATE_TABLE = "CREATE TABLE Feedback(s_no INTEGER PRIMARY KEY AUTOINCREMENT," +
            "student_id TEXT," +
            "ic_feedback TEXT," +
            "date TEXT," +
            "status TEXT," +
            "FOREIGN KEY (student_id) REFERENCES Student_Record(student_id)" +
            ")";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(School_Class_CREATE_TABLE);
        db.execSQL(Section_Name_CREATE_TABLE);
        db.execSQL(Student_Record_CREATE_TABLE);
        db.execSQL(STUDENT_ATTENDANCE_CREATE_TABLE);
        db.execSQL(Homework_CREATE_TABLE);
        db.execSQL(Subjects_CREATE_TABLE);
        db.execSQL(Message_CREATE_TABLE);
        db.execSQL(TeacherList_CREATE_TABLE);
        db.execSQL(ResultList_CREATE_TABLE);
        db.execSQL(ExamType_CREATE_TABLE);
        db.execSQL(EXAM_RESULT_CREATE_TABLE);
        db.execSQL(USER_STAFF_CREATE_TABLE);
        db.execSQL(ParentSubject_CREATE_TABLE);
        db.execSQL(GROUP_RECORD_CREATE_TABLE);
        db.execSQL(GROUP_MESSAGE_CREATE_TABLE);
        db.execSQL(Feedback_CREATE_TABLE);
        db.execSQL(LOCATION_MAP_CREATE_TABLE);
        db.execSQL(CURRENT_LOCATION_USER_CREATE_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion <= 8) {

            if (!isTableExist("School_Class", db))
                db.execSQL(School_Class_CREATE_TABLE);

            if (!isTableExist("Section_Name", db))
                db.execSQL(Section_Name_CREATE_TABLE);

            if (!isTableExist("Student_Record", db))
                db.execSQL(Student_Record_CREATE_TABLE);

            if (!isTableExist("StudentAttendance", db))
                db.execSQL(STUDENT_ATTENDANCE_CREATE_TABLE);

            if (!isTableExist("Homework", db))
                db.execSQL(Homework_CREATE_TABLE);

            if (!isTableExist("Subjects", db))
                db.execSQL(Subjects_CREATE_TABLE);

            if (!isTableExist("Message", db))
                db.execSQL(Message_CREATE_TABLE);

            if (!isTableExist("TeacherList", db))
                db.execSQL(TeacherList_CREATE_TABLE);

            if (!isTableExist("ExamResult", db))
                db.execSQL(EXAM_RESULT_CREATE_TABLE);

            if (!isTableExist("ExamType", db))
                db.execSQL(ExamType_CREATE_TABLE);

            if (!isTableExist("ResultList", db))
                db.execSQL(ResultList_CREATE_TABLE);

            if (!isTableExist("UserStaffList", db))
                db.execSQL(USER_STAFF_CREATE_TABLE);

            if (!isTableExist("ParentSubject", db))
                db.execSQL(ParentSubject_CREATE_TABLE);

            if (!isTableExist("GroupRecord", db))
                db.execSQL(GROUP_RECORD_CREATE_TABLE);

            if (!isTableExist("GroupMsg", db))
                db.execSQL(GROUP_MESSAGE_CREATE_TABLE);

            if (!isTableExist("Feedback", db))
                db.execSQL(Feedback_CREATE_TABLE);

            if (!isTableExist("map_data", db))
                db.execSQL(LOCATION_MAP_CREATE_TABLE);

            if (!isTableExist("currentLocationData", db))
                db.execSQL(CURRENT_LOCATION_USER_CREATE_TABLE);

        }
      /*  if ((oldVersion == 1 || oldVersion == 2 || oldVersion == 3) && newVersion == 4) {

            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS GroupRecord");
            db.execSQL("DROP TABLE IF EXISTS GroupMsg");

            db.execSQL(GROUP_RECORD_CREATE_TABLE);
            db.execSQL(GROUP_MESSAGE_CREATE_TABLE);
            db.execSQL(STUDENT_ATTENDANCE_CREATE_TABLE);

            *//* --- copy data from previous version Attendance Table to New Table ---*//*
            Cursor cursor = db.rawQuery("Select * from Attendance", null);
            while (cursor.moveToNext()) {
                ContentValues cv = new ContentValues();
                cv.put("student_id", cursor.getString(cursor.getColumnIndex("student_id")));
                cv.put("section_id", cursor.getString(cursor.getColumnIndex("section_id")));
                cv.put("subjectId", "");
                cv.put("date", cursor.getString(cursor.getColumnIndex("date")));
                cv.put("present_status", cursor.getString(cursor.getColumnIndex("present_status")));
                cv.put("sending_status", cursor.getString(cursor.getColumnIndex("sending_status")));


                if ((db.insert("StudentAttendance", null, cv) == -1))
                    Log.e("AttendanceInserDB", "error");

            }
            cursor.close();
            db.execSQL("DROP TABLE IF EXISTS Attendance");

            db.execSQL(LOCATION_MAP_CREATE_TABLE);
            getHostNameFromServer();

        }
        if (oldVersion <= 5) {
            db.execSQL(EXAM_RESULT_CREATE_TABLE_COPY);

            db.execSQL("INSERT INTO ExamResultCopy (examName, class_id, subjectid, section_id, student_id, " +
                    "student_name ,grade, obtained_marks, maximum_marks, result, date,sending_Status)" +
                    " SELECT examName, class_id, subjectid ,section_id ,student_id , student_name, " +
                    "grade, obtained_marks, maximum_marks, result, date, sending_Status FROM ExamResult ");
            db.execSQL("DROP TABLE ExamResult");
            db.execSQL("ALTER TABLE ExamResultCopy RENAME TO ExamResult");
        }
        Log.e("RITU", "OnUpgrade()");
        if (newVersion == 6) {
            Log.e("RITU", "OnUpgrade() v6");
            db.execSQL(USER_STAFF_CREATE_TABLE);
            logoutUser(db);
        }*/


        logoutUser(db);


    }

    private void logoutUser(SQLiteDatabase db) {
        SharedPreferences sp = Prefs.with(context).getSharedPreferences();
        String orgKey = sp.getString(Config.SMS_ORGANIZATION_NAME, "");
        // String loggedUserName = sp.getString("forLoginUserName", "");

        String loggedUserName = sp.getString("forLoginUserName", "").equalsIgnoreCase("") ? sp.getString("loggedUserName", "") : sp.getString("forLoginUserName", "");
        String loggedPassword = sp.getString("loggedPassword", "") /*"demo"*/;

        sp.edit().clear().commit();
        sp.edit().putString(Config.SMS_ORGANIZATION_NAME, orgKey).putString("loggedUserName", loggedUserName).putString("loggedPassword", loggedPassword).commit();

        deleteDatabase(db);
        onUpgradeRun = true;
        Log.e("RITU", "OnUpgrade Logout");
    }

    public void saveLocation(Location location) {
        Calendar cal = Calendar.getInstance();
        ContentValues cv = new ContentValues();
        cv.put("date", cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.DAY_OF_MONTH));
        cv.put("time", cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND));
        cv.put("latitude", location.getLatitude());
        cv.put("longitude", location.getLongitude());
        if ((getDbWriteObject().insert("currentLocationData", null, cv) == -1)) {
            Log.e("error LocationSaved", location.getLatitude() + "  ------   " + location.getLongitude());
            Toast.makeText(context, "error in insertion currentLocationData", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("LocationSaved", location.getLatitude() + "  ------   " + location.getLongitude());
            // Toast.makeText(context, " inserted currentLocationData ", Toast.LENGTH_SHORT).show();
        }
        try {
            sendLocationList();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendLocationList() throws JSONException {
        Cursor cursorGroupList = getDbReadObject().rawQuery("Select * from currentLocationData", null);
        final JSONArray jsonArray = new JSONArray();

        // String  userId = sharedPreferences.getString("staffUserID","");
        // String  driverRouteId = sharedPreferences.getString("driverRouteId","");
        String imei = sharedPreferences.getString("IMEI", "");
        if (imei.equalsIgnoreCase("")) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            sharedPreferences.edit().putString("IMEI", tm.getDeviceId()).commit();
        }
        while (cursorGroupList.moveToNext()) {
            Log.e("Location--->", "lat : " + cursorGroupList.getString(cursorGroupList.getColumnIndex("latitude"))
                    + "long : " + cursorGroupList.getString(cursorGroupList.getColumnIndex("longitude"))
                    + " date : " + cursorGroupList.getString(cursorGroupList.getColumnIndex("date"))
                    + " time : " + cursorGroupList.getString(cursorGroupList.getColumnIndex("time"))

            );
            final JSONObject param = new JSONObject();
            // param.put("routeID", driverRouteId);
            param.put("identifier", imei);
            // param.put("type", "Vehicle");
            //    param.put("id", userId);
            param.put("date", cursorGroupList.getString(cursorGroupList.getColumnIndex("date")));
            param.put("time", cursorGroupList.getString(cursorGroupList.getColumnIndex("time")));
            param.put("lat", cursorGroupList.getString(cursorGroupList.getColumnIndex("latitude")));
            param.put("lng", cursorGroupList.getString(cursorGroupList.getColumnIndex("longitude")));
            jsonArray.put(param);
        }

        JSONObject dataJson = new JSONObject();
        dataJson.put("dataArray", jsonArray);
        Log.e("varsha", dataJson.toString());

        String addNotiUrl = Config.GPS_DATA_URL;

        Log.d("GpsData", addNotiUrl);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, addNotiUrl, dataJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("GpsData", jsonObject.toString());

                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                            getDbReadObject().delete("currentLocationData", null, null);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("GpsData", volleyError.toString());

                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,


                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public ArrayList<ExamData> getFilledExamResultListFromDatabase() {
        ArrayList<ExamData> listData = new ArrayList<>();
        SQLiteDatabase db = getDbWriteObject();
        if (db == null || !db.isOpen()) {
            db = getWritableDatabase();
        }
        if (isTableExist("ExamResult", db) && isTableExist("School_Class", db) && isTableExist("Section_Name", db) && isTableExist("Subjects", db)) {
            Cursor cc = getDbReadObject().rawQuery("Select * " +
                            " FROM ExamResult,School_Class,Section_Name,Subjects where" +
                            " ExamResult.section_id=Section_Name.section_id  and Section_Name.class_id=School_Class.class_id and" +
                            " ExamResult.subjectid=Subjects.subjectid and Subjects.section_id=ExamResult.section_id"
                    ,
                    null);
            ArrayList<String> examTye = new ArrayList<>();
            ArrayList<String> sectionId = new ArrayList<>();
            ArrayList<String> subjectId = new ArrayList<>();
            ArrayList<String> examDate = new ArrayList<>();

            if (cc != null) {
                while (cc.moveToNext()) {

                    if ((examTye.contains(cc.getString(cc.getColumnIndex("examName")))
                            && sectionId.contains(cc.getString(cc.getColumnIndex("section_id")))
                            && subjectId.contains(cc.getString(cc.getColumnIndex("subjectid")))
                            && examDate.contains(cc.getString(cc.getColumnIndex("date"))))

                            ) {


                    } else {

                        examTye.add(cc.getString(cc.getColumnIndex("examName")));
                        sectionId.add(cc.getString(cc.getColumnIndex("section_id")));
                        subjectId.add(cc.getString(cc.getColumnIndex("subjectid")));
                        examDate.add(cc.getString(cc.getColumnIndex("date")));
                        listData.add(new ExamData(
                                cc.getString(cc.getColumnIndex("examName")),
                                cc.getString(cc.getColumnIndex("section_id")),
                                cc.getString(cc.getColumnIndex("class_no")) + " " + cc.getString(cc.getColumnIndex("section")),
                                cc.getString(cc.getColumnIndex("subjectid")),
                                cc.getString(cc.getColumnIndex("subjectname")),
                                cc.getString(cc.getColumnIndex("date")),
                                cc.getString(cc.getColumnIndex("maximum_marks")),
                                cc.getString(cc.getColumnIndex("class_id"))
                        ));
                    }
                }
                cc.close();
            }

        }
        db.close();
        return listData;
    }


    public boolean isTableExist(String tableName, SQLiteDatabase db) {

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        if (!cursor.moveToFirst()) {
            cursor.close();
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }


    private void getHostNameFromServer() {

        final Map<String, Object> param = new LinkedHashMap<>();
        param.put("applicationName", "apischoolerp");
        param.put("organizationKey", Prefs.with(context).getSharedPreferences().getString("organization_name", ""));
        //   String url = "http://cpanel.zeroerp.com/Login/GetHostName";
        //   String url = "http://192.168.1.151/cpanel/Login/GetHostName";


        String hostNameUrl = "http://testapicpanel.zeroerp.com/HostApi.php?"
                + "data=" + (new JSONObject(param));

        Log.e("HostNameUrl", hostNameUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
                hostNameUrl

                , new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("HostName", s);

                JSONObject job = null;
                try {
                    job = new JSONObject(s);
                    String code = job.optString("code");
                    switch (code) {
                        case 200 + "":
                            Prefs.with(context).getSharedPreferences().edit()
                                    .putString("HostName", job.optString("applicationURL"))
                                    .commit();

                            break;


                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());
                Log.d("HostNameJson", new JSONObject(param).toString());

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(context).addToRequestQueue(request);

    }

    /* ---   fill data into GroupRecord Table if its conversation not available ----*/

    public int createChat(String userId, String userName, String userType) {
      userType =  (userType.equalsIgnoreCase("parents")?"Parent":userType).toUpperCase();
        Log.e("userId", userId + " DBHANDLER");
        Log.e("userName", userName + " DBHANDLER");
        Log.e("userType", userType + " userType");
     //   Log.e("userId", userId + " DBHANDLER");

        ContentValues cv = new ContentValues();
        cv.put("groupName", userName);
        cv.put("groupAdmin", sharedPreferences.getString("loggedUserID", "Not Found"));
        cv.put("groupAdminType", sharedPreferences.getString("user_type", "Not Found").toUpperCase());
        cv.put("groupMembers", userId);
        cv.put("groupMembersType", userType);
        cv.put("createdAt", Config.simple.format(new Date()));
        int count = 0;

        Cursor cursorCreateChat = getDbWriteObject().rawQuery("Select * from GroupRecord where groupMembers=?" +
                        " AND groupMembersType=?",
                new String[]{userId, userType});
        if (cursorCreateChat != null && cursorCreateChat.moveToNext()) {
            count = cursorCreateChat.getInt(cursorCreateChat.getColumnIndex("groupId"));
        } else {
            if ((getDbWriteObject().insert("GroupRecord", null, cv) == -1))
                Toast.makeText(context, "error in insertion section", Toast.LENGTH_SHORT).show();
            //      else Toast.makeText(context, " inserted  " + group_name, Toast.LENGTH_SHORT).show();

            Cursor ccGroupRecord = getDbReadObject().rawQuery("Select * from GroupRecord", null);
            if (ccGroupRecord != null && ccGroupRecord.moveToLast())
                count = ccGroupRecord.getInt(cursorCreateChat.getColumnIndex("groupId"));
            ccGroupRecord.close();
        }

        cursorCreateChat.close();

        return count;
    }

    public ArrayList<ChatData> getLastChatConversationList(int groupId) {
        ArrayList<ChatData> lastChatConversationList = new ArrayList<>();

        Cursor ccList = getDbReadObject().rawQuery("Select *" +
                        " FROM GroupMsg where groupId=? ORDER BY DATE(createdAtMsg) ASC ",
                new String[]{groupId + ""});

        String date_pre = "";
        if (ccList != null) {
            while (ccList.moveToNext()) {
                String date_arr[] = ccList.getString(ccList.getColumnIndex("createdAtMsg")).split(" ");
                if (date_pre.equalsIgnoreCase(date_arr[1] + " " + date_arr[0] + " " + date_arr[2])) {
                    lastChatConversationList.add(new ChatData(
                            Integer.parseInt(ccList.getString(ccList.getColumnIndex("groupId"))),
                            Integer.parseInt(ccList.getString(ccList.getColumnIndex("msgId"))),
                            ccList.getString(ccList.getColumnIndex("userID")),
                            ccList.getString(ccList.getColumnIndex("userType")),
                            ccList.getString(ccList.getColumnIndex("userName")),
                            ccList.getString(ccList.getColumnIndex("msg")),
                            ccList.getString(ccList.getColumnIndex("createdAtMsg")),
                            ccList.getString(ccList.getColumnIndex("status")),
                            false
                    ));
                } else {
                    lastChatConversationList.add(new ChatData(
                            Integer.parseInt(ccList.getString(ccList.getColumnIndex("groupId"))),
                            Integer.parseInt(ccList.getString(ccList.getColumnIndex("msgId"))),
                            ccList.getString(ccList.getColumnIndex("userID")),
                            ccList.getString(ccList.getColumnIndex("userType")),
                            ccList.getString(ccList.getColumnIndex("userName")),
                            ccList.getString(ccList.getColumnIndex("msg")),
                            ccList.getString(ccList.getColumnIndex("createdAtMsg")),
                            ccList.getString(ccList.getColumnIndex("status")),
                            true
                    ));

                    date_pre = date_arr[1] + " " + date_arr[0] + " " + date_arr[2];
                }
            }
            ccList.close();
        }


        ContentValues cv = new ContentValues();
        cv.put("status", "read");
        getDbWriteObject().update("GroupMsg", cv, "groupId=? AND status=?", new String[]{groupId + "", "get"});

        return lastChatConversationList;

    }

    public Bundle saveChatConversations(int groupId, String userId, String userName, String userType,
                                        String msg) {

        boolean firstTime = true;
        String msgId = "";
        if (groupId == 0) {
            groupId = createChat(userId, userName, userType);
            firstTime = false;
        } else firstTime = true;


        if (firstTime) {

            ContentValues cv = new ContentValues();
            cv.put("groupId", groupId);
            cv.put("userID", userId);
            cv.put("userType", userType);
            cv.put("userName", userName);
            cv.put("msg", msg);
            cv.put("createdAtMsg", Config.msgDateFormat.format(new Date()));
            cv.put("status", "pending");

            if ((getDbWriteObject().insert("GroupMsg", null, cv) == -1))
                Toast.makeText(context, "error in insertion msg", Toast.LENGTH_SHORT).show();
            else {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, " inserted msg ", Toast.LENGTH_SHORT).show();
            }

            Cursor cb = getDbReadObject().rawQuery("Select * from GroupMsg where groupId=?",
                    new String[]{groupId + ""});
            if (cb != null) {
                cb.moveToLast();
                msgId = cb.getString(cb.getColumnIndex("msgId"));
                cb.close();
            }

            //   db.close();

        }

        Bundle b = new Bundle();
        b.putInt("groupId", groupId);
        b.putString("msgId", msgId);
        return b;
    }

    public ArrayList<ChatList> getChatList() {
        ArrayList<ChatList> chatList = new ArrayList<>();

        Cursor cursorGroupList = getDbReadObject().rawQuery("Select * from GroupRecord", null);

        if (cursorGroupList != null) {
            while (cursorGroupList.moveToNext()) {
                String groupID = cursorGroupList.getString(cursorGroupList.getColumnIndex("groupId"));

                Cursor cursorLastMsg = getDbReadObject().rawQuery("Select * from GroupMsg where groupId=?",
                        new String[]{groupID});
                if (cursorLastMsg != null && cursorLastMsg.moveToLast()) {

                    Cursor cursorReadMsgList = getDbReadObject().rawQuery("Select * from GroupMsg" +
                            " where groupId=? AND status=?", new String[]{groupID, "get"}
                    );
                    if (cursorReadMsgList != null) {
                        if (cursorReadMsgList.moveToLast()) {

                            chatList.add(new ChatList(Integer.parseInt(groupID),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupName")),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembers")),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembersType")),
                                    cursorLastMsg.getString(cursorLastMsg.getColumnIndex("msg")),
                                    cursorLastMsg.getString(cursorLastMsg.getColumnIndex("createdAtMsg")),
                                    cursorReadMsgList.getCount()
                            ));


                        } else {

                            chatList.add(new ChatList(Integer.parseInt(groupID),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupName")),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembers")),
                                    cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembersType")),
                                    cursorLastMsg.getString(cursorLastMsg.getColumnIndex("msg")),
                                    cursorLastMsg.getString(cursorLastMsg.getColumnIndex("createdAtMsg")),
                                    0
                            ));

                        }
                        cursorReadMsgList.close();
                    } else {
                        chatList.add(new ChatList(Integer.parseInt(groupID),
                                cursorGroupList.getString(cursorGroupList.getColumnIndex("groupName")),
                                cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembers")),
                                cursorGroupList.getString(cursorGroupList.getColumnIndex("groupMembersType")),
                                cursorLastMsg.getString(cursorLastMsg.getColumnIndex("msg")),
                                cursorLastMsg.getString(cursorLastMsg.getColumnIndex("createdAtMsg")),
                                0
                        ));

                    }

                    cursorLastMsg.close();
                }


            }
            cursorGroupList.close();
        }

        Collections.sort(chatList, new Comparator<ChatList>() {
            @Override
            public int compare(ChatList o1, ChatList o2) {
                Calendar calLeft = Calendar.getInstance();
                Calendar calRight = Calendar.getInstance();
                try {
                    calLeft.setTime(Config.msgDateFormat.parse(o2.getLastChatTime() ));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    calRight.setTime(Config.msgDateFormat.parse(o1.getLastChatTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                                                /*int res = calLeft.compareTo(calRight);
                                                if (res == 0) return 0;
                                                else if (res > 0) return -1;
                                                else if (res < 0) return 1;
*/
                return calLeft.compareTo(calRight);

            }
        });

        return chatList;
    }

    public void updateChatStatus(int msgId, String status) {
        //  SQLiteDatabase db = super.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("status", status);

        getDbWriteObject().update("GroupMsg", cv, "msgId=?",
                new String[]{msgId + ""});
    }


    public Bundle saveChatFromFcm(String userId, String userName, String userType, String message, String createdAtMsg) {
        int groupId = 0;

        groupId = createChat(userId, userName, userType);

        ContentValues cv = new ContentValues();
        cv.put("groupId", groupId);
        cv.put("userID", userId);
        cv.put("userType", userType);
        cv.put("userName", userName);
        cv.put("msg", message);
        cv.put("createdAtMsg", createdAtMsg);
        cv.put("status", "get");

        // SQLiteDatabase  db = super.getWritableDatabase();

           /*--- for this message is available in list of messages or not ---*/
//        boolean isAvailable = false;
//        Cursor c_available = getDbReadObject().rawQuery("Select * from GroupMsg where groupId=?",
//                new String[]{groupId + ""});
//        if (c_available.moveToNext())
//            isAvailable = true;

        if (userId == "" || userId == null) ;
        else {
            if ((getDbWriteObject().insert("GroupMsg", null, cv) == -1))
                Log.d("errorInSaving", userName);
            else Log.d("SavedInSuccessfully", userName);

        }



            /*--- for getting serial number of message ---*/
        Cursor cbSerialNumber = getDbReadObject().rawQuery("Select * from GroupMsg where groupId=?",
                new String[]{groupId + ""});
        String msgId = "";
        if (cbSerialNumber.moveToLast())
            msgId = cbSerialNumber.getString(cbSerialNumber.getColumnIndex("msgId"));
        cbSerialNumber.close();

        Bundle b = new Bundle();
        b.putInt("groupId", groupId);
        b.putString("msgId", msgId);
        // b.putBoolean("isAvailable", isAvailable);
        //  db.close();
        return b;
    }


    public void sendPendingChats() throws JSONException {
        String db_name = sharedPreferences.getString("organization_name", "Not Found");
        String sender_id = sharedPreferences.getString("loggedUserID", "Not Found");
        String sender_name = sharedPreferences.getString("loggedUserName", "Not Found");
        String senderType = sharedPreferences.getString("user_type", "Not Found").toUpperCase();

        final Cursor cs = getDbWriteObject().rawQuery("Select * from GroupMsg,GroupRecord " +
                        "where status=? and GroupMsg.groupId=GroupRecord.groupId",
                new String[]{"pending"});

        while (cs != null && cs.moveToNext()) {
            JSONArray jsonArray = new JSONArray();

            JSONObject map = new JSONObject();
            map.put("app_msg_s_no", cs.getString(cs.getColumnIndex("msgId")));
            map.put("sender_id", sender_id);
            map.put("sender_name", sender_name);
            map.put("senderType", senderType);
            map.put("msg", cs.getString(cs.getColumnIndex("msg")));
            map.put("time", cs.getString(cs.getColumnIndex("createdAtMsg")));
            map.put("receiver_id", cs.getString(cs.getColumnIndex("groupMembers")));
            map.put("receiver_type", cs.getString(cs.getColumnIndex("groupMembersType")));
            jsonArray.put(map);

            final JSONObject param = new JSONObject();
            param.put("DB_Name", db_name);
            param.put("GroupData", jsonArray);

            String chatUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    +
                    "ChatDetails.php?action=insertmsg";

            Log.e("ChatDetailUrl", chatUrl);
            Log.e("param", param.toString());

            // RequestQueue rqueue = Volley.newRequestQueue(this);
            JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, chatUrl, param
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {
                    Log.d("PendingMsgResponse", s.toString());


                    JSONObject jsonObject_response = s;
                    if (jsonObject_response.optString("GcmResponse").equalsIgnoreCase("1")) {
                        ContentValues cv = new ContentValues();
                        cv.put("status", "send");

                        getDbWriteObject().update("GroupMsg", cv, "msgId=?",
                                new String[]{jsonObject_response.optString("app_msg_s_no")});
                    }


                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("PendingMsgResponse", volleyError.toString());
                    //   Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                }
            });


            int socketTimeout = 0;
            string_req.setRetryPolicy(new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //rqueue.add(string_req);
            AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);
        }


        if (cs != null) cs.close();


    }


    public void updateSentStatusOfMsg(String msgId, String status) {
        //  SQLiteDatabase db = super.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("status", status);

        int affectedRows = getDbWriteObject().update("GroupMsg", cv, "msgId=?",
                new String[]{msgId});
        Log.e("affectedRows", affectedRows + "rows");

    }


    public ArrayList<StudentData> getClassSectionList() {
        //  ArrayList<CreateClass> classSectionIdList = new ArrayList<>();
        ArrayList<StudentData> classSectionIdList = new ArrayList<>();
        Cursor cs = getDbReadObject().rawQuery("Select * from School_Class,Section_Name where " +
                        "School_Class.class_id= Section_Name.class_id",
                null);

        if (cs != null) {

            while (cs.moveToNext()) {
//                classSectionIdList.add(new CreateClass(cs.getString(cs.getColumnIndex("section_id"))
//                        , cs.getString(cs.getColumnIndex("class_no")) + " " +
//                        cs.getString(cs.getColumnIndex("section"))));
                classSectionIdList.add(new StudentData(cs.getString(cs.getColumnIndex("class_no")) + " " + cs.getString(cs.getColumnIndex("section")),
                        cs.getString(cs.getColumnIndex("section_id"))));

            }
            cs.close();
        }


        return classSectionIdList;
    }


    public String getSectionId(String loggedUserID) {
        Cursor cc = getDbReadObject().rawQuery("Select section_id from Student_Record where student_id=?",
                new String[]{loggedUserID});
        String sectionId = "";
        if (cc != null && cc.moveToNext()) {
            sectionId = cc.getString(cc.getColumnIndex("section_id"));
            cc.close();
        }

        return sectionId;
    }


    public boolean deleteDatabase() {
        SQLiteDatabase db = dbRead;
        if (db == null || !db.isOpen()) {
            db = super.getReadableDatabase();
        }
        if (isTableExist("StudentAttendance", db))
            db.delete("StudentAttendance", null, null);

        if (isTableExist("Homework", db))
            db.delete("Homework", null, null);

        if (isTableExist("ExamResult", db))
            db.delete("ExamResult", null, null);

        if (isTableExist("Student_Record", db))
            db.delete("Student_Record", null, null);

        if (isTableExist("Subjects", db))
            db.delete("Subjects", null, null);

        if (isTableExist("Section_Name", db))
            db.delete("Section_Name", null, null);

        if (isTableExist("School_Class", db))
            db.delete("School_Class", null, null);

        if (isTableExist("Message", db))
            db.delete("Message", null, null);

        if (isTableExist("TeacherList", db))
            db.delete("TeacherList", null, null);

        if (isTableExist("ExamType", db))
            db.delete("ExamType", null, null);

        if (isTableExist("ResultList", db))
            db.delete("ResultList", null, null);

        if (isTableExist("ParentSubject", db))
            db.delete("ParentSubject", null, null);

        if (isTableExist("GroupMsg", db))
            db.delete("GroupMsg", null, null);

        if (isTableExist("GroupRecord", db))
            db.delete("GroupRecord", null, null);

        if (isTableExist("Feedback", db))
            db.delete("Feedback", null, null);

        if (isTableExist("map_data", db))
            db.delete("map_data", null, null);

        if (isTableExist("UserStaffList", db))
            db.delete("UserStaffList", null, null);
        db.close();
        //dbwrite.close();

        return true;
    }

    public boolean deleteDatabase(SQLiteDatabase db) {

        if (db == null || !db.isOpen()) {
            db = super.getReadableDatabase();
        }
        if (isTableExist("StudentAttendance", db))
            db.delete("StudentAttendance", null, null);

        if (isTableExist("Homework", db))
            db.delete("Homework", null, null);

        if (isTableExist("ExamResult", db))
            db.delete("ExamResult", null, null);

        if (isTableExist("Student_Record", db))
            db.delete("Student_Record", null, null);

        if (isTableExist("Subjects", db))
            db.delete("Subjects", null, null);

        if (isTableExist("Section_Name", db))
            db.delete("Section_Name", null, null);

        if (isTableExist("School_Class", db))
            db.delete("School_Class", null, null);

        if (isTableExist("Message", db))
            db.delete("Message", null, null);

        if (isTableExist("TeacherList", db))
            db.delete("TeacherList", null, null);

        if (isTableExist("ExamType", db))
            db.delete("ExamType", null, null);

        if (isTableExist("ResultList", db))
            db.delete("ResultList", null, null);

        if (isTableExist("ParentSubject", db))
            db.delete("ParentSubject", null, null);

        if (isTableExist("GroupMsg", db))
            db.delete("GroupMsg", null, null);

        if (isTableExist("GroupRecord", db))
            db.delete("GroupRecord", null, null);

        if (isTableExist("Feedback", db))
            db.delete("Feedback", null, null);

        if (isTableExist("map_data", db))
            db.delete("map_data", null, null);

        if (isTableExist("UserStaffList", db))
            db.delete("UserStaffList", null, null);

        if (isTableExist("currentLocationData", db))
            db.delete("currentLocationData", null, null);

        //dbwrite.close();

        return true;
    }


    public boolean saveStudentDataFromServer(String[] name, String[] id, String class_id, String section_id) {
        //   SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SSSSSSSSS", "Student");
        ContentValues cv = new ContentValues();


        for (int i = 0; i < id.length; i++) {
            cv.put("class_id", class_id);
            cv.put("section_id", section_id);
            cv.put("student_name", name[i]);
            cv.put("student_id", id[i]);

            if (id[i] == null) ;
            else {
                Cursor cs3 = getDbWriteObject().rawQuery("Select * from Student_Record where student_id=?", new String[]{id[i]});
                if (cs3 != null) {
                    if (!cs3.moveToNext()) {
                        if ((getDbWriteObject().insert("Student_Record", null, cv) == -1))
                            Toast.makeText(context, "error in insertion section", Toast.LENGTH_SHORT).show();

                        // Log.e("name", name[i]);
                    } else
                        getDbWriteObject().update("Student_Record", cv, "student_id=?", new String[]{id[i]});
                    cs3.close();
                }
            }


        }
        //  db.close();
        return true;
    }

    public boolean saveSectionIntoDataBase(String class_id, String section_id, String section_name) {
        // SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SSSSSSSSS", "Section");
        ContentValues cv = new ContentValues();

        cv.put("class_id", class_id);
        cv.put("section_id", section_id);
        cv.put("section", section_name);

        Cursor cs = getDbWriteObject().rawQuery("Select * from Section_Name where class_id=? AND section_id=?", new String[]{class_id, section_id});

        if (cs != null && cs.moveToNext()) ;
        else {

            if ((getDbWriteObject().insert("Section_Name", null, cv) == -1))
                Toast.makeText(context, "error in insertion section", Toast.LENGTH_SHORT).show();
//            else
//                Log.d("RITU", section_name);
//            Toast.makeText(context, "inserted into database Section_Name ", Toast.LENGTH_SHORT).show();


        }
        if (cs != null) cs.close();
        // db.close();
        return true;
    }

    public boolean saveClassIntoDataBase(String class_name, String class_id) {
        //   SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SSSSSSSSS", "Class");
        ContentValues cv = new ContentValues();

        cv.put("class_no", class_name);
        cv.put("class_id", class_id);

        Cursor cs = getDbWriteObject().rawQuery("Select * from School_Class where class_id=?", new String[]{class_id});
        if (cs != null) {
            if (!cs.moveToNext()) {
                if ((getDbWriteObject().insert("School_Class", null, cv) == -1))
                    Toast.makeText(context, "error in insertion class", Toast.LENGTH_SHORT).show();

            } else {
                getDbWriteObject().update("School_Class", cv, "class_id=?",
                        new String[]{class_id});
            }

            cs.close();
        }
        //  db.close();
        return true;
    }

    public String[][] getClassName() {

        //  SQLiteDatabase db = super.getDbReadObject();
        Cursor cs = getDbReadObject().rawQuery("Select * from School_Class", null);
        String[] cls_name = new String[cs.getCount()];
        String[] cls_id = new String[cs.getCount()];
        int i = 0;
        while (cs != null && cs.moveToNext()) {
            cls_name[i] = cs.getString(cs.getColumnIndex("class_no"));
            cls_id[i] = cs.getString(cs.getColumnIndex("class_id"));
            Log.e("SSSSSSSSS", cls_name[i] + " " + cls_id[i]);
            i++;
        }
        String[][] abc = new String[][]{cls_id, cls_name};
        if (cs != null) cs.close();
        //  db.close();
        return abc;

    }

    public String[][] getSectionName(String attandence_class_id) {
        //    SQLiteDatabase db = super.getDbReadObject();
        Cursor cs = getDbWriteObject().rawQuery("Select * from Section_Name where class_id=?", new String[]{attandence_class_id});

        String[] sec_name = new String[cs.getCount()];
        String[] sec_id = new String[cs.getCount()];

        int i = 0;
        while (cs != null && cs.moveToNext()) {
            sec_name[i] = cs.getString(cs.getColumnIndex("section"));
            sec_id[i] = cs.getString(cs.getColumnIndex("section_id"));
            i++;
        }
        String abc[][] = new String[][]{sec_id, sec_name};
        if (cs != null) cs.close();
        //  db.close();
        return abc;


    }

    public String[][] getStudentData(String class_id, String section_id, String date_name) {

        int i = 0;

        Cursor cs = getDbReadObject().rawQuery("Select * from Student_Record where class_id=? " +
                        "AND section_id=? order by student_name COLLATE NOCASE",
                new String[]{class_id, section_id});
        String[] st_name = new String[cs.getCount()];
        String[] prasent_status = new String[cs.getCount()];
        String[] id = new String[cs.getCount()];
        if (cs != null) {
            while (cs.moveToNext()) {

                st_name[i] = cs.getString(cs.getColumnIndex("student_name"));
                prasent_status[i] = "no";
                id[i] = cs.getString(cs.getColumnIndex("student_id"));
                i++;
            }
            cs.close();
        }   //  db.close();
        // }
        // cuu.close();
        //    db.close();
        String[][] abc = new String[][]{st_name, id, prasent_status};
        return abc;
    }


    public List<StudentData> getStudents(String class_id, String section_id) {
        //     SQLiteDatabase db = super.getDbReadObject();
        Cursor cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? order by student_name COLLATE NOCASE",
                new String[]{section_id});
        List<StudentData> data = new ArrayList<>();
        if (cs != null) {
            while (cs.moveToNext()) {
                data.add(new StudentData(cs.getString(cs.getColumnIndex("student_name")),
                        cs.getString(cs.getColumnIndex("student_id"))));

            }
            cs.close();
        }  //  db.close();
        return data;
    }


    private void sendData(final String key) throws JSONException {
        if (!sharedPreferences.getString("user_type", "").equalsIgnoreCase("")) {
            ConnectivityManager com = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            NetworkInfo[] networkInfos = com.getAllNetworkInfo();

            for (NetworkInfo ni : networkInfos) {
            /*-----  for WIFI Data Network  --------*/
                if (ni != null && ni.getTypeName().equalsIgnoreCase("WIFI")) {

                    if (ni != null && ni.isConnected()) {

                        if (key.equalsIgnoreCase("ic_attendance")) {
                            if (sharedPreferences.getBoolean("wifi_attendance", false))
                                sendAttendanceToServer();
                        } else if (key.equalsIgnoreCase("homework")) {
                            if (sharedPreferences.getBoolean("wifi_homework", false)) {
                                sendHomeWorkToServer();
                                sendHomeWorkEvaluationToServer();
                            }
                        } else if (key.equalsIgnoreCase("result")) {
                            if (sharedPreferences.getBoolean("wifi_result", false))
                                sendExamResultToServer();
                        } else {
                            if (sharedPreferences.getBoolean("autoSendingWifi", false))
                                sendDataToServer();
                        }


                    }

                }
            /*-----  for Mobile Data Network  --------*/
                if (ni != null && ni.getTypeName().equalsIgnoreCase("MOBILE")) {

                    if (ni != null && ni.isConnected()) {

                        if (key.equalsIgnoreCase("ic_attendance")) {
                            if (sharedPreferences.getBoolean("MobileData_attendance", false))
                                sendAttendanceToServer();
                        } else if (key.equalsIgnoreCase("homework")) {
                            if (sharedPreferences.getBoolean("MobileData_homework", false)) {
                                sendHomeWorkToServer();
                                sendHomeWorkEvaluationToServer();
                            }
                        } else if (key.equalsIgnoreCase("result")) {
                            if (sharedPreferences.getBoolean("MobileData_result", false))
                                sendExamResultToServer();
                        } else {
                            if (sharedPreferences.getBoolean("autoSendingMobileData", false))
                                sendDataToServer();
                        }

                    }


                }

            }


        /*-----  for Roaming Network  --------*/
            final TelephonyManager telephonyManager = (TelephonyManager)
                    context.getSystemService(context.TELEPHONY_SERVICE);
            new PhoneStateListener() {
                @Override
                public void onServiceStateChanged(ServiceState serviceState) {
                    super.onServiceStateChanged(serviceState);

                    if (telephonyManager != null && telephonyManager.isNetworkRoaming()) {
                        Log.d("roaming", "yes");

                        if (key.equalsIgnoreCase("ic_attendance")) {
                            if (sharedPreferences.getBoolean("roaming_attendance", false))
                                try {
                                    sendAttendanceToServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                        } else if (key.equalsIgnoreCase("homework")) {
                            if (sharedPreferences.getBoolean("roaming_homework", false)) {
                                try {
                                    sendHomeWorkToServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    sendHomeWorkEvaluationToServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (key.equalsIgnoreCase("result")) {
                            if (sharedPreferences.getBoolean("roaming_result", false))
                                try {
                                    sendExamResultToServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                        } else {
                            if (sharedPreferences.getBoolean("autoSendingRoaming", false))
                                try {
                                    sendDataToServer();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                        }


                    } else Log.d("roaming", "no");


                }
            };

        }
    }

    public void sendDataToServer() throws JSONException {
        SQLiteDatabase db = dbwrite;
        if (db == null || !db.isOpen()) {
            db = getWritableDatabase();
        }
        if (isTableExist("StudentAttendance", db)) {
            Cursor cutestatt = getDbReadObject().rawQuery("Select * from StudentAttendance where sending_status=?", new String[]{"Pending"});
            if (cutestatt != null && cutestatt.moveToNext()) {
                //   sendAttendance();
                sendAttendanceToServer();
                cutestatt.close();
            }
        }
        if (isTableExist("Homework", db)) {
            Cursor cutest = getDbWriteObject().rawQuery("Select * from Homework where sending_work_status=?", new String[]{"Pending"});
            if (cutest != null && cutest.moveToNext()) {
                sendHomeWorkToServer();
                sendHomeWorkEvaluationToServer();
                cutest.close();
            }
        }

        if (isTableExist("ExamResult", db)) {
            Cursor cu_result = getDbWriteObject().rawQuery("Select * from ExamResult where sending_Status=?", new String[]{"Pending"});
            if (cu_result != null && cu_result.moveToNext()) {
                sendExamResultToServer();

                cu_result.close();
            }
        }
       // if (sharedPreferences.getString("user_type", "").equalsIgnoreCase("Teacher"))
            sendFeedback();

        sendPendingChats();
        db.close();
    }

    public void sendFeedback() throws JSONException {
        //  String send_url = "http://junctiondev.cloudapp.net/sms/android/examResult.php?action=insertfeedback";
        //   SQLiteDatabase db = super.getReadableDatabase();

        JSONArray jsonarray_review = new JSONArray();

        Cursor cu_review = getDbReadObject().rawQuery("Select * from Feedback where status=?", new String[]{"pending"});
        // Log.d("cu_review",cu_review.getCount()+"");
        if (cu_review != null && cu_review.getCount() > 0) {
            while (cu_review.moveToNext()) {
                JSONObject map = new JSONObject();
                map.put("student_id", cu_review.getString(cu_review.getColumnIndex("student_id")));
                map.put("feedback", cu_review.getString(cu_review.getColumnIndex("ic_feedback")));
                map.put("date", cu_review.getString(cu_review.getColumnIndex("date")));
                map.put("s_no", cu_review.getString(cu_review.getColumnIndex("s_no")));
                jsonarray_review.put(map);
            }
            cu_review.close();
            //  SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
            String org = sharedPreferences.getString("organization_name", "Not Found");

            final JSONObject param = new JSONObject();
            //   param.put("DB_Name", org);
            //   param.put("session", sharedPreferences.getString("session", ""));
            param.put("SenderID", sharedPreferences.getString("staffUserID", "0"));
            param.put("ReviewData", jsonarray_review);

            // RequestQueue rqueue = Volley.newRequestQueue(context);

            String testUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    + "FeedbackApi.php?action=FeedbackPost&databaseName=" +
                    org;
            Log.e("FeedbackUrl", testUrl);
            Log.e("param", param.toString());


            JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, testUrl, param

                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {
                    Log.d("FeedbackResponse", s.toString());

                    JSONObject resJsonObj = s;
                    if (resJsonObj.optString("code").equalsIgnoreCase("201")) {
                        JSONArray sno_arr = null;
                        try {
                            sno_arr = new JSONArray(resJsonObj.optString("result"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //    SQLiteDatabase db = DbHandler.super.getWritableDatabase();

                        if (sno_arr.length() > 0) {
                            if (sharedPreferences.getBoolean("toastVisibility", false))
                                Config.myToast(context, "", "Feedback Uploaded Successfully", Gravity.TOP);
                        }

                        for (int j = 0; j < sno_arr.length(); j++) {
                            JSONObject jobj = sno_arr.optJSONObject(j);

                            if (jobj.optString("result").equalsIgnoreCase("inserted")) {
                                ContentValues cv = new ContentValues();
                                cv.put("status", "Sent");
                                getDbWriteObject().update("Feedback", cv, "s_no=?",
                                        new String[]{jobj.optString("s_no")});
                            }


                        }
                    }


                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("errorVolley", volleyError.toString());

                    if (sharedPreferences.getBoolean("toastVisibility", false))
                        Config.myToast(context, "", "Feedback  Not Send \n" +
                                "Some Error Occurred \n" +
                                "Try Again Later ....!! ", Gravity.TOP);
                }
            });
            string_req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);

        }
    }

    public void sendHomeWorkToServer() throws JSONException {
        //  String send_url = SplashScreen.hostName + "android/gethomework.php";
        //  SQLiteDatabase db = super.getReadableDatabase();

        JSONArray jsonarray_homework_cls = new JSONArray();
        Cursor cu_work = getDbReadObject().rawQuery("Select * from Homework where sending_work_status=? ORDER BY class_id ASC ", new String[]{"Pending"});

        if (cu_work != null && cu_work.getCount() > 0) {
            final String[] sending_status_classID = new String[cu_work.getCount()];
            final String[] sending_status_sectionID = new String[cu_work.getCount()];
            final String[] sending_status_date = new String[cu_work.getCount()];
            final String[] sending_status_subID = new String[cu_work.getCount()];

            String cls_work = "";
            int j = 0;
            while (cu_work.moveToNext()) {

                if (cls_work.equalsIgnoreCase(cu_work.getString(cu_work.getColumnIndex("class_id")))) {
                } else {
                    cls_work = cu_work.getString(cu_work.getColumnIndex("class_id"));

                    Cursor c1 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND sending_work_status=? ORDER BY section_id ASC", new String[]{cls_work, "Pending"});
                    String sec = "";
                    JSONArray json_array_section = new JSONArray();

                    if (c1 != null && c1.getCount() > 0) {
                        while (c1.moveToNext()) {

                            JSONArray json_array_date = new JSONArray();
                            if (sec.equalsIgnoreCase(c1.getString(c1.getColumnIndex("section_id"))))
                                ;
                            else {

                                sec = c1.getString(c1.getColumnIndex("section_id"));

                                Cursor c2 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND sending_work_status=? ORDER BY date ASC",
                                        new String[]{cls_work, sec, "Pending"});
                                String dat = "";
                                if (c2 != null && c2.getCount() > 0) {
                                    while (c2.moveToNext()) {

                                        if (dat.equalsIgnoreCase(c2.getString(c2.getColumnIndex("date"))))
                                            ;
                                        else {
                                            JSONArray subject_array = new JSONArray();

                                            dat = c2.getString(c2.getColumnIndex("date"));

                                            String sub_id = "";

                                            Cursor c3 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=? AND sending_work_status=?",
                                                    new String[]{cls_work, sec, dat, "Pending"});

                                            if (c3 != null && c3.getCount() > 0) {
                                                while (c3.moveToNext()) {
                                                    if (sub_id.equalsIgnoreCase(c3.getString(c3.getColumnIndex("subjectid"))))
                                                        ;
                                                    else {
                                                        sub_id = c3.getString(c3.getColumnIndex("subjectid"));

                                                        Cursor c4 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=? AND subjectid=? AND sending_work_status=?",
                                                                new String[]{cls_work, sec, dat, sub_id, "Pending"});
                                                        if (c4 != null && c4.getCount() > 0) {
                                                            if (c4.moveToNext()) {
                                                                Map<String, Object> work_subid_map = new LinkedHashMap<>();
                                                                work_subid_map.put("SubjectId", sub_id);
                                                                work_subid_map.put("HomeWork", c4.getString(c4.getColumnIndex("work")));
                                                                //  work_subid_map.put("AdmissionID", c4.getString(c4.getColumnIndex("homeworkEvaluation")));

                                                                subject_array.put(new JSONObject(work_subid_map));
                                                            }

                                                            c4.close();
                                                        }
                                                    }

                                                }


                                                c3.close();

                                            }
                                            Map<String, Object> date_map = new LinkedHashMap<>();
                                            date_map.put("Date", dat);
                                            date_map.put("SubjectWork", subject_array);

                                            json_array_date.put(new JSONObject(date_map));

                                            sending_status_classID[j] = cls_work;
                                            sending_status_sectionID[j] = sec;
                                            sending_status_date[j] = dat;
                                            sending_status_subID[j] = sub_id;
                                            j++;

                                        }

                                    }
                                    c2.close();
                                }
                                Map<String, Object> section_map = new LinkedHashMap<>();
                                section_map.put("SectionID", sec);
                                section_map.put("SectionData", json_array_date);

                                json_array_section.put(new JSONObject(section_map));
                            }


                        }
                        c1.close();
                    }
                    Map<String, Object> class_map = new LinkedHashMap<>();
                    class_map.put("ClassID", cls_work);
                    class_map.put("ClassData", json_array_section);

                    jsonarray_homework_cls.put(new JSONObject(class_map));

                }

            }
            cu_work.close();

            //  SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
            String org = sharedPreferences.getString("organization_name", "Not Found");

            JSONObject school_map = new JSONObject();


            school_map.put("SchoolHomeWork", jsonarray_homework_cls);


            final JSONObject param = new JSONObject();
            //  param.put("databaseName", org);
            //   param.put("action","homeworkInsert");
            param.put("userId", sharedPreferences.getString("loggedUserID", ""));
            param.put("session", sharedPreferences.getString("session", ""));
            param.put("SchoolData", school_map);

            //    RequestQueue rqueue = Volley.newRequestQueue(context);

            String homeUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    +
                    "HomeworkApi.php?action=homeworkInsert&databaseName=" + org;

            Log.d("HomeworkUrl", homeUrl);
            Log.d("param", param.toString());

            JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, homeUrl, param
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {

                    //  Toast.makeText(context, "Response of Homework  " + s, Toast.LENGTH_LONG).show();
                    Log.d("ResponseOfHomework", s.toString());

                    JSONObject resJson = s;
                    if (resJson.optString("code").equalsIgnoreCase("201")) {

                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(resJson.optString("result"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Config.myToast(context, "homework", "Home Work Uploaded Successfully", Gravity.TOP);

                        //  SQLiteDatabase db = DbHandler.super.getWritableDatabase();
                        for (int j = 0; j < jsonArray.length(); j++) {
                            ContentValues cv = new ContentValues();
                            cv.put("sending_work_status", "Sent");
//                        Toast.makeText(context, " success", Toast.LENGTH_LONG).show();
                            JSONObject jsonObject = jsonArray.optJSONObject(j);
                            if (jsonObject.optString("result").equalsIgnoreCase("error")) {
                                if (sharedPreferences.getBoolean("toastVisibility", false))
                                    Config.myToast(context, "homework", "Home Work not Uploaded Successfully", Gravity.TOP);

                            } else {
                                getDbWriteObject().update("Homework", cv, "section_id=? AND date=? AND subjectid=?",
                                        new String[]{jsonObject.optString("SectionID"),
                                                jsonObject.optString("date"),
                                                jsonObject.optString("SubjectId"),
                                        });
                                Log.d("updated", jsonObject.optString("date"));
                            }

                        }
                    }


                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("error volley", volleyError.toString());


                    if (sharedPreferences.getBoolean("toastVisibility", false))
                        Config.myToast(context, "homework", "Homework Not Send \n" +
                                "Some Error Occurred \n" +
                                "Try Again Later ....!! ", Gravity.TOP);

                }
            });

//        Toast.makeText(context, newtext JSONObject(param).toString(),Toast.LENGTH_LONG).show();
            string_req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);

        }
    }

    public void sendHomeWorkEvaluationToServer() throws JSONException {
        //  String send_url = SplashScreen.hostName + "android/gethomework.php";
        //  SQLiteDatabase db = super.getReadableDatabase();

        JSONArray jsonarray_homework_cls = new JSONArray();
        Cursor cu_work = getDbReadObject().rawQuery("Select * from Homework where sending_work_status=? ORDER BY class_id ASC ", new String[]{"Pending"});
        if (cu_work != null && cu_work.getCount() > 0) {


            final String[] sending_status_classID = new String[cu_work.getCount()];
            final String[] sending_status_sectionID = new String[cu_work.getCount()];
            final String[] sending_status_date = new String[cu_work.getCount()];
            final String[] sending_status_subID = new String[cu_work.getCount()];

            String cls_work = "";
            int j = 0;
            while (cu_work.moveToNext()) {

                if (cls_work.equalsIgnoreCase(cu_work.getString(cu_work.getColumnIndex("class_id")))) {
                } else {
                    cls_work = cu_work.getString(cu_work.getColumnIndex("class_id"));

                    Cursor c1 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND sending_work_status=? ORDER BY section_id ASC", new String[]{cls_work, "Pending"});
                    String sec = "";
                    JSONArray json_array_section = new JSONArray();
                    while (c1.moveToNext()) {

                        JSONArray json_array_date = new JSONArray();
                        if (sec.equalsIgnoreCase(c1.getString(c1.getColumnIndex("section_id")))) ;
                        else {

                            sec = c1.getString(c1.getColumnIndex("section_id"));

                            Cursor c2 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND sending_work_status=? ORDER BY date ASC",
                                    new String[]{cls_work, sec, "Pending"});
                            String dat = "";

                            while (c2.moveToNext()) {

                                if (dat.equalsIgnoreCase(c2.getString(c2.getColumnIndex("date")))) ;
                                else {
                                    JSONArray subject_array = new JSONArray();

                                    dat = c2.getString(c2.getColumnIndex("date"));

                                    String sub_id = "";

                                    Cursor c3 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=? AND sending_work_status=?",
                                            new String[]{cls_work, sec, dat, "Pending"});


                                    while (c3.moveToNext()) {
                                        if (sub_id.equalsIgnoreCase(c3.getString(c3.getColumnIndex("subjectid"))))
                                            ;
                                        else {
                                            sub_id = c3.getString(c3.getColumnIndex("subjectid"));

                                            Cursor c4 = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=? AND subjectid=? AND sending_work_status=?",
                                                    new String[]{cls_work, sec, dat, sub_id, "Pending"});

                                            if (c4.moveToNext()) {
                                                Map<String, Object> work_subid_map = new LinkedHashMap<>();
                                                work_subid_map.put("SubjectId", sub_id);
                                                // work_subid_map.put("HomeWork", c4.getString(c4.getColumnIndex("work")));
                                                work_subid_map.put("AdmissionID", c4.getString(c4.getColumnIndex("homeworkEvaluation")));

                                                subject_array.put(new JSONObject(work_subid_map));
                                            }

                                            c4.close();
                                        }

                                    }


                                    c3.close();


                                    Map<String, Object> date_map = new LinkedHashMap<>();
                                    date_map.put("Date", dat);
                                    date_map.put("SubjectWork", subject_array);

                                    json_array_date.put(new JSONObject(date_map));

                                    sending_status_classID[j] = cls_work;
                                    sending_status_sectionID[j] = sec;
                                    sending_status_date[j] = dat;
                                    sending_status_subID[j] = sub_id;
                                    j++;

                                }

                            }
                            c2.close();
                            Map<String, Object> section_map = new LinkedHashMap<>();
                            section_map.put("SectionID", sec);
                            section_map.put("SectionData", json_array_date);

                            json_array_section.put(new JSONObject(section_map));
                        }


                    }
                    Map<String, Object> class_map = new LinkedHashMap<>();
                    class_map.put("ClassID", cls_work);
                    class_map.put("ClassData", json_array_section);

                    jsonarray_homework_cls.put(new JSONObject(class_map));
                    c1.close();
                }

            }
            cu_work.close();

            //  SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
            String org = sharedPreferences.getString("organization_name", "Not Found");

            JSONObject school_map = new JSONObject();


            school_map.put("SchoolHomeWork", jsonarray_homework_cls);


            final JSONObject param = new JSONObject();
            //  param.put("databaseName", org);
            //   param.put("action","homeworkInsert");
            param.put("userId", sharedPreferences.getString("loggedUserID", ""));
            param.put("session", sharedPreferences.getString("session", ""));
            param.put("SchoolData", school_map);

            String evaUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    +
                    "HomeworkEvaluationApi.php?action=homeworkEvaluationInsert&databaseName=" + org;
            Log.e("HomeworkEvalUrl", evaUrl);
            Log.e("param", param.toString());

            JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, evaUrl, param
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {

                    //  Toast.makeText(context, "Response of Homework  " + s, Toast.LENGTH_LONG).show();
                    Log.d("ResponseOfEvalHomework", s.toString());

                    JSONObject resJson = s;
                    if (resJson.optString("code").equalsIgnoreCase("201")) {

                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(resJson.optString("result"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Config.myToast(context, "homework", "Home Work Uploaded Successfully", Gravity.TOP);

                        //  SQLiteDatabase db = DbHandler.super.getWritableDatabase();
                        for (int j = 0; j < jsonArray.length(); j++) {
                            ContentValues cv = new ContentValues();
                            cv.put("sending_work_status", "Sent");
//                        Toast.makeText(context, " success", Toast.LENGTH_LONG).show();
                            JSONObject jsonObject = jsonArray.optJSONObject(j);
                            if (jsonObject.optString("result").equalsIgnoreCase("error")) {
                                if (sharedPreferences.getBoolean("toastVisibility", false))
                                    Config.myToast(context, "homework", "Home Work not Uploaded Successfully", Gravity.TOP);

                            } else {
                                getDbWriteObject().update("Homework", cv, "section_id=? AND date=? AND subjectid=?",
                                        new String[]{jsonObject.optString("SectionID"),
                                                jsonObject.optString("date"),
                                                jsonObject.optString("SubjectId"),
                                        });
                                Log.d("updated", jsonObject.optString("date"));
                            }

                        }
                    }


                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("errorEvalvolley", volleyError.toString());


                    if (sharedPreferences.getBoolean("toastVisibility", false))
                        Config.myToast(context, "homework", "Homework Not Send \n" +
                                "Some Error Occurred \n" +
                                "Try Again Later ....!! ", Gravity.TOP);

                }
            });

//        Toast.makeText(context, newtext JSONObject(param).toString(),Toast.LENGTH_LONG).show();
            string_req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);

        }
        //   db.close();
    }

    public void sendAttendanceToServer() throws JSONException {

        Cursor cursorAllSection = getDbReadObject().rawQuery("Select * from StudentAttendance where  " +
                "sending_status=? ORDER BY section_id  ASC", new String[]{"Pending"});

        if (cursorAllSection != null && cursorAllSection.getCount() > 0) {

            String sec = "";
            JSONArray json_array_section = new JSONArray();
            while (cursorAllSection.moveToNext()) {
               /* Log.e("row-1 : ", cursorAllSection.getString(cursorAllSection.getColumnIndex("section_id")) +
                        " " +
                        cursorAllSection.getString(cursorAllSection.getColumnIndex("date")) +
                        " " +
                        cursorAllSection.getString(cursorAllSection.getColumnIndex("student_id")) +
                        " " +
                        cursorAllSection.getString(cursorAllSection.getColumnIndex("subjectId")) +
                        " " +
                        cursorAllSection.getString(cursorAllSection.getColumnIndex("present_status"))


                );*/

                JSONArray json_array_date = new JSONArray();
                if (sec.equalsIgnoreCase(cursorAllSection.getString(cursorAllSection.getColumnIndex("section_id"))))
                    ;
                else {

                    sec = cursorAllSection.getString(cursorAllSection.getColumnIndex("section_id"));

                    Cursor cursorDateSub = getDbReadObject().rawQuery("Select * from StudentAttendance where " +
                                    "section_id=? AND sending_status=? ",
                            new String[]{sec, "Pending"});
                    String dat = "";
                    String subNameId = "NOTFound";
                    if (cursorDateSub != null && cursorDateSub.getCount() > 0) {
                        while (cursorDateSub.moveToNext()) {


                            if (dat.equalsIgnoreCase(cursorDateSub.getString(cursorDateSub.getColumnIndex("date"))) &&
                                    subNameId.equalsIgnoreCase(cursorDateSub.getString(cursorDateSub.getColumnIndex("subjectId"))))
                                ;
                            else {
                                JSONArray prasent_student_array = new JSONArray();
                                JSONArray apsent_student_array = new JSONArray();

                                dat = cursorDateSub.getString(cursorDateSub.getColumnIndex("date"));
                                subNameId = cursorDateSub.getString(cursorDateSub.getColumnIndex("subjectId"));

                                Cursor cursorPresent = getDbReadObject().rawQuery("Select * from StudentAttendance where " +
                                                "section_id=? AND date=? AND subjectId=? AND present_status=? " +
                                                "AND sending_status=?",
                                        new String[]{sec, dat, subNameId, "P", "Pending"});
                                if (cursorPresent != null && cursorPresent.getCount() > 0) {
                                    while (cursorPresent.moveToNext()) {
                                        prasent_student_array.put(cursorPresent.getString(cursorPresent.getColumnIndex("student_id")));
                                    }
                                    cursorPresent.close();
                                }
                                Cursor cursorAbsent = getDbReadObject().rawQuery("Select * from StudentAttendance where " +
                                                "section_id=? AND date=? AND subjectId=? AND present_status=? AND sending_status=?",
                                        new String[]{sec, dat, subNameId, "A", "Pending"});
                                if (cursorAbsent != null && cursorAbsent.getCount() > 0) {
                                    while (cursorAbsent.moveToNext()) {
                                        apsent_student_array.put(cursorAbsent.getString(cursorAbsent.getColumnIndex("student_id")));
                                    }
                                    cursorAbsent.close();
                                }
                                Map<String, Object> date_map = new LinkedHashMap<>();
                                date_map.put("Date", dat);
                                date_map.put("subjectId", subNameId);
                                date_map.put("PresentStudentId", prasent_student_array);
                                date_map.put("AbsentStudentId", apsent_student_array);

                                json_array_date.put(new JSONObject(date_map));

                            }

                        }
                        cursorDateSub.close();
                    }
                    Map<String, Object> section_map = new LinkedHashMap<>();
                    section_map.put("SectionId", sec);
                    section_map.put("SectionData", json_array_date);

                    json_array_section.put(new JSONObject(section_map));
                }


            }
            cursorAllSection.close();

            // SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
            String db_name = sharedPreferences.getString("organization_name", "Not Found");

            Map<String, Object> school_map = new LinkedHashMap<>();
            school_map.put("SchoolAttendance", json_array_section);

            final JSONObject param = new JSONObject();
            //  param.put("DB_Name", db_name);
            param.put("userName", sharedPreferences.getString("loggedUserID", "")/*+"-"+
                sharedPreferences.getString("loggedUserName", "")+"-"+
                sharedPreferences.getString("user_type", "")*/);
            param.put("session", sharedPreferences.getString("session", ""));
            param.put("SchoolData", new JSONObject(school_map));

            //   RequestQueue rqueue = Volley.newRequestQueue(context);

            String attUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    +
                    "StudentAttendanceApi.php?action=attendanceInsert&databaseName=" + db_name;
            Log.d("AttendanceUrl", attUrl);
            Log.d("param", param.toString());


            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, attUrl, param,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());

                            JSONObject resJson = jsonObject;
                            if (resJson.optString("code").equalsIgnoreCase("201")) {
                                JSONArray jarr = null;
                                try {
                                    jarr = new JSONArray(resJson.optString("result"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                for (int i = 0; i < jarr.length(); i++) {
                                    JSONObject job = jarr.optJSONObject(i);

                                    if (job.optString("result").equalsIgnoreCase("error")) {
                                        if (sharedPreferences.getBoolean("toastVisibility", false))
                                            Config.myToast(context, "ic_attendance", "Attendance Not Uploaded\n please Retry to send", Gravity.TOP);
                                    } else {
                                        ContentValues cv = new ContentValues();
                                        cv.put("sending_status", "Sent");

                                        getDbWriteObject().update("StudentAttendance", cv, "date=? AND subjectId=? " +
                                                        "AND sending_status=?",
                                                new String[]{job.optString("onDate"), job.optString("subjectId"), "Pending"});

                                    }


                                }
                                if (jarr.length() > 0) {
                                    if (sharedPreferences.getBoolean("toastVisibility", false))
                                        Config.myToast(context, "ic_attendance", "Attendance Uploaded Successfully", Gravity.TOP);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            if (sharedPreferences.getBoolean("toastVisibility", false))
                                Config.myToast(context, "ic_attendance", "Attendance Not Send \n" +
                                        "Some Error Occurred \n" +
                                        "Try Again Later ....!! ", Gravity.TOP);
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(context).addToRequestQueue(jsonObjectRequest);
        }
        //  cu.close();
        // db.close();
    }

    public void sendExamResultToServer() throws JSONException {
        //  String send_url = "http://junctiondev.cloudapp.net/sms/android/examResult.php?action=insert";
        // SQLiteDatabase db = super.getReadableDatabase();

        //  SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
        String db_name = sharedPreferences.getString("organization_name", "Not Found");
        String evaluated_by = "";
        if (sharedPreferences.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sharedPreferences.getString("user_type", "").equalsIgnoreCase("admin"))
            evaluated_by = "0"; /* 0 uses for admin*/
        else evaluated_by = sharedPreferences.getString("loggedUserID", "Not Found");

        JSONArray jsonarray = new JSONArray();

        Cursor cu = getDbReadObject().rawQuery("Select * from ExamResult where sending_Status=?", new String[]{"Pending"});
        if (cu != null && cu.getCount() > 0) {
            while (cu.moveToNext()) {
                JSONObject map = new JSONObject();
                map.put("examName", cu.getString(cu.getColumnIndex("examName")));
                map.put("section_id", cu.getString(cu.getColumnIndex("section_id")));
                map.put("student_id", cu.getString(cu.getColumnIndex("student_id")));
                map.put("session", sharedPreferences.getString("session", ""));
                map.put("subjectid", cu.getString(cu.getColumnIndex("subjectid")));
                map.put("obtained_marks", cu.getString(cu.getColumnIndex("obtained_marks")));
                map.put("maximum_marks", cu.getString(cu.getColumnIndex("maximum_marks")));
                map.put("resultID", cu.getString(cu.getColumnIndex("result")));
                map.put("grade", cu.getString(cu.getColumnIndex("grade")));
                map.put("exam_date", cu.getString(cu.getColumnIndex("date")));
                map.put("evaluated_by", evaluated_by);

                jsonarray.put(map);
            }


            Map<String, Object> school_map = new LinkedHashMap<>();
            school_map.put("SchoolResult", jsonarray);

            final JSONObject param = new JSONObject();
            //param.put("DB_Name", db_name);
            param.put("session", sharedPreferences.getString("session", ""));
            param.put("SchoolData", new JSONObject(school_map));

            // RequestQueue rqueue = Volley.newRequestQueue(context);
            String testUrl = " http://192.168.1.161/apischoolerp/ExamDetails.php?action=insert";

            String examUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    +
                    "ExamApi.php?action=examInsert&databaseName=" + db_name;
            Log.d("ExamUrl", examUrl);
            Log.d("param", param.toString());

            JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST,
                    examUrl, param
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {

                    // Toast.makeText(context, s, Toast.LENGTH_LONG).show();
                    Log.e("examResponse", s.toString());

                    JSONObject resJson = s;
                    if (resJson.optString("code").equalsIgnoreCase("201")) {
                        JSONArray jar = null;
                        try {
                            jar = new JSONArray(resJson.optString("result"));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                        ContentValues cv = new ContentValues();

                        for (int i = 0; i < jar.length(); i++) {
                            JSONObject job = jar.optJSONObject(i);

                            cv.put("sending_Status", "Sent");

                            if (!job.optString("result").equalsIgnoreCase("error")) {
                                getDbWriteObject().update("ExamResult", cv, "examName=? AND subjectid=? AND student_id=? AND date=?",
                                        new String[]{job.optString("examName"), job.optString("subjectid"), job.optString("student_id"),
                                                job.optString("examDate")});
                            }
                        }

                        if (jar.length() > 0) {
                            if (sharedPreferences.getBoolean("toastVisibility", false))
                                Config.myToast(context, "result", "Exam Result Uploaded Successfully", Gravity.TOP);
                        }


                    }


                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    //  Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                    if (sharedPreferences.getBoolean("toastVisibility", false))
                        Config.myToast(context, "result", "Exam Result Not Send \\nSome Error Occurred \\nTry Again Later ....!! ", Gravity.TOP);
                }
            });

//        Toast.makeText(context, newtext JSONObject(param).toString(),Toast.LENGTH_LONG).show();
            string_req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);


            cu.close();
        }
        //  db.close();

    }

    public static void longInfo(String str) {
        if (str.length() > 4000) {
            Log.i("JSON_Format", str.substring(0, 4000));
           // longInfo(str.substring(4000));
        } else
            Log.i("JSON_Format", str);
    }

    public Bundle getDataForStatus(int which_table) {
        //  SQLiteDatabase db = super.getReadableDatabase();
        String[][] returnData = new String[][]{};

        ArrayList<String> arr_cls = new ArrayList<String>();
        ArrayList<String> arr_sec = new ArrayList<String>();
        ArrayList<String> arr_date = new ArrayList<String>();
        ArrayList<String> arr_status = new ArrayList<String>();
        Bundle b = new Bundle();
        switch (which_table) {
            case 0:/* 0 for attendance*/
                Cursor cc = getDbReadObject().rawQuery("Select * from StudentAttendance,School_Class,Section_Name" +
                        " where School_Class.class_id=Section_Name.class_id AND " +
                        " Section_Name.section_id=StudentAttendance.section_id ORDER BY date ASC", null);
                if (cc != null && cc.getCount() > 0) {
                    String cls = "";
                    String sec = "";
                    String dat = "";
                    String status = "";

                    while (cc.moveToNext()) {

                        if (cls.equalsIgnoreCase(cc.getString(cc.getColumnIndex("class_id"))) && sec.equalsIgnoreCase(cc.getString(cc.getColumnIndex("section_id")))
                                && dat.equalsIgnoreCase(cc.getString(cc.getColumnIndex("date"))) && status.equalsIgnoreCase(cc.getString(cc.getColumnIndex("sending_status"))))
                            ;
                        else {
                            cls = cc.getString(cc.getColumnIndex("class_id"));
                            sec = cc.getString(cc.getColumnIndex("section_id"));
                            dat = cc.getString(cc.getColumnIndex("date"));
                            status = cc.getString(cc.getColumnIndex("sending_status"));

                            arr_cls.add(cc.getString(cc.getColumnIndex("class_no")));
                            arr_sec.add(cc.getString(cc.getColumnIndex("section")));
                            arr_date.add(dat);
                            arr_status.add(status);

                        }
                    }


                    cc.close();
                }
                break;

            case 1:/* 0 for homework*/

                Cursor cc1 = getDbReadObject().rawQuery("Select * from School_Class,Homework,Section_Name where Homework.class_id=School_Class.class_id AND Section_Name.section_id=Homework.section_id ORDER BY date ASC", null);

                // int i = 0;

                if (cc1 != null && cc1.getCount() > 0) {
                    while (cc1.moveToNext()) {
                        if (arr_cls.contains(cc1.getString(cc1.getColumnIndex("class_no"))) &&
                                arr_sec.contains(cc1.getString(cc1.getColumnIndex("section"))) &&
                                arr_date.contains(cc1.getString(cc1.getColumnIndex("date")))
                                && arr_status.contains(cc1.getString(cc1.getColumnIndex("sending_work_status")))
                                ) ;
                        else {
                            arr_cls.add(cc1.getString(cc1.getColumnIndex("class_no")));
                            arr_sec.add(cc1.getString(cc1.getColumnIndex("section")));
                            arr_date.add(cc1.getString(cc1.getColumnIndex("date")));
                            arr_status.add(cc1.getString(cc1.getColumnIndex("sending_work_status")));


                        }

                    }
                    cc1.close();
                }

                break;

            case 2:/* 0 for exam result*/
                Cursor cc2 = getDbReadObject().rawQuery("Select * from School_Class,ExamResult,Section_Name where ExamResult.class_id=School_Class.class_id AND Section_Name.section_id=ExamResult.section_id ORDER BY ExamResult.date ASC", null);
                if (cc2 != null && cc2.getCount() > 0) {
                    //  int i2 = 0;
                    String exm = "";
                    String sec2 = "";
                    String cls2 = "";
                    String dat2 = "";
                    String status2 = "";

                    while (cc2.moveToNext()) {
                        if (exm.equalsIgnoreCase(cc2.getString(cc2.getColumnIndex("examName"))) && cls2.equalsIgnoreCase(cc2.getString(cc2.getColumnIndex("class_id"))) && sec2.equalsIgnoreCase(cc2.getString(cc2.getColumnIndex("section_id")))
                                && dat2.equalsIgnoreCase(cc2.getString(cc2.getColumnIndex("date"))) && status2.equalsIgnoreCase(cc2.getString(cc2.getColumnIndex("sending_Status"))))
                            ;
                        else {

                            exm = cc2.getString(cc2.getColumnIndex("examName"));
                            sec2 = cc2.getString(cc2.getColumnIndex("section_id"));
                            cls2 = cc2.getString(cc2.getColumnIndex("class_id"));
                            dat2 = cc2.getString(cc2.getColumnIndex("date"));
                            status2 = cc2.getString(cc2.getColumnIndex("sending_Status"));

                            arr_cls.add(exm);
                            arr_sec.add(cc2.getString(cc2.getColumnIndex("class_no")) + " " + cc2.getString(cc2.getColumnIndex("section")));
                            arr_date.add(dat2);
                            arr_status.add(status2);
                            // i2++;
                        }


                    }

                    cc2.close();
                }
                //   returnData = new String[][]{examName, sec_value2, date_value2, status_value2};
                break;

            case 3:/* 0 for feedback or teacher's notes*/
                Cursor cc3 = getDbReadObject().rawQuery("Select * from Feedback order by date ASC", null);

                if (cc3 != null && cc3.getCount() > 0) {
                    //   int i3 = 0;
                    Log.e("feedbackLength", cc3.getCount() + "");
                    while (cc3.moveToNext()) {
                        String[] stId = cc3.getString(cc3.getColumnIndex("student_id")).split("_");
                        Cursor c = getDbReadObject().rawQuery("Select * from Student_Record,School_Class,Section_Name where student_id=? AND Student_Record.class_id=School_Class.class_id AND Student_Record.section_id=Section_Name.section_id",
                                new String[]{stId[1]});
                        if (c != null && c.getCount() > 0) {
                            if (c.moveToNext()) {
//                        cls_value3[i3] = c.getString(c.getColumnIndex("student_name"));
//                        sec_value3[i3] = c.getString(c.getColumnIndex("class_no")) + " " + c.getString(c.getColumnIndex("section"));
                                String a[] = cc3.getString(cc3.getColumnIndex("date")).split(" ");
//                        date_value3[i3] = a[0] + " " + a[1] + " " + a[2];
//                        status_value3[i3] = cc3.getString(cc3.getColumnIndex("status"));

                                arr_cls.add(c.getString(c.getColumnIndex("student_name")));
                                arr_sec.add(c.getString(c.getColumnIndex("class_no")) + " " + c.getString(c.getColumnIndex("section")));
                                arr_date.add(a[0] + " " + a[1] + " " + a[2]);
                                arr_status.add(cc3.getString(cc3.getColumnIndex("status")));
                            }
                            c.close();
                        }
                    }
                    cc3.close();
                } //  returnData = new String[][]{cls_value3, sec_value3, date_value3, status_value3};

                break;

        }
        b.putStringArrayList("class", arr_cls);
        b.putStringArrayList("sec", arr_sec);
        b.putStringArrayList("date", arr_date);
        b.putStringArrayList("status", arr_status);
        return b;
    }

    public void saveHomeWork(String cls_selected_id, String section_selected_id, String dates, String sub_id, String work) {
        //  SQLiteDatabase db = super.getWritableDatabase();
//        for (int i = 0; i < dates.length; i++) {
        ContentValues cv = new ContentValues();

        Cursor csub = getDbReadObject().rawQuery("Select * from Subjects where subjectid=?", new String[]{sub_id});
        if (csub != null && csub.getCount() > 0) {
            csub.moveToNext();
            String sub_name = csub.getString(csub.getColumnIndex("subjectname"));

            cv.put("class_id", cls_selected_id);
            cv.put("section_id", section_selected_id);
            cv.put("date", dates);
            cv.put("subjectid", sub_id);
            cv.put("subjectname", sub_name);
            cv.put("work", work);
            cv.put("homeworkEvaluation", "");
            cv.put("sending_work_status", "Pending");


            Cursor cs = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=? AND subjectid=?",
                    new String[]{cls_selected_id, section_selected_id, dates, sub_id});

            if (cs != null && cs.moveToNext()) {

                int aa = getDbWriteObject().update("Homework", cv, "class_id=? AND section_id=? AND date=? AND subjectid=?",
                        new String[]{cls_selected_id, section_selected_id, dates, sub_id});

//            Toast.makeText(context, "updation done in  " + aa + "   " + dates + " " + sub_name, Toast.LENGTH_SHORT).show();
                cs.close();
            } else {
                if ((getDbWriteObject().insert("Homework", null, cv) == -1)) {
                    if (sharedPreferences.getBoolean("toastVisibility", false))
                        Toast.makeText(context, "error in insertion Homewrok  " + dates, Toast.LENGTH_SHORT).show();
                }
//
// Toast.makeText(context, "inserted in  " + dates + " " + sub_name, Toast.LENGTH_SHORT).show();

            }


            csub.close();
        }
//        }
        //db.close();

        //  SharedPreferences sp = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("typeReq", "homework");
        editor.commit();


        try {
            sendData("homework");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //    context.startService(new Intent(context, NetworkCheckLisener.class));

    }

    public boolean saveSubjectIntoDatabase(String class_id, String section_id, String[] sub_id, String[] sub_name) {
        // SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SSSSSSSSS", "Subject");
        ContentValues cv = new ContentValues();

        for (int i = 0; i < sub_id.length; i++) {
            cv.put("class_id", class_id);
            cv.put("section_id", section_id);
            cv.put("subjectid", sub_id[i]);
            cv.put("subjectname", sub_name[i]);

            Cursor cc = getDbReadObject().rawQuery("Select * from Subjects where class_id=? AND section_id=? AND subjectid=?",
                    new String[]{class_id, section_id, sub_id[i]});

            if (cc != null) {
                if (!cc.moveToNext()) {
                    if (getDbWriteObject().insert("Subjects", null, cv) == -1) {
                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Toast.makeText(context, "error in subject insertion", Toast.LENGTH_SHORT).show();
                    }
                } else
                    getDbWriteObject().update("Subjects", cv, "class_id=? AND section_id=? AND subjectid=?", new String[]{class_id, section_id, sub_id[i]});

                cc.close();
            }
        }

        //  db.close();
        return true;
    }

    public String[][] getSubjectsClasswise(String cls_selected_id, String section_selected_id) {
        //   SQLiteDatabase db = super.getReadableDatabase();

        Cursor cs = getDbReadObject().rawQuery("Select * from Subjects where class_id=? AND section_id=?",
                new String[]{cls_selected_id, section_selected_id});
        String[] subid = new String[cs.getCount()];
        String[] subname = new String[cs.getCount()];
        int pos = 0;

        if (cs != null) {
            while (cs.moveToNext()) {
                subid[pos] = cs.getString(cs.getColumnIndex("subjectid"));
                subname[pos] = cs.getString(cs.getColumnIndex("subjectname"));
                pos++;
            }
            cs.close();
        }  // db.close();
        String[][] abc = new String[][]{subid, subname};
        return abc;
    }

    public Bundle getHomeworkForAddWork(String section_selected_id, String date_of_work, String[] sub_id, String[] sub_name) {
        //    SQLiteDatabase db = super.getReadableDatabase();
        List<StudentData> listOfHomework = new ArrayList<StudentData>();
        ArrayList<Integer> leftIndex = new ArrayList<>();
        for (int i = 0; i < sub_id.length; i++) {
            Cursor cs = getDbReadObject().rawQuery("Select * from Homework where section_id=? AND date=? AND subjectid=?",
                    new String[]{section_selected_id, date_of_work, sub_id[i]});
            if (cs != null) {
                if (cs.moveToNext()) {
                    listOfHomework.add(new StudentData(cs.getString(cs.getColumnIndex("work")),
                            sub_name[i], sub_id[i]));

                } else {
                    leftIndex.add(i);
                }
                cs.close();
            }
        }

        Bundle b = new Bundle();
        b.putSerializable("listOfHomework", (Serializable) listOfHomework);
        b.putIntegerArrayList("leftIndex", leftIndex);
        return b;

    }


    public void deleteData(boolean att, boolean home, boolean result, boolean msg) {
        //    SQLiteDatabase db = super.getReadableDatabase();
        if (att) {
            int aa = 0;
            aa = aa + getDbWriteObject().delete("StudentAttendance", "sending_status=?", new String[]{"Sent"});
            if (sharedPreferences.getBoolean("toastVisibility", false))
                Toast.makeText(context, aa + " Attendance deleted", Toast.LENGTH_LONG).show();
        }

        if (home) {
            int aa = 0;
            aa = aa + getDbWriteObject().delete("Homework", "sending_work_status=?", new String[]{"Sent"});
            if (sharedPreferences.getBoolean("toastVisibility", false))
                Toast.makeText(context, aa + " homework deleted", Toast.LENGTH_LONG).show();
        }

        if (msg) {
            int aa = 0, ag = 0, ar = 0;
            aa = aa + getDbWriteObject().delete("Message", null, null);
            ag = aa + getDbWriteObject().delete("GroupMsg", null, null);
            ar = aa + getDbWriteObject().delete("GroupRecord", null, null);
            Log.e("msg", ag + "  " + ar + " deleted");


            if (sharedPreferences.getBoolean("toastVisibility", false))
                Toast.makeText(context, aa + " Message deleted", Toast.LENGTH_LONG).show();
        }

        if (result) {
            int aa = 0;
            aa = aa + getDbWriteObject().delete("ExamResult", "sending_Status=?", new String[]{"Sent"});
            if (sharedPreferences.getBoolean("toastVisibility", false))
                Toast.makeText(context, aa + " ExamResult deleted", Toast.LENGTH_LONG).show();
        }


        //  db.close();
    }

    public void saveExamList(String[] exam_type_array) {
        //    SQLiteDatabase db = super.getWritableDatabase();
        ContentValues cv = new ContentValues();

        for (int i = 0; i < exam_type_array.length; i++) {
            cv.put("examName", exam_type_array[i]);


            if ((getDbWriteObject().insert("ExamType", null, cv) == -1)) {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, "error in insertion examType ", Toast.LENGTH_SHORT).show();
            }

        }

    }

    public String[] getExamType() {
        //  SQLiteDatabase db = super.getWritableDatabase();
        Cursor cc = getDbReadObject().rawQuery("Select * from ExamType", null);
        String[] examName = new String[cc.getCount()];

        int i = 0;
        if (cc != null) {
            while (cc.moveToNext()) {
                examName[i] = cc.getString(cc.getColumnIndex("examName"));
                i++;
            }
            cc.close();
        }
        // db.close();
        return examName;
    }

    public void savePreviousExamResultDataFromServer(ArrayList<PreviousResultData> resultData) {


        try {
            SQLiteDatabase db = super.getWritableDatabase();
            Cursor cursor = null;
            for (PreviousResultData obj : resultData) {
                ContentValues cv = new ContentValues();

                cv.put("examName", obj.getExam_Type() + "");
                cv.put("class_id", getClassIdViaSectionID(obj.getSection_Id()));
                cv.put("section_id", obj.getSection_Id() + "");
                cv.put("subjectid", obj.getSubject_Id() + "");
                cv.put("student_id", obj.getStudent_Id() + "");
                // cv.put("student_name", obj.getExam_Type());
                cv.put("grade", obj.getGrade() + "");
                cv.put("obtained_marks", obj.getMarks_Obtain() + "");
                cv.put("maximum_marks", obj.getMax_Marks() + "");
                cv.put("sending_Status", "Sent");
                cv.put("date", obj.getDateOfExam() + "");
                cv.put("result", obj.getResult() + "");

                try {
                    cursor = db.rawQuery("Select * from ExamResult where examName=?" +
                                    " AND subjectid=? AND student_id=? AND date=?",
                            new String[]{obj.getExam_Type(), obj.getSubject_Id(), obj.getStudent_Id(), obj.getDateOfExam()});


                    if (cursor.moveToFirst())
                        db.update("ExamResult", cv, "examName=? AND subjectid=? AND student_id=? AND date=?",
                                new String[]{obj.getExam_Type(), obj.getSubject_Id(), obj.getStudent_Id(), obj.getDateOfExam()});
                    else if ((db.insert("ExamResult", null, cv) == -1)) {
                        //Log.d("examSaved", "error in insertion ExamResult ");
                    }
                } finally {
                    if (cursor != null)
                        cursor.close();
                }


                //   cursor.close();
            }
            db.close();
        } catch (SQLException s) {
            Log.e("ERROR", "Error with DB Open");
            new Exception("Error with DB Open");
        }


    }

    private String getClassIdViaSectionID(String section_id) {
        Cursor cc = getDbReadObject().rawQuery("Select class_id from Section_Name where section_id=?",
                new String[]{section_id});

        if (cc != null && cc.moveToNext()) {
            return cc.getString(cc.getColumnIndex("class_id"));
        } else return "";
    }

    public void saveExamResult(String selected_examtype, String selected_class_id, String selected_section,
                               String selected_subject, String maximum_marks, String examDate, StudentData obj) {
        //  SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SaveExamResult", "Save");

        Log.e("examName", selected_examtype);
        Log.e("section_id", selected_section);
        Log.e("subjectid", selected_subject);
        Log.e("date", examDate);

        ContentValues cv = new ContentValues();

        cv.put("examName", selected_examtype);
        cv.put("class_id", selected_class_id);
        cv.put("section_id", selected_section);
        cv.put("subjectid", selected_subject);
        cv.put("student_id", obj.getStudentID());
        cv.put("student_name", obj.getStudentName());
        cv.put("grade", obj.getStudentGrade());
        cv.put("obtained_marks", obj.getStudentMarks());
        cv.put("maximum_marks", maximum_marks);
        cv.put("sending_Status", "Pending");
        cv.put("date", examDate);
        cv.put("result", obj.getStudentResult());

        Cursor cc = getDbReadObject().rawQuery("Select * from ExamResult where examName=?" +
                        " AND subjectid=? AND student_id=? AND date=?",
                new String[]{selected_examtype, selected_subject, obj.getStudentID(), examDate});

        if (cc != null) {
            if (cc.moveToNext())
                getDbWriteObject().update("ExamResult", cv, "examName=? AND subjectid=? AND student_id=? AND date=?",
                        new String[]{selected_examtype, selected_subject, obj.getStudentID(), examDate});
            else if ((getDbWriteObject().insert("ExamResult", null, cv) == -1)) {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, "error in insertion ExamResult ", Toast.LENGTH_SHORT).show();

            } // else Log.d("exam marks saved", obj.getStudentMarks());
            cc.close();
        } //  db.close();


        //   SharedPreferences sp = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("typeReq", "result");
        editor.commit();

        try {
            sendData("result");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // context.startService(new Intent(context, NetworkCheckLisener.class));

    }


    public ArrayList<StudentData> getExamResult(String selected_examtype, String selected_subject, String[] studentID,
                                                String examDate) {
        //  SQLiteDatabase db = super.getReadableDatabase();

        int st_length = studentID.length;
        ArrayList<StudentData> studentDatas = new ArrayList<StudentData>(st_length);
        for (int i = 0; i < st_length; i++) {
            Cursor cc = getDbReadObject().rawQuery("Select * from ExamResult,Student_Record where Student_Record.student_id=ExamResult.student_id AND examName=? AND subjectid=? AND ExamResult.student_id=?  AND date=?",
                    new String[]{selected_examtype, selected_subject, studentID[i], examDate});

            if (cc != null) {
                if (cc.moveToNext()) {
                    studentDatas.add(new StudentData(studentID[i], cc.getString(cc.getColumnIndex("student_name")),
                            cc.getString(cc.getColumnIndex("grade")), cc.getString(cc.getColumnIndex("obtained_marks")),
                            cc.getString(cc.getColumnIndex("result")), cc.getString(cc.getColumnIndex("maximum_marks"))));

                } else {
                    Cursor c1 = getDbReadObject().rawQuery("Select * from Student_Record where student_id=?", new String[]{studentID[i]});
                    if (c1 != null) {
                        if (c1.moveToNext())
                            studentDatas.add(new StudentData(studentID[i], c1.getString(c1.getColumnIndex("student_name")),
                                    "", "", "", "0"));

                        c1.close();
                    }
                }
                cc.close();
            }
        }

        //  db.close();
        return studentDatas;
    }

    public String getMaximumMarks(String selected_examtype, String selected_subject,
                                  String selected_class_id, String selected_section, String examDate) {
        // SQLiteDatabase db = super.getReadableDatabase();
        Cursor cc = getDbReadObject().rawQuery("Select * from ExamResult where  examName=? AND " +
                        "subjectid=? AND class_id=? AND section_id=?  AND date=?",
                new String[]{selected_examtype, selected_subject, selected_class_id, selected_section, examDate});

        String max = "";
        if (cc != null) {
            if (cc.moveToNext()) {
                max = cc.getString(cc.getColumnIndex("maximum_marks"));
            }
            if (max.equalsIgnoreCase("") || max == null)
                max = "";

            cc.close();
        }  //  db.close();
        return max;
    }

    public String[][] getResultList() {
        //  SQLiteDatabase db = super.getWritableDatabase();
        Cursor cc = getDbReadObject().rawQuery("Select * from ResultList", null);
        String[] resultID = new String[cc.getCount()];
        String[] resultName = new String[cc.getCount()];

        int i = 0;
        if (cc != null) {
            while (cc.moveToNext()) {
                resultID[i] = cc.getString(cc.getColumnIndex("ResultID"));
                resultName[i] = cc.getString(cc.getColumnIndex("ResultName"));
                i++;
            }
            cc.close();
        }  // db.close();
        return new String[][]{resultID, resultName};

    }

    public String[][] getSubjectListForParent() {
        // SQLiteDatabase db = super.getReadableDatabase();
        Cursor c1 = getDbReadObject().rawQuery("Select * from ParentSubject", null);
        String[] subjectID = new String[c1.getCount()];
        String[] subjectName = new String[c1.getCount()];
        int i = 0;

        if (c1 != null) {
            while (c1.moveToNext()) {
                subjectID[i] = c1.getString(c1.getColumnIndex("subjectid"));
                subjectName[i] = c1.getString(c1.getColumnIndex("subjectname"));
                i++;
            }

            c1.close();
        }  //   db.close();
        return new String[][]{subjectID, subjectName};
    }

    public void saveSubjectForParent(String[] sub_id, String[] sub_name) {
        // SQLiteDatabase db = super.getWritableDatabase();
        Log.e("SSSSSSSSS", "SubjectParent");
        ContentValues cv = new ContentValues();

        for (int i = 0; i < sub_id.length; i++) {
            cv.put("subjectid", sub_id[i]);
            cv.put("subjectname", sub_name[i]);

            if ((getDbWriteObject().insert("ParentSubject", null, cv) == -1)) {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, "error in parent subject insertion", Toast.LENGTH_SHORT).show();
            }

        }


    }

    public void saveSessionReview(String[] stID, String feedback, String date, String feedbackOf) {
        //  SQLiteDatabase db = super.getWritableDatabase();

        for (int i = 0; i < stID.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put("student_id", feedbackOf + "_" + stID[i]);
            cv.put("ic_feedback", feedback);
            cv.put("date", date);
            cv.put("status", "pending");

            if ((getDbReadObject().insert("Feedback", null, cv) == -1)) {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, "error in insertion review", Toast.LENGTH_SHORT).show();
            } else {
                if (sharedPreferences.getBoolean("toastVisibility", false))
                    Toast.makeText(context, " inserted review ", Toast.LENGTH_SHORT).show();
            }
        }

        try {
            sendFeedback();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


   /* public List<MessageData> getAllFeedback(String studentID) {
        //  SQLiteDatabase db = super.getReadableDatabase();
        Cursor cc = getDbReadObject().rawQuery("Select * from Feedback where student_id=?", new String[]{studentID});
        ArrayList<MessageData> list = new ArrayList<>();
        String date_pre = "";

        if (cc != null) {
            while (cc.moveToNext()) {
                String date_arr[] = cc.getString(cc.getColumnIndex("date")).split(" ");

                if (date_pre.equalsIgnoreCase(date_arr[1] + " " + date_arr[0] + " " + date_arr[2])) {
                    list.add(new MessageData(cc.getString(cc.getColumnIndex("ic_feedback")),
                            cc.getString(cc.getColumnIndex("date")),
                            sharedPreferences.getString("loggedUserName", "Not Found"),
                            cc.getString(cc.getColumnIndex("status")),
                            date_arr[1] + " " + date_arr[0] + " " + date_arr[2],
                            false));
                } else {
                    list.add(new MessageData(cc.getString(cc.getColumnIndex("ic_feedback")),
                            cc.getString(cc.getColumnIndex("date")),
                            sharedPreferences.getString("loggedUserName", "Not Found"),
                            cc.getString(cc.getColumnIndex("status")),
                            date_arr[1] + " " + date_arr[0] + " " + date_arr[2],
                            true));

                    date_pre = date_arr[1] + " " + date_arr[0] + " " + date_arr[2];
                }


            }
            cc.close();
        }
        return list;

    }*/


    synchronized public boolean getDataFromServer() {
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING));
                final String loggedUserType = sharedPreferences.getString("user_type", "Not Found");


                String db_name = sharedPreferences.getString("organization_name", "Not Found");
                final JSONObject param = new JSONObject();
                try {
                    param.put("databaseName", db_name);
                } catch (JSONException e) {
                    e.printStackTrace();
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                }
                try {
                    param.put("session", sharedPreferences.getString("session", ""));
                } catch (JSONException e) {
                    e.printStackTrace();
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                }

                try {
                    param.put("databaseName", db_name);
                } catch (JSONException e) {
                    e.printStackTrace();
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                }
                if (sharedPreferences.getBoolean(Config.ADMIN_DATA_FETCHED, true))
                    deleteDatabase(getDbReadObject());
                if (loggedUserType.equalsIgnoreCase("Teacher"))
                    try {
                        param.put("userType", "staff");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                    }
                else if (loggedUserType.equalsIgnoreCase("Parent")
                        ||
                        loggedUserType.equalsIgnoreCase("Student"))
                    try {
                        param.put("userType", "student");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                    }
                else if (loggedUserType.equalsIgnoreCase("Administrator")) {
                    try {
                        param.put("userType", "Administrator");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                    }

                }

                try {
                    param.put("userId", sharedPreferences.getString("loggedUserID", "Not Found"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                }
                /*if (!sharedPreferences.getString("user_type", "").equalsIgnoreCase(""))
                    Toast.makeText(context, context.getString(R.string.fetching_data)
                        , Toast.LENGTH_LONG).show();
*/

                String getDetailsUrl = sharedPreferences.getString("HostName", "Not Found")
                        + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                        + "GetSchoolDetailsApi.php";

       /* Log.e("url", sharedPreferences.getString("HostName", "Not Found") +
                "GetDetails.php?action=get");*/

                Log.e("url", getDetailsUrl);
                Log.e("param", param.toString());

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, getDetailsUrl, param,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject jsonObject) {
                                DbHandler.longInfo(jsonObject.toString());

                                if (sharedPreferences.getBoolean(Config.ADMIN_DATA_FETCHED, true)) {
                                    sharedPreferences.edit().putBoolean(Config.ADMIN_DATA_FETCHED, false).commit();
                                }

                                JSONObject resJson = jsonObject;
                                if (resJson.optString("code").equalsIgnoreCase("200")) {

                                    /********************   Truncate The table *****************/
                                    getDbWriteObject().delete("Subjects", null, null);
                                    getDbWriteObject().delete("ParentSubject", null, null);
                                    getDbWriteObject().delete("Student_Record", null, null);
                                    getDbWriteObject().delete("ExamType", null, null);
                                    getDbWriteObject().delete("TeacherList", null, null);
                                    getDbWriteObject().delete("ResultList", null, null);
                                    getDbWriteObject().delete("Section_Name", null, null);
                                    getDbWriteObject().delete("School_Class", null, null);
                                    /*************************************/


                                    JSONObject main_json_obj = resJson.optJSONObject("result");
                                    JSONArray main_json_array = main_json_obj.optJSONArray("school");
                                    String class_name, class_id = null;

                    /*--------------  Saving Exam Type List   ----------------- */

                                    if (main_json_obj.optJSONArray("Exam_type") != null) {
                                        JSONArray jsonArray_examlist = null;
                                        try {
                                            jsonArray_examlist = main_json_obj.getJSONArray("Exam_type");
                                            String[] exam_type_array = new String[jsonArray_examlist.length()];
                                            for (int i = 0; i < jsonArray_examlist.length(); i++) {
                                                exam_type_array[i] = jsonArray_examlist.getString(i);
                                            }
                                            saveExamList(exam_type_array);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }


                        /*--------------  Saving Teacher List   ----------------- */
                                    JSONArray jsonarryteacher = new JSONArray();
                                    jsonarryteacher = main_json_obj.optJSONArray("TeacherList");

                                    if (jsonarryteacher.length() > 0) {
                                        for (int i = 0; i < jsonarryteacher.length(); i++) {
                                            JSONObject job = jsonarryteacher.optJSONObject(i);

                                            String teacherID = job.optString("TeacherID");
                                            ContentValues cv = new ContentValues();
                                            cv.put("TeacherID", teacherID);
                                            cv.put("TeacherName", job.optString("TeacherName"));

                                            Cursor cTeacher = getDbReadObject().rawQuery("Select * from TeacherList where TeacherID=?",
                                                    new String[]{teacherID});
                                            if (cTeacher != null && cTeacher.moveToNext()) {
                                                getDbWriteObject().update("TeacherList", cv, "TeacherID=?", new String[]{teacherID});
                                                Log.e("Teacher","Update"+" "+job.optString("TeacherName"));
                                            } else if ((getDbWriteObject().insert("TeacherList", null, cv) == -1))
                                                Log.e("Teacher","interted"+" "+job.optString("TeacherName"));

                                        }
                                    }

                                        /*--------------  Saving UserStaffList     ----------------- */
                                    //  JSONArray jsonarryUserStaffList = new JSONArray();
                                    JSONArray jsonarryUserStaffList = main_json_obj.optJSONArray("UserListData");

                                    if (jsonarryUserStaffList.length() > 0) {
                                        for (int i = 0; i < jsonarryUserStaffList.length(); i++) {
                                            JSONObject job = jsonarryUserStaffList.optJSONObject(i);

                                            String teacherID = job.optString("TeacherID");
                                            ContentValues cv = new ContentValues();
                                            cv.put("TeacherID", teacherID);
                                            cv.put("TeacherName", job.optString("TeacherName"));
                                            cv.put("UserType", job.optString("UserType"));

                                            Cursor cTeacher = getDbReadObject().rawQuery("Select * from UserStaffList where TeacherID=?",
                                                    new String[]{teacherID});
                                            if (cTeacher != null && cTeacher.moveToNext()) {
                                                getDbWriteObject().update("UserStaffList", cv, "TeacherID=?", new String[]{teacherID});

                                            } else if ((getDbWriteObject().insert("UserStaffList", null, cv) == -1))
                                                ;

                                        }
                                    }
            /*--------------  Saving Result List   ----------------- */

                                    JSONArray jsonarryresult = main_json_obj.optJSONArray("ResultList");

                                    if (jsonarryresult.length() > 0) {
                                        for (int i = 0; i < jsonarryresult.length(); i++) {
                                            JSONObject job = jsonarryresult.optJSONObject(i);

                                            String resultID = job.optString("ResultID");
                                            ContentValues cv1 = new ContentValues();
                                            cv1.put("ResultID", resultID);
                                            cv1.put("ResultName", job.optString("ResultName"));

                                            Cursor cc = getDbReadObject().rawQuery("Select * from ResultList where ResultID=?",
                                                    new String[]{resultID});

                                            if (cc != null && cc.moveToNext())
                                                getDbWriteObject().update("ResultList", cv1, "ResultID=?", new String[]{resultID});
                                            else if ((getDbWriteObject().insert("ResultList", null, cv1) == -1))
                                                ;

                                        }
                                    }


                     /*--------------  Saving School class, section, subject, student   ----------------- */
                                    for (int a = 0; a < main_json_array.length(); a++) {

                                        JSONObject cls_jobj = main_json_array.optJSONObject(a);

                                        class_name = cls_jobj.optString("class");
                                        class_id = cls_jobj.optString("classid");


                                        saveClassIntoDataBase(class_name, class_id);

                                        JSONArray student_array = cls_jobj.optJSONArray("student");

                                        String section_name, section_id = null;

                                        for (int c = 0; c < student_array.length(); c++) {
                                            JSONObject class1_job = student_array.optJSONObject(c);

                                            section_name = class1_job.optString("section");
                                            section_id = class1_job.optString("sectionid");
                                            saveSectionIntoDataBase(class_id, section_id, section_name);

                                            try {
                                                if (class1_job.optJSONArray("student_name") != null) {
                                                    JSONArray student_name_array = class1_job.optJSONArray("student_name");
                                                    JSONArray student_id_array = class1_job.optJSONArray("student_id");
                                                    String[] name = new String[student_name_array.length()];
                                                    String[] id = new String[student_id_array.length()];

                                                    for (int d = 0; d < student_name_array.length(); d++) {

                                                        name[d] = student_name_array.getString(d);
                                                        id[d] = student_id_array.getString(d);

                                                        if (loggedUserType.equalsIgnoreCase("Parent") || loggedUserType.equalsIgnoreCase("Student")) {

                                                            if (sharedPreferences.getString("loggedUserID", "Not Found").equalsIgnoreCase(id[d])) {
                                                                JSONArray subject_array = class1_job.optJSONArray("subject");

                                                                String[] sub_id = new String[subject_array.length()];
                                                                String[] sub_name = new String[subject_array.length()];

                                                                for (int sub = 0; sub < subject_array.length(); sub++) {
                                                                    JSONObject sub_obj = subject_array.optJSONObject(sub);

                                                                    sub_id[sub] = sub_obj.optString("subjectid");
                                                                    sub_name[sub] = sub_obj.optString("subjectname");

                                                                }
                                                                saveSubjectForParent(sub_id, sub_name);

                                                            }
                                                        }
                                                    }

                                                    if (name.length > 0 && id.length > 0)
                                                        saveStudentDataFromServer(name, id, class_id, section_id);

                                                }


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            JSONArray subject_array = null;

                                            subject_array = class1_job.optJSONArray("subject");


                                            String[] sub_id = new String[subject_array.length()];
                                            String[] sub_name = new String[subject_array.length()];

                                            for (int sub = 0; sub < subject_array.length(); sub++) {

                                                JSONObject sub_obj = null;
                                                try {
                                                    sub_obj = subject_array.getJSONObject(sub);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                sub_id[sub] = sub_obj.optString("subjectid");
                                                sub_name[sub] = sub_obj.optString("subjectname");


                                            }
                                            saveSubjectIntoDatabase(class_id, section_id, sub_id, sub_name);

                                        }

                                    }

                                      /*  if (!sharedPreferences.getString("user_type", "").equalsIgnoreCase(""))
                                            Toast.makeText(context, context.getString(R.string.fetched_data_successfully)
                                                    , Toast.LENGTH_LONG).show();*/


                                    sharedPreferences.edit().putString("lastSessionDataFetch", sharedPreferences.getString("session", "")).commit();

                                } else if (!sharedPreferences.getString("user_type", "").equalsIgnoreCase(""))
                                    Toast.makeText(context, context.getString(R.string.data_not_fetched_successfully_try_again_later)
                                            , Toast.LENGTH_LONG).show();

                                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("DATA_FETCH"));

                                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                Log.d("error volley", volleyError.toString());
                                if (sharedPreferences.getBoolean(Config.ADMIN_DATA_FETCHED, true)) {
                                    sharedPreferences.edit().putBoolean(Config.ADMIN_DATA_FETCHED, false).commit();
                                }
                                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("DATA_FETCH"));
                                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                                    Toast.makeText(context, context.getString(R.string.internet_not_available_please_check_your_internet_connectivity)
                                            , Toast.LENGTH_LONG).show();


                                } else
                                    Toast.makeText(context, context.getString(R.string.data_not_fetched_successfully_try_again_later)
                                            , Toast.LENGTH_LONG).show();
                                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.DATA_LOADING_COMPLETE));
                            }

                        });

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppRequestQueueController.getInstance(context).cancelRequestByTag("getDetails");
                AppRequestQueueController.getInstance(context).addToRequestQueue(jsonObjectRequest, "getDetails");

            }
        }, 500);
        return true;
    }


    public ArrayList<StudentData> getTeacherDataForMsg() {
        ArrayList<ChatPermission> chatPermissionList = new ArrayList<>();

        String userType = sharedPreferences.getString("user_type", "Not Found");
        boolean isAdmin = false;
        if (!(userType.equalsIgnoreCase("Administrator") || userType.equalsIgnoreCase("Admin"))) {
            if (!sharedPreferences.getString("MessagePermissionResult", "").equalsIgnoreCase("")) {
                ChatPermission chatPermissionData = (new Gson()).fromJson(sharedPreferences.getString("MessagePermissionResult", ""), new TypeToken<ChatPermission>() {
                }.getType());
                chatPermissionList = chatPermissionData.getResult();
            }
        } else isAdmin = true;


        String userID = (isAdmin || userType.equalsIgnoreCase("student") || userType.equalsIgnoreCase("parent") ) ? ""
                : sharedPreferences.getString("loggedUserID", "Not Found");

/* -----  order by LOWER(TITLE)"
  or
  ORDER BY TITLE COLLATE NOCASE
   * */
        Cursor cc = getDbReadObject().rawQuery("Select * from UserStaffList where TeacherID!=? order by TeacherName COLLATE NOCASE",
                new String[]{userID});
        ArrayList<StudentData> teacherData = new ArrayList<>();

        if (cc != null) {
            while (cc.moveToNext()) {
                boolean isPermission = false;
                String userTypeReceiver = "";

                if (isAdmin) isPermission = true;
                else {
                    userTypeReceiver = cc.getString(cc.getColumnIndex("UserType"));
                    Log.e("userTypeReceiver", userTypeReceiver);

                    for (ChatPermission chatObj : chatPermissionList) {

                        if (chatObj.getToUserType().equalsIgnoreCase(userTypeReceiver)) {
                            isPermission = true;
                            break;
                        }
                    }
                }
                if (isPermission)
                    teacherData.add(new StudentData(
                            cc.getString(cc.getColumnIndex("TeacherName")) /*+ " (" + cc.getString(cc.getColumnIndex("UserType")) + ")"*/,
                            cc.getString(cc.getColumnIndex("TeacherID")),
                            cc.getString(cc.getColumnIndex("UserType")), 0

                    ));
            }
            cc.close();
        }
        //  db.close();

        return teacherData;
    }

    public String[] getHomework(String cls_selected_id, String section_selected_id, String[] week_list) {
        //  SQLiteDatabase db = super.getReadableDatabase();

        String[] work = new String[week_list.length];
        int date_work = 0;
        for (int i = 0; i < week_list.length; i++) {
            Cursor cs = getDbReadObject().rawQuery("Select * from Homework where class_id=? AND section_id=? AND date=?",
                    new String[]{cls_selected_id, section_selected_id, week_list[i]});
            String aa = "";
            String sub_work = "";

            if (cs != null && cs.moveToNext()) {
                sub_work = context.getString(R.string.assignment_available);

            }
            work[i] = sub_work;

            date_work++;
            if (cs != null) cs.close();
        }

        //   db.close();
        return work;
    }


    public List<HomeworkEvaluations> getHomeworkEvalData(String dateOfHomework, String sectionId,
                                                         String subjectID) {
        // SQLiteDatabase db = super.getReadableDatabase();

        List<HomeworkEvaluations> homeworkEvaluationsList = new ArrayList<>();


        Cursor cs = getDbReadObject().rawQuery("Select * from Homework where section_id=? AND date=? AND subjectid=? ",
                new String[]{sectionId, dateOfHomework, subjectID});
        Log.e("sectionId", sectionId);
        Log.e("dateOfHomework", dateOfHomework);
        Log.e("subjectID", subjectID);

        if (cs != null) {
            if (cs.moveToNext()) {
                String homeworkEval = cs.getString(cs.getColumnIndex("homeworkEvaluation")) + "";
                Log.e("homeworkEvaluation", cs.getString(cs.getColumnIndex("homeworkEvaluation")) + " aaa");
                if (homeworkEval.contains("C")) {
                    String[] rr = cs.getString(cs.getColumnIndex("homeworkEvaluation")).split(",");
                    Log.e("ritu", rr.length + "");

                    for (int st = 0; st < rr.length; st++) {
                        String[] aa = rr[st].split("-");
                        //  Log.e("ritu", aa[0]);

                        if (aa.length >= 2) {
                            Log.e("homeworkEvaluation", cs.getString(cs.getColumnIndex("homeworkEvaluation")) + " aaa");
                            Cursor c2 = getDbReadObject().rawQuery("Select * from Student_Record where student_id=?",
                                    new String[]{aa[0]});
                            if (c2 != null) {
                                if (c2.moveToNext()) {
                                    homeworkEvaluationsList.add(new HomeworkEvaluations(
                                            c2.getString(c2.getColumnIndex("student_name")),
                                            aa[0]
                                            , aa[1]));
                                    Log.e("homeworkEvaluation", c2.getString(c2.getColumnIndex("student_name")) +
                                            aa[0] + aa[1]);
                                }

                                c2.close();
                            }
                        }
                    }
                } else {
                    Cursor c1 = getDbReadObject().rawQuery("Select * from Student_Record where section_id=?", new String[]{sectionId});

                    Log.e("sectionId", sectionId);
                    Log.e("sectionId", c1.getCount() + "");
                    if (c1 != null) {
                        while (c1.moveToNext()) {
                            Log.e("ritu", c1.getString(c1.getColumnIndex("student_name")));
                            homeworkEvaluationsList.add(new HomeworkEvaluations(
                                    c1.getString(c1.getColumnIndex("student_name")),
                                    c1.getString(c1.getColumnIndex("student_id"))
                                    , "NE"));
                            //   homeworkEval = homeworkEval + c1.getString(c1.getColumnIndex("student_id")) + "-INC";

                        }
                        c1.close();
                    }
                }

            }
            cs.close();
        }
        return homeworkEvaluationsList;

    }


    public void saveHomeworkEvaluation(String classId, String sectionId, String subId, String dateofHomework, String homeworkEval) {
        //  SQLiteDatabase db = super.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("sending_work_status", "Pending");
        cv.put("homeworkEvaluation", homeworkEval);
        Log.e("resultDB", homeworkEval);
        Log.e("resultDB", getDbWriteObject().update("Homework", cv, "section_id=? AND subjectid=? AND date=?", new String[]{sectionId, subId, dateofHomework}) + "");
        //    db.close();
        try {
            sendHomeWorkEvaluationToServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean insertHomeworkFromServer(List<ContentValues> dataList) {
        //  SQLiteDatabase db = super.getWritableDatabase();

        for (int i = 0; i < dataList.size(); i++) {
            ContentValues cv = dataList.get(i);

            String classID = cv.getAsString("class_id");
            String section_id = cv.getAsString("section_id");
            String subjectid = cv.getAsString("subjectid");
            String date = cv.getAsString("date");

            Log.e("section_id", section_id);
            Log.e("subjectid", subjectid);
            Log.e("date", date);
            Log.e("homeworkEvaluation", cv.getAsString("homeworkEvaluation"));

            Cursor c1 = getReadableDatabase().rawQuery("Select * from Homework where section_id=? AND subjectid=? AND date=?",
                    new String[]{section_id, subjectid,
                            date});
            if (c1 != null) {
                if (c1.moveToNext()) {

                    int aa = getDbWriteObject().update("Homework", cv, "class_id=? AND section_id=? AND date=? AND subjectid=?",
                            new String[]{classID, section_id, date, subjectid});

                    Log.e("resultUpdationDB", aa + "");
                } else {
                    if ((getDbWriteObject().insert("Homework", null, cv) == -1)) {
                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Toast.makeText(context, "error in insertion review", Toast.LENGTH_SHORT).show();
                    } else {
                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Log.e("insertedhomework", cv.getAsString("section_id") + cv.getAsString("subjectid") +
                                    cv.getAsString("date"));
                    }


                }

                c1.close();
            }
        }

        return true;
    }


    public ArrayList<StudentData> getStudentsForMessaging(String section_id,
                                                          String type, boolean allStudentFetch) {
        //   SQLiteDatabase db = super.getReadableDatabase();
        // SharedPreferences sharedPreferences = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
        String user_type = sharedPreferences.getString("user_type", "Not Found");
        String userID = sharedPreferences.getString("loggedUserID", "Not Found");
        ArrayList<StudentData> dataArrayList = new ArrayList<>();
        Cursor cs = null;

        if (("student".equalsIgnoreCase(type)) && user_type.equalsIgnoreCase("Student")) {
            if (allStudentFetch)
                cs = getDbReadObject().rawQuery("Select * from Student_Record where student_id!=? order by student_name COLLATE NOCASE",
                        new String[]{userID});
            else
                cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? and student_id!=? order by student_name COLLATE NOCASE",
                        new String[]{section_id, userID});

        } else if (("student".equalsIgnoreCase(type)) && user_type.equalsIgnoreCase("Parent")) {
            if (allStudentFetch)
                cs = getDbReadObject().rawQuery("Select * from Student_Record order by student_name COLLATE NOCASE",
                        null);
            else
                cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? order by student_name COLLATE NOCASE",
                        new String[]{section_id});
        }
        if (("parent".equalsIgnoreCase(type)) && user_type.equalsIgnoreCase("Parent")) {

            if (allStudentFetch)
                cs = getDbReadObject().rawQuery("Select * from Student_Record where student_id!=? order by student_name COLLATE NOCASE",
                        new String[]{userID});
            else
                cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? and student_id!=? order by student_name COLLATE NOCASE",
                        new String[]{section_id, userID});
        } else if (("parent".equalsIgnoreCase(type)) && user_type.equalsIgnoreCase("Student")) {

            if (allStudentFetch)
                cs = getDbReadObject().rawQuery("Select * from Student_Record order by student_name COLLATE NOCASE",
                        null);
            else
                cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? order by student_name COLLATE NOCASE",
                        new String[]{section_id});
        } else /*if (user_type.equalsIgnoreCase("Teacher") || user_type.equalsIgnoreCase("Administrator") || user_type.equalsIgnoreCase("Admin"))*/ {
            if (allStudentFetch)
                cs = getDbReadObject().rawQuery("Select * from Student_Record order by student_name COLLATE NOCASE",
                        null);
            else
                cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? order by student_name COLLATE NOCASE",
                        new String[]{section_id});
        }

        if (cs != null) {
            while (cs.moveToNext()) {
//                Log.e("ID", "ID:" + cs.getString(cs.getColumnIndex("student_id"))
//                        + "NAME:" + cs.getString(cs.getColumnIndex("student_name")));

                dataArrayList.add(new StudentData(cs.getString(cs.getColumnIndex("student_name")),
                        cs.getString(cs.getColumnIndex("student_id"))));

            }


            cs.close();
        }
        return dataArrayList;
    }


//    public ArrayList<Attendance> getStudentsList(String section_id, String date_name, String subjectId) {
//
//
//        Cursor cuu = getDbReadObject().rawQuery("Select Student_Record.student_id,Student_Record.student_name," +
//                        "StudentAttendance.present_status from Student_Record  LEFT JOIN StudentAttendance ON" +
//                        "  Student_Record.student_id=StudentAttendance.student_id " +
//                        " AND StudentAttendance.date=? AND StudentAttendance.subjectId=? where Student_Record.section_id=? ",
//                new String[]{date_name, subjectId, section_id});
//
//        Log.e("subjectId", subjectId);
//
//        ArrayList<Attendance> attendanceArrayList = new ArrayList<>();
//
//        if (cuu != null && cuu.getCount() > 0) {
//
//            while (cuu.moveToNext()) {
//                attendanceArrayList.add(new Attendance(
//                        cuu.getString(cuu.getColumnIndex("student_id")),
//                        cuu.getString(cuu.getColumnIndex("student_name")),
//                        cuu.getString(cuu.getColumnIndex("present_status"))
//                ));
//            }
//            cuu.close();
//        } else {
//            Cursor cs = getDbReadObject().rawQuery("Select * from Student_Record where section_id=? order by student_name COLLATE NOCASE",
//                    new String[]{section_id});
//
//            if (cs != null) {
//                while (cs.moveToNext()) {
//                    attendanceArrayList.add(new Attendance(
//                            cs.getString(cs.getColumnIndex("student_id")),
//                            cs.getString(cs.getColumnIndex("student_name")),
//                            "no"
//                    ));
//
//                }
//                cs.close();
//            }    //  db.close();
//        }
//
//
//        return attendanceArrayList;

//    }

    public void saveAttendanceOfStudent(String section_id, String date_name,
                                        ArrayList<Attendance> attendanceArrayList, String subjectId) throws JSONException {

        //   SQLiteDatabase db = super.getWritableDatabase();
        ContentValues cv = new ContentValues();

        for (Attendance obj : attendanceArrayList) {
            cv.put("student_id", obj.getAdmissionID());
            cv.put("section_id", section_id);
            cv.put("date", date_name);
            cv.put("present_status", obj.getPresentStatus());
            cv.put("sending_status", "Pending");

//            if (!subjectWiseOrNot)
//                subjectId = "0";

            cv.put("subjectId", subjectId);

            Cursor cq = getDbReadObject().rawQuery("Select * from StudentAttendance where  section_id=? AND " +
                            "date=? AND student_id=? AND subjectId=?",
                    new String[]{section_id, date_name, obj.getAdmissionID(), subjectId});

            if (cq != null) {
                if (cq.moveToNext()) {

                    int aa = getDbWriteObject().update("StudentAttendance", cv,
                            "student_id=? AND section_id=? AND date=? AND subjectId=?",
                            new String[]{obj.getAdmissionID(), section_id, date_name, subjectId});

//               Toast.makeText(context, "updation done in  " + aa, Toast.LENGTH_SHORT).show();
                    Log.d("ritu updated", obj.getStudentName() + " " + obj.getPresentStatus() + " section_id " + section_id);
                } else {
                    if ((getDbWriteObject().insert("StudentAttendance", null, cv) == -1)) {
                        Log.d("rituUpdated", obj.getStudentName() + " " + obj.getPresentStatus() + " section_id " + section_id);
//                        Toast.makeText(context, "not inserted " + id, Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("rituError", obj.getStudentName() + " " + obj.getPresentStatus() + " section_id " + section_id);
//                    Toast.makeText(context, "inserted " + id, Toast.LENGTH_SHORT).show();
                    }
                }
                cq.close();
            }
        }

        //  db.close();

        //   SharedPreferences sp = context.getSharedPreferences("myPreference", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("typeReq", "ic_attendance");
        editor.commit();


        sendData("ic_attendance");


    }


    public ArrayList<HomeworkEvaluations> getStudentNameForHomeworkReportEval(String studentEvaluation) {
        ArrayList<HomeworkEvaluations> evaluationsArrayList = new ArrayList<>();
//Log.e("studentEvaluation",studentEvaluation);
        if (studentEvaluation.contains(",")) {

            String studentEvaList[] = studentEvaluation.split(",");


            for (int stu = 0; stu < studentEvaList.length; stu++) {

                //  Log.e("studentEvaList",studentEvaList[stu]);

                if (studentEvaList[stu].contains("-")) {

                    String[] dataListSpilited = studentEvaList[stu].split("-");

                    //   Log.e("dataListSpilited",dataListSpilited[0]);
                    //   Log.e("dataListSpilited",dataListSpilited[1]);
                    if (dataListSpilited.length >= 2) {

                        Cursor cStudentFind = getDbReadObject().rawQuery("Select * from " +
                                "Student_Record where student_id=?", new String[]{dataListSpilited[0]});

                        // Log.e("cStudentFind",cStudentFind.getCount()+ " ritu");

                        if (cStudentFind != null && cStudentFind.moveToNext()) {

                            //  Log.e("Studentname",cStudentFind.getString(cStudentFind.getColumnIndex("student_name")));

                            evaluationsArrayList.add(new HomeworkEvaluations(
                                    cStudentFind.getString(cStudentFind.getColumnIndex("student_name")),
                                    dataListSpilited[0],
                                    dataListSpilited[1]));
                            cStudentFind.close();
                        }

                    }
                }
            }


        }
        //   Log.e("evaluationsArrayList",evaluationsArrayList.size()+" ritu");

        return evaluationsArrayList;
    }

    public List<StudentData> getTeacherList() {
        //  SQLiteDatabase db = super.getWritableDatabase();
        Cursor cc = getDbReadObject().rawQuery("Select * from TeacherList", null);
        ArrayList<StudentData> teacherListForFeedback = new ArrayList<>();

        Log.e("TeacherList",cc.getCount()+" TeacherList");
        while (cc.moveToNext()) {
            teacherListForFeedback.add(new StudentData(cc.getString(cc.getColumnIndex("TeacherName")),
                    cc.getString(cc.getColumnIndex("TeacherID"))));
            Log.e("TeacherList",cc.getString(cc.getColumnIndex("TeacherName"))+" TeacherList");
        }
        cc.close();
        //  db.close();
        return teacherListForFeedback;
    }


    public void saveAttendanceFromServer(String section_id, String date_name, String subjectId, JSONArray jsonArray) {
        ContentValues cv = new ContentValues();
        Log.e("saveAttendance", section_id);
        Log.e("saveAttendance", date_name);
        Log.e("saveAttendance", subjectId);


        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jj = null;
            try {
                jj = jsonArray.getJSONObject(i);
                cv.put("student_id", jj.getString("StudentID"));
                cv.put("section_id", section_id);
                cv.put("date", date_name);
                cv.put("present_status", jj.optString("presentStatus"));
                cv.put("sending_status", "Sent");

                Log.e("saveAttendance", jj.getString("StudentID"));
                Log.e("saveAttendance", jj.getString("presentStatus"));


                cv.put("subjectId", subjectId);

                Cursor cq = getDbReadObject().rawQuery("Select * from StudentAttendance where  section_id=? AND " +
                                "date=? AND student_id=? AND subjectId=?",
                        new String[]{section_id, date_name, jj.getString("StudentID"), subjectId});

                Log.e("saveAttendance", cq.getCount() + " ritu");

                if (cq != null) {
                    if (cq.moveToNext()) {

                        int aa = getDbWriteObject().update("StudentAttendance", cv,
                                "student_id=? AND section_id=? AND date=? AND subjectId=?",
                                new String[]{jj.getString("StudentID"), section_id, date_name, subjectId});

                    } else {
                        if ((getDbWriteObject().insert("StudentAttendance", null, cv) == -1)) {
                            Log.d("rituUpdated", jj.getString("StudentID") + " " + jj.getString("presentStatus") + " section_id " + section_id);
//                        Toast.makeText(context, "not inserted " + id, Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("rituError", jj.getString("StudentID") + " " + jj.getString("presentStatus") + " section_id " + section_id);
//                    Toast.makeText(context, "inserted " + id, Toast.LENGTH_SHORT).show();
                        }

                    }
                    cq.close();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }


}


