package in.junctiontech.school;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.Calendar;

import in.junctiontech.school.FCM.Config;


public class EnterMobileNumber extends AppCompatActivity {

    private EditText et_enter_mobile_number;
    private SharedPreferences sharedPreferences;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_enter_mobile_number);
        et_enter_mobile_number = (EditText)findViewById(R.id.et_enter_mobile_number);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
//        if (!sharedPreferences.getString("mobileNumber","").equalsIgnoreCase(""))
//        et_enter_mobile_number.setText(sharedPreferences.getString("mobileNumber",""));
        calendar= Calendar.getInstance();
    }

    public void cancel(View view) {
        Config.hideKeyboard(this);

        SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("permissionReadContact", true);
                editor.commit();

        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 7);
        editor.putString("nextPermissionReadContactDate", calendar.get(Calendar.YEAR) + "-" +
                calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH));
        editor.commit();
      //  OtherUserMainDrawarActivity.saveContactInServer(this,sharedPreferences);
        //finish();
        overridePendingTransition(R.anim.nothing,R.anim.slide_out);
    }

    public void saveNumber(View view) {
        Config.hideKeyboard(this);
        if (et_enter_mobile_number.getText().length()<10){
            et_enter_mobile_number.setError(getString(R.string.please_enter_valid_mobile_mumber));
        }else {

        sharedPreferences.edit().putString("mobileNumber",
                et_enter_mobile_number.getText().toString())
                .putBoolean("permissionReadContact", false).commit();
           // OtherUserMainDrawarActivity.saveContactInServer(this,sharedPreferences);
            //finish();
            overridePendingTransition(R.anim.nothing,R.anim.slide_out);
        }
    }

}
