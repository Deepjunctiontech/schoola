package in.junctiontech.school;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

/**
 * adb shell am broadcast -a com.android.vending.INSTALL_REFERRER -n in.junctiontech.school/.ReferrerCatcher --es "referrer" "deep"
 */

public class ReferrerCatcher extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String referrer = "WellCome MySchool!";
        if (bundle != null) {
            referrer = bundle.getString("referrer", "varsha");
            Log.e("referrer", referrer);
            if (!referrer.contains("utm_source=google-play")) {
              //  Toast.makeText(context, referrer + "", Toast.LENGTH_LONG).show();
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("ReferrerOrganizationCode").putExtra("OrganizationCode", referrer));
            }

        }
     }
}
