package in.junctiontech.school.menuActions.aboutus;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class AboutUsScrollingActivity extends AppCompatActivity {

    private TextView tv_about_us_junction_detail;
    private int  colorIs =0;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((CollapsingToolbarLayout)findViewById(R.id.toolbar_layout)).setContentScrimColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        /*   For Background display */
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            findViewById(R.id.layout_about_us).setBackgroundResource(R.drawable.ic_background_landscape);
        else
                 findViewById(R.id.layout_about_us).setBackgroundResource(R.drawable.ic_background_verticle);


        View view = getLayoutInflater().inflate(R.layout.content_about_us_scrolling, null);

        tv_about_us_junction_detail = (TextView) view.findViewById(R.id.tv_about_us_junction_detail);
        //  Spannable span = new SpannableString(tv_about_us_junction_detail.getText());
        int totalChar = tv_about_us_junction_detail.getText().length();
        //  span.setSpan(new RelativeSizeSpan(0.8f), 0, totalChar, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //  tv_about_us_junction_detail.setText(span);

        String upToNCharacters = tv_about_us_junction_detail.getText().toString()
                .substring(0, Math.min(tv_about_us_junction_detail.getText().toString().length()
                        , 3));
        tv_about_us_junction_detail.setText(upToNCharacters);
        FloatingActionButton   fab_contact_us=  (FloatingActionButton) findViewById(R.id.fab_contact_us);
        fab_contact_us .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.zeroerp.com/"));

                startActivity(viewIntent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        fab_contact_us.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        ((AppBarLayout)findViewById(R.id.app_bar)).setBackgroundColor(colorIs);
        ((CollapsingToolbarLayout)findViewById(R.id.toolbar_layout)).setBackgroundColor(colorIs);

        ((TextView)findViewById(R.id.tv_about_us_junction_detail_heading)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_about_us_junction_detail_heading2)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_about_us_junction_detail_heading3)).setTextColor(colorIs);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }
}
