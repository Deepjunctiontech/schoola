package in.junctiontech.school.menuActions;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

public class SendingStatus extends AppCompatActivity {

    private Toolbar toolbar;
    private SharedPreferences sp;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_data);
        toolbar = (Toolbar) findViewById(R.id.notification_toolbar);
        this.setSupportActionBar(toolbar);
        sp = Prefs.with(this).getSharedPreferences();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setColorApp();
    }

    private void setColorApp() {

        int colorIs = Config.getAppColor(this,true);
        //getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        tabLayout.setBackgroundColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment fAtt = new AttendanceFragment();
        Bundle b = new Bundle();
        b.putInt("value", 0);
        fAtt.setArguments(b);
        adapter.addFragment(fAtt, getString(R.string.attendance));
        adapter.addFragment(new HomeworkFragment(), getString(R.string.assignment));
        adapter.addFragment(new ExamFragment(), getString(R.string.exam_results));

        if (!(sp.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("admin")))
            adapter.addFragment(new FeedbackFragment(), getString(R.string.teacher_notes));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static class HomeworkFragment extends Fragment {


        private View convertView;
        private ListView listview_sending_status;
        private TextView tv_empty_attendance;
        private DbHandler db;
        private ArrayList class_name, section_name, dates_name, sending_status;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (class_name.size() == 0) {
                tv_empty_attendance.setVisibility(View.VISIBLE);

                tv_empty_attendance.setText("No " + getString(R.string.assignment) + "  Available...!!");
                tv_empty_attendance.setPadding(0, 30, 0, 30);
                tv_empty_attendance.setGravity(Gravity.CENTER);
                tv_empty_attendance.setTextColor(Color.RED);
            } else tv_empty_attendance.setVisibility(View.INVISIBLE);
            listview_sending_status.setAdapter(new Hadapter(getContext(), class_name, section_name, dates_name, sending_status));

        }


        public HomeworkFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            db = DbHandler.getInstance(getContext());
            // Bundle b= getArguments();
            Bundle b = db.getDataForStatus(1);
            class_name = b.getStringArrayList("class");
            section_name = b.getStringArrayList("sec");
            dates_name = b.getStringArrayList("date");
            sending_status = b.getStringArrayList("status");


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            convertView = inflater.inflate(R.layout.layout_send_status, container, false);
            listview_sending_status = (ListView) convertView.findViewById(R.id.listview_sending_status_attendance);
            tv_empty_attendance = (TextView) convertView.findViewById(R.id.tv_empty_data);
            return convertView;
        }

        private class Hadapter extends BaseAdapter {
            Context context;
            ArrayList<String> class_name, section_name, dates_name, sending_status;

            public Hadapter(Context context, ArrayList class_name, ArrayList section_name, ArrayList dates_name, ArrayList sending_status) {
                this.context = context;
                this.class_name = class_name;
                this.section_name = section_name;
                this.dates_name = dates_name;
                this.sending_status = sending_status;
            }

            @Override
            public int getCount() {
                return class_name.size();
            }

            @Override
            public Object getItem(int position) {
                return class_name.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.sending_status_layoutfile, parent, false);

                ((TextView) convertView.findViewById(R.id.sendingstatus_class_name)).setText(class_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_section_name)).setText(section_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_date)).setText(dates_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setText(sending_status.get(position));
                if (sending_status.get(position).equalsIgnoreCase("Pending"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.RED);
                else if (sending_status.get(position).equalsIgnoreCase("Received"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.BLUE);
                else
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(getResources().getColor(R.color.sentDataColor));
                return convertView;
            }
        }

    }

    public static class ExamFragment extends Fragment {

        private View convertView;
        private ListView listview_sending_status;
        private TextView tv_empty_attendance;
        private DbHandler db;
        ArrayList<String> class_name, section_name, dates_name, sending_status;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (class_name.size() == 0) {
                tv_empty_attendance.setVisibility(View.VISIBLE);

                tv_empty_attendance.setText("No " + getString(R.string.exam_results) + "  Available...!!");
                tv_empty_attendance.setPadding(0, 30, 0, 30);
                tv_empty_attendance.setGravity(Gravity.CENTER);
                tv_empty_attendance.setTextColor(Color.RED);
            } else tv_empty_attendance.setVisibility(View.INVISIBLE);
            listview_sending_status.setAdapter(new Eadapter(getContext(), class_name, section_name, dates_name, sending_status));

        }


        public ExamFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            db = DbHandler.getInstance(getContext());

            Bundle b = db.getDataForStatus(2);
            class_name = b.getStringArrayList("class");
            section_name = b.getStringArrayList("sec");
            dates_name = b.getStringArrayList("date");
            sending_status = b.getStringArrayList("status");


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            convertView = inflater.inflate(R.layout.layout_send_status, container, false);
            listview_sending_status = (ListView) convertView.findViewById(R.id.listview_sending_status_attendance);
            tv_empty_attendance = (TextView) convertView.findViewById(R.id.tv_empty_data);
            ((TextView) convertView.findViewById(R.id.tv_examNameForStatus)).setText(getString(R.string.exam_results));
            ((TextView) convertView.findViewById(R.id.tv_classNameForStatus)).setText(getString(R.string.class_text));
            return convertView;
        }

        private class Eadapter extends BaseAdapter {
            Context context;
            ArrayList<String> class_name, section_name, dates_name, sending_status;

            public Eadapter(Context context, ArrayList<String> class_name, ArrayList<String> section_name, ArrayList<String> dates_name, ArrayList<String> sending_status) {
                this.context = context;
                this.class_name = class_name;
                this.section_name = section_name;
                this.dates_name = dates_name;
                this.sending_status = sending_status;
            }

            @Override
            public int getCount() {
                return class_name.size();
            }

            @Override
            public Object getItem(int position) {
                return class_name.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.sending_status_layoutfile, parent, false);

                ((TextView) convertView.findViewById(R.id.sendingstatus_class_name)).setText(class_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_section_name)).setText(section_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_date)).setText(dates_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setText(sending_status.get(position));
                if (sending_status.get(position).equalsIgnoreCase("Pending"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.RED);
                else if (sending_status.get(position).equalsIgnoreCase("Received"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.BLUE);
                else
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(getResources().getColor(R.color.sentDataColor));
                return convertView;
            }
        }

    }

    public static class AttendanceFragment extends Fragment {
        private View convertView;
        private ListView listview_sending_status;
        private TextView tv_empty_attendance;
        private DbHandler db;
        ArrayList<String> class_name, section_name, dates_name, sending_status;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (class_name.size() == 0) {
                tv_empty_attendance.setVisibility(View.VISIBLE);

                tv_empty_attendance.setText("No " + getString(R.string.attendance) + "  Available...!!");
                tv_empty_attendance.setPadding(0, 30, 0, 30);
                tv_empty_attendance.setGravity(Gravity.CENTER);
                tv_empty_attendance.setTextColor(Color.RED);
            } else tv_empty_attendance.setVisibility(View.INVISIBLE);
            listview_sending_status.setAdapter(new Aadapter(getContext(), class_name, section_name, dates_name, sending_status));

        }


        public AttendanceFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            db = DbHandler.getInstance(getContext());

            Bundle b = db.getDataForStatus(0);
            class_name = b.getStringArrayList("class");
            section_name = b.getStringArrayList("sec");
            dates_name = b.getStringArrayList("date");
            sending_status = b.getStringArrayList("status");


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            convertView = inflater.inflate(R.layout.layout_send_status, container, false);
            listview_sending_status = (ListView) convertView.findViewById(R.id.listview_sending_status_attendance);
            tv_empty_attendance = (TextView) convertView.findViewById(R.id.tv_empty_data);
            return convertView;
        }

        private class Aadapter extends BaseAdapter {
            Context context;
            ArrayList<String> class_name, section_name, dates_name, sending_status;

            public Aadapter(Context context, ArrayList<String> class_name, ArrayList<String> section_name, ArrayList<String> dates_name, ArrayList<String> sending_status) {
                this.context = context;
                this.class_name = class_name;
                this.section_name = section_name;
                this.dates_name = dates_name;
                this.sending_status = sending_status;
            }

            @Override
            public int getCount() {
                return class_name.size();
            }

            @Override
            public Object getItem(int position) {
                return class_name.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.sending_status_layoutfile, parent, false);

                ((TextView) convertView.findViewById(R.id.sendingstatus_class_name)).setText(class_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_section_name)).setText(section_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_date)).setText(dates_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setText(sending_status.get(position));
                if (sending_status.get(position).equalsIgnoreCase("Pending"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.RED);
                else if (sending_status.get(position).equalsIgnoreCase("Received"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.BLUE);
                else
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(getResources().getColor(R.color.sentDataColor));
                return convertView;
            }
        }

    }


    public static class FeedbackFragment extends Fragment {
        private View convertView;
        private ListView listview_sending_status;
        private TextView tv_empty_attendance;
        private DbHandler db;
        private ArrayList class_name, section_name, dates_name, sending_status;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (class_name.size() == 0) {
                tv_empty_attendance.setVisibility(View.VISIBLE);

                tv_empty_attendance.setText("No " + getString(R.string.feedback) + "  Available...!!");
                tv_empty_attendance.setPadding(0, 30, 0, 30);
                tv_empty_attendance.setGravity(Gravity.CENTER);
                tv_empty_attendance.setTextColor(Color.RED);
            } else tv_empty_attendance.setVisibility(View.INVISIBLE);
            listview_sending_status.setAdapter(new Fadapter(getContext(), class_name, section_name, dates_name, sending_status));

        }


        public FeedbackFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            db = DbHandler.getInstance(getContext());

            Bundle b = db.getDataForStatus(3);
            class_name = b.getStringArrayList("class");
            section_name = b.getStringArrayList("sec");
            dates_name = b.getStringArrayList("date");
            sending_status = b.getStringArrayList("status");


        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            convertView = inflater.inflate(R.layout.layout_send_status, container, false);
            listview_sending_status = (ListView) convertView.findViewById(R.id.listview_sending_status_attendance);
            tv_empty_attendance = (TextView) convertView.findViewById(R.id.tv_empty_data);
            ((TextView) convertView.findViewById(R.id.tv_examNameForStatus)).setText(getString(R.string.name));
            ((TextView) convertView.findViewById(R.id.tv_classNameForStatus)).setText(getString(R.string.class_text));


            return convertView;
        }

        private class Fadapter extends BaseAdapter {
            Context context;
            ArrayList<String> class_name;
            ArrayList<String> section_name;
            ArrayList<String> dates_name;
            ArrayList<String> sending_status;

            public Fadapter(Context context, ArrayList<String> class_name, ArrayList<String> section_name, ArrayList<String> dates_name, ArrayList<String> sending_status) {
                this.context = context;
                this.class_name = class_name;
                this.section_name = section_name;
                this.dates_name = dates_name;
                this.sending_status = sending_status;
            }

            @Override
            public int getCount() {
                return class_name.size();
            }

            @Override
            public Object getItem(int position) {
                return class_name.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.sending_status_layoutfile, parent, false);

                ((TextView) convertView.findViewById(R.id.sendingstatus_class_name)).setText(class_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_section_name)).setText(section_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_date)).setText(dates_name.get(position));
                ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setText(sending_status.get(position));
                if (sending_status.get(position).equalsIgnoreCase("Pending"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.RED);
                else if (sending_status.get(position).equalsIgnoreCase("Received"))
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(Color.BLUE);
                else
                    ((TextView) convertView.findViewById(R.id.sendingstatus_tv)).setTextColor(getResources().getColor(R.color.sentDataColor));
                return convertView;
            }
        }


    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }
}
