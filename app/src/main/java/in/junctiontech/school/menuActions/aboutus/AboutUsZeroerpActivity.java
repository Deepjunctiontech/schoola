package in.junctiontech.school.menuActions.aboutus;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.managefee.AnotherActivity;

public class AboutUsZeroerpActivity extends AppCompatActivity {


    private int colorIs = 0;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView)findViewById(R.id.tv_connection_through_social_media)).setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us_zeroerp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        ((ImageButton) findViewById(R.id.ib_junction_tech)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AboutUsScrollingActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });


        ((ImageButton) findViewById(R.id.ib_facebook)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_FACEBOOK_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });

        ((ImageButton) findViewById(R.id.ib_youtube)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_YOUTUBE_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        ((ImageButton) findViewById(R.id.ib_linkdin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_LINKDIN_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        ((ImageButton) findViewById(R.id.ib_twitter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_Twiter_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        ((ImageButton) findViewById(R.id.ib_gplus)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_G_PLUS_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        ((ImageButton) findViewById(R.id.ib_blogger)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_BLOGER_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });

        ((ImageButton) findViewById(R.id.ib_tumblr)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsZeroerpActivity.this, AnotherActivity.class).putExtra("id", "aboutUs")
                        .putExtra("url", Config.ZEROERP_TUMBLER_LINK));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
