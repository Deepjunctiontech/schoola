package in.junctiontech.school.menuActions;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

public class DataAutoSending extends AppCompatActivity {

    private TextView tv_autoSendingMobileData, tv_autoSendingWiFi, tv_autoSendingRoaming;
    private CheckBox ck_autoSendingMobileData, ck_autoSendingWiFi, ck_autoSendingRoaming;
   // private SharedPreferences sp_autoDataSend;
  //  private SharedPreferences.Editor editor;
    private CheckBox ck_attendance_dataautosend, ck_homework_dataautosend, ck_result_dataautosend;
    private boolean mat = false, mho = false, mer = false,
            wat = false, who = false, wer = false,
            rat = false, rho = false, rer = false;
    private SwitchCompat switchButton_autoSendingRoaming_attendance, switchButton_autoSendingRoaming_homework, switchButton_autoSendingRoaming_exam_result;
    private SwitchCompat switchButton_autoSendingMobileData_attendance, switchButton_autoSendingMobileData_homework, switchButton_autoSendingMobileData_exam_result;
    private SwitchCompat switchButton_autoSendingWiFi_attendance, switchButton_autoSendingWiFi_homework, switchButton_autoSendingWiFi_exam_result;
    private SharedPreferences sharedPreferences;
    private int  colorIs;

    private void setColorApp() {

           colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
      //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView)findViewById(R.id.tv_when_using_mobile_data)).setTextColor(colorIs);
         ((TextView)findViewById(R.id.tv_when_using_wifi)).setTextColor(colorIs);
         ((TextView)findViewById(R.id.tv_when_using_roaming)).setTextColor(colorIs);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));

        setContentView(R.layout.activity_data_auto_sending);
        getSupportActionBar().setTitle(R.string.data_auto_sending);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
//        AdView mAdView = (AdView) findViewById(R.id.adview_data_auto_sending);
//        AdRequest adRequest = newtext AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        /* mobile data */
        switchButton_autoSendingMobileData_attendance = (SwitchCompat) findViewById(R.id.switchButton_autoSendingMobileData_attendance);
        switchButton_autoSendingMobileData_homework = (SwitchCompat) findViewById(R.id.switchButton_autoSendingMobileData_homework);
        switchButton_autoSendingMobileData_exam_result = (SwitchCompat) findViewById(R.id.switchButton_autoSendingMobileData_exam_result);

        Config.setSwitchCompactColor(switchButton_autoSendingMobileData_attendance,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingMobileData_homework,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingMobileData_exam_result,colorIs);

        (switchButton_autoSendingMobileData_attendance).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mat = isChecked;
                myMobileData();
            }
        });
        (switchButton_autoSendingMobileData_homework).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mho = isChecked;
                myMobileData();
            }
        });
        (switchButton_autoSendingMobileData_exam_result).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mer = isChecked;
                myMobileData();
            }
        });

        /* wifi */
        switchButton_autoSendingWiFi_attendance = (SwitchCompat) findViewById(R.id.switchButton_autoSendingWiFi_attendance);
        switchButton_autoSendingWiFi_homework = (SwitchCompat) findViewById(R.id.switchButton_autoSendingWiFi_homework);
        switchButton_autoSendingWiFi_exam_result = (SwitchCompat) findViewById(R.id.switchButton_autoSendingWiFi_exam_result);

        Config.setSwitchCompactColor(switchButton_autoSendingWiFi_attendance,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingWiFi_homework,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingWiFi_exam_result,colorIs);



        (switchButton_autoSendingWiFi_attendance).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wat = isChecked;
                myWiFiData();
            }
        });
        (switchButton_autoSendingWiFi_homework).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                who = isChecked;
                myWiFiData();
            }
        });
        (switchButton_autoSendingWiFi_exam_result).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wer = isChecked;
                myWiFiData();
            }
        });

 /* roaming */
        switchButton_autoSendingRoaming_attendance = (SwitchCompat) findViewById(R.id.switchButton_autoSendingRoaming_attendance);
        switchButton_autoSendingRoaming_homework = (SwitchCompat) findViewById(R.id.switchButton_autoSendingRoaming_homework);
        switchButton_autoSendingRoaming_exam_result = (SwitchCompat) findViewById(R.id.switchButton_autoSendingRoaming_exam_result);

        Config.setSwitchCompactColor(switchButton_autoSendingRoaming_attendance,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingRoaming_homework,colorIs);
        Config.setSwitchCompactColor(switchButton_autoSendingRoaming_exam_result,colorIs);


        (switchButton_autoSendingRoaming_attendance).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rat = isChecked;
                myRoamingData();
            }
        });
        switchButton_autoSendingRoaming_homework.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rho = isChecked;
                myRoamingData();
            }
        });
        switchButton_autoSendingRoaming_exam_result.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rer = isChecked;
                myRoamingData();
            }
        });


    //    sp_autoDataSend = getSharedPreferences("data_auto_send", MODE_PRIVATE);


        if (sharedPreferences != null) {
            mat = sharedPreferences.getBoolean("MobileData_attendance", false);
            mho = sharedPreferences.getBoolean("MobileData_homework", false);
            mer = sharedPreferences.getBoolean("MobileData_result", false);
            if(mat)
                switchButton_autoSendingMobileData_attendance.setChecked(true);

            if(mho)
                switchButton_autoSendingMobileData_homework.setChecked(true);

            if(mer)
                switchButton_autoSendingMobileData_exam_result.setChecked(true);


           // myMobileData();

            wat = sharedPreferences.getBoolean("wifi_attendance", false);
            who = sharedPreferences.getBoolean("wifi_homework", false);
            wer = sharedPreferences.getBoolean("wifi_result", false);

            if(wat)
                switchButton_autoSendingWiFi_attendance.setChecked(true);

            if(who)
                switchButton_autoSendingWiFi_homework.setChecked(true);

            if(wer)
                switchButton_autoSendingWiFi_exam_result.setChecked(true);


            //  myWiFiData();

            rat = sharedPreferences.getBoolean("roaming_attendance", false);
            rho = sharedPreferences.getBoolean("roaming_homework", false);
            rer = sharedPreferences.getBoolean("roaming_result", false);

            if(rat)
                switchButton_autoSendingRoaming_attendance.setChecked(true);

            if(rho)
                switchButton_autoSendingRoaming_homework.setChecked(true);

            if(rer)
                switchButton_autoSendingRoaming_exam_result.setChecked(true);


        }

    }



    public void myMobileData() {

        sharedPreferences.edit().putBoolean("MobileData_attendance", mat)
                .putBoolean("MobileData_homework", mho)
                .putBoolean("MobileData_result", mer).commit();




    }

    public void myWiFiData() {

    sharedPreferences.edit().putBoolean("wifi_attendance", wat)
                .putBoolean("wifi_homework", who)
                .putBoolean("wifi_result", wer)
                .commit();

    }

    public void myRoamingData() {

        sharedPreferences.edit().putBoolean("roaming_attendance", rat)
                .putBoolean("roaming_homework", rho)
                .putBoolean("roaming_result", rer)
                .commit();


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing,R.anim.slide_out);
        return true;
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing,R.anim.slide_out);
        super.onBackPressed();
    }

}
