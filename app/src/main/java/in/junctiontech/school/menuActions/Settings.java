package in.junctiontech.school.menuActions;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Objects;

import in.junctiontech.school.schoolnew.permissions.communication.MessengerPermissionsActivity;
import in.junctiontech.school.admin.userpermission.PermissionsActivity;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class Settings extends AppCompatActivity {

    private ListView lv_setting_page;
    private String[] class_id, class_name, section_id, section_name;
    private int selectedDefaultClass, selectedDefaultSection;
    private SwitchCompat switchButton_toast_visibility;
    private DbHandler db;
    private SharedPreferences sharedPsreferences;

    private int colorIs;
    private SwitchCompat  switch_compact_notification_sound, switch_compact_chat_notification_sound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));
        db = DbHandler.getInstance(this);
        setContentView(R.layout.activity_settings);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


       /* if (getIntent().getBooleanExtra("setClass", false))
            openAlertDefaultClassSection();*/

        LinearLayout ly_data_auto_sending =  findViewById(R.id.ly_data_auto_sending);
        (ly_data_auto_sending).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this, DataAutoSending.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);

            }
        });
        LinearLayout ly_sending_status =  findViewById(R.id.ly_sending_status);
        LinearLayout ly_default_class_section_setting =  findViewById(R.id.ly_default_class_section_setting);
        LinearLayout ly_delete_sent_data =  findViewById(R.id.ly_delete_sent_data);
        LinearLayout ly_admin_permission =  findViewById(R.id.ly_admin_permission);
        LinearLayout ly_admin_permission_msg =  findViewById(R.id.ly_admin_permission_msg);
        LinearLayout ly_admin_app_color =  findViewById(R.id.ly_admin_app_color);

        String user_type = Gc.getSharedPreference(Gc.USERTYPETEXT, this);

        if (user_type.equalsIgnoreCase("Administrator") || user_type.equalsIgnoreCase("Admin")) {

//            ly_admin_permission.setVisibility(View.VISIBLE);
            ly_admin_permission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Settings.this, PermissionsActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });
            ly_admin_permission_msg.setVisibility(View.VISIBLE);
            ly_admin_permission_msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Settings.this, MessengerPermissionsActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });
            ly_admin_app_color.setVisibility(View.VISIBLE);
            ly_admin_app_color.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(Settings.this, AnotherActivity.class).putExtra("id", "changeAppColor"),
                            676);
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });
        }else   if (user_type.equalsIgnoreCase("Parents") || user_type.equalsIgnoreCase("Parent")
                || user_type.equalsIgnoreCase("Student") || user_type.equalsIgnoreCase("student") || user_type.equalsIgnoreCase("students")){
            ly_sending_status.setVisibility(View.GONE);
            (  findViewById(R.id.ly_delete_sent_data)).setVisibility(View.GONE);
            (  findViewById(R.id.ly_data_auto_sending)).setVisibility(View.GONE);
        }

        ly_delete_sent_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder delete_alert = new AlertDialog.Builder(Settings.this,
                        AlertDialog.THEME_HOLO_LIGHT);
                View view_delete = getLayoutInflater().inflate(R.layout.delete_view, null);
                final CheckBox check_att =  view_delete.findViewById(R.id.check_att);
                final CheckBox check_homework = view_delete.findViewById(R.id.check_homework);
                final CheckBox check_result =  view_delete.findViewById(R.id.check_result);
                final CheckBox check_msg =  view_delete.findViewById(R.id.check_msg);

                ((TextView) view_delete.findViewById(R.id.tv_delete_data_title)).setBackgroundColor(colorIs);
                check_att.setTextColor(colorIs);
                check_homework.setTextColor(colorIs);
                check_msg.setTextColor(colorIs);
                check_result.setTextColor(colorIs);
                delete_alert.setView(view_delete);

                delete_alert.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteData(check_att.isChecked(), check_homework.isChecked(),
                                check_result.isChecked(), check_msg.isChecked());

                    }
                });

                delete_alert.show();
            }
        });

        (ly_sending_status).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this, SendingStatus.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
       /* (findViewById(R.id.ly_language_setting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Settings.this, LanguageSelection.class), 101);

                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });*/
      /*  ly_default_class_section_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAlertDefaultClassSection();
            }
        });*/
//        ((LinearLayout)findViewById(R.id.ly_account_info)).setOnClickListener(newtext View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(newtext Intent(Settings.this, DataAutoSending.class));
//            }
//        });



          switch_compact_notification_sound = (SwitchCompat) findViewById(R.id.switch_compact_notification_sound);

        switch_compact_notification_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPsreferences.edit().putBoolean(Config.NOTIFICATION_SOUND, isChecked).apply();
            }
        });


          switch_compact_chat_notification_sound = (SwitchCompat) findViewById(R.id.switch_compact_chat_notification_sound);

        switch_compact_chat_notification_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPsreferences.edit().putBoolean(Config.CHAT_NOTIFICATION_SOUND, isChecked).apply();
            }
        });

        setColorApp();

        switch_compact_notification_sound.setChecked(sharedPsreferences.getBoolean(Config.NOTIFICATION_SOUND, true));
        switch_compact_chat_notification_sound.setChecked(sharedPsreferences.getBoolean(Config.CHAT_NOTIFICATION_SOUND, true));
       /* switchButton_toast_visibility = (SwitchCompat) findViewById(R.id.switchButton_toast_visibility);
        switchButton_toast_visibility.setChecked(sharedPsreferences.getBoolean("toastVisibility", false));

        switchButton_toast_visibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPsreferences.edit();
                editor.putBoolean("toastVisibility", isChecked);
                editor.commit();
            }
        });*/

    }

 /*   private void openAlertDefaultClassSection() {
        AlertDialog.Builder alert_select_default_class_section = new AlertDialog.Builder(Settings.this);
        alert_select_default_class_section.setMessage(getString(R.string.select_your_default_class_section));
        View view_select_default_class_section = getLayoutInflater().inflate(R.layout.select_default_class_section, null);
        Spinner spinner_classes = (Spinner) view_select_default_class_section.findViewById(R.id.spinner_classes);
        final Spinner spinner_sections = (Spinner) view_select_default_class_section.findViewById(R.id.spinner_sections);
        String[][] data = db.getClassName();

        class_id = data[0];
        class_name = data[1];
        if (class_id.length == 0)
            if (sharedPsreferences.getBoolean("toastVisibility", false))
                Toast.makeText(this, getString(R.string.sections_not_available), Toast.LENGTH_SHORT).show();

        final ArrayAdapter<String> arr = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, class_name);
        spinner_classes.setAdapter(arr);

        selectedDefaultClass = sharedPsreferences.getInt("defaultClass", 0);
        spinner_classes.setSelection(selectedDefaultClass);

        spinner_classes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDefaultClass = position;
                String[][] data = db.getSectionName(class_id[position]);
                section_id = data[0];
                section_name = data[1];

                if (section_id.length == 0) {
                    if (sharedPsreferences.getBoolean("toastVisibility", false))
                        Toast.makeText(Settings.this, getString(R.string.sections_not_available), Toast.LENGTH_SHORT).show();

                }

                ArrayAdapter<String> arr_sec = new ArrayAdapter<String>(Settings.this,
                        android.R.layout.simple_list_item_1, section_name);
                spinner_sections.setAdapter(arr_sec);

                if (sharedPsreferences.getInt("defaultClass", 0) == selectedDefaultClass) {
                    selectedDefaultSection = sharedPsreferences.getInt("defaultSection", 0);
                    spinner_sections.setSelection(selectedDefaultSection);
                } else {
                    spinner_sections.setSelection(0);
                }


                spinner_sections.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedDefaultSection = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        alert_select_default_class_section.setView(view_select_default_class_section);
        alert_select_default_class_section.setPositiveButton(getString(R.string.set), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = sharedPsreferences.edit();
                editor.putInt("defaultClass", selectedDefaultClass);
                editor.putInt("defaultSection", selectedDefaultSection);
                editor.putBoolean("defaultSetting", false);
                editor.commit();
                if (getIntent().getBooleanExtra("setClass", false))
                    finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        alert_select_default_class_section.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (getIntent().getBooleanExtra("setClass", false))
                    finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        alert_select_default_class_section.show();
    }*/

    @Override
    public boolean onSupportNavigateUp() {
//        if (getIntent().getBooleanExtra("previousFinished", true)) {
//            startActivity(new Intent(this, MainActivity.class));
//            finish();
//            overridePendingTransition(R.anim.enter, R.anim.nothing);
//        } else {
//            finish();
//            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
//        }

        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        Log.e("langChangeSett", getIntent().getBooleanExtra("LangChange", false) + " ritu");

        if (getIntent().getBooleanExtra("LangChange", false))
            setResult(RESULT_OK, intent);
        else setResult(RESULT_CANCELED, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return false;
    }

    @Override
    public void onBackPressed() {
//        if (getIntent().getBooleanExtra("previousFinished", true)) {
//            startActivity(new Intent(this, MainActivity.class));
//            finish();
//            overridePendingTransition(R.anim.enter, R.anim.nothing);
//        } else {
//            finish();
//            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
//        }
        //  super.onBackPressed();
        Log.e("langChangeSett", getIntent().getBooleanExtra("LangChange", false) + " ritu");
        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        intent.putExtra("Ritu", "Bhade");

        if (getIntent().getBooleanExtra("LangChange", false)) {
            setResult(RESULT_OK, intent);
            Log.e("langChangeSett", RESULT_OK + " ritu");

        } else {
            setResult(RESULT_CANCELED, intent);
            Log.e("langChangeSett", RESULT_CANCELED + " ritu");

        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 101) {

            Intent intent = getIntent();
            intent.putExtra("LangChange", true);

            finish();
            startActivity(intent);

        } else if (requestCode == 676) {

            int col = Config.getAppColor(this, false);
            if (colorIs != col) {
                colorIs = Config.getAppColor(this, true);
                setColorApp();
            }
        }
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //   getWindow().setNavigationBarColor(colorIs);
        Config.setSwitchCompactColor(switch_compact_chat_notification_sound, colorIs);
        Config.setSwitchCompactColor(switch_compact_notification_sound, colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

}
