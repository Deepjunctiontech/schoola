package in.junctiontech.school.menuActions;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.LocaleList;
import android.util.Log;

import java.util.Locale;

import in.junctiontech.school.Prefs;

/**
 * Created by JAYDEVI BHADE on 6/11/2016.
 */
public class LanguageSetup extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

    }

    public static boolean changeLang(Context context, String lang) {
        SharedPreferences sharedPsreferences = context.getSharedPreferences("APP_LANG", Context.MODE_PRIVATE);
        String pre_lang = sharedPsreferences.getString("app_language", "");
        Locale obj;
        boolean res = false;
        if (lang.equalsIgnoreCase(""))
            obj = Locale.getDefault();
        else {
            if (pre_lang.equalsIgnoreCase(lang)) ;
            else
                res = true;
            obj = new Locale(lang);
        }

        saveLocale(context, obj.getLanguage());

        Locale.setDefault(obj);

        Configuration config = new Configuration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(obj);

        } else {
            config.locale = obj;
        }
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

//        android.content.res.Configuration config = new android.content.res.Configuration();
//        config.locale = obj;
//
//        context.getApplicationContext().getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());


        return res;
    }

    private static void saveLocale(Context context, String lang) {
        SharedPreferences sp = context.getSharedPreferences("APP_LANG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("app_language", lang);
        editor.apply();

    }

}
