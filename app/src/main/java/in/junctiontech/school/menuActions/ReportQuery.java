package in.junctiontech.school.menuActions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class ReportQuery extends AppCompatActivity {
    //    private SharedPreferences sharedPsreferences;
    private EditText et_contact_email, et_contact_number, et_report_bug, et_report_bug_title,
            et_country_code;
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private ProgressDialog progressbar;
    private Spinner sp_report_bug_bug_status;
    private Button btnIssueAttachment;
    private AlertDialog.Builder alertAdmin;
    private View snackbar;
    private int appColor;
    private String attachmentFile;
    private String attachmentType;
    MainDatabase mDb;
    SchoolDetailsEntity schoolDetails = new SchoolDetailsEntity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        mDb = MainDatabase.getDatabase(this);
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));
        setContentView(R.layout.activity_report_bug);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        progressbar = new ProgressDialog(this);
        appColor = Config.getAppColor(this, true);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        snackbar = findViewById(R.id.top_layout);
        et_contact_email = findViewById(R.id.et_contact_email);
        et_country_code = findViewById(R.id.et_country_code_contact_number);
        et_contact_number = findViewById(R.id.et_contact_number);
        et_report_bug = findViewById(R.id.et_report_bug);
        et_report_bug_title = findViewById(R.id.et_report_bug_title);
        alertAdmin = new AlertDialog.Builder(ReportQuery.this);

        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, schoolDetailsEntity -> {

            if (!(schoolDetailsEntity == null)) {
                schoolDetails = schoolDetailsEntity;

                if (schoolDetails.Email != null)
                    et_contact_email.setText(schoolDetails.Email);

                if (schoolDetails.Mobile != null && schoolDetails.Mobile.contains("-")) {
                    HashMap<String, String> phoneSplit = Gc.splitPhoneNumber(schoolDetails.Mobile);

                    et_contact_number.setText(phoneSplit.get("phone"));
                    et_country_code.setText(phoneSplit.get("ccode"));
                }
            }
        });

        btnIssueAttachment = findViewById(R.id.btnIssueAttachment);

        btnIssueAttachment.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 103);
        });

        //  sp_report_bug_bug_priority = findViewById(R.id.sp_report_bug_bug_priority);
        sp_report_bug_bug_status = findViewById(R.id.sp_report_bug_bug_status);


        setColorApp();
    }

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView) findViewById(R.id.tv_report_bug_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_contact_email)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_contact_number)).setTextColor(colorIs);
        (findViewById(R.id.button_report_bug_save)).setBackgroundColor(colorIs);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (!progressbar.isShowing()) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            return true;
        } else return false;
    }

    @Override
    public void onBackPressed() {
        if (!progressbar.isShowing()) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            super.onBackPressed();
        }
    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public void sendReportBug(View v) {

        if ("".equalsIgnoreCase(et_report_bug_title.getText().toString())) {
            alertAdmin.setTitle(R.string.title);
            alertAdmin.setMessage(getString(R.string.blank_entry_not_allowed));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();
            return;
        }

        if ("".equalsIgnoreCase(et_contact_email.getText().toString())) {
            alertAdmin.setTitle(R.string.contact_email);
            alertAdmin.setMessage(getString(R.string.blank_entry_not_allowed));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();
            et_contact_email.setError(getString(R.string.please_fill_email));
            return;
        }

        String mobile = et_country_code.getText().toString().trim()
                + "-" + et_contact_number.getText().toString().trim();
        if (mobile.length() > 15 || mobile.length() < 5
                || "".equals(et_country_code.getText().toString().trim())
                || "".equals(et_contact_number.getText().toString().trim())) {

            alertAdmin.setTitle(R.string.phone_number);
            alertAdmin.setMessage(getString(R.string.please_enter_valid_Phone_mumber));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();

            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_Phone_mumber),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            et_contact_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return;
        }

        if ("".equalsIgnoreCase(et_report_bug.getText().toString())) {
            alertAdmin.setTitle(R.string.description);
            alertAdmin.setMessage(getString(R.string.blank_entry_not_allowed));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();
            return;
        }
/*
        if (sp_report_bug_bug_priority.getSelectedItemPosition() == 0) {

            TextView errorText = (TextView) sp_report_bug_bug_priority.getSelectedView();
            errorText.setError(getString(R.string.select));
            errorText.setTextColor(Color.RED);
            return;
        }*/

        if (sp_report_bug_bug_status.getSelectedItemPosition() == 0) {

            TextView errorText = (TextView) sp_report_bug_bug_status.getSelectedView();
            errorText.setError(getString(R.string.select));
            errorText.setTextColor(Color.RED);
            return;
        }
        getAuthToken();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 103 && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap b = BitmapFactory.decodeStream(inputStream);
                attachmentFile = getStringImage(b);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            /*   Uri uri = data.getData();

            long size = new File(getPath(uri)).length() / 1024;
            if (size > (1024 * 5)) {
                Config.responseSnackBarHandler("image size not more than 5 mb !", snackbar, appColor);
            } else {
                try {
                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    Bitmap b = BitmapFactory.decodeStream(inputStream);
                    String filePath = getPath(uri);
                    attachmentFile = getStringImage(b);
                    attachmentType = filePath.substring(filePath.lastIndexOf("/"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }


    private void sendBugReport(final String token) throws JSONException {
        progressbar.show();
        final JSONObject param = new JSONObject();
        param.put("bugCode", randomAlphaNumeric(6));
        param.put("OrganizationKey", "APIPERMISSION");
        param.put("reporterOrganizationKey", Gc.getSharedPreference(Gc.ERPINSTCODE, this));
        // param.put("applicationID", "schoolerp");
        param.put("applicationID", "SchoolAndroid");
        param.put("email", et_contact_email.getText().toString());

        param.put("mobile", Gc.makePhoneNumber(et_country_code.getText().toString().trim(), et_contact_number.getText().toString().trim()));
        param.put("databaseName", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
        param.put("reportDevice", "android");
        param.put("url", "google.com");
        param.put("title", et_report_bug_title.getText().toString().trim());
        param.put("openDate", (new SimpleDateFormat("dd-M-yyyy HH:mm:ss", Locale.ENGLISH)).format(new Date()));
        param.put("status", "new");
        param.put("bugPriority", "medium");
        param.put("bugStatus", sp_report_bug_bug_status.getSelectedItem().toString());
        param.put("description", et_report_bug.getText().toString().trim());

        param.put("developerUserID", "0");
        param.put("reporterUserID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));

        if (attachmentFile != null && !attachmentFile.isEmpty()) {
            final JSONObject media = new JSONObject();
            media.put("file", attachmentFile);
            media.put("Type", "jpeg");
            param.put("media", media);
        }

        et_report_bug_title.setText("");
        et_report_bug.setText("");

        //   RequestQueue rqueue = Volley.newRequestQueue(this);
        //  String bugUrl = Config.ORGANIZATION_ID_CHECK + "BugApi.php";
        String bugUrl = "http://bugsapi.zeroerp.in/BugApi";


        // Log.e("ReportQueryUrl", bugUrl);
        // Log.e("param", param.toString());

        JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, bugUrl, param
                , jsonObject -> {
            Log.e("responseReportQuery", jsonObject.toString());
            progressbar.cancel();
            if (jsonObject.optString("code").equalsIgnoreCase("200") ||
                    jsonObject.optString("code").equalsIgnoreCase("201")) {
                alertAdmin.setMessage(getString(R.string.report_submitted_successfully));
                alertAdmin.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                });
                alertAdmin.show();
            } else {
                alertAdmin.setMessage(getString(R.string.report_not_submitted_try_again));
                alertAdmin.setPositiveButton(getString(R.string.ok), (dialog, which) -> {});
                alertAdmin.show();
            }
        }, volleyError -> {
            progressbar.dismiss();
            Log.d("error volley", volleyError.toString());
            //    Toast.makeText(FCMIntentService.this, volleyError.toString(), Toast.LENGTH_LONG).show();
            alertAdmin.setMessage(getString(R.string.report_not_submitted_try_again));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new LinkedHashMap<>();
                header.put("Auth_Token", token);
                header.put("Content-Type", "application/json");
                header.put("OrganizationKey", "APIPERMISSION");
                return header;
            }


        };
        string_req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req);
    }

    public void getAuthToken() {
        JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.GET, "http://apiorgusrmgmt.zeroerp.in/LoginApi", null
                , jsonObject -> {
            Log.e("responseReportQuery", jsonObject.toString());
            progressbar.cancel();
            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                try {
                    sendBugReport(jsonObject.optJSONObject("result").optString("Token"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                alertAdmin.setMessage(getString(R.string.report_not_submitted_try_again));
                alertAdmin.setPositiveButton(getString(R.string.ok), null);
                alertAdmin.show();

            }

        }, volleyError -> {
            progressbar.dismiss();
            Log.d("error volley", volleyError.toString());
            //    Toast.makeText(FCMIntentService.this, volleyError.toString(), Toast.LENGTH_LONG).show();
            alertAdmin.setMessage(getString(R.string.report_not_submitted_try_again));
            alertAdmin.setPositiveButton(getString(R.string.ok), null);
            alertAdmin.show();

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new LinkedHashMap<>();
                header.put("Action", "Authentication");
                header.put("Content-Type", "application/json");
                try {
                    header.put("filter", String.valueOf(new JSONObject().put("Mobile", "9876543211").put("Password", "initial")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                header.put("OrganizationKey", "APIPERMISSION");
                header.put("App_key", "SUlQRjBvTEgvRGNjSzNSOUF2cjJWUT09");
                Log.e("header", header.toString());
                return header;
            }
        };
        string_req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req);

    }
}
