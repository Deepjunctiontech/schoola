package in.junctiontech.school.menuActions.aboutus;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12-04-2017.
 */

public class WebViewFragment extends Fragment {
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressDialog progressbar;
    private WebView web_view;
    private SwipeRefreshLayout  swipe_refresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // progressbar = new ProgressDialog(getActivity());
       // progressbar.setCancelable(false);
      //  progressbar.setMessage(getString(R.string.please_wait));
        progressbar =  ProgressDialog.show(getActivity(), "",  getString(R.string.please_wait), true);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    web_view.loadUrl(getTag());
                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.layout_about_us, container, false);
        swipe_refresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_web_view_swipe_refresh);
        swipe_refresh.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                web_view.loadUrl(getTag());
            }
        });
        web_view = (WebView) view.findViewById(R.id.fragment_web_view);

        web_view.clearCache(true);
        web_view.clearHistory();
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        web_view.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                    view.loadUrl(url);
              return true;
            }

            public void onPageFinished(WebView view, String url)
            {
                //Toast.makeText(myActivity.this, "Oh no!", Toast.LENGTH_SHORT).show();
                progressbar.dismiss();
                if (swipe_refresh.isRefreshing()) swipe_refresh.setRefreshing(false);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                String summary = "<html><body><strong>" + getString(R.string.time_is_over) + "</body></html>";
                web_view.loadData(summary, "text/html", "utf-8");
            }

        }); //End WebViewClient

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            web_view.loadUrl(getTag());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (progressbar.isShowing())
            progressbar.dismiss();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


}
