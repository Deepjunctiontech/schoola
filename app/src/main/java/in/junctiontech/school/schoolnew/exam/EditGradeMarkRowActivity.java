package in.junctiontech.school.schoolnew.exam;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.common.Gc;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class EditGradeMarkRowActivity extends AppCompatActivity {

    TextView tv_max_marks;
    Button btn_grade,btn_result;
    EditText et_range_start,et_range_end;

    GradeMarksEntity gradeMarks ;

    ArrayList<String> gradeNameList = new ArrayList<>();
    ArrayList<String> gradeIdList = new ArrayList<>();

    ArrayList<String> resultNameList = new ArrayList<>();
    ArrayList<String> resultIdList = new ArrayList<>();

    ArrayList<GradeMarksEntity> gradeMarksForMaxMarks = new ArrayList<>();


    int selectedGrade;
    int selectedResult;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_grade.setBackgroundColor(colorIs);
        btn_result.setBackgroundColor(colorIs);
        et_range_end.setTextColor(colorIs);
        et_range_start.setTextColor(colorIs);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_grade_mark_row);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        tv_max_marks = findViewById(R.id.tv_max_marks);
        btn_grade = findViewById(R.id.btn_grade);
        et_range_start = findViewById(R.id.et_range_start);
        et_range_end = findViewById(R.id.et_range_end);
        btn_result = findViewById(R.id.btn_result);

        tv_max_marks.setText(getIntent().getStringExtra("maxmarks"));

        gradeMarks = new Gson().fromJson(getIntent().getStringExtra("grademarkrow"),GradeMarksEntity.class);

        // the activity must meet these pre conditions to run
        if (gradeMarks == null) //entry check
            finish();

        gradeNameList = getIntent().getStringArrayListExtra("gradenamelist");
        gradeIdList = getIntent().getStringArrayListExtra("gradeidlist");

        resultNameList = getIntent().getStringArrayListExtra("resultnamelist");
        resultIdList = getIntent().getStringArrayListExtra("resultidlist");

        gradeMarksForMaxMarks = new Gson()
                .fromJson(getIntent().getStringExtra("grademarkslist"),
                        new TypeToken<List<GradeMarksEntity>>(){}.getType());

        btn_grade.setText(gradeMarks.gradeValue);
        btn_grade.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditGradeMarkRowActivity.this);
            ArrayAdapter<String> adapter =  new ArrayAdapter<>(EditGradeMarkRowActivity.this, android.R.layout.select_dialog_singlechoice);

            adapter.addAll(gradeNameList);
            builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            builder.setAdapter(adapter, (dialogInterface, i) -> {

                String newGrade = gradeNameList.get(i);
                for (GradeMarksEntity g : gradeMarksForMaxMarks){
                    if (g.gradeValue.equalsIgnoreCase(newGrade)){
                        if (!(g.gradeValue.equalsIgnoreCase(btn_grade.getText().toString()))) {
                            Config.responseSnackBarHandler(getString(R.string.you_entered_some_duplicate_grades_value),
                                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
                            return;
                        }
                    }

                }

                btn_grade.setText(gradeNameList.get(i));
                gradeMarks.gradeValue = gradeNameList.get(i);
                gradeMarks.grade = gradeIdList.get(i);
                selectedGrade = i;
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        });

        btn_result.setText(gradeMarks.resultValue);
        btn_result.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(EditGradeMarkRowActivity.this);
            ArrayAdapter<String> adapter =  new ArrayAdapter<>(EditGradeMarkRowActivity.this, android.R.layout.select_dialog_singlechoice);

            adapter.addAll(resultNameList);
            builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            builder.setAdapter(adapter, (dialogInterface, i) -> {
                btn_result.setText(resultNameList.get(i));
                gradeMarks.resultValue = resultNameList.get(i);
                gradeMarks.gradeResult = resultIdList.get(i);
                selectedResult = i;
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });

        et_range_start.setText(gradeMarks.rangeStart);

        et_range_start.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                gradeMarks.rangeStart = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_range_end.setText(gradeMarks.rangeEnd);
        et_range_end.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                gradeMarks.rangeEnd = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/8412328980", this, adContainer);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        if("".equalsIgnoreCase(et_range_end.getText().toString())){
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

            alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.grade_end_cannot_be_empty) + "<Br>"));
            alert1.setTitle(getString(R.string.information));
            alert1.setIcon(R.drawable.ic_clickhere);
            alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

            });
            alert1.show();
            Config.responseSnackBarHandler(getString(R.string.grade_end_cannot_be_empty),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }



        if (Integer.parseInt(et_range_end.getText().toString()) >= Integer.parseInt(et_range_start.getText().toString())){
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

            alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.grade_end_should_be_smaller) + "<Br>"));
            alert1.setTitle(getString(R.string.information));
            alert1.setIcon(R.drawable.ic_clickhere);
            alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

            });
            alert1.show();
            Config.responseSnackBarHandler(getString(R.string.grade_end_should_be_smaller),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }else{

            Intent resultIntent = new Intent();

            resultIntent.putExtra("grademarkrow", new Gson().toJson(gradeMarks));
            resultIntent.putExtra("rownumber",getIntent().getStringExtra("rownumber"));
            setResult(Activity.RESULT_OK, resultIntent);
    //        finish();
            super.onBackPressed();
        }
    }
}
