package in.junctiontech.school.schoolnew.messaging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.admin.msgpermission.ChatPermission;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.FCM.MyFCMPushReceiver;
import in.junctiontech.school.NewMessageNotification;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.ChatData;
import in.junctiontech.school.models.ChatList;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.schoolnew.messaging.MsgAdapters.ChatConversationAdapter;
import in.junctiontech.school.schoolnew.messenger.ConversationsActivity;

/**
 * Created by JAYDEVI BHADE on 12/28/2016.
 */

public class ChatActivity extends AppCompatActivity {

    private DbHandler db;
    private SharedPreferences sp;
    private Intent local_intent;
    private EditText et_message;
    private RecyclerView recycler_view_chat_layout;
    private StudentData dataObject;
    private ArrayList<ChatData> lastChatConversationList;
    private int groupID;
    private String loggedUserID;
    private String loggedUserName;
    private String loggedUserType, msgUserType;
    private ChatConversationAdapter mAdapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static boolean active = false;
    private Config configInstance;
    private boolean userStatus = false;
    private String userToken = "";
    private ArrayList<ChatPermission> chatPermissionList = new ArrayList<>();
    private boolean isPermissionToReply = false;
    private String userTypeReceiver = "";
    private ActionBar actionBar;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        //   getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        configInstance = (Config) getApplication();
        setContentView(R.layout.chat_layout);
      /*  AdView mAdView = (AdView) findViewById(R.id.adview_chat_layout);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);*/
        //this.context = this;

        /*   For Background display */
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_landscape);
        else
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_vartical);


      /*  if (!Config.checkInternet(this))
            mAdView.setVisibility(View.GONE);

        if (sp.getBoolean("addStatusGone", false))
            mAdView.setVisibility(View.GONE);*/

        db = DbHandler.getInstance(this);
//         Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.message);
//         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//          getSupportActionBar().setIcon(getResources().getDrawable( R.mipmap.ic_launcher));

        actionBar = getSupportActionBar();

        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


        local_intent = getIntent();
        setColorApp();
        /* ----   get Last Conversation list from data base -----*/

        loggedUserID = sp.getString("loggedUserID", "Not Found");
        loggedUserName = sp.getString("loggedUserName", "Not Found");
        loggedUserType = sp.getString("user_type", "Not Found").toUpperCase();

        groupID = local_intent.getIntExtra("groupID", 0);


        dataObject = (StudentData) local_intent.getSerializableExtra("data");
        msgUserType = local_intent.getStringExtra("userType");
        /*if ( msgUserType.equalsIgnoreCase("parent") ||msgUserType.equalsIgnoreCase("student")  )*/
        userTypeReceiver = local_intent.getStringExtra("userType");
        getSupportActionBar().setTitle("  " + userTypeReceiver + " : "
                + dataObject.getStudentName());

       /* else  getSupportActionBar().setTitle(*//*local_intent.getStringExtra("userType") + " : "
                +*//* dataObject.getStudentName());*/
        // if (Config.checkInternet(this))
        try {
            sendReqToKnowUserstatus();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        lastChatConversationList = db.getLastChatConversationList(groupID);

        et_message = (EditText) findViewById(R.id.et_message_chat);
        recycler_view_chat_layout = (RecyclerView) findViewById(R.id.recycler_view_chat_layout);
        setUpRecycler();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // boolean get = true;
                Log.e("chatGroupId", groupID + " ritu");
                Log.e("goupId", intent.getIntExtra("groupID", 0) + "chatId");
                if (intent.getAction().equals(Config.chatMessage)) {

                    // newtext push notification is received


                    if (intent.getBooleanExtra(Config.responseChatOrNot, false)) {
                       /* update message response to chat list*/
                        int msgId = 0;
                        try {
                            msgId = Integer.parseInt(intent.getStringExtra("msgId"));
                        } finally {

                        }
                        for (int i = lastChatConversationList.size() - 1; i >= 0; i--) {
                            ChatData model = lastChatConversationList.get(i);
                            if (model.getMsgId() == msgId) {
                                model.setStatus(intent.getStringExtra("status"));
                                break;
                            }
                        }

                        Log.e("ChatresponseChatOrNot", true + " ritu");
                    } else {
                        if (intent.getIntExtra("groupID", 0) == groupID) {
                        /* new message add to chat list*/
                            final ChatData chatDataObj = (ChatData) intent.getSerializableExtra(Config.chatDataObj);
                            lastChatConversationList.add(chatDataObj);
                            /* update received message status as read */
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    db.updateChatStatus(chatDataObj.getMsgId(), "read");
                                }

                            }, 10);


                            /* decrease number of message number from notification tray */
                            configInstance.decreaseNumberOfMessageCounter();
                            if (configInstance.getGroupIdsList().size() > 0)
                                configInstance.removeGroupIdChatList(groupID);
                        } else {
                            /* show notification if other user message comes*/
                            showNotification((ChatData) intent.getSerializableExtra(Config.chatDataObj));
                        }
                        Log.e("ChatresponseChatOrNot", false + " ritu");
                    }
                    mAdapter.notifyDataSetChanged();
                    if (mAdapter.getItemCount() > 1) {
                        recycler_view_chat_layout.getLayoutManager().smoothScrollToPosition(recycler_view_chat_layout, null, mAdapter.getItemCount());
                    }
                } else if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    try {
                        sendReqToKnowUserstatus();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendPendingMessage();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }, 10);
                }
            }

        };

      //  Log.e("ChatPermission",sp.getString("MessagePermissionResult", ""));

        /*************     reply Chat permission check        *********************/
        if (!(loggedUserType.equalsIgnoreCase("Administrator") || loggedUserType.equalsIgnoreCase("Admin"))) {
            if (!sp.getString("MessagePermissionResult", "").equalsIgnoreCase("")) {
                ChatPermission chatPermissionData = (new Gson()).fromJson(sp.getString("MessagePermissionResult", ""), new TypeToken<ChatPermission>() {
                }.getType());
                chatPermissionList = chatPermissionData.getResult();
                String userTypeReceiverTemp = userTypeReceiver.equalsIgnoreCase("parent")?"parents":userTypeReceiver;
                for (ChatPermission chatObj : chatPermissionList) {

                    if (chatObj.getToUserType().equalsIgnoreCase(userTypeReceiverTemp)) {
                        isPermissionToReply = true;
                        break;
                    }
                }
            }

        } else isPermissionToReply = true;

        /**********************************************************/

    }

    public Bitmap createCircleBitmap(Bitmap bitmapimg){
        Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
                bitmapimg.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
                bitmapimg.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmapimg.getWidth() / 2,
                bitmapimg.getHeight() / 2, bitmapimg.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmapimg, rect, rect, paint);
        return output;
    }

    private void sendPendingMessage() throws JSONException {
        for (final ChatData chatObject : lastChatConversationList) {
            if (chatObject.getStatus().equalsIgnoreCase("pending")) {
                String db_name = sp.getString("organization_name", "Not Found");

                JSONArray jsonArray = new JSONArray();

                JSONObject map = new JSONObject();
                map.put("app_msg_s_no", chatObject.getMsgId());
                map.put("sender_id", chatObject.getUserID());
                map.put("sender_name", chatObject.getUserName());
                map.put("senderType", chatObject.getUserType());
                map.put("msg", chatObject.getMsg());
                map.put("time", chatObject.getCreatedAtMsg());
                map.put("receiver_id", dataObject.getStudentID());
                map.put("receiver_type", msgUserType);
                // map.put("image",);
                jsonArray.put(map);

                final JSONObject param = new JSONObject();
                param.put("DB_Name", db_name);
                param.put("GroupData", jsonArray);

                // RequestQueue rqueue = Volley.newRequestQueue(this);
                String url = sp.getString("HostName", "Not Found")
                        + sp.getString(Config.applicationVersionName, "Not Found")
                        + "ChatDetails.php?action=insertmsg";
                Log.d("url", url);
                Log.d("param", param.toString());

                JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST, url, param
                        , new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject_response) {
                        Log.d("chatMsgResponse", jsonObject_response.toString());
                        if (jsonObject_response.optInt("GcmResponse") > 0) {
                            chatObject.setStatus("send");

                            mAdapter.notifyItemChanged(lastChatConversationList.indexOf(chatObject));


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    db.updateChatStatus(chatObject.getMsgId(), "send");
                                }


                            }, 10);
                        }

                    }


                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("action=insertmsg", volleyError.toString());
                        //   Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
                // disabling retry policy so that it won't make
                // multiple http calls

                int socketTimeout = 0;
                string_req.setRetryPolicy(new DefaultRetryPolicy(socketTimeout,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                //rqueue.add(string_req);
                AppRequestQueueController.getInstance(this).addToRequestQueue(string_req);
            }
        }
    }


    private void showNotification(ChatData serializableExtra) {
        NotificationCompat.Builder mybuilder;

        //   configInstance.removeGroupIdChatList(groupID);
        configInstance.increaseNumberOfMessageCounter();


        String title = "";
        String tiker = "", lastMessage = "";


        //   if (configInstance.getGroupIdsList().size() == 1) {
        title = serializableExtra.getUserName();
        lastMessage = serializableExtra.getMsg();
        //  } else if (configInstance.getGroupIdsList().size() > 1) {
        //  title = "MySchool";
        tiker = configInstance.getNumberOfMessageCounter() + " " + getString(R.string.message) +
                " From  " + configInstance.getGroupIdsList().size() + " Conversations";
        //  }

        //   if (configInstance.getNumberOfMessageCounter()>0){
        NewMessageNotification.notify(this, lastMessage, title, tiker, configInstance.getNumberOfMessageCounter());

//            mybuilder = new NotificationCompat.Builder(this).
//                    setSmallIcon(R.drawable.school_logo)
//                    .setContentTitle(serializableExtra.getUserName()).
//                            setContentText(lastMessage)
//                    .setNumber(configInstance.getNumberOfMessageCounter()).setAutoCancel(true);
//
//            // }
//            Intent myNotificationIntent = new Intent(this, ConversationListActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
//            taskStackBuilder.addParentStack(MainActivity.class);
//            taskStackBuilder.addNextIntent(myNotificationIntent);
//
//            PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            mybuilder.setContentIntent(pendingIntent);
//
//            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            Notification note = mybuilder.build();
//            note.defaults = Notification.DEFAULT_VIBRATE;
//            note.defaults = Notification.DEFAULT_SOUND;
//            manager.notify(1, note);
//        }

    }

    private void setUpRecycler() {
        mAdapter = new ChatConversationAdapter(this, lastChatConversationList, loggedUserID, loggedUserType);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setStackFromEnd(true);
        recycler_view_chat_layout.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recycler_view_chat_layout.setItemAnimator(itemAnimator);

        //  recycler_view_chat_layout.setItemAnimator(newtext DefaultItemAnimator());
//        if (mAdapter.getItemCount() > 1) {
//            mLayoutManager.smoothScrollToPosition(recycler_view_chat_layout, null, mAdapter.getItemCount() - 1);
//        }
        recycler_view_chat_layout.setAdapter(mAdapter);
    }


    private void sendReqToKnowUserstatus() throws JSONException {
      if (!userTypeReceiver.equalsIgnoreCase("class")){
        String db_name = sp.getString("organization_name", "Not Found");
        String userTypeSender = getIntent().getStringExtra("userType");
        final JSONObject param = new JSONObject();
        param.put("DB_Name", db_name);
        param.put("receiverId", (userTypeSender.equalsIgnoreCase("Administrator") || userTypeSender.equalsIgnoreCase("admin")) ? dataObject.getStudentName() : dataObject.getStudentID());
        param.put("receiver_type", getIntent().getStringExtra("userType"));

        //   RequestQueue rqueue = Volley.newRequestQueue(this);
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ChatDetails.php?action=userStatus";
        Log.d("url", url);
        Log.d("param", param.toString());
        JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST,
                url, param
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject s) {
                Log.d("userStatus", s.toString());
                String status = s.optString("result", "not available");
                if (sp.getBoolean("toastVisibility", false))
                    Toast.makeText(ChatActivity.this, status, Toast.LENGTH_LONG).show();
                getSupportActionBar().setSubtitle("  " + status);

                userToken = s.optString("token", "");
                if (status.equalsIgnoreCase("available"))
                    userStatus = true;
                else userStatus = false;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            sendPendingMessage();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, 10);

                         Glide
                        .with(getApplicationContext())
                        .load(s.optString("image"))
                        .into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                if (!isFinishing()) {

//                                    Drawable d =new BitmapDrawable(getResources(), createCircleBitmap(resource)); /*new BitmapDrawable(getResources(), resource);*/
//                                    actionBar.setLogo(d);
                                }
                            }
                        }) ;
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("UserAvailability", volleyError.toString());
                //   Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    Toast.makeText(ChatActivity.this, getString(R.string.internet_not_available_please_check_your_internet_connectivity)
                            , Toast.LENGTH_LONG).show();

                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        });
        // disabling retry policy so that it won't make
        // multiple http calls

        int socketTimeout = 0;
        string_req.setRetryPolicy(new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req);


        db.close();}
    }

    public void sendMessage(View v) throws JSONException {
        if (!isPermissionToReply) {
            Toast.makeText(this, getString(R.string.you_dont_have_permission_to_send_messages_to) + " " + userTypeReceiver, Toast.LENGTH_SHORT).show();
        } else {
            String et_msg = et_message.getText().toString();
            if (et_msg.equalsIgnoreCase("") || et_msg == null) ;
            else {
                Bundle b = db.saveChatConversations(groupID,
                        loggedUserID,
                        loggedUserName,
                        loggedUserType,
                        et_msg
                );
                et_message.setText("");

         /* ---   set the GroupId into local variable ----*/
                groupID = b.getInt("groupId");
                Log.e("groupId", groupID + "");


                ChatData chatObject = new ChatData(
                        groupID,
                        Integer.parseInt(b.getString("msgId")),
                        loggedUserID,
                        loggedUserType,
                        loggedUserName,
                        et_msg,
                        Config.msgDateFormat.format(new Date()),
                        "pending",
                        false
                );
                lastChatConversationList.add(chatObject);
                Log.e("loggedUserName", sp.getString("loggedUserName", "Not Found"));
                Log.e("userType", sp.getString("user_type", "Not Found").toUpperCase());

                mAdapter.notifyDataSetChanged();
                if (mAdapter.getItemCount() > 1) {
                    recycler_view_chat_layout.getLayoutManager().smoothScrollToPosition(recycler_view_chat_layout, null, mAdapter.getItemCount());
                }

                //   if (!local_intent.getStringExtra("userType").equalsIgnoreCase("Class"))
                //  if (userToken.equalsIgnoreCase("")||  msgUserType.equalsIgnoreCase("Class"))
                sendMsgToUser(chatObject);
                //else
                //  sendDirectMsgFirebase(chatObject);
            }
        }

    }

    private void sendMsgToUser(final ChatData chatObject) throws JSONException {
        String db_name = sp.getString("organization_name", "Not Found");

        JSONArray jsonArray = new JSONArray();

        //   for (ChatData chatDataObj :lastChatConversationList) {
        JSONObject map = new JSONObject();
        map.put("app_msg_s_no", chatObject.getMsgId());
        map.put("sender_id", chatObject.getUserID());
        map.put("sender_name", chatObject.getUserName());
        map.put("senderType", chatObject.getUserType());
        map.put("msg", chatObject.getMsg());
        map.put("time", chatObject.getCreatedAtMsg());
        map.put("receiver_id", dataObject.getStudentID());
        map.put("receiver_type", msgUserType);
        // map.put("image",);
        jsonArray.put(map);

        final JSONObject param = new JSONObject();
        param.put("DB_Name", db_name);
        param.put("GroupData", jsonArray);

        // RequestQueue rqueue = Volley.newRequestQueue(this);
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ChatDetails.php?action=insertmsg";
        Log.d("url", url);
        Log.d("param", param.toString());

        JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST,
                url, param
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject_response) {
                Log.d("chatMsgResponse", jsonObject_response.toString());
                if (jsonObject_response.optString("GcmResponse").equalsIgnoreCase("1")) {
                    chatObject.setStatus("send");

                    mAdapter.notifyItemChanged(lastChatConversationList.indexOf(chatObject));
                    db.updateChatStatus(chatObject.getMsgId(), "send");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendPendingMessage();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }, 10);

                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("action=insertmsg", volleyError.toString());
                //   Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
            }
        });
        // disabling retry policy so that it won't make
        // multiple http calls

        int socketTimeout = 0;
        string_req.setRetryPolicy(new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //rqueue.add(string_req);
        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req,chatObject.getMsgId()+"");

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ConversationsActivity.class);
        if (lastChatConversationList.size() > 0 &&
                local_intent.getBooleanExtra("refresh", false)) {
            ChatList chatListObj = new ChatList(groupID,
                    dataObject.getStudentName(),
                    dataObject.getStudentID(),
                    msgUserType,
                    lastChatConversationList.get(lastChatConversationList.size() - 1).getMsg(),
                    lastChatConversationList.get(lastChatConversationList.size() - 1).getCreatedAtMsg(),
                    0
            );

            intent.putExtra("data", chatListObj);
            setResult(RESULT_OK, intent);

        } else {
            setResult(RESULT_CANCELED, intent);

        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_landscape);
        else
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_vartical);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ChatActivity", "onResume");
        // registering the receiver for newtext notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.chatMessage));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));

        // clearing the notification tray
        MyFCMPushReceiver.clearNotifications(this);

        ArrayList<ChatData> lastChatConversationListLocal = db.getLastChatConversationList(groupID);
        if (lastChatConversationList.size() != lastChatConversationListLocal.size()) {
            mAdapter.updateList(lastChatConversationList = lastChatConversationListLocal);
         /* if (mAdapter.getItemCount() > 1) {
              recycler_view_chat_layout.getLayoutManager().smoothScrollToPosition(recycler_view_chat_layout, null, mAdapter.getItemCount());
          }*/
        }
    }

    @Override
    protected void onPause() {
        Log.e("ChatActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void onRefreshActivity() {
        Log.e("onRefreshChatActivity", "onRefreshActivity");
        this.lastChatConversationList = db.getLastChatConversationList(groupID);
        mAdapter.updateList(this.lastChatConversationList);
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getItemCount() > 1) {
            recycler_view_chat_layout.getLayoutManager().smoothScrollToPosition(recycler_view_chat_layout, null, mAdapter.getItemCount());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;

    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    private void sendDirectMsgFirebase(final ChatData chatObject) throws JSONException {
        JSONObject map = new JSONObject();
        map.put("app_msg_s_no", chatObject.getMsgId());
        map.put("senderid", chatObject.getUserID());
        map.put("senderName", chatObject.getUserName());
        map.put("senderType", chatObject.getUserType());
        map.put("msg", chatObject.getMsg());
        map.put("timestamp", chatObject.getCreatedAtMsg());
        map.put("receiver_id", dataObject.getStudentID());
        map.put("receiver_type", msgUserType);


        JSONObject jsonObjectNotification = new JSONObject();
        jsonObjectNotification.put("title", "MySchool Messages: " + chatObject.getUserName());
        jsonObjectNotification.put("body", chatObject.getMsg());

        JSONObject jsonObjectData = new JSONObject();
        jsonObjectData.put("title", "MySchool Messages");
        jsonObjectData.put("message", map);


       /* if (tokenList.length()==1)
        jsonObject.put("to", "erOgLsDQck8:APA91bGF8L3FBqdLbz2XjAIznSfRVEjdfR-lot8y5ONOqeDd5NILLgqYZheh6lvaUFF_VjpRfve4kBIlFCjEHOggAKIBzBiKzSkRgh8T1L4jX4j3i08qAK1HzRa_NMCOwBQdBp7FOrIE");
        else*/
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("to", userToken);
        jsonObject.put("content_available", true);
        jsonObject.put("priority", "high");
        jsonObject.put("notification", jsonObjectNotification);
        jsonObject.put("data", jsonObjectData);

        Log.e("DATA", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/fcm/send",
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject_response) {
                        Log.e("NOTIResponse", jsonObject_response.toString());
                        Log.d("chatMsgResponse", jsonObject_response.toString());
                        if (jsonObject_response.optInt("success") >= 1) {
                            chatObject.setStatus("send");

                            mAdapter.notifyItemChanged(lastChatConversationList.indexOf(chatObject));
                            db.updateChatStatus(chatObject.getMsgId(), "send");
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        sendPendingMessage();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }


                            }, 10);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("NOTIResponse", volleyError.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new LinkedHashMap<>();
                header.put("project_id", "1070281245079");
                header.put("Content-Type", "application/json");
                header.put("Authorization", "key=AAAA-TG7wZc:APA91bH_Bk8AEieFr82P7MP5BgRCyrIVeTNbsMViu3KDnpOXW-SkzYHqdsOvS-SQearc5IqOeKxWkWFu5rtud1pAO2OpY6Nh7q07QIquic8pA2PvIPgq_0tf9CH4e8vvPSsNutavJTo3t2aUxaFIJRT_n-Mcq2erwg");
                return header;
            }


        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }

}
