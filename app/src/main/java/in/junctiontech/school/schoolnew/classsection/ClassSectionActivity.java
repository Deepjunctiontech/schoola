package in.junctiontech.school.schoolnew.classsection;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassStudentCountTupple;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.DB.viewmodel.SchoolClassViewModel;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.communicate.SendSMSActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.classfee.ClassFeeActivity;
import in.junctiontech.school.schoolnew.model.SchoolClasses;

/**
 * Created by JAYDEVI BHADE on 18/1/2017.
 */

public class ClassSectionActivity extends AppCompatActivity {

    MainDatabase mDb;
    SchoolClassViewModel mSchoolClassViewModel;

    private ProgressDialog progressbar;

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    FloatingActionButton fb_class_add;
    TextView tv_class_label;

    String action_requested;

    private ArrayList<SchoolClassEntity> schoolClassNames = new ArrayList<>();
    HashMap<String, String> stCount = new HashMap<>();

    int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_class_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        tv_class_label.setTextColor(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_create_school_class);

        mDb = MainDatabase.getDatabase(this);

        mSchoolClassViewModel = ViewModelProviders.of(this).get(SchoolClassViewModel.class);

        action_requested = Strings.nullToEmpty(getIntent().getStringExtra("action"));

        tv_class_label = findViewById(R.id.tv_class_label);


        if (Gc.getSharedPreference(Gc.CLASSEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        fb_class_add = findViewById(R.id.fb_class_add);
        fb_class_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, ClassSectionActivity.this)
                        .equalsIgnoreCase(Gc.TRUE))
                    showDialogForClassCreate();
                else
                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                            findViewById(R.id.classlayout),
                            R.color.fragment_first_green);
            }
        });

        mRecyclerView = findViewById(R.id.recycler_view_school_class_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mAdapter = new ClassListAdapter();
        mRecyclerView.setAdapter(mAdapter);
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4974263968", this, adContainer);
        }

    }

    void showDialogForClassCreate() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_update_class);
        dialog.setTitle(R.string.create_class);


        final EditText editText = dialog.findViewById(R.id.et_class_name);
        Button btnSave = dialog.findViewById(R.id.save);
        btnSave.setBackgroundColor(colorIs);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ("".equals(editText.getText().toString().trim().toUpperCase())) {
                    editText.setError(getResources().getString(R.string.error_field_required),
                            getResources().getDrawable(R.drawable.ic_alert));
                    return;
                }

                try {
                    createNewClass(editText.getText().toString().trim().toUpperCase());
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        Button btnCancel = dialog.findViewById(R.id.cancel);
        btnCancel.setBackgroundColor(colorIs);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void createNewClass(String className) throws JSONException {
        // Create a new class if Validation Succeeds

        progressbar.show();
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "classSection/classSectionFee";

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String parsedDate = formatter.format(new Date());

        final JSONObject param = new JSONObject();
        param.put("ClassName", className.trim().toUpperCase());
        param.put("Session", Gc
                .getSharedPreference(Gc.APPSESSION, this));
        param.put("DOE", parsedDate);
        param.put("ClassStatus", "Active");

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                job -> {
                    progressbar.cancel();
                    Log.i("Success Class", job.toString());
                    String code = job.optString("code");

                    switch (code) {
                        case Gc.APIRESPONSE200:
                            final SchoolClassEntity schoolClassEntity;
                            try {
                                JSONObject resultjobj = job.getJSONObject("result");

                                JSONArray classjobj = resultjobj.getJSONArray("classSections");

                                JSONObject jsonObject = classjobj.getJSONObject(0);

                                JSONArray section = jsonObject.getJSONArray("section");

                                JSONObject jsonObjects = section.getJSONObject(0);

                                JSONObject classFeesObj = jsonObjects.getJSONObject("classFees");

                                JSONArray classFees = new JSONArray();

                                classFees.put(classFeesObj);

                                SchoolClasses schoolClasses = new Gson().fromJson(classjobj.getString(0), SchoolClasses.class);
                                schoolClassEntity = new SchoolClassEntity();
                                schoolClassEntity.ClassId = schoolClasses.ClassId;
                                schoolClassEntity.ClassName = schoolClasses.ClassName;
                                schoolClassEntity.ClassStatus = schoolClasses.ClassStatus;
                                schoolClassEntity.Session = schoolClasses.Session;
                                schoolClassEntity.DOE = schoolClasses.DOE;

                                new Thread(() -> mDb.schoolClassModel().insertSchoolclass(schoolClassEntity)).start();

                                final ArrayList<SchoolClassSectionEntity> schoolClassSectionEntity = new Gson()
                                        .fromJson(jsonObject.getString("section"), new TypeToken<List<SchoolClassSectionEntity>>() {
                                        }.getType());

                                new Thread(() -> {
                                    ArrayList<SchoolClassSectionEntity> sc = new ArrayList<>();
                                    sc.add(schoolClassSectionEntity.get(0));
                                    mDb.schoolClassSectionModel().insertSchoolClassSection(sc);
                                    HashMap<String, String> setDefaults = new HashMap<String, String>();
                                    setDefaults.put(Gc.CLASSEXISTS, Gc.EXISTS);
                                    Gc.setSharedPreference(setDefaults, ClassSectionActivity.this);
                                }).start();

                                if (classFees.length() > 0) {
                                    HashMap<String, String> setDefaults = new HashMap<>();
                                    setDefaults.put(Gc.CLASSFEESEXISTS, Gc.EXISTS);
                                    Gc.setSharedPreference(setDefaults, ClassSectionActivity.this);
                                    final ArrayList<ClassFeesEntity> classFeesEntities = MapModelToEntity.mapClassFeeModelToEntity(classFees);

                                    new Thread(() -> {
                                        mDb.classFeesModel().deleteClassFeeForSection(schoolClassSectionEntity.get(0).SectionId);
                                        mDb.classFeesModel().insertClassFee(classFeesEntities);
                                    }).start();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Config.responseSnackBarHandler(getString(R.string.success),
                                    findViewById(R.id.classlayout),
                                    R.color.fragment_first_green);
                            break;

                        case Gc.APIRESPONSE401:

                            HashMap<String, String> setDefaults1 = new HashMap<>();
                            setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults1, ClassSectionActivity.this);

                            Intent intent1 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);

                            finish();

                            break;

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, ClassSectionActivity.this);

                            Intent intent2 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);

                            finish();

                            break;

                        default:
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.classlayout),
                                    R.color.fragment_first_blue);
                    }
                },
                error -> {
                    progressbar.cancel();
                    VolleyLog.e("Error: ", error.getMessage());
                    Config.responseVolleyErrorHandler(ClassSectionActivity.this, error, findViewById(R.id.classlayout));

                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);

    }

    void updateClassToServer(String classUpdate, String classid) throws JSONException {
        // update the class
        progressbar.show();
        final Map<String, Object> urlparam = new LinkedHashMap<>();
        urlparam.put("ClassId", classid);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "classSection/manageClass/" + new JSONObject(urlparam);


        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String parsedDate = formatter.format(new Date());

        final JSONObject param = new JSONObject();
        param.put("ClassName", classUpdate);
        param.put("Session", Gc
                .getSharedPreference(Gc.APPSESSION, this));
        param.put("DOE", parsedDate);
        param.put("ClassStatus", "Active");

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressbar.cancel();
                        String code = job.optString("code");

                        switch (code) {
                            case Gc.APIRESPONSE200:
                                final SchoolClassEntity schoolClassEntity;

                                JSONObject resultjobj = null;
                                try {
                                    resultjobj = job.getJSONObject("result");
                                    JSONArray classjobj = resultjobj.getJSONArray("classSections");
                                    SchoolClasses schoolClasses = new Gson().fromJson(classjobj.getString(0), SchoolClasses.class);

                                    schoolClassEntity = new SchoolClassEntity();
                                    schoolClassEntity.ClassId = schoolClasses.ClassId;
                                    schoolClassEntity.ClassName = schoolClasses.ClassName;
                                    schoolClassEntity.ClassStatus = schoolClasses.ClassStatus;
                                    schoolClassEntity.Session = schoolClasses.Session;
                                    schoolClassEntity.DOE = schoolClasses.DOE;

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mDb.schoolClassModel().updateSchoolClass(schoolClassEntity);
                                        }
                                    }).start();
                                    Config.responseSnackBarHandler(getString(R.string.success),
                                            findViewById(R.id.classlayout),
                                            R.color.fragment_first_green);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, ClassSectionActivity.this);

                                Intent intent1 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, ClassSectionActivity.this);

                                Intent intent2 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                            default:
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.classlayout),
                                        R.color.fragment_first_blue);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.cancel();
                        VolleyLog.e("Class createError:", error.getMessage());
                        Config.responseVolleyErrorHandler(ClassSectionActivity.this, error, findViewById(R.id.classlayout));

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);

    }

    @Override
    protected void onResume() {
        super.onResume();

        mSchoolClassViewModel.getSchoolClassesLive().observe(this, new Observer<List<SchoolClassEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolClassEntity> schoolClassEntities) {
                schoolClassNames.clear();
                schoolClassNames.addAll(schoolClassEntities);

                mDb.schoolStudentModel().getNumberOfStudents().observe(ClassSectionActivity.this, new Observer<List<ClassStudentCountTupple>>() {
                    @Override
                    public void onChanged(@Nullable List<ClassStudentCountTupple> classStudentCounts) {
                        stCount.clear();
                        for (ClassStudentCountTupple st : Objects.requireNonNull(classStudentCounts)) {
                            stCount.put(st.ClassId, st.stcount);
                        }

                        mAdapter.notifyDataSetChanged();

                        mDb.schoolStudentModel().getNumberOfStudents().removeObservers(ClassSectionActivity.this);
                    }
                });
                mAdapter.notifyDataSetChanged();
            }
        });


        if (schoolClassNames.size() == 0) {
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.classlayout), R.string.click_to_add_class,
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(3000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        } else if (!(Gc.getSharedPreference(Gc.CLASSEXISTS, this).equalsIgnoreCase(Gc.EXISTS))) {

            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.classlayout),
                    "Section for Class does not Exist!! \n Select Class to add Sections",
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        }

    }

    @Override
    protected void onPause() {
        mSchoolClassViewModel.getSchoolClassesLive().removeObservers(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (!(Strings.nullToEmpty(getIntent().getStringExtra(Gc.SETUP)).equals(Gc.SETUP))) {
            super.onBackPressed();
            return;
        }


        if (schoolClassNames.size() == 0) {
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.classlayout), R.string.class_is_not_available_please_first_add_class,
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(4000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        } else if (!(Gc.getSharedPreference(Gc.CLASSEXISTS, this).equalsIgnoreCase(Gc.EXISTS))) {

            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.classlayout),
                    "Section for Class does not Exist!! \n Select Class to add Sections",
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(4000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if (Gc.getSharedPreference(Gc.CLASSEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            menu.findItem(R.id.menu_next).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void updateClass(final String classUpdate, final String classid) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_update_class);
        dialog.setTitle(R.string.update_class);

        final EditText editText = dialog.findViewById(R.id.et_class_name);
        editText.setText(classUpdate);
        Button btnSave = dialog.findViewById(R.id.save);
        btnSave.setBackgroundColor(colorIs);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editText.getText().toString().trim().equals("")) {
                    editText.setError(getResources().getString(R.string.error_field_required),
                            getResources().getDrawable(R.drawable.ic_alert));
                    return;
                }

                try {
                    updateClassToServer(editText.getText().toString(), classid);
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Button btnCancel = dialog.findViewById(R.id.cancel);
        btnCancel.setBackgroundColor(colorIs);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void deleteClass(String classId) {

        AlertDialog.Builder alert1 = new AlertDialog.Builder(ClassSectionActivity.this);
        alert1.setTitle("Are You Sure, Want To Delete This Record ?");
        alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
            classDelete(classId);
        });
        alert1.setNegativeButton(getString(R.string.cancel), (dialog, which) -> {

        });

        alert1.show();
    }

    private void classDelete(String classId) {

        progressbar.show();
        final Map<String, Object> urlparam = new LinkedHashMap<>();
        urlparam.put("ClassId", classId);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "classSection/classSection/" + new JSONObject(urlparam);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(),
                job -> {
                    progressbar.cancel();
                    String code = job.optString("code");

                    switch (code) {
                        case Gc.APIRESPONSE200:
                            new Thread(() -> {
                                mDb.schoolClassModel().deleteClass(classId);
                                mDb.schoolClassSectionModel().deleteClassSections(classId);
                                mDb.classFeesModel().deleteClassFee(classId);
                            }).start();

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.classlayout),
                                    R.color.fragment_first_green);
                            break;

                        case Gc.APIRESPONSE401:

                            HashMap<String, String> setDefaults1 = new HashMap<>();
                            setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults1, ClassSectionActivity.this);

                            Intent intent1 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);

                            finish();

                            break;

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, ClassSectionActivity.this);

                            Intent intent2 = new Intent(ClassSectionActivity.this, AdminNavigationDrawerNew.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);

                            finish();

                            break;

                        default:
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.classlayout),
                                    R.color.fragment_first_blue);
                    }
                },
                error -> {
                    progressbar.cancel();
                    VolleyLog.e("Class createError:", error.getMessage());
                    Config.responseVolleyErrorHandler(ClassSectionActivity.this, error, findViewById(R.id.classlayout));

                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);
    }

    public class ClassListAdapter extends RecyclerView.Adapter<ClassListAdapter.MyViewHolder> {

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.class_section_list, parent, false);

            return new MyViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            holder.tv_item_class_name.setText(schoolClassNames.get(position).ClassName);

            holder.tv_student_count.setText((stCount.get(schoolClassNames.get(position).ClassId) == null) ? "0" : (stCount.get(schoolClassNames.get(position).ClassId)));

            holder.tv_item_class_name.setTextColor(colorIs);
        }

        @Override
        public int getItemCount() {
            return schoolClassNames.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_item_class_name, tv_student_count;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_item_class_name = itemView.findViewById(R.id.tv_item_class_name);
                tv_student_count = itemView.findViewById(R.id.tv_student_count);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder choices = new AlertDialog.Builder(ClassSectionActivity.this);
                        String[] aa = new String[]
                                {
                                        getResources().getString(R.string.section_list),
                                        getResources().getString(R.string.send_sms),
                                        getResources().getString(R.string.rename),
                                        getResources().getString(R.string.delete)
                                };

                        choices.setItems(aa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which) {
                                    case 0:
                                        Intent intent = new Intent(ClassSectionActivity.this, SectionActivity.class);
                                        intent.putExtra("ClassName", schoolClassNames.get(getAdapterPosition()).ClassName);
                                        intent.putExtra("ClassId", schoolClassNames.get(getAdapterPosition()).ClassId);
                                        startActivity(intent);
                                        break;
                                    case 1:
                                        if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, ClassSectionActivity.this).equalsIgnoreCase(Gc.TRUE))
                                            startActivity(new Intent(ClassSectionActivity.this, SendSMSActivity.class)
                                                    .putExtra("ClassId", schoolClassNames.get(getAdapterPosition()).ClassId));
                                        else
                                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                    findViewById(R.id.classlayout),
                                                    R.color.fragment_first_blue);
                                        break;

                                    case 2:
                                        if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, ClassSectionActivity.this).equalsIgnoreCase(Gc.TRUE))
                                            updateClass(schoolClassNames.get(getAdapterPosition()).ClassName,
                                                    schoolClassNames.get(getAdapterPosition()).ClassId);
                                        else
                                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                    findViewById(R.id.classlayout),
                                                    R.color.fragment_first_blue);
                                        break;
                                    case 3:
                                        if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, ClassSectionActivity.this).equalsIgnoreCase(Gc.TRUE))


                                            deleteClass(schoolClassNames.get(getAdapterPosition()).ClassId);
                                        else
                                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                    findViewById(R.id.classlayout),
                                                    R.color.fragment_first_blue);
                                        break;

                                }
                            }
                        });
                        if ("".equalsIgnoreCase(action_requested))
                            choices.show();
                        else if ("sms".equalsIgnoreCase(action_requested)) {
                            if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, ClassSectionActivity.this).equalsIgnoreCase(Gc.TRUE))
                                startActivity(new Intent(ClassSectionActivity.this, SendSMSActivity.class)
                                        .putExtra("ClassId", schoolClassNames.get(getAdapterPosition()).ClassId));
                            else
                                Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                        findViewById(R.id.classlayout),
                                        R.color.fragment_first_blue);
                        }
                    }
                });
            }
        }

    }

}
