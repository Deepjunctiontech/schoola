package in.junctiontech.school.schoolnew.communicate;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class CreateNoticeBoardActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;
    EditText et_notice_description;
   /* RadioButton rb_notice_active, rb_notice_inactive;
    Button btn_pick_image;
    ImageView iv_notice_image;*/

    RadioGroup rg_type_of_notice, rg_type;

    RadioButton rb_notice_circular, rb_notice_class_diary, rb_notice_syllabus, rb_every_one, rb_only_staff, rb_only_student, rb_select_class;

    TextView tv_typeofnotice, tv_sendnoticeto;

    Button btn_image_pdf, expiry_date, btn_class_sections;

    TextView tv_selected_section;

    NoticeBoardEntity noticeUpdate;

    private RadioButton radioButton, radioType;

    private int colorIs;

    ArrayList<String> sectionList = new ArrayList<>();

    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    ArrayList<String> selectedClassSections = new ArrayList<>();

    ImageView iv_notice_image;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        tv_sendnoticeto.setTextColor(colorIs);
        tv_typeofnotice.setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notice_board);

        mDb = MainDatabase.getDatabase(this);

      /*  rb_notice_active = findViewById(R.id.rb_notice_active);
        rb_notice_inactive = findViewById(R.id.rb_notice_inactive);
        btn_pick_image = findViewById(R.id.btn_pick_image);
        btn_pick_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        iv_notice_image = findViewById(R.id.iv_notice_image);
*/
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initializeInputFields();

        if (!("".equalsIgnoreCase(Strings.nullToEmpty(getIntent().getStringExtra("notice"))))) {
            noticeUpdate = new Gson().fromJson(getIntent().getStringExtra("notice"), NoticeBoardEntity.class);
            et_notice_description.setText(noticeUpdate.Description);
            expiry_date.setText(noticeUpdate.Date);
            rg_type_of_notice.setVisibility(View.GONE);

            if(!noticeUpdate.Flag.isEmpty()){
                switch (noticeUpdate.Flag) {
                    case "Circular":
                        rb_notice_circular.setChecked(true);
                        break;
                    case "Class Diary":
                        rb_notice_class_diary.setChecked(true);
                        break;
                    case "Syllabus":
                        rb_notice_syllabus.setChecked(true);
                        break;
                }
                TypeOfNotice();
            }

            if(!noticeUpdate.Type.isEmpty()){
                switch (noticeUpdate.Type) {
                    case "Every One":
                        rb_every_one.setChecked(true);
                        break;
                    case "Staff Only":
                        rb_only_staff.setChecked(true);

                        break;
                    case "Student Only":
                        rb_only_student.setChecked(true);

                        break;
                    case "Class":
                        rb_select_class.setChecked(true);
                        break;
                }
                notificationFor();
            }

            if(noticeUpdate.Sections !=null && !noticeUpdate.Sections.isEmpty()){
                selectedClassSections.addAll(Arrays.asList(noticeUpdate.Sections.split(",")));
            }
        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2717628104", this, adContainer);
        }
    }

    private void initializeInputFields() {
        progressBar = findViewById(R.id.progressBar);
        et_notice_description = findViewById(R.id.et_notice_description);
        tv_typeofnotice = findViewById(R.id.tv_typeofnotice);
        rg_type_of_notice = findViewById(R.id.rg_type_of_notice);

        tv_sendnoticeto = findViewById(R.id.tv_sendnoticeto);
        rg_type = findViewById(R.id.rg_type);

        rg_type_of_notice.setOnCheckedChangeListener((group, checkedId) -> TypeOfNotice());

        rg_type.setOnCheckedChangeListener((group, checkedId) -> notificationFor());

        rb_notice_circular = findViewById(R.id.rb_notice_circular);
        rb_notice_class_diary = findViewById(R.id.rb_notice_class_diary);
        rb_notice_syllabus = findViewById(R.id.rb_notice_syllabus);
        rb_every_one = findViewById(R.id.rb_every_one);
        rb_only_staff = findViewById(R.id.rb_only_staff);
        rb_only_student = findViewById(R.id.rb_only_student);
        rb_select_class = findViewById(R.id.rb_select_class);


        iv_notice_image = findViewById(R.id.iv_notice_image);

        btn_image_pdf = findViewById(R.id.btn_image_pdf);

        btn_image_pdf.setOnClickListener(view -> {
            String[] mimeTypes =
                    {"image/*"};
            Intent intent = new Intent();
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length >=0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
            // intent.setType("image/*,application/pdf");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        });

        expiry_date = findViewById(R.id.expiry_date);
        btn_class_sections = findViewById(R.id.btn_class_sections);
        btn_class_sections.setOnClickListener(view -> selectClassSections());
        initializeDatePickers();
    }

    void TypeOfNotice() {
        int selectedId = rg_type_of_notice.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);
        if (radioButton.getText().equals(getString(R.string.circular))) {
            rb_every_one.setEnabled(true);
            rb_only_staff.setEnabled(true);
            rb_only_student.setEnabled(true);
        } else if (radioButton.getText().equals(getString(R.string.class_diary))) {
            rb_every_one.setEnabled(false);
            rb_only_staff.setEnabled(false);
            rb_only_student.setEnabled(false);
            rb_select_class.setChecked(true);
        } else if (radioButton.getText().equals(getString(R.string.syllabus))) {
            rb_every_one.setEnabled(false);
            rb_only_staff.setEnabled(false);
            rb_only_student.setEnabled(false);
            rb_select_class.setChecked(true);
        }
        notificationFor();
    }

    void notificationFor() {
        int selectedId = rg_type.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);
        if (radioButton.getText().equals(getString(R.string.select_class))) {
            btn_class_sections.setVisibility(View.VISIBLE);
        } else {
            btn_class_sections.setVisibility(View.GONE);
        }
    }

    void initializeDatePickers() {
        final Calendar myCalendar = Calendar.getInstance();
        // Notice Expiry Date
        final DatePickerDialog.OnDateSetListener noticeDate = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
            expiry_date.setText(sdf.format(myCalendar.getTime()));
        };
        expiry_date.setOnClickListener(view -> new DatePickerDialog(CreateNoticeBoardActivity.this, noticeDate, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

    }

    void selectClassSections() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.select_class);

        final boolean[] checkedItems = new boolean[classNameSectionList.size()];

        Object[] objNames = sectionList.toArray();

        String[] strNames = Arrays.copyOf(objNames, Objects.requireNonNull(objNames).length, String[].class);

        for (int i = 0; i < classNameSectionList.size(); i++) {
            checkedItems[i] = selectedClassSections.contains(classNameSectionList.get(i).SectionId);
        }

        builder.setMultiChoiceItems(strNames, checkedItems, (dialogInterface, i, b) -> {
            if (b) {
                if (!(selectedClassSections.contains(classNameSectionList.get(i).SectionId)))
                    selectedClassSections.add(classNameSectionList.get(i).SectionId);
            } else {
                selectedClassSections.remove(classNameSectionList.get(i).SectionId);
            }
        }).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
            /*if (selectedClassSections != null) {
                StringBuilder Sections = new StringBuilder();
                for (String s : selectedClassSections) {
                    if (Sections.toString().equals("")) {
                        Sections = new StringBuilder(s);
                    } else {
                        Sections.append(",").append(s);
                    }
                }
                btn_class_sections.setText(Sections);
            }*/
            dialogInterface.dismiss();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        classNameSectionList.clear();
        sectionList.clear();

        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).observe(this, classNameSectionNames -> {
            classNameSectionList.clear();
            sectionList.clear();
            assert classNameSectionNames != null;
            classNameSectionList.addAll(classNameSectionNames);
            for (int i = 0; i < classNameSectionList.size(); i++) {
                sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName);
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            if (validateInput()) {
                if (noticeUpdate == null) {
                    try {
                        createNotice();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        updateNotice();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);

    }

    void createNotice() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = new JSONObject();
        param.put("Description", et_notice_description.getText().toString().trim());
        param.put("Status", Gc.ACTIVE);
        param.put("Date", expiry_date.getText().toString().trim());

        int selectedId = rg_type_of_notice.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);
        if (!radioButton.getText().equals("")) {
            param.put("Flag", radioButton.getText().toString().trim());
        }

        if (selectedClassSections != null) {
            StringBuilder Sections = new StringBuilder();
            for (String s : selectedClassSections) {
                if (Sections.toString().equals("")) {
                    Sections = new StringBuilder(s);
                } else {
                    Sections.append(",").append(s);
                }
            }
            param.put("Sections", Sections.toString().trim());
        }

        int selectedIdd = rg_type.getCheckedRadioButtonId();
        radioType = findViewById(selectedIdd);
        if (radioType.getText().equals(getString(R.string.every_one))) {
            param.put("Type", "Every One");
        } else if (radioType.getText().equals(getString(R.string.only_staff))) {
            param.put("Type", "Staff Only");
        } else if (radioType.getText().equals(getString(R.string.only_student))) {
            param.put("Type", "Student Only");
        } else if (radioType.getText().equals(getString(R.string.select_class))) {
            param.put("Type", "Class");
        }

        if (iv_notice_image.getDrawable() != null) {
            Bitmap image = ((BitmapDrawable) iv_notice_image.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            JSONObject i = new JSONObject();
            i.put("fileData", encodedImage);
            i.put("fileType", "jpg");
            param.put("file", i);
        }


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "noticeBoard/noticeBoards";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:

//                    Intent intent = new Intent();
//                    intent.putExtra("message", getString(R.string.success));
//                    setResult(RESULT_OK, intent);
                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, CreateNoticeBoardActivity.this);

                    Intent intent2 = new Intent(CreateNoticeBoardActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, CreateNoticeBoardActivity.this);

                    Intent intent1 = new Intent(CreateNoticeBoardActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(CreateNoticeBoardActivity.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return param == null ? null : param.toString().getBytes(StandardCharsets.UTF_8);
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    int SELECT_PICTURE = 101;

    void updateNotice() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = new JSONObject();
        param.put("Description", et_notice_description.getText().toString().trim());
        param.put("Date", expiry_date.getText().toString().trim());

        int selectedId = rg_type_of_notice.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);
        if (!radioButton.getText().equals("")) {
            param.put("Flag", radioButton.getText().toString().trim());
        }

        if (selectedClassSections != null) {
            StringBuilder Sections = new StringBuilder();
            for (String s : selectedClassSections) {
                if (Sections.toString().equals("")) {
                    Sections = new StringBuilder(s);
                } else {
                    Sections.append(",").append(s);
                }
            }
            param.put("Sections", Sections.toString().trim());
        }

        int selectedIdd = rg_type.getCheckedRadioButtonId();
        radioType = findViewById(selectedIdd);
        if (radioType.getText().equals(getString(R.string.every_one))) {
            param.put("Type", "Every One");
        } else if (radioType.getText().equals(getString(R.string.only_staff))) {
            param.put("Type", "Staff Only");
        } else if (radioType.getText().equals(getString(R.string.only_student))) {
            param.put("Type", "Student Only");
        } else if (radioType.getText().equals(getString(R.string.select_class))) {
            param.put("Type", "Class");
        }

        if (iv_notice_image.getDrawable() != null) {
            Bitmap image = ((BitmapDrawable) iv_notice_image.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            JSONObject i = new JSONObject();
            i.put("fileData", encodedImage);
            i.put("fileType", "jpg");
            param.put("file", i);
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "noticeBoard/noticeBoards/"
                + new JSONObject().put("Notify_id", noticeUpdate.Notify_id);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
//                    try {

//                        final ArrayList<NoticeBoardEntity> notices = new Gson()
//                                .fromJson(job.getJSONObject("result").optString("noticeBoards"),
//                                        new TypeToken<List<NoticeBoardEntity>>() {
//                                        }.getType()
//                                );
//                        if (notices == null) {
//                            Config.responseSnackBarHandler("Something Wrong!! Report to info@zeroerp.com",
//                                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
//                            return;
//                        }
//
//                        if (notices.size() > 0) {
//                            new Thread(() -> mDb.noticeBoardModel().insertNotice(notices)).start();
//
////                            Intent intent = new Intent();
////                            intent.putExtra("message", getString(R.string.success));
////                            setResult(RESULT_OK, intent);
//                            finish();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                    finish();
                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, CreateNoticeBoardActivity.this);

                    Intent intent2 = new Intent(CreateNoticeBoardActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, CreateNoticeBoardActivity.this);

                    Intent intent1 = new Intent(CreateNoticeBoardActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    finish();
                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(CreateNoticeBoardActivity.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return param == null ? null : param.toString().getBytes(StandardCharsets.UTF_8);
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Glide.with(this)
                        .load(data.getData())
                        .apply(RequestOptions.overrideOf(600, 1200))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(iv_notice_image);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    Boolean validateInput() {

        if ("".equals(et_notice_description.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.description) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            et_notice_description.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(expiry_date.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.expiry_date) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            expiry_date.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        int selectedId = rg_type_of_notice.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);
        if (radioButton.getText().equals("")) {
            Config.responseSnackBarHandler(getString(R.string.type_of_notice) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
        //    expiry_date.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        int selectedIdd = rg_type.getCheckedRadioButtonId();
        radioType = findViewById(selectedIdd);
        if (!radioType.getText().equals("")) {
            if (radioType.getText().equals("Select Class")){
                if (selectedClassSections == null) {
                    Config.responseSnackBarHandler(getString(R.string.select_class) + " " + getString(R.string.field_can_not_be_blank),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    return false;
                }
            }
        } else  {
            Config.responseSnackBarHandler(getString(R.string.send_notice_to) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }
        return true;
    }
}
