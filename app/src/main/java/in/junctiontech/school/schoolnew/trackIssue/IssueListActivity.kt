package `in`.junctiontech.school.schoolnew.trackIssue

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.menuActions.ReportQuery
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.io.FileNotFoundException
import java.net.MalformedURLException
import java.util.*

class IssueListActivity : AppCompatActivity() {


    private var fbAddIssue: FloatingActionButton? = null
    private var colorIs = 0
    private var mediaLinkPath: String? = null
    var progressBar: ProgressBar? = null

    private val issueList: ArrayList<IssueListsData> = ArrayList<IssueListsData>()
    private var adapter: IssueListAdapter? = null

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
        fbAddIssue!!.backgroundTintList = ColorStateList.valueOf(colorIs)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_issue)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        progressBar = findViewById(R.id.progressBar)
        fbAddIssue = findViewById(R.id.fb_add_issue)

        fbAddIssue!!.setOnClickListener {
            startActivityForResult(Intent(this, ReportQuery::class.java), 113)
        }

        val mRecyclerView = findViewById<RecyclerView>(R.id.rv_issue_list)
        mRecyclerView.setBackgroundColor(resources.getColor(R.color.backgroundColor))

        mRecyclerView.setHasFixedSize(true)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager
        adapter = IssueListAdapter()
        mRecyclerView.adapter = adapter

        if (!listOf<String>(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase(Locale.ROOT)) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase(Locale.ROOT).equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/4118385648", this, adContainer)
        }

        setColorApp()
        getAuthToken()
    }

    private fun getAuthToken() {
        progressBar!!.visibility = View.VISIBLE
        val stringReq: JsonObjectRequest = object : JsonObjectRequest(Method.GET, "http://apiorgusrmgmt.zeroerp.in/LoginApi", null
                , Response.Listener { jsonObject: JSONObject ->
            Log.e("responseReportQuery", jsonObject.toString())
            progressBar!!.visibility = View.GONE
            if (jsonObject.optString("code").equals("200", ignoreCase = true)) {
                try {
                    getIssueList(jsonObject.optJSONObject("result").optString("Token"), jsonObject.optJSONObject("result").optString("OrganizationKey"))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }, Response.ErrorListener { volleyError: VolleyError ->
            progressBar!!.visibility = View.GONE
        }) {
            override fun getHeaders(): Map<String, String> {
                val header: HashMap<String, String> = LinkedHashMap()
                header["Action"] = "Authentication"
                header["Content-Type"] = "application/json"
                try {
                    header["filter"] = JSONObject().put("Mobile", "9876543211").put("Password", "initial").toString()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                header["OrganizationKey"] = "APIPERMISSION"
                header["App_key"] = "SUlQRjBvTEgvRGNjSzNSOUF2cjJWUT09"
                return header
            }
        }
        stringReq.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(this).addToRequestQueue(stringReq)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 113 && resultCode == Activity.RESULT_OK && data != null) {
            getAuthToken()
        }
        else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getIssueList(Token: String, OrganizationKey: String) {
        progressBar!!.visibility = View.VISIBLE
        val stringReq: JsonObjectRequest = object : JsonObjectRequest(Method.GET, Gc.ISSUEREPORTURL + "/BugApi", null
                , Response.Listener { jsonObject: JSONObject ->
            Log.e("responseReportQuery", jsonObject.toString())
            progressBar!!.visibility = View.GONE
            if (jsonObject.optString("code").equals("200", ignoreCase = true)) {
                try {
                    mediaLinkPath = jsonObject.optString("mediaLinkPath")
                    val obj = Gson()
                            .fromJson<ArrayList<IssueListsData>>(jsonObject.optString("result"),
                                    object : TypeToken<List<IssueListsData>>() {}.type)
                    issueList.clear()
                    issueList.addAll(obj)
                    adapter!!.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }, Response.ErrorListener { volleyError: VolleyError ->
            progressBar!!.visibility = View.GONE
        }) {
            override fun getHeaders(): Map<String, String> {
                val header: HashMap<String, String> = LinkedHashMap()
                header["OrganizationKey"] = OrganizationKey
                header["Auth_Token"] = Token
                header["Content-Type"] = "application/json"
                try {
                    header["filter"] = JSONObject().put("databaseName", Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)).put("reporterOrganizationKey", Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)).toString()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                return header
            }
        }
        stringReq.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(this).addToRequestQueue(stringReq)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class IssueListAdapter : RecyclerView.Adapter<IssueListAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_issue_layout, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val bugListData = issueList[position]
            holder.tvItemBugTitle.text = bugListData.title
            holder.tvItemBugDescription.text = bugListData.description
            holder.tvItemBugStatus.text = bugListData.status
            holder.tvItemBugCode.text = "#" + bugListData.bugCode
        }

        override fun getItemCount(): Int {
            return issueList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tvItemBugTitle: TextView = itemView.findViewById(R.id.tv_item_bug_title)
            val tvItemBugDescription: TextView = itemView.findViewById(R.id.tv_item_bug_description)
            val tvItemBugStatus: TextView = itemView.findViewById(R.id.tv_item_bug_status)
            val tvItemBugCode: TextView = itemView.findViewById(R.id.tv_item_BugCode)

            init {
                itemView.setOnClickListener {
                    try {
                        val choices = AlertDialog.Builder(this@IssueListActivity)
                        val menu = ArrayList<String>()
                        menu.add("Issue Log")
                        if (!issueList[layoutPosition].mediaPath.isNullOrEmpty()) {
                            menu.add("View Attachment")
                        }
                        val objNames = menu.toTypedArray()
                        choices.setItems(objNames) { _, which ->
                            if (objNames[which] == "View Attachment") {
                                val intent = Intent(this@IssueListActivity, FullScreenImageActivity::class.java)
                                val stom = ArrayList<String>()
                                stom.add(mediaLinkPath + "/" + issueList[layoutPosition].mediaPath)
                                intent.putStringArrayListExtra("images", stom)
                                intent.putExtra("Track Issue", getString(R.string.gallery))
                                startActivity(intent)
                            } else {
                                val intent = Intent(this@IssueListActivity, IssueLogActivity::class.java)
                                intent.putExtra("IssueDetails", issueList[layoutPosition])
                                startActivity(intent)
                            }
                        }
                        choices.show()
                    } catch (e: MalformedURLException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}