package in.junctiontech.school.schoolnew.assignments;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.AssignmentEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class Assignments extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    private FloatingActionButton fb_create_assignment;

    ArrayList<AssignmentEntity> assignmentList = new ArrayList<>();

    String[] permissions = new String[0];
    RecyclerView mRecyclerView;
    AssignmentListAdapter adapter;

    String selectedSectionID;
    String selectedSectionname;
    String assignmentdate;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_create_assignment.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        progressBar.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignments);

        mDb = MainDatabase.getDatabase(this);

        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        fb_create_assignment = findViewById(R.id.fb_create_assignment);

        if (Gc.getSharedPreference(Gc.HOMEWORKCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {

            fb_create_assignment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Assignments.this, AssignmentCreateActivity.class);
                    intent.putExtra("createAssignment", Gc.TRUE);
                    intent.putExtra("selectedSectionID", selectedSectionID);
                    intent.putExtra("selectedSectionname", selectedSectionname);
                    intent.putExtra("assignmentdate", assignmentdate);
                    startActivityForResult(intent, Gc.ASSIGNMENT_REQUEST_CODE);
                }
            });

            permissions = new String[]{
                    getResources().getString(R.string.update),
                    getResources().getString(R.string.evaluate),
                    getResources().getString(R.string.delete)
//                    //                context.getResources().getString(R.string.assignment_submission)
//
            };

        } else {
            fb_create_assignment.hide();
//            permissions = new String[]{
//                    getResources().getString(R.string.show),
//                    getResources().getString(R.string.submit)
//            };
        }
        mRecyclerView = findViewById(R.id.rv_assignments);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new AssignmentListAdapter(this, assignmentList, permissions);
        mRecyclerView.setAdapter(adapter);

        selectedSectionID = Strings.nullToEmpty(getIntent().getStringExtra("selectedSectionID"));
        assignmentdate = Strings.nullToEmpty(getIntent().getStringExtra("assignmentdate"));
        selectedSectionname = Strings.nullToEmpty(getIntent().getStringExtra("selectedSectionname"));

        if (assignmentdate.equalsIgnoreCase("") || selectedSectionID.equalsIgnoreCase("")) {
            Config.responseSnackBarHandler(getString(R.string.class_is_not_available_please_first_add_class),
                    findViewById(R.id.top_assignment), R.color.fragment_first_green);
            finish();
        } else {

            Date date = new Date();
            try {
                date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(assignmentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mDb.assignmentModel()
                    .getAssignmentsForDate(selectedSectionID, date).observe(this, new Observer<List<AssignmentEntity>>() {
                @Override
                public void onChanged(@Nullable List<AssignmentEntity> assignmentEntities) {
                    assignmentList.clear();
                    assignmentList.addAll(assignmentEntities);
                    adapter.notifyDataSetChanged();
                    if (assignmentList.size() == 0)
                        Config.responseSnackBarHandler(getString(R.string.assignment_not_available),
                                findViewById(R.id.top_assignment), R.color.fragment_first_blue);
                }
            });
        }

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1304520045", this, adContainer);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_assignment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    void updateAssignment(String assignment) {

        Intent intent = new Intent(this, AssignmentCreateActivity.class);
        intent.putExtra("assignment", assignment);
        intent.setAction(Gc.UPDATE_ASSIGNMENT);
        startActivityForResult(intent, Gc.ASSIGNMENT_REQUEST_CODE);

    }

    void deleteAssignment(String assignment) {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject p = new JSONObject();

        final AssignmentEntity assignmentEntity = new Gson().fromJson(assignment, AssignmentEntity.class);
        final ArrayList<AssignmentEntity> assignmentEntities = new ArrayList<AssignmentEntity>();
        assignmentEntities.add(assignmentEntity);

        String homeworkDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(assignmentEntity.dateofhomework);


        try {
            p.put("sectionid", assignmentEntity.sectionid);
            p.put("subjectid", assignmentEntity.subjectid);
            p.put("dateofhomework", homeworkDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "homework/homework/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:

                    new Thread(() -> {
                        mDb.assignmentModel().deleteAssignment(assignmentEntity);
                    }).start();

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_assignment), R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, Assignments.this);

                    Intent intent2 = new Intent(Assignments.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();
                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, Assignments.this);

                    Intent intent1 = new Intent(Assignments.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_assignment), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(Assignments.this, error, findViewById(R.id.top_assignment));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void evaluateAssignment(String homeworkId, String SectionId) {
        Intent intent = new Intent(this, EvaluateHomeworkActivity.class);
        intent.putExtra("homeworkId", homeworkId);
        intent.putExtra("SectionId", SectionId);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Gc.ASSIGNMENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                String message = data.getExtras().getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.top_assignment), R.color.fragment_first_green);
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
