package in.junctiontech.school.schoolnew.owner;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.R;

/**
 * Created by LENEVO on 05-07-2017.
 */

public class OwnerSchoolAdapter extends RecyclerView.Adapter<OwnerSchoolAdapter.MyViewHoler> {
    private OwnerSchoolsActivity ownerSchoolsActivity;
    private ArrayList<OwnerSchoolData> ownerSchoolList;
    private int colorIs;

    public OwnerSchoolAdapter(OwnerSchoolsActivity ownerSchoolsActivity, ArrayList<OwnerSchoolData> ownerSchoolList, int colorIs) {
        this.ownerSchoolList = ownerSchoolList;
        this.ownerSchoolsActivity = ownerSchoolsActivity;
        this.colorIs =colorIs;
    }

    @Override
    public OwnerSchoolAdapter.MyViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_owner_school_item, parent, false);
        return new OwnerSchoolAdapter.MyViewHoler(view);
    }

    @Override
    public void onBindViewHolder(OwnerSchoolAdapter.MyViewHoler holder, int position) {
        OwnerSchoolData obj = ownerSchoolList.get(position);
        holder.owner_item_organization_name.setText(obj.getOrganizationName());
        holder.owner_item_organization_name.setTextColor(colorIs);
        Glide.with(ownerSchoolsActivity).load(obj.getLogoLink())
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .thumbnail(0.5f)
                .into(holder.owner_item_organization_logo);
    }

    @Override
    public int getItemCount() {
        return ownerSchoolList.size();
    }

    public class MyViewHoler extends RecyclerView.ViewHolder {
        public CircleImageView owner_item_organization_logo;
        public TextView owner_item_organization_name;

        public MyViewHoler(View itemView) {
            super(itemView);
            owner_item_organization_logo = (CircleImageView) itemView.findViewById(R.id.owner_item_organization_logo);
            owner_item_organization_name = (TextView) itemView.findViewById(R.id.owner_item_organization_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ownerSchoolsActivity.startActivity(new Intent(ownerSchoolsActivity,OrganizationDetailActivity.class)
                            .putExtra("data",ownerSchoolList.get(getLayoutPosition())));
                    ownerSchoolsActivity.overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }
            });
        }
    }
}
