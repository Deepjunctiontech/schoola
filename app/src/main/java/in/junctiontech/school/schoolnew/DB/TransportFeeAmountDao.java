package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface TransportFeeAmountDao {
    @Query("select * from TransportFeeAmountEntity")
    LiveData<List<TransportFeeAmountEntity>> getTransportFeeAmount();

    @Insert(onConflict = REPLACE)
    void insertTransportFee(List<TransportFeeAmountEntity> transportFees);

    @Query("delete from TransportFeeAmountEntity")
    void deleteAll();
}
