package in.junctiontech.school.schoolnew.common;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.SchoolAccountsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.model.ClassFees;
import in.junctiontech.school.schoolnew.model.FeeType;
import in.junctiontech.school.schoolnew.model.SchoolAccounts;
import in.junctiontech.school.schoolnew.model.SchoolSubjects;

public class MapModelToEntity {

    public static FeeTypeEntity mapFeeTypeModelToEntity(FeeType feeType){
        FeeTypeEntity feeTypeEntity = new FeeTypeEntity();

        feeTypeEntity.FeeTypeID = feeType.FeeTypeID;
        feeTypeEntity.FeeType = feeType.FeeType;
        feeTypeEntity.Frequency = feeType.Frequency;
        feeTypeEntity.generationDate = feeType.generationDate;
        feeTypeEntity.Status = feeType.Status;
        feeTypeEntity.CreatedOn = feeType.CreatedOn;
        feeTypeEntity.CreatedBy = feeType.CreatedBy;
        feeTypeEntity.UpdatedBy = feeType.UpdatedBy;
        feeTypeEntity.UpdatedOn = feeType.UpdatedOn;
        return feeTypeEntity;
    }

    public static ArrayList<ClassFeesEntity> mapClassFeeModelToEntity(JSONArray classFeesjarr){

        final ArrayList<ClassFeesEntity> classFeesEntities = new ArrayList<>();


        for(int i = 0; i < classFeesjarr.length(); i++){

            ClassFees classFees = new ClassFees();
            try {
                classFees = new Gson().fromJson(classFeesjarr.getString(i),ClassFees.class);

                ArrayList<ClassFeesEntity> classFeesEntities1 = new ArrayList<>();

                for (int k = 0; k < classFees.fees.size(); k++) {
                    ClassFeesEntity classFeesEntity = new ClassFeesEntity();
                    classFeesEntity.ClassId = classFees.ClassId;
                    classFeesEntity.ClassName = classFees.ClassName;
                    classFeesEntity.SectionId = classFees.SectionId;
                    classFeesEntity.SectionName = classFees.SectionName;
                    classFeesEntity.Session = classFees.Session;
                    classFeesEntity.FeeId = classFees.fees.get(k).FeeId;
                    classFeesEntity.FeeTypeID = classFees.fees.get(k).FeeTypeID;
                    classFeesEntity.FeeType = classFees.fees.get(k).FeeType;
                    classFeesEntity.Frequency = classFees.fees.get(k).Frequency;
                    classFeesEntity.FeeStatus = classFees.fees.get(k).FeeStatus;
                    classFeesEntity.Amount = classFees.fees.get(k).Amount;
                    classFeesEntity.DOE = classFees.fees.get(k).DOE;
                    classFeesEntities1.add(classFeesEntity);
                }
                classFeesEntities.addAll(classFeesEntities1);




            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return classFeesEntities;
    }

    public static ArrayList<ClassFeesTransportEntity> mapClassFeeTransportToEntity(JSONArray classFeesjarr){

        final ArrayList<ClassFeesTransportEntity> classTransportEntities = new ArrayList<>();

        for(int i = 0; i < classFeesjarr.length(); i++){

            ClassFees classFees = new ClassFees();
            try {
                classFees = new Gson().fromJson(classFeesjarr.getString(i),ClassFees.class);

                ArrayList<ClassFeesTransportEntity> classTransportFees = new ArrayList<>();

                for (int k = 0; k < classFees.transportFees.size(); k++) {
                    ClassFeesTransportEntity classFeesTransportEntity = new ClassFeesTransportEntity();
                    classFeesTransportEntity.ClassId = classFees.ClassId;
                    classFeesTransportEntity.ClassName = classFees.ClassName;
                    classFeesTransportEntity.SectionId = classFees.SectionId;
                    classFeesTransportEntity.SectionName = classFees.SectionName;
                    classFeesTransportEntity.Session = classFees.Session;
                    classFeesTransportEntity.FeeId = classFees.transportFees.get(k).FeeId;
                    classFeesTransportEntity.FeeTypeID = classFees.transportFees.get(k).FeeTypeID;
                    classFeesTransportEntity.FeeType = classFees.transportFees.get(k).FeeType;
                    classFeesTransportEntity.Frequency = classFees.transportFees.get(k).Frequency;
                    classFeesTransportEntity.FeeStatus = classFees.transportFees.get(k).FeeStatus;
                    classFeesTransportEntity.Amount = classFees.transportFees.get(k).Amount;
                    classFeesTransportEntity.DOE = classFees.transportFees.get(k).DOE;
                    classFeesTransportEntity.Distance = classFees.transportFees.get(k).Distance;
                    classFeesTransportEntity.MasterEntryValue = classFees.transportFees.get(k).MasterEntryValue;

                    classTransportFees.add(classFeesTransportEntity);
                }
                classTransportEntities.addAll(classTransportFees);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return classTransportEntities;
    }


    public static ArrayList<SchoolAccountsEntity> mapSchoolAccountsToEntity(JSONArray accountsjarr) throws JSONException {
        ArrayList<SchoolAccountsEntity> schoolAccountsEntities = new ArrayList<SchoolAccountsEntity>();

        for (int i = 0; i < accountsjarr.length(); i++ ) {
            SchoolAccounts schoolAccounts = new Gson().fromJson(accountsjarr.getString(i), SchoolAccounts.class);

            SchoolAccountsEntity schoolAccountsEntity = new SchoolAccountsEntity();

            schoolAccountsEntity.AccountId = schoolAccounts.AccountId;
            schoolAccountsEntity.AccountName = schoolAccounts.AccountName;
            schoolAccountsEntity.AccountType = schoolAccounts.AccountType;
            schoolAccountsEntity.accountTypeName = schoolAccounts.accountTypeName;
            schoolAccountsEntity.AccountStatus = schoolAccounts.AccountStatus;


            schoolAccountsEntity.AccountDate = Gc.convertDate(schoolAccounts.AccountDate);
            schoolAccountsEntity.OpeningBalance = schoolAccounts.OpeningBalance;
            schoolAccountsEntity.AccountBalance = schoolAccounts.AccountBalance;
            schoolAccountsEntity.BankAccountName = schoolAccounts.BankAccountName;
            schoolAccountsEntity.BankName = schoolAccounts.BankName;
            schoolAccountsEntity.BranchName = schoolAccounts.BranchName;
            schoolAccountsEntity.IFSCCode = schoolAccounts.IFSCCode;
            schoolAccountsEntity.ManagedBy = schoolAccounts.ManagedBy;
            schoolAccountsEntity.managedByName = schoolAccounts.managedByName;
            schoolAccountsEntity.DOE = schoolAccounts.DOE;
            schoolAccountsEntities.add(schoolAccountsEntity);
        }
        return schoolAccountsEntities;
    }


    public static ArrayList<SchoolSubjectsEntity> mapSubjectModelToEntity(JSONArray subjectsjarr) throws JSONException {
        ArrayList<SchoolSubjectsEntity> schoolSubjectsEntities = new ArrayList<>();

        for ( int i = 0; i < subjectsjarr.length(); i++){
            SchoolSubjects schoolSubjects = new Gson().fromJson(subjectsjarr.getString(i), SchoolSubjects.class);

            SchoolSubjectsEntity schoolSubjectsEntity = new SchoolSubjectsEntity();

            schoolSubjectsEntity.SubjectId = schoolSubjects.SubjectId;
            schoolSubjectsEntity.SubjectName = schoolSubjects.SubjectName;
            schoolSubjectsEntity.Session = schoolSubjects.Session;
            schoolSubjectsEntity.SubjectAbb = schoolSubjects.SubjectAbb;
            schoolSubjectsEntity.SubjectStatus = schoolSubjects.SubjectStatus;
            schoolSubjectsEntity.Class = schoolSubjects.Class;
            schoolSubjectsEntity.DOE = schoolSubjects.DOE;
            schoolSubjectsEntity.DOL = schoolSubjects.DOL;
            schoolSubjectsEntities.add(schoolSubjectsEntity);
        }
        return schoolSubjectsEntities;
    }

    public static ArrayList<SchoolSubjectsClassEntity> mapSubjectClassesModelToEntity(JSONArray subjectsjarr) throws JSONException {
        ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClassEntities = new ArrayList<>();

        for (int k = 0 ; k < subjectsjarr.length(); k++ ) {
            SchoolSubjects schoolSubjects = new Gson().fromJson(subjectsjarr.getString(k), SchoolSubjects.class);
            for (int i = 0; i < schoolSubjects.classSection.size(); i++) {
                SchoolSubjectsClassEntity schoolSubjectsClassEntity = new SchoolSubjectsClassEntity();
                schoolSubjectsClassEntity.ClassId = schoolSubjects.classSection.get(i).ClassId;
                schoolSubjectsClassEntity.ClassName = schoolSubjects.classSection.get(i).ClassName;
                schoolSubjectsClassEntity.SectionId = schoolSubjects.classSection.get(i).SectionId;
                schoolSubjectsClassEntity.SectionName = schoolSubjects.classSection.get(i).SectionName;
                schoolSubjectsClassEntity.SubjectId = schoolSubjects.classSection.get(i).SubjectId;
                schoolSubjectsClassEntities.add(schoolSubjectsClassEntity);
            }
        }
        return schoolSubjectsClassEntities;
    }
}
