package in.junctiontech.school.schoolnew.owner;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 05-07-2017.
 */

public class OwnerSchoolData implements Serializable {
    String organizationName ,organizationKey, logoLink;

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationKey() {
        return organizationKey;
    }

    public void setOrganizationKey(String organizationKey) {
        this.organizationKey = organizationKey;
    }

    public String getLogoLink() {
        return logoLink;
    }

    public void setLogoLink(String logoLink) {
        this.logoLink = logoLink;
    }

    public OwnerSchoolData(String organizationName, String organizationKey, String logoLink) {
        this.organizationName = organizationName;
        this.organizationKey = organizationKey;
        this.logoLink = logoLink;
    }
}
