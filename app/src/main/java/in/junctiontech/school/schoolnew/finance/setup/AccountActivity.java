package in.junctiontech.school.schoolnew.finance.setup;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import androidx.lifecycle.Observer;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.SchoolAccountsEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;

public class AccountActivity extends AppCompatActivity {
    private FloatingActionButton fb_account_add;

    private ProgressDialog progressBar;

    private ArrayList<String> accountTypeList = new ArrayList<>();
    private ArrayList<MasterEntryEntity> masterEntriesAccountType = new ArrayList<>();

    private ArrayList<SchoolAccountsEntity> schoolAccounts = new ArrayList<>();

    MainDatabase mDb ;

    private RecyclerView mRecyclerView;
    private AccountAdapter adapter;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_account_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_finance_account);
        mDb = MainDatabase.getDatabase(this);

        if(Gc.getSharedPreference(Gc.ACCOUNTEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        masterEntriesAccountType = (ArrayList<MasterEntryEntity>) mDb.masterEntryModel().getMasterEntryValuesForKey("AccountType");
        fb_account_add = findViewById(R.id.fb_account_add);
        for (int i = 0; i < masterEntriesAccountType.size(); i++){
            accountTypeList.add(masterEntriesAccountType.get(i).MasterEntryValue);
        }

        fb_account_add
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i("Floating button clicked", "in accounts");
                        showDialogForAccountCreate();
                    }
                });
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);

        mRecyclerView = findViewById(R.id.rv_account_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new AccountAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3106648522", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolAccountsModel().getSchoolAccounts().observe(this, new Observer<List<SchoolAccountsEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolAccountsEntity> schoolAccountsEntities) {
                schoolAccounts.clear();
                if(schoolAccountsEntities.size()>0){
                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ACCOUNTEXISTS, Gc.EXISTS);
                    Gc.setSharedPreference(setDefaults, AccountActivity.this);
                }else{
                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ACCOUNTEXISTS, Gc.NOTFOUND);
                    Gc.setSharedPreference(setDefaults, AccountActivity.this);
                }

                for(int i = 0 ; i < schoolAccountsEntities.size(); i++){
                    schoolAccounts.add(schoolAccountsEntities.get(i));
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolAccountsModel().getSchoolAccounts().removeObservers(this);
    }

    void showDialogForAccountCreate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.account);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.fragment_fee_account, null);

        final EditText et_account_name = view.findViewById(R.id.et_fee_account_account_name);

        final EditText et_opening_balance = view.findViewById(R.id.et_fee_account_opening_balance);

        final Spinner sp_account_type = view.findViewById(R.id.sp_fee_account_account_type);

        final Calendar myCalendar = Calendar.getInstance();
        final Button btn_account_start_date = view.findViewById(R.id.btn_fee_account_account_start_date);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                btn_account_start_date.setText(sdf.format(myCalendar.getTime()));


            }

        };

        btn_account_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AccountActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,accountTypeList);
        arrayAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_account_type.setAdapter(arrayAdapter);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called","");
            }
        });

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if(et_account_name.getText().toString().trim().isEmpty()){
                    et_account_name.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
                else if (et_opening_balance.getText().toString().trim().isEmpty()) {
                    et_opening_balance.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert)); }
                else if(btn_account_start_date.getText().toString().trim().isEmpty()){
                    btn_account_start_date.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
                    else {

                    Log.i("Account name",et_account_name.getText().toString() );
                    Log.i("Balance", et_opening_balance.getText().toString());
                    Log.i("Type", sp_account_type.getSelectedItem().toString());
                    Log.i("Date", btn_account_start_date.getText().toString());
                createAccount(et_account_name.getText().toString().trim(),
                        et_opening_balance.getText().toString().trim(),
                        masterEntriesAccountType.get(sp_account_type.getSelectedItemPosition()).MasterEntryId,
                        btn_account_start_date.getText().toString().trim());
                wantToCloseDialog = true;
                }
                if(wantToCloseDialog)
                    dialog.dismiss();
            }
        });
    }

    void createAccount(String accName, String opbalance, String acctype, String accstdate){
        progressBar.show();

        final JSONObject param = new JSONObject();
        try {
            param.put("AccountType", acctype);
            param.put("AccountName",accName);
            param.put("OpeningBalance", opbalance);
            param.put("AccountDate",accstdate);
            param.put("AccountStatus", Gc.ACTIVE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "finance/accounts"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                Log.i("Account ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray accountsjarr                 = resultjobj.getJSONArray("accounts");
                            saveSchoolAccountsToDataBase(accountsjarr);

                            Config.responseSnackBarHandler(getString(R.string.success),
                                    findViewById(R.id.ll_account_activity),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, AccountActivity.this);

                        Intent intent1 = new Intent(AccountActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, AccountActivity.this);

                        Intent intent2 = new Intent(AccountActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.ll_account_activity),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
            Config.responseVolleyErrorHandler(AccountActivity.this,error,
                    findViewById(R.id.ll_account_activity));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void showDialogForAccountUpdate(final SchoolAccountsEntity schoolAccountsEntity){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.account);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.fragment_fee_account, null);

        final EditText et_account_name = view.findViewById(R.id.et_fee_account_account_name);
        et_account_name.setText(schoolAccountsEntity.AccountName);
        final EditText et_opening_balance = view.findViewById(R.id.et_fee_account_opening_balance);
        et_opening_balance.setText(schoolAccountsEntity.OpeningBalance);



        final Calendar myCalendar = Calendar.getInstance();
        final Button btn_account_start_date = view.findViewById(R.id.btn_fee_account_account_start_date);
        btn_account_start_date.setText(schoolAccountsEntity.AccountDate);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                btn_account_start_date.setText(sdf.format(myCalendar.getTime()));
            }

        };

        btn_account_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AccountActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final Spinner sp_account_type = view.findViewById(R.id.sp_fee_account_account_type);
        int position = 0;

        int len = masterEntriesAccountType.size();

        for (int i = 0; i <len;  i++){
            if (masterEntriesAccountType.get(i).MasterEntryValue.equalsIgnoreCase(schoolAccountsEntity.accountTypeName))
                position = i;
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,accountTypeList);
        arrayAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_account_type.setAdapter(arrayAdapter);
        sp_account_type.setSelection(position);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called","");
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if(et_account_name.getText().toString().trim().isEmpty()){
                    et_account_name.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
                else if (et_opening_balance.getText().toString().trim().isEmpty()) {
                    et_opening_balance.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert)); }
                else if(btn_account_start_date.getText().toString().trim().isEmpty()){
                    btn_account_start_date.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
                else {

                    Log.i("Account name",et_account_name.getText().toString() );
                    Log.i("Balance", et_opening_balance.getText().toString());
                    Log.i("Type", sp_account_type.getSelectedItem().toString());
                    Log.i("Date", btn_account_start_date.getText().toString());
                    schoolAccountsEntity.AccountName = et_account_name.getText().toString().trim();
                    schoolAccountsEntity.OpeningBalance = et_opening_balance.getText().toString().trim();
                    schoolAccountsEntity.accountTypeName = sp_account_type.getSelectedItem().toString();
                    schoolAccountsEntity.AccountType = masterEntriesAccountType.get(sp_account_type.getSelectedItemPosition()).MasterEntryId;
                    schoolAccountsEntity.AccountDate = btn_account_start_date.getText().toString();
                    updateAccount(schoolAccountsEntity);
                    wantToCloseDialog = true;
                }
                if(wantToCloseDialog)
                    dialog.dismiss();
            }
        });
    }


    void updateAccount(SchoolAccountsEntity schoolAccountsEntity){
        progressBar.show();

        final JSONObject param = new JSONObject();
        final JSONObject p = new JSONObject();
        try {
            p.put("AccountId", schoolAccountsEntity.AccountId);
            param.put("AccountType", schoolAccountsEntity.AccountType);
            param.put("AccountName",schoolAccountsEntity.AccountName);
            param.put("OpeningBalance", schoolAccountsEntity.OpeningBalance);
            param.put("AccountDate",schoolAccountsEntity.AccountDate);
            param.put("AccountStatus", Gc.ACTIVE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "finance/accounts/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray accountsjarr                 = resultjobj.getJSONArray("accounts");
                            saveSchoolAccountsToDataBase(accountsjarr);

                            Config.responseSnackBarHandler(getString(R.string.success),
                                    findViewById(R.id.ll_account_activity),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_account_activity),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
                Config.responseVolleyErrorHandler(AccountActivity.this,error,
                        findViewById(R.id.ll_account_activity));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);



    }

    private void saveSchoolAccountsToDataBase(JSONArray accountsjarr){
        if(accountsjarr.length()>0) {
            HashMap<String, String> setDefaults = new HashMap<>();
            setDefaults.put(Gc.ACCOUNTEXISTS, Gc.EXISTS);
            Gc.setSharedPreference(setDefaults, this);

            try {
                final ArrayList<SchoolAccountsEntity> schoolAccountsEntities =
                        MapModelToEntity.mapSchoolAccountsToEntity(accountsjarr);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mDb.schoolAccountsModel().insertSchoolAccount(schoolAccountsEntities);
                    }
                }).start();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if(Gc.getSharedPreference(Gc.ACCOUNTEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            menu.findItem(R.id.menu_next).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.MyViewHolder> {


        public class MyViewHolder extends RecyclerView.ViewHolder  {
            TextView account_name,accountStartDate,account_type,opening_balance,current_balance;
            public MyViewHolder(View itemView) {
                super(itemView);
                account_name = itemView.findViewById(R.id.account_name);
                accountStartDate = itemView.findViewById(R.id.accountStartDate);
                account_type = itemView.findViewById(R.id.account_type);
                opening_balance = itemView.findViewById(R.id.opening_balance);
                current_balance = itemView.findViewById(R.id.current_balance);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder choices = new AlertDialog.Builder(AccountActivity.this);
                        String[] aa = new String[]
                                { getString(R.string.update)};

                        choices.setItems(aa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which){
                                    case 0:
                                        showDialogForAccountUpdate(schoolAccounts.get(getAdapterPosition()));
                                        break;

                                }

                            }
                        });
                        choices.show();
                    }
                });
            }
        }

        @NonNull
        @Override
        public AccountAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.finance_account_list, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AccountAdapter.MyViewHolder holder, int position) {
            holder.account_name.setText(schoolAccounts.get(position).AccountName);
            holder.account_name.setTextColor(colorIs);
            holder.account_type.setText(schoolAccounts.get(position).accountTypeName);
            holder.account_type.setTextColor(colorIs);
            holder.accountStartDate.setText(schoolAccounts.get(position).AccountDate);
            holder.current_balance.setText(schoolAccounts.get(position).AccountBalance);
            holder.opening_balance.setText(schoolAccounts.get(position).OpeningBalance);
        }

        @Override
        public int getItemCount() {
            return schoolAccounts.size();
        }

    }

}
