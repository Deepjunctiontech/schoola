package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(primaryKeys = {"SubjectId","SectionId"})
public class SchoolSubjectsClassEntity {
    public String ClassId;
    public String ClassName;
    @NonNull
    public String SectionId;
    public String SectionName;
    @NonNull
    public String SubjectId;
}
