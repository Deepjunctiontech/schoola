package `in`.junctiontech.school.schoolnew.assignments.adpter

import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.assignments.AssignmentUpdateActivity
import `in`.junctiontech.school.schoolnew.assignments.modelhelper.HomeworkDetails
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import java.util.*

class HomeworkDetailsAdapter internal constructor(private val context: Context,
                                                  private val homeworkDetails: ArrayList<HomeworkDetails>) :
        RecyclerView.Adapter<HomeworkDetailsAdapter.MyViewHolder>() {
    var permissions = arrayOf(
            context.resources.getString(R.string.delete)
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_homework_details,
                parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.rlHomeworkItemLayout.visibility = View.VISIBLE
        holder.rlHomeworkReceived.visibility = View.GONE
        holder.rlHomeworkImage.visibility = View.GONE
        holder.rlReceivedPdf.visibility = View.GONE
        holder.rlReceivedHomework.visibility = View.GONE


        holder.rlSendHomeworkLayout.visibility = View.GONE
        holder.rlSendHomeworkPictureLayout.visibility = View.GONE
        holder.rlSendPdf.visibility = View.GONE
        holder.llHomeworkLayout.visibility = View.GONE


        val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, context)

        if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {
            if (Gc.getSharedPreference(Gc.USERID, context) == homeworkDetails[position].studentId && homeworkDetails[position].userId.isNullOrEmpty()) {
                holder.rlSendHomeworkLayout.visibility = View.VISIBLE
                if (homeworkDetails[position].descriptionType == "image") {
                    holder.rlSendHomeworkPictureLayout.visibility = View.VISIBLE
                    Glide.with(context)
                            .load(homeworkDetails[position].mediaUrl)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ivSendHomeworkPictures)
                }
                else if (homeworkDetails[position].descriptionType == "pdf") {
                    holder.rlSendPdf.visibility = View.VISIBLE
                    val path = homeworkDetails[position].mediaUrl
                    val pdfName = path!!.substring(path.lastIndexOf('/') + 1)
                    holder.tvSendPdfName.text = pdfName
                }
                else {
                    holder.llHomeworkLayout.visibility = View.VISIBLE
                    holder.tvSendHomework.text = homeworkDetails[position].description
                }
            } else {
                holder.rlHomeworkReceived.visibility = View.VISIBLE
                if (homeworkDetails[position].descriptionType == "image") {
                    holder.rlHomeworkImage.visibility = View.VISIBLE
                    Glide.with(context)
                            .load(homeworkDetails[position].mediaUrl)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ivReceivedHomeworkImage)
                } else if (homeworkDetails[position].descriptionType == "pdf") {
                    holder.rlReceivedPdf.visibility = View.VISIBLE
                    val path = homeworkDetails[position].mediaUrl
                    val pdfName = path!!.substring(path.lastIndexOf('/') + 1)
                    holder.tvPdfName.text = pdfName
                } else {
                    holder.rlReceivedHomework.visibility = View.VISIBLE
                    holder.tvHomework.text = homeworkDetails[position].description
                }
            }
        } else {
            if (Gc.getSharedPreference(Gc.USERID, context) == homeworkDetails[position].userId) {
                holder.rlSendHomeworkLayout.visibility = View.VISIBLE
                if (homeworkDetails[position].descriptionType == "image") {
                    holder.rlSendHomeworkPictureLayout.visibility = View.VISIBLE
                    Glide.with(context)
                            .load(homeworkDetails[position].mediaUrl)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ivSendHomeworkPictures)
                } else if (homeworkDetails[position].descriptionType == "pdf") {
                    holder.rlSendPdf.visibility = View.VISIBLE
                    val path = homeworkDetails[position].mediaUrl
                    val pdfName = path!!.substring(path.lastIndexOf('/') + 1)
                    holder.tvSendPdfName.text = pdfName
                } else {
                    holder.llHomeworkLayout.visibility = View.VISIBLE
                    holder.tvSendHomework.text = homeworkDetails[position].description
                }
            } else {
                holder.rlHomeworkReceived.visibility = View.VISIBLE
                if (homeworkDetails[position].descriptionType == "image") {
                    holder.rlHomeworkImage.visibility = View.VISIBLE
                    Glide.with(context)
                            .load(homeworkDetails[position].mediaUrl)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ivReceivedHomeworkImage)
                } else if (homeworkDetails[position].descriptionType == "pdf") {
                    holder.rlReceivedPdf.visibility = View.VISIBLE
                    val path = homeworkDetails[position].mediaUrl
                    val pdfName = path!!.substring(path.lastIndexOf('/') + 1)
                    holder.tvPdfName.text = pdfName
                } else {
                    holder.rlReceivedHomework.visibility = View.VISIBLE
                    holder.tvHomework.text = homeworkDetails[position].description
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return homeworkDetails.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        /* Headers Views */
        var rlHomeworkItemLayout: RelativeLayout = itemView.findViewById(R.id.rl_homework_item_layout)
        var tvHomeworkDateHeading: TextView = itemView.findViewById(R.id.tv_homework_date_heading)

        /* Homework Received */
        var rlHomeworkReceived: RelativeLayout = itemView.findViewById(R.id.rl_homework_received)

        /* receive image */
        var rlHomeworkImage: RelativeLayout = itemView.findViewById(R.id.rl_homework_image)
        var ivReceivedHomeworkImage: ImageView = itemView.findViewById(R.id.iv_received_homework_image)

        /* received pdf */
        var rlReceivedPdf: RelativeLayout = itemView.findViewById(R.id.rl_received_pdf)
        var tvPdfName: TextView = itemView.findViewById(R.id.tv_pdf_name)

        /*received homework */
        var rlReceivedHomework: RelativeLayout = itemView.findViewById(R.id.rl_received_homework)
        var tvHomework: TextView = itemView.findViewById(R.id.tv_homework)

        /* Homework Send */
        var rlSendHomeworkLayout: RelativeLayout = itemView.findViewById(R.id.rl_send_homework_Layout)

        /* receive image */
        var rlSendHomeworkPictureLayout: RelativeLayout = itemView.findViewById(R.id.rl_send_homework_picture_layout)
        var ivSendHomeworkPictures: ImageView = itemView.findViewById(R.id.iv_send_homework_pictures)

        /* received pdf */
        var rlSendPdf:RelativeLayout=itemView.findViewById(R.id.rl_send_pdf)
        var tvSendPdfName:TextView=itemView.findViewById(R.id.tv_send_pdf_name)

        /*received homework */
        var llHomeworkLayout: LinearLayout = itemView.findViewById(R.id.ll_homework_layout)
        var tvSendHomework: TextView = itemView.findViewById(R.id.tv_send_homework)

        init {
            itemView.setOnClickListener {
                val choices = AlertDialog.Builder(context)
                choices.setItems(permissions) { _: DialogInterface?, which: Int ->
                    when (which) {
                        0 -> (context as AssignmentUpdateActivity).deleteHomeworkDetails(homeworkDetails[layoutPosition], layoutPosition)
                    }
                }
                choices.show()
            }


            ivSendHomeworkPictures.setOnLongClickListener() {
                val choices = AlertDialog.Builder(context)
                choices.setItems(permissions) { _: DialogInterface?, which: Int ->
                    when (which) {
                        0 -> (context as AssignmentUpdateActivity).deleteHomeworkDetails(homeworkDetails[layoutPosition], layoutPosition)
                    }
                }
                choices.show()
                true
            }

            ivReceivedHomeworkImage.setOnLongClickListener() {
                val choices = AlertDialog.Builder(context)
                choices.setItems(permissions) { _: DialogInterface?, which: Int ->
                    when (which) {
                        0 -> (context as AssignmentUpdateActivity).deleteHomeworkDetails(homeworkDetails[layoutPosition], layoutPosition)
                    }
                }
                choices.show()
                true
            }

            ivSendHomeworkPictures.setOnClickListener {
                val homeworkImageURLs = ArrayList<String>()
                var i = 0
                var viewPosition = 0
                for (g in homeworkDetails) {
                    if (!(g.mediaUrl.isNullOrEmpty()) && "image" == g.descriptionType) {
                        homeworkImageURLs.add(g.mediaUrl!!)
                        if (g.mediaUrl == homeworkDetails[layoutPosition].mediaUrl) {
                            viewPosition = i
                        }
                        i++
                    }
                }
                val intent = Intent(context, FullScreenImageActivity::class.java)
                intent.putStringArrayListExtra("images", homeworkImageURLs)
                intent.putExtra("index", viewPosition)
                intent.putExtra("title", String.format("%s : %s", "Homework", "Image"))
                context.startActivity(intent)
            }

            ivReceivedHomeworkImage.setOnClickListener {
                val homeworkImageURLs = ArrayList<String>()
                var i = 0
                var viewPosition = 0
                for (g in homeworkDetails) {
                    if (!(g.mediaUrl.isNullOrEmpty()) && "image" == g.descriptionType) {
                        homeworkImageURLs.add(g.mediaUrl!!)
                        if (g.mediaUrl == homeworkDetails[layoutPosition].mediaUrl) {
                            viewPosition = i
                        }
                        i++
                    }
                }
                val intent = Intent(context, FullScreenImageActivity::class.java)
                intent.putStringArrayListExtra("images", homeworkImageURLs)
                intent.putExtra("index", viewPosition)
                intent.putExtra("title", String.format("%s : %s", "Homework", "Image"))
                context.startActivity(intent)
            }

            tvSendPdfName.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + homeworkDetails[layoutPosition].mediaUrl), "text/html")
                context.startActivity(intent)
            }

            tvSendPdfName.setOnLongClickListener() {
                val choices = AlertDialog.Builder(context)
                choices.setItems(permissions) { _: DialogInterface?, which: Int ->
                    when (which) {
                        0 -> (context as AssignmentUpdateActivity).deleteHomeworkDetails(homeworkDetails[layoutPosition], layoutPosition)
                    }
                }
                choices.show()
                true
            }

            tvPdfName.setOnLongClickListener() {
                val choices = AlertDialog.Builder(context)
                choices.setItems(permissions) { _: DialogInterface?, which: Int ->
                    when (which) {
                        0 -> (context as AssignmentUpdateActivity).deleteHomeworkDetails(homeworkDetails[layoutPosition], layoutPosition)
                    }
                }
                choices.show()
                true
            }

            tvPdfName.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + homeworkDetails[layoutPosition].mediaUrl), "text/html")
                context.startActivity(intent)
            }

        }
    }
}