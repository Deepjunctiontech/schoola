package in.junctiontech.school.schoolnew.setup;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.feesetup.feetype.FeeTypeActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.classfee.ClassFeeActivity;
import in.junctiontech.school.schoolnew.finance.setup.AccountActivity;
import in.junctiontech.school.schoolnew.schooldetails.SchoolPublicDetailsActivity;
import in.junctiontech.school.schoolnew.schoolsession.SessionActivity;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.subject.SubjectActivity;

public class SetupActivity extends AppCompatActivity {

    ArrayList<String> setupList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        buildSetupList();
    }

    void buildSetupList(){

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE,this).equalsIgnoreCase(Gc.ADMIN)) {
            HashMap<String,String> setDefaults = new HashMap<>();

            setDefaults.put(Gc.FEEPAYMENTALLOWED,Gc.TRUE);
            setDefaults.put(Gc.CLASSSECTIONEDITALLOWED, Gc.TRUE);
            setDefaults.put(Gc.FEESETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.HOMEWORKCREATEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.SUBJECTCREATEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.STUDENTSETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.GALLERYEDITALLOWED, Gc.TRUE);
            setDefaults.put(Gc.SENDMESSAGEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.STAFFSETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.CREATEEVENTALLOWED, Gc.TRUE);
            Gc.setSharedPreference(setDefaults,this);


            if (Gc.getSharedPreference(Gc.SESSIONEXISTS, this).equals(Gc.NOTFOUND)) {
                Intent intent = new Intent(SetupActivity.this, SessionActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }

            if (Gc.getSharedPreference(Gc.SCHOOLDETAILEXISTS, this).equals(Gc.NOTFOUND)) {
                Intent intent = new Intent(SetupActivity.this, SchoolPublicDetailsActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }

            if (Gc.getSharedPreference(Gc.FEETYPEEXISTS, this).equals(Gc.NOTFOUND)) {
                Intent intent = new Intent(SetupActivity.this, FeeTypeActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }
            if (!(Gc.getSharedPreference(Gc.CLASSEXISTS, this).equals(Gc.EXISTS))) {
                Intent intent = new Intent(SetupActivity.this, ClassSectionActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }

            if (Gc.getSharedPreference(Gc.CLASSFEESEXISTS, this).equals(Gc.NOTFOUND)) {
                Intent intent = new Intent(SetupActivity.this, ClassFeeActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }


            if (!(Gc.getSharedPreference(Gc.SUBJECTEXISTS, this).equals(Gc.EXISTS))) {
                Intent intent = new Intent(SetupActivity.this, SubjectActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }

            if (Gc.getSharedPreference(Gc.ACCOUNTEXISTS, this).equals(Gc.NOTFOUND)) {
                Intent intent = new Intent(SetupActivity.this, AccountActivity.class);
                intent.putExtra(Gc.SETUP, Gc.SETUP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                return;
            }
            Intent intent = new Intent(SetupActivity.this, AdminNavigationDrawerNew.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
            finish();
        }else {
            Intent intent = new Intent(SetupActivity.this, AdminNavigationDrawerNew.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
            finish();
        }
    }
}
