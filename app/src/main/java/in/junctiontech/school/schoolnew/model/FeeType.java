package in.junctiontech.school.schoolnew.model;

/**
 * Created by deep on 27/03/18.
 */

public class FeeType {
    public String FeeTypeID;
    public String FeeType;
    public String Frequency;
    public String generationDate;
    public String Status;
    public String CreatedOn;
    public String CreatedBy;
    public String UpdatedBy;
    public String UpdatedOn;

}
