package in.junctiontech.school.schoolnew.feesetup.feetype.receive;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolAccountsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class ReceiveFeeActivity extends AppCompatActivity {

    ProgressBar progressBar;

    MainDatabase mDb;

    TextView tv_student_name, tv_current_class;

    Button btn_select_month;
    Button btn_account;
    Button btn_effective_date;

    TextView tv_due_fees_total;

    TextView tv_session_total, tv_quarterly_total, tv_monthly_total;

    LinearLayout ll_annual, ll_quarterly,ll_monthly;

    String selectedStudent;
    SchoolStudentEntity studentEntity;

    int selectedAccountId;

    ArrayList<SchoolAccountsEntity> accountEntityList = new ArrayList<>();
    ArrayList<String>  accountsList = new ArrayList<>();

    ArrayList<FeeDue> feeDueList = new ArrayList<>();
    ArrayList<String> filledFeeAmount = new ArrayList<>();
    RecyclerView mRecyclerView;
    ReceiveFeeActivityAdapter adapter;

    AlertDialog.Builder builder;
    ArrayAdapter<String> accountAdapter;

    ArrayList<String> userPermissions = new ArrayList<>();

    String userRole;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_fee);

        mDb = MainDatabase.getDatabase(this);

        initializeViews();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

         userRole = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

        if ( userRole.equalsIgnoreCase(Gc.STUDENTPARENT)||  userRole.equalsIgnoreCase(Gc.STAFF) ) {
            if (!(Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED,this).equalsIgnoreCase(Gc.TRUE))){
                btn_account.setVisibility(View.GONE);
                btn_effective_date.setVisibility(View.GONE);
            }
        }

        selectedStudent = getIntent().getStringExtra("student");
        if(!("".equalsIgnoreCase(Strings.nullToEmpty(selectedStudent)))){
            studentEntity = new Gson().fromJson(selectedStudent, SchoolStudentEntity.class);

            tv_student_name.setText(studentEntity.StudentName);
            tv_current_class.setText(studentEntity.ClassName+" : "+ studentEntity.SectionName);
        }

        builder = new AlertDialog.Builder(ReceiveFeeActivity.this);
        accountAdapter =
                new ArrayAdapter<String>(ReceiveFeeActivity.this, android.R.layout.select_dialog_singlechoice);


                mDb.schoolAccountsModel().getSchoolAccounts().observe(this, new Observer<List<SchoolAccountsEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<SchoolAccountsEntity> schoolAccountsEntities) {
                        accountEntityList.clear();
                        accountsList.clear();
                        accountAdapter.clear();

                        accountEntityList.addAll(schoolAccountsEntities);
                        for (SchoolAccountsEntity account : accountEntityList) {
                            accountsList.add(account.AccountName + "-" + account.accountTypeName);
                        }

                        accountAdapter.addAll(accountsList);
                        accountAdapter.notifyDataSetChanged();
                    }
                });

        String date = new SimpleDateFormat("MMM-yyyy", Locale.US).format(new Date());
        btn_select_month.setText(date);
        try {
            getFeeDueForStudent();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6002266990", this, adContainer);
        }

    }

    void getFeeDueForStudent() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();
        p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this));
        p.put("admissionId", studentEntity.AdmissionId);
        p.put("monthYear",btn_select_month.getText().toString());

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/feesPayment/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                progressBar.setVisibility(View.GONE);
                switch (code) {
                    case "200":
                        try{

                            ArrayList<FeeDue> feeDues = new Gson()
                                    .fromJson(job.getJSONObject("result").getJSONObject("studentFees")
                                            .optString("fees"), new TypeToken<List<FeeDue>>(){}.getType());

                            feeDueList.clear();
                            filledFeeAmount.clear();
                            if (Objects.requireNonNull(feeDues).size()>0){
//                                buildFeeDueList(feeDues);

                                Collections.sort(feeDues, new Comparator<FeeDue>() {
                                    @Override
                                    public int compare(FeeDue feeDue, FeeDue t1) {
                                        String s1 = feeDue.FeeTypeFrequency;
                                        String s2 = t1.FeeTypeFrequency;
                                        return s1.compareToIgnoreCase(s2);
                                    }
                                });

                                feeDueList.addAll(feeDues);
                                for (FeeDue fee : feeDueList){
                                    filledFeeAmount.add(fee.AmountBalance);
                                }

                                adapter.notifyDataSetChanged();
                                calculateFeesSumForDisplay();
                            }

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(ReceiveFeeActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void initializeViews(){
        progressBar = findViewById(R.id.progressBar);
        tv_student_name = findViewById(R.id.tv_student_name);
        tv_current_class = findViewById(R.id.tv_current_class);

        btn_select_month = findViewById(R.id.btn_select_month);
        btn_effective_date = findViewById(R.id.btn_effective_date);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
        btn_effective_date.setText(simpleDateFormat.format(new Date()));

        initializeDatePickers();

        btn_account = findViewById(R.id.btn_account);
        btn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


                builder.setAdapter(accountAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        btn_account.setText(accountsList.get(i));
                        selectedAccountId = i;
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        tv_due_fees_total = findViewById(R.id.tv_due_fees_total);
        tv_session_total = findViewById(R.id.tv_session_total);
        tv_quarterly_total = findViewById(R.id.tv_quarterly_total);
        tv_monthly_total = findViewById(R.id.tv_monthly_total);

        mRecyclerView = findViewById(R.id.rv_fee_list);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ReceiveFeeActivityAdapter();
        mRecyclerView.setAdapter(adapter);


    }

    void initializeDatePickers(){
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener feedate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                btn_select_month.setText(sdf.format(myCalendar.getTime()));

                try {
                    getFeeDueForStudent();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };
        btn_select_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ReceiveFeeActivity.this, feedate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        final Calendar myCalendar1 = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener feePaidDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar1.set(Calendar.YEAR, year);
                myCalendar1.set(Calendar.MONTH, monthOfYear);
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                btn_effective_date.setText(sdf.format(myCalendar1.getTime()));

            }

        };
        btn_effective_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ReceiveFeeActivity.this, feePaidDate, myCalendar1
                        .get(Calendar.YEAR), myCalendar1.get(Calendar.MONTH),
                        myCalendar1.get(Calendar.DAY_OF_MONTH)).show();

            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void buildFeeDueList(ArrayList<FeeDue> feeDues){
        ll_quarterly.removeAllViews();
        ll_monthly.removeAllViews();
        ll_annual.removeAllViews();
        for (FeeDue fee: feeDues){
            final View view = getLayoutInflater().inflate(R.layout.item_fee_due, null);
            ((TextView) view.findViewById(R.id.tv_fee_name))
                    .setText(fee.FeeTypeName);

            ((TextView) view.findViewById(R.id.tv_fee_amount))
                    .setText(fee.FeeAmount);

            EditText et_due_amount = view.findViewById(R.id.et_due_amount);
                    et_due_amount.setText(fee.AmountBalance);
                    et_due_amount.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                            String amount = ((TextView) view.findViewById(R.id.tv_fee_amount))
//                                    .getText().toString();
//                            int original = Integer.parseInt(amount);
//                            int newamount = Integer.parseInt(charSequence.toString());
//                            if(newamount>original){
//                                ((EditText) view.findViewById(R.id.et_due_amount))
//                                        .setError(getString(R.string.due_amount), getDrawable(R.drawable.ic_alert));
//                            }
//
//                            calculateFeesSumForDisplay();
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            int original = Integer.parseInt(((TextView) view.findViewById(R.id.tv_fee_amount))
                                    .getText().toString());
                            if("".equals(editable.toString())){
                                ((EditText) view.findViewById(R.id.et_due_amount)).setText("0");
                            }else{
                                if(Integer.parseInt(editable.toString())> original)
                                    ((EditText) view.findViewById(R.id.et_due_amount))
                                            .setError(getString(R.string.due_amount), getDrawable(R.drawable.ic_alert));
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    calculateFeesSumForDisplay();

                                }
                            });
                        }
                    });

            switch(fee.FeeTypeFrequency) {

                case "Session":
                    view.findViewById(R.id.tv_month_name).setVisibility(View.GONE);
                    ll_annual.addView(view);
                break;

                case "Monthly":
                    view.findViewById(R.id.tv_month_name).setVisibility(View.VISIBLE);
                    ((TextView) view.findViewById(R.id.tv_month_name))
                            .setText(fee.MonthYear);
                    ll_monthly.addView(view);
                    break;

                case "Quarterly":
                    view.findViewById(R.id.tv_month_name).setVisibility(View.VISIBLE);
                    ((TextView) view.findViewById(R.id.tv_month_name))
                            .setText(fee.MonthYear);
                    ll_quarterly.addView(view);
                    break;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                calculateFeesSumForDisplay();
            }
        });
    }

    void calculateFeesSumForDisplay(){

        int quarterly=0,monthly=0,session=0;
        for(int i =0; i < feeDueList.size();i++){
            int amount = 0;
            if (!("".equalsIgnoreCase(filledFeeAmount.get(i))))amount = Integer.parseInt(filledFeeAmount.get(i));
            else amount=0;

            if(feeDueList.get(i).FeeTypeFrequency.equalsIgnoreCase("monthly"))monthly
                    += amount;
            if(feeDueList.get(i).FeeTypeFrequency.equalsIgnoreCase("quarterly"))quarterly
                    += amount;
            if(feeDueList.get(i).FeeTypeFrequency.equalsIgnoreCase("session"))session
                    += amount;
        }
        tv_monthly_total.setText(monthly+"");
        tv_quarterly_total.setText(quarterly+"");
        tv_session_total.setText(session+"");

        tv_due_fees_total.setText((session+monthly+quarterly)+"");
    }

    class ReceiveFeeActivityAdapter extends RecyclerView.Adapter<ReceiveFeeActivityAdapter.MyViewHolder>{

        @NonNull
        @Override
        public ReceiveFeeActivityAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_fee_due, parent, false);

            return new MyViewHolder(view);         }

        @Override
        public void onBindViewHolder(@NonNull ReceiveFeeActivityAdapter.MyViewHolder holder, final int position) {
            holder.tv_fee_name.setText(feeDueList.get(position).FeeTypeName);
            holder.tv_month_name.setText(feeDueList.get(position).MonthYear);
            holder.tv_fee_amount.setText(feeDueList.get(position).FeeAmount);
            holder.et_due_amount.setText(filledFeeAmount.get(position));

        }

        @Override
        public int getItemCount() {
            return feeDueList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_fee_name, tv_month_name, tv_fee_amount;
            TextView et_due_amount;
            public MyViewHolder(View itemView) {
                super(itemView);

                tv_fee_name = itemView.findViewById(R.id.tv_fee_name);
                tv_month_name = itemView.findViewById(R.id.tv_month_name);
                tv_fee_amount = itemView.findViewById(R.id.tv_fee_amount);
                et_due_amount = itemView.findViewById(R.id.et_due_amount);

                if (Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED,ReceiveFeeActivity.this).equalsIgnoreCase(Gc.TRUE))
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final int position = getAdapterPosition();
                        final Dialog dialog = new Dialog(ReceiveFeeActivity.this);
                        dialog.setContentView(R.layout.layout_fee_due);
                        dialog.setTitle(R.string.enter_amount);

                        final TextView tv_fee_frequency = dialog.findViewById(R.id.tv_fee_frequency);
                        tv_fee_frequency.setText(feeDueList.get(position).FeeTypeFrequency);

                        final TextView tv_fee_type_name = dialog.findViewById(R.id.tv_fee_type_name);
                        tv_fee_type_name.setText(feeDueList.get(position).FeeTypeName);

                        final TextView tv_fee_amount = dialog.findViewById(R.id.tv_fee_amount);
                        tv_fee_amount.setText(feeDueList.get(position).FeeAmount);

                        final TextView tv_fee_paid = dialog.findViewById(R.id.tv_fee_paid);
                        tv_fee_paid.setText(feeDueList.get(position).AmountPaid);

                        final TextView tv_to_be_paid = dialog.findViewById(R.id.tv_to_be_paid);
                        tv_to_be_paid.setText(feeDueList.get(position).AmountBalance);

                        final EditText et_item_amount = dialog.findViewById(R.id.et_item_amount);
                        et_item_amount.setText(filledFeeAmount.get(position));

                        Button btnSave          =  dialog.findViewById(R.id.save);
                        btnSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                notifyDataSetChanged();
                                filledFeeAmount.set(position,et_item_amount.getText().toString());
                                calculateFeesSumForDisplay();

                                dialog.dismiss();
                            }
                        });

                        Button btnCancel        =  dialog.findViewById(R.id.cancel);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                        dialog.setCancelable(false);

                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
            menu.findItem(R.id.menu_next).setVisible(false);
        if (!(Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED,this).equalsIgnoreCase(Gc.TRUE))){
            menu.findItem(R.id.action_save).setVisible(false);

        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();
                break;

            case R.id.action_save:
                if (validateInput()) {
                    try {
                        saveStudentFeePaymentToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
        }
        return super.onOptionsItemSelected(item);

    }

    Boolean validateInput(){

        if (getString(R.string.month).equals(btn_select_month.getText().toString())){
            Config.responseSnackBarHandler(getString(R.string.month)+" "+ getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            btn_select_month.setError(getString(R.string.error_field_required),getDrawable(R.drawable.ic_alert));
            return false;
        }
        if (getString(R.string.select_account).equals(btn_account.getText().toString())){
            Config.responseSnackBarHandler(getString(R.string.account)+" "+ getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            btn_account.setError(getString(R.string.error_field_required),getDrawable(R.drawable.ic_alert));
            return false;
        }

        if (feeDueList == null || feeDueList.size()==0){
            Config.responseSnackBarHandler(getString(R.string.fees_not_available),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        return true;
    }


    void saveStudentFeePaymentToServer() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONArray p = new JSONArray();
        final JSONObject param = new JSONObject();


        param.put("AdmissionId", studentEntity.AdmissionId);
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("Account", accountEntityList.get(selectedAccountId).AccountId);
        param.put("DOP",btn_effective_date.getText().toString());

        int size = feeDueList.size();

        for (int i = 0; i <size ; i++){
            p.put(new JSONObject()
                    .put("FeeId", feeDueList.get(i).FeeId)
                    .put("MonthYear", feeDueList.get(i).MonthYear)
                    .put("AmountBalance", feeDueList.get(i).AmountBalance)
                    .put("AmountPay",filledFeeAmount.get(i)));
        }

        param.put("feesPay",p);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/oneStepFeePayment"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case "200":

                        Intent intent = new Intent();
                        intent.putExtra("message", getString(R.string.success));
                        setResult(RESULT_OK, intent);
                        finish();

                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(ReceiveFeeActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

}

class FeeDue{
    public String FeeTypeName;
    public String FeeId;
    public String FeeTypeFrequency;
    public String MonthYear;
    public String FeeAmount;
    public String AmountPaid;
    public String AmountBalance;
    public String FeeStatus;
}
