package in.junctiontech.school.schoolnew.assignments;

import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.AssignmentEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class AssignmentMonthWiseActivity extends AppCompatActivity {

    Button btn_select_class;
    ProgressBar progressBar;

    Button btn_prev_month;
    TextView tv_display_month;
    Button btn_next_month;

    String firstdate;
    String monthYear;
    String lastdate;

    Date firstDayDate;
    Date lastDayDate;

    MainDatabase mDb ;

    CalendarAdapter calAdapter;
    RecyclerView mRecyclerView;
    ArrayList<String> dayList = new ArrayList<>();

    ArrayList<AssignmentEntity> assignmentList = new ArrayList<>();

    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    ArrayList<String> sectionList = new ArrayList<>();
    int selectedSection;
    String selectedSectionID;
    String selectedSectionname;

    int counter ;


    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_next_month.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        btn_prev_month.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        tv_display_month.setTextColor(colorIs);
        btn_select_class.setBackgroundColor(colorIs);
        progressBar.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_month_wise);

        mDb = MainDatabase.getDatabase(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btn_select_class = findViewById(R.id.btn_select_class);
        progressBar = findViewById(R.id.progressBar);
        btn_prev_month = findViewById(R.id.btn_prev_month);
        tv_display_month = findViewById(R.id.tv_display_month);
        btn_next_month = findViewById(R.id.btn_next_month);
        mRecyclerView = findViewById(R.id.rv_calendar);

        RecyclerView.LayoutManager calLayoutManager = new GridLayoutManager(this, 7);
        mRecyclerView.setLayoutManager(calLayoutManager);

        calAdapter = new CalendarAdapter();
        mRecyclerView.setAdapter(calAdapter);

        monthYear = new SimpleDateFormat("yyyy-MM", Locale.US).format(new Date());
        firstdate =  monthYear + "-01" ;

        try {
            firstDayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(firstdate);

            Calendar c = Calendar.getInstance();
            c.setTime(firstDayDate); // yourdate is an object of type Date

            String monthName = (String)android.text.format.DateFormat.format("MMMM", firstDayDate);
            int year = c.get(Calendar.YEAR);

            tv_display_month.setText(monthName + year);

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            int maxDays = c.getActualMaximum(Calendar.DATE);
            lastdate =  new SimpleDateFormat("yyyy-MM", Locale.US).format(new Date()) +"-"+ maxDays ;

            lastDayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(lastdate);

            buildCalenderForMonth(dayOfWeek, maxDays);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/7351053643",this,adContainer);
        }

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)){
            btn_select_class.setVisibility(View.GONE);

            String s = Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this );
            StudentInformation studentInformation = new Gson().fromJson(s, StudentInformation.class);

            selectedSectionID = Strings.nullToEmpty( studentInformation.SectionId);
            selectedSectionname = studentInformation.ClassName + " " + studentInformation.SectionName;

            mDb.assignmentModel().getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate).observe(AssignmentMonthWiseActivity.this, new Observer<List<AssignmentEntity>>() {
                @Override
                public void onChanged(@Nullable List<AssignmentEntity> assignmentEntities) {
                    assignmentList.clear();
                    assignmentList.addAll(assignmentEntities);
                    calAdapter.notifyDataSetChanged();
//                    Toast.makeText(AssignmentMonthWiseActivity.this,"When student selected : "+ (counter++), Toast.LENGTH_SHORT).show();

                    mDb.assignmentModel()
                            .getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate)
                            .removeObserver(this);
                }
            });

            getAssignments(selectedSectionID, firstdate, lastdate);


        }else {
            btn_select_class.setVisibility(View.VISIBLE);

            mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this))
                    .observe(this, new Observer<List<ClassNameSectionName>>() {
                        @Override
                        public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                            classNameSectionList.clear();
                            sectionList.clear();
                            classSectionAdapter.clear();
                            classNameSectionList.addAll(classNameSectionNames);
                            for(int i = 0; i < classNameSectionList.size(); i++){
                                sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
                            }
                            classSectionAdapter.addAll(sectionList);
                            classSectionAdapter.notifyDataSetChanged();
                        }
                    });


        }


//            selectedSectionID = "";


        classListBuilder = new AlertDialog.Builder(AssignmentMonthWiseActivity.this);
        classSectionAdapter =
                new ArrayAdapter<String>(AssignmentMonthWiseActivity.this, android.R.layout.select_dialog_singlechoice);

        btn_select_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classListBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                classListBuilder.setAdapter(classSectionAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        btn_select_class.setText(classNameSectionList.get(i).ClassName + " "
                                + classNameSectionList.get(i).SectionName);
                        selectedSection = i;
                        selectedSectionID = classNameSectionList.get(selectedSection).SectionId;
                            getAssignments(selectedSectionID, firstdate, lastdate);

                            selectedSectionname = classNameSectionList.get(selectedSection).ClassName
                                    + " "+ classNameSectionList.get(selectedSection).SectionName;

                        mDb.assignmentModel().getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate).observe(AssignmentMonthWiseActivity.this, new Observer<List<AssignmentEntity>>() {
                            @Override
                            public void onChanged(@Nullable List<AssignmentEntity> assignmentEntities) {
                                assignmentList.clear();
                                assignmentList.addAll(assignmentEntities);
                                calAdapter.notifyDataSetChanged();
//                                Toast.makeText(AssignmentMonthWiseActivity.this,"ClassSelected : "+ (counter++), Toast.LENGTH_SHORT).show();

                                mDb.assignmentModel()
                                        .getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate)
                                        .removeObserver(this);
                            }
                        });

                    }
                });
                AlertDialog dialog = classListBuilder.create();
                dialog.show();
            }
        });


        btn_prev_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equalsIgnoreCase(Strings.nullToEmpty(selectedSectionID))){
                    Config.responseSnackBarHandler(getString(R.string.select_class),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);
                    return;
                }

                displayMonth(-1);

            }
        });

        btn_next_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equalsIgnoreCase(Strings.nullToEmpty(selectedSectionID))){
                    Config.responseSnackBarHandler(getString(R.string.select_class),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);
                    return;
                }
                displayMonth(1);
            }
        });
    }

    void displayMonth(int addmonth){
            Calendar c = Calendar.getInstance();
            c.setTime(firstDayDate);

            c.add(Calendar.MONTH, addmonth);
            firstDayDate = c.getTime();
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            int maxDays = c.getActualMaximum(Calendar.DATE);

            c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            lastDayDate = c.getTime();

            firstdate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(firstDayDate);
            lastdate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(lastDayDate);
            monthYear = new SimpleDateFormat("yyyy-MM", Locale.US).format(firstDayDate);

            String monthName = (String)android.text.format.DateFormat.format("MMMM", firstDayDate);
            int year = c.get(Calendar.YEAR);

            tv_display_month.setText(monthName + year);

            buildCalenderForMonth(dayOfWeek, maxDays);


            mDb.assignmentModel().getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate)
                    .observe(AssignmentMonthWiseActivity.this, new Observer<List<AssignmentEntity>>() {
                @Override
                public void onChanged(@Nullable List<AssignmentEntity> assignmentEntities) {
                    assignmentList.clear();
                    assignmentList.addAll(assignmentEntities);
                    calAdapter.notifyDataSetChanged();
//                    Toast.makeText(AssignmentMonthWiseActivity.this,"NextPrevious : "+(counter++), Toast.LENGTH_SHORT).show();
                    mDb.assignmentModel()
                            .getAssignmentsBetweenDates(selectedSectionID,firstDayDate, lastDayDate)
                            .removeObserver(this);
                }
            });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.refresh:
                if ("".equalsIgnoreCase(Strings.nullToEmpty(selectedSectionID))){
                    Config.responseSnackBarHandler(getString(R.string.select_class),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);
                    return false;
                }
                getAssignments(selectedSectionID, firstdate, lastdate);

                break;


        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void buildCalenderForMonth(int dayOfWeek, int maxDays){
        dayList.clear();
        dayList.add("Sun");
        dayList.add("Mon");
        dayList.add("Tue");
        dayList.add("Wed");
        dayList.add("Thu");
        dayList.add("Fri");
        dayList.add("Sat");
        switch (dayOfWeek){
            case 1:
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 2:
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 3:
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 4:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 5:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 6:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 7:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
        }
        calAdapter.notifyDataSetChanged();

    }

    void getAssignments(String sectionid , String fromDate , String toDate){

        progressBar.setVisibility(View.VISIBLE);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "homework/homework"
                ;
        try {

//            if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)){
                url = url + "/" + new JSONObject()
                        .put("sectionid", selectedSectionID)
                        .put("BetweenDate", fromDate+"||"+toDate);

//            }else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)){
//                url = url + "/" + new JSONObject()
//                        .put("sectionid", Gc.getSharedPreference(Gc.APPSESSION, this))
//                        .put("createdby", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this));
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                @Override
                                public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                                        throws JsonParseException {
                                    try {
                                        return df.parse(json.getAsString());
                                    } catch (ParseException e) {
                                        return null;
                                    }
                                }
                            });
                            Gson gson = gsonBuilder.create();

                            final ArrayList<AssignmentEntity> assignmentEntities = gson
                                    .fromJson(resultjobj.getString("homeworkDetails"), new TypeToken<List<AssignmentEntity>>(){}.getType());

                            int size = assignmentEntities.size();
                            for (int i =0; i <size ; i++ ){
                                if (assignmentEntities.get(i).dosubmission == null)
                                    assignmentEntities.get(i).dosubmission = assignmentEntities.get(i).dateofhomework;
                            }

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.assignmentModel().deleteMonthForSection(selectedSectionID, firstDayDate, lastDayDate);
                                    mDb.assignmentModel().insertAssignment(assignmentEntities);
                                }
                            }).start();
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);


                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE204:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                mDb.assignmentModel().deleteMonthForSection(selectedSectionID, firstDayDate, lastDayDate);
                            }
                        }).start();
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_green);
                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, AssignmentMonthWiseActivity.this);

                        Intent intent2 = new Intent(AssignmentMonthWiseActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();
                        break;
                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, AssignmentMonthWiseActivity.this);

                        Intent intent1 = new Intent(AssignmentMonthWiseActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(AssignmentMonthWiseActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder>{

        @NonNull
        @Override
        public CalendarAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calender_date, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CalendarAdapter.MyViewHolder holder, int position) {
            String dateName = dayList.get(position);
            if (position < 7 ){
                holder.tv_cal_date.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                holder.tv_cal_date.setText(dateName);
            }else{
                if (dayList.get(position).equalsIgnoreCase("")){
                    holder.tv_cal_date.setBackground(null);
                    holder.tv_cal_date.setText(null);
                }else {
                    holder.tv_cal_date.setText(dateName);
                    int dateInt = Integer.parseInt(dateName);
                    if (dateInt>0 && dateInt < 10){
                        String key = monthYear +"-" +"0" + dateInt  ;
                        holder.tv_cal_date.setBackground(null);
                        for (AssignmentEntity a : assignmentList){
                            if (key.equalsIgnoreCase(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(a.dateofhomework))){
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                                    break;
                            }
                        }

                    }else{
                        String key = monthYear +"-"+ dateInt;
                        holder.tv_cal_date.setBackground(null);
                        for (AssignmentEntity a : assignmentList){
                            if (key.equalsIgnoreCase(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(a.dateofhomework))){
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                                break;
                            }
                        }
                    }
                }
            }
        }


//        Boolean containsKey(String key ){
//
//            for(int i=0; i < assignmentList.size(); i++) {
//                AssignmentEntity s = assignmentList.get(i);
//                //search the string
//                if(s.dosubmission.equal) {
//                    return i
//                }
//            }
//            return -1
//
//            return false;
//        }

        @Override
        public int getItemCount() {
            return dayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_cal_date;
            public MyViewHolder(View itemView) {
                super(itemView);
                tv_cal_date = itemView.findViewById(R.id.tv_cal_date);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if ("".equalsIgnoreCase(Strings.nullToEmpty(selectedSectionID))){
                            Config.responseSnackBarHandler(getString(R.string.select_class),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);
                            return;
                        }

                        int position = getAdapterPosition();
                        String dateName = dayList.get(position);
                        if (position < 7 ){
                            Toast.makeText(AssignmentMonthWiseActivity.this,dayList.get(position),Toast.LENGTH_SHORT)
                                    .show();
                        }else {
                            if (dayList.get(position).equalsIgnoreCase("")){
                                Toast.makeText(AssignmentMonthWiseActivity.this,"Empty",Toast.LENGTH_SHORT).show();
                            }else{
                                int dateInt = Integer.parseInt(dateName);
                                final String key ;
                                if (dateInt>0 && dateInt < 10){
                                    key = monthYear +"-"+"0" +dateInt  ;
                                }else{
                                    key = monthYear +"-"+ dateInt;
                                }
                                Intent intent = new Intent(AssignmentMonthWiseActivity.this, Assignments.class );
                                intent.putExtra("assignmentdate", key);
                                intent.putExtra("selectedSectionID",selectedSectionID);
                                intent.putExtra("selectedSectionname", selectedSectionname);
                                startActivity(intent);


                            }
                        }
                    }
                });
            }
        }
    }

}
