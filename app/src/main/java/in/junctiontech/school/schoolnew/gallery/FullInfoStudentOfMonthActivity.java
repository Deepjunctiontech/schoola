package in.junctiontech.school.schoolnew.gallery;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 20-11-2017.
 */

public class FullInfoStudentOfMonthActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private int appColor;
    private ProgressDialog progressbar;
    private ImageView iv_student_of_the_year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_student_of_the_month);
        sp = Prefs.with(this).getSharedPreferences();

        appColor = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(appColor));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.information));
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.uploading));

        iv_student_of_the_year =  findViewById(R.id.iv_student_of_the_year1);
        setBitmapfromUrl(sp.getString(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, ""));
        TextView  tv_student_of_the_year1= findViewById(R.id.tv_student_of_the_year1);
        tv_student_of_the_year1 .setText(sp.getString(Config.STUDENT_OF_THE_MONTH_MESSAGE, ""));
        tv_student_of_the_year1.setTextColor(appColor);
    }

    public void setBitmapfromUrl(String imageUrl) {
        File targetImage = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME +
                "/" + Config.SCHOOL_IMAGE_FOLDER_NAME, sp.getString(Config.STUDENT_OF_THE_MONTH_PATH,""));

        if (targetImage.exists()){
            try {
                iv_student_of_the_year.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(targetImage)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else
        Glide
                .with(this)
                .load(imageUrl)
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.ic_happy_smile)
                    .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(iv_student_of_the_year);

    }    @Override
    public void onBackPressed() {
        Log.e("bbbbbbbbbbbb", "bbbbbbb");
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }
}
