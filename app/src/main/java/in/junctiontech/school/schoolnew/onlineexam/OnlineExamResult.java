package in.junctiontech.school.schoolnew.onlineexam;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.onlineexam.db.OnlineExamModel;
import in.junctiontech.school.schoolnew.onlineexam.db.OnlineExamResultModel;

public class OnlineExamResult extends AppCompatActivity {

    private TextView tv_number_of_question, tv_number_of_attempted_question,
            tv_number_of_right_question, tv_number_of_wrong_question,
            tv_exam_name, tv_exam_level, tv_subject_name, tv_online_exam_question_data,
            tv_online_exam_your_answer, tv_online_exam_right_answer,
            tv_online_exam_question_solution, tv_online_exam_answer_description,
            tv_online_exam_question_list, tv_online_exam_result_cuttoff,
            tv_online_exam_scord_card_status_wish, tv_online_exam_scord_card_status,
            tv_online_exam_gain_marks, tv_online_exam_total_marks, tv_number_of_not_attempted_question;
    TextView label_subject;

    private RecyclerView recycler_view_online_exam_result;
    private OnlineExamResultModel onlineExamData;
    private MyRecycleAdapter madapter;
    private LinearLayout ly_online_exam_result_details, ly_questions_color_description,
            ll_online_exam_question_solution,ll_exam_result_questions;
    private ImageView iv_online_exam_scord_card_right, iv_online_exam_scord_card_left;

    Button btn_result_summary,btn_goto_questions;

    ArrayList<OnlineExamModel.ExamSectionDetails.SectionQuestion> questionsList = new ArrayList<>();

    private void setColorApp() {

        int colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
     //   getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_exam_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1746076801",this,adContainer);
        }

        String examString = getIntent().getStringExtra("studentOnlineExamResult");

        onlineExamData = new Gson().fromJson(examString, OnlineExamResultModel.class);
        Log.e("ritu", (new Gson()).toJson(onlineExamData));

        String result = "Pass";
        int maxMarks = 0;
        int obtainedMarks = 0;
        for (OnlineExamResultModel.ExamResultModel resultModel : onlineExamData.result){
            result = resultModel.result;
            maxMarks = maxMarks + Integer.parseInt(resultModel.maxMarks);
            obtainedMarks = obtainedMarks + Integer.parseInt(resultModel.obtainMarks);

        }
        int numberOfQuestions = 0;
        int numberofAttempt = 0;
        int numberofNotAttempted = 0;
        int numberOfCorrect = 0;
        int numberOfInCorrect = 0;
        String examSubjects = "" ;

        for (OnlineExamModel.ExamSectionDetails sections : onlineExamData.examSections){

            if (examSubjects.equalsIgnoreCase("")){
                examSubjects =  sections.sectionName + getString(R.string.separaterdash) + sections.SubjectName;
            }else{
                examSubjects = examSubjects + "\n" + sections.sectionName + getString(R.string.separaterdash) + sections.SubjectName;
            }

            for (OnlineExamModel.ExamSectionDetails.SectionQuestion questionOption: sections.sectionQuestions){
                numberOfQuestions++;
                questionsList.add(questionOption);

                if (questionOption.studentAnswer != null){
                    numberofAttempt++;
                    switch (questionOption.questionType){
                        case "MultipleChoice":
                            if (questionOption.studentAnswer.equalsIgnoreCase(questionOption.answer)){
                                numberOfCorrect++;
                            }else{
                                numberOfInCorrect++;

                            }
                            break;

                            default:
                                if (Integer.parseInt(questionOption.obtainMarks) > 0){
                                    numberOfCorrect++;
                                }else{
                                    numberOfInCorrect++;
                                }
                                break;
                    }


                }else{
                    numberofNotAttempted++;
                }


            }

        }

        tv_online_exam_gain_marks.setText(String.valueOf(obtainedMarks));
        tv_online_exam_total_marks.setText(String.valueOf(maxMarks));

        if ("Pass".equalsIgnoreCase(result)) {
            tv_online_exam_scord_card_status_wish.setText(getString(R.string.congratulations));
            tv_online_exam_scord_card_status.setText(getString(R.string.you_cleared_the_exam));
            iv_online_exam_scord_card_right.setImageResource(R.drawable.ic_happy_smile);
            iv_online_exam_scord_card_left.setImageResource(R.drawable.ic_happy_smile);
        } else {
            tv_online_exam_scord_card_status_wish.setText(getString(R.string.sorry));
            tv_online_exam_scord_card_status.setText(getString(R.string.you_not_cleared_the_exam));
            iv_online_exam_scord_card_right.setImageResource(R.drawable.ic_sad_smile);
            iv_online_exam_scord_card_left.setImageResource(R.drawable.ic_sad_smile);
        }


        tv_exam_name.setText(onlineExamData.onlineExamName);
        tv_exam_level.setText(onlineExamData.examCheckStatus);
        tv_number_of_question.setText(String.valueOf(numberOfQuestions));
        tv_number_of_attempted_question.setText(String.valueOf(numberofAttempt)+
                        "/" +
                        String.valueOf(numberOfQuestions));

        tv_number_of_not_attempted_question.setText(String.valueOf(numberofNotAttempted));


        tv_number_of_right_question.setText(String.valueOf(numberOfCorrect));
        tv_number_of_wrong_question.setText(String.valueOf(numberOfInCorrect));
        label_subject.setText(getString(R.string.section)+ getString(R.string.separaterdash) + getString(R.string.subject));
        tv_subject_name.setText(examSubjects);
        //  tv_online_exam_result_status.setText(onlineExamData.getOnline_student_status());
//        tv_online_exam_result_cuttoff.setText(onlineExamData.getCutoff());
    }

    private void initViews() {
        iv_online_exam_scord_card_left = findViewById(R.id.iv_online_exam_scord_card_left);
        iv_online_exam_scord_card_right =  findViewById(R.id.iv_online_exam_scord_card_right);
        tv_online_exam_scord_card_status_wish =  findViewById(R.id.tv_online_exam_scord_card_status_wish);
        tv_online_exam_scord_card_status =  findViewById(R.id.tv_online_exam_scord_card_status);
        tv_online_exam_gain_marks =  findViewById(R.id.tv_online_exam_gain_marks);
        tv_online_exam_total_marks =  findViewById(R.id.tv_online_exam_total_marks);

        tv_online_exam_question_list =  findViewById(R.id.tv_online_exam_question_list);
        tv_number_of_question =  findViewById(R.id.tv_number_of_question);
        tv_number_of_attempted_question =  findViewById(R.id.tv_number_of_attempted_question);
        tv_number_of_not_attempted_question =  findViewById(R.id.tv_number_of_not_attempted_question);
        tv_number_of_right_question =  findViewById(R.id.tv_number_of_right_question);
        tv_number_of_wrong_question =  findViewById(R.id.tv_number_of_wrong_question);
        tv_exam_name =  findViewById(R.id.tv_exam_name);
        tv_exam_level =  findViewById(R.id.tv_exam_level);

        btn_result_summary = findViewById(R.id.btn_result_summary);
        btn_result_summary.setOnClickListener(v -> {
            ly_online_exam_result_details.setVisibility(View.VISIBLE);
            ll_exam_result_questions.setVisibility(View.GONE);

        });

        btn_goto_questions = findViewById(R.id.btn_goto_questions);
        btn_goto_questions.setOnClickListener(v -> {
            ly_online_exam_result_details.setVisibility(View.GONE);
            ll_exam_result_questions.setVisibility(View.VISIBLE);
            viewQuestions(v);
        });

        label_subject = findViewById(R.id.label_subject);
        tv_subject_name =  findViewById(R.id.tv_online_exam_subject_name);
        //  tv_online_exam_result_status =  findViewById(R.id.tv_online_exam_result_status);
        tv_online_exam_result_cuttoff =  findViewById(R.id.tv_online_exam_result_cuttoff);

        recycler_view_online_exam_result =  findViewById(R.id.recycler_view_online_exam_result);
        ly_online_exam_result_details =  findViewById(R.id.ly_online_exam_result_details);
        ly_questions_color_description =  findViewById(R.id.ly_questions_color_description);
        ll_online_exam_question_solution =  findViewById(R.id.ll_online_exam_question_solution);
        ll_exam_result_questions = findViewById(R.id.ll_exam_result_questions);

        tv_online_exam_question_data = findViewById(R.id.tv_online_exam_question_data);
        tv_online_exam_your_answer =  findViewById(R.id.tv_online_exam_your_answer);
        tv_online_exam_right_answer =  findViewById(R.id.tv_online_exam_right_answer);
        tv_online_exam_question_solution =  findViewById(R.id.tv_online_exam_question_solution);
        tv_online_exam_answer_description =  findViewById(R.id.tv_online_exam_answer_description);


        tv_online_exam_question_list.setVisibility(View.GONE);
        recycler_view_online_exam_result.setVisibility(View.GONE);
        ly_questions_color_description.setVisibility(View.GONE);

        ll_online_exam_question_solution.setVisibility(View.GONE);

    }


    public class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {
        Context context;
        ArrayList<OnlineExamModel.ExamSectionDetails.SectionQuestion> onlineExamData;

        public MyRecycleAdapter(Context context, ArrayList<OnlineExamModel.ExamSectionDetails.SectionQuestion> onlineExamData) {
            this.context = context;
            this.onlineExamData = onlineExamData;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.day_homework, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            OnlineExamModel.ExamSectionDetails.SectionQuestion quesObj = onlineExamData.get(position);

            //animate(holder);
            holder.month_date.setTextColor(Color.WHITE);
            holder.month_date.setText((position + 1) + "");


            if (quesObj.studentAnswer == null)
                holder.month_date.setBackgroundResource(R.drawable.not_attempted_question);
            else if (quesObj.studentAnswer.equalsIgnoreCase(quesObj.answer)) {
                holder.month_date.setBackgroundResource(R.drawable.right_answer);
            } else holder.month_date.setBackgroundResource(R.drawable.gridborder);

        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return onlineExamData.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView month_date;

            public MyViewHolder(View itemView) {
                super(itemView);
                month_date =  itemView.findViewById(R.id.month_date);
                month_date.setOnClickListener(v -> {
                    ll_online_exam_question_solution.setVisibility(View.VISIBLE);
//                        tv_online_exam_question_list.setVisibility(View.GONE);
//                        recycler_view_online_exam_result.setVisibility(View.GONE);

                    OnlineExamModel.ExamSectionDetails.SectionQuestion quesObj = onlineExamData.get(getLayoutPosition());
                    tv_online_exam_question_data.setText("Q " + (getLayoutPosition() + 1) + ". "
                            + quesObj.qustion);

                    if (null == quesObj.studentAnswer)
                        tv_online_exam_your_answer.setText(getString(R.string.your_answer) + getString(R.string.not_attempted_description));
                    else {
                        String textString = "";
                        if (quesObj.questionType.equals("MultipleChoice")) {
                            if (quesObj.studentAnswer.length() == 1) {
                                textString = getString(R.string.your_answer)
                                        + "(" + quesObj.studentAnswer + ")\n "
                                        + quesObj.qus_option.get(Integer.parseInt(quesObj.studentAnswer) - 1).Options;

                            } else if (quesObj.studentAnswer.length() > 1) {
                                ArrayList<String> selectedOptions = new ArrayList<>(Arrays.asList(quesObj.studentAnswer.split(",")));

                                textString = getString(R.string.your_answer) + "("
                                        + quesObj.studentAnswer + ")\n ";

                                for (String s : selectedOptions) {
                                    textString = textString
                                            + quesObj.qus_option.get(Integer.parseInt(s) - 1).Options + "\n";
                                }

                            }
                        }
                        else{
                            textString = getString(R.string.your_answer)
                                    + " " + quesObj.studentAnswer ;
                        }

                        tv_online_exam_your_answer.setText(textString);
                    }
                    if (quesObj.questionType.equals("MultipleChoice")) {
                        if (quesObj.answer.length() == 1) {
                            String textString = getString(R.string.right_answer_details) + "("
                                    + quesObj.answer + ")\n " + quesObj.qus_option.get(Integer.parseInt(quesObj.answer) -1).Options;
                            tv_online_exam_right_answer.setText(textString);
                        }else if (quesObj.answer.length() > 1){
                            String textString = getString(R.string.right_answer_details) + "("
                                    + quesObj.answer + ") \n";
                            ArrayList<String>  selectedOptions = new ArrayList<>(Arrays.asList(quesObj.answer.split(",")));

                            for (String s: selectedOptions){
                                textString = textString
                                        + quesObj.qus_option.get(Integer.parseInt(s) -1).Options + "\n";
                            }
                            tv_online_exam_right_answer.setText(textString);

                        }
                    }
                    else{
                        String textString = getString(R.string.right_answer_details) + " "
                                + quesObj.answer;
                        tv_online_exam_right_answer.setText(textString);
                    }

                    tv_online_exam_question_solution.setText(quesObj.qust_solution);
                    tv_online_exam_answer_description.setText(quesObj.qust_ans_description);
                });
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    public void viewQuestions(View view) {
        tv_online_exam_question_list.setVisibility(View.VISIBLE);
        recycler_view_online_exam_result.setVisibility(View.VISIBLE);
        ly_questions_color_description.setVisibility(View.VISIBLE);
        ly_online_exam_result_details.setVisibility(View.GONE);

        madapter = new MyRecycleAdapter(this, questionsList);
        RecyclerView.LayoutManager mLayoutManager = new StaggeredGridLayoutManager(5, 1);
        recycler_view_online_exam_result.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recycler_view_online_exam_result.setHasFixedSize(true);
        recycler_view_online_exam_result.setItemAnimator(itemAnimator);
        recycler_view_online_exam_result.setAdapter(madapter);


    }
}
