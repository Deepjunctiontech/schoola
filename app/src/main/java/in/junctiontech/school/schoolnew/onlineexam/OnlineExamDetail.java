package in.junctiontech.school.schoolnew.onlineexam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.onlineexam.db.OnlineExamModel;

import static in.junctiontech.school.schoolnew.common.Gc.SCHOOLPREF;

public class OnlineExamDetail extends AppCompatActivity {

    ProgressBar progressBar;
    OnlineExamModel exam ;

    RecyclerView mRecyclerView;
    ListAdapter adapter;

    TextView tv_exam_name,tv_duration,tv_max_marks,tv_questions;

    Button btn_exam_status,btn_exam_result;

    TextView tv_open_date,tv_open_time,tv_close_date,tv_close_time;

    SharedPreferences pref;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_exam_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode

        progressBar = findViewById(R.id.progressBar);

        btn_exam_status = findViewById(R.id.btn_exam_status);
        btn_exam_result = findViewById(R.id.btn_exam_result);
        tv_exam_name = findViewById(R.id.tv_exam_name);
        tv_duration = findViewById(R.id.tv_duration);
        tv_max_marks = findViewById(R.id.tv_max_marks);
        tv_questions = findViewById(R.id.tv_questions);

        tv_open_date = findViewById(R.id.tv_open_date);
        tv_open_time = findViewById(R.id.tv_open_time);
        tv_close_date = findViewById(R.id.tv_close_date);
        tv_close_time = findViewById(R.id.tv_close_time);

        mRecyclerView = findViewById(R.id.rv_exam_sections);

        exam = new Gson().fromJson(getIntent().getStringExtra("exam"), OnlineExamModel.class);

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6020363732",this,adContainer);
        }

        if (exam!=null){

            if (exam.examSectionDetails == null){
                exam.examSectionDetails = new ArrayList<>();
            }

            mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

            mRecyclerView.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(layoutManager);

            adapter = new ListAdapter();
            mRecyclerView.setAdapter(adapter);

            tv_exam_name.setText(Strings.nullToEmpty(exam.onlineExamName));
            btn_exam_status.setText(Strings.nullToEmpty(exam.status));
            btn_exam_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startExam(exam);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            btn_exam_result.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!(exam.status.equalsIgnoreCase("Finish"))){
                        Config.responseSnackBarHandler(getString(R.string.your_test_was_not_finished_this_test_available_till_timer_not_finishes),
                                findViewById(R.id.top_layout),R.color.fragment_first_green);
                    }else{
                        try {
                            getFinishedExamDetailsFromServer(exam);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            tv_duration.setText(Strings.nullToEmpty(exam.examDuration));
            tv_max_marks.setText(Strings.nullToEmpty(exam.examMaxMarks));
            tv_questions.setText(Strings.nullToEmpty(exam.numberOfQuestions));
            tv_open_date.setText(Strings.nullToEmpty(exam.examOpenDate));
            tv_open_time.setText(Strings.nullToEmpty(exam.examOpenTime));
            tv_close_date.setText(Strings.nullToEmpty(exam.examCloseDate));
            tv_close_time.setText(Strings.nullToEmpty(exam.examCloseTime));

            if(setStatusMessageForButton(Strings.nullToEmpty(exam.status))){
                btn_exam_status.setEnabled(true);
                btn_exam_result.setVisibility(View.GONE);
            }else {
                btn_exam_status.setEnabled(false);
                btn_exam_result.setVisibility(View.VISIBLE);
            }


            try {
                getExamDetailsFromServer(exam);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setColorApp();
        }
    }

    Boolean setStatusMessageForButton(String status){


        // Start or Continue will allow test to Start
        // Rule 1 - Date Time  should be valid
        // Status should be New or Running
        // Atleast one section should exist

        if (exam.examSectionDetails == null || exam.examSectionDetails.size() < 1) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());

        try {
            Date examStartDate = sdf.parse(exam.examOpenDate+" "+exam.examOpenTime);
            Date examEndDate = sdf.parse(exam.examCloseDate+" "+exam.examCloseTime);

            Date now = new Date();

            if(now.compareTo(examStartDate) > 0 && now.compareTo(examEndDate) < 0){
                switch (status){
                    case "New":
                        btn_exam_status.setText(R.string.start_test);
                        btn_exam_result.setVisibility(View.GONE);
                        return true;

                    case "Running":
                        btn_exam_status.setText("Continue Test");
                        btn_exam_result.setVisibility(View.GONE);

                        return  true;

                        default:
                            btn_exam_status.setText(R.string.completed);
                            btn_exam_result.setVisibility(View.VISIBLE);

                            return false;
                }
            }else{
                btn_exam_status.setText("Test can not be started");
                btn_exam_result.setVisibility(View.VISIBLE);

                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    void getExamDetailsFromServer(final OnlineExamModel examEntity) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        btn_exam_status.setVisibility(View.GONE);
        btn_exam_result.setVisibility(View.GONE);

        final JSONObject p = new JSONObject();

        p.put("onlineExamAssignToStudentId", examEntity.onlineExamAssignToStudentId);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "onlineExam/getExamInstructions/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                btn_exam_status.setVisibility(View.VISIBLE);
                btn_exam_result.setVisibility(View.VISIBLE);

                Log.i("Online Exams List ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            OnlineExamModel onlineExam = new Gson()
                                    .fromJson(resultjobj.getString("studentExamInstructions"), OnlineExamModel.class);

                            exam.numberOfQuestions = Strings.nullToEmpty(onlineExam.numberOfQuestions);
                            tv_questions.setText(exam.numberOfQuestions);
                            exam.examTimer = Strings.nullToEmpty(onlineExam.examTimer);
                            exam.examMaxMarks = Strings.nullToEmpty(onlineExam.examMaxMarks);
                            tv_max_marks.setText(exam.examMaxMarks);

                            exam.status = onlineExam.status;

                            exam.examSectionDetails.addAll(onlineExam.examSectionDetails);
                            adapter.notifyDataSetChanged();

                            if(setStatusMessageForButton(Strings.nullToEmpty(exam.status))){
                                btn_exam_status.setEnabled(true);
                            }else {
                                btn_exam_status.setEnabled(false);
                            }

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(OnlineExamDetail.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    void getFinishedExamDetailsFromServer(final OnlineExamModel examEntity) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        btn_exam_result.setVisibility(View.GONE);
        btn_exam_status.setVisibility(View.GONE);

        final JSONObject p = new JSONObject();

        p.put("onlineExamAssignToStudentId", examEntity.onlineExamAssignToStudentId);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "onlineExam/getStudentExamResultDetails/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                btn_exam_result.setVisibility(View.VISIBLE);
                btn_exam_status.setVisibility(View.VISIBLE);
                Log.i("Online Exams Result ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result").getJSONObject("studentOnlineExamResult");

                            if (resultjobj.length() > 0 ){
                                String result = resultjobj.toString();
                                Intent intent = new Intent(OnlineExamDetail.this, OnlineExamResult.class);
                                intent.putExtra("studentOnlineExamResult", result);
                                startActivity(intent);
                                finish();
                            }else{
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                            }

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(OnlineExamDetail.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    void startExam(final OnlineExamModel examEntity) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();

        p.put("onlineExamAssignToStudentId", examEntity.onlineExamAssignToStudentId);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "onlineExam/studentOnlineExam/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                Log.i("Online Exams List ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result").getJSONObject("studentExamDetails");

                            ArrayList<OnlineExamModel.ExamSectionDetails> examSections = new Gson()
                                    .fromJson(resultjobj.optString("examSections"),
                                            new TypeToken<List<OnlineExamModel.ExamSectionDetails>>(){}.getType());

                            exam.examSectionDetails.clear();
                            exam.examSectionDetails.addAll(examSections);

                            SharedPreferences.Editor editor = pref.edit();

                            String runningExam = new Gson().toJson(exam);

//                            editor.putString(Gc.ONLINETESTRUNNING, runningExam);
//                            editor.apply();

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);

                            Intent intent = new Intent(OnlineExamDetail.this, StartOnlineExam.class);
                            intent.putExtra("exam", new Gson().toJson(examEntity));
                            startActivity(intent);
                            finish();

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(OnlineExamDetail.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    private class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder>{

        @NonNull
        @Override
        public ListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.online_exam_section, viewGroup, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListAdapter.MyViewHolder myViewHolder, int i) {
            myViewHolder.exam_section_name.setText(exam.examSectionDetails.get(i).sectionName);
            myViewHolder.exam_section_subject.setText(exam.examSectionDetails.get(i).SubjectName);
            myViewHolder.section_total_questions.setText(exam.examSectionDetails.get(i).numberOfQuestions);
            myViewHolder.section_maximum_marks.setText(exam.examSectionDetails.get(i).maxMarks);
        }

        @Override
        public int getItemCount() {
            return exam.examSectionDetails.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView exam_section_name,
                    exam_section_subject,
                    section_total_questions,
                    section_completed_questions,
                    section_maximum_marks;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                exam_section_name = itemView.findViewById(R.id.exam_section_name);
                exam_section_name.setTextColor(colorIs);

                exam_section_subject = itemView.findViewById(R.id.exam_section_subject);
                exam_section_subject.setTextColor(colorIs);

                section_total_questions = itemView.findViewById(R.id.section_total_questions);
                section_total_questions.setTextColor(colorIs);

                section_completed_questions = itemView.findViewById(R.id.section_completed_questions);
                section_completed_questions.setTextColor(colorIs);

                section_maximum_marks = itemView.findViewById(R.id.section_maximum_marks);
                section_maximum_marks.setTextColor(colorIs);
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
