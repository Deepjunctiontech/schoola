package in.junctiontech.school.schoolnew.staff;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.communicate.SendSMSActivity;
import in.junctiontech.school.schoolnew.reviewlog.ViewFeedback;
import in.junctiontech.school.staffattendance.StaffAttendanceReportActivity;

public class StaffActivity extends AppCompatActivity {

    MainDatabase mDb;

    FloatingActionButton fb_staff_add;
    RecyclerView mRecyclerView;

    StaffListAdapter adapter ;
    ArrayList<SchoolStaffEntity> staffEntities = new ArrayList<>();

    private ArrayList<SchoolStaffEntity> staffEntitiesfilter = new ArrayList<>();
    int colorIs;

    String action_requested;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_staff_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        fb_staff_add = findViewById(R.id.fb_staff_add);

        fb_staff_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Gc.getSharedPreference(Gc.STAFFSETUPALLOWED, StaffActivity.this).equalsIgnoreCase(Gc.TRUE))
                    startActivityForResult(new Intent(StaffActivity.this, StaffCreateActivity.class)
                        , Gc.STAFF_CREATE_REQUEST_CODE);
                else
                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                            findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);

            }
        });

        action_requested = Strings.nullToEmpty(getIntent().getStringExtra("action"));
        if (!("".equalsIgnoreCase(action_requested))){
            fb_staff_add.hide();
            Config.responseSnackBarHandler(getString(R.string.select_staff),
                    findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);
        }

        mRecyclerView = findViewById(R.id.rv_staff_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new StaffListAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4852836853", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolStaffModel().getAllStaff().observe(this, new Observer<List<SchoolStaffEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolStaffEntity> schoolStaffEntities) {
                staffEntities.clear();
                staffEntitiesfilter.clear();
                staffEntities.addAll(schoolStaffEntities);
                staffEntitiesfilter.addAll(schoolStaffEntities);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolStaffModel().getAllStaff().removeObservers(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterStudent(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterStudent(s);
                return true;
            }
        });
        return true;
    }

    void filterStudent(String text) {

        staffEntities.clear();
        if (text.isEmpty()) {
            staffEntities.addAll(staffEntitiesfilter);
        } else {
            text = text.toLowerCase();
            for (SchoolStaffEntity s : staffEntitiesfilter) {
                if (s.StaffName.toLowerCase().contains(text)
                        || s.UserType.toLowerCase().contains(text)
                        || s.StaffMobile.toLowerCase().contains(text)
                ) {
                    staffEntities.add(s);
                }
            }

        }
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Gc.STAFF_CREATE_REQUEST_CODE){
            if (resultCode == RESULT_OK) {
                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.ll_staff_activity),R.color.fragment_first_green);
            }
        }else

            super.onActivityResult(requestCode, resultCode, data);
    }

    void updateStaff(String staff){
        Intent intent = new Intent(this, StaffCreateActivity.class);
        intent.putExtra("staff", staff);
        startActivityForResult(intent, Gc.STAFF_CREATE_REQUEST_CODE);
    }

    public class StaffListAdapter extends RecyclerView.Adapter<StaffListAdapter.MyViewHolder> {


        @NonNull
        @Override
        public StaffListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff_list_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull StaffListAdapter.MyViewHolder holder, int position) {
            holder.tv_staff_name.setText(staffEntities.get(position).StaffName);
//            holder.tv_staff_name.setTextColor(colorIs);
            holder.tv_staff_userDesignation.setText(staffEntities.get(position).StaffPositionValue);


            if (!(Strings.nullToEmpty(staffEntities.get(position).staffProfileImage).equalsIgnoreCase("")))
                Glide.with(StaffActivity.this).load(staffEntities.get(position).staffProfileImage)
                        .apply(new RequestOptions()
                                .error(R.drawable.ic_single)
                                .placeholder(R.drawable.ic_single)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                        )
                        .thumbnail(0.5f)
                        .into(holder.staff_profile_image);
            else
                holder.staff_profile_image.setImageDrawable(getDrawable(R.drawable.ic_single));
        }

        @Override
        public int getItemCount() {
            return staffEntities.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_staff_name, tv_staff_userType,tv_staff_userDesignation;
            //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
            // Button btn_item_create_class_add_section;
            CircleImageView staff_profile_image;

            public MyViewHolder(final View itemView) {
                super(itemView);

                staff_profile_image   = itemView.findViewById(R.id.staff_profile_image);
                staff_profile_image.setVisibility(View.VISIBLE);
                tv_staff_name =  itemView.findViewById(R.id.tv_staff_name);
//            tv_staff_name.setTextColor(appColor);

//            tv_staff_userType = itemView.findViewById(R.id.tv_staff_userType);
//            tv_staff_userType.setVisibility(View.VISIBLE);

                tv_staff_userDesignation =  itemView.findViewById(R.id.tv_staff_userDesignation);
                tv_staff_userDesignation.setVisibility(View.VISIBLE);

                itemView.setOnClickListener(v -> {
                    AlertDialog.Builder choices = new AlertDialog.Builder(StaffActivity.this);
                    final String staff = new Gson().toJson(staffEntities.get(getAdapterPosition()));

                    String[] aa = new String[]
                            { getResources().getString(R.string.edit),
                                    getResources().getString(R.string.sms),
                                    getResources().getString(R.string.attendance_report)
                            };

                    choices.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {

                            switch (which){
                                case 0:
                                    if (Gc.getSharedPreference(Gc.STAFFSETUPALLOWED, StaffActivity.this).equalsIgnoreCase(Gc.TRUE))
                                        updateStaff(staff);
                                    else
                                        Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);
                                    break;
                                case 1:

                                    if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, StaffActivity.this).equalsIgnoreCase(Gc.TRUE))
                                        startActivity(new Intent(StaffActivity.this, SendSMSActivity.class)
                                                .putExtra("staff",staff));
                                    else
                                        Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);
                                    break;
                                case 2:
                                    if (Gc.getSharedPreference(Gc.STAFFSETUPALLOWED, StaffActivity.this).equalsIgnoreCase(Gc.TRUE))
                                        startActivity(new Intent(StaffActivity.this, StaffAttendanceReportActivity.class)
                                            .putExtra("staff",staff));
                                    else
                                        Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);
                                    break;
                            }

                        }
                    });
                    if ("".equalsIgnoreCase(action_requested))
                    choices.show();
                    else if (action_requested.equalsIgnoreCase("showAttendence")){
                        startActivity(new Intent(StaffActivity.this, StaffAttendanceReportActivity.class)
                                .putExtra("staff",staff));
                    }else if (action_requested.equalsIgnoreCase("sms")){
                        if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, StaffActivity.this).equalsIgnoreCase(Gc.TRUE))
                            startActivity(new Intent(StaffActivity.this, SendSMSActivity.class)
                                    .putExtra("staff",staff));
                        else
                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                    findViewById(R.id.ll_staff_activity),R.color.fragment_first_blue);
                    }else if("reviewLog".equalsIgnoreCase(action_requested)){
                        startActivity(new Intent(StaffActivity.this, ViewFeedback.class)
                                .putExtra("staff",staff));
                    }

                });

            }
        }
    }
}
