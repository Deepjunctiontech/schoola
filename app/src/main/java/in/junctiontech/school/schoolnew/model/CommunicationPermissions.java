package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by deep on 27/03/18.
 */
@Entity
public class CommunicationPermissions {
    @PrimaryKey(autoGenerate = true)
    public int autoid;
    public String FromUserType;
    public String ToUserType;

}
