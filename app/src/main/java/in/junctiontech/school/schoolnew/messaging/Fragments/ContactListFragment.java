package in.junctiontech.school.schoolnew.messaging.Fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class ContactListFragment extends
        Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private StaffListFragment staffListFragment;
    private ParentListFragment parentListFragment;
    private StudentListFragment studentListFragment;
    private int lastSelectedPager = 0;
    private boolean firstMenuVisibility;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_pager, container, false);
        viewPager =  convertView.findViewById(R.id.container_pager);
        setupViewPager(viewPager);


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        lastSelectedPager = 0;

                        getActivity().setTitle(getString(R.string.parent));
                        break;

                    case 1:
                        lastSelectedPager = 1;
                        getActivity().setTitle(getString(R.string.student) + " " +
                                getString(R.string.list));
                        break;

                    case 2:
                        lastSelectedPager = 2;
                        getActivity().setTitle(getString(R.string.staff));
                        break;
//
//                    case 3:
//                        getSupportActionBar().setTitle(getString(R.string.section) + " " +
//                                getString(R.string.list));
//                        break;
//
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout = (TabLayout) convertView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setAnimation(AnimationUtils.loadAnimation(getContext(),
                R.anim.animator_for_bounce));

        return convertView;
    }

    private void setupViewPager(ViewPager viewPager) {
        ContactListFragment.ViewPagerAdapter adapter = new ContactListFragment.
                ViewPagerAdapter(getActivity().getSupportFragmentManager());

        parentListFragment = new ParentListFragment();
        studentListFragment = new StudentListFragment();
        staffListFragment = new StaffListFragment();

        adapter.addFragment(parentListFragment, getString(R.string.parent));
        adapter.addFragment(studentListFragment, getString(R.string.student));
        adapter.addFragment(staffListFragment, getString(R.string.staff));

        viewPager.setAdapter(adapter);

    }

    public void setCustumMenuVisibility(boolean custumMenuVisibility) {
        switch (lastSelectedPager) {
            case 0:
                if (parentListFragment!=null)
                parentListFragment.setMenuVisibility(custumMenuVisibility);
                break;
            case 1:
                if (studentListFragment!=null)
                studentListFragment.setMenuVisibility(custumMenuVisibility);
                break;
            case 2:
                if (staffListFragment !=null)
                staffListFragment.setMenuVisibility(custumMenuVisibility);
                break;
        }

    }

    public void setFirstMenuVisibility(boolean firstMenuVisibility) {
        this.firstMenuVisibility = firstMenuVisibility;

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
