package `in`.junctiontech.school.schoolnew.trackIssue

import java.io.Serializable

class IssueLogsData:Serializable{
    var bugLogID:String=""
    var OrganizationKey:String=""
    var bugCode:String=""
    var description:String=""
    var developerUserID:String=""
    var clientEmail:String=""
    var responseUserID:String=""
    var status:String=""
    var communication:String=""
    var dateTime:String=""
    var mediaPath:String=""
}