package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolSubjectsDao {
    @Query("Select * from SchoolSubjectsEntity where Session LIKE :session")
    LiveData<List <SchoolSubjectsEntity>> getSchoolSubjects(String session);

    @Insert(onConflict = REPLACE)
    void insertSubject(List<SchoolSubjectsEntity> schoolSubjectsEntity);
}
