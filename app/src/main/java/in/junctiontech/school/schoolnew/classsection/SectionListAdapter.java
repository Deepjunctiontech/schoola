package in.junctiontech.school.schoolnew.classsection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.communicate.SendSMSActivity;

public class SectionListAdapter extends RecyclerView.Adapter<SectionListAdapter.MyViewHolder>{
    private ArrayList<SchoolClassSectionEntity> schoolSectionList;
    private HashMap<String,String> stCount;
    private Context context;
    int colorIs;

     SectionListAdapter(Context context, ArrayList<SchoolClassSectionEntity> sectionList,HashMap<String,String> stCount){
        this.schoolSectionList = sectionList;
        this.stCount = stCount;
        this.context = context;
         colorIs = Config.getAppColor((Activity) context, true);

     }

     public class MyViewHolder extends RecyclerView.ViewHolder {
         TextView tv_item_class_name, tv_designation;
         MyViewHolder(View itemView) {
            super(itemView);
             tv_item_class_name =  itemView.findViewById(R.id.tv_item_class_name);
             tv_designation = itemView.findViewById(R.id.tv_designation);

             itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     AlertDialog.Builder choices = new AlertDialog.Builder(context);
                     String[] aa = new String[]
                             {
                                     context.getResources().getString(R.string.send_sms),
                                     context.getResources().getString(R.string.rename),
                                     context.getResources().getString(R.string.delete)

                             };
                     choices.setItems(aa, new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialogInterface, int i) {
                             switch (i){
                                 case 0:
                                     if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, context).equalsIgnoreCase(Gc.TRUE))
                                         context.startActivity(new Intent(context, SendSMSActivity.class)
                                                 .putExtra("section",schoolSectionList.get(getAdapterPosition()).SectionId));
                                     else
                                         Config.responseSnackBarHandler(context.getString(R.string.permission_denied),
                                                 ((SectionActivity) context).findViewById(R.id.classlayout),
                                                 R.color.fragment_first_blue);
                                     break;

                                     case 1:
                                     if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, context).equalsIgnoreCase(Gc.TRUE))
                                         updateSection(context,
                                                 schoolSectionList.get(getAdapterPosition()).SectionName,
                                                 schoolSectionList.get(getAdapterPosition()).SectionId,
                                                 schoolSectionList.get(getAdapterPosition()).ClassId);
                                     else
                                         Config.responseSnackBarHandler(context.getString(R.string.permission_denied),
                                                 ((SectionActivity) context).findViewById(R.id.classlayout),
                                                 R.color.fragment_first_blue);
                                     break;
                                     case 2:
                                     if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, context).equalsIgnoreCase(Gc.TRUE))
                                         DeleteSection(context,
                                                 schoolSectionList.get(getAdapterPosition()).SectionId);
                                     else
                                         Config.responseSnackBarHandler(context.getString(R.string.permission_denied),
                                                 ((SectionActivity) context).findViewById(R.id.classlayout),
                                                 R.color.fragment_first_blue);
                                     break;
                             }
                         }
                     });
                     choices.show();
                 }
             });
        }
    }

    private void DeleteSection(Context context,  String sectionId) {

        if (context instanceof SectionActivity){

            AlertDialog.Builder alert1 = new AlertDialog.Builder(context);
            alert1.setTitle("Are You Sure, Want To Delete This Record ?");
            alert1.setPositiveButton(context.getString(R.string.ok), (dialog, which) -> {
                ((SectionActivity)context).deleteSection(sectionId);
            });
            alert1.setNegativeButton(context.getString(R.string.cancel), (dialog, which) -> {

            });

            alert1.show();

        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(v);
     }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_item_class_name.setText(schoolSectionList.get(position).SectionName);
        holder.tv_item_class_name.setTextColor(colorIs);
        holder.tv_designation.setText((stCount.get(
                schoolSectionList.get(position).SectionId)== null)
                ? "0"
                : (stCount.get(schoolSectionList.get(position).SectionId)));

    }


    @Override
    public int getItemCount() {
        return schoolSectionList.size();
        }

        private void updateSection(final Context context , String selectedSection, final String sectionid, final String sectionclassid){
            if (context instanceof SectionActivity){

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_update_class);
                dialog.setTitle(R.string.update_section);

                final EditText editText =  dialog.findViewById(R.id.et_class_name);
                editText.setText(selectedSection);

                Button btnSave          =  dialog.findViewById(R.id.save);
                btnSave.setBackgroundColor(colorIs);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            ((SectionActivity)context).updateSection(editText.getText().toString(),sectionid,sectionclassid);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Button btnCancel        =  dialog.findViewById(R.id.cancel);
                btnCancel.setBackgroundColor(colorIs);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }


        }
}
