package in.junctiontech.school.schoolnew.exam;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ExamTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class ExamTypeActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    FloatingActionButton fb_exam_type;
    RecyclerView mRecyclerView;

    final String EXAMTYPE = "Exam_Type";

    ArrayList<ExamTypeEntity> examTypeList = new ArrayList<>();

    ExamTypeAdapter adapter;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_exam_type.setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_type);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        fb_exam_type = findViewById(R.id.fb_exam_type);

        mRecyclerView = findViewById(R.id.rv_exam_type);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ExamTypeAdapter();
        mRecyclerView.setAdapter(adapter);

        fb_exam_type.setOnClickListener(view -> showDialogForExamTypeCreate());

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3900855448",this,adContainer);
        }
    }

    void showDialogForExamTypeCreate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.exam_type);

        LayoutInflater inflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_exam_type = view.findViewById(R.id.et_subject_name);
        et_exam_type.setHint(R.string.exam_type);
        et_exam_type.setFilters(new InputFilter[] { new InputFilter.LengthFilter(25) });

        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_exam_type.getText().toString().trim().isEmpty()){
                et_exam_type.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {
                createExamType(et_exam_type.getText().toString().trim().toUpperCase());
                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }

    void showDialogForExamTypeUpdate(ExamTypeEntity examType){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.exam_type);

        LayoutInflater inflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_exam_type = view.findViewById(R.id.et_subject_name);
        et_exam_type.setHint(R.string.exam_type);
        et_exam_type.setFilters(new InputFilter[] { new InputFilter.LengthFilter(25) });
        et_exam_type.setText(examType.Exam_Type);

        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_exam_type.getText().toString().trim().isEmpty()){
                et_exam_type.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {

                    updateExamType(et_exam_type.getText().toString().trim().toUpperCase(), examType);

                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }


    void createExamType(final String examtype){
        progressBar.setVisibility(View.VISIBLE);

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();

        try {
            p.put(EXAMTYPE, examtype);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

        param.put(p);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/examTypes"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<ExamTypeEntity> examTypeEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("examTypes"),new TypeToken<List<ExamTypeEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.examTypeModel().insertExamTypes(examTypeEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ExamTypeActivity.this);

                    Intent intent1 = new Intent(ExamTypeActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ExamTypeActivity.this);

                    Intent intent2 = new Intent(ExamTypeActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ExamTypeActivity.this, error, findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                return param.toString().getBytes(StandardCharsets.UTF_8);
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void updateExamType(final String examType , ExamTypeEntity examEntity) {
        progressBar.setVisibility(View.VISIBLE);

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();
        JSONObject urlparam = new JSONObject();
        try {
            p.put(EXAMTYPE, examType);
            urlparam.put("ExamTypeID",examEntity.ExamTypeID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

        param.put(p);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/examTypes/"
                + urlparam
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:

                    try {
                        ArrayList<ExamTypeEntity> examTypeEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("examTypes"),new TypeToken<List<ExamTypeEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.examTypeModel().insertExamTypes(examTypeEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ExamTypeActivity.this);

                    Intent intent1 = new Intent(ExamTypeActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ExamTypeActivity.this);

                    Intent intent2 = new Intent(ExamTypeActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ExamTypeActivity.this, error, findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                return param.toString().getBytes(StandardCharsets.UTF_8);
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mDb.examTypeModel().getExamTypes().observe(this, examTypeEntities -> {
            examTypeList.clear();
            examTypeList.addAll(examTypeEntities);
            adapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.examTypeModel().getExamTypes().removeObservers(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class ExamTypeAdapter extends RecyclerView.Adapter<ExamTypeAdapter.MyViewHolder>{

        @NonNull
        @Override
        public ExamTypeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ExamTypeAdapter.MyViewHolder holder, int position) {
            holder.tv_single_item.setText(examTypeList.get(position).Exam_Type);
        }

        @Override
        public int getItemCount() {
            return examTypeList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);

                itemView.setOnClickListener(v -> {
                    showDialogForExamTypeUpdate(examTypeList.get(getAdapterPosition()));
                });
            }
        }
    }

}
