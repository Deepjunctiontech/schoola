package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolClassDao {
    @Query("Select * from SchoolClassEntity where Session LIKE :session order by ClassName")
    List<SchoolClassEntity> getSchoolClasses(String session);

    @Query("Select * from SchoolClassEntity where Session LIKE :session order by ClassName")
    LiveData<List<SchoolClassEntity>> getSchoolClassesLive(String session);

    @Query("Select ClassId from SchoolClassEntity where ClassId LIKE :session and ClassName LIKE :classname limit 1")
    String getClassId(String session, String classname);

    @Query("delete from SchoolClassEntity where ClassId = :classId")
    void deleteClass(String classId);

    @Query("delete from SchoolClassEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertSchoolclass(SchoolClassEntity schoolClassEntity);

    @Update
    void updateSchoolClass(SchoolClassEntity schoolClassEntity);

    @Insert(onConflict = REPLACE)
    void insertSchoolClasses(List<SchoolClassEntity> schoolClassEntities);
}
