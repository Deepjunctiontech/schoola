package in.junctiontech.school.schoolnew.subject;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class SubjectActivity extends AppCompatActivity {

    private FloatingActionButton fb_subject_add;

    private ProgressDialog progressBar;

    private ArrayList<SchoolSubjectsEntity> schoolSubjects = new ArrayList<>();
    private ArrayList<SchoolSubjectsEntity> subjectFilterList = new ArrayList<>();
    private ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    String[] sectionList = new String[]{""};

    String[] permissions;

    SchoolSubjectsEntity subjectUpdate;

    MainDatabase mDb;

    private RecyclerView mRecyclerView;
    private SubjectAdapter adapter;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_subject_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_subject);
        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);


        fb_subject_add = findViewById(R.id.fb_subject_add);

        fb_subject_add
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i("Floating button clicked", "in subject");
                        showDialogForSubjectCreate();
                    }
                });
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);

        mRecyclerView = findViewById(R.id.rv_subject_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
//        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new SubjectAdapter();
        mRecyclerView.setAdapter(adapter);

        if (Gc.getSharedPreference(Gc.SUBJECTCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            permissions = new String[]{
                    getResources().getString(R.string.subject_for_classes),
                    getResources().getString(R.string.rename)
            };
            fb_subject_add.show();
        } else if (Gc.getSharedPreference(Gc.SUBJECTDISPLAYALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            permissions = new String[]{
                    getString(R.string.subject_for_classes)
            };

        }

        if (Strings.nullToEmpty(getIntent().getStringExtra(Gc.SETUP)).equalsIgnoreCase(Gc.SETUP)) {
            fb_subject_add.show();

        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/9303217758", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolSubjectsModel()
                .getSchoolSubjects(Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, new Observer<List<SchoolSubjectsEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<SchoolSubjectsEntity> schoolSubjectsEntities) {
                        schoolSubjects.clear();
                        subjectFilterList.clear();
//                if(schoolSubjectsEntities.size()>0){
//                    HashMap<String, String> setDefaults = new HashMap<>();
//                    setDefaults.put(Gc.SUBJECTEXISTS, Gc.EXISTS);
//                    Gc.setSharedPreference(setDefaults, SubjectActivity.this);
//                }else{
//                    HashMap<String, String> setDefaults = new HashMap<>();
//                    setDefaults.put(Gc.SUBJECTEXISTS, Gc.NOTFOUND);
//                    Gc.setSharedPreference(setDefaults, SubjectActivity.this);
//                }

                        for (int i = 0; i < schoolSubjectsEntities.size(); i++) {
                            schoolSubjects.add(schoolSubjectsEntities.get(i));
                        }
                        subjectFilterList.addAll(schoolSubjectsEntities);
                        adapter.notifyDataSetChanged();
                    }
                });

        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).observe(this, new Observer<List<ClassNameSectionName>>() {
            @Override
            public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                classNameSectionList.clear();
                classNameSectionList.addAll(classNameSectionNames);
                sectionList = new String[classNameSectionList.size()];

                for (int i = 0; i < classNameSectionList.size(); i++) {
                    sectionList[i] = (classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName);
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolSubjectsModel()
                .getSchoolSubjects(Gc.getSharedPreference(Gc.APPSESSION, this))
                .removeObservers(this);

        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).removeObservers(this);
    }

    void showDialogForSubjectCreate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.create_subject);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_subject_name = view.findViewById(R.id.et_subject_name);
        et_subject_name.setHint(R.string.subject_name);
        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called", "");
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if (et_subject_name.getText().toString().trim().isEmpty()) {
                    et_subject_name.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else if (et_subject_abbr.getText().toString().trim().isEmpty()) {
                    et_subject_abbr.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else {

                    Log.i("Subject name", et_subject_name.getText().toString());
                    Log.i("Abbreviation", et_subject_abbr.getText().toString());

                    createSubject(et_subject_name.getText().toString().trim(),
                            et_subject_abbr.getText().toString().trim());
                    wantToCloseDialog = true;
                }
                if (wantToCloseDialog)
                    dialog.dismiss();
            }
        });
    }

    void showDialogForSubjectRename(SchoolSubjectsEntity subjectsEntity) {
        subjectUpdate = subjectsEntity;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.update_subject);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_subject_name = view.findViewById(R.id.et_subject_name);
        et_subject_name.setText(subjectsEntity.SubjectName);

        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setText(subjectsEntity.SubjectAbb);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called", "");
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if (et_subject_name.getText().toString().trim().isEmpty()) {
                    et_subject_name.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else if (et_subject_abbr.getText().toString().trim().isEmpty()) {
                    et_subject_abbr.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else {

                    Log.i("Subject name", et_subject_name.getText().toString());
                    Log.i("Abbreviation", et_subject_abbr.getText().toString());

                    subjectUpdate.SubjectName = et_subject_name.getText().toString().trim();
                    subjectUpdate.SubjectAbb = et_subject_abbr.getText().toString().trim();
                    try {
                        updateSubject(subjectUpdate);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    wantToCloseDialog = true;
                }
                if (wantToCloseDialog)
                    dialog.dismiss();
            }
        });
    }

    void createSubject(String subjectName, String subjectAbbr) {
        progressBar.show();

        final JSONObject param = new JSONObject();
        try {
            param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
            param.put("SubjectName", subjectName);
            param.put("SubjectAbb", subjectAbbr);
            param.put("Class", "");
            param.put("SubjectStatus", Gc.ACTIVE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "subject/subjects";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                Log.i("Subject ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray subjectsjarr = resultjobj.getJSONArray("subjects");
                            saveSchoolSubjectsToDataBase(subjectsjarr);

                            JSONArray subjectClassjarr = resultjobj.getJSONArray("subjectSectionMapping");
                            if (subjectClassjarr.length() > 0) {
                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.SUBJECTCLASSEXISTS, Gc.EXISTS);
                                Gc.setSharedPreference(setDefaults, SubjectActivity.this);

                                final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClassEntities =
                                        new Gson().fromJson(subjectClassjarr.toString(), new TypeToken<List<SchoolSubjectsClassEntity>>() {
                                        }
                                                .getType());

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolSubjectsClassModel().insertSubJectClassMapping(schoolSubjectsClassEntities);
                                    }
                                }).start();
                            }

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.ll_subject_activity), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, SubjectActivity.this);

                        Intent intent1 = new Intent(SubjectActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, SubjectActivity.this);

                        Intent intent2 = new Intent(SubjectActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_subject_activity), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
                Config.responseVolleyErrorHandler(SubjectActivity.this, error,
                        findViewById(R.id.ll_subject_activity));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void updateSubject(SchoolSubjectsEntity subjectsEntity) throws JSONException {

        progressBar.show();

        final JSONObject param = new JSONObject();
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("SubjectName", subjectsEntity.SubjectName);
        param.put("SubjectAbb", subjectsEntity.SubjectAbb);
        param.put("SubjectStatus", Gc.ACTIVE);


        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "subject/subjects/"
                + new JSONObject().put("SubjectId", subjectsEntity.SubjectId);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray subjectsjarr = resultjobj.getJSONArray("subjects");
                            saveSchoolSubjectsToDataBase(subjectsjarr);

                            JSONArray subjectClassjarr = resultjobj.getJSONArray("subjectSectionMapping");
                            if (subjectClassjarr.length() > 0) {
                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.SUBJECTCLASSEXISTS, Gc.EXISTS);
                                Gc.setSharedPreference(setDefaults, SubjectActivity.this);

                                final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClassEntities =
                                        new Gson().fromJson(subjectClassjarr.toString(), new TypeToken<List<SchoolSubjectsClassEntity>>() {
                                        }
                                                .getType());

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolSubjectsClassModel().insertSubJectClassMapping(schoolSubjectsClassEntities);
                                    }
                                }).start();
                            }

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.ll_subject_activity), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, SubjectActivity.this);

                        Intent intent1 = new Intent(SubjectActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, SubjectActivity.this);

                        Intent intent2 = new Intent(SubjectActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_subject_activity), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
                Config.responseVolleyErrorHandler(SubjectActivity.this, error,
                        findViewById(R.id.ll_subject_activity));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void saveSchoolSubjectsToDataBase(JSONArray subjectsjarr) {
        if (subjectsjarr.length() > 0) {
            HashMap<String, String> setDefaults = new HashMap<>();
            setDefaults.put(Gc.SUBJECTEXISTS, Gc.EXISTS);
            Gc.setSharedPreference(setDefaults, this);

            try {
                final ArrayList<SchoolSubjectsEntity> schoolSubjectsEntities;
                final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClassEntities;
                schoolSubjectsEntities = MapModelToEntity.mapSubjectModelToEntity(subjectsjarr);
//                schoolSubjectsClassEntities = MapModelToEntity.mapSubjectClassesModelToEntity(subjectsjarr);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mDb.schoolSubjectsModel().insertSubject(schoolSubjectsEntities);
                    }
                }).start();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void assignSubjectToClass(final String subjectid) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.subject_for_classes);

        final List<String> sectionids = mDb.schoolSubjectsClassModel().getClassForSubject(subjectid);

        boolean[] checkedItems = new boolean[classNameSectionList.size()];

        for (int i = 0; i < classNameSectionList.size(); i++) {
            if (sectionids.contains(classNameSectionList.get(i).SectionId)) {
                checkedItems[i] = true;
            } else {
                checkedItems[i] = false;
            }
        }

        builder.setMultiChoiceItems(sectionList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                if (b) {
                    sectionids.add(classNameSectionList.get(i).SectionId);
                } else if (sectionids.contains(classNameSectionList.get(i).SectionId)) {
                    sectionids.remove(classNameSectionList.get(i).SectionId);
                }
            }
        });

        if (Gc.getSharedPreference(Gc.SUBJECTCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {

            builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        updateClassAndSubjectAssign(sectionids, subjectid);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateClassAndSubjectAssign(List<String> sectionids, final String subjectid) throws JSONException {

        final JSONObject param = new JSONObject();
        String s = new String();
        for (int i = 0; i < sectionids.size(); i++) {
            s += sectionids.get(i) + ",";
        }
        if (!(("").equals(s))) {
            s = s.substring(0, s.length() - 1);
        }

        param.put("Class", s);
//        p.put("Frequency", "Monthly");
        JSONObject p = new JSONObject();
        p.put("SubjectId", subjectid);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "subject/subjects/" + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                Log.i("Subject class ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try {
                            JSONArray subjectClassjarr = job.getJSONObject("result").getJSONArray("subjectSectionMapping");

                            if (subjectClassjarr.length() > 0) {
                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.SUBJECTCLASSEXISTS, Gc.EXISTS);
                                Gc.setSharedPreference(setDefaults, SubjectActivity.this);


                                final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClassEntities =
                                        new Gson().fromJson(subjectClassjarr.toString(), new TypeToken<List<SchoolSubjectsClassEntity>>() {
                                        }.getType());


                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolSubjectsClassModel().deleteAll(subjectid);

                                        mDb.schoolSubjectsClassModel().insertSubJectClassMapping(schoolSubjectsClassEntities);
                                    }
                                }).start();
                            } else {


                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolSubjectsClassModel().deleteAll(subjectid);
                                    }
                                }).start();

                            }
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.ll_subject_activity), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case "500":

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, SubjectActivity.this);
                        finish();


                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_subject_activity), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
                Config.responseVolleyErrorHandler(SubjectActivity.this, error, findViewById(R.id.ll_subject_activity));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void renameSubject(SchoolSubjectsEntity subjectsEntity) throws JSONException {

        showDialogForSubjectRename(subjectsEntity);
//        ArrayList<String> sectionids = new ArrayList<>();
//        updateClassAndSubjectAssign(sectionids,subjectid);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if (Gc.getSharedPreference(Gc.SUBJECTEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            menu.findItem(R.id.menu_next).setVisible(false);
        }

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterSubject(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterSubject(s);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :"
                        +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!(Gc.getSharedPreference(Gc.SUBJECTEXISTS, this).equals(Gc.EXISTS))) {
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.ll_subject_activity), R.string.subjects_not_available,
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_blue));
            snackbarObj.show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void filterSubject(String text) {
        schoolSubjects.clear();
        if (text.isEmpty()) {
            schoolSubjects.addAll(subjectFilterList);
        } else {
            text = text.toLowerCase();
            for (SchoolSubjectsEntity s : subjectFilterList) {
                if (s.SubjectName.toLowerCase().contains(text)
                        || Strings.nullToEmpty(s.SubjectAbb).toLowerCase().contains(text)
                ) {
                    schoolSubjects.add(s);
                }
            }

        }
        adapter.notifyDataSetChanged();
    }

    public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {

        @NonNull
        @Override
        public SubjectAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list_recycler, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SubjectAdapter.MyViewHolder holder, int position) {
            holder.tv_subject_name.setText(schoolSubjects.get(position).SubjectName);
            holder.tv_subject_name.setTextColor(colorIs);
            holder.tv_subject_abbr.setText(schoolSubjects.get(position).SubjectAbb);
            holder.tv_subject_id.setText(schoolSubjects.get(position).SubjectId);
        }

        @Override
        public int getItemCount() {
            return schoolSubjects.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_subject_name, tv_subject_abbr, tv_subject_id;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_subject_name = itemView.findViewById(R.id.tv_subject_name);
                tv_subject_abbr = itemView.findViewById(R.id.tv_subject_abbr);
                tv_subject_id = itemView.findViewById(R.id.tv_subject_id);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder choices = new AlertDialog.Builder(SubjectActivity.this);


                        choices.setItems(permissions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which) {
                                    case 0:
                                        assignSubjectToClass(schoolSubjects.get(getAdapterPosition()).SubjectId);
                                        break;

                                    case 1:
                                        try {
                                            renameSubject(schoolSubjects.get(getAdapterPosition()));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                }
                            }
                        });
                        choices.show();
                    }
                });
            }
        }
    }
}
