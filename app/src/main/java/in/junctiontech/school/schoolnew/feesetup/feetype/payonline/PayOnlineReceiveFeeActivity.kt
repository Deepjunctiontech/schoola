package `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.atom.mpsdklibrary.PayActivity
import com.google.common.base.Strings
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
//import com.paytm.pgsdk.PaytmOrder
//import com.paytm.pgsdk.PaytmPGService
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class PayOnlineReceiveFeeActivity : AppCompatActivity() {
    private lateinit var upToMonth: String
    lateinit var progressBar: ProgressBar
    lateinit var selectedStudent: String
    internal var studentEntity: SchoolStudentEntity? = null
    lateinit var adapter: PayOnlineReceiveFeeActivityAdapter
    private lateinit var tvPayOnlineDueFeesTotal: TextView
    private lateinit var tvPayOnlineSessionTotal: TextView
    private lateinit var tvPayOnlineQuarterlyTotal: TextView
    private lateinit var tvPayOnlineMonthlyTotal: TextView
    private lateinit var etStudentFirstName: EditText
    private lateinit var etStudentLastName: EditText
    private lateinit var etPayOnlineStudentEmail: EditText
    private lateinit var etStudentMobileNumber: EditText
    private lateinit var btnPayNow: Button

    private val feeDueList = ArrayList<FeeDues>()

    internal var filledFeeAmount = ArrayList<String>()
    internal var OnlineFeePartialPayment = "allowed"
    lateinit var mRecyclerView: RecyclerView

    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fee_receive_online_pay)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initializeViews()

        val studentString = intent.getStringExtra("student")
        if (studentString == null){
            finish()
            return
        }
        selectedStudent = studentString


        val upToMonthString = intent.getStringExtra("upToMonth")
        if (upToMonthString == null){
            finish()
            return
        }
        upToMonth = upToMonthString



        if (upToMonth.isNotEmpty() && !"".equals(Strings.nullToEmpty(selectedStudent), ignoreCase = true)) {
            studentEntity = Gson().fromJson(selectedStudent, SchoolStudentEntity::class.java)
            getFeeDueForStudent()
            if (studentEntity!!.StudentName.isNotEmpty()) {
                val studentName = studentEntity!!.StudentName.split(" ")

                if (studentName[0].isNotEmpty()) {
                    etStudentFirstName.setText(studentName[0])
                }

                if (studentName.size > 1 && studentName[1].isNotEmpty()) {
                    etStudentLastName.setText(studentName[1])
                }
            }

            etPayOnlineStudentEmail.setText(studentEntity!!.StudentEmail)

            if (studentEntity?.Mobile != null) {
                etStudentMobileNumber.setText(studentEntity!!.Mobile.replace("-", ""))
            }

        }
        setColorApp()
        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/8838098769", this, adContainer)
        }
    }

    private fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)

        tvPayOnlineDueFeesTotal = findViewById(R.id.tv_pay_online_due_fees_total)
        tvPayOnlineSessionTotal = findViewById(R.id.tv_pay_online_session_total)
        tvPayOnlineQuarterlyTotal = findViewById(R.id.tv_pay_online_quarterly_total)
        tvPayOnlineMonthlyTotal = findViewById(R.id.tv_pay_online_monthly_total)

        etStudentFirstName = findViewById(R.id.et_student_first_name)
        etStudentLastName = findViewById(R.id.et_student_last_name)
        etPayOnlineStudentEmail = findViewById(R.id.et_pay_online_student_email)
        etStudentMobileNumber = findViewById(R.id.et_student_mobile_number)

        /*mDb!!.signedInStudentInformationModel().signedInStudentInformation
                .observe(this, Observer { studentInformation ->

                    signedInStudent = studentInformation
                    student = signedInStudent!!.student

                    val studentName = signedInStudent!!.StudentName.split(" ")

                    if (studentName[0].isNotEmpty()) {
                        etStudentFirstName.setText(studentName[0])
                    }

                    if (studentName.size > 1 && studentName[1].isNotEmpty()) {
                        etStudentLastName.setText(studentName[1])
                    }

                    etPayOnlineStudentEmail.setText(signedInStudent!!.StudentEmail)
                    etStudentMobileNumber.setText(signedInStudent!!.Mobile.replace("-", ""))


                    mDb!!.signedInStudentInformationModel().signedInStudentInformation.removeObservers(this@PayOnlineReceiveFeeActivity)
                })*/

        btnPayNow = findViewById(R.id.btn_pay_now)

        btnPayNow.setOnClickListener {
            if (validateInput()) {
                getPaymentGatewayInfo()
            }
        }

        mRecyclerView = findViewById(R.id.rv_pay_online_fee_list)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = PayOnlineReceiveFeeActivityAdapter()
        mRecyclerView.adapter = adapter

    }

    internal fun validateInput(): Boolean {
        if (etStudentFirstName.text.isEmpty()) {
            Config.responseSnackBarHandler(getString(R.string.student_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            etStudentFirstName.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if (etStudentLastName.text.isEmpty()) {
            Config.responseSnackBarHandler(getString(R.string.student_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            etStudentLastName.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if (etPayOnlineStudentEmail.text.isEmpty()) {
            Config.responseSnackBarHandler(getString(R.string.email_id) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            etPayOnlineStudentEmail.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if (etStudentMobileNumber.text.isEmpty()) {
            Config.responseSnackBarHandler(getString(R.string.mobile_number) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            etStudentMobileNumber.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if (feeDueList.size == 0) {
            Config.responseSnackBarHandler(getString(R.string.fees_not_available),
                    findViewById(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            return false
        }
        return true
    }

    private fun getPaymentGatewayInfo() {
        progressBar.visibility = View.VISIBLE

        val p = JSONArray()
        val param = JSONObject()

        param.put("AdmissionId", Gc.getSharedPreference(Gc.STUDENTADMISSIONID, applicationContext))
        param.put("monthYear", upToMonth)
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this))
        param.put("name", etStudentFirstName.text.toString())
        param.put("lastName", etStudentLastName.text.toString())
        param.put("email", etPayOnlineStudentEmail.text.toString())
        param.put("mobile", etStudentMobileNumber.text.toString())
        param.put("surl", "xyz")
        param.put("furl", "xyz")
        param.put("curl", "xyz")

        val size = feeDueList.size

        for (i in 0 until size) {
            p.put(JSONObject()
                    .put("FeeId", feeDueList[i].FeeId)
                    .put("MonthYear", feeDueList[i].MonthYear)
                    .put("AmountBalance", feeDueList[i].AmountBalance)
                    .put("AmountPay", filledFeeAmount[i]))
        }

        param.put("feesPay", p)

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/payOnline")

        val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE

            when (code) {
                "200" -> try {
                    val pg = job.getJSONObject("result").optString("pg")
//                    if (pg == "PAYTM") {
//                        val alert1 = AlertDialog.Builder(this)
//                        alert1.setMessage(HtmlCompat.fromHtml("Amount To Pay : ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("TXN_AMOUNT")} "
//                                + "<br><br>Name: ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("namelabel")}"
//                                + "<br><br>Email: ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("emaillabel")}" +
//                                "<br><br>Mobile : ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("phonelabel")}", HtmlCompat.FROM_HTML_MODE_LEGACY))
//                        alert1.setTitle(getString(R.string.information))
//                        alert1.setIcon(R.drawable.ic_clickhere)
//                        alert1.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
//                        alert1.setPositiveButton(getString(R.string.confirm)) { _, _ -> paytmPaymentGateway(job.getJSONObject("result").getJSONObject("pendingTransaction"), job.getJSONObject("result").optString("server").toString()) }
//                        alert1.setCancelable(false)
//                        alert1.show()
//                    } else

                    if (pg == "ATOM") {
                        val alert1 = AlertDialog.Builder(this)
                        alert1.setMessage(HtmlCompat.fromHtml("Amount To Pay : ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("amountlabel")} "
                                + "<br><br>Name: ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("namelabel")}"
                                + "<br><br>Email: ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("emaillabel")}" +
                                "<br><br>Mobile : ${job.getJSONObject("result").getJSONObject("pendingTransaction").optString("phonelabel")}", HtmlCompat.FROM_HTML_MODE_LEGACY))
                        alert1.setTitle(getString(R.string.information))
                        alert1.setIcon(R.drawable.ic_clickhere)
                        alert1.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
                        alert1.setPositiveButton(getString(R.string.confirm)) { _, _ -> atomPaymentGateway(job.getJSONObject("result").getJSONObject("pendingTransaction"), job.getJSONObject("result").optString("server").toString()) }
                        alert1.setCancelable(false)
                        alert1.show()
                    } else {
                        Config.responseSnackBarHandler("Invalid Payment Gateway Info",
                                findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@PayOnlineReceiveFeeActivity, error, findViewById<View>(R.id.activity_fee_receive_online_pay))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }

            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun atomPaymentGateway(jsonObject: JSONObject, server: String) {

        val tempamt = jsonObject.optString("amt").toDouble()
        val amt = tempamt.toString()

        val newPayIntent = Intent(this, PayActivity::class.java)
        newPayIntent.putExtra("merchantId", jsonObject.optString("login"))
        // txnscamt Fixed. Must be 0
        newPayIntent.putExtra("txnscamt", jsonObject.optString("txnscamt"))
        newPayIntent.putExtra("loginid", jsonObject.optString("login"))
        newPayIntent.putExtra("password", jsonObject.optString("pass"))
        newPayIntent.putExtra("prodid", jsonObject.optString("prodid"))
        // txncurr Fixed. Must be �INR�
        newPayIntent.putExtra("txncurr", jsonObject.optString("txncurr"))
        newPayIntent.putExtra("clientcode", "007")
        newPayIntent.putExtra("custacc", jsonObject.optString("custacc"))
        newPayIntent.putExtra("channelid", "INT")
        // amt  Should be 2 decimal number i.e 1.00
        newPayIntent.putExtra("amt", amt)
        newPayIntent.putExtra("txnid", jsonObject.optString("txnid"))
        //Date Should be in same format
        newPayIntent.putExtra("date", jsonObject.optString("date"))
        newPayIntent.putExtra("signature_request", jsonObject.optString("reqhashkey"))
        newPayIntent.putExtra("signature_response", jsonObject.optString("resphashkey"))
        newPayIntent.putExtra("discriminator", "All")

        if ("production" == server) {
            newPayIntent.putExtra("isLive", true)
        } else {
            newPayIntent.putExtra("isLive", false)
        }
        //Optinal Parameters
        //Only for Name
        newPayIntent.putExtra("customerName", jsonObject.optString("namelabel"))
        //Only for Email ID
        newPayIntent.putExtra("customerEmailID", jsonObject.optString("emaillabel"))
        //Only for Mobile Number
        newPayIntent.putExtra("customerMobileNo", jsonObject.optString("phonelabel"))
        newPayIntent.putExtra("udf1", jsonObject.optString("udf1"))
        newPayIntent.putExtra("udf2", jsonObject.optString("udf2"))

        newPayIntent.putExtra("udf3", jsonObject.optString("udf3"))

        val udf9 = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext) + "," +
                Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext) + "," +
                Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext) + "," +
                Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this) + Gc.ERPAPIVERSION

        newPayIntent.putExtra("udf9", udf9)
        //Only for Address
        //  newPayIntent.putExtra("billingAddress", "Pune");

        startActivityForResult(newPayIntent, 2866)

    }

    private fun paytmPaymentGateway(jsonObject: JSONObject, server: String) {
//        val service: PaytmPGService = if ("production" == server) {
//            PaytmPGService.getProductionService()
//        } else {
//            PaytmPGService.getStagingService()
//        }
//        val paramMap = HashMap<String, String>()
//
//        paramMap["MID"] = jsonObject.optString("MID")
//        paramMap["ORDER_ID"] = jsonObject.optString("ORDER_ID")
//        paramMap["CUST_ID"] = jsonObject.optString("CUST_ID")
//        paramMap["MOBILE_NO"] = jsonObject.optString("MOBILE_NO")
//        paramMap["EMAIL"] = jsonObject.optString("EMAIL")
//        paramMap["CHANNEL_ID"] = jsonObject.optString("CHANNEL_ID")
//        paramMap["TXN_AMOUNT"] = jsonObject.optString("TXN_AMOUNT")
//        paramMap["WEBSITE"] = jsonObject.optString("WEBSITE")
//        paramMap["INDUSTRY_TYPE_ID"] = jsonObject.optString("INDUSTRY_TYPE_ID")
//        paramMap["CALLBACK_URL"] = jsonObject.optString("CALLBACK_URL")
//        paramMap["CHECKSUMHASH"] = jsonObject.optString("CHECKSUMHASH")
//        val order = PaytmOrder(paramMap)
//        service.initialize(order, null)
//        service.startPaymentTransaction(this, true, true, object : PaytmPaymentTransactionCallback {
//            /*Call Backs*/
//            override fun someUIErrorOccurred(inErrorMessage: String) {
//                /*Display the error message as below */
//                //   Log.e("someUIErrorOccurred", "Payment Transaction response $inErrorMessage")
//                Toast.makeText(applicationContext, "UI Error $inErrorMessage", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onTransactionResponse(inResponse: Bundle) {
//                paytmResponse(inResponse)
//                /*Display the message as below */
//                //  Log.e("onTransactionResponse", "Payment Transaction response $inResponse")
//            }
//
//            override fun networkNotAvailable() {
//                Toast.makeText(applicationContext, "Network connection error: Check your internet connectivity", Toast.LENGTH_LONG).show()
//            }
//
//            override fun clientAuthenticationFailed(inErrorMessage: String) {
//                //     Log.e("AuthenticationFailed", "Payment Transaction response $inErrorMessage")
//                /*Display the message as below */
//                Toast.makeText(applicationContext, "Authentication failed: Server error$inErrorMessage", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onErrorLoadingWebPage(iniErrorCode: Int, inErrorMessage: String, inFailingUrl: String) {
//                //   Log.e("onErrorLoadingWebPage", "Payment Transaction response $inFailingUrl")
//                /*Display the message as below */
//                Toast.makeText(applicationContext, "Unable to load webpage $inFailingUrl", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onBackPressedCancelTransaction() {
//                // Log.e("CancelTransaction", "Payment Transaction cancelled")
//                /*Display the message as below */
//                Toast.makeText(applicationContext, "Transaction cancelled", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle) {}
//        })
    }

    private fun paytmResponse(inResponse: Bundle) {
        val param = JSONObject()

        param.put("userid", studentEntity!!.AdmissionId)

        val json = JSONObject()
        val keys = inResponse.keySet()
        for (key in keys) {
            try {
                json.put(key, JSONObject.wrap(inResponse.get(key)))
            } catch (e: JSONException) {

            }
        }
        param.put("postResponce", json)

        sendResponseToServer(param);
    }

    private fun sendResponseToServer(param: JSONObject) {
        progressBar.visibility = View.VISIBLE

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/payOnlineResponce")

        val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")

            progressBar.visibility = View.GONE

            when (code) {
                "200" -> try {
                    val alert1 = AlertDialog.Builder(this)
                    alert1.setMessage(HtmlCompat.fromHtml("Status : ${job.getJSONObject("result").optString("status")} "
                            + "<br>Amount: ${job.getJSONObject("result").optString("amount")}"
                            + "<br>Transaction ID: ${job.getJSONObject("result").optString("transactionid")}"
                            + "<br>Bank Reference Number: ${job.getJSONObject("result").optString("bank_ref_num")}"
                            + "<br>Name: ${job.getJSONObject("result").optString("firstName")}"
                            + "<br>Email: ${job.getJSONObject("result").optString("email")}"
                            + "<br>Phone Number: ${job.getJSONObject("result").optString("phone")}", HtmlCompat.FROM_HTML_MODE_LEGACY))
                    alert1.setTitle(getString(R.string.information))
                    alert1.setIcon(R.drawable.ic_clickhere)
                    alert1.setPositiveButton(getString(R.string.ok)) { _, _ ->
                        val intent = Intent()
                        intent.putExtra("message", getString(R.string.success))
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                    alert1.setCancelable(false)
                    alert1.show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@PayOnlineReceiveFeeActivity, error, findViewById<View>(R.id.activity_fee_receive_online_pay))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    @Throws(JSONException::class)
    internal fun getFeeDueForStudent() {
        progressBar.visibility = View.VISIBLE

        val p = JSONObject()
        p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this))
        p.put("admissionId", studentEntity!!.AdmissionId)
        p.put("monthYear", upToMonth)

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/feesPayment/"
                + p)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {
                    if (job.getJSONObject("result").optString("OnlineFeePartialPayment").isNotEmpty()) {
                        OnlineFeePartialPayment = job.getJSONObject("result").optString("OnlineFeePartialPayment").toLowerCase()
                    }
                    val feeDue = Gson()
                            .fromJson<ArrayList<FeeDues>>(job.getJSONObject("result").getJSONObject("studentFees")
                                    .optString("fees"), object : TypeToken<List<FeeDues>>() {

                            }.type)
                    feeDueList.clear()
                    filledFeeAmount.clear()
                    if (Objects.requireNonNull<ArrayList<FeeDues>>(feeDue).size > 0) {
                        feeDue.sortWith(Comparator { feeDue, t1 ->
                            val s1 = feeDue.FeeTypeFrequency
                            val s2 = t1.FeeTypeFrequency
                            s1!!.compareTo(s2!!, ignoreCase = true)
                        })

                        feeDueList.addAll(feeDue)
                        for (i in feeDueList.indices) {
                            filledFeeAmount.add(feeDueList[i].AmountBalance.toString())
                        }
                        adapter.notifyDataSetChanged()
                        calculateFeesSumForDisplay()
                    }
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_green)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@PayOnlineReceiveFeeActivity, error, findViewById<View>(R.id.activity_fee_receive_online_pay))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }

    internal fun calculateFeesSumForDisplay() {
        var quarterly = 0
        var monthly = 0
        var session = 0
        for (i in feeDueList.indices) {
            val amount = if (!"".equals(filledFeeAmount[i], ignoreCase = true))
                Integer.parseInt(filledFeeAmount[i])
            else
                0
            if (feeDueList[i].FeeTypeFrequency.equals("monthly", ignoreCase = true))
                monthly += amount
            if (feeDueList[i].FeeTypeFrequency.equals("quarterly", ignoreCase = true))
                quarterly += amount
            if (feeDueList[i].FeeTypeFrequency.equals("session", ignoreCase = true))
                session += amount
        }
        tvPayOnlineMonthlyTotal.text = monthly.toString()
        tvPayOnlineQuarterlyTotal.text = quarterly.toString()
        tvPayOnlineSessionTotal.text = session.toString()
        tvPayOnlineDueFeesTotal.text = (session + monthly + quarterly).toString()
    }

    inner class PayOnlineReceiveFeeActivityAdapter : RecyclerView.Adapter<PayOnlineReceiveFeeActivityAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_fee_due, parent, false)

            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvFeeName.text = feeDueList[position].FeeTypeName
            holder.tvMonthName.text = feeDueList[position].MonthYear
            holder.tvFeeAmount.text = feeDueList[position].FeeAmount
            holder.etDueAmount.text = filledFeeAmount[position]
        }

        override fun getItemCount(): Int {
            return feeDueList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvFeeName: TextView = itemView.findViewById(R.id.tv_fee_name)
            internal var tvMonthName: TextView = itemView.findViewById(R.id.tv_month_name)
            internal var tvFeeAmount: TextView = itemView.findViewById(R.id.tv_fee_amount)
            internal var etDueAmount: TextView = itemView.findViewById(R.id.et_due_amount)

            init {
                if (Gc.getArrayList(Gc.SIGNEDINUSERPERMISSION, this@PayOnlineReceiveFeeActivity)!!.contains(Gc.PayOnline)) {
                    if (OnlineFeePartialPayment == "allowed") {
                        itemView.setOnClickListener {
                            val position = adapterPosition
                            val dialog = Dialog(this@PayOnlineReceiveFeeActivity)
                            dialog.setContentView(R.layout.layout_fee_due)
                            dialog.setTitle(R.string.enter_amount)

                            val tvFeeFrequency = dialog.findViewById<TextView>(R.id.tv_fee_frequency)
                            tvFeeFrequency.text = feeDueList[position].FeeTypeFrequency

                            val tvFeeTypeName = dialog.findViewById<TextView>(R.id.tv_fee_type_name)
                            tvFeeTypeName.text = feeDueList[position].FeeTypeName

                            val tvFeeAmount = dialog.findViewById<TextView>(R.id.tv_fee_amount)
                            tvFeeAmount.text = feeDueList[position].FeeAmount

                            val tvFeePaid = dialog.findViewById<TextView>(R.id.tv_fee_paid)
                            tvFeePaid.text = feeDueList[position].AmountPaid

                            val tvToBePaid = dialog.findViewById<TextView>(R.id.tv_to_be_paid)
                            tvToBePaid.text = feeDueList[position].AmountBalance

                            val etItemAmount = dialog.findViewById<EditText>(R.id.et_item_amount)
                            etItemAmount.setText(filledFeeAmount[position])

                            val btnSave = dialog.findViewById<Button>(R.id.save)
                            btnSave.setOnClickListener {
                                notifyDataSetChanged()
                                filledFeeAmount[position] = etItemAmount.text.toString()
                                calculateFeesSumForDisplay()
                                dialog.dismiss()
                            }

                            val btnCancel = dialog.findViewById<Button>(R.id.cancel)
                            btnCancel.setOnClickListener { dialog.dismiss() }
                            dialog.show()
                            dialog.setCancelable(false)
                        }
                    }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed here it is 1
        if (requestCode == 2866) {
            if (data != null) {
                //  Log.e("Atom ",data.toString())
                val resKey = data.getStringArrayExtra("responseKeyArray")

                val resValue = data.getStringArrayExtra("responseValueArray")

                val param = JSONObject()

                param.put("userid", studentEntity!!.AdmissionId)

                val json = JSONObject()

                if (resKey != null && resValue != null) {
                    for (i in resKey.indices) {
                        try {
                            json.put(resKey[i], JSONObject.wrap(resValue[i]))
                        } catch (e: JSONException) {

                        }
                    }
                }
                param.put("postResponce", json)
                atomSendResponseToServer(param)
            }
        }
    }

    private fun atomSendResponseToServer(param: JSONObject) {
        Log.e("param", param.toString());

        progressBar.visibility = View.VISIBLE

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/androidAtomPayOnlineResponce")

        val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val alert1 = AlertDialog.Builder(this)
                    alert1.setMessage(HtmlCompat.fromHtml("Status : ${job.getJSONObject("result").optString("status")} "
                            + "<br>Amount: ${job.getJSONObject("result").optString("amount")}"
                            + "<br>Transaction ID: ${job.getJSONObject("result").optString("transactionid")}"
                            + "<br>Bank Reference Number: ${job.getJSONObject("result").optString("bank_ref_num")}"
                            + "<br>Name: ${job.getJSONObject("result").optString("firstName")}"
                            + "<br>Email: ${job.getJSONObject("result").optString("email")}"
                            + "<br>Phone Number: ${job.getJSONObject("result").optString("phone")}", HtmlCompat.FROM_HTML_MODE_LEGACY))
                    alert1.setTitle(getString(R.string.information))
                    alert1.setIcon(R.drawable.ic_clickhere)
                    alert1.setPositiveButton(getString(R.string.ok)) { _, _ ->
                        val intent = Intent()
                        intent.putExtra("message", getString(R.string.success))
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                    alert1.setCancelable(false)
                    alert1.show()

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@PayOnlineReceiveFeeActivity, error, findViewById<View>(R.id.activity_fee_receive_online_pay))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }
}

class FeeDues {
    var FeeTypeName: String? = null
    var FeeId: String? = null
    var FeeTypeFrequency: String? = null
    var MonthYear: String? = null
    var FeeAmount: String? = null
    var AmountPaid: String? = null
    var AmountBalance: String? = null
    var FeeStatus: String? = null
}