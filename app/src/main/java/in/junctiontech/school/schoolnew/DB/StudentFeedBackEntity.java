package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class StudentFeedBackEntity {
    @PrimaryKey
    @NonNull
    public String s_no;
    public String id;
    public String feedbackLog;
    public String date;
    public String senderID;
    public String visibility;
    public String senderName;
    public String Name;

}
