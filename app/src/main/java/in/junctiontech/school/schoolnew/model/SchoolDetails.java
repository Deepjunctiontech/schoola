package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by deep on 27/03/18.
 */
@Entity
public class SchoolDetails {
    @PrimaryKey
    @NonNull
    public String Id;
    public String CurrentSession;
    public String SchoolStartDate;
    public String sessionStartDate;
    public String SchoolName;
    public String SchoolMoto;
    public String Logo;
    public String SchoolAddress;
    public String City;
    public String District;
    public String PIN;
    public String State;
    public String Country;
    public String Mobile;
    public String AlternateMobile;
    public String Email;
    public String Landline;
    public String Fax;
    public String DateOfEstablishment;
    public String Board;
    public String AffiliatedBy;
    public String RegistrationNo;
    public String AffiliationNo;
    public String facebookId;
    public String YouTubeId;
    public String LinkdinId;
    public String gPlusId;
    public String schoolDescription;
    public String schoolMapLocation;
    public String schoolImage;
    public String Terms;
    public String themeColor;
    public String Website;

}
