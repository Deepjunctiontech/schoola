package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FeeTypeDao {


    @Query("select * from FeeTypeEntity where FeeType NOT LIKE 'TRANSPORT' and FeeType NOT LIKE 'TRANSPORT-%' order by Status , Frequency ")
    LiveData<List<FeeTypeEntity>> getFeeTypes();

    @Query("select * from FeeTypeEntity where Status == 'Active' and FeeType NOT LIKE 'TRANSPORT' and FeeType NOT LIKE 'TRANSPORT-%' order by Status , Frequency ") //
    LiveData<List<FeeTypeEntity>> getFeeTypesExcludeTransport();

    @Query("select * from FeeTypeEntity where FeeType == 'TRANSPORT'")
    LiveData<FeeTypeEntity> getFeeTypesTransport();

    @Insert(onConflict = REPLACE)
    void insertFeeType(FeeTypeEntity feeTypeEntity);

    @Query("delete from FeeTypeEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertFeeTypes(List<FeeTypeEntity> feeTypeEntity);
}
