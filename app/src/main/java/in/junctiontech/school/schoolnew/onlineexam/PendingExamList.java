package in.junctiontech.school.schoolnew.onlineexam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.StudentInformation;
import in.junctiontech.school.schoolnew.onlineexam.db.OnlineExamModel;

import static in.junctiontech.school.schoolnew.common.Gc.SCHOOLPREF;

public class PendingExamList extends AppCompatActivity {

    SharedPreferences pref;

    ProgressBar progressBar;
    RecyclerView mRecyclerView;
    ExamListAdapter adapter;
    TextView tv_pending_test;
    RadioButton rb_pending_exams, rb_prev_exams;

    ArrayList<OnlineExamModel> examList = new ArrayList<>();
    ArrayList<OnlineExamModel> pendingExamList = new ArrayList<>();
    ArrayList<OnlineExamModel> prevExamList = new ArrayList<>();
    StudentInformation student;

    private int colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_exam_list);
        pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progressBar);
        tv_pending_test = findViewById(R.id.tv_pending_tests);
        rb_pending_exams = findViewById(R.id.rb_pending_exams);
        rb_pending_exams.setOnClickListener(v -> {
            examList.clear();
            examList.addAll(pendingExamList);
            tv_pending_test.setText(String.valueOf(examList.size()));
            adapter.notifyDataSetChanged();
        });
        rb_prev_exams = findViewById(R.id.rb_prev_exams);
        rb_prev_exams.setOnClickListener(v -> {
            examList.clear();
            examList.addAll(prevExamList);
            tv_pending_test.setText(String.valueOf(examList.size()));
            adapter.notifyDataSetChanged();
        });

        mRecyclerView = findViewById(R.id.rv_pending_exams);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ExamListAdapter();
        mRecyclerView.setAdapter(adapter);

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            student = new Gson().fromJson(Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this), StudentInformation.class);
        } else {
            finish();
            return;
        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3402418187", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            getAssignedOnlineExamFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    private void getAssignedOnlineExamFromServer() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();

        p.put("AdmissionId", student.AdmissionId);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "onlineExam/getStudentExam/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            Log.i("Online Exams List ", job.toString());
            String code = job.optString("code");
            switch (code) {

                case Gc.APIRESPONSE200:
                try {
                    JSONObject resultjobj = job.getJSONObject("result");

                    ArrayList<OnlineExamModel> onlineExams = new Gson()
                            .fromJson(resultjobj.getString("studentCurrentExam"), new TypeToken<List<OnlineExamModel>>() {
                            }.getType());

                    examList.clear();
                    pendingExamList.clear();
                    prevExamList.clear();

                    pendingExamList.addAll(onlineExams);

                    ArrayList<OnlineExamModel> prevExams = new Gson()
                            .fromJson(resultjobj.getString("studentHistoryExam"), new TypeToken<List<OnlineExamModel>>() {
                            }.getType());

                    prevExamList.addAll(prevExams);

                    if (rb_pending_exams.isChecked()) {
                        examList.addAll(pendingExamList);
                        adapter.notifyDataSetChanged();
                    } else if (rb_prev_exams.isChecked()) {
                        examList.addAll(prevExamList);
                        adapter.notifyDataSetChanged();
                    }
                    tv_pending_test.setText(String.valueOf(examList.size()));

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_green);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, PendingExamList.this);

                    Intent intent1 = new Intent(PendingExamList.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, PendingExamList.this);

                    Intent intent2 = new Intent(PendingExamList.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }

        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(PendingExamList.this, error, findViewById(R.id.top_layout));
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    private class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.MyViewHolder> {
        @NonNull
        @Override
        public ExamListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_item_recycler, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ExamListAdapter.MyViewHolder myViewHolder, int i) {
            myViewHolder.tv_single_item.setText(examList.get(i).onlineExamName);
        }

        @Override
        public int getItemCount() {
            return examList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);
                itemView.setOnClickListener(v -> showExamDetails(examList.get(getLayoutPosition())));
            }
        }
    }

    void showExamDetails(final OnlineExamModel examEntity) {
            Intent intent = new Intent(PendingExamList.this, OnlineExamDetail.class);
            intent.putExtra("exam", new Gson().toJson(examEntity));
            startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
