package in.junctiontech.school.schoolnew.messenger;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.FullScreenImageActivity;
import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MyViewHolder> {

    private ArrayList<ChatListEntity> chatListEntities;
    private Context context;

    MessageListAdapter(Context context, ArrayList<ChatListEntity> chatListEntities) {
        this.chatListEntities = chatListEntities;
        this.context = context;
    }

    @NonNull
    @Override
    public MessageListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_conversation, parent, false);
        return new MessageListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageListAdapter.MyViewHolder holder, int position) {

        holder.ll_sender_chat_picture.setVisibility(View.GONE);

        if (chatListEntities.get(position).byMe == 1) {
            holder.tv_logged_user_chat_msg.setText(chatListEntities.get(position).msg);
            holder.tv_logged_user_chat_time.setText(chatListEntities.get(position).enteredDate);

            holder.ll_sender_chat.setVisibility(View.GONE);
            holder.ll_logged_user_chat.setVisibility(View.VISIBLE);
            if ("text".equals(chatListEntities.get(position).msgType)) {
                holder.ll_logged_user_chat_picture.setVisibility(View.GONE);
            }

            if (chatListEntities.get(position).sent == 1) {
                holder.iv_logged_user_alarm.setImageResource(android.R.drawable.ic_lock_idle_alarm);
            }
            if (chatListEntities.get(position).received == 1) {
                holder.iv_logged_user_alarm.setImageResource(R.drawable.ic_double_tick);
            }
        }
        else {
            holder.ll_logged_user_chat.setVisibility(View.GONE);
            holder.ll_sender_chat.setVisibility(View.VISIBLE);
            if (("".equals(chatListEntities.get(position).msg))) {
                holder.ll_sender_chat.setVisibility(View.GONE);
            }

            holder.tv_sender_chat_msg.setText(chatListEntities.get(position).msg);
            holder.tv_sender_user_chat_time.setText(chatListEntities.get(position).enteredDate);

            if (!("".equals(chatListEntities.get(position).imagepath)) && chatListEntities.get(position).imagepath != null) {
                holder.ll_sender_chat_picture.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(chatListEntities.get(position).imagepath)
                        .apply(new RequestOptions()
                                .error(R.drawable.ic_single)
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .thumbnail(0.5f)
                        .into(holder.iv_sender_chat_picture);
            }
        }
    }

    @Override
    public int getItemCount() {
        return chatListEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_logged_user_chat_msg, tv_logged_user_chat_time;

        TextView tv_chat_day_name, tv_sender_chat_msg, tv_sender_user_chat_time;
        LinearLayout ll_sender_chat_picture, ll_sender_chat, ll_logged_user_chat_picture,
                ll_logged_user_chat;
        ImageView iv_sender_chat_picture,
        //                iv_sender_user_alarm,
        iv_logged_user_chat_picture,
                iv_logged_user_alarm;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_chat_day_name = itemView.findViewById(R.id.tv_chat_day_name);
            /*--- sender ids ---*/
            ll_sender_chat_picture = itemView.findViewById(R.id.ll_sender_chat_picture);
            iv_sender_chat_picture = itemView.findViewById(R.id.iv_sender_chat_picture);
            ll_sender_chat = itemView.findViewById(R.id.ll_sender_chat);
            tv_sender_chat_msg = itemView.findViewById(R.id.tv_sender_chat_msg);
//            iv_sender_user_alarm =  itemView.findViewById(R.id.iv_sender_user_alarm);
            tv_sender_user_chat_time = itemView.findViewById(R.id.tv_sender_user_chat_time);

            /*--- logged user ids ---*/
            ll_logged_user_chat_picture = itemView.findViewById(R.id.ll_logged_user_chat_picture);
            iv_logged_user_chat_picture = itemView.findViewById(R.id.iv_logged_user_chat_picture);
            ll_logged_user_chat = itemView.findViewById(R.id.ll_logged_user_chat);
            tv_logged_user_chat_msg = itemView.findViewById(R.id.tv_logged_user_chat_msg);
            iv_logged_user_alarm = itemView.findViewById(R.id.iv_logged_user_alarm);
            tv_logged_user_chat_time = itemView.findViewById(R.id.tv_logged_user_chat_time);

            ll_sender_chat_picture.setOnClickListener(view -> {
                ArrayList<String> galleryImageURLs = new ArrayList<>();
                int i = 0;
                int viewPosition = 0;
                for (ChatListEntity g : chatListEntities) {
                    if (!("".equals(g.imagepath)) && g.imagepath != null) {
                        galleryImageURLs.add(g.imageOriginalPath);
                        if (g.imageOriginalPath.equals(chatListEntities.get(getAdapterPosition()).imageOriginalPath)) {
                            viewPosition = i;
                        }
                        i++;
                    }
                }

                Intent intent = new Intent(context, FullScreenImageActivity.class);
                intent.putStringArrayListExtra("images", galleryImageURLs);
                intent.putExtra("index", viewPosition);
                intent.putExtra("title", String.format("%s : %s", chatListEntities.get(getAdapterPosition()).withType, chatListEntities.get(getAdapterPosition()).withName));
                context.startActivity(intent);
            });
        }
    }
}
