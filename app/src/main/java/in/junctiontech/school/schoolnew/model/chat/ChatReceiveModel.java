package in.junctiontech.school.schoolnew.model.chat;

public class ChatReceiveModel {
    public String msg;
    public String senderId;
    public String msgType;
    public String enteredDate;
    public String msgId;
    public String senderType;
    public String withType;
    public String withId;
}
