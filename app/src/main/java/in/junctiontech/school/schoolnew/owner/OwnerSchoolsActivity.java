package in.junctiontech.school.schoolnew.owner;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class OwnerSchoolsActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rv_owner_school_list;
    private ArrayList<OwnerSchoolData> ownerSchoolList;
    private OwnerSchoolAdapter adapter;
    private int colorIs;
    private View snackbar;
    private boolean isFabOpen;
    private FloatingActionButton fab, fab_edit, fab_msg, fab_delete;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_schools);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        colorIs = Config.getAppColor(this, true);

        snackbar = findViewById(R.id.rl_owner_organization_activity);
        fab = ((FloatingActionButton) findViewById(R.id.fbtn_add_new_owner_organization));
        fab_edit = ((FloatingActionButton) findViewById(R.id.fbtn_edit_new_owner_organization));
        fab_msg = ((FloatingActionButton) findViewById(R.id.fbtn_msg_new_owner_organization));
        fab_delete = ((FloatingActionButton) findViewById(R.id.fbtn_delete_new_owner_organization));
        fab.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fab_edit.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fab_msg.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fab_delete.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.responseSnackBarHandler("You are able to add new Organization soon !", snackbar);
            }
        });
*/
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab_msg.setOnClickListener(this);
        fab_edit.setOnClickListener(this);
        fab_delete.setOnClickListener(this);


        rv_owner_school_list = ((RecyclerView) findViewById(R.id.rv_owner_school_list));
        ownerSchoolList = new ArrayList<OwnerSchoolData>();
        ownerSchoolList.add(new OwnerSchoolData("DEMOSCHOOL", "DEMOSCHOOL", "http://testeducation.zeroerp.com/upload/2F40F6F0.jpg"));
        ownerSchoolList.add(new OwnerSchoolData("DEMOCOACHING", "DEMOCOACHING", "https://education.zeroerp.com/upload/AC7D151D.jpg"));
      /*  ownerSchoolList.add(new OwnerSchoolData("RIPS", "RIPS", "https://education.zeroerp.com/upload/d4e96d3904c285ab9b200ce24e475e4f70ee260314948404822.gif"));
        ownerSchoolList.add(new OwnerSchoolData("CYGNUS123", "CYGNUS123", "https://education.zeroerp.com/upload/a7147bc427ab534b3710f6348092bdf4f4a9d53c14939785182.png"));
        ownerSchoolList.add(new OwnerSchoolData("Demoschool", "DEMOSCHOOL", "https://education.zeroerp.com/upload/61819FB4.jpg"));
        ownerSchoolList.add(new OwnerSchoolData("BCES", "BCES", "https://education.zeroerp.com/upload/4ebae80373f81dff3eda7a6ae3560b63100981e014971658196.jpg"));
        ownerSchoolList.add(new OwnerSchoolData("GSCOACHING", "GSCOACHING", "https://education.zeroerp.com/upload/658dca0f9c2dc4b179ba23b224baae8239778cc014976847154.jpg"));
       */ ownerSchoolList.add(new OwnerSchoolData("DEMOCOLLEGE", "DEMOCOLLEGE", "https://education.zeroerp.com/upload/3A2C9F3A.jpg"));
        setUpRecycler();

    }

    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fab_msg.startAnimation(fab_close);
            fab_edit.startAnimation(fab_close);
            fab_delete.startAnimation(fab_close);
            fab_msg.setClickable(false);
            fab_edit.setClickable(false);
            fab_delete.setClickable(false);
            isFabOpen = false;
            Log.d("Ritu", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab_msg.startAnimation(fab_open);
            fab_edit.startAnimation(fab_open);
            fab_delete.startAnimation(fab_open);
            fab_msg.setClickable(true);
            fab_edit.setClickable(true);
            fab_delete.setClickable(true);
            isFabOpen = true;
            Log.d("Ritu", "open");

        }
    }

    private void setUpRecycler() {
        rv_owner_school_list.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new OwnerSchoolAdapter(this, ownerSchoolList, colorIs);
        rv_owner_school_list.setAdapter(adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fbtn_add_new_owner_organization:

                animateFAB();
                break;
            case R.id.fbtn_edit_new_owner_organization:
                startActivity(new Intent(this, AddOrganizationActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                Log.d("Ritu", "Fab edit");
                break;
            case R.id.fbtn_msg_new_owner_organization:

                Log.d("Ritu", "Fab msg");
                break;
            case R.id.fbtn_delete_new_owner_organization:

                Log.d("Ritu", "Fab delete");
                break;
        }
    }
}
