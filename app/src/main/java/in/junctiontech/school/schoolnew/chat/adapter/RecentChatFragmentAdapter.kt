package `in`.junctiontech.school.schoolnew.chat.adapter

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.chatdb.ChatListEntity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

class RecentChatFragmentAdapter(chatLists: ArrayList<ChatListEntity>, private val context: Context, activity: FragmentActivity?,val listener: ChatItemClickListner) : RecyclerView.Adapter<RecentChatFragmentAdapter.ViewHolder>() {
    private var chatList: ArrayList<ChatListEntity> = chatLists

    private val colorIs = Config.getAppColor(activity, true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_chat_list_new, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listener)
        val obj: ChatListEntity = chatList[position]
        holder.tvChatName.text = String.format("%s : %s", obj.withType, obj.withName)
        holder.tv_chat_unread_counter.visibility=View.GONE

        holder.tv_chat_last_chat_time.text = obj.enteredDate

        if(obj.readByMe>0){
            holder.tv_chat_unread_counter.text = obj.readByMe.toString()
            holder.tv_chat_unread_counter.visibility=View.VISIBLE
        }

        holder.tvChatLastChatMsg.text = obj.msg
        if (!"".equals(obj.withProfileUrl, ignoreCase = true)) {
            Glide.with(context)
                    .load(obj.withProfileUrl)
                    .apply(RequestOptions()
                            .error(R.drawable.ic_single)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(holder.icContact)
        } else {
            holder.icContact.setImageDrawable(getDrawable(context, R.drawable.ic_single))
        }
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var tvChatName: TextView = itemView.findViewById(R.id.tv_chat_name)
        var tvChatLastChatMsg: TextView = itemView.findViewById(R.id.tv_chat_last_chat_msg)
        var tv_chat_unread_counter: TextView = itemView.findViewById(R.id.tv_chat_unread_counter)
        var icContact: CircleImageView = itemView.findViewById(R.id.ic_contact)
        var tv_chat_last_chat_time: TextView = itemView.findViewById(R.id.tv_chat_last_chat_time)
        // var recentChatTab: LinearLayout = itemView.findViewById(R.id.recentChatTab)

        fun bind(listener: ChatItemClickListner) {
            // this is the click listener. It calls the onItemClicked interface method implemented in the Activity
            itemView.setOnClickListener {
                listener.onChatItemClickCallback(adapterPosition)
            }
        }
        /* var tvChatUnreadCounter: TextView = itemView.findViewById(R.id.tv_chat_unread_counter)
         var tvChatLastChatTime: TextView = itemView.findViewById(R.id.tv_chat_last_chat_time)*/
        init {
            tvChatName.setTextColor(colorIs)
            /*itemView.setOnClickListener(this);
            val intent = Intent(activitys, MessageListActivity::class.java)
            intent.putExtra("isFromRecentChat", "yes")
            startActivity(intent)*/
        }
    }
    interface ChatItemClickListner {
        fun onChatItemClickCallback(position: Int)

    }
}
