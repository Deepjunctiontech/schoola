package `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class PayOnlineActivity : AppCompatActivity() {
    private var colorIs: Int = 0
    private var progressBar: ProgressBar? = null
    lateinit var btnPayOnlineSelectMonth: Button
    lateinit var btnAgreeToTermsPay: Button
    lateinit var selectedStudent: String
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_online_pay)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initializeViews()

      /*  val date = SimpleDateFormat("MMM-yyyy", Locale.US).format(Date())
        btnPayOnlineSelectMonth.text = date*/
        val studentString = intent.getStringExtra("student")
        if (studentString == null){
            finish()
            return
        }
        selectedStudent = studentString
        btnPayOnlineSelectMonth.text = intent.getStringExtra("date")


        setColorApp()
        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/2655833796", this, adContainer)
        }
    }

    private fun initializeDatePickers() {
        val myCalendar = Calendar.getInstance()

        val upToMonth = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            val myFormat = "MMM-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            btnPayOnlineSelectMonth.setText(sdf.format(myCalendar.time))
        }
        btnPayOnlineSelectMonth.setOnClickListener {
            DatePickerDialog(this@PayOnlineActivity, upToMonth, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    private fun initializeViews() {
        progressBar = findViewById<ProgressBar>(R.id.progressBar)

        btnPayOnlineSelectMonth = findViewById(R.id.btn_pay_online_select_month)

        initializeDatePickers()

        btnAgreeToTermsPay = findViewById(R.id.btn_agree_to_terms_pay)
        btnAgreeToTermsPay.setOnClickListener {
            if (btnPayOnlineSelectMonth.text.isNotEmpty() && getString(R.string.month) != btnPayOnlineSelectMonth.getText().toString()) {
               /* val intent = Intent()
                intent.putExtra("message", getString(R.string.success))
                setResult(Activity.RESULT_OK, intent)
                finish()*/
                val intent = Intent(this@PayOnlineActivity, PayOnlineReceiveFeeActivity::class.java)
                intent.putExtra("upToMonth", btnPayOnlineSelectMonth.text.toString())
                intent.putExtra("student", selectedStudent)
                startActivityForResult(intent, Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE)
                //startActivityForResult(intent, Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE);
            } else {
                if (getString(R.string.month) == btnPayOnlineSelectMonth.getText().toString()) {
                    Config.responseSnackBarHandler(getString(R.string.month) + " " + getString(R.string.field_can_not_be_blank),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue)
                    btnPayOnlineSelectMonth.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
                }
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent()
                intent.putExtra("message", getString(R.string.success))
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        } else
            super.onActivityResult(requestCode, resultCode, data)

    }

}