package `in`.junctiontech.school.schoolnew.student

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.MasterEntryEntity
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*

class StudentTerminateActivity : AppCompatActivity() {

    private var colorIs: Int = 0
    internal lateinit var mDb: MainDatabase
    lateinit var progressBar: ProgressBar

    private lateinit var etStudentTerminationRemarks: TextView
    private lateinit var btnSelectStudentDateOfTermination: Button
    private lateinit var btnSelectStudentReason: Button

    private var terminationReasonList = ArrayList<String>()
    private var terminationReasonListMaster = ArrayList<MasterEntryEntity>()
    lateinit var terminationReasonListBuilder: AlertDialog.Builder
    lateinit var terminationReasonListAdapter: ArrayAdapter<String>

    var selectedReason: Int = 0
    lateinit var studentUpdate: SchoolStudentEntity


    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terminate_student)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)

        btnSelectStudentDateOfTermination = findViewById(R.id.btn_select_student_date_of_termination)
        etStudentTerminationRemarks = findViewById(R.id.et_student_termination_remarks)

        mDb = MainDatabase.getDatabase(this)

        getDataFromRoom()
        initializeDatePickers()
        selectTerminationReason()
        if (!Strings.nullToEmpty(intent.getStringExtra("student")).equals("", ignoreCase = true)) {
            studentUpdate = Gson().fromJson(intent.getStringExtra("student"), SchoolStudentEntity::class.java)


        } else {

        }

        setColorApp()

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/8851312686", this, adContainer)
        }
    }


    internal fun initializeDatePickers() {
        val myCalendar = Calendar.getInstance()
        // Date of  Termination
        val dot = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val sdf = SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US)
            btnSelectStudentDateOfTermination.text = sdf.format(myCalendar.time)
        }

        btnSelectStudentDateOfTermination.setOnClickListener { _ ->
            DatePickerDialog(this@StudentTerminateActivity, dot, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    private fun getDataFromRoom() {
        mDb.masterEntryModel().getMasterEntryValuesGeneric("TerminationReason")
                .observe(this, Observer<List<MasterEntryEntity>> { masterEntryEntities ->
                    terminationReasonListMaster.clear()
                    terminationReasonList.clear()
                    terminationReasonListMaster.addAll(masterEntryEntities)
                    for (i in terminationReasonListMaster.indices) {
                        terminationReasonList.add(terminationReasonListMaster.get(i).MasterEntryValue)
                    }
                    terminationReasonListAdapter.clear()
                    terminationReasonListAdapter.addAll(terminationReasonList)
                    terminationReasonListAdapter.notifyDataSetChanged()
                })
    }


    private fun selectTerminationReason() {
        btnSelectStudentReason = findViewById(R.id.btn_select_student_reason)

        terminationReasonListBuilder = AlertDialog.Builder(this@StudentTerminateActivity)
        terminationReasonListAdapter = ArrayAdapter<String>(this@StudentTerminateActivity, android.R.layout.select_dialog_singlechoice)

        terminationReasonListAdapter.addAll(terminationReasonList)

        btnSelectStudentReason.setOnClickListener { _ ->
            terminationReasonListBuilder.setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.dismiss() }
            terminationReasonListBuilder.setAdapter(terminationReasonListAdapter) { _, i ->
                btnSelectStudentReason.text = terminationReasonList[i]
                selectedReason = 1 + i
            }
            val dialog = terminationReasonListBuilder.create()
            dialog.show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                if (validateInput()) {
                    terminateStudent()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun terminateStudent() {
        progressBar.visibility = View.VISIBLE

        val param = JSONObject()
        param.put("DateOfTermination", btnSelectStudentDateOfTermination.text.toString().trim { it <= ' ' })
        param.put("TerminationReason", terminationReasonListMaster[selectedReason - 1].MasterEntryId)
        param.put("TerminationRemarks", etStudentTerminationRemarks.text.toString().trim { it <= ' ' })
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this))
        param.put("RegistrationId", studentUpdate.RegistrationId)

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentTermination")

        val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            progressBar.visibility = View.GONE

            val code = job.optString("code")

            when (code) {
                Gc.APIRESPONSE200 -> try {
                    val schoolStudentEntities = ArrayList<SchoolStudentEntity>()
                    studentUpdate.Status = "Terminated"
                    schoolStudentEntities.add(studentUpdate)
                    Thread(Runnable { mDb.schoolStudentModel().insertStudents(schoolStudentEntities) }).start()

                    val intent = Intent()
                    intent.putExtra("message", getString(R.string.success)
                            + " : "
                            + job.optString("message")
                    )
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                Gc.APIRESPONSE401 -> {
                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this@StudentTerminateActivity)

                    val intent1 = Intent(this@StudentTerminateActivity, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this@StudentTerminateActivity)

                    val intent2 = Intent(this@StudentTerminateActivity, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_terminate_student), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@StudentTerminateActivity, error, findViewById<View>(R.id.rl_terminate_student))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }

            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun validateInput(): Boolean {
        if ("" == btnSelectStudentDateOfTermination.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.date_of_termination) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_terminate_student), R.color.fragment_first_blue)
            btnSelectStudentDateOfTermination.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if ("" == etStudentTerminationRemarks.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.termination_remarks) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_terminate_student), R.color.fragment_first_blue)
            etStudentTerminationRemarks.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if ("" == btnSelectStudentReason.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.reason) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_terminate_student), R.color.fragment_first_blue)
            btnSelectStudentReason.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}