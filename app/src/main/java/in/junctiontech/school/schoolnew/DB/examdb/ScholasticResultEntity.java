package in.junctiontech.school.schoolnew.DB.examdb;

public class ScholasticResultEntity {
    public String AdmissionId;
    public String AdmissionNo;
    public String StudentName;
    public String Exam_Detail_Id;
    public String Exam_Type;
    public String Exam_Detail_Status;
    public String Section_Id;
    public String Student_Id;
    public String Session;
    public String Subject_Id;
    public String Marks_Obtain;
    public String Max_Marks;
    public String Result;
    public String Grade;
    public String Remarks;
    public String DateOfExam;
    public String DOC;
    public String DOU;
    public String Evaluated_By;
    public String ResultValue;
    public String Absent;

}
