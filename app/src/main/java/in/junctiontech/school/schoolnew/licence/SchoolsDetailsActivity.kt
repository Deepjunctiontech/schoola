package `in`.junctiontech.school.schoolnew.licence

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolDetailsEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class SchoolsDetailsActivity : AppCompatActivity() {
    private lateinit var schoolDetails: SchoolDetailsEntity

    private lateinit var mDb: MainDatabase

    private lateinit var tvEmailId: TextView
    private lateinit var tvContactNumber: TextView
    private lateinit var tvLicenceExpiredMsg: TextView

    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_school)
        mDb = MainDatabase.getDatabase(applicationContext)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setColorApp()
        initializeViews()
    }

    override fun onResume() {
        super.onResume()
        mDb.schoolDetailsModel().schoolDetailsLive.observe(this, Observer { schoolDetailsEntity ->
            if (schoolDetailsEntity != null) {
                schoolDetails = schoolDetailsEntity
                setTextInViews()
            }
        })
        getLicenceDetails()
    }


    private fun getLicenceDetails() {
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/licenseDetails")

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            when (job.optString("code")) {
                "200" -> try {
                    val license = job.getJSONObject("result").getJSONObject("licenseDetails").optString("license")
                    if (license != "") {
                        val setDefaults = HashMap<String, String>()
                        setDefaults[Gc.LICENCE] = license
                        Gc.setSharedPreference(setDefaults, this)
                        initializeViews()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_details_school), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            Config.responseVolleyErrorHandler(this@SchoolsDetailsActivity, error, findViewById<View>(R.id.activity_details_school))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun initializeViews() {
        tvEmailId = findViewById(R.id.tv_email_id)
        tvContactNumber = findViewById(R.id.tv_contact_number)
        tvLicenceExpiredMsg = findViewById(R.id.tv_licence_expired_msg)

        val licenceExpiry = Gc.getSharedPreference(Gc.LICENCE, this)

        if (!licenceExpiry.equals("", ignoreCase = true)) {
            val licenceExpiryArry = licenceExpiry.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val calCurrent = Calendar.getInstance()
            val calLicence = Calendar.getInstance()
            if (licenceExpiryArry.size >= 3) {
                if (licenceExpiryArry[0].length == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[0]), Integer.parseInt(licenceExpiryArry[1]) - 1, Integer.parseInt(licenceExpiryArry[2]))
                else if (licenceExpiryArry[2].length == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[2]), Integer.parseInt(licenceExpiryArry[1]) - 1, Integer.parseInt(licenceExpiryArry[0]))

            }

            val diff = calLicence.timeInMillis - calCurrent.timeInMillis

            val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)

            if (dayCount.toInt() < 0) {
                tvLicenceExpiredMsg.text = "Your Organization Licence Is Expired At " + licenceExpiry + ". Please Contact Your Institute at Following Details"
            } else {
                finish()
            }
        }
    }

    private fun setTextInViews() {
        tvEmailId.text = Strings.nullToEmpty(schoolDetails.Email)
        tvContactNumber.text = Strings.nullToEmpty(schoolDetails.Mobile)
    }
}