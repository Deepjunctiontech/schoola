package in.junctiontech.school.schoolnew.student;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.attendance.StudentMonthAttendanceActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.communicate.SendSMSActivity;
import in.junctiontech.school.schoolnew.exam.ExamResultStudentActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.StudentFeeListActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.receive.FeeReceiptsActivity;
import in.junctiontech.school.schoolnew.reviewlog.ViewFeedback;

public class StudentActivity extends AppCompatActivity {
    ProgressBar progressBar;

    private FloatingActionButton fb_student_add;

    private ArrayList<SchoolStudentEntity> schoolStudentList = new ArrayList<>();
    private ArrayList<SchoolStudentEntity> studentfilterList = new ArrayList<>();

    MainDatabase mDb;

    private StudentListAdapter adapter;

    String action_requested;

    private int colorIs;
    boolean Terminated = false;
    Button btn_terminated_student;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_student_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        btn_terminated_student.setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        mDb = MainDatabase.getDatabase(this);
        fb_student_add = findViewById(R.id.fb_student_add);
        progressBar = findViewById(R.id.progressBar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        fb_student_add.setOnClickListener(view -> showActivityForStudentCreate());

        if (Gc.getSharedPreference(Gc.STUDENTSETUPALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            fb_student_add.show();
        }

        action_requested = Strings.nullToEmpty(getIntent().getStringExtra("action"));
        if (!("".equalsIgnoreCase(action_requested))) {
            fb_student_add.hide();
            Config.responseSnackBarHandler(getString(R.string.select_student),
                    findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
        }

        btn_terminated_student = findViewById(R.id.btn_terminated_student);
        btn_terminated_student.setOnClickListener(view -> {
            Terminated = !Terminated;
            if (Terminated) {
                mDb.schoolStudentModel()
                        .getAllSessionStudents(Gc.getSharedPreference(Gc.APPSESSION, this))
                        .observe(this, schoolStudentEntities -> {
                            schoolStudentList.clear();
                            studentfilterList.clear();
                            schoolStudentList.addAll(schoolStudentEntities);
                            studentfilterList.addAll(schoolStudentEntities);
                            adapter.notifyDataSetChanged();

                        });
            } else {
                mDb.schoolStudentModel()
                        .getAllStudentsSession(Gc.getSharedPreference(Gc.APPSESSION, this))
                        .observe(this, schoolStudentEntities -> {
                            schoolStudentList.clear();
                            studentfilterList.clear();
                            schoolStudentList.addAll(schoolStudentEntities);
                            studentfilterList.addAll(schoolStudentEntities);
                            adapter.notifyDataSetChanged();

                        });

            }
        });

        RecyclerView mRecyclerView = findViewById(R.id.rv_student_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new StudentListAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/8408938483", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolStudentModel()
                .getAllStudentsSession(Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, schoolStudentEntities -> {
                    schoolStudentList.clear();
                    studentfilterList.clear();
                    schoolStudentList.addAll(schoolStudentEntities);
                    studentfilterList.addAll(schoolStudentEntities);
                    adapter.notifyDataSetChanged();

                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolStudentModel()
                .getAllSessionStudents(Gc.getSharedPreference(Gc.APPSESSION, this))
                .removeObservers(this);
    }

    void showActivityForStudentCreate() {
        startActivityForResult(new Intent(StudentActivity.this, StudentCreateActivity.class),
                Gc.STUDENT_CREATE_REQUEST_CODE)
        ;

    }

    void showActivityForStudentUpdate(String student) {
        Intent intent = new Intent(StudentActivity.this, StudentCreateActivity.class);
        intent.putExtra("student", student);
        startActivityForResult(intent, Gc.STUDENT_CREATE_REQUEST_CODE);
    }

    void showActivityForStudentTerminate(String student) {
        Intent intent = new Intent(StudentActivity.this, StudentTerminateActivity.class);
        intent.putExtra("student", student);
        startActivityForResult(intent, Gc.STUDENT_TERMINATE_REQUEST_CODE);
    }

    private void studentTerminationCancel(String student) {
        android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setTitle("Are You Sure To Cancel Termination ?");
        alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        alertUpdate.setPositiveButton(R.string.yes, (dialog, which) -> {
                SchoolStudentEntity studentUpdate = new Gson().fromJson(student, SchoolStudentEntity.class);
                try {
                    cancelTermination(studentUpdate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        });
        alertUpdate.setNegativeButton(R.string.no, null);
        //alertUpdate.setView(view_update);
        alertUpdate.setCancelable(false);
        alertUpdate.show();
    }

    private void cancelTermination(SchoolStudentEntity studentUpdate) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = new JSONObject();
        param.put("RegistrationId", studentUpdate.RegistrationId);
        /*param.put("Username", userName.trim());
        param.put("Password", password.trim());*/

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentTermination";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    studentUpdate.Status = "Studying";
                    final ArrayList<SchoolStudentEntity> schoolStudentEntities = new ArrayList<>();
                    schoolStudentEntities.add(studentUpdate);
                    new Thread(() -> mDb.schoolStudentModel().insertStudents(schoolStudentEntities)).start();

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.ll_student_activity), R.color.fragment_first_green);
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, StudentActivity.this);

                    Intent intent1 = new Intent(StudentActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();
                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, StudentActivity.this);

                    Intent intent2 = new Intent(StudentActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    finish();

                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(StudentActivity.this, error, findViewById(R.id.ll_student_activity));
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Gc.STUDENT_CREATE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.ll_student_activity), R.color.fragment_first_green);
            }
        } else if (requestCode == Gc.STUDENT_TERMINATE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.ll_student_activity), R.color.fragment_first_green);
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                Objects.requireNonNull(searchManager).getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterStudent(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterStudent(s);
                return true;
            }
        });
        return true;
    }

    void filterStudent(String text) {

        schoolStudentList.clear();
        if (text.isEmpty()) {
            schoolStudentList.addAll(studentfilterList);
        } else {
            text = text.toLowerCase();
            for (SchoolStudentEntity s : studentfilterList) {
                if (s.StudentName.toLowerCase().contains(text)
                        || s.ClassName.toLowerCase().contains(text)
                        || s.AdmissionNo.toLowerCase().contains(text)
                ) {
                    schoolStudentList.add(s);
                }
            }

        }
        adapter.notifyDataSetChanged();

    }

    public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {


        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_for_list_data, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            if (Terminated && schoolStudentList.get(position).Status.equalsIgnoreCase("Terminated")) {
                if (schoolStudentList.get(position).Status.equalsIgnoreCase("Terminated")) {
                    holder.itemView.setBackgroundColor(Color.GRAY);
                } else {
                    holder.itemView.setBackgroundColor(Color.WHITE);
                }

                holder.tv_item_name.setText(schoolStudentList.get(position).StudentName.replace("_", " "));
                holder.tv_item_name.setTextColor(colorIs);

                holder.tv_designation.setText(schoolStudentList.get(position).ClassName
                        + " : "
                        + schoolStudentList.get(position).SectionName
                );
                holder.tv_designation.setTextColor(colorIs);

                holder.tv_admission_no.setText(schoolStudentList.get(position).AdmissionNo);

                if (!("".equals(schoolStudentList.get(position).studentProfileImage)))
                    Glide.with(StudentActivity.this)
                            .load(schoolStudentList.get(position).studentProfileImage)
                            .apply(new RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.iv_item_icon);
                else
                    holder.iv_item_icon.setImageDrawable(getDrawable(R.drawable.ic_single));

            } else {
                if (!schoolStudentList.get(position).Status.equalsIgnoreCase("Terminated")) {

                    holder.itemView.setBackgroundColor(Color.WHITE);


                    holder.tv_item_name.setText(schoolStudentList.get(position).StudentName.replace("_", " "));
                    holder.tv_item_name.setTextColor(colorIs);

                    holder.tv_designation.setText(schoolStudentList.get(position).ClassName
                            + " : "
                            + schoolStudentList.get(position).SectionName
                    );
                    holder.tv_designation.setTextColor(colorIs);

                    holder.tv_admission_no.setText(schoolStudentList.get(position).AdmissionNo);

                    if (!("".equals(schoolStudentList.get(position).studentProfileImage)))
                        Glide.with(StudentActivity.this)
                                .load(schoolStudentList.get(position).studentProfileImage)
                                .apply(new RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .thumbnail(0.5f)
                                .into(holder.iv_item_icon);
                    else
                        holder.iv_item_icon.setImageDrawable(getDrawable(R.drawable.ic_single));
                }


            }
        }

        @Override
        public int getItemCount() {
            return schoolStudentList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_item_name, tv_designation, tv_admission_no;

            CircleImageView iv_item_icon;

            public MyViewHolder(View itemView) {
                super(itemView);
                iv_item_icon = itemView.findViewById(R.id.tv_item_profile_image);
                iv_item_icon.setVisibility(View.VISIBLE);
                tv_item_name = itemView.findViewById(R.id.tv_item_title);
//            tv_item_name.setTextColor(appColor);
                tv_designation = itemView.findViewById(R.id.tv_item_subtitle);
                tv_designation.setVisibility(View.VISIBLE);

                tv_admission_no = itemView.findViewById(R.id.tv_admission_no);
                tv_admission_no.setVisibility(View.VISIBLE);

                itemView.setOnClickListener(view -> {
                    AlertDialog.Builder choices = new AlertDialog.Builder(StudentActivity.this);
                    final String student = new Gson().toJson(schoolStudentList.get(getAdapterPosition()));

                    String terminate;
                    if (schoolStudentList.get(getAdapterPosition()).Status.equals("Terminated")) {
                        terminate = getResources().getString(R.string.cancel_termination);
                    } else {
                        terminate = getResources().getString(R.string.terminate_student);
                    }

                    String[] aa = new String[]
                            {getResources().getString(R.string.edit),
                                    getResources().getString(R.string.update_student_fee),
                                    getResources().getString(R.string.fee_payment),
//                                    context.getResources().getString(R.string.copy_username_password),
                                    getResources().getString(R.string.sms),
                                    getResources().getString(R.string.view_attendance),
                                    getResources().getString(R.string.exam_result_scholastic),
                                    terminate
                            };

                    choices.setItems(aa, (dialogInterface, which) -> {

                        switch (which) {
                            case 0:
                                if (Gc.getSharedPreference(Gc.STUDENTSETUPALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                                    showActivityForStudentUpdate(student);
                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                            findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                                }
                                break;

                            case 1:
                                if (Gc.getSharedPreference(Gc.FEESETUPALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                                    startActivity(new Intent(StudentActivity.this, StudentFeeListActivity.class)
                                            .putExtra("student", student));
                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                            findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                                }
                                break;

                            case 2:
                                startActivity(new Intent(StudentActivity.this, FeeReceiptsActivity.class)
                                        .putExtra("student", student));
                                break;
                            case 3:
                                if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                                    startActivity(new Intent(StudentActivity.this, SendSMSActivity.class)
                                            .putExtra("student", student));
                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                            findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                                }
                                break;
                            case 4:
                                startActivity(new Intent(StudentActivity.this, StudentMonthAttendanceActivity.class)
                                        .putExtra("student", student));
                                break;
                            case 5:
                                startActivity(new Intent(StudentActivity.this, ExamResultStudentActivity.class)
                                        .putExtra("student", student));

                                break;
                            case 6:
                                if (Gc.getSharedPreference(Gc.STUDENTSETUPALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                                    if (schoolStudentList.get(getAdapterPosition()).Status.equals("Terminated")) {
                                        studentTerminationCancel(student);
                                    } else {
                                        showActivityForStudentTerminate(student);
                                    }


                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.permission_denied), findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                                }
                                break;
                            case 7:
                                if (Gc.getSharedPreference(Gc.STUDENTSETUPALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                                    Config.responseSnackBarHandler(getString(R.string.comming_soon), findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
//                                            startActivity(new Intent(StudentActivity.this, ChangeClassActivity.class)
//                                                .putExtra("student",student));
                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                            findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                                }
                                break;
                        }

                    });
                    if ("".equalsIgnoreCase(action_requested))
                        choices.show();
                    else if ("sms".equalsIgnoreCase(action_requested)) {
                        if (Gc.getSharedPreference(Gc.SENDMESSAGEALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                            startActivity(new Intent(StudentActivity.this, SendSMSActivity.class)
                                    .putExtra("student", student));
                        } else {
                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                    findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                        }
                    } else if ("updateStudentFee".equalsIgnoreCase(action_requested)) {
                        if (Gc.getSharedPreference(Gc.FEESETUPALLOWED, StudentActivity.this).equalsIgnoreCase(Gc.TRUE)) {
                            startActivity(new Intent(StudentActivity.this, StudentFeeListActivity.class)
                                    .putExtra("student", student));
                        } else {
                            Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                    findViewById(R.id.ll_student_activity), R.color.fragment_first_blue);
                        }
                    } else if ("feepayment".equalsIgnoreCase(action_requested)) {
                        startActivity(new Intent(StudentActivity.this, FeeReceiptsActivity.class)
                                .putExtra("student", student));
                    } else if ("feeReceipt".equalsIgnoreCase(action_requested)) {
                        startActivity(new Intent(StudentActivity.this, FeeReceiptsActivity.class)
                                .putExtra("student", student));
                    } else if ("scholasticExamResult".equalsIgnoreCase(action_requested)) {
                        startActivity(new Intent(StudentActivity.this, ExamResultStudentActivity.class)
                                .putExtra("student", student));
                    } else if ("reviewLog".equalsIgnoreCase(action_requested)) {
                        startActivity(new Intent(StudentActivity.this, ViewFeedback.class)
                                .putExtra("student", student));
                    }
                });
            }
        }
    }

}
