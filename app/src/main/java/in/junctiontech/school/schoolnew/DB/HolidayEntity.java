package in.junctiontech.school.schoolnew.DB;

public class HolidayEntity {
     public String SectionID;
    public String  Date;
    public String SlotID;
    public String Holiday;
    public String SlotName;
    public String StartTime;
    public String EndTime;
    public String Subject;
    public String Teacher;
    public String Chapter;
    public String Topic;
    public String Comment;
    public String CreatedBy;
    public String CreatedDate;
    public String Flag;
    public String Type;
    public String ClassName;
    public String SectionName;
}
