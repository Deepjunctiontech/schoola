package in.junctiontech.school.schoolnew.messenger;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.chatdb.ChatDatabase;
import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class ConversationsActivity extends AppCompatActivity {

    private MainDatabase mDb;
    ChatDatabase chatDb;
    private ArrayList<ChatListEntity> chatList = new ArrayList<>();
    private ArrayList<ChatListEntity> chatFilterList = new ArrayList<>();
    private ConversationsAdapter adapter = new ConversationsAdapter();

    FloatingActionButton fb_start_conversation;
    RecyclerView mRecyclerView;

    RadioButton rb_messages;

    private int colorIs;

    StudentInformation studentInfo;
    SignedInStaffInformationEntity staffInfo;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_start_conversation.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        mDb = MainDatabase.getDatabase(this);
        chatDb = ChatDatabase.getDatabase(this);

        rb_messages = findViewById(R.id.rb_messages);
        rb_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConversationsActivity.this, MessageListActivity.class);
                intent.putExtra("messages", "messages");
                startActivity(intent);
            }
        });

        fb_start_conversation = findViewById(R.id.fb_start_conversation);
        fb_start_conversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConversationsActivity.this, StartNewConversationActivity.class);
                if (studentInfo != null)
                    intent.putExtra(Gc.STUDENTPARENT, new Gson().toJson(studentInfo));
                else if (staffInfo != null) intent.putExtra(Gc.STAFF, new Gson().toJson(staffInfo));
                startActivity(intent);
            }
        });
        mRecyclerView = findViewById(R.id.rv_conversations);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(adapter);

        chatDb.chatListModel().getRecentChats().observe(this, new Observer<List<ChatListEntity>>() {
            @Override
            public void onChanged(@Nullable List<ChatListEntity> chatListEntities) {
                chatList.clear();
                chatFilterList.clear();
                chatList.addAll(chatListEntities);
                chatFilterList.addAll(chatList);
                adapter.notifyDataSetChanged();
            }
        });

        String role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

        if (role.equalsIgnoreCase(Gc.STUDENTPARENT)) {
            mDb.signedInStudentInformationModel().getSignedInStudentInformation()
                    .observe(this, new Observer<StudentInformation>() {
                        @Override
                        public void onChanged(@Nullable StudentInformation studentInformation) {
                            studentInfo = studentInformation;
                        }
                    });
        } else if (role.equalsIgnoreCase(Gc.STAFF)) {
            mDb.signedInStaffInformationModel().getSignedInStaff()
                    .observe(this, new Observer<SignedInStaffInformationEntity>() {
                        @Override
                        public void onChanged(@Nullable SignedInStaffInformationEntity signedInStaffInformationEntity) {
                            staffInfo = signedInStaffInformationEntity;
                        }
                    });

        } else if (role.equalsIgnoreCase(Gc.ADMIN)) {

        }
        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4118385648",this,adContainer);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sort_and_search, menu);
        menu.findItem(R.id.action_save).setVisible(false);
        menu.findItem(R.id.action_select_all).setVisible(false);
        menu.findItem(R.id.menu_sort_by_alphabetical).setVisible(false);
        menu.findItem(R.id.menu_sort_by_number).setVisible(false);
        menu.findItem(R.id.action_help).setVisible(false);
        menu.findItem(R.id.action_delete).setVisible(false);
//        menu.findItem(R.id.action_delete).setVisible(true);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.menu_action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterConversations(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterConversations(s);
                return true;
            }
        });
        return true;
    }

    void filterConversations(String text) {
        chatList.clear();
        if (text.isEmpty()) {
            chatList.addAll(chatFilterList);
        } else {
            text = text.toLowerCase();
            for (ChatListEntity c : chatFilterList) {
                if (c.withName.toLowerCase().contains(text)
                        || c.withType.toLowerCase().contains(text)
                        || c.msg.toLowerCase().contains(text)) {
                    chatList.add(c);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();
                break;
            case R.id.action_delete:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.MyViewHolder> {
        @NonNull
        @Override
        public ConversationsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_list, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ConversationsAdapter.MyViewHolder holder, int position) {
            ChatListEntity obj = chatList.get(position);
            holder.tv_chat_name.setText(String.format("%s : %s", obj.withType, obj.withName));
            holder.tv_chat_last_chat_msg.setText(obj.msg);
            if (!("".equalsIgnoreCase(obj.withProfileUrl))) {
                Glide.with(ConversationsActivity.this)
                        .load(obj.withProfileUrl)
                        .apply(new RequestOptions()
                                .error(R.drawable.ic_single)
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .thumbnail(0.5f)
                        .into(holder.ic_contact);
            } else {
                holder.ic_contact.setImageDrawable(getDrawable(R.drawable.ic_single));
            }
        }

        @Override
        public int getItemCount() {
            return chatList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_chat_name, tv_chat_unread_counter, tv_chat_last_chat_time, tv_chat_last_chat_msg;
            CircleImageView ic_contact;

            public MyViewHolder(View itemView) {
                super(itemView);

                tv_chat_name = itemView.findViewById(R.id.tv_chat_name);
                tv_chat_name.setTextColor(colorIs);
                tv_chat_unread_counter = itemView.findViewById(R.id.tv_chat_unread_counter);
                tv_chat_last_chat_msg = itemView.findViewById(R.id.tv_chat_last_chat_msg);
                tv_chat_last_chat_time = itemView.findViewById(R.id.tv_chat_last_chat_time);
                ic_contact = itemView.findViewById(R.id.ic_contact);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String chat = new Gson().toJson(chatList.get(getAdapterPosition()));
                        Intent intent = new Intent(ConversationsActivity.this, MessageListActivity.class);
                        intent.putExtra("previous", chat);
                        startActivity(intent);

                    }
                });

                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {


                        return true;
                    }
                });
            }
        }
    }

}
