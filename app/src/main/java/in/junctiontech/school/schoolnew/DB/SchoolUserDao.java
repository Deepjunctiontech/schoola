package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolUserDao {
    @Query("select * from SchoolUserEntity")
    LiveData<List<SchoolUserEntity>> getAllUsers();

    @Query("select * from SchoolUserEntity where StaffId like :staffid")
    LiveData<SchoolUserEntity> getUserforStaff(String staffid);

    @Insert(onConflict = REPLACE)
    void insertUsers(List<SchoolUserEntity> userEntities);
}
