package `in`.junctiontech.school.schoolnew.assignments.modelhelper

class HomeworkDetails {
    var homeworkDetailsId : String? = null
    var homeworkId : String? = null
    var studentId : String? = null
    var userId  : String? = null
    var userType : String? = null
    var date  : String? = null
    var description : String? = null
    var descriptionType : String? = null
    var mediaUrl  : String? = null
}