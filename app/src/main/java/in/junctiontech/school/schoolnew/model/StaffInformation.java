package in.junctiontech.school.schoolnew.model;

/**
 * Created by deep on 28/03/18.
 */

public class StaffInformation {
    public String gcmToken;
    public String IMEI;
    public String UserDevice;
    public String Status;
    public String Staff_terms;
    public String StaffId;
    public String StaffStatus;
    public String StaffPosition;
    public String StaffName;
    public String StaffMobile;
    public String staffProfileImage;
    public String StaffEmail;
    public String StaffAlternateMobile;
    public String StaffFName;
    public String StaffMName;
    public String StaffDOJ;
    public String StaffDOB;
    public String StaffPresentAddress;
    public String StaffPermanentAddress;
    public String StaffPositionValue;

}
