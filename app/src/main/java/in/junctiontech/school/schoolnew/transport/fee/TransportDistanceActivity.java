package in.junctiontech.school.schoolnew.transport.fee;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class TransportDistanceActivity extends AppCompatActivity {

    MainDatabase mDb;

    FloatingActionButton fb_distance_add;

    RecyclerView mRecyclerView;

    ArrayList<MasterEntryEntity> transportDistanceList = new ArrayList<>();

    TransportDistanceAdapter adapter;

    int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));

        fb_distance_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport_distance);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        fb_distance_add = findViewById(R.id.fb_distance_add);
        fb_distance_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForDistanceCreate();
            }
        });

        mRecyclerView = findViewById(R.id.rv__transport_fee__distance);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new TransportDistanceAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2546237712", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDb.masterEntryModel().getMasterEntryValuesGeneric(Gc.DISTANCE).observe(this, new Observer<List<MasterEntryEntity>>() {
            @Override
            public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                transportDistanceList.clear();
                transportDistanceList.addAll(masterEntryEntities);
                adapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.masterEntryModel().getMasterEntryValuesGeneric(Gc.DISTANCE).removeObservers(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:

                Intent intent = new Intent(TransportDistanceActivity.this, TransportFeeAmountActivity.class);
                intent.putExtra("type", getIntent().getStringExtra("type"));
                intent.putExtra("feetypeid", getIntent().getStringExtra("feetypeid"));
                intent.putExtra("fees", getIntent().getStringExtra("fees"));
                startActivity(intent);
                finish();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void showDialogForDistanceCreate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.update);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.layout_transport_distance, null);

        final EditText et_distance = view.findViewById(R.id.et_distance);
        et_distance.setHint(R.string.enter_new_distance);

        final RadioButton rb_km = view.findViewById(R.id.rb_km);
        final RadioButton rb_miles = view.findViewById(R.id.rb_miles);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called", "");
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if (et_distance.getText().toString().trim().isEmpty()) {
                    et_distance.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                    et_distance.requestFocus();
                } else if (!(rb_km.isChecked()) && !(rb_miles.isChecked())) {
                    rb_km.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else {

                    Log.i("dISTANCE", et_distance.getText().toString());
                    String distancetext = et_distance.getText().toString().trim();
                    if (rb_km.isChecked())
                        distancetext = distancetext + " " + rb_km.getText();
                    else if (rb_miles.isChecked())
                        distancetext = distancetext + " " + rb_miles.getText();

                    try {
                        createDistanceForTransport(distancetext);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    wantToCloseDialog = true;
                }
                if (wantToCloseDialog)
                    dialog.dismiss();
            }
        });
    }

    void createDistanceForTransport(final String distance) throws JSONException {

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();
        p.put("MasterEntryStatus", Gc.ACTIVE);
        p.put("MasterEntryName", "Distance");
        p.put("MasterEntryValue", distance);

        param.put(p);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":


                        try {
                            JSONArray masterEntry = job.getJSONObject("result").getJSONArray("masterEntry");


                            final MasterEntryEntity masterEntryEntity = new Gson().fromJson(masterEntry.get(0).toString(), MasterEntryEntity.class);
//                            masterEntryEntity.MasterEntryId = masterEntryEntity.MasterEntryId;
//
//                            masterEntryEntity.MasterEntryValue = masterEntryEntity.MasterEntryValue;
//                            masterEntryEntity.MasterEntryStatus = Gc.ACTIVE;
//                            masterEntryEntity.MasterEntryName = Gc.DISTANCE;


                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.masterEntryModel().insertMasterEntry(masterEntryEntity);
                                }
                            }).start();
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Config.responseVolleyErrorHandler(TransportDistanceActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public class TransportDistanceAdapter extends RecyclerView.Adapter<TransportDistanceAdapter.MyViewHolder> {
        @NonNull
        @Override
        public TransportDistanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TransportDistanceAdapter.MyViewHolder holder, int position) {
            holder.tv_single_item.setText(transportDistanceList.get(position).MasterEntryValue);
            holder.tv_single_item.setTextColor(colorIs);
        }

        @Override
        public int getItemCount() {
            return transportDistanceList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                itemView.setOnClickListener(v -> {
                    showDialogForDistanceUpdate(transportDistanceList.get(getAdapterPosition()));
                });
            }
        }
    }

    private void showDialogForDistanceUpdate(MasterEntryEntity masterEntryEntity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.update);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.layout_transport_distance, null);

        final EditText et_distance = view.findViewById(R.id.et_distance);
        et_distance.setHint(R.string.enter_new_distance);

        final RadioButton rb_km = view.findViewById(R.id.rb_km);
        final RadioButton rb_miles = view.findViewById(R.id.rb_miles);

        if (!masterEntryEntity.MasterEntryValue.isEmpty()) {
            String[] separated = masterEntryEntity.MasterEntryValue.split(" ");
            et_distance.setText(separated[0]);
            if (separated.length > 1 && "KM".equals(separated[1])) {
                rb_km.setChecked(true);
            }
            if (separated.length > 1 && "MILES".equals(separated[1])) {
                rb_miles.setChecked(true);
            }
        }

        builder.setView(view);
        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> {
        });
        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
        });

        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;
            if (et_distance.getText().toString().trim().isEmpty()) {
                et_distance.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                et_distance.requestFocus();
            } else if (!(rb_km.isChecked()) && !(rb_miles.isChecked())) {
                rb_km.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
            } else {
                String distanceText = et_distance.getText().toString().trim();
                if (rb_km.isChecked())
                    distanceText = distanceText + " " + rb_km.getText();
                else if (rb_miles.isChecked())
                    distanceText = distanceText + " " + rb_miles.getText();

                try {
                    updateDistanceForTransport(distanceText, masterEntryEntity.MasterEntryId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                wantToCloseDialog = true;
            }
            if (wantToCloseDialog)
                dialog.dismiss();
        });
    }

    private void updateDistanceForTransport(String distanceText, String masterEntryId) throws JSONException {
        final JSONObject param = new JSONObject();
        param.put("MasterEntryStatus", Gc.ACTIVE);
        param.put("MasterEntryName", "Distance");
        param.put("MasterEntryValue", distanceText);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/" + new JSONObject().put("MasterEntryId", masterEntryId);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");

            Log.e("Transprot Update Log", job.toString());

            switch (code) {
                case "200":
                    try {
                        JSONArray masterEntry = job.getJSONObject("result").getJSONArray("masterEntry");

                        final MasterEntryEntity masterEntryEntity = new Gson().fromJson(masterEntry.get(0).toString(), MasterEntryEntity.class);

                        new Thread(() -> mDb.masterEntryModel().insertMasterEntry(masterEntryEntity)).start();

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> Config.responseVolleyErrorHandler(TransportDistanceActivity.this, error, findViewById(R.id.top_layout))) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

}
