package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolClassEntity {
    @PrimaryKey
    @NonNull
    public String ClassId;
    public String Session;
    public String ClassName;
    public String ClassStatus;
    public String DOE;
}
