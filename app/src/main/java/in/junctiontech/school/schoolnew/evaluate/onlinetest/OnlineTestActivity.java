package in.junctiontech.school.schoolnew.evaluate.onlinetest;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import in.junctiontech.school.R;

public class OnlineTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_test);
    }
}
