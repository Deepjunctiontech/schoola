package in.junctiontech.school.schoolnew.schoolcalender;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TimePicker;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class CreateEventCalendarActivity extends AppCompatActivity {

    MainDatabase mDb ;

    ProgressBar progressBar;

    EditText et_event_title;

    Button btn_start_date, btn_end_date,btn_start_time,btn_end_time;

    SchoolCalenderEntity event;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_start_date.setBackgroundColor(colorIs);
        btn_end_date.setBackgroundColor(colorIs);
        btn_start_time.setBackgroundColor(colorIs);
        btn_end_time.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event_calendar);
        mDb = MainDatabase.getDatabase(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progressBar);

        et_event_title = findViewById(R.id.et_event_title);
        btn_start_date = findViewById(R.id.btn_start_date);
        btn_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "startDate");
            }
        });

        btn_end_date = findViewById(R.id.btn_end_date);
        btn_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "endDate");
            }
        });

        btn_start_time = findViewById(R.id.btn_start_time);
        btn_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "startTime");


            }
        });

        btn_end_time = findViewById(R.id.btn_end_time);
        btn_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "endTime");
            }
        });

        String updateEvent = getIntent().getStringExtra("event");

        if (updateEvent!=null){
            event = new Gson().fromJson(updateEvent, SchoolCalenderEntity.class);
            et_event_title.setText(event.Title);
            btn_start_date.setText(event.startdate);
            btn_end_date.setText(event.enddate);
            btn_start_time.setText(event.StartTime);
            btn_end_time.setText(event.EndTime);

        }else {
            event = new SchoolCalenderEntity();
        }

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/5893976855", this, adContainer);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
            menu.findItem(R.id.menu_next).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save:

                if(validateInput()){
                    try {
                        saveEventToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    Boolean validateInput(){

        if("".equalsIgnoreCase(Strings.nullToEmpty(et_event_title.getText().toString().trim()))){
            Config.responseSnackBarHandler(getString(R.string.title) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if("".equalsIgnoreCase(Strings.nullToEmpty(event.startdate))){
            Config.responseSnackBarHandler(getString(R.string.start)  + " " +  getString(R.string.date) + " " +  getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if("".equalsIgnoreCase(Strings.nullToEmpty(event.StartTime))){
            Config.responseSnackBarHandler(getString(R.string.start_time) + " " +  getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if("".equalsIgnoreCase(Strings.nullToEmpty(event.enddate))){
            Config.responseSnackBarHandler(getString(R.string.end) + " " + getString(R.string.date) +" " +  getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if("".equalsIgnoreCase(Strings.nullToEmpty(event.EndTime))){
            Config.responseSnackBarHandler(getString(R.string.end_time) + " " +  getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        return true;
    }

    void saveEventToServer() throws JSONException {

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();

        p.put("title", et_event_title.getText().toString().trim());
        p.put("color","#4085b4");
        p.put("startDate",event.startdate);
        p.put("startTime",event.StartTime);
        p.put("endDate",event.enddate);
        p.put("endTime",event.EndTime);
        p.put("status",Gc.ACTIVE);

        param.put(p);

        int method = Request.Method.POST;

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "calendar/calendar"
                ;

        if(!("".equalsIgnoreCase(Strings.nullToEmpty(event.CalendarId)))){
            url = url + "/" + new JSONObject().put("CalendarId",event.CalendarId);

            method = Request.Method.PUT;
        }

            progressBar.setVisibility(View.VISIBLE);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            final ArrayList<SchoolCalenderEntity> eventList = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("calendar"),
                                            new TypeToken<List<SchoolCalenderEntity>>(){}.getType());

                            if (eventList!= null && eventList.size()>0) {

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolCalenderModel().insertSchoolCalender(eventList);
                                    }
                                }).start();

                                Intent intent = new Intent();
                                intent.putExtra("message", getString(R.string.success));
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                            //         Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(CreateEventCalendarActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        static String currentTime;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker timePicker, int hour, int min) {
            Log.i("Time picked", this.getTag());

            switch (Objects.requireNonNull(this.getTag())){
                case "startTime":

                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).btn_start_time.setText(hour+":"+min);
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).event.StartTime = hour+":"+min;
                    break;

                case "endTime":
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).btn_end_time.setText(hour+":"+min);
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).event.EndTime = hour+":"+min;

                    break;
            }

        }

    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String date = day+"-"+Gc.getMonthForInt(month)+"-"+year;

            switch (Objects.requireNonNull(this.getTag())) {
                case "startDate":
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).btn_start_date.setText(date);
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).event.startdate = date;

                    break;

                case "endDate":
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).btn_end_date.setText(date);
                    ((CreateEventCalendarActivity) Objects.requireNonNull(getActivity())).event.enddate = date;

                    break;
            }
        }
    }
}
