package in.junctiontech.school.schoolnew.feesetup.feetype.classfee;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class ClassFeeActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

//    Spinner sp_create_class_fee_class, sp_create_class_fee_section;
//    ArrayAdapter<String> classAdapter, sectionAdapter;
    TextView tv_manage_fee_total_session,tv_manage_fee_total_quarterly,tv_manage_fee_total_monthly;
    Button btn_manage_fee_add_new_fee;
    LinearLayout ll_fee_list;
    ArrayList<FeeTypeEntity> feeTypeList = new ArrayList<>();

    //Data holders for Spinners
    ArrayList<SchoolClassEntity> schoolClassEntityList = new ArrayList<>();
    ArrayList<SchoolClassSectionEntity> schoolClassSectionEntityList = new ArrayList<>();
    ArrayList<String> classNameList = new ArrayList<>(), classIdList = new ArrayList<>();
    ArrayList<String> sectionIdList = new ArrayList<>(), sectionNameList = new ArrayList<>();
    String selectedclassid;


    CheckBox cb_all_section_same_fees;
    ArrayList<SchoolClassSectionEntity> allSections = new ArrayList<>();

    Button btn_class_fee_class;
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    ArrayList<String> sectionList = new ArrayList<>();
    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    int selectedSection;


    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_class_fee_class.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = MainDatabase.getDatabase(this);
        // Initialization of Views Begins
        setContentView(R.layout.fragment_create_fee);

        progressBar = findViewById(R.id.progressBar);

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4939987831", this, adContainer);
        }

        if(Gc.getSharedPreference(Gc.CLASSFEESEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        // *  ----------------------New Logic for class selection ---------
        //__________________________________________________________________
        //__________________________________________________________________
        //__________________________________________________________________
        cb_all_section_same_fees = findViewById(R.id.cb_all_section_same_fees);
        cb_all_section_same_fees.setOnClickListener(view -> {
            if(cb_all_section_same_fees.isChecked()){
                mDb.schoolClassSectionModel()
                        .getClassSectionLive(classNameSectionList.get(selectedSection).ClassId)
                        .observe(ClassFeeActivity.this, schoolClassSectionEntities -> {
                            allSections.addAll(schoolClassSectionEntities);

                            mDb.schoolClassSectionModel()
                                    .getClassSectionLive(classNameSectionList.get(selectedSection).ClassId).removeObservers(ClassFeeActivity.this);
                        });
            }else {
                allSections.clear();
            }
        });
        btn_class_fee_class = findViewById(R.id.btn_class_fee_class);

        classListBuilder = new AlertDialog.Builder(ClassFeeActivity.this);
        classSectionAdapter =
                new ArrayAdapter<>(ClassFeeActivity.this, android.R.layout.select_dialog_singlechoice);




        btn_class_fee_class.setOnClickListener(view -> {

            classListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            classListBuilder.setAdapter(classSectionAdapter, (dialogInterface, i) -> {
                btn_class_fee_class.setText(classNameSectionList.get(i).ClassName + " "
                        + classNameSectionList.get(i).SectionName);
                selectedSection = i;

                tv_manage_fee_total_monthly.setText("0");
                tv_manage_fee_total_session.setText("0");

                ArrayList<ClassFeesEntity> classFeesEntities;
                classFeesEntities = (ArrayList<ClassFeesEntity>) mDb.classFeesModel()
                        .getFeeforClassSection(classNameSectionList.get(i).SectionId);

                try {
                    getClassFees(classNameSectionList.get(i).SectionId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                buildFeeListWhenClassFeeDoesNotExist(classFeesEntities);
                calculateFeesSumForDisplay();
            });

            AlertDialog dialog = classListBuilder.create();
            dialog.show();
        });


        //__________________________________________________________________
        //__________________________________________________________________
        //__________________________________________________________________

        // *  ----------------------New Logic for class selection ----------


//        sp_create_class_fee_class = findViewById(R.id.sp_create_class_fee_class);
//        sp_create_class_fee_section = findViewById(R.id.sp_create_class_fee_section);

        tv_manage_fee_total_session = findViewById(R.id.tv_manage_fee_total_session);
        tv_manage_fee_total_monthly = findViewById(R.id.tv_manage_fee_total_monthly);
        tv_manage_fee_total_quarterly = findViewById(R.id.tv_manage_fee_total_quarterly);

        btn_manage_fee_add_new_fee= findViewById(R.id.btn_manage_fee_add_new_fee);

        btn_manage_fee_add_new_fee.setOnClickListener(view -> {
//                Log.i("Classid" , classNameList.get(sp_create_class_fee_class.getSelectedItemPosition()));
//                Log.i("sectionid" , sectionNameList.get(sp_create_class_fee_section.getSelectedItemPosition()));

            try {
                createClassFees(classNameSectionList.get(selectedSection).SectionId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        ll_fee_list = findViewById(R.id.ll_class_fee_fee_list);

        setColorApp();

        // Initialization of View Ends

        // Spinner data assignment
//        classAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1, this.classNameList);
//        classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
//        sp_create_class_fee_class.setAdapter(classAdapter);
//
//        sectionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, this.sectionNameList );
//        sectionAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
//        sp_create_class_fee_section.setAdapter(sectionAdapter);
//
//        // Set onClick Listeners Begins
//        sp_create_class_fee_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                HashMap<String,String> setDefaults = new HashMap<>();
//                setDefaults.put(Gc.DEFAULTCLASS,classIdList.get(i));
//                Gc.setSharedPreference(setDefaults,ClassFeeActivity.this);
//
//                schoolClassSectionEntityList = (ArrayList<SchoolClassSectionEntity>) mDb.schoolClassSectionModel()
//                        .getClassSections(classIdList.get(i));
//
//                sectionNameList.clear();
//                sectionIdList.clear();
//                for(int k = 0; k < schoolClassSectionEntityList.size(); k++){
//                    sectionNameList.add(schoolClassSectionEntityList.get(k).SectionName);
//                    sectionIdList.add(schoolClassSectionEntityList.get(k).SectionId);
//                }
//                if (sectionNameList.size() > 0){
//                    sectionAdapter.notifyDataSetChanged();
//                    sp_create_class_fee_section.setSelection(0);
//                    View itemView = (View)sp_create_class_fee_section.getChildAt(0);
//                    long itemId = sp_create_class_fee_section.getAdapter().getItemId(0);
//
//
//                    sp_create_class_fee_section.performItemClick(itemView,0,itemId);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        sp_create_class_fee_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                tv_manage_fee_total_monthly.setText("0");
//                tv_manage_fee_total_session.setText("0");
//
//                ArrayList<ClassFeesEntity> classFeesEntities = new ArrayList<>();
//                classFeesEntities = (ArrayList<ClassFeesEntity>) mDb.classFeesModel().getFeeforClassSection(sectionIdList.get(i));
//
//                    buildFeeListWhenClassFeeDoesNotExist(classFeesEntities);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });

    }

    void buildFeeListWhenClassFeeExists( ArrayList<ClassFeesEntity> classFeesEntities){
        ll_fee_list.removeAllViews();
        for(int i = 0; i < classFeesEntities.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View view = layoutInflater.inflate(R.layout.item_add_manage_fee, null);

            TextView tv_item_fee_name = view.findViewById(R.id.tv_item_fee_name);
            tv_item_fee_name.setText(classFeesEntities.get(i).FeeType);

            TextView tv_item_fee_frequency = view.findViewById(R.id.tv_item_fee_frequency);
            tv_item_fee_frequency.setText(classFeesEntities.get(i).Frequency);

            TextView tv_item_feetypeid = view.findViewById(R.id.tv_item_feetypeid);
            tv_item_feetypeid.setText(classFeesEntities.get(i).FeeTypeID);

            TextView tv_item_feeid = view.findViewById(R.id.tv_item_feeid);
            tv_item_feeid.setText(classFeesEntities.get(i).FeeId);

            EditText et_item_amount = view.findViewById(R.id.et_item_amount);
            et_item_amount.setText(classFeesEntities.get(i).Amount);

            calculateFeesSumForDisplay();


            et_item_amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    calculateFeesSumForDisplay();

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            ll_fee_list.addView(view);
        }

    }

    void buildFeeListWhenClassFeeDoesNotExist(  ArrayList<ClassFeesEntity> classFeesEntities){
        ll_fee_list.removeAllViews();

        for(int i = 0; i < feeTypeList.size(); i++) {

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View view = layoutInflater.inflate(R.layout.item_add_manage_fee, null);
            TextView tv_item_fee_name = view.findViewById(R.id.tv_item_fee_name);
            tv_item_fee_name.setText(feeTypeList.get(i).FeeType);

            TextView tv_item_fee_frequency = view.findViewById(R.id.tv_item_fee_frequency);
            tv_item_fee_frequency.setText(feeTypeList.get(i).Frequency);

            TextView tv_item_feetypeid = view.findViewById(R.id.tv_item_feetypeid);
            tv_item_feetypeid.setText(feeTypeList.get(i).FeeTypeID);

            EditText et_item_amount = view.findViewById(R.id.et_item_amount);

            for(int k = 0; k < classFeesEntities.size(); k++) {
                if ( (feeTypeList.get(i).FeeTypeID).equals(classFeesEntities.get(k).FeeTypeID))
                    et_item_amount.setText(classFeesEntities.get(k).Amount);
            }

            et_item_amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    calculateFeesSumForDisplay();

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            ll_fee_list.addView(view);
        }
    }

    void calculateFeesSumForDisplay(){
        int sumsession = 0;
        int summonthly = 0;
        int sumquarterly = 0;
        for (int k = 0; k < ll_fee_list.getChildCount(); k++){
            View view1 = ll_fee_list.getChildAt(k);
            String enteredamount = ((EditText)view1.findViewById(R.id.et_item_amount)).getText().toString();
            String frequency = ((TextView) view1.findViewById(R.id.tv_item_fee_frequency)).getText().toString();
            if ("Monthly".equalsIgnoreCase(frequency)) {
                summonthly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            }else if ("Session".equalsIgnoreCase(frequency)){
                sumsession += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            }else if ("Quarterly".equalsIgnoreCase(frequency))
                sumquarterly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
        }
        tv_manage_fee_total_session.setText(sumsession+"");
        tv_manage_fee_total_quarterly.setText(sumquarterly+"");
        tv_manage_fee_total_monthly.setText(summonthly+"");
    }

    JSONObject buildFeeListforCreate() throws JSONException {

        JSONObject finaljobj = new JSONObject();

        JSONArray feearr = new JSONArray();

        for (int k = 0; k < ll_fee_list.getChildCount(); k++){
            JSONObject feejobj = new JSONObject();
            String feestatus = "Active";
            View view1 = ll_fee_list.getChildAt(k);
            String enteredamount = ((EditText)view1.findViewById(R.id.et_item_amount)).getText().toString();
            String frequency = ((TextView) view1.findViewById(R.id.tv_item_fee_frequency)).getText().toString();
            String feetypeid = ((TextView) view1.findViewById(R.id.tv_item_feetypeid)).getText().toString();
            if ("".equals(enteredamount))  // || "0".equals(enteredamount))
            { feestatus = "Inactive"; enteredamount = 0+"";}
            else {feestatus = "Active";}

                feejobj.put("FeeTypeID",feetypeid);
                feejobj.put("Amount",enteredamount);
                feejobj.put("FeeStatus",feestatus);
                feearr.put(feejobj);

        }

        if(feearr.length()<1){
            return null;
        }

        JSONArray sectionfeejarr = new JSONArray();

        if(cb_all_section_same_fees.isChecked()){
            for(SchoolClassSectionEntity section : allSections){
                JSONObject sectionjobj = new JSONObject();
                sectionjobj.put("SectionId", section.SectionId);
                sectionjobj.put("FeeType", feearr);
                sectionfeejarr.put(sectionjobj);
            }

        }else {

            JSONObject sectionjobj = new JSONObject();
            sectionjobj.put("SectionId", classNameSectionList.get(selectedSection).SectionId);
            sectionjobj.put("FeeType", feearr);

            sectionfeejarr.put(sectionjobj);
        }
            finaljobj.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
            finaljobj.put("Section", sectionfeejarr);

            return finaljobj;

    }

    JSONArray buildFeeListforUpdate() throws JSONException{

        JSONArray feearr = new JSONArray();

        for (int k = 0; k < ll_fee_list.getChildCount(); k++){
            JSONObject feejobj = new JSONObject();
            String feestatus = "Active";
            View view1 = ll_fee_list.getChildAt(k);

            String enteredamount = ((EditText)view1.findViewById(R.id.et_item_amount)).getText().toString();
            String feetypeid = ((TextView) view1.findViewById(R.id.tv_item_feetypeid)).getText().toString();
            String feeid = ((TextView) view1.findViewById(R.id.tv_item_feeid)).getText().toString();

            if ("".equals(enteredamount) || "0".equals(enteredamount))
            { feestatus = "Inactive"; enteredamount = 0+"";}
            else {feestatus = "Active";}

            feejobj.put("FeeTypeID",feetypeid);
            feejobj.put("Amount",enteredamount);
            feejobj.put("FeeStatus",feestatus);
            feejobj.put("FeeId",feeid);
            feearr.put(feejobj);
        }

        return feearr;
    }

    @Override
    protected void onResume() {
        super.onResume();


        mDb.feeTypeModel().getFeeTypesExcludeTransport().observe(this, feeTypeEntities -> {
            feeTypeList.clear();
            for ( int i = 0; i < feeTypeEntities.size(); i++){
                feeTypeList.add(feeTypeEntities.get(i));
            }

        });

         mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this)).observe(this, new Observer<List<ClassNameSectionName>>() {
             @Override
             public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                 classNameSectionList.clear();
                 classNameSectionList.addAll(classNameSectionNames);
                 sectionList.clear();
                 for(int i = 0; i < classNameSectionList.size(); i++){
                     sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
                 }
                 classSectionAdapter.clear();
                 classSectionAdapter.addAll(sectionList);
                 classSectionAdapter.notifyDataSetChanged();
             }
         });

    }

    private void resetSpinners(){
        schoolClassEntityList.clear();
        schoolClassSectionEntityList.clear();
        classNameList.clear();
        classIdList.clear();
        sectionNameList.clear();
        sectionIdList.clear();
    }
    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this))
                .removeObservers(this);
        mDb.feeTypeModel().getFeeTypesExcludeTransport().removeObservers(this);
    }

    @Override
    public void onBackPressed() {
        if(Gc.getSharedPreference(Gc.CLASSFEESEXISTS,this).equals(Gc.EXISTS)) {
            super.onBackPressed();
            mDb.schoolClassModel()
                    .getSchoolClassesLive(Gc.getSharedPreference(Gc.APPSESSION, this))
                    .removeObservers(this);
            mDb.feeTypeModel().getFeeTypesExcludeTransport().removeObservers(this);
        }else {
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.activity_manage_class_fees), getString(R.string.class_fee)+" : "+ getString(R.string.fees_not_available),
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(4000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        }
    }

    void createClassFees(String sectionid) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject param = buildFeeListforCreate();

        if(param == null){
            Config.responseSnackBarHandler(getString(R.string.fees_not_available),
                    findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_green);
            return;
        }
//        p.put("Frequency", "Monthly");

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/classFees"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try{
                        JSONObject resultjobj = job.getJSONObject("result");

                        JSONArray classFeesjarr = resultjobj.getJSONArray("classFees");

                        saveClassFeeToDataBase(classFeesjarr, sectionid);

                        Config.responseSnackBarHandler(getString(R.string.success),
                                findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_green);
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_blue);
            }
        }, error -> {
        progressBar.setVisibility(View.GONE);
        Config.responseVolleyErrorHandler(ClassFeeActivity.this,error,findViewById(R.id.activity_manage_class_fees));

    }){
        @Override
        public Map<String, String> getHeaders() {
            HashMap<String, String> headers = new HashMap<>();

            headers.put("APPKEY", Gc.APPKEY);
            headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
            headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
            headers.put("Content-Type", Gc.CONTENT_TYPE);
            headers.put("DEVICE", Gc.DEVICETYPE);
            headers.put("DEVICEID",Gc.id(getApplicationContext()));
            headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
            headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
            headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
            headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

            return headers;
        }
        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
        @Override
        public byte[] getBody() {
            try {
                return param == null ? null : param.toString().getBytes("utf-8");
            } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                return null;
            }
        }
    };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
                AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void getClassFees(String sectionid) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/classFees/"
                + new JSONObject().put("fee.SectionId", sectionid)
                ;


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try{
                        JSONObject resultjobj = job.getJSONObject("result");

                        JSONArray classFeesjarr = resultjobj.getJSONArray("classFees");

                        saveClassFeeToDataBase(classFeesjarr, sectionid);

                        Config.responseSnackBarHandler(getString(R.string.success),
                                findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_green);
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;



                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(ClassFeeActivity.this,error,findViewById(R.id.activity_manage_class_fees));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, 0));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    void updateClassFees() throws JSONException {

        final JSONArray param = buildFeeListforUpdate();
//        final JSONObject p = new JSONObject();
//
////        p.put("Frequency", "Monthly");
//
//
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/classFees"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");




                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }).start();
                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Log.i("Class fee not updated ", job.optString("message"));
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.activity_manage_class_fees),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Config.responseVolleyErrorHandler(ClassFeeActivity.this,error,findViewById(R.id.activity_manage_class_fees));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void saveClassFeeToDataBase(JSONArray classFeesjarr, String sectionid) {
        if (classFeesjarr.length() > 0) {
            HashMap<String, String> setDefaults = new HashMap<>();
            setDefaults.put(Gc.CLASSFEESEXISTS, Gc.EXISTS);
            Gc.setSharedPreference(setDefaults, this);
            final ArrayList<ClassFeesEntity> classFeesEntities = MapModelToEntity.mapClassFeeModelToEntity(classFeesjarr);

            new Thread(() -> {
                mDb.classFeesModel().deleteClassFeeForSection(sectionid);
                mDb.classFeesModel().insertClassFee(classFeesEntities);
            }).start();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
//        menu.findItem(R.id.action_save).setVisible(false);

        if(Gc.getSharedPreference(Gc.CLASSFEESEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            menu.findItem(R.id.menu_next).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;

            case R.id.action_save:
                try {
                    if (classNameSectionList.size() > 0) {
                        createClassFees(classNameSectionList.get(selectedSection).SectionId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
