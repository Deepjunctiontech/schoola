package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserPermissionsDao {
    @Query("select permission from UserPermissionsEntity")
    LiveData<List<String>> getUserPermissions();

    @Insert(onConflict = REPLACE)
    void insertUserPermission(List<UserPermissionsEntity> userPermissionsEntity);

    @Query("delete from UserPermissionsEntity")
    void deleteAll();
}
