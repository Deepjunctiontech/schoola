package in.junctiontech.school.schoolnew.transport.fee;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.common.Gc;

public class TransportFeeSetupActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;
    RadioButton rb_transport_fixed,
            rb_transport_distance_wise,

    rb_transport_fee_monthly,
            rb_transport_fee_quarterly,
            rb_transport_fee_session;

    RadioGroup rg_transport_frequency,
            rg_transport_fee_type;

    FloatingActionButton fb_transport_setup_next;


    static Boolean feeExists = true;
    static Boolean feeChanged = false;


    String feetypes;
    String feeTypeID, feeType;

    String[] monthList;
    ArrayList<String> excludedMonths = new ArrayList<>();

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport_fee_setup);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);


        rb_transport_fixed = findViewById(R.id.rb_transport_fixed);
        rb_transport_distance_wise = findViewById(R.id.rb_transport_distance_wise);

        rg_transport_fee_type = findViewById(R.id.rg_transport_fee_type);
        rg_transport_frequency = findViewById(R.id.rg_transport_frequency);

        fb_transport_setup_next = findViewById(R.id.fb_transport_setup_next);
        fb_transport_setup_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateInput()) return;
                if (!transportFeeEdited()) return;
                if (rb_transport_distance_wise.isChecked()) {
                    Intent intent = new Intent(TransportFeeSetupActivity.this, TransportDistanceActivity.class);
                    intent.putExtra("type", "distancewise");
                    intent.putExtra("feetypeid", feeTypeID);
                    intent.putExtra("fees", feetypes);
                    startActivity(intent);
                }

                if (rb_transport_fixed.isChecked()) {
                    Intent intent = new Intent(TransportFeeSetupActivity.this, TransportFeeAmountActivity.class);
                    intent.putExtra("type", "fixed");
                    intent.putExtra("fees", feetypes);
                    intent.putExtra("feetypeid", feeTypeID);
                    startActivity(intent);
                }
            }
        });


        monthList = Gc.findMonthsInSession(Gc.getSharedPreference(Gc.CURRENTSESSIONSTART, this));

        setupTransportFeeType();
        setupTransportFeeFrequency();

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1971522642", this, adContainer);
        }
//
//        if(rb_transport_fixed.isChecked()) {
//            ll_transport_fee_amount_fixed.setVisibility(View.VISIBLE);
//            fb_distance_add.setVisibility(View.GONE);
//        }
//
//        if (rb_transport_distance_wise.isChecked()){
//            ll_transport_fee_amount_fixed.setVisibility(View.GONE);
//            fb_distance_add.setVisibility(View.VISIBLE);
//            mRecyclerView.setVisibility(View.VISIBLE);
//
//
//        }


//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }


    void getExcludedMonths() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.exclude_months);

        final boolean[] checkedItems = new boolean[monthList.length];

        for (int i = 0; i < monthList.length; i++) {
            if (excludedMonths.contains(monthList[i]))
                checkedItems[i] = false;
            else
                checkedItems[i] = true;
        }

        builder.setMultiChoiceItems(monthList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                if (b) {
                    if (excludedMonths.contains(monthList[i]))
                        excludedMonths.remove(monthList[i]);
                } else {
                    if (!(excludedMonths.contains(monthList[i])))
                        excludedMonths.add(monthList[i]);

                }
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void getTransportFeeType() {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject param = new JSONObject();

        try {
            param.put("session", Gc.getSharedPreference(Gc.APPSESSION, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/transportFee/"
                + param;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");
                progressBar.setVisibility(View.GONE);
                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            feetypes = resultjobj.optString("Fees");

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.TRANSPORTFEETYPE, job.getJSONObject("result").optString("Type"));
                            Gc.setSharedPreference(setDefaults, TransportFeeSetupActivity.this);

                            ModelTransportFeeType modelTransportFeeType = new Gson().fromJson(job.optString("result"), ModelTransportFeeType.class);
                            ArrayList<FeeTypeModel> feeTypeModel = new Gson().fromJson(resultjobj.optString("Fees"), new TypeToken<List<FeeTypeModel>>() {
                            }.getType());

                            ArrayList<DistanceModel> distanceModels = new Gson().fromJson(resultjobj.optString("Distance"), new TypeToken<List<DistanceModel>>() {
                            }.getType());
                            Log.i("response ", new Gson().toJson(modelTransportFeeType));

                            setupUiForTransport(modelTransportFeeType, feeTypeModel, distanceModels);


                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(TransportFeeSetupActivity.this, error, findViewById(R.id.top_layout));


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void setupTransportFeeType() {
        rb_transport_fixed.setOnClickListener(view -> {
            feeChanged = true;
        });

        rb_transport_distance_wise.setOnClickListener(view -> {
            if (rb_transport_distance_wise.isChecked()) {
                feeChanged = true;
            }
        });
    }

    void setupTransportFeeFrequency() {

        rb_transport_fee_session = findViewById(R.id.rb_transport_fee_session);
        rb_transport_fee_session.setOnClickListener(view -> {
            feeChanged = true;
        });
        rb_transport_fee_quarterly = findViewById(R.id.rb_transport_fee_quarterly);
        rb_transport_fee_quarterly.setVisibility(View.GONE);
        rb_transport_fee_quarterly.setOnClickListener(view -> {
            feeChanged = true;
        });

        rb_transport_fee_monthly = findViewById(R.id.rb_transport_fee_monthly);
        rb_transport_fee_monthly.setOnClickListener(view -> {
            feeChanged = true;
            getExcludedMonths();
        });


        if (Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toUpperCase().equals(Gc.PLANTYPEFREE)) {
            LinearLayout linearLayout3 =findViewById(R.id.linearLayout3);
            linearLayout3.setVisibility(View.GONE);
            rb_transport_fee_session.setChecked(true);
        }
    }

    void setupUiForTransport(ModelTransportFeeType modelTransportFeeType,
                             ArrayList<FeeTypeModel> feeTypeModels,
                             ArrayList<DistanceModel> distanceModels) {

        feeType = modelTransportFeeType.FeeType;
        feeTypeID = modelTransportFeeType.FeeTypeID;

        switch (modelTransportFeeType.Frequency) {
            case "Monthly":
                rb_transport_fee_monthly.setChecked(true);
                break;

            case "Quarterly":
                rb_transport_fee_quarterly.setChecked(true);
                break;

            case "Session":
                rb_transport_fee_session.setChecked(true);
                break;

            default:
                feeExists = false;
        }

        switch (modelTransportFeeType.Type) {
            case "DistanceWise":
                rb_transport_distance_wise.setChecked(true);

                break;
            case "Fixed":
                rb_transport_fixed.setChecked(true);

                break;
            default:
                feeExists = false;
        }
        excludedMonths.clear();
        excludedMonths.addAll(Arrays.asList(modelTransportFeeType.ExMonths));

    }

    JSONObject jsonForSaveTransportFee() {
        JSONObject param = new JSONObject();
        try {
            param.put("dataType", "transportFeeType");
            if (rb_transport_fixed.isChecked()) {
                param.put("Type", "Fixed");
            } else if (rb_transport_distance_wise.isChecked()) {
                param.put("Type", "DistanceWise");
            }

            if (rb_transport_fee_session.isChecked()) {
                param.put("Frequency", "Session");
            } else if (rb_transport_fee_quarterly.isChecked()) {
                param.put("Frequency", "Quarterly");
            } else if (rb_transport_fee_monthly.isChecked()) {
                param.put("Frequency", "Monthly");
            }
            param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));

            JSONArray exmontharr = new JSONArray();
            for (String s : excludedMonths) {
                exmontharr.put(s);
            }

            if (!(excludedMonths.isEmpty())) {
                param.put("monthYear", exmontharr);
            }
            return param;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    void saveTransportFee() {
//        progressBar.setVisibility(View.VISIBLE);

        if (!validateInput())
            return;

        final JSONObject param = jsonForSaveTransportFee();
        if (param == null) {
            Config.responseSnackBarHandler("Something is wrong",
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return;

        }

//        p.put("Frequency", "Monthly");
//        {"dataType":"transportFeeType","Type":"DistanceWise","Frequency":"Monthly","monthYear":["May-2018","Aug-2018"],"Session":"2018-2019"}

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/transportFee";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                switch (job.optString("code")) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.TRANSPORTFEETYPE, job.getJSONObject("result").optString("Type"));
                            Gc.setSharedPreference(setDefaults, TransportFeeSetupActivity.this);

                            feeChanged = false;

                            new Thread(() -> {
                                mDb.transportFeeAmountModel().deleteAll();
                            }).start();

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(TransportFeeSetupActivity.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void updateTransportDistance(String distance, String masterentryid) {

        final JSONObject param = new JSONObject();
        final JSONObject p = new JSONObject();


        try {
            param.put("MasterEntryStatus", Gc.ACTIVE);
            param.put("MasterEntryName", "Distance");
            param.put("MasterEntryValue", distance);

            p.put("MasterEntryId", masterentryid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case "200":
                    try {
                        JSONObject resultjobj = job.getJSONObject("result");


                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);

                        getTransportFeeType();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            Config.responseVolleyErrorHandler(TransportFeeSetupActivity.this, error, findViewById(R.id.top_layout));
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void updateTransportDistanceDialog(final TransportDistanceAmount transportDistanceAmount) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.update);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.layout_transport_distance, null);

        final EditText et_distance = view.findViewById(R.id.et_distance);
        et_distance.setHint(R.string.enter_new_distance);

        final RadioButton rb_km = view.findViewById(R.id.rb_km);
        final RadioButton rb_miles = view.findViewById(R.id.rb_miles);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.i("Positive button called", "");
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wantToCloseDialog = false;

                if (et_distance.getText().toString().trim().isEmpty()) {
                    et_distance.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else if (!(rb_km.isChecked()) && !(rb_miles.isChecked())) {
                    rb_km.setError(getString(R.string.error_field_required), getResources().getDrawable(R.drawable.ic_alert));
                } else {

                    Log.i("dISTANCE", et_distance.getText().toString());
                    String distancetext = et_distance.getText().toString().trim();
                    if (rb_km.isChecked())
                        distancetext = distancetext + " " + rb_km.getText();
                    else if (rb_miles.isChecked())
                        distancetext = distancetext + " " + rb_miles.getText();

                    updateTransportDistance(distancetext, transportDistanceAmount.MasterEntryId);
                    wantToCloseDialog = true;
                }
                if (wantToCloseDialog)
                    dialog.dismiss();
            }
        });


    }

    Boolean validateInput() {
        if (rg_transport_fee_type.getCheckedRadioButtonId() == -1) {
            Config.responseSnackBarHandler(getString(R.string.select_transport_type),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        if (rg_transport_frequency.getCheckedRadioButtonId() == -1) {
            Config.responseSnackBarHandler(getString(R.string.select_fee_frequency),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        return true;
    }

    Boolean transportFeeEdited() {
        if (feeChanged) {
            Config.responseSnackBarHandler(getString(R.string.first_save_transport_fee),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        } else return true;
    }

    @Override
    protected void onResume() {
        getTransportFeeType();

        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                saveTransportFee();
                break;
            case R.id.menu_next:

                if (!validateInput()) return false;
                if (!transportFeeEdited()) return false;
                if (rb_transport_distance_wise.isChecked()) {
                    Intent intent = new Intent(TransportFeeSetupActivity.this, TransportDistanceActivity.class);
                    intent.putExtra("type", "distancewise");
                    intent.putExtra("feetypeid", feeTypeID);
                    intent.putExtra("fees", feetypes);
                    startActivity(intent);
                }

                if (rb_transport_fixed.isChecked()) {
                    Intent intent = new Intent(TransportFeeSetupActivity.this, TransportFeeAmountActivity.class);
                    intent.putExtra("type", "fixed");
                    intent.putExtra("fees", feetypes);
                    intent.putExtra("feetypeid", feeTypeID);
                    startActivity(intent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

class TransportDistanceAmount {
    public String MasterEntryId;
    public String MasterEntryValue;
    public String FeeType;
    public String Amount;
}


