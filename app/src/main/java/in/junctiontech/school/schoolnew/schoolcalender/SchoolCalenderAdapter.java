package in.junctiontech.school.schoolnew.schoolcalender;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class SchoolCalenderAdapter extends RecyclerView.Adapter<SchoolCalenderAdapter.MyViewHolder>{
    private ArrayList<SchoolCalenderEntity> schoolCalenderEntities = new ArrayList<>() ;
    private ArrayList<SchoolCalenderEntity> searchItems = new ArrayList<>() ;

    private Context context;

    public SchoolCalenderAdapter(Context context, ArrayList<SchoolCalenderEntity> schoolCalenderEntities){
        this.context = context;
        this.schoolCalenderEntities = schoolCalenderEntities;
        searchItems.addAll(schoolCalenderEntities);
    }

    @NonNull
    @Override
    public SchoolCalenderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar_event_layout, parent, false);

        return new MyViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull SchoolCalenderAdapter.MyViewHolder holder, int position) {
        holder.tv_item_calendar_event_data.setText(schoolCalenderEntities.get(position).Title);
        holder.tv_item_calendar_event_data_date.setText(
                schoolCalenderEntities.get(position).startdate
                        + context.getResources().getString(R.string.start_time)
                        + schoolCalenderEntities.get(position).StartTime
                        + " : "
                        + schoolCalenderEntities.get(position).enddate
                        + context.getResources().getString(R.string.end_time)
                        + schoolCalenderEntities.get(position).EndTime
        ) ;

        holder.tv_item_calendar_event_data.setBackgroundColor(Color.parseColor(schoolCalenderEntities.get(position).Color));

    }

    @Override
    public int getItemCount() {
        return schoolCalenderEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_item_calendar_event_data,tv_item_calendar_event_data_date;
        public MyViewHolder(View itemView) {
            super(itemView);

            tv_item_calendar_event_data = itemView.findViewById(R.id.tv_item_calendar_event_data);
            tv_item_calendar_event_data.setVisibility(View.VISIBLE);
            tv_item_calendar_event_data_date = itemView.findViewById(R.id.tv_item_calendar_event_data_date);
            tv_item_calendar_event_data_date.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Gc.getSharedPreference(Gc.CREATEEVENTALLOWED, context).equalsIgnoreCase(Gc.TRUE)) {
                        AlertDialog.Builder choices = new AlertDialog.Builder(context);
                        String[] aa = new String[]
                                {context.getResources().getString(R.string.update),
                                        context.getResources().getString(R.string.delete)
                                };

                        choices.setItems(aa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which) {
                                    case 0:
                                        ((SchoolCalenderActivity) context).updateEventCalander(schoolCalenderEntities.get(getAdapterPosition()));
                                        break;
                                    case 1:
                                        ((SchoolCalenderActivity) context).deleteEventCalander(schoolCalenderEntities.get(getAdapterPosition()));
                                        break;
                                }

                            }
                        });
                        choices.show();
                    }else{
                        //TODO show details of calender
                    }
                }
            });
        }
    }
    public void filter(String text) {

        schoolCalenderEntities.clear();
        if(text.isEmpty()){
            schoolCalenderEntities.addAll(searchItems);
        } else{
            text = text.toLowerCase();
            for(SchoolCalenderEntity item: searchItems){
                if(item.Title.toLowerCase().contains(text)
                        || item.startdate.toLowerCase().contains(text)
                        || item.enddate.toLowerCase().contains(text)
                        ){
                    schoolCalenderEntities.add(item);
                }
            }
        }
            notifyDataSetChanged();
    }

    public void updateSearchItems(List<SchoolCalenderEntity> schoolCalenderEntities){
        searchItems.clear();
        searchItems.addAll((ArrayList<SchoolCalenderEntity>)schoolCalenderEntities);
    }

}
