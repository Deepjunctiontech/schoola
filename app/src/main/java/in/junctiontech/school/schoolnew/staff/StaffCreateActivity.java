package in.junctiontech.school.schoolnew.staff;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class StaffCreateActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;

    Button btn_create_staff_user_type;
    ArrayList<MasterEntryEntity> masterEntryUserTypes = new ArrayList<>();
    ArrayList<String> userTypes = new ArrayList<>();
    AlertDialog.Builder userTypesListBuilder;
    ArrayAdapter<String> userTypesListAdapter;
    int selectedUserType;

    Button btn_create_staff_staff_position;
    ArrayList<MasterEntryEntity> masterEntryStaffPositions = new ArrayList<>();
    ArrayList<String> staffPositions = new ArrayList<>();
    AlertDialog.Builder staffPositionListBuilder;
    ArrayAdapter<String> staffPositionListAdapter;
    int selectedStaffPosition;

    Button btn_gender;
    ArrayList<MasterEntryEntity> masterEntryGender = new ArrayList<>();
    ArrayList<String> gender = new ArrayList<>();
    AlertDialog.Builder genderListBuilder;
    ArrayAdapter<String> genderListAdapter;
    int selectedGender = 0;

    Button btn_date_of_joining, btn_date_of_birth;


    EditText et_create_staff_new_staff_name;
    EditText et_create_staff_new_staff_email;
    EditText et_create_staff_country_code;
    EditText et_create_staff_new_staff_mobile;
    EditText et_create_staff_new_staff_user_name;
    EditText et_create_staff_new_staff_password;
    EditText et_create_staff_father_name, et_create_staff_mother_name, et_create_staff_present_address;

    CircleImageView staff_profile_photo;
    private boolean isProfileChanged;

    SwitchCompat switch_compact_create_staff_send_sms;
    Button btn_create_new_staff;

    SchoolStaffEntity staffUpdate;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_create_staff_user_type.setBackgroundColor(colorIs);
        btn_create_staff_staff_position.setBackgroundColor(colorIs);
        btn_create_new_staff.setBackgroundColor(colorIs);
        btn_date_of_joining.setBackgroundColor(colorIs);
        btn_date_of_birth.setBackgroundColor(colorIs);
        btn_gender.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_staff);

        mDb = MainDatabase.getDatabase(this);

        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        selectUserTypeDialog();
        selectStaffPositionDialog();
        selectGenderDialog();

        initializeViews();

        btn_create_new_staff = findViewById(R.id.btn_create_new_staff);
        btn_create_new_staff.setOnClickListener(view -> {
            if (validateInput()) {
                try {
                    createStaff();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        initializeDateOfJoining();

        if (!(Strings.nullToEmpty(getIntent().getStringExtra("staff")).equalsIgnoreCase(""))) {
            staffUpdate = new Gson().fromJson(getIntent().getStringExtra("staff"), SchoolStaffEntity.class);
            btn_create_staff_user_type.setVisibility(View.GONE);
            btn_create_staff_staff_position.setVisibility(View.GONE);

            findViewById(R.id.ll_create_staff_create_user).setVisibility(View.GONE);
            btn_create_new_staff.setVisibility(View.GONE);

            et_create_staff_new_staff_name.setText(staffUpdate.StaffName);
            et_create_staff_new_staff_email.setText(staffUpdate.StaffEmail);
            HashMap<String, String> split = Gc.splitPhoneNumber(Strings.nullToEmpty(staffUpdate.StaffMobile));
            et_create_staff_new_staff_mobile.setText(split.get("phone"));
            et_create_staff_country_code.setText(split.get("ccode"));

            et_create_staff_father_name.setText(staffUpdate.StaffFName);
            et_create_staff_mother_name.setText(staffUpdate.StaffMName);
            et_create_staff_present_address.setText(staffUpdate.StaffPresentAddress);
            btn_date_of_birth.setText(staffUpdate.StaffDOB);
            btn_date_of_joining.setText(staffUpdate.StaffDOJ);

            if (!(Strings.nullToEmpty(staffUpdate.staffGenderValue).equalsIgnoreCase(""))){
                btn_gender.setText(staffUpdate.staffGenderValue);
            }

            if (!(Strings.nullToEmpty(staffUpdate.staffProfileImage).equalsIgnoreCase(""))){
                Glide.with(this)
                        .load(staffUpdate.staffProfileImage)
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_single))
                        .apply(RequestOptions.errorOf(R.drawable.ic_single))
                        .apply(RequestOptions.circleCropTransform())
                        .into(staff_profile_photo);
            }

        } else {
            staff_profile_photo.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) (findViewById(R.id.staff_create_at)).getLayoutParams();
            params.setMargins(0, 0, 0, 0);

            (findViewById(R.id.staff_create_at)).setLayoutParams(params);

            (findViewById(R.id.ll_update_staff)).setVisibility(View.GONE);
        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6987566671", this, adContainer);
        }
    }

    void initializeDateOfJoining() {
        final Calendar myCalendar = Calendar.getInstance();
        // Date of Joining

        final DatePickerDialog.OnDateSetListener date = (DatePicker view, int year, int monthOfYear, int dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
            btn_date_of_joining.setText(sdf.format(myCalendar.getTime()));
        };

        btn_date_of_joining.setOnClickListener(view -> {
            new DatePickerDialog(StaffCreateActivity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        // Date of Birth
        final DatePickerDialog.OnDateSetListener dob = (DatePicker view, int year, int monthOfYear, int dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
            btn_date_of_birth.setText(sdf.format(myCalendar.getTime()));
        };

        btn_date_of_birth.setOnClickListener(view -> {
            new DatePickerDialog(StaffCreateActivity.this, dob, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    private void initializeViews() {
        et_create_staff_new_staff_name = findViewById(R.id.et_create_staff_new_staff_name);
        et_create_staff_new_staff_email = findViewById(R.id.et_create_staff_new_staff_email);
        et_create_staff_country_code = findViewById(R.id.et_create_staff_country_code);
        et_create_staff_new_staff_mobile = findViewById(R.id.et_create_staff_new_staff_mobile);
        et_create_staff_new_staff_user_name = findViewById(R.id.et_create_staff_new_staff_user_name);
        et_create_staff_new_staff_password = findViewById(R.id.et_create_staff_new_staff_password);
        btn_date_of_joining = findViewById(R.id.btn_date_of_joining);

        btn_date_of_birth = findViewById(R.id.btn_date_of_birth);

        et_create_staff_father_name = findViewById(R.id.et_create_staff_father_name);
        et_create_staff_mother_name = findViewById(R.id.et_create_staff_mother_name);
        et_create_staff_present_address = findViewById(R.id.et_create_staff_present_address);

        staff_profile_photo = findViewById(R.id.staff_profile_photo);

        staff_profile_photo.setOnClickListener(view -> {
            CropImage.activity()
                    .setAspectRatio(1, 1)
                    .setMaxCropResultSize(1920, 1080)
                    .start(this);
        });


//        switch_compact_create_staff_send_sms = findViewById(R.id.switch_compact_create_staff_send_sms);
//        if(Gc.getSharedPreference(Gc.SENDSMSENABLED,this).equalsIgnoreCase(Gc.TRUE)){
//            switch_compact_create_staff_send_sms.setChecked(true);
//        }else {
//            switch_compact_create_staff_send_sms.setChecked(false);
//        }
//        switch_compact_create_staff_send_sms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(final CompoundButton compoundButton, boolean b) {
//                if(b){
//                    AlertDialog.Builder alertSms = new AlertDialog.Builder(StaffCreateActivity.this);
//                    alertSms.setIcon(getResources().getDrawable(R.drawable.ic_coins));
//                    alertSms.setTitle(getString(R.string.sms_cost_alert));
//                    alertSms.setMessage(getString(R.string.sms_alert));
//                    alertSms.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            checkAndroidSMSPermission();
//                            HashMap<String,String> setDefaults = new HashMap<>();
//                            setDefaults.put(Gc.SENDSMSENABLED, Gc.TRUE);
//                            Gc.setSharedPreference(setDefaults, StaffCreateActivity.this);
//                        }
//                    });
//                    alertSms.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            compoundButton.setChecked(false);
//                            HashMap<String,String> setDefaults = new HashMap<>();
//                            setDefaults.put(Gc.SENDSMSENABLED, Gc.FALSE);
//                            Gc.setSharedPreference(setDefaults, StaffCreateActivity.this);
//                        }
//                    });
//                    alertSms.show();
//                }
//            }
//        });
    }

    void checkAndroidSMSPermission() {

        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                        != PackageManager.PERMISSION_GRANTED

        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.SEND_SMS

                }, 100);
            }
        }
    }

    private void selectUserTypeDialog() {
        btn_create_staff_user_type = findViewById(R.id.btn_create_staff_user_type);
        userTypesListBuilder = new AlertDialog.Builder(StaffCreateActivity.this);
        userTypesListAdapter = new ArrayAdapter<String>(StaffCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        userTypesListAdapter.addAll(userTypes);
        userTypesListBuilder.setAdapter(userTypesListAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btn_create_staff_user_type.setText(userTypes.get(i));
                selectedUserType = i;
            }
        });


        btn_create_staff_user_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userTypesListBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = userTypesListBuilder.create();
                dialog.show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Uri resultUri = result.getUri();
            isProfileChanged = true;
            Glide.with(this)
                    .load(new File(resultUri.getPath())) // Uri of the picture
                    .into(staff_profile_photo);
        }
    }

    private void selectStaffPositionDialog() {
        btn_create_staff_staff_position = findViewById(R.id.btn_create_staff_staff_position);
        staffPositionListBuilder = new AlertDialog.Builder(StaffCreateActivity.this);
        staffPositionListAdapter = new ArrayAdapter<String>(StaffCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        staffPositionListAdapter.addAll(staffPositions);
        staffPositionListBuilder.setAdapter(staffPositionListAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                btn_create_staff_staff_position.setText(staffPositions.get(i));
                selectedStaffPosition = i;
            }
        });


        btn_create_staff_staff_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staffPositionListBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = staffPositionListBuilder.create();
                dialog.show();
            }
        });

    }

    private void selectGenderDialog() {
        btn_gender = findViewById(R.id.btn_gender);
        genderListBuilder = new AlertDialog.Builder(StaffCreateActivity.this);
        genderListAdapter = new ArrayAdapter<String>(StaffCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        genderListAdapter.addAll(gender);
        genderListBuilder.setAdapter(genderListAdapter, (dialogInterface, i) -> {
            btn_gender.setText(gender.get(i));
            selectedGender = i + 1;
        });

        btn_gender.setOnClickListener(view -> {
            genderListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog dialog = genderListBuilder.create();
            dialog.show();
        });
    }

    private Boolean validateInput() {
        if (staffUpdate == null) {
            if (getString(R.string.select_user_type).equals(btn_create_staff_user_type.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.user_type) + " " + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                btn_create_staff_user_type.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return false;
            }

            if (getString(R.string.designation).equals(btn_create_staff_staff_position.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.designation) + " " + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                btn_create_staff_staff_position.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return false;
            }

            if (getString(R.string.date_of_joining).equalsIgnoreCase(btn_date_of_joining.getText().toString())) {
                Config.responseSnackBarHandler(getString(R.string.date_of_joining) + " " + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                return false;
            }

        }
        if ("".equals(et_create_staff_new_staff_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.name_can_not_be_blank),
                    findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
            et_create_staff_new_staff_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(et_create_staff_new_staff_email.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_email_id),
                    findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
            et_create_staff_new_staff_email.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        String mobile = et_create_staff_country_code.getText().toString().trim()
                + "-" + et_create_staff_new_staff_mobile.getText().toString().trim();
        if (mobile.length() > 15 || mobile.length() < 5
                || "".equals(et_create_staff_country_code.getText().toString().trim())
                || "".equals(et_create_staff_new_staff_mobile.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_mobile_mumber),
                    findViewById(R.id.rl_staff_create_top), R.color.fragment_first_green);
            et_create_staff_new_staff_mobile.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }
        if (staffUpdate == null) {

            if ("".equals(et_create_staff_new_staff_user_name.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.user_name) + " " + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                et_create_staff_new_staff_user_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return false;
            }

            if ("".equals(et_create_staff_new_staff_password.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.password_can_not_be_blank),
                        findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                et_create_staff_new_staff_password.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return false;
            }
        }
        return true;
    }

    private JSONObject buildDataForCreateStaff() throws JSONException {
        JSONObject param = new JSONObject();
        param.put("UserType", masterEntryUserTypes.get(selectedUserType).MasterEntryId);
        param.put("UserName", et_create_staff_new_staff_user_name.getText().toString().trim());
        param.put("Password", et_create_staff_new_staff_password.getText().toString().trim());
        param.put("StaffPosition", masterEntryStaffPositions.get(selectedStaffPosition).MasterEntryId);
        param.put("StaffName", et_create_staff_new_staff_name.getText().toString().trim());
        param.put("StaffMobile", Gc.makePhoneNumber(
                et_create_staff_country_code.getText().toString().trim()
                , et_create_staff_new_staff_mobile.getText().toString().trim())
        );
        param.put("StaffEmail", et_create_staff_new_staff_email.getText().toString().trim());
        param.put("StaffDOJ", btn_date_of_joining.getText());
        return param;
    }

    private void createStaff() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);


        final JSONObject param = buildDataForCreateStaff();

//        p.put("Frequency", "Monthly");


        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "staff/staffRegistration";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray staffjarr = resultjobj.getJSONArray("staffDetails");
                            final ArrayList<SchoolStaffEntity> staffEntities =
                                    new Gson().fromJson(staffjarr.toString(), new TypeToken<List<SchoolStaffEntity>>() {
                                    }
                                            .getType());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.schoolStaffModel().insertStaff(staffEntities);
                                }
                            }).start();

//                            if (switch_compact_create_staff_send_sms.isChecked())
//                            sendSMS();

                            Intent intent = new Intent();
                            intent.putExtra("message", job.optString("message"));
                            setResult(RESULT_OK, intent);
                            finish();
                            //         Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StaffCreateActivity.this);

                        Intent intent1 = new Intent(StaffCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StaffCreateActivity.this);

                        Intent intent2 = new Intent(StaffCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(StaffCreateActivity.this, error, findViewById(R.id.rl_staff_create_top));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void updateStaff() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject param = new JSONObject();

        param.put("StaffName", et_create_staff_new_staff_name.getText().toString().trim());
        param.put("StaffEmail", et_create_staff_new_staff_email.getText().toString().trim());
        param.put("StaffMobile", et_create_staff_country_code.getText().toString().trim()
                + "-" + et_create_staff_new_staff_mobile.getText().toString().trim());

        param.put("StaffDOB", btn_date_of_birth.getText().toString().trim());
        param.put("StaffPresentAddress", et_create_staff_present_address.getText().toString().trim());
        param.put("StaffFName", et_create_staff_father_name.getText().toString().trim());
        param.put("StaffMName", et_create_staff_mother_name.getText().toString().trim());

        if (selectedGender > 0) {
            param.put("staffGender", masterEntryGender.get(selectedGender - 1).MasterEntryId);
        }

        if (isProfileChanged) {
            Bitmap image = ((BitmapDrawable) staff_profile_photo.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("staffProfileImage", new JSONObject().put("Type", "jpg").put("StaffProfile", encodedImage));
        }

        Log.e("Ok", param.toString());
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "staff/staffRegistration/"
                + new JSONObject().put("StaffId", staffUpdate.StaffId);
        ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            JSONArray staffjarr = resultjobj.getJSONArray("staffDetails");
                            final ArrayList<SchoolStaffEntity> staffEntities =
                                    new Gson().fromJson(staffjarr.toString(), new TypeToken<List<SchoolStaffEntity>>() {
                                    }
                                            .getType());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.schoolStaffModel().insertStaff(staffEntities);
                                }
                            }).start();


                            Intent intent = new Intent();
                            intent.putExtra("message", job.optString("message"));
                            setResult(RESULT_OK, intent);
                            finish();
                            //         Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_staff_create_top), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(StaffCreateActivity.this, error, findViewById(R.id.rl_staff_create_top));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                if (validateInput()) {
                    if (staffUpdate == null) {
                        try {
                            createStaff();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            updateStaff();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.masterEntryModel().getMasterEntryValuesForKeyLive("Gender")
                .observe(this, masterEntryEntities -> {
                    masterEntryGender.clear();
                    gender.clear();
                    genderListAdapter.clear();
                    masterEntryGender.addAll(masterEntryEntities);
                    for (int i = 0; i < masterEntryGender.size(); i++) {
                        gender.add(masterEntryGender.get(i).MasterEntryValue);
                    }
                    genderListAdapter.addAll(gender);
                    genderListAdapter.notifyDataSetChanged();
                });

        mDb.masterEntryModel().getMasterEntryValuesForKeyLive("UserType")
                .observe(this, new Observer<List<MasterEntryEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                        masterEntryUserTypes.clear();
                        userTypes.clear();
                        userTypesListAdapter.clear();
                        masterEntryUserTypes.addAll(masterEntryEntities);
                        for (int i = 0; i < masterEntryUserTypes.size(); i++) {
                            userTypes.add(masterEntryUserTypes.get(i).MasterEntryValue);
                        }
                        userTypesListAdapter.addAll(userTypes);
                        userTypesListAdapter.notifyDataSetChanged();
                    }
                });

        mDb.masterEntryModel().getMasterEntryValuesForKeyLive("StaffPosition")
                .observe(this, new Observer<List<MasterEntryEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                        masterEntryStaffPositions.clear();
                        staffPositions.clear();
                        staffPositionListAdapter.clear();
                        masterEntryStaffPositions.addAll(masterEntryEntities);
                        for (int i = 0; i < masterEntryStaffPositions.size(); i++) {
                            staffPositions.add(masterEntryStaffPositions.get(i).MasterEntryValue);
                        }
                        staffPositionListAdapter.addAll(staffPositions);
                        staffPositionListAdapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.masterEntryModel().getMasterEntryValuesForKeyLive("UserType").removeObservers(this);
        mDb.masterEntryModel().getMasterEntryValuesForKeyLive("StaffPosition").removeObservers(this);
    }

    void sendSMS() {
        String shareBody =
                getString(R.string.link) + " - " +
                        Config.SCHOOL_PLAY_STORE_LINK + BuildConfig.APPLICATION_ID + "\n" +
                        getString(R.string.key) + "-" + Gc.getSharedPreference(Gc.ERPINSTCODE, this) + "\n" +

                        getString(R.string.login_id) + "-" + et_create_staff_new_staff_user_name.getText().toString() + "\n" +

                        getString(R.string.password) + "-" + et_create_staff_new_staff_password.getText().toString();

        SmsManager smsManager = SmsManager.getDefault();

        ArrayList<String> parts = smsManager.divideMessage(shareBody);
        int messageCount = parts.size();

        String mobile = Gc.makePhoneNumber(et_create_staff_country_code.getText().toString(),
                et_create_staff_new_staff_mobile.getText().toString());

        smsManager.sendMultipartTextMessage(mobile, null, parts, null, null);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
