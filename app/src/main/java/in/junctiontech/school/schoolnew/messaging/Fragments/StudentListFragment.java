package in.junctiontech.school.schoolnew.messaging.Fragments;

import android.app.SearchManager;
import androidx.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.messaging.MsgAdapters.StudentListAdapter;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class StudentListFragment extends
        Fragment implements SearchView.OnQueryTextListener{
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private SwipeRefreshLayout swipe_refresh_list;
    private MainDatabase mdb;
    private ArrayList<SchoolStudentEntity> studentListData= new ArrayList<>();
    private StudentListAdapter adapter;
    private boolean msgToStudent = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data =  convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available =  convertView.findViewById(R.id.tv_view_all_data_not_data_available);
        swipe_refresh_list =convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);
        if (!msgToStudent)
            tv_view_all_data_not_data_available.setText(getString(R.string.you_dont_have_permission_to_send_messages_to)+" "+
                    getString(R.string.students));

        setHasOptionsMenu(true);
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();

    }


    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new StudentListAdapter(getContext(), studentListData,  Config.getAppColor(getActivity(),false));
        recycler_view_list_of_data.setAdapter(adapter);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mdb = MainDatabase.getDatabase(getContext());

            mdb.schoolStudentModel()
                    .getAllStudentsSession(Gc.getSharedPreference(Gc.APPSESSION, getContext()))
                    .observe(this, new Observer<List<SchoolStudentEntity>>() {
                @Override
                public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                    studentListData.clear();
                    studentListData.addAll(schoolStudentEntities);
                    adapter.notifyDataSetChanged();
                }
            });

//        Bundle b = db.getClassSectionList();
//        sectionIdList = b.getStringArrayList("SectionIdList");
//        sectionNameList = b.getStringArrayList("SectionNameList");
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setFilter(ArrayList<SchoolStudentEntity> studentListData) {
        if (adapter!=null)
        adapter.setFilter(studentListData);

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_sort_and_search, menu);
  /*  *//* --  toolbar spinner ---*//*
        MenuItem itemSpinner = menu.findItem(R.id.menu_select_class_spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(itemSpinner);

        ArrayAdapter<String> adapterSectionList = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, sectionNameList);
        spinner.setAdapter(adapterSectionList);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position==0)
                    studentListData = db.getStudentsForMessaging("1", "parent", true);
                else
                    studentListData = db.getStudentsForMessaging(sectionIdList.get(position), "parent", false);
                adapter.updateList(studentListData);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setAdapter(adapterSectionList);*/

        /* --  toolbar search ---*/

        SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.menu_action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));

//        final MenuItem item = menu.findItem(R.id.menu_action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(this);

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                setFilter(studentListData);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                setFilter(studentListData);
                return true;
            }
        });

//        MenuItemCompat.setOnActionExpandListener(item,
//                new MenuItemCompat.OnActionExpandListener() {
//                    @Override
//                    public boolean onMenuItemActionCollapse(MenuItem item) {
//// Do something when collapsed
//                          setFilter(studentListData);
//                        return true; // Return true to collapse action view
//                    }
//
//                    @Override
//                    public boolean onMenuItemActionExpand(MenuItem item) {
//// Do something when expanded
//                        return true; // Return true to expand action view
//                    }
//                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_sort_by_alphabetical:
                Toast.makeText(getContext(), getString(R.string.sort_by_alphabetical), Toast.LENGTH_LONG).show();
                Collections.sort(studentListData, new Comparator<SchoolStudentEntity>() {
                    @Override
                    public int compare(SchoolStudentEntity o1, SchoolStudentEntity o2) {
                        String lname = o1.StudentName.toLowerCase();
                        String rname = o2.StudentName.toLowerCase();

                        //  Log.e("compareTo",lname.compareTo(rname)+" ritu");
                        //  Log.e("ArrayListSort","INTEGER");
                        return lname.compareTo(rname);


                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                if (adapter!=null)
                adapter.notifyDataSetChanged();
                break;

            case R.id.menu_sort_by_number:
                Toast.makeText(getContext(), getString(R.string.sort_by_number), Toast.LENGTH_LONG).show();

                Collections.sort(studentListData, new Comparator<SchoolStudentEntity>() {
                    @Override
                    public int compare(SchoolStudentEntity o1, SchoolStudentEntity o2) {
                        String lid = o1.AdmissionNo;
                        String rid = o2.AdmissionNo;

                        try {
                            int ld = Integer.parseInt(lid);
                            int rd = Integer.parseInt(rid);
                            //  Log.e("ArrayListSort","INTEGER");
                            if (ld > rd)
                                return 1;
                            else if (ld < rd)
                                return -1;
                            else
                                return 0;

                        } catch (NumberFormatException e) {
                            // Log.e("ArrayListSort","String");
                            return lid.compareTo(rid);
                        }

                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                if (adapter!=null)
                adapter.notifyDataSetChanged();
                break;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<SchoolStudentEntity> filteredModelList = filter(studentListData, newText);
        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<SchoolStudentEntity> filter(ArrayList<SchoolStudentEntity> models, String query) {
        query = query.toLowerCase();final ArrayList<SchoolStudentEntity> filteredModelList = new ArrayList<>();

        for (SchoolStudentEntity model : models) {

            if (model.StudentName.toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    Boolean hasMessengerPermission(){
        Gc.getSharedPreference(Gc.USERTYPECODE,getActivity());

        return false;
    }
}
