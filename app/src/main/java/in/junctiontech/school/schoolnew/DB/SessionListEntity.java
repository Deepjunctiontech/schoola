package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by deep on 27/03/18.
 */
@Entity
public class SessionListEntity {
    @PrimaryKey
    @NonNull
    public String session;
    public String sessionStartDate;
    public String sessionEndDate;
}
