package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.DB.SessionListEntity;

/**
 * Created by deep on 27/03/18.
 */
@Entity
public class AcademicSessions {
    @PrimaryKey
    @NonNull
    public String currentSession;
    public String type;
    public ArrayList<SessionListEntity> sessionList;
}
