package in.junctiontech.school.schoolnew.gallery;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 03-10-2017.
 */

public class GalleryData implements Serializable {
    String thumbnailUrl, originalUrl, name, size;

    public GalleryData(String name,String thumbnailUrl, String originalUrl, String size) {
        this.name = name;
        this.thumbnailUrl = thumbnailUrl;
        this.originalUrl = originalUrl;
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
