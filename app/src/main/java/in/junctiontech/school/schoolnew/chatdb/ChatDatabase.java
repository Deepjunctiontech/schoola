package in.junctiontech.school.schoolnew.chatdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {
        ChatListEntity.class
}, version = 3)


public abstract class ChatDatabase extends RoomDatabase {
    private static ChatDatabase CHATINSTANCE;

    public abstract ChatListDao chatListModel();

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ChatListEntity ADD COLUMN imageOriginalPath TEXT DEFAULT NULL");
        }
    };
    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ChatListEntity ADD COLUMN localPath TEXT DEFAULT NULL");
            database.execSQL("ALTER TABLE ChatListEntity ADD COLUMN fileSize TEXT DEFAULT NULL");
        }
    };
   /* private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ChatListEntity ADD COLUMN fileSize TEXT DEFAULT NULL");
        }
    };
*/

    public static ChatDatabase getDatabase(Context context) {
        if (CHATINSTANCE == null) {
            synchronized (ChatDatabase.class) {
                if (CHATINSTANCE == null) {
                    CHATINSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ChatDatabase.class,
                            "school_chat")
                            .addMigrations(MIGRATION_1_2,MIGRATION_2_3)
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return CHATINSTANCE;
    }

    /*public static void destroyInstance() {
        CHATINSTANCE = null;
    }*/
}
