package in.junctiontech.school.schoolnew.onlineexam.db;

import java.util.ArrayList;

public class OnlineExamResultModel {
    public String onlineExamName;
    public String examOpenDate;
    public String examOpenTime;
    public String examCloseDate;
    public String examCloseTime;
    public String examDuration;
    public String examTimer;
    public String examCompletionTime;
    public String examStartTime;
    public String examEndTime;
    public String showResult;
    public String showSolution;
    public String examCheckStatus;
    public String StudentName;
    public int numberOfSections;


    public ArrayList<ExamResultModel> result;
    public ArrayList<OnlineExamModel.ExamSectionDetails> examSections;

    public class ExamResultModel{
        public String examType;
        public String SubjectName;
        public String maxMarks;
        public String obtainMarks;
        public String grade;
        public String result;
    }



}
