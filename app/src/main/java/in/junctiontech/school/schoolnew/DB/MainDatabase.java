package in.junctiontech.school.schoolnew.DB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksDao;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.common.Converters;
import in.junctiontech.school.schoolnew.model.AdminInformationEntity;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;
import in.junctiontech.school.schoolnew.model.StudentInformation;

@Database(entities = {
        SchoolDetailsEntity.class,
        SessionListEntity.class,
        OrganizationPermissionEntity.class,
        SchoolClassEntity.class,
        SchoolClassSectionEntity.class,
        SchoolSubjectsEntity.class,
        SchoolSubjectsClassEntity.class,
        MasterEntryEntity.class,
        SchoolAccountsEntity.class,
        FeeTypeEntity.class,
        ExamTypeEntity.class,
        ClassFeesEntity.class,
        ClassFeesTransportEntity.class,
        UserPermissionsEntity.class,
        SignedInUserInformationEntity.class,
        SignedInStaffInformationEntity.class,
        StudentInformation.class,
        AdminInformationEntity.class,
        CommunicationPermissions.class,
        SchoolCalenderEntity.class,
        NoticeBoardEntity.class,
        SchoolStudentEntity.class,
        SchoolStaffEntity.class,
        SchoolUserEntity.class,
        AssignmentEntity.class,
        TransportFeeAmountEntity.class,
        GradeMarksEntity.class
}, version = 8)
@TypeConverters({Converters.class})
public abstract class MainDatabase extends RoomDatabase {
    private static MainDatabase INSTANCE;

    public abstract SchoolDetailsDao schoolDetailsModel();

    public abstract SessionListDao sessionListModel();

    public abstract OrganizationPermissionDao organizationPermissionModel();

    public abstract SchoolClassDao schoolClassModel();

    public abstract SchoolClassSectionDao schoolClassSectionModel();

    public abstract SchoolSubjectsDao schoolSubjectsModel();

    public abstract SchoolSubjectsClassDao schoolSubjectsClassModel();

    public abstract MasterEntryDao masterEntryModel();

    public abstract SchoolAccountsDao schoolAccountsModel();

    public abstract FeeTypeDao feeTypeModel();

    public abstract ExamTypeDao examTypeModel();

    public abstract ClassFeesDao classFeesModel();

    public abstract UserPermissionsDao userPermissionsModel();

    public abstract SignedInUserInformationDao signedInUserInformationModel();

    public abstract SignedInStaffInformationDao signedInStaffInformationModel();

    public abstract SignedInStudentInformationDao signedInStudentInformationModel();

    public abstract SignedInAdminInformationDao signedInAdminInformationModel();

    public abstract CommunicationPermissionsDao communicationPermissionsModel();

    public abstract SchoolCalenderDao schoolCalenderModel();

    public abstract NoticeBoardDao noticeBoardModel();

    public abstract SchoolStudentDao schoolStudentModel();

    public abstract SchoolStaffDao schoolStaffModel();

    public abstract SchoolUserDao schoolUserModel();

    public abstract AssignmentDao assignmentModel();

    public abstract TransportFeeAmountDao transportFeeAmountModel();

    public abstract ClassFeesTransportDao classFeesTransportModel();

    public abstract GradeMarksDao gradeMarksModel();

    static final Migration MIGRATION_7_8 = new Migration(7, 8) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
           database.execSQL("ALTER TABLE AssignmentEntity " +
                    "ADD COLUMN homeworkId TEXT  DEFAULT Null");
        }
    };

    public static MainDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MainDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MainDatabase.class, "singleschool_database")
                            .allowMainThreadQueries()
                            .addMigrations(MIGRATION_7_8)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
