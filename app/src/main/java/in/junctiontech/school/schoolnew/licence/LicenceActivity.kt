package `in`.junctiontech.school.schoolnew.licence

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class LicenceActivity : AppCompatActivity() {
    private val schoolModulesList = ArrayList<Modules>()
    internal var orgPermissionList = ArrayList<String>()
    lateinit var progressBar: ProgressBar
    private lateinit var mDb: MainDatabase
    private lateinit var tvLicenceDate: TextView
    private lateinit var tvModuleRequestInfo: TextView
    private lateinit var tvDaysLeft: TextView
    private lateinit var tvDaysLeftText: TextView
    private lateinit var btnRequestToExtend: Button
    private lateinit var btnCheckPlans: Button
    private lateinit var rvModulesList: RecyclerView
    lateinit var adapter: ModulesListAdapter

    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_licence)
        mDb = MainDatabase.getDatabase(applicationContext)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setColorApp()
        initializeViews()
        getModules()
        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/6934212891", this, adContainer)
        }

    }
    override fun onResume() {

        super.onResume()

        getLicenceDetails()

    }

    private fun getLicenceDetails() {
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/licenseDetails")

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code=job.optString("code")
            when (job.optString("code")) {
                "200" -> try {
                    val license = job.getJSONObject("result").getJSONObject("licenseDetails").optString("license")
                    if (license != "") {
                        val setDefaults = HashMap<String, String>()
                        setDefaults[Gc.LICENCE] = license
                        Gc.setSharedPreference(setDefaults, this)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_licence), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            Config.responseVolleyErrorHandler(this@LicenceActivity, error, findViewById<View>(R.id.activity_licence))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun initializeViews() {

        mDb.organizationPermissionModel().organizationPermissions.observe(this, Observer<List<String>> { string ->
            orgPermissionList.clear()
            for (i in string.indices) {
                orgPermissionList.add(string[i])
            }
        })

        progressBar = findViewById(R.id.progressBar)

        tvModuleRequestInfo = findViewById(R.id.tv_module_request_info)
        tvLicenceDate = findViewById(R.id.tv_licence_date)
        tvDaysLeft = findViewById(R.id.tv_days_left)
        tvDaysLeftText = findViewById(R.id.tv_days_left_text)
        btnRequestToExtend = findViewById(R.id.btn_request_to_extend)
        btnCheckPlans = findViewById(R.id.btn_check_plans)
        rvModulesList = findViewById(R.id.rv_modules_list)
        rvModulesList.setHasFixedSize(true)

        btnRequestToExtend.setOnClickListener {
            tvModuleRequestInfo.visibility = View.VISIBLE
            tvModuleRequestInfo.text = "To Extend Your License Please Contact Info@zeroerp.com"
            btnRequestToExtend.visibility = View.GONE
        }

        btnCheckPlans.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.zeroerp.com/zeroerp-school-management-software-pricing/"))
            startActivity(browserIntent)
        }

        val licenceExpiry = Gc.getSharedPreference(Gc.LICENCE, this)

        if (!licenceExpiry.equals("", ignoreCase = true)) {

            tvLicenceDate.text = licenceExpiry

            val licenceExpiryArry = licenceExpiry.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val calCurrent = Calendar.getInstance()
            val calLicence = Calendar.getInstance()
            if (licenceExpiryArry.size >= 3) {
                if (licenceExpiryArry[0].length == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[0]), Integer.parseInt(licenceExpiryArry[1]) - 1, Integer.parseInt(licenceExpiryArry[2]))
                else if (licenceExpiryArry[2].length == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[2]), Integer.parseInt(licenceExpiryArry[1]) - 1, Integer.parseInt(licenceExpiryArry[0]))

            }

            val diff = calLicence.timeInMillis - calCurrent.timeInMillis

            val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)

            if(dayCount.toInt()<0){
                val x = Math.abs(dayCount.toInt())

                tvDaysLeft.text = "Your Software License Has Expired "+ x.toString()+" Ago ......"
                tvDaysLeftText.visibility=View.GONE
            }else{
                tvDaysLeft.text = dayCount.toInt().toString()
            }
        }

        val layoutManager = LinearLayoutManager(this)
        rvModulesList.layoutManager = layoutManager

        adapter = ModulesListAdapter()
        rvModulesList.adapter = adapter
    }

    private fun getModules() {
        progressBar.visibility = View.VISIBLE

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/modulesList")

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE

            when (code) {
                "200" -> try {
                    val modules = Gson()
                            .fromJson<ArrayList<Modules>>(job.getJSONObject("result")
                                    .optString("moduleList"), object : TypeToken<List<Modules>>() {

                            }.type)
                    schoolModulesList.clear()
                    schoolModulesList.addAll(modules)

                    adapter.notifyDataSetChanged()


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_licence), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@LicenceActivity, error, findViewById<View>(R.id.activity_licence))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class ModulesListAdapter : RecyclerView.Adapter<ModulesListAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_modules_list, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvModuleName.text = schoolModulesList[position].module
            holder.tvModuleRequest.visibility = View.GONE
            if (orgPermissionList.contains(schoolModulesList[position].module)) {
                holder.itemView.setBackgroundColor(Color.WHITE)
                holder.tvModuleStatus.text = "Activated"
                holder.btnModuleRequest.visibility = View.GONE
            } else {
                holder.tvModuleStatus.text = "Deactivated"
                holder.itemView.setBackgroundColor(Color.GRAY)
                holder.btnModuleRequest.visibility = View.VISIBLE
            }
        }

        override fun getItemCount(): Int {
            return schoolModulesList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvModuleName: TextView = itemView.findViewById(R.id.tv_module_name)
            internal var tvModuleStatus: TextView = itemView.findViewById(R.id.tv_module_status)
            internal var tvModuleRequest: TextView = itemView.findViewById(R.id.tv_module_request)
            internal var btnModuleRequest: Button = itemView.findViewById(R.id.btn_module_request)

            init {
                btnModuleRequest.setOnClickListener {
                    btnModuleRequest.visibility = View.GONE
                    tvModuleRequest.visibility = View.VISIBLE
                }
            }
        }
    }
}

class Modules {
    var module: String? = null
}