package `in`.junctiontech.school.schoolnew.chat.adapter

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

class StudentListFragmentAdapter(staffInfo: ArrayList<SchoolStudentEntity>, private val context: Context, activity: FragmentActivity?,val listener: StudentClickListener) : RecyclerView.Adapter<StudentListFragmentAdapter.ViewHolder>() {
    private var studentListInfo: ArrayList<SchoolStudentEntity> = staffInfo

    private val colorIs = Config.getAppColor(activity, true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listener)
        holder.tvChatName.setTextColor(colorIs)

        holder.tvChatName.text = String.format("%s %s",
                studentListInfo[position].StudentName,
                studentListInfo[position].ClassName)

        if ("" != studentListInfo[position].studentProfileImage) Glide.with(context)
                .load(studentListInfo[position].studentProfileImage)
                .apply(RequestOptions()
                        .error(R.drawable.ic_single)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .thumbnail(0.5f)
                .into(holder.icContact) else holder.icContact.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_single))
    }

    override fun getItemCount(): Int {
        return studentListInfo.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvChatName: TextView = itemView.findViewById(R.id.tv_chat_name)
        var icContact: CircleImageView = itemView.findViewById(R.id.ic_contact)
        fun bind(listener: StudentClickListener) {




            // this is the click listener. It calls the onItemClicked interface method implemented in the Activity
            itemView.setOnClickListener {
                listener.onStartChatStudentClickCallback(adapterPosition)
            }
        }
        /*init {
            itemView.setOnClickListener { view: View ->
                val intent = Intent(this@StaffListFragment, MessageListActivity::class.java)
                val staff = Gson().toJson(staffListInfo.get(adapterPosition))
                intent.putExtra("with", staff)
                intent.putExtra("withType", "staff")
                startActivity(intent)
                finish()
            }
        }*/
    }
    interface StudentClickListener{
        fun onStartChatStudentClickCallback(position: Int)
    }
}
