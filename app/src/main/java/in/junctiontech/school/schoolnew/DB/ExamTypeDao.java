package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ExamTypeDao {
    @Query("select * from ExamTypeEntity")
    LiveData<List<ExamTypeEntity>> getExamTypes();

    @Query("select Exam_Type from ExamTypeEntity")
    LiveData<List<String>> getExamTypesName();

    @Insert(onConflict = REPLACE)
    void insertExamType(ExamTypeEntity examTypeEntity);

    @Query("delete from ExamTypeEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertExamTypes(List<ExamTypeEntity> examTypeEntity);
}
