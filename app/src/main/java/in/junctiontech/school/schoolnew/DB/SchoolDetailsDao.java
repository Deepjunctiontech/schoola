package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolDetailsDao {
    @Query("select * from SchoolDetailsEntity limit 1")
    LiveData<SchoolDetailsEntity> getSchoolDetailsLive();

    @Query("select * from SchoolDetailsEntity limit 1")
    SchoolDetailsEntity getSchoolDetails();

    @Query("select currentsession from SchoolDetailsEntity limit 1")
    String getCurrentSession();

    @Insert(onConflict = REPLACE)
    void insertSchoolDetails(SchoolDetailsEntity schoolDetailsEntity);

    @Update(onConflict = REPLACE)
    void updateSchoolDetails(SchoolDetailsEntity schoolDetailsEntity);
}
