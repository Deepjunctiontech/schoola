package `in`.junctiontech.school.schoolnew.feesetup.feetype.receive

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline.FeeDues
import `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline.OnlineTransactionsActivity
import `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline.PayOnlineActivity
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StudentDueActivity : AppCompatActivity() {

    private var colorIs: Int = 0
    lateinit var progressBar: ProgressBar

    private lateinit var adapter: StudentDueActivityAdapter
    lateinit var mRecyclerView: RecyclerView

    private lateinit var tvStudentName: TextView
    private lateinit var tvCurrentClass: TextView
    private lateinit var tvStudentDueFeesTotal: TextView
    private lateinit var tvStudentDueSessionTotal: TextView
    private lateinit var tvStudentDueQuarterlyTotal: TextView
    private lateinit var tvStudentDueMonthlyTotal: TextView
    private lateinit var tvStudentFeesAmountTotal: TextView
    private lateinit var tvStudentPaidFeesTotal: TextView

    private lateinit var btnSelectMonth: Button
    private lateinit var btnStudentPayNow: Button
    private lateinit var btnStudentViewTransactions: Button

    private lateinit var selectedStudent: String
    private lateinit var studentEntity: SchoolStudentEntity

    private val feeDueList = ArrayList<FeeDues>()
    internal var filledFeeAmount = ArrayList<String>()
    var dateStr = ""
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_due_student)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initializeViews()

        val studentString = intent.getStringExtra("student")
        if (studentString == null){
            finish()
            return
        }
        selectedStudent = studentString

        if (!"".equals(Strings.nullToEmpty(selectedStudent), ignoreCase = true) && selectedStudent.isNotEmpty()) {
            studentEntity = Gson().fromJson(selectedStudent, SchoolStudentEntity::class.java)
            val classSectionName = "${studentEntity.ClassName} : ${studentEntity.SectionName}"
            tvStudentName.text = studentEntity.StudentName
            tvCurrentClass.text = classSectionName


            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            try {
                val date = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))

                dateStr = SimpleDateFormat("MMM-yyyy", Locale.getDefault()).format(date)

            } catch (e: ParseException) {
                e.printStackTrace()
            }

            btnSelectMonth.text = dateStr
            try {
                getFeeDueForStudent()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        setColorApp()

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase())&&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/7290332938", this, adContainer)
        }
    }

    private fun getFeeDueForStudent() {
        progressBar.visibility = View.VISIBLE

        val p = JSONObject()
        p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this))
        p.put("admissionId", studentEntity.AdmissionId)
        p.put("monthYear", btnSelectMonth.text.toString())

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/feesPayment/"
                + p)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            Log.e("Response", "$job")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val feeDue = Gson()
                            .fromJson<ArrayList<FeeDues>>(job.getJSONObject("result").getJSONObject("studentFees")
                                    .optString("fees"), object : TypeToken<List<FeeDues>>() {
                            }.type)
                    feeDueList.clear()
                    filledFeeAmount.clear()
                    if (Objects.requireNonNull<ArrayList<FeeDues>>(feeDue).size > 0) {
                        feeDue.run {
                            sortWith(Comparator { feeDue, t1 ->
                                val s1 = feeDue.FeeTypeFrequency
                                val s2 = t1.FeeTypeFrequency
                                s1!!.compareTo(s2!!, ignoreCase = true)
                            })
                        }

                        feeDueList.addAll(feeDue)
                        for (i in feeDueList.indices) {
                            filledFeeAmount.add(feeDueList[i].AmountBalance.toString())
                        }
                        adapter.notifyDataSetChanged()
                        calculateFeesSumForDisplay()
                    }
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById<View>(R.id.activity_due_student), R.color.fragment_first_green)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_due_student), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@StudentDueActivity, error, findViewById<View>(R.id.activity_due_student))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    internal fun calculateFeesSumForDisplay() {
        var quarterly = 0
        var monthly = 0
        var session = 0
        var totalAmount = 0
        var totalPaidAmount = 0

        for (i in feeDueList.indices) {
            if (!"".equals(feeDueList[i].AmountPaid, ignoreCase = true))
                totalPaidAmount += Integer.parseInt(feeDueList[i].AmountPaid.toString())

            if (!"".equals(feeDueList[i].FeeAmount, ignoreCase = true))
                totalAmount += Integer.parseInt(feeDueList[i].FeeAmount.toString())

            val amount = if (!"".equals(filledFeeAmount[i], ignoreCase = true))
                Integer.parseInt(filledFeeAmount[i])
            else
                0

            if (feeDueList[i].FeeTypeFrequency.equals("monthly", ignoreCase = true))
                monthly += amount
            if (feeDueList[i].FeeTypeFrequency.equals("quarterly", ignoreCase = true))
                quarterly += amount
            if (feeDueList[i].FeeTypeFrequency.equals("session", ignoreCase = true))
                session += amount
        }

        tvStudentDueMonthlyTotal.text = monthly.toString()
        tvStudentDueQuarterlyTotal.text = quarterly.toString()
        tvStudentDueSessionTotal.text = session.toString()
        tvStudentDueFeesTotal.text = (session + monthly + quarterly).toString()

        tvStudentPaidFeesTotal.text = totalPaidAmount.toString()
        tvStudentFeesAmountTotal.text = totalAmount.toString()
    }

    internal fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)
        tvStudentName = findViewById(R.id.tv_student_name)
        tvCurrentClass = findViewById(R.id.tv_current_class)

        btnStudentPayNow = findViewById(R.id.btn_student_pay_now)

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
            if (Gc.getArrayList(Gc.SIGNEDINUSERPERMISSION, this)!!.contains(Gc.PayOnline)) {
                btnStudentPayNow.visibility = View.VISIBLE
                findViewById<LinearLayout>(R.id.ll_btn_student_pay_now).visibility = View.VISIBLE
            }
        }

        btnStudentPayNow.setOnClickListener {
            val intent = Intent(this@StudentDueActivity, PayOnlineActivity::class.java)
            intent.putExtra("student", selectedStudent)
            intent.putExtra("date", dateStr)
            startActivityForResult(intent, Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE)
        }

        btnStudentViewTransactions = findViewById(R.id.btn_student_view_transactions)

        btnStudentViewTransactions.setOnClickListener {
            val intent = Intent(this@StudentDueActivity, OnlineTransactionsActivity::class.java)
            intent.putExtra("student", selectedStudent)
            startActivityForResult(intent, Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE)
        }

        btnSelectMonth = findViewById(R.id.btn_select_month)

        initializeDatePickers()

        tvStudentDueFeesTotal = findViewById(R.id.tv_student_due_fees_total)
        tvStudentDueSessionTotal = findViewById(R.id.tv_student_due_session_total)
        tvStudentDueQuarterlyTotal = findViewById(R.id.tv_student_due_quarterly_total)
        tvStudentDueMonthlyTotal = findViewById(R.id.tv_student_due_monthly_total)
        tvStudentPaidFeesTotal = findViewById(R.id.tv_student_paid_fees_total)
        tvStudentFeesAmountTotal = findViewById(R.id.tv_student_fees_amount_total)

        mRecyclerView = findViewById(R.id.rv_student_due_fee_list)

        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = StudentDueActivityAdapter()
        mRecyclerView.adapter = adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                getFeeDueForStudent()
            }
        } else
            super.onActivityResult(requestCode, resultCode, data)

    }

    private fun initializeDatePickers() {
        val myCalendar = Calendar.getInstance()
        val feeDate = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, _ ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            val myFormat = "MMM-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            dateStr= sdf.format(myCalendar.time)
            btnSelectMonth.text = sdf.format(myCalendar.time)

            try {
                getFeeDueForStudent()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        btnSelectMonth.setOnClickListener {
            DatePickerDialog(this@StudentDueActivity, feeDate, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_fee_receipt, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fee_receipt -> {
                val intent = Intent(this, FeeReceiptsActivity::class.java)
                intent.putExtra("student", selectedStudent)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class StudentDueActivityAdapter : RecyclerView.Adapter<StudentDueActivityAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_student_fee_due, parent, false))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            var feeTypeName = feeDueList[position].FeeTypeName
            if (feeDueList[position].FeeTypeFrequency != "Session") {
                feeTypeName = feeTypeName + " (" + feeDueList[position].MonthYear + ")"
            }
            holder.tvFeeName.text = feeTypeName
            holder.tvFeeAmount.text = feeDueList[position].FeeAmount
            holder.tvFeeDuePaidAmount.text = feeDueList[position].AmountPaid
            holder.tvDueBalance.text = feeDueList[position].AmountBalance
        }

        override fun getItemCount(): Int {
            return feeDueList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvFeeName: TextView = itemView.findViewById(R.id.tv_due_fee_name)
            internal var tvFeeDuePaidAmount: TextView = itemView.findViewById(R.id.tv_fee_due_paid_amount)
            internal var tvDueBalance: TextView = itemView.findViewById(R.id.tv_due_balance)
            internal var tvFeeAmount: TextView = itemView.findViewById(R.id.tv_due_fee_amount)
        }
    }
}