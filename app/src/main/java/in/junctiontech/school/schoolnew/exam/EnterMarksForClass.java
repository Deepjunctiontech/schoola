package in.junctiontech.school.schoolnew.exam;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.DB.examdb.ScholasticResultEntity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EnterMarksForClass extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    LinearLayout student_layout;
    TextView tv_class_name,tv_subject_name, tv_max_marks,tv_exam_type ;

    String sectionid, subjectid, examtype,examtypeid;

    ArrayList<GradeMarksEntity> gradeMarksList = new ArrayList<>();

    HashMap<String, String> resultList = new HashMap<>();

    ArrayList<ScholasticResultEntity> scholasticResultEntities = new ArrayList<>();

    int maxMarks;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        tv_class_name.setTextColor(colorIs);
        tv_subject_name.setTextColor(colorIs);
        tv_max_marks.setTextColor(colorIs);
        tv_exam_type.setTextColor(colorIs);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_marks_for_class);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);


        progressBar = findViewById(R.id.progressBar);

        validateIntentData(); // if intent data is not filled and is null , exit here This activity should run only if all data is passed

        tv_class_name = findViewById(R.id.tv_class_name);
        tv_class_name.setText(getIntent().getStringExtra("classsectionname"));

        sectionid = getIntent().getStringExtra("sectionid");
        subjectid = getIntent().getStringExtra("subjectid");
        examtype = getIntent().getStringExtra("examtype");
        examtypeid = getIntent().getStringExtra("examtypeid");

        tv_subject_name = findViewById(R.id.tv_subject_name);
        tv_subject_name.setText(getIntent().getStringExtra("subjectname"));

        tv_max_marks = findViewById(R.id.tv_max_marks);
        tv_max_marks.setText(getString(R.string.maximum_marks) + "\n" + getIntent().getStringExtra("maxmarks"));
        maxMarks = Integer.parseInt(getIntent().getStringExtra("maxmarks"));
        mDb.gradeMarksModel().getGradeMarks(getIntent().getStringExtra("maxmarks")).observe(this, gradeMarksEntities -> {

            if (gradeMarksEntities == null){
                finish();
            }else{
                gradeMarksList.addAll(gradeMarksEntities);

            }
        });
        tv_exam_type = findViewById(R.id.tv_exam_type);
        tv_exam_type.setText(getIntent().getStringExtra("examtype"));

        student_layout = findViewById(R.id.student_layout);

        mDb.masterEntryModel().getMasterEntryValuesGeneric("Result")
                .observe(this, masterEntryEntities -> {
                    resultList.clear();
                    for (MasterEntryEntity m : masterEntryEntities){
                        resultList.put(m.MasterEntryValue, m.MasterEntryId);
                    }
                });

//        getScholasticResultForClassSubjectExam();

        scholasticResultEntities = new Gson()
                .fromJson(getIntent().getStringExtra("result"),
                        new TypeToken<List<ScholasticResultEntity>>(){}.getType());

        build_Layout();

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/7572507146", this, adContainer);
        }
    }

    void saveScholasticMarks() throws JSONException {


        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String studentIds = "";
        JSONArray studentIdJson = new JSONArray();
        JSONArray marksJson = new JSONArray();
        JSONArray gradeJson = new JSONArray();
        JSONArray resultJson = new JSONArray();
        StringBuilder marks = new StringBuilder();
        String grades = "";
        String results = "";

        for (int k = 0; k < student_layout.getChildCount(); k++) {

            View view1 = student_layout.getChildAt(k);
            String mark = ((EditText)view1.findViewById(R.id.et_item_marks)).getText().toString().trim();
            if ("".equalsIgnoreCase(mark)) {
                continue;
            }
                if (Float.parseFloat(mark) > maxMarks) {
                    Config.responseSnackBarHandler(getString(R.string.should_not_be_greater_than_maximum_marks),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                String admissionid = ((TextView) view1.findViewById(R.id.tv_admission_id)).getText().toString();
                String grade = ((TextView) view1.findViewById(R.id.tv_grade)).getText().toString();
                String resultValue = ((TextView) view1.findViewById(R.id.tv_result)).getText().toString();
                String result = resultList.get(resultValue);

                studentIdJson.put(admissionid);
                marksJson.put(mark);
                gradeJson.put(grade);
                resultJson.put(result);

        }

        if (studentIdJson.length() == 0){
            Config.responseSnackBarHandler(getString(R.string.students_not_available_or_no_marks_filled),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }


        if (marksJson.length() == 0){
            Config.responseSnackBarHandler(getString(R.string.fill_marks),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }

        if (gradeJson.length() == 0){
            Config.responseSnackBarHandler(getString(R.string.write_grade_name),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }

        if (resultJson.length() == 0){
            Config.responseSnackBarHandler(getString(R.string.result_not_available),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        JSONObject param = new JSONObject();
        param.put("Exam_Type", examtypeid);
        param.put("Section_Id",sectionid);
        param.put("Subject_Id", subjectid);
//        param.put("DateOfExam", formatter.format( new Date()));
        param.put("Max_Marks", maxMarks);
        param.put("Remarks","");
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("Student_Id", studentIdJson);
        param.put("Marks_Obtain", marksJson);
        param.put("Grade",gradeJson);
        param.put("Result", resultJson);

            String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/resultEntryScholastic"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), jsonObject -> {
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

            switch (code){

                case Gc.APIRESPONSE200:

                    new Thread(() -> {
                        try {
                            Config.responseSnackBarHandler(jsonObject.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
                            Thread.sleep(1000);
                            finish();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }).start();


                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, EnterMarksForClass.this);

                    Intent intent1 = new Intent(EnterMarksForClass.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, EnterMarksForClass.this);

                    Intent intent2 = new Intent(EnterMarksForClass.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(EnterMarksForClass.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    void getScholasticResultForClassSubjectExam(){

        progressBar.setVisibility(View.VISIBLE);

        JSONObject p = new JSONObject();
        try {
            p.put("Exam_Type",examtypeid);
            p.put("Section_Id", sectionid);
            p.put("Subject_Id",subjectid);
            p.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "exam/resultEntryScholastic/"
                    + p
                    ;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), jsonObject -> {
                progressBar.setVisibility(View.GONE);
                String code = jsonObject.optString("code");

                switch (code){

                    case Gc.APIRESPONSE200:

                        try {
                            ArrayList<ScholasticResultEntity> scholasticResults = new Gson()
                                    .fromJson(jsonObject.getJSONObject("result").optString("resultEntryScholastic"),
                                            new TypeToken<List<ScholasticResultEntity>>(){}.getType());


                            if (scholasticResultEntities != null){
                                for (int i = 0; i < scholasticResultEntities.size(); i++) {
                                    LayoutInflater layoutInflater = LayoutInflater.from(this);

                                    View view = layoutInflater.inflate(R.layout.fill_marks_for_class_student, null);

                                    TextView tv_item_student_name = view.findViewById(R.id.tv_item_student_name);
                                    tv_item_student_name.setText(scholasticResultEntities.get(i).StudentName);


                                    TextView tv_admission_id = view.findViewById(R.id.tv_admission_id);
                                    tv_admission_id.setText(scholasticResultEntities.get(i).AdmissionId);


                                    EditText et_item_marks = view.findViewById(R.id.et_item_marks);
                                    et_item_marks.setText(Strings.nullToEmpty(scholasticResultEntities.get(i).Marks_Obtain));


                                    TextView tv_grade = view.findViewById(R.id.tv_grade);
                                    tv_grade.setText(scholasticResultEntities.get(i).Grade);

                                    TextView tv_result = view.findViewById(R.id.tv_result);
                                    tv_result.setText(scholasticResultEntities.get(i).Result);

                                    TextView resultMasterEntry = view.findViewById(R.id.resultMasterEntry);
                                    resultMasterEntry.setText(scholasticResultEntities.get(i).ResultValue);

                                    et_item_marks.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            if (count == 0){
                                                tv_grade.setText(null);
                                                tv_result.setText(null);
                                                return;
                                            }
                                            int marks = Integer.parseInt(s.toString());
                                            if (marks > maxMarks){
                                                Config.responseSnackBarHandler(getString(R.string.should_not_be_greater_than_maximum_marks),
                                                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                                                return;
                                            }else{
                                                ArrayList<String> grademarks = getGrade(marks);

                                                if(grademarks.size() == 3 ) {
                                                    tv_grade.setText(grademarks.get(0));
                                                    tv_result.setText(grademarks.get(1));
                                                    resultMasterEntry.setText(grademarks.get(2));
                                                }else{
                                                    Config.responseSnackBarHandler(getString(R.string.scholastic_grade_marks_setup_not_correct),
                                                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                                                }
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });


                                    student_layout.addView(view);
                                }

                            }else{
                                Config.responseSnackBarHandler(getString(R.string.students_not_available),
                                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, EnterMarksForClass.this);

                        Intent intent1 = new Intent(EnterMarksForClass.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, EnterMarksForClass.this);

                        Intent intent2 = new Intent(EnterMarksForClass.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        progressBar.setVisibility(View.GONE);
                        Config.responseSnackBarHandler(jsonObject.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }, error -> {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(EnterMarksForClass.this,error,findViewById(R.id.top_layout));

            }){
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
        }


        @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void validateIntentData(){
       if(getIntent().getStringExtra("sectionid") == null
               || getIntent().getStringExtra("subjectid") == null
               || getIntent().getStringExtra("examtype") == null
               || getIntent().getStringExtra("maxmarks") == null
               || getIntent().getStringExtra("classsectionname") == null
               || getIntent().getStringExtra("subjectname") == null
               || getIntent().getStringExtra("result") == null

       ){
           finish();
       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:

                try {
                    saveScholasticMarks();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
        return super.onOptionsItemSelected(item);

    }

    ArrayList<String> getGrade(int marks){
        ArrayList<String> gradeResult = new ArrayList<>();
        for (GradeMarksEntity g: gradeMarksList){
            if (marks <= Float.parseFloat( g.rangeStart) && marks >= Float.parseFloat( g.rangeEnd) ){
                gradeResult.add(g.gradeValue);
                gradeResult.add(g.resultValue);
                gradeResult.add(g.gradeResult);
                return gradeResult;
            }
        }
        return gradeResult;
    }

    void build_Layout(){
        if (scholasticResultEntities != null){
            for (int i = 0; i < scholasticResultEntities.size(); i++) {
                LayoutInflater layoutInflater = LayoutInflater.from(this);

                View view = layoutInflater.inflate(R.layout.fill_marks_for_class_student, null);

                TextView tv_item_student_name = view.findViewById(R.id.tv_item_student_name);
                tv_item_student_name.setText(scholasticResultEntities.get(i).StudentName);


                TextView tv_admission_id = view.findViewById(R.id.tv_admission_id);
                tv_admission_id.setText(scholasticResultEntities.get(i).AdmissionId);


                EditText et_item_marks = view.findViewById(R.id.et_item_marks);
                et_item_marks.setText(Strings.nullToEmpty(scholasticResultEntities.get(i).Marks_Obtain));


                TextView tv_grade = view.findViewById(R.id.tv_grade);
                tv_grade.setText(scholasticResultEntities.get(i).Grade);

                TextView tv_result = view.findViewById(R.id.tv_result);
                tv_result.setText(scholasticResultEntities.get(i).ResultValue);

                TextView resultMasterEntry = view.findViewById(R.id.resultMasterEntry);
                resultMasterEntry.setText(scholasticResultEntities.get(i).Result);

                et_item_marks.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (count == 0){
                            tv_grade.setText(null);
                            tv_result.setText(null);
                            return;
                        }
                        int marks = Integer.parseInt(s.toString());
                        if (marks > maxMarks){
                            Config.responseSnackBarHandler(getString(R.string.should_not_be_greater_than_maximum_marks),
                                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
                            return;
                        }else{
                            ArrayList<String> grademarks = getGrade(marks);

                            if(grademarks.size() == 3 ) {
                                tv_grade.setText(grademarks.get(0));
                                tv_result.setText(grademarks.get(1));
                                resultMasterEntry.setText(grademarks.get(2));
                            }else{
                                Config.responseSnackBarHandler(getString(R.string.scholastic_grade_marks_setup_not_correct),
                                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                student_layout.addView(view);
            }

        }else{
            Config.responseSnackBarHandler(getString(R.string.students_not_available),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
        }
    }

}
