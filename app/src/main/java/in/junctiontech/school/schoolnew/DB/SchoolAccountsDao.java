package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;
@Dao
public interface SchoolAccountsDao {
    @Query("select * from SchoolAccountsEntity")
    LiveData<List<SchoolAccountsEntity>> getSchoolAccounts();

    @Insert(onConflict = REPLACE)
    void insertSchoolAccount(List<SchoolAccountsEntity> schoolAccountsEntity);
}
