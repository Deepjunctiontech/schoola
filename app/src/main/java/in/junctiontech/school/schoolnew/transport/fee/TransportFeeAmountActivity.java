package in.junctiontech.school.schoolnew.transport.fee;

import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;

public class TransportFeeAmountActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;

    LinearLayout ll_transport_fee_amount_fixed;

    FloatingActionButton fb_back;

    ArrayList<MasterEntryEntity> transportDistanceList = new ArrayList<>();

    final String DISTANCE = "Distance";
    final String DISTANCE_WISE = "distancewise";
    final String FIXED = "fixed";

    Boolean isSaved = false;

    RecyclerView mRecyclerView ;
    TransportFeeDistanceAdapter adapter;

    ArrayList<FeeTypeModel> feeTypeModel;
    String transportFeeType;
    String feeTypeId ;

    ArrayList<TransportDistanceAmount> distanceAmounts = new ArrayList<>();

    EditText et_fixed_amount;

    int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport_fee_amount);
        progressBar = findViewById(R.id.progressBar);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        ll_transport_fee_amount_fixed = findViewById(R.id.ll_transport_fee_amount_fixed);
        et_fixed_amount = findViewById(R.id.et_fixed_amount);

        fb_back = findViewById(R.id.fb_back_todistance);
        fb_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.rv__transport_fee__distance);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new TransportFeeDistanceAdapter(this, distanceAmounts);
        mRecyclerView.setAdapter(adapter);

       if("fixed".equalsIgnoreCase(getIntent().getStringExtra("type"))) {
           mRecyclerView.setVisibility(View.GONE);
           ll_transport_fee_amount_fixed.setVisibility(View.VISIBLE);
       }
       else ll_transport_fee_amount_fixed.setVisibility(View.GONE);

       transportFeeType = getIntent().getStringExtra("type");
       feeTypeId = getIntent().getStringExtra("feetypeid");

       feeTypeModel = new Gson().fromJson(getIntent().getStringExtra("fees"),new TypeToken<List<FeeTypeModel>>() {
        }.getType());

       setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1041584354", this, adContainer);
        }
    }

    @Override
    public void onBackPressed() {
        if(isSaved){super.onBackPressed();return;};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.first_save_transport_fee);
// Add the buttons
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                // User clicked OK button
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
                // User cancelled the dialog
            }
        });
// Set other dialog properties

// Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

//        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mDb.masterEntryModel().getMasterEntryValuesGeneric(DISTANCE).observe(this, new Observer<List<MasterEntryEntity>>() {
            @Override
            public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                transportDistanceList.clear();
                transportDistanceList.addAll(masterEntryEntities);
                distanceAmounts.clear();
                if(transportFeeType.equalsIgnoreCase(DISTANCE_WISE)) {
                    if (feeTypeModel != null) {

                        ArrayList<TransportDistanceAmount> tds = new ArrayList<>();
                        for (MasterEntryEntity d : transportDistanceList) {
                            TransportDistanceAmount td = new TransportDistanceAmount();
                            td.MasterEntryValue = d.MasterEntryValue;
                            td.MasterEntryId = d.MasterEntryId;
                            for (FeeTypeModel f : feeTypeModel) {
                                if (f.Distance.equalsIgnoreCase(d.MasterEntryId)) {
                                    td.Amount = f.Amount;
                                    td.FeeType = f.FeeType;
                                }
                            }
                            tds.add(td);
                        }
                        distanceAmounts.addAll(tds);
                        adapter.notifyDataSetChanged();
                    }

                }else if (transportFeeType.equalsIgnoreCase(FIXED)){
                    if(feeTypeModel != null && feeTypeModel.size() >0) {
                        et_fixed_amount.setText(feeTypeModel.get(0).Amount);
                    }
                }

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                try {
                    saveTransportFeeAmount();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void saveTransportFeeAmount() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);
        final JSONObject param = new JSONObject();

        param.put("dataType","transportFee");
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION,this));
        param.put("FeeTypeID", feeTypeId);

        if(transportFeeType.equalsIgnoreCase(FIXED)){
            param.put("Type","Fixed");
            param.put("Amount", et_fixed_amount.getText().toString().trim());

        }else if (transportFeeType.equalsIgnoreCase(DISTANCE_WISE)){
            ArrayList<TransportDistanceAmount> allvalues = adapter.getMasterEntryEntities();
            param.put("Type","DistanceWise");

            JSONArray jamount = new JSONArray();
            JSONArray jdistance = new JSONArray();

            for(TransportDistanceAmount a: allvalues){
                jamount.put(a.Amount);
                jdistance.put(a.MasterEntryId);
            }
            param.put("Amount",jamount);
            param.put("distance", jdistance);

        }


        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/transportFee"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            JSONArray classFeesjarr = resultjobj.getJSONArray("classFees");

                            if (classFeesjarr.length() > 0) {

                                final ArrayList<ClassFeesEntity> classFeesEntities = MapModelToEntity.mapClassFeeModelToEntity(classFeesjarr);
                                final ArrayList<ClassFeesTransportEntity> classFeesTransportEntities = MapModelToEntity.mapClassFeeTransportToEntity(classFeesjarr);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.classFeesModel().deleteAll();
                                        mDb.classFeesTransportModel().deleteAll();
                                        mDb.classFeesModel().insertClassFee(classFeesEntities);
                                        mDb.classFeesTransportModel().insertClassFee(classFeesTransportEntities);
                                    }
                                }).start();
                            }

//                            final ArrayList<TransportFeeAmountEntity> transportAmounts = new Gson()
//                                    .fromJson(resultjobj.optString("Fees"),
//                                            new TypeToken<List<TransportFeeAmountEntity>>(){}.getType());
//
//                            if (transportAmounts!=null && transportAmounts.size()>0){
//
//                                new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        mDb.transportFeeAmountModel().deleteAll();
//                                        mDb.transportFeeAmountModel().insertTransportFee(transportAmounts);
//                                    }
//                                }).start();
//                            }


                            isSaved=true;

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, TransportFeeAmountActivity.this);

                        Intent intent1 = new Intent(TransportFeeAmountActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, TransportFeeAmountActivity.this);

                        Intent intent2 = new Intent(TransportFeeAmountActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(TransportFeeAmountActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);



    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
