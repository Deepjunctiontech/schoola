package in.junctiontech.school.schoolnew.messaging.MsgAdapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class StudentListAdapter  extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {
    private ArrayList<SchoolStudentEntity> studentList= new ArrayList<>();

    private Context context;
    private int appColor;

    public StudentListAdapter(Context context, ArrayList<SchoolStudentEntity> studentList, int appColor) {

        this.studentList = null==studentList?new ArrayList<SchoolStudentEntity>():studentList;
        this.context = context;
        this.appColor = appColor;

    }

    @Override
    public StudentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
        return new StudentListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final StudentListAdapter.MyViewHolder holder, int position) {
        final SchoolStudentEntity studentObj = studentList.get(position);
        String next = "<font color='#727272'>" + "(" + studentObj.AdmissionNo + ")" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(studentObj.StudentName .replace("_"," ")+ " " + next));
 //      holder.tv_item_name.setText(studentObj.getStudentName().replace("_"," "));
//
//        holder.tv_designation.setText(context.getString(R.string.class_text)+
//                " : "+ studentObj.getClassName()+" "+studentObj.getSectionName());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void updateList(ArrayList<SchoolStudentEntity> studentListData) {
        this.studentList=studentListData;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView tv_item_name;
        //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
        // Button btn_item_create_class_add_section;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_slot_start_time);
           tv_item_name.setTextColor(appColor);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ((ConversationListActivity)context).
//                            startChat(
//                                    studentList.get(getLayoutPosition()),
//                                    "Student",
//                                    true // we use false for we will  refresh chat list on back
//                                    //pressed
//                            );
                }
            });



        }
    }

    public void setFilter(ArrayList<SchoolStudentEntity> studentList) {
        this.studentList = new ArrayList<>();
        this.studentList.addAll(studentList);
        notifyDataSetChanged();
    }


}


