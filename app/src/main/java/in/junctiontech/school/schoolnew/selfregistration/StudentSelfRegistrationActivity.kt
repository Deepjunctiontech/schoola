package `in`.junctiontech.school.schoolnew.selfregistration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MasterEntryEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.ClassNameSectionName
import `in`.junctiontech.school.schoolnew.model.SchoolClasses
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class StudentSelfRegistrationActivity : AppCompatActivity() {
    var progressBar: ProgressBar? = null

    private lateinit var etStudentName: EditText
    private lateinit var btnStudentSelectClass: Button
    private lateinit var btnStudentSelectGender: Button
    private lateinit var btnStudentSelfRegister: Button

    private var mobile: String? = null
    private var idToken: String? = null

    var sectionList = ArrayList<String>()
    var classNameSectionList: ArrayList<ClassNameSectionName> = ArrayList()
    private var classListBuilder: AlertDialog.Builder? = null
    var classSectionAdapter: ArrayAdapter<String>? = null

    var genderList = ArrayList<String>()
    var genderListMaster: ArrayList<MasterEntryEntity> = ArrayList()
    private var genderListBuilder: AlertDialog.Builder? = null
    var genderListAdapter: ArrayAdapter<String>? = null
    private var selectedSection = 0
    private var selectedGender = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_self_student)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mobile = intent.getStringExtra("mobile")
        idToken = intent.getStringExtra("idToken")

        initializeViews()
        getClassSections()
        getGender()
    }

    private fun initializeViews() {

        progressBar = findViewById(R.id.progressBar)
        etStudentName = findViewById(R.id.et_student_name)
        btnStudentSelectClass = findViewById(R.id.btn_student_select_class)

        /*  Class Section Select Adapter */

        classListBuilder = AlertDialog.Builder(this)
        classSectionAdapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice)
        classSectionAdapter!!.addAll(sectionList)

        btnStudentSelectClass.setOnClickListener {
            classListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            classListBuilder!!.setAdapter(classSectionAdapter) { _: DialogInterface?, i: Int ->
                btnStudentSelectClass.text = sectionList[i]
                selectedSection = i
            }
            val dialog: AlertDialog = classListBuilder!!.create()
            dialog.show()
        }

        /*  Gender Select Adapter */
        btnStudentSelectGender = findViewById(R.id.btn_student_select_gender)
        genderListBuilder = AlertDialog.Builder(this)
        genderListAdapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice)
        genderListAdapter!!.addAll(genderList)
        btnStudentSelectGender.setOnClickListener {
            genderListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            genderListBuilder!!.setAdapter(genderListAdapter) { _: DialogInterface?, i: Int ->
                btnStudentSelectGender.text = genderList[i]
                selectedGender = i
            }
            val dialog: AlertDialog = genderListBuilder!!.create()
            dialog.show()
        }

        btnStudentSelfRegister = findViewById(R.id.btn_student_self_register)


        btnStudentSelfRegister.setOnClickListener {
            if (validateInput()) {
                try {
                    staffSelfRegistration()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun getClassSections() {


        progressBar!!.visibility = View.VISIBLE

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "selfRegistration/getSelfRegistrationClassSection")

        Log.e("url", url)
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val schoolClassesArrayList: ArrayList<SchoolClasses> = Gson()
                            .fromJson<ArrayList<SchoolClasses>>(job.getJSONObject("result").optString("classSections"),
                                    object : TypeToken<List<SchoolClasses?>?>() {}.type)

                    val classesArrList = JSONArray()

                    for (cl in schoolClassesArrayList) {
                        if (cl.section.size > 0) {
                            for (sec in cl.section) {
                                val classes = JSONObject()
                                try {
                                    classes.put("ClassId", sec.ClassId)
                                    classes.put("ClassName", cl.ClassName)
                                    classes.put("SectionId", sec.SectionId)
                                    classes.put("SectionName", sec.SectionName)
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }

                                classesArrList.put(classes)
                            }
                        }
                    }

                    if (classesArrList.length() > 0) {
                        val classNameSectionNames: ArrayList<ClassNameSectionName> = Gson()
                                .fromJson<ArrayList<ClassNameSectionName>>(classesArrList.toString(),
                                        object : TypeToken<List<ClassNameSectionName?>?>() {}.type)

                        classNameSectionList.clear()
                        sectionList.clear()
                        classSectionAdapter!!.clear()
                        classNameSectionList.addAll(classNameSectionNames)
                        for (i in classNameSectionList.indices) {
                            sectionList.add(classNameSectionList[i].ClassName + " " + classNameSectionList[i].SectionName)
                        }
                        classSectionAdapter!!.addAll(sectionList)
                        classSectionAdapter!!.notifyDataSetChanged()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_student_self_registration))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }

    private fun getGender() {
        progressBar!!.visibility = View.VISIBLE
        val p = JSONObject()
        p.put("MasterEntryName", "Gender")
        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/" + p)

        Log.e("url", url)
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val masterEntity = Gson()
                            .fromJson<ArrayList<MasterEntryEntity>>(job.getJSONObject("result").optString("masterEntry"), object : TypeToken<List<MasterEntryEntity>>() {

                            }.type)
                    genderListMaster.clear()
                    genderList.clear()
                    genderListMaster.addAll(masterEntity)
                    for (i in genderListMaster.indices) {
                        genderList.add(genderListMaster[i].MasterEntryValue)
                    }
                    genderListAdapter!!.clear()
                    genderListAdapter!!.addAll(genderList)
                    genderListAdapter!!.notifyDataSetChanged()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_student_self_registration))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }

    private fun validateInput(): Boolean {
        if ("" == etStudentName.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.name_can_not_be_blank),
                    findViewById(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            etStudentName.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.select_class) == btnStudentSelectClass.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.select_class) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            btnStudentSelectClass.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.gender) == btnStudentSelectGender.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.gender) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            btnStudentSelectGender.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (mobile.isNullOrEmpty() || mobile!!.length > 15 || mobile!!.length < 5) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_mobile_mumber),
                    findViewById(R.id.rl_student_self_registration), R.color.fragment_first_green)
            return false
        }

        return true
    }

    @Throws(JSONException::class)
    private fun staffSelfRegistration() {
        progressBar!!.visibility = View.VISIBLE
        val param = JSONObject()
        param.put("Gender", genderListMaster[selectedGender].MasterEntryId)
        param.put("SectionId", classNameSectionList[selectedSection].SectionId)
        param.put("StudentName", etStudentName.text.toString().trim { it <= ' ' })
        param.put("Mobile", mobile)
        // Log.e("param", param.toString());
        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "selfRegistration/studentSelfRegistration")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            progressBar!!.visibility = View.GONE
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> try {
                    val intent = Intent()
                    intent.putExtra("userId", job.optString("result"))
                    intent.putExtra("name", etStudentName.text.toString().trim { it <= ' ' })
                    intent.putExtra("mobile", mobile)
                    intent.putExtra("userType", "student")
                    intent.putExtra("idToken", idToken)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_student_self_registration), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error: VolleyError? ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById(R.id.rl_student_self_registration))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}