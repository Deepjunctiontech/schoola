package in.junctiontech.school.schoolnew.owner;

import android.graphics.Paint;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class AddOrganizationActivity extends AppCompatActivity {

    private int  colorIs;
    private Button  btn_request_otp,btn_add_organization;
    private EditText  et_add_organization_otp, et_add_organization_registered_email,et_add_organization_password,
            et_add_organization_user_id,et_add_organization_organization_key;
    private TextView tv_add_organization_selected_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_organization);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        colorIs = Config.getAppColor(this, true);


        btn_add_organization = (Button)findViewById(R.id.btn_add_organization_add_organization);
        btn_request_otp = (Button)findViewById(R.id.btn_add_organization_request_otp);
        btn_request_otp.setBackgroundColor(colorIs);
        btn_add_organization.setBackgroundColor(colorIs);


        ((TextView) findViewById(R.id.tv_add_organization_back_color)).setBackgroundColor(colorIs);
        tv_add_organization_selected_text= (TextView) findViewById(R.id.tv_add_organization_selected_text);
               // .setBackgroundColor(colorIs);

        et_add_organization_otp= (EditText)findViewById(R.id.et_add_organization_otp);
        et_add_organization_registered_email= (EditText)findViewById(R.id.et_add_organization_registered_email);
        et_add_organization_password= (EditText)findViewById(R.id.et_add_organization_password);
        et_add_organization_user_id= (EditText)findViewById(R.id.et_add_organization_user_id);
        et_add_organization_organization_key= (EditText)findViewById(R.id.et_add_organization_organization_key);

        CheckBox ck_add_organization= (CheckBox)findViewById(R.id.ck_add_organization);
        ck_add_organization.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        ck_add_organization.setTextColor(colorIs);
        ck_add_organization.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){// through otp
                    buttonView.setText("Add through User id/password");
                    tv_add_organization_selected_text.setText("OTP");

                    et_add_organization_registered_email.setVisibility(View.VISIBLE);
                    et_add_organization_otp.setVisibility(View.GONE);
                    btn_request_otp.setVisibility(View.VISIBLE);
                    btn_add_organization.setVisibility(View.GONE);
                    et_add_organization_password.setVisibility(View.GONE);
                    et_add_organization_user_id.setVisibility(View.GONE);
                    et_add_organization_organization_key.setVisibility(View.GONE);
                }else {// through User id pwd
                    buttonView.setText("Add through OTP");
                    tv_add_organization_selected_text.setText("User id/password");

                    et_add_organization_registered_email.setVisibility(View.GONE);
                    et_add_organization_otp.setVisibility(View.GONE);
                    btn_request_otp.setVisibility(View.GONE);
                    btn_add_organization.setVisibility(View.VISIBLE);
                    et_add_organization_password.setVisibility(View.VISIBLE);
                    et_add_organization_user_id.setVisibility(View.VISIBLE);
                    et_add_organization_organization_key.setVisibility(View.VISIBLE);
                }
            }
        });


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
