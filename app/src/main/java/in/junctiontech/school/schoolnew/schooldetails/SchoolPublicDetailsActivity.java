package in.junctiontech.school.schoolnew.schooldetails;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.communicate.SendSMSActivity;
import in.junctiontech.school.schoolnew.registration.CityData;
import in.junctiontech.school.schoolnew.registration.CountryData;
import in.junctiontech.school.schoolnew.registration.StateData;

public class SchoolPublicDetailsActivity extends AppCompatActivity {

    MainDatabase mDb;

    SchoolDetailsEntity schoolDetails = new SchoolDetailsEntity();
    EditText et_general_setting_school_name,
            et_general_setting_school_moto,
            et_general_setting_school_board,
            et_general_setting_affiliated_by,
            et_general_setting_affiliated_number,
            et_general_setting_school_registration_number,
            et_general_setting_address,
            et_school_detail_pin_code,
            et_school_detail_country_code,
            et_school_detail_phone_number,
            et_school_detail_alternative_country_code,
            et_school_detail_alternative_phone_number,
            et_school_detail_landline_country_code,
            et_school_detail_landline_number,
            et_school_detail_school_email;

    Button btn_date_of_establishment, btn_school_country, btn_school_state, btn_school_city;
    int selectedCountryPosition = 0;
    int selectedStatePosition=-1, selectedCityPosition = -1;

    ArrayList<String> countryNameList = new ArrayList<>();
    ArrayList<CountryData> countryList = new ArrayList<>();
    AlertDialog.Builder countryListBuilder;
    ArrayAdapter<String> countryListAdapter;

    ArrayList<String> stateNameList = new ArrayList<>();
    ArrayList<StateData> stateList = new ArrayList<>();
    AlertDialog.Builder stateListBuilder;
    ArrayAdapter<String> stateListAdapter;

    ArrayList<String> cityNameList = new ArrayList<>();
    ArrayList<CityData> cityList = new ArrayList<>();
    AlertDialog.Builder cityListBuilder;
    ArrayAdapter<String> cityListAdapter;

    private String firstTimeSetup;

    int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_date_of_establishment.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_public_detail);

        mDb = MainDatabase.getDatabase(this);

        firstTimeSetup = getIntent().getStringExtra(Gc.SETUP);

        if (Gc.getSharedPreference(Gc.SCHOOLDETAILEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        initializeAllViews();
        setTextInViews();
        fetchCountry();
        setColorApp();
    }

    private void setTextInViews() {
        et_general_setting_school_name.setText(Strings.nullToEmpty(schoolDetails.SchoolName));
        et_general_setting_school_moto.setText(Strings.nullToEmpty(schoolDetails.SchoolMoto));
        et_general_setting_school_board.setText(Strings.nullToEmpty(schoolDetails.Board));
        et_general_setting_affiliated_by.setText(Strings.nullToEmpty(schoolDetails.AffiliatedBy));
        et_general_setting_affiliated_number.setText(Strings.nullToEmpty(schoolDetails.AffiliationNo));
        et_general_setting_school_registration_number.setText(Strings.nullToEmpty(schoolDetails.RegistrationNo));

        btn_school_country.setText(Strings.nullToEmpty(schoolDetails.Country));
        btn_school_state.setText(Strings.nullToEmpty(schoolDetails.State));
        btn_school_city.setText(Strings.nullToEmpty(schoolDetails.City));

        btn_date_of_establishment.setText(
                "".equals(Strings.nullToEmpty(schoolDetails.DateOfEstablishment))
                        ? getString(R.string.date_of_stablishment)
                        : Strings.nullToEmpty(schoolDetails.DateOfEstablishment)
        );

        et_general_setting_address.setText(Strings.nullToEmpty(schoolDetails.SchoolAddress));
        et_school_detail_pin_code.setText(Strings.nullToEmpty(schoolDetails.PIN));

        HashMap<String, String> split = Gc.splitPhoneNumber(Strings.nullToEmpty(schoolDetails.Mobile));
        et_school_detail_country_code.setText(split.get("ccode"));
        et_school_detail_phone_number.setText(split.get("phone"));

        split.clear();
        split = Gc.splitPhoneNumber(Strings.nullToEmpty(schoolDetails.AlternateMobile));
        et_school_detail_alternative_country_code.setText(split.get("ccode"));
        et_school_detail_alternative_phone_number.setText(split.get("phone"));

        split.clear();
        split = Gc.splitPhoneNumber(Strings.nullToEmpty(schoolDetails.Landline));
        et_school_detail_landline_country_code.setText(split.get("ccode"));
        et_school_detail_landline_number.setText(split.get("phone"));

        et_school_detail_school_email.setText(Strings.nullToEmpty(schoolDetails.Email));
    }

    private void initializeAllViews() {

        btn_school_country = findViewById(R.id.btn_school_country);
        countryListBuilder = new AlertDialog.Builder(this);
        countryListAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        countryListAdapter.addAll(countryNameList);
        btn_school_country.setOnClickListener(view -> {
            countryListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            countryListBuilder.setAdapter(countryListAdapter, (dialogInterface, i) -> {
                btn_school_country.setText(countryNameList.get(i));
                selectedCountryPosition = i;
                btn_school_state.setText(getString(R.string.state));
                btn_school_city.setText(getString(R.string.city));
                selectedStatePosition = -1;
                selectedCityPosition = -1;

                stateList.clear();
                stateNameList.clear();
                stateListAdapter.clear();
                stateListAdapter.addAll(stateNameList);
                stateListAdapter.notifyDataSetChanged();

                cityList.clear();
                cityNameList.clear();
                cityListAdapter.clear();
                cityListAdapter.addAll(cityNameList);
                cityListAdapter.notifyDataSetChanged();

                fetchState();
            });
            AlertDialog dialog = countryListBuilder.create();
            dialog.show();
        });

        btn_school_state = findViewById(R.id.btn_school_state);
        stateListBuilder = new AlertDialog.Builder(this);
        stateListAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        stateListAdapter.addAll(stateNameList);
        btn_school_state.setOnClickListener(view -> {
            stateListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            stateListBuilder.setAdapter(stateListAdapter, (dialogInterface, i) -> {
                btn_school_state.setText(stateNameList.get(i));
                selectedStatePosition = i;

                btn_school_city.setText(getString(R.string.city));
                selectedCityPosition = -1;
                cityList.clear();
                cityNameList.clear();
                cityListAdapter.clear();
                cityListAdapter.addAll(cityNameList);
                cityListAdapter.notifyDataSetChanged();

                fetchCity();
            });
            AlertDialog dialog = stateListBuilder.create();
            dialog.show();
        });

        btn_school_city = findViewById(R.id.btn_school_city);
        cityListBuilder = new AlertDialog.Builder(this);
        cityListAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        cityListAdapter.addAll(cityNameList);
        btn_school_city.setOnClickListener(view -> {
            cityListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            cityListBuilder.setAdapter(cityListAdapter, (dialogInterface, i) -> {
                btn_school_city.setText(cityNameList.get(i));
                selectedCityPosition = i;
            });
            AlertDialog dialog = cityListBuilder.create();
            dialog.show();
        });

        et_general_setting_school_name = findViewById(R.id.et_general_setting_school_name);
        et_general_setting_school_moto = findViewById(R.id.et_general_setting_school_moto);
        et_general_setting_school_board = findViewById(R.id.et_general_setting_school_board);
        et_general_setting_affiliated_by = findViewById(R.id.et_general_setting_affiliated_by);
        et_general_setting_affiliated_number = findViewById(R.id.et_general_setting_affiliated_number);
        et_general_setting_school_registration_number = findViewById(R.id.et_general_setting_school_registration_number);
        btn_date_of_establishment = findViewById(R.id.btn_date_of_establishment);
        et_general_setting_address = findViewById(R.id.et_general_setting_address);
        et_school_detail_pin_code = findViewById(R.id.et_school_detail_pin_code);
        et_school_detail_country_code = findViewById(R.id.et_school_detail_country_code);
        et_school_detail_phone_number = findViewById(R.id.et_school_detail_phone_number);
        et_school_detail_alternative_country_code = findViewById(R.id.et_school_detail_alternative_country_code);
        et_school_detail_alternative_phone_number = findViewById(R.id.et_school_detail_alternative_phone_number);
        et_school_detail_landline_country_code = findViewById(R.id.et_school_detail_landline_country_code);
        et_school_detail_landline_number = findViewById(R.id.et_school_detail_landline_number);
        et_school_detail_school_email = findViewById(R.id.et_school_detail_school_email);

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener feedate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

                btn_date_of_establishment.setText(sdf.format(myCalendar.getTime()));
            }

        };

        btn_date_of_establishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(SchoolPublicDetailsActivity.this, feedate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


    }

    private void fetchCity() {
        String url = Gc.ERPCPANELURL + "CitysDetailApi";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");
            switch (code) {
                case "200":
                    ArrayList<CityData> cityListData = new Gson()
                            .fromJson(job.optString("result"), new TypeToken<List<CityData>>() {
                            }.getType());
                    cityList.clear();
                    cityNameList.clear();
                    cityList.addAll(cityListData);
                    for (int i = 0; i < cityList.size(); i++) {
                        if (Strings.nullToEmpty(schoolDetails.City).equalsIgnoreCase(cityList.get(i).getToponymName())) {
                            btn_school_city.setText(cityList.get(i).getToponymName());
                            selectedCityPosition = i;
                        }
                        cityNameList.add(cityList.get(i).getToponymName());
                    }
                    cityListAdapter.clear();
                    cityListAdapter.addAll(cityNameList);
                    cityListAdapter.notifyDataSetChanged();
                    break;
                case "401":
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
            }
        }, error -> Config.responseVolleyErrorHandler(SchoolPublicDetailsActivity.this, error, findViewById(R.id.rl_school_public_details))) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                JSONObject jobFilter = new JSONObject();
                try {
                    jobFilter.put("citynameId", stateList.get(selectedStatePosition).getStategeonameId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                headers.put("filter", jobFilter.toString());
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void fetchState() {
        String url = Gc.ERPCPANELURL + "StatesDetailApi";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");
            switch (code) {
                case "200":
                    ArrayList<StateData> stateListData = new Gson()
                            .fromJson(job.optString("result"), new TypeToken<List<StateData>>() {
                            }.getType());
                    stateList.clear();
                    stateNameList.clear();
                    stateList.addAll(stateListData);
                    for (int i = 0; i < stateList.size(); i++) {
                        if (Strings.nullToEmpty(schoolDetails.State).equalsIgnoreCase(stateList.get(i).getName())) {
                            btn_school_state.setText(stateList.get(i).getName());
                            selectedStatePosition = i;
                            fetchCity();
                        }
                        stateNameList.add(stateList.get(i).getName());
                    }
                    stateListAdapter.clear();
                    stateListAdapter.addAll(stateNameList);
                    stateListAdapter.notifyDataSetChanged();
                    break;
                case "401":
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
            }
        }, error -> Config.responseVolleyErrorHandler(SchoolPublicDetailsActivity.this, error, findViewById(R.id.rl_school_public_details))) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                JSONObject jobFilter = new JSONObject();
                try {
                    jobFilter.put("geonameId", countryList.get(selectedCountryPosition).getGeonameId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                headers.put("filter", jobFilter.toString());
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void fetchCountry() {
        String url = Gc.ERPCPANELURL + "CountrysDetailApi";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");
                switch (code) {
                    case Gc.APIRESPONSE200:
                        ArrayList<CountryData> countryListData = new Gson()
                                .fromJson(job.optString("result"), new TypeToken<List<CountryData>>() {
                                }.getType());
                        countryList.clear();
                        countryNameList.clear();
                        countryList.addAll(countryListData);
                        for (int i = 0; i < countryList.size(); i++) {
                            if (!(Strings.nullToEmpty(schoolDetails.Country).isEmpty()) && schoolDetails.Country.equals(countryList.get(i).getCountryName())) {
                                btn_school_country.setText(countryList.get(i).getCountryName());
                                selectedCountryPosition = i;
                                fetchState();
                            }
                            countryNameList.add(countryList.get(i).getCountryName());
                        }
                        countryListAdapter.clear();
                        countryListAdapter.addAll(countryNameList);
                        countryListAdapter.notifyDataSetChanged();
                        break;
                    case Gc.APIRESPONSE401:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
                }
            }
        }, error -> Config.responseVolleyErrorHandler(SchoolPublicDetailsActivity.this, error, findViewById(R.id.rl_school_public_details))) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*HashMap<String, String> headers = new HashMap<>();
                return headers;*/
                return new HashMap<>();
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    JSONObject buildJsonForUpdate() {

        String Country = null, State = null, City = null;

        if (countryList.size() > 0 && selectedCountryPosition > -1) {
            Country = Objects.requireNonNull(countryList.get(selectedCountryPosition).getCountryName()).trim();
        }

        if (stateList.size() > 0 && selectedStatePosition > -1) {
            State = Objects.requireNonNull(stateList.get(selectedStatePosition).getName()).trim();
        }

        if (cityList.size() > 0 && selectedCityPosition > -1) {
            City = Objects.requireNonNull(cityList.get(selectedCityPosition).getToponymName()).trim();
        }


        if ("".equals(et_general_setting_school_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.school_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            et_general_setting_school_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }

        if ("".equals(Country)) {
            Config.responseSnackBarHandler(getString(R.string.country) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            btn_school_country.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));

            return null;
        }

        if ("India".equals(Country) && ("".equals(State)||State == null)) {
            Config.responseSnackBarHandler(getString(R.string.state) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            btn_school_state.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }

        if ("India".equals(Country) && ("".equals(City)||City == null)) {
            Config.responseSnackBarHandler(getString(R.string.city) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            btn_school_city.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }

        if (!("".equalsIgnoreCase(et_school_detail_phone_number.getText().toString()))) {
            if ("".equalsIgnoreCase(et_school_detail_country_code.getText().toString())) {
                Config.responseSnackBarHandler(getString(R.string.country_code) + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_country_code.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));

                return null;
            }
        }

        if (!("".equalsIgnoreCase(et_school_detail_country_code.getText().toString()))) {
            if ("".equalsIgnoreCase(et_school_detail_phone_number.getText().toString())) {
                Config.responseSnackBarHandler(getString(R.string.phone_number) + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_phone_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));

                return null;
            }
        }

        if (!("".equalsIgnoreCase(et_school_detail_alternative_country_code.getText().toString()))) {
            if ("".equalsIgnoreCase(et_school_detail_alternative_phone_number.getText().toString())) {
                Config.responseSnackBarHandler(getString(R.string.alternative_phone_number) + getString(R.string.country_code_filled_but_phone_is_empty),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_alternative_phone_number.setError(getString(R.string.country_code_filled_but_phone_is_empty), getDrawable(R.drawable.ic_alert));

                return null;
            }
        }

        if (!("".equalsIgnoreCase(et_school_detail_landline_country_code.getText().toString()))) {
            if ("".equalsIgnoreCase(et_school_detail_landline_number.getText().toString())) {
                Config.responseSnackBarHandler(getString(R.string.landline_number) + getString(R.string.country_code_filled_but_phone_is_empty),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_landline_number.setError(getString(R.string.country_code_filled_but_phone_is_empty), getDrawable(R.drawable.ic_alert));

                return null;
            }
        }

        String mobile = et_school_detail_country_code.getText().toString().trim()
                + "-" + et_school_detail_phone_number.getText().toString().trim();
        if (mobile.length() > 15 || mobile.length() < 5
                || "".equals(et_school_detail_country_code.getText().toString().trim())
                || "".equals(et_school_detail_phone_number.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_Phone_mumber),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            et_school_detail_phone_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }

        String alternatePhone = et_school_detail_alternative_country_code.getText().toString().trim()
                + "-" + et_school_detail_alternative_phone_number.getText().toString().trim();
        if (alternatePhone.length() > 1) {
            if ((alternatePhone.length() > 15) || (alternatePhone.length() < 5)
                    || "".equals(et_school_detail_alternative_country_code.getText().toString().trim())
                    || "".equals(et_school_detail_alternative_phone_number.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.please_enter_valid_Phone_mumber),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_alternative_phone_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return null;
            }
        }

        String landLine = et_school_detail_landline_country_code.getText().toString().trim()
                + "-" + et_school_detail_landline_number.getText().toString().trim();
        if (landLine.length() > 1) {
            if ((landLine.length() > 15) || (landLine.length() < 5)
                    || "".equals(et_school_detail_landline_country_code.getText().toString().trim())
                    || "".equals(et_school_detail_landline_number.getText().toString().trim())) {
                Config.responseSnackBarHandler(getString(R.string.please_enter_valid_Phone_mumber),
                        findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                et_school_detail_landline_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
                return null;
            }
        }
        if ("".equals(et_school_detail_school_email.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.email_id) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            et_school_detail_school_email.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }
        if (getString(R.string.date_of_stablishment).equals(btn_date_of_establishment.getText())) {
            Config.responseSnackBarHandler(getString(R.string.date_of_stablishment) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
            btn_date_of_establishment.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return null;
        }



        JSONObject param = new JSONObject();
        try {

            param.put("SchoolName", et_general_setting_school_name.getText().toString().trim());
            param.put("SchoolMoto", et_general_setting_school_moto.getText().toString().trim());
            param.put("Board", et_general_setting_school_board.getText().toString().trim());
            param.put("AffiliatedBy", et_general_setting_affiliated_by.getText().toString().trim());
            param.put("AffiliationNo", et_general_setting_affiliated_number.getText().toString().trim());
            param.put("RegistrationNo", et_general_setting_school_registration_number.getText().toString().trim());
            param.put("Country", Country);
            param.put("State", State);
            param.put("City", City);
            param.put("DateOfEstablishment", btn_date_of_establishment.getText().toString());
            param.put("SchoolAddress", et_general_setting_address.getText().toString().trim());
            param.put("PIN", et_school_detail_pin_code.getText().toString().trim());
            param.put("Mobile", mobile);
            param.put("AlternateMobile", et_school_detail_alternative_country_code.getText().toString().trim()
                    + "-" + et_school_detail_alternative_phone_number.getText().toString().trim());
            param.put("Landline", et_school_detail_landline_country_code.getText().toString().trim()
                    + "-" + et_school_detail_landline_number.getText().toString().trim());
            param.put("Email", et_school_detail_school_email.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;
    }

    private void updateSchoolPublicDetails() throws JSONException {

        final JSONObject param = buildJsonForUpdate();

        if (param == null) return;

        JSONObject p = new JSONObject();

        int method;

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION;
        final String apiurl = "schoolDetail/schoolDetails";

        if (!(Strings.nullToEmpty(schoolDetails.Id).equals(""))) {
            p.put("id", schoolDetails.Id);
            method = Request.Method.PUT;
            url = url + apiurl + "/" + p;
        } else {
            method = Request.Method.POST;
            url = url + apiurl;
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                      //  JSONObject resultjobj = job.getJSONObject("result");
                        final SchoolDetailsEntity schoolDetailsEntity = new Gson()
                                .fromJson(job.getJSONObject("result").optString("schoolDetails"), SchoolDetailsEntity.class);

                        if (!(Strings.nullToEmpty(schoolDetailsEntity.Id).equals(""))) {
                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.EXISTS);
                            Gc.setSharedPreference(setDefaults, SchoolPublicDetailsActivity.this);

                            new Thread(() -> mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity)).start();
                            Config.responseSnackBarHandler(getString(R.string.data_added_successfully),
                                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_green);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, SchoolPublicDetailsActivity.this);

                    Intent intent1 = new Intent(SchoolPublicDetailsActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, SchoolPublicDetailsActivity.this);

                    Intent intent2 = new Intent(SchoolPublicDetailsActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;
                case "304":
                    Config.responseSnackBarHandler(getString(R.string.data_added_successfully),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
            }
        }, error -> Config.responseVolleyErrorHandler(SchoolPublicDetailsActivity.this, error, findViewById(R.id.rl_school_public_details))) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, schoolDetailsEntity -> {
            if (!(schoolDetailsEntity == null)) {
                schoolDetails = schoolDetailsEntity;
                setTextInViews();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolDetailsModel().getSchoolDetailsLive().removeObservers(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        if (Gc.SETUP.equals(firstTimeSetup)) {
            menu.findItem(R.id.menu_next).setVisible(true);
        }
        if (Gc.getSharedPreference(Gc.SCHOOLDETAILEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            menu.findItem(R.id.menu_next).setVisible(false);
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            try {
                updateSchoolPublicDetails();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        if (id == R.id.action_help) {
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


            alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                    + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                    " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                    ":<font color='#ce001f'>Again</font>"));
            alert1.setTitle(getString(R.string.help));
            alert1.setIcon(R.drawable.ic_help);
            alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

            });
            alert1.show();
        }

        if (id == R.id.menu_next) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (Gc.getSharedPreference(Gc.SCHOOLDETAILEXISTS, this).equalsIgnoreCase(Gc.EXISTS))
            super.onBackPressed();
        else
            Config.responseSnackBarHandler(getString(R.string.school_details) + " " + getString(R.string.it_is_mandatory),
                    findViewById(R.id.rl_school_public_details), R.color.fragment_first_blue);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}