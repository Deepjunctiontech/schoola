package in.junctiontech.school.schoolnew.attendance;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.Attendance;


    public class StudentAttendanceListAdapter extends RecyclerView.Adapter<StudentAttendanceListAdapter.MyViewHolder> {
        private Context context;
        private ArrayList<Attendance> attendanceArrayList;
        private int presentStudentColor;
        private int absentStudentColor;

        StudentAttendanceListAdapter(Context context, ArrayList<Attendance> attendanceArrayList) {
            this.context = context;
            this.attendanceArrayList = attendanceArrayList;
            presentStudentColor = context.getResources().getColor(android.R.color.holo_green_light);
            absentStudentColor = context.getResources().getColor(android.R.color.holo_red_light);
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_all_student, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final Attendance obj = attendanceArrayList.get(position);
            holder.item_id.setText( obj.getAdmissionNo());
            holder.name_check.setText(obj.getStudentName());


            if(!("".equals(attendanceArrayList.get(position).profileUrl)))
                Glide.with(context)
                        .load(attendanceArrayList.get(position).profileUrl)
                        .apply(new RequestOptions()
                                .error(R.drawable.ic_single)
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .apply(RequestOptions.circleCropTransform())
                        .thumbnail(0.5f)
                        .into(holder.civ_item_profile_image);
            else holder.civ_item_profile_image.setImageDrawable(context.getDrawable(R.drawable.ic_single));

            if (obj.getPresentStatus() != null && obj.getPresentStatus().equalsIgnoreCase("P")) {
//                holder.name_check.setChecked(true);
                obj.setPresentStatus("P");
                holder.ly_view_all_student.setBackgroundColor(presentStudentColor);

            } else if (obj.getPresentStatus() != null && obj.getPresentStatus().equalsIgnoreCase("A")){
//                holder.name_check.setChecked(true);
                obj.setPresentStatus("A");
                holder.ly_view_all_student.setBackgroundColor(absentStudentColor);
            } else {
//                holder.name_check.setChecked(false);
                obj.setPresentStatus("");
                holder.ly_view_all_student.setBackgroundColor(context.getResources().getColor(R.color.white));

            }
        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return attendanceArrayList.size();
        }

        public void updateList(ArrayList<Attendance> attendanceArrayList) {
            this.attendanceArrayList = attendanceArrayList;
            this.notifyDataSetChanged();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            // private final CircleImageView civ_item_profile_image;
            TextView name_check, item_id;
            ConstraintLayout ly_view_all_student;

            ImageView civ_item_profile_image;

            public MyViewHolder(View itemView) {
                super(itemView);
                name_check = itemView.findViewById(R.id.ck_name_check);
                /*civ_item_profile_image = (CircleImageView) itemView.findViewById(R.id.civ_item_profile_image);
                civ_item_profile_image.setVisibility(View.VISIBLE);*/
                //   name_check.setTextColor(getResources().getColor(R.color.white));
                    item_id =  itemView.findViewById(R.id.item_id);
                ly_view_all_student =  itemView.findViewById(R.id.ly_view_all_student);
                civ_item_profile_image = itemView.findViewById(R.id.civ_item_profile_image);
                // item_id.setVisibility(View.VISIBLE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(attendanceArrayList.get(getAdapterPosition()).presentStatus.equalsIgnoreCase("P")){
//                            name_check.setChecked(true);
                            ly_view_all_student.setBackgroundColor(absentStudentColor);
                            attendanceArrayList.get(getAdapterPosition()).setPresentStatus("A");
                        }else if(attendanceArrayList.get(getAdapterPosition()).presentStatus.equalsIgnoreCase("A")){
//                            name_check.setChecked(false);
                            ly_view_all_student.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                            attendanceArrayList.get(getAdapterPosition()).setPresentStatus("");
                        }else if(attendanceArrayList.get(getAdapterPosition()).presentStatus.equalsIgnoreCase("")){
//                            name_check.setChecked(true);
                            ly_view_all_student.setBackgroundColor(presentStudentColor);
                            attendanceArrayList.get(getAdapterPosition()).setPresentStatus("P");
                        }
                        notifyDataSetChanged();
                    }
                });
            }


        }


    }

