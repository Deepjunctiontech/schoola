package in.junctiontech.school.schoolnew.DB.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SessionListDao;
import in.junctiontech.school.schoolnew.DB.SessionListEntity;

public class SessionRepository {
    private SessionListDao mSessionDao;
    private LiveData<List<SessionListEntity>> mAllSession;

    public SessionRepository(Application app){
        MainDatabase mDb = MainDatabase.getDatabase(app);
        mSessionDao = mDb.sessionListModel();
        mAllSession = mSessionDao.getSessionList();
    }

    public LiveData<List<SessionListEntity>> getSessionList(){
        return mAllSession;
    }

    public void insert(SessionListEntity[] sessionListEntity){
        new insertAsyncTask(mSessionDao).execute(sessionListEntity);
    }

    private static class insertAsyncTask extends AsyncTask<SessionListEntity, Void, Void> {
        private SessionListDao mAsyncTaskDao;

        insertAsyncTask(SessionListDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(SessionListEntity... sessionListEntities) {
            ArrayList<SessionListEntity> sessionEntities = new ArrayList<SessionListEntity>();
            for (int i = 0; i < sessionListEntities.length; i++){
                sessionEntities.add(sessionListEntities[i]);
            }
            mAsyncTaskDao.insertSessionList(sessionEntities);
            return null;
        }
    }

    }
