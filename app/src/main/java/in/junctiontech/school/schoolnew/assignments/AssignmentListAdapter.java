package in.junctiontech.school.schoolnew.assignments;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.AssignmentEntity;

public class AssignmentListAdapter extends RecyclerView.Adapter<AssignmentListAdapter.MyViewHolder> {
    Context context;
    ArrayList<AssignmentEntity> assignmentEntities;
    String[] permissions;
    int colorIs;

    public AssignmentListAdapter(Context context, ArrayList<AssignmentEntity> assignmentEntities, String[] permissions) {
        this.context = context;
        this.assignmentEntities = assignmentEntities;
        this.permissions = permissions;
        colorIs = Config.getAppColor((Activity) context, true);
    }

    @NonNull
    @Override
    public AssignmentListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assignment_list_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignmentListAdapter.MyViewHolder holder, int position) {

        String as_date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                .format(assignmentEntities.get(position).dateofhomework);

        String sub_date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                .format(assignmentEntities.get(position).dosubmission);

        holder.tv_assignment_text.setText(Html.fromHtml(assignmentEntities.get(position).homework));
        holder.tv_assignment_text.setTextColor(colorIs);

        holder.tv_assignment_date.setText(as_date);
        holder.tv_assignment_date.setTextColor(colorIs);

        holder.tv_submission_date.setText(sub_date);

        holder.tv_assignment_subject.setText(assignmentEntities.get(position).SubjectName);
        holder.tv_assignment_subject.setTextColor(colorIs);

        holder.tv_class_name.setText(assignmentEntities.get(position).ClassName);
        holder.tv_section_name.setText(assignmentEntities.get(position).SectionName);
    }

    @Override
    public int getItemCount() {
        return assignmentEntities.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_assignment_text,
                tv_assignment_date,
                tv_submission_date,
                tv_assignment_subject,
                tv_class_name,
                tv_section_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_assignment_text = itemView.findViewById(R.id.tv_assignment_text);
            tv_assignment_date = itemView.findViewById(R.id.tv_assignment_date);
            tv_submission_date = itemView.findViewById(R.id.tv_submission_date);
            tv_assignment_subject = itemView.findViewById(R.id.tv_assignment_subject);
            tv_class_name = itemView.findViewById(R.id.tv_class_name);
            tv_section_name = itemView.findViewById(R.id.tv_section_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String assignment = new Gson().toJson(assignmentEntities.get(getAdapterPosition()));

                    if (permissions.length == 0) {
                        ((Assignments) context).updateAssignment(assignment);
                    } else if (permissions.length > 0) {

                        AlertDialog.Builder choices = new AlertDialog.Builder(context);

                        choices.setItems(permissions, (dialogInterface, which) -> {

                            switch (which) {
                                case 0:
                                    ((Assignments) context).updateAssignment(assignment);
                                    break;
                                case 1:
                                    ((Assignments) context).evaluateAssignment(assignmentEntities.get(getLayoutPosition()).homeworkId, assignmentEntities.get(getLayoutPosition()).sectionid);
                                    break;
                                case 2:
                                    ((Assignments) context).deleteAssignment(assignment);
                                    break;
                            }

                        });
                        choices.show();
                    }
                }
            });
        }
    }
}
