package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface NoticeBoardDao {
    @Insert(onConflict = REPLACE)
    void insertNotice(List<NoticeBoardEntity> noticeBoardEntity);

    @Insert(onConflict = REPLACE)
    void insertNoticeSingle(NoticeBoardEntity noticeBoardEntity);

    @Delete
    void deleteNotice(NoticeBoardEntity noticeBoardEntity);

    @Query("delete from NoticeBoardEntity where Notify_id like :notify_id")
    void deleteSingleNotice(String notify_id);

    @Query("select * from NoticeBoardEntity where Status LIKE :status and Date >= :date order by Date , Description ")
    LiveData<List<NoticeBoardEntity>> getNoticesActive(String status, String date);

    @Query("select * from NoticeBoardEntity where Status LIKE :status and Flag LIKE :flag and Date >= :date order by Date , Description ")
    LiveData<List<NoticeBoardEntity>> getNoticesForType(String status, String date, String flag);
}
