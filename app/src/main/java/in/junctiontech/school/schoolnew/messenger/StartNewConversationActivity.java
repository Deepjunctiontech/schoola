package in.junctiontech.school.schoolnew.messenger;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class StartNewConversationActivity extends AppCompatActivity {

    MainDatabase mDb;

    RecyclerView mRecyclerView;
    ContactListAdapter adapter = new ContactListAdapter();

    RadioButton rb_show_student, rb_show_staff, rb_show_class;

    ArrayList<SchoolStaffEntity> staffList = new ArrayList<>();
    ArrayList<SchoolStaffEntity> staffFilterList = new ArrayList<>();


    ArrayList<SchoolStudentEntity> studentList = new ArrayList<>();
    ArrayList<SchoolStudentEntity> studentfilterList = new ArrayList<>();

    ArrayList<ClassNameSectionName> classList = new ArrayList<>();

    ArrayList<String> chatPermissions = new ArrayList<>();

    StudentInformation studentInfo;
    SignedInStaffInformationEntity staffInfo;

    String role;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_new_conversation);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initializeViews();

        setColorApp();


        mDb.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, this))
                .observe(this, communicationPermissions -> {
                    chatPermissions.clear();
                    for (CommunicationPermissions c : communicationPermissions) {
                        chatPermissions.add(c.ToUserType);
                    }
                });

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6838621380", this, adContainer);
        }
    }

    void initializeViews() {
        mRecyclerView = findViewById(R.id.rv_new_conversation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(adapter);

        rb_show_class = findViewById(R.id.rb_show_class);
        rb_show_student = findViewById(R.id.rb_show_student);
        rb_show_staff = findViewById(R.id.rb_show_staff);

        role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

        switch (role) {
            case Gc.STAFF:
                staffInfo = new Gson().fromJson(Strings.nullToEmpty(getIntent().getStringExtra(Gc.STAFF))
                        , SignedInStaffInformationEntity.class);

                rb_show_student.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                            mDb.schoolStudentModel()
                                    .getAllStudents()
                                    .observe(StartNewConversationActivity.this, new Observer<List<SchoolStudentEntity>>() {
                                        @Override
                                        public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                                            studentList.clear();
                                            studentfilterList.clear();
                                            studentList.addAll(schoolStudentEntities);
                                            studentfilterList.addAll(schoolStudentEntities);
                                            adapter.notifyDataSetChanged();

                                            mDb.schoolStudentModel()
                                                    .getAllStudents()
                                                    .removeObserver(this);
                                        }
                                    });
                        } else {
                            studentList.clear();
                            studentfilterList.clear();
                            adapter.notifyDataSetChanged();
                            Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to)
                                            + getString(R.string.student),
                                    findViewById(R.id.top_layout), R.color.fragment_first_blue);

                        }
                    }
                });

                break;

            case Gc.STUDENTPARENT:
                studentInfo = new Gson().fromJson(Strings.nullToEmpty(getIntent().getStringExtra(Gc.STUDENTPARENT))
                        , StudentInformation.class);
                rb_show_student.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                            mDb.schoolStudentModel()
                                    .getAllStudents()
                                    .observe(StartNewConversationActivity.this, new Observer<List<SchoolStudentEntity>>() {
                                        @Override
                                        public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                                            studentList.clear();
                                            studentfilterList.clear();
                                            studentList.addAll(schoolStudentEntities);
                                            studentfilterList.addAll(schoolStudentEntities);
                                            adapter.notifyDataSetChanged();

                                            mDb.schoolStudentModel()
                                                    .getAllStudents()
                                                    .removeObserver(this);
                                        }
                                    });
                        } else {
                            studentList.clear();
                            studentfilterList.clear();
                            adapter.notifyDataSetChanged();
                            Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to)
                                            + getString(R.string.student),
                                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
//                            mDb.schoolStudentModel().getAllStudentsClassMessenger(studentInfo.ClassId, studentInfo.AdmissionId)
//                                    .observe(StartNewConversationActivity.this, new Observer<List<SchoolStudentEntity>>() {
//                                        @Override
//                                        public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
//                                            studentList.clear();
//                                            studentfilterList.clear();
//                                            studentList.addAll(schoolStudentEntities);
//                                            studentfilterList.addAll(schoolStudentEntities);
//                                            adapter.notifyDataSetChanged();
//                                        }
//                                    });
                        }
                    }
                });


                break;

            case Gc.ADMIN:
                rb_show_student.setOnClickListener(view -> {
                    mDb.schoolStudentModel()
                            .getAllStudents()
                            .observe(StartNewConversationActivity.this, schoolStudentEntities -> {
                                studentList.clear();
                                studentfilterList.clear();
                                studentList.addAll(schoolStudentEntities);
                                studentfilterList.addAll(schoolStudentEntities);
                                adapter.notifyDataSetChanged();
                            });
                });
                break;
        }

        rb_show_class.setOnClickListener(view -> {
            mDb.schoolClassSectionModel()
                    .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, StartNewConversationActivity.this))
                    .observe(StartNewConversationActivity.this, classNameSectionNames -> {
                        classList.clear();
                        if (role.equalsIgnoreCase(Gc.STUDENTPARENT)) {
                            for (ClassNameSectionName c : Objects.requireNonNull(classNameSectionNames)) {
                                if (c.ClassId.equalsIgnoreCase(studentInfo.ClassId))
                                    classList.add(c);
                            }
                        } else if (role.equalsIgnoreCase(Gc.STAFF)) {
                            if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                                classList.addAll(classNameSectionNames);
                            } else {
                                Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to)
                                                + getString(R.string.class_and_section),
                                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
                            }
                        } else {
                            classList.addAll(classNameSectionNames);
                        }
                        adapter.notifyDataSetChanged();
                    });
        });


        rb_show_staff.setOnClickListener(view -> {
            mDb.schoolStaffModel().getAllStaff().observe(StartNewConversationActivity.this, new Observer<List<SchoolStaffEntity>>() {
                @Override
                public void onChanged(@Nullable List<SchoolStaffEntity> schoolStaffEntities) {
                    staffList.clear();
                    staffFilterList.clear();

                    if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, StartNewConversationActivity.this).equalsIgnoreCase(Gc.ADMIN)) {
                        staffList.addAll(schoolStaffEntities);
                        staffFilterList.addAll(schoolStaffEntities);
                    } else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, StartNewConversationActivity.this)
                            .equalsIgnoreCase(Gc.STUDENTPARENT)) {
                        for (SchoolStaffEntity s : Objects.requireNonNull(schoolStaffEntities)) {

                            if (chatPermissions.contains(s.UserTypeValue)) {
                                staffList.add(s);
                                staffFilterList.add(s);
                            }

                        }
                    } else {
                        for (SchoolStaffEntity s : Objects.requireNonNull(schoolStaffEntities)) {

                            if (chatPermissions.contains(s.UserTypeValue) && !(staffInfo.StaffId.equalsIgnoreCase(s.StaffId))) {
                                staffList.add(s);
                                staffFilterList.add(s);
                            }

                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterChatEntity(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterChatEntity(s);
                return true;
            }
        });
        return true;
    }

    void filterChatEntity(String text) {

        if (rb_show_student.isChecked()) {

            studentList.clear();
            if (text.isEmpty()) {
                studentList.addAll(studentfilterList);
            } else {
                text = text.toLowerCase();
                for (SchoolStudentEntity s : studentfilterList) {
                    if (s.StudentName.toLowerCase().contains(text)
                            || s.ClassName.toLowerCase().contains(text)
                            || s.AdmissionNo.toLowerCase().contains(text)
                    ) {
                        studentList.add(s);
                    }
                }
            }
        } else if (rb_show_staff.isChecked()) {

            staffList.clear();
            if (text.isEmpty()) {
                staffList.addAll(staffFilterList);
            } else {
                text = text.toLowerCase();
                for (SchoolStaffEntity s : staffFilterList) {
                    if (s.StaffName.toLowerCase().contains(text)
                            || s.StaffPositionValue.toLowerCase().contains(text)
                    ) {
                        staffList.add(s);
                    }
                }

            }
        }
        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyViewHolder> {

        @NonNull
        @Override
        public ContactListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_list, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ContactListAdapter.MyViewHolder holder, int position) {
            holder.tv_chat_name.setTextColor(colorIs);
            if (rb_show_staff.isChecked()) {
                holder.tv_chat_name.setText(staffList.get(position).StaffName
                        + " "
                        + staffList.get(position).UserTypeValue);

                if (!("".equals(staffList.get(position).staffProfileImage)))
                    Glide.with(StartNewConversationActivity.this)
                            .load(staffList.get(position).staffProfileImage)
                            .apply(new RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ic_contact);
                else
                    holder.ic_contact.setImageDrawable(getDrawable(R.drawable.ic_single));

            } else if (rb_show_student.isChecked()) {
                holder.tv_chat_name.setText(String.format("%s %s",
                        studentList.get(position).StudentName,
                        studentList.get(position).ClassName)

                );

                if (!("".equals(studentList.get(position).studentProfileImage)))
                    Glide.with(StartNewConversationActivity.this)
                            .load(studentList.get(position).studentProfileImage)
                            .apply(new RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ic_contact);
                else
                    holder.ic_contact.setImageDrawable(getDrawable(R.drawable.ic_single));
            } else if (rb_show_class.isChecked()) {
                holder.tv_chat_name.setText(classList.get(position).ClassName
                        + " "
                        + classList.get(position).SectionName
                );
                holder.ic_contact.setImageDrawable(getDrawable(R.drawable.ic_single));


            }
        }

        @Override
        public int getItemCount() {
            if (rb_show_staff.isChecked()) {
                return staffList.size();
            } else if (rb_show_student.isChecked()) {
                return studentList.size();
            } else if (rb_show_class.isChecked())
                return classList.size();
            else return 0;

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_chat_name;
            CircleImageView ic_contact;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_chat_name = itemView.findViewById(R.id.tv_chat_name);
                ic_contact = itemView.findViewById(R.id.ic_contact);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(StartNewConversationActivity.this, MessageListActivity.class);
                    if (rb_show_student.isChecked()) {
                        String student = new Gson().toJson(studentList.get(getAdapterPosition()));
                        intent.putExtra("with", student);
                        intent.putExtra("withType", "student");
                        startActivity(intent);
                        finish();
                    } else if (rb_show_staff.isChecked()) {
                        String staff = new Gson().toJson(staffList.get(getAdapterPosition()));
                        intent.putExtra("with", staff);
                        intent.putExtra("withType", "staff");
                        startActivity(intent);
                        finish();
                    } else if (rb_show_class.isChecked()) {
                        String section = new Gson().toJson(classList.get(getAdapterPosition()));
                        intent.putExtra("with", section);
                        intent.putExtra("withType", "section");
                        startActivity(intent);
                        finish();
                    }
                });

            }
        }
    }
}
