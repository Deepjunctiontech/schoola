package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SignedInUserInformationEntity {
    @PrimaryKey
    @NonNull
    public String userID;
    public String userType  ;
    public String userTypeValue;
    public String userName;
    public String accessToken;
}
