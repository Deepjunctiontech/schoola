package `in`.junctiontech.school.schoolnew.feesetup.feetype.payonline

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class OnlineTransactionsActivity : AppCompatActivity() {
    private var colorIs: Int = 0
    private var progressBar: ProgressBar? = null

    lateinit var selectedStudent: String
    internal var studentEntity: SchoolStudentEntity? = null

    lateinit var mRecyclerView: RecyclerView
    lateinit var adapter: OnlineTransactionsActivityAdapter

    private val payOnline = ArrayList<PayOnline>()

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_transactions_online)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val studentString = intent.getStringExtra("student")
        if (studentString == null){
            finish()
            return
        }

        selectedStudent = studentString


        if (!"".equals(Strings.nullToEmpty(selectedStudent), ignoreCase = true)) {
            studentEntity = Gson().fromJson(selectedStudent, SchoolStudentEntity::class.java)
        }


        progressBar = findViewById(R.id.progressBar)


        mRecyclerView = findViewById(R.id.rv_online_transactions)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = OnlineTransactionsActivityAdapter()
        mRecyclerView.adapter = adapter

        setColorApp()
        getOnlineTransactions()

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/2655833796", this, adContainer)
        }
    }

    private fun getOnlineTransactions() {

        progressBar!!.visibility = View.VISIBLE
        var url = "";
        if (!"".equals(Strings.nullToEmpty(selectedStudent), ignoreCase = true)) {
            val p = JSONObject()
            p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this))
            p.put("admissionId", studentEntity!!.AdmissionId)
            url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "fee/payOnline/"
                    + p)
        } else {
            url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "fee/payOnline")
        }


        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val onlineTransactions = Gson()
                            .fromJson<ArrayList<PayOnline>>(job.getJSONObject("result").optString("onlineTransactions")
                                    , object : TypeToken<List<PayOnline>>() {

                            }.type)
                    payOnline.clear()

                    if (Objects.requireNonNull<ArrayList<PayOnline>>(onlineTransactions).size > 0) {


                        payOnline.addAll(onlineTransactions)

                        adapter.notifyDataSetChanged()

                    }
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_green)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.activity_fee_receive_online_pay), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@OnlineTransactionsActivity, error, findViewById<View>(R.id.activity_fee_receive_online_pay))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class OnlineTransactionsActivityAdapter : RecyclerView.Adapter<OnlineTransactionsActivityAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.rv_online_transaction, parent, false)

            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvTransactionStatus.text = payOnline[position].status
            holder.tvTransactionAmount.text = payOnline[position].amount
            holder.tvTransactionId.text = payOnline[position].orderId
            holder.tvTransactionDate.text = payOnline[position].createdOn
        }

        override fun getItemCount(): Int {
            return payOnline.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvTransactionStatus: TextView = itemView.findViewById(R.id.tv_transaction_status)
            internal var tvTransactionAmount: TextView = itemView.findViewById(R.id.tv_transaction_amount)
            internal var tvTransactionId: TextView = itemView.findViewById(R.id.tv_transaction_id)
            internal var tvTransactionDate: TextView = itemView.findViewById(R.id.tv_transaction_date)

            init {

            }
        }
    }

}

class PayOnline {
    var status: String? = null
    var amount: String? = null
    var orderId: String? = null
    var firstName: String? = null
    var email: String? = null
    var phone: String? = null
    var createdOn: String? = null
}