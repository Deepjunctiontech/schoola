package in.junctiontech.school.schoolnew.feesetup.feetype;

import java.util.ArrayList;

public class FeeStructureModel {
    public String feestructureid;
    public String from;
    public String status;

    public ArrayList<StudentFeeModel> basicfee ;
    public ArrayList<TransportFeeModel> transportfees;

}

class StudentFeeModel{
    public String  FeeTypeName;
    public String  Frequency;
    public String  FeeId;
    public String  ActualAmount;
    public String StudentFeeAmount;
    public String Assign;
}

class TransportFeeModel{
    public String  FeeTypeName;
    public String  Frequency;
    public String  FeeId;
    public String  ActualAmount;
    public String StudentFeeAmount;
    public String Assign;
    public String  Distance;
    public String  DistanceKM;
}
