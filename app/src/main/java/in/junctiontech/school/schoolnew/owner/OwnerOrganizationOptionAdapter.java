package in.junctiontech.school.schoolnew.owner;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 05-07-2017.
 */

public class OwnerOrganizationOptionAdapter extends RecyclerView.Adapter<OwnerOrganizationOptionAdapter.MyViewHoler> {
    private OrganizationDetailActivity organizationDetailActivity;
    private ArrayList<OwnerViewAction> ownerSchoolList;
    private int colorIs;

    public OwnerOrganizationOptionAdapter(OrganizationDetailActivity organizationDetailActivity,
                                          ArrayList<OwnerViewAction> ownerSchoolList, int colorIs) {
        this.organizationDetailActivity = organizationDetailActivity;
        this.ownerSchoolList = ownerSchoolList;
        this.colorIs = colorIs;
    }
    @Override
    public OwnerOrganizationOptionAdapter.MyViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_owner_school_item, parent, false);
        return new OwnerOrganizationOptionAdapter.MyViewHoler(view);
    }


    @Override
    public void onBindViewHolder(OwnerOrganizationOptionAdapter.MyViewHoler holder, int position) {
        OwnerViewAction obj = ownerSchoolList.get(position);
        holder.owner_item_organization_logo.setBackgroundResource(obj.getIconId());
        holder.owner_item_organization_name.setText(obj.getLabel());
        holder.owner_item_organization_name.setTextColor(colorIs);
    }

    @Override
    public int getItemCount() {
        return ownerSchoolList.size();
    }
    public class MyViewHoler extends RecyclerView.ViewHolder {
        public CircleImageView owner_item_organization_logo;
        public TextView owner_item_organization_name;

        public MyViewHoler(View itemView) {
            super(itemView);
            owner_item_organization_logo = (CircleImageView) itemView.findViewById(R.id.owner_item_organization_logo);
            owner_item_organization_logo.setPadding(10,10,10,10);
            owner_item_organization_name = (TextView) itemView.findViewById(R.id.owner_item_organization_name);

        }
    }
}
