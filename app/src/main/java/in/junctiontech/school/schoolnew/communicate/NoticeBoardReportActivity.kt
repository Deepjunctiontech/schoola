package `in`.junctiontech.school.schoolnew.communicate

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.NoticeBoardEntity
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class NoticeBoardReportActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar

    internal var studentDetails = ArrayList<StudentDetails>()
    internal var staffDetails = ArrayList<StaffDetails>()

    internal var llStudentParentDetails: LinearLayout? = null
    internal var llStaffDetails: LinearLayout? = null

    internal var tvTotalReceiver: TextView? = null
    internal var tvTotalNotSend: TextView? = null
    internal var tvTotalSend: TextView? = null
    internal var tvTotalReceived: TextView? = null
    internal var error_message: TextView? = null
    lateinit var noticeReport: NoticeBoardEntity

    private var colorIs: Int = 0
    lateinit var adapter: NoticeBoardReportStudentAdapter
    lateinit var adapter1: NoticeBoardReportStaffAdapter
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice_board_report)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        progressBar = findViewById(R.id.progressBar)

        llStudentParentDetails = findViewById(R.id.ll_student_parent_details)
        llStaffDetails = findViewById(R.id.ll_staff_details)

        tvTotalReceiver = findViewById(R.id.tv_total_receiver)
        tvTotalNotSend = findViewById(R.id.tv_total_not_send)
        tvTotalSend = findViewById(R.id.tv_total_send)
        tvTotalReceived = findViewById(R.id.tv_total_received)
        error_message = findViewById(R.id.error_message)

        val mRecyclerView = findViewById<RecyclerView>(R.id.rv_ll_student_parent_details_list)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = NoticeBoardReportStudentAdapter()
        mRecyclerView.adapter = adapter

        val staffRecyclerViews = findViewById<RecyclerView>(R.id.rv_ll_staff_details_list)
        staffRecyclerViews.setHasFixedSize(true)

        val layoutManager1 = LinearLayoutManager(this)
        staffRecyclerViews.layoutManager = layoutManager1

        adapter1 = NoticeBoardReportStaffAdapter()
        staffRecyclerViews.adapter = adapter1


        if (!"".equals(Strings.nullToEmpty(intent.getStringExtra("notice")), ignoreCase = true)) {
            noticeReport = Gson().fromJson(intent.getStringExtra("notice"), NoticeBoardEntity::class.java)

            getNoticeToServerReport(noticeReport.Notify_id);
        }

        setColorApp()

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/1292865798", this, adContainer)
        }
    }

    private fun getNoticeToServerReport(notifyId: String) {
        progressBar.visibility = View.VISIBLE

        val p = JSONObject()
        p.put("Notify_id", notifyId)

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "noticeBoard/noticeBoardsNotificationReports/"
                + p)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            progressBar.visibility = View.GONE

            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> try {

                    val totalReceiver = job.getJSONObject("result").getJSONObject("noticeBoardsNotificationReports")
                            .optString("TotalReceiver")

                    val totalSend = job.getJSONObject("result").getJSONObject("noticeBoardsNotificationReports")
                            .optString("TotalSend")

                    val totalReceived = job.getJSONObject("result").getJSONObject("noticeBoardsNotificationReports")
                            .optString("TotalRecived")

                    tvTotalReceiver!!.text = totalReceiver
                    tvTotalNotSend!!.text = (Integer.valueOf(totalReceiver) - Integer.valueOf(totalSend)).toString()
                    tvTotalSend!!.text = totalSend
                    tvTotalReceived!!.text = totalReceived

                    val studentParentDetails = Gson()
                            .fromJson<ArrayList<StudentDetails>>(job.getJSONObject("result").getJSONObject("noticeBoardsNotificationReports")
                                    .optString("StudentDetails"), object : TypeToken<List<StudentDetails>>() {
                            }.type)

                    if (studentParentDetails.size > 0) {
                        llStudentParentDetails!!.visibility = View.VISIBLE
                        studentDetails.clear()
                        studentDetails.addAll(studentParentDetails)
                        adapter.notifyDataSetChanged()
                    }


                    val staffsDetails = Gson()
                            .fromJson<ArrayList<StaffDetails>>(job.getJSONObject("result").getJSONObject("noticeBoardsNotificationReports")
                                    .optString("StaffDetails"), object : TypeToken<List<StaffDetails>>() {
                            }.type)

                    if (staffsDetails.size > 0) {
                        llStaffDetails!!.visibility = View.VISIBLE
                        staffDetails.clear()
                        staffDetails.addAll(staffsDetails)
                        adapter.notifyDataSetChanged()
                    }


                    if (staffsDetails.size <= 0 && studentParentDetails.size <= 0) {
                        error_message!!.visibility = View.VISIBLE
                        error_message!!.text = "Notification Report Not Found Please Update Notifications"
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                Gc.APIRESPONSE401 -> {

                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this@NoticeBoardReportActivity)

                    val intent1 = Intent(this@NoticeBoardReportActivity, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {

                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this@NoticeBoardReportActivity)

                    val intent2 = Intent(this@NoticeBoardReportActivity, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }

                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.activity_notice_board_report), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.setVisibility(View.GONE)

            Config.responseVolleyErrorHandler(this@NoticeBoardReportActivity, error, findViewById(R.id.activity_notice_board_report))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class NoticeBoardReportStudentAdapter : RecyclerView.Adapter<NoticeBoardReportStudentAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notice_board_report, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            val receiverName = "( ${studentDetails[position].AdmissionNo}) ${studentDetails[position].StudentName} C/O ${studentDetails[position].FatherName}"

            holder.tvReceiverName.text = receiverName
            if (studentDetails[position].StudentNotificationStatus!!.toLowerCase() == "n") {
                holder.tvStudentReceived.text = "Not Send (Not Logged In)"
                holder.tvStudentReceived.setBackgroundColor(Color.RED)
            } else if (studentDetails[position].StudentNotificationStatus!!.toLowerCase() == "s") {
                holder.tvStudentReceived.text = "Send"
                holder.tvStudentReceived.setBackgroundColor(Color.YELLOW)
            } else if (studentDetails[position].StudentNotificationStatus!!.toLowerCase() == "r") {
                holder.tvStudentReceived.text = "Recived"
                holder.tvStudentReceived.setBackgroundColor(Color.GREEN)
            } else {
                holder.tvStudentReceived.setBackgroundColor(0)
            }

            if (studentDetails[position].ParentNotificationStatus!!.toLowerCase() == "n") {
                holder.tvParentReceived.text = "Not Send (Not Logged In)"
                holder.tvParentReceived.setBackgroundColor(Color.RED)
            } else if (studentDetails[position].ParentNotificationStatus!!.toLowerCase() == "s") {
                holder.tvParentReceived.text = "Send"
                holder.tvParentReceived.setBackgroundColor(Color.YELLOW)

            } else if (studentDetails[position].ParentNotificationStatus!!.toLowerCase() == "r") {
                holder.tvParentReceived.text = "Recived"
                holder.tvParentReceived.setBackgroundColor(Color.GREEN)
            } else {
                holder.tvParentReceived.setBackgroundColor(0)
            }
        }

        override fun getItemCount(): Int {
            return studentDetails.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvReceiverName: TextView = itemView.findViewById(R.id.tv_receiver_name)
            internal var tvStudentReceived: TextView = itemView.findViewById(R.id.tv_student_received)
            internal var tvParentReceived: TextView = itemView.findViewById(R.id.tv_parent_received)
        }
    }

    inner class NoticeBoardReportStaffAdapter : RecyclerView.Adapter<NoticeBoardReportStaffAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notice_board_report, parent, false)

            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            var receiverName = "( ${staffDetails[position].Username})"

            if (!staffDetails[position].StaffName.isNullOrEmpty()) {
                receiverName += " ${staffDetails[position].StaffName}"
            }

            holder.tvReceiverName.text = receiverName

            if (staffDetails[position].StaffNotificationStatus!!.toLowerCase() == "n") {
                holder.tvStudentReceived.text = "Not Send (Not Logged In)"
                holder.tvStudentReceived.setBackgroundColor(Color.RED)
            } else if (staffDetails[position].StaffNotificationStatus!!.toLowerCase() == "s") {
                holder.tvStudentReceived.text = "Send"
                holder.tvStudentReceived.setBackgroundColor(Color.YELLOW)

            } else if (staffDetails[position].StaffNotificationStatus!!.toLowerCase() == "r") {
                holder.tvStudentReceived.text = "Recived"
                holder.tvStudentReceived.setBackgroundColor(Color.GREEN)
            } else {
                holder.tvStudentReceived.setBackgroundColor(0)
            }
        }

        override fun getItemCount(): Int {
            return staffDetails.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvReceiverName: TextView = itemView.findViewById(R.id.tv_receiver_name)
            internal var tvStudentLoggedIn: TextView = itemView.findViewById(R.id.tv_student_logged_in)
            internal var tvParentLoggedIn: TextView = itemView.findViewById(R.id.tv_parent_logged_in)
            internal var tvStudentReceived: TextView = itemView.findViewById(R.id.tv_student_received)
            internal var tvParentReceived: TextView = itemView.findViewById(R.id.tv_parent_received)

            init {
                tvParentLoggedIn.text = ""
                tvParentReceived.text = ""
                tvStudentLoggedIn.text = "Staff Logged In"
            }

        }
    }

}


internal class StudentDetails {
    var AdmissionId: String? = null
    var AdmissionNo: String? = null
    var StudentName: String? = null
    var FatherName: String? = null
    var Mobile: String? = null
    var StudentNotificationStatus: String? = null
    var ParentNotificationStatus: String? = null
}

internal class StaffDetails {
    var UserId: String? = null
    var Username: String? = null
    var StaffName: String? = null
    var StaffMobile: String? = null
    var StaffFName: String? = null
    var StaffNotificationStatus: String? = null
}
