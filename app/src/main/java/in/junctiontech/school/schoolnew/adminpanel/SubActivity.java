package in.junctiontech.school.schoolnew.adminpanel;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.createtimetable.CreateSlotsActivity;
import in.junctiontech.school.createtimetable.DateWiseActivity;
import in.junctiontech.school.createtimetable.DayWiseActivity;
import in.junctiontech.school.createtimetable.ViewTimetableActivity;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.menuActions.aboutus.AboutUsZeroerpActivity;
import in.junctiontech.school.models.GeneralSettingData;
import in.junctiontech.school.schoolinformation.SchoolInformationActivity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.SchoolLibrary.Library;
import in.junctiontech.school.schoolnew.assignments.AssignmentMonthWiseActivity;
import in.junctiontech.school.schoolnew.attendance.StudentDayAttendenceActivity;
import in.junctiontech.school.schoolnew.attendance.StudentMonthAttendanceActivity;
import in.junctiontech.school.schoolnew.chat.ChatActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.communicate.SMSInformationActivity;
import in.junctiontech.school.schoolnew.exam.CoScholasticAreaActivity;
import in.junctiontech.school.schoolnew.exam.CoScholasticGradeActivity;
import in.junctiontech.school.schoolnew.exam.EnterExamResultActivity;
import in.junctiontech.school.schoolnew.exam.ExamResultStudentActivity;
import in.junctiontech.school.schoolnew.exam.ExamTypeActivity;
import in.junctiontech.school.schoolnew.exam.ScholasticGradeActivity;
import in.junctiontech.school.schoolnew.exam.ScholasticGradeMarksActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.FeeTypeActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.StudentFeeListActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.classfee.ClassFeeActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.receive.FeeReceiptsActivity;
import in.junctiontech.school.schoolnew.feesetup.feetype.receive.StudentDueActivity;
import in.junctiontech.school.schoolnew.finance.setup.AccountActivity;
import in.junctiontech.school.schoolnew.leave.AssignedLeaveActivity;
import in.junctiontech.school.schoolnew.leave.LeaveActivity;
import in.junctiontech.school.schoolnew.licence.LicenceActivity;
import in.junctiontech.school.schoolnew.messenger.ConversationsActivity;
import in.junctiontech.school.schoolnew.notificationbar.NewNotificationBarActivity;
import in.junctiontech.school.schoolnew.reviewlog.MyReview;
import in.junctiontech.school.schoolnew.schooldetails.SchoolPublicDetailsActivity;
import in.junctiontech.school.schoolnew.schoolsession.SessionActivity;
import in.junctiontech.school.schoolnew.staff.StaffActivity;
import in.junctiontech.school.schoolnew.student.StudentActivity;
import in.junctiontech.school.schoolnew.trackIssue.IssueListActivity;
import in.junctiontech.school.schoolnew.transport.fee.TransportFeeSetupActivity;
import in.junctiontech.school.staffattendance.StaffAttendanceActivity;
import in.junctiontech.school.tranport.FirstScreenViewLocationActivity;
import in.junctiontech.school.tranport.TrackLocationActivity;

public class SubActivity extends AppCompatActivity {

    MainDatabase mDb;

    private SharedPreferences sp;
    private String loggedUserID, userType;
    private int colorIs;
    private RecyclerView recycler_view_list_of_data;
    private ArrayList<UserActionModel> userActionList = new ArrayList<>();
    private UserActionAdapter userActionAdapter;
    private boolean isImageChanged = false, isSessionChanged = false;
    private boolean isDataUpdated = false, isDataInserted = false;
    private int res = 0;
    private boolean isPermissionForPayment = false;
    private ProgressDialog progressbar;
    private GeneralSettingData generalSettingData;

    ArrayList<String> userPermissionList = new ArrayList<>();

    String whichActionData;
    String selectedStudent;

    String selectedStaff;

    UserActionModel createFeeTypeObjModel = new UserActionModel(R.string.create_fee_type, R.drawable.ic_salary, true,
            Color.parseColor("#b400ab"), "createFeeType");

    UserActionModel classFeeObjModel = new UserActionModel(R.string.class_fee, R.drawable.ic_coaching, true,
            Color.parseColor("#057643"), "classFee");

    UserActionModel assignedLeaveObjModel = new UserActionModel(R.string.assigned_leave, R.drawable.ic_leave, true,
            0, "assignedLeave");

    UserActionModel appliedLeaveObjModel = new UserActionModel(R.string.apply_leave, R.drawable.ic_leaves, true,
            0, "applyLeave");

    UserActionModel feeAccountObjModel = new UserActionModel(R.string.fee_account, R.drawable.ic_coins, false,
            0, "feeAccount");
    UserActionModel transportFeeObjModel = new UserActionModel(R.string.transport_fee, R.drawable.ic_maps_directions_car, false,
            0, "transportFee");
    UserActionModel feePaymentObjModel = new UserActionModel(R.string.fee_payment, R.drawable.ic_payment, false,
            0, "feePayment");

    UserActionModel feeReceiptObjModel = new UserActionModel(R.string.fee_receipt, R.drawable.ic_fee_receipt, false,
            0, "feeReceipt");
    UserActionModel broadcastObjModel = new UserActionModel(R.string.broadcast, R.drawable.ic_notification, false,
            0, "broadcast");
    UserActionModel updateStudentFeeObjModel = new UserActionModel(R.string.update_student_fee, R.drawable.ic_salary, true,
            Color.parseColor("#279609"), "updateStudentFee");

    UserActionModel createStudentObjModel = new UserActionModel(R.string.students, R.drawable.ic_student, true,
            Color.parseColor("#b400ab"), "createStudent");
    UserActionModel changeClassObjModel = new UserActionModel(R.string.change_class, R.drawable.ic_coaching, true,
            Color.parseColor("#057643"), "changeClass");
    UserActionModel promoteToNextClassObjModel = new UserActionModel(R.string.promotion_to_next_class, R.drawable.ic_promotion, false,
            0, "promoteToNextClass");
    UserActionModel studentAttendanceEntryObjModel = new UserActionModel(R.string.attendance_entry, R.drawable.ic_attendance, false,
            0, "studentAttendanceEntry");
    /* UserActionModel reportViewObjModel = new UserActionModel(R.string.report_view, R.drawable.reportview, false,
             0, "reportView");*/
    UserActionModel createStaffObjModel = new UserActionModel(R.string.create_staff, R.drawable.ic_teacher1, true,
            Color.parseColor("#0c6603"), "createStaff");
    UserActionModel staffAttendanceObjModel = new UserActionModel(R.string.staff_attendance, R.drawable.ic_staff_attendance, true,
            Color.parseColor("#cd95e6"), "staffAttendance");
    UserActionModel staffAttendanceReportObjModel = new UserActionModel(R.string.staff_attendance_report, R.drawable.ic_promotion, false,
            0, "staffAttendanceReport");

    //Exam
    UserActionModel createGradesObjModel = new UserActionModel(R.string.create_scholastic_grades, R.drawable.ic_a_plus, false,
            0, "createGrades");
    UserActionModel createCoScholasticGradesObjModel = new UserActionModel(R.string.create_coscholastic_grades, R.drawable.ic_a_plus, false,
            0, "createCoScholasticGrades");
    UserActionModel createCoScholasticAreaObjModel = new UserActionModel(R.string.create_coscholastic_area, R.drawable.ic_a_plus, false,
            0, "createCoScholasticArea");
    UserActionModel gradeMarksSetupObjModel = new UserActionModel(R.string.scholastic_grade_marks_setup, R.drawable.ic_exam_result, false,
            0, "gradeMarksSetup");
    UserActionModel examTypeObjModel = new UserActionModel(R.string.exam_type, R.drawable.ic_library, false,
            0, "createExamType");
    UserActionModel fillExamResultObjModel = new UserActionModel(R.string.exam_results, R.drawable.ic_test, false,
            0, "fillExamResult");
    UserActionModel sendSmsObjModel = new UserActionModel(R.string.send_sms, android.R.drawable.ic_dialog_email, true,
            Color.parseColor("#f74162"), "sendSms");
    UserActionModel messengerActivityObjModel = new UserActionModel(R.string.messager, R.drawable.ic_message, false, 0
            , "messengerActivity");
    UserActionModel createNotificationObjModel = new UserActionModel(R.string.create_notice, R.drawable.ic_notification, false,
            0, "createNotice");
    /*UserActionModel myNotificationObjModel = new UserActionModel(R.string.my_notification, R.drawable.ic_sand_clock1, true,
            Color.parseColor("#cd95e6"), "MyNotification");*/
//    UserActionModel createSlotTimeTableObjModel = new UserActionModel(R.string.create_slots, R.drawable.ic_section, true,
//            Color.parseColor("#1f33a4"), "createSlotTimeTable");
    UserActionModel dayWiseTimeTableObjModel = new UserActionModel(R.string.day_wise, R.drawable.ic_day_wise, false,
            0, "dayWiseTimeTable");
    UserActionModel dateWiseTimeTableObjModel = new UserActionModel(R.string.date_wise, R.drawable.ic_date_wise, false,
            0, "dateWiseTimeTable");
    UserActionModel viewTimeTableObjModel = new UserActionModel(R.string.view_time_table, R.drawable.ic_view_timetable, false,
            0, "viewTimeTable");
    UserActionModel schoolInformationObjModel = new UserActionModel(R.string.logo_and_social, R.drawable.ic_home, true,
            Color.parseColor("#0c6603"), "schoolInformation");
    UserActionModel schoolDetailsObjModel = new UserActionModel(R.string.basic_details, R.drawable.ic_school, true,
            Color.parseColor("#1f33a4"), "schoolDetails");
    UserActionModel sessionObjModel = new UserActionModel(R.string.school_session, R.drawable.ic_section, true,
            Color.parseColor("#f74162"), "schoolSession");
    UserActionModel reportBugObjModel = new UserActionModel(R.string.report_queries, R.drawable.ic_report_bug, true,
            Color.parseColor("#f74162"), "reportBug");
    UserActionModel softwareDetailsObjModel = new UserActionModel(R.string.software_details, R.drawable.ic_fee_receipt, true,
            Color.parseColor("#f74162"), "softwareDetails");
    UserActionModel howToUseObjModel = new UserActionModel(R.string.how_to_use, android.R.drawable.ic_menu_help, true,
            Color.parseColor("#cd95e6"), "howToUse");
    UserActionModel faqObjModel = new UserActionModel(R.string.faq, R.drawable.ic_faq, false,
            0, "faq");
    UserActionModel softwarePricingObjModel = new UserActionModel(R.string.software_pricing, R.drawable.ic_coins, false,
            0, "softwarePricing");
    UserActionModel aboutJunctionObjModel = new UserActionModel(R.string.about_us, R.drawable.ic_junction, false,
            0, "aboutZeroerp");

    UserActionModel homeworkReportObjModel = new UserActionModel(R.string.homework_report, R.drawable.ic_homework, false,
            0, "homeworkReport");
    UserActionModel homeworkEntryObjModel = new UserActionModel(R.string.assignment_entry, R.drawable.ic_homework, false,
            0, "homeworkEntry");

    UserActionModel attendanceReportObjModel = new UserActionModel(R.string.attendance_report, R.drawable.ic_staff_attendance, true,
            Color.parseColor("#cd95e6"), "attendanceReport");
    UserActionModel examResultReportObjModel = new UserActionModel(R.string.exam_result_scholastic, R.drawable.ic_staff_attendance, true,
            Color.parseColor("#cd95e6"), "examResultReport");

    UserActionModel issuedBooksObjModel = new UserActionModel(R.string.book_issued, R.drawable.library, false,
            0, "issuedBooks");
    UserActionModel addVehicleObjModel = new UserActionModel(R.string.add_vehicle, R.drawable.ic_maps_directions_car, false,
            0, "addVehicle");
    UserActionModel addStopObjModel = new UserActionModel(R.string.add_stop, android.R.drawable.ic_dialog_map, true,
            Color.parseColor("#cd95e6"), "addStop");
    /*UserActionModel transportSetupObjModel = new UserActionModel(R.string.transport_setup, R.drawable.ic_maps_directions_car, false,
            0, "transportSetup");*/
    UserActionModel viewLocationObjModel = new UserActionModel(R.string.view_location, R.drawable.ic_map, false,
            0, "viewLocation");

    UserActionModel studentReviewLogObjModel = new UserActionModel(R.string.student_review_log, R.drawable.ic_student, true,
            Color.parseColor("#285fe6"), "studentReviewLog");

    UserActionModel myReviewLogObjModel = new UserActionModel(R.string.my_review_log, R.drawable.ic_fee_receipt, true,
            Color.parseColor("#285fe6"), "myReviewLog");

    UserActionModel staffReviewLogObjModel = new UserActionModel(R.string.staff_review_log, R.drawable.ic_teacher1, true,
            Color.parseColor("#0c6603"), "staffReviewLog");

    UserActionModel viewStudentReviewObjModel = new UserActionModel(R.string.view_student_review, R.drawable.ic_student, true,
            Color.parseColor("#b400ab"), "viewStudentReview");

    UserActionModel viewStaffReviewObjModel = new UserActionModel(R.string.view_staff_review, R.drawable.ic_teacher1, true,
            Color.parseColor("#cd95e6"), "viewStaffReview");

    UserActionModel parentHelpObjModel = new UserActionModel(R.string.parent_help, R.drawable.ic_parent, false,
            0, "parentHelp");
    UserActionModel studentHelpObjModel = new UserActionModel(R.string.student_help, R.drawable.ic_student, true,
            Color.parseColor("#285fe6"), "studentHelp");
    UserActionModel staffHelpObjModel = new UserActionModel(R.string.staff_help, R.drawable.ic_teacher1, true,
            Color.parseColor("#0c6603"), "staffHelp");
     /*   UserActionModel viewGalleryObjModel = new UserActionModel(R.string.view_gallery,  R.drawable.ic_gallery, false  ,
               0, "viewGallery");
        UserActionModel addGalleryObjModel = new UserActionModel(R.string.add_gallery,  R.drawable.ic_add_gallery, false  ,
               0, "addGallery");*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();

        mDb = MainDatabase.getDatabase(this);

        whichActionData = getIntent().getStringExtra("whichActionData");

        loggedUserID = Gc.getSharedPreference(Gc.USERID, this);
        userType = Gc.getSharedPreference(Gc.USERTYPETEXT, this);
        colorIs = Config.getAppColor(this, true);

        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        setContentView(R.layout.fragment_view_all_list);

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/5134046064", this, adContainer);
        }

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.checking_permission));
        generalSettingData = (GeneralSettingData) getIntent().getSerializableExtra("GeneralSettingObj");
        final SwipeRefreshLayout swipe_refresh_list = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });

        findViewById(R.id.fl_fragment_all_list).setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recycler_view_list_of_data = findViewById(R.id.recycler_view_list_of_data);


        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
            switch (getIntent().getStringExtra("whichActionData")) {
                case "manageFee":
                    getSupportActionBar().setTitle(getString(R.string.manage_fees));
                    userActionList.add(createFeeTypeObjModel);
                    userActionList.add(classFeeObjModel);
                    userActionList.add(transportFeeObjModel);
                    userActionList.add(feePaymentObjModel);
                    userActionList.add(feeReceiptObjModel);
                    userActionList.add(updateStudentFeeObjModel);
                    break;
                case "finance":
                    getSupportActionBar().setTitle(getString(R.string.finance));
                    userActionList.add(feeAccountObjModel);
                    break;
                case "createStudent":
                    getSupportActionBar().setTitle(getString(R.string.student));
                    userActionList.add(createStudentObjModel);
                    userActionList.add(updateStudentFeeObjModel);
//                    userActionList.add(changeClassObjModel);
//                    userActionList.add(promoteToNextClassObjModel);
                    userActionList.add(studentAttendanceEntryObjModel);
//                    userActionList.add(attendanceReportObjModel);
                    break;
                case "createStaff":
                    getSupportActionBar().setTitle(getString(R.string.staff));
                    userActionList.add(createStaffObjModel);
                    userActionList.add(staffAttendanceObjModel);
                    userActionList.add(staffAttendanceReportObjModel);
                    break;
                case "teachersNotes":
                    getSupportActionBar().setTitle(getString(R.string.review_log));
                    userActionList.add(studentReviewLogObjModel);
                    userActionList.add(staffReviewLogObjModel);
//                    userActionList.add(viewStudentReviewObjModel);
//                    userActionList.add(viewStaffReviewObjModel);
                    break;
                case "exam":
                    getSupportActionBar().setTitle(getString(R.string.exam));
                    userActionList.add(createGradesObjModel);
                    userActionList.add(gradeMarksSetupObjModel);
                    userActionList.add(examTypeObjModel);
                    userActionList.add(fillExamResultObjModel);
                    userActionList.add(examResultReportObjModel);
                    userActionList.add(createCoScholasticAreaObjModel);
                    userActionList.add(createCoScholasticGradesObjModel);

                    break;
                case "homework":
                    getSupportActionBar().setTitle(getString(R.string.assignment));
                    userActionList.add(homeworkEntryObjModel);
                    break;
                case "message":
                    sp.edit().putBoolean("SMSToStudent", true).putBoolean("SMSToParent", true).putBoolean("SMSToTeacher", true).commit();
                    getSupportActionBar().setTitle(getString(R.string.communicate));
                    userActionList.add(sendSmsObjModel);
                    userActionList.add(messengerActivityObjModel);
                    userActionList.add(createNotificationObjModel);
//                    userActionList.add(broadcastObjModel);
                    break;
                case "timeTable":
                    getSupportActionBar().setTitle(getString(R.string.time_table));
//                    userActionList.add(createSlotTimeTableObjModel);
//                    userActionList.add(dayWiseTimeTableObjModel);
                    userActionList.add(dateWiseTimeTableObjModel);
                    userActionList.add(viewTimeTableObjModel);

                    break;
                case "schoolInformation":
                    getSupportActionBar().setTitle(getString(R.string.school_information));
                    userActionList.add(schoolDetailsObjModel);
                    userActionList.add(schoolInformationObjModel);
                    userActionList.add(sessionObjModel);
                    break;
                case "help":
                    getSupportActionBar().setTitle(getString(R.string.help));
                    if (!(Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equalsIgnoreCase(Gc.DEMO))) {
                        userActionList.add(reportBugObjModel);
                        userActionList.add(softwareDetailsObjModel);
                    }
//                    userActionList.add(howToUseObjModel);
                    userActionList.add(faqObjModel);
//                    userActionList.add(softwarePricingObjModel);
//                    userActionList.add(aboutJunctionObjModel);
//                    userActionList.add(parentHelpObjModel);
//                    userActionList.add(studentHelpObjModel);
//                    userActionList.add(staffHelpObjModel);
                    break;
                case "library":
                    getSupportActionBar().setTitle(getString(R.string.library));
                    userActionList.add(issuedBooksObjModel);
                    break;
                case "transport":
                    getSupportActionBar().setTitle(getString(R.string.transport_text));
//                    userActionList.add(addVehicleObjModel);
//                    userActionList.add(addStopObjModel);
                    //  userActionList.add(transportSetupObjModel);
                    userActionList.add(viewLocationObjModel);
                    break;
            }

        }

        recycler_view_list_of_data.setPadding(15, 15, 10, 15);
        recycler_view_list_of_data.setLayoutManager(new GridLayoutManager(this, 2));
        userActionAdapter = new UserActionAdapter(this, userActionList, colorIs, false);
        recycler_view_list_of_data.setAdapter(userActionAdapter);
    }

    void buildUserActionList() {
        userActionList.clear();
        switch (whichActionData) {
            case "schoolInformation":
                Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.school_information));
                userActionList.add(schoolInformationObjModel);
                userActionList.add(sessionObjModel);
                break;
            case "manageFee":
                Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.manage_fees));

                if (Gc.getSharedPreference(Gc.FEESETUPALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
                    userActionList.add(createFeeTypeObjModel);
                    userActionList.add(classFeeObjModel);
                    userActionList.add(transportFeeObjModel);
                    userActionList.add(updateStudentFeeObjModel);
                }

                if (Gc.STUDENTPARENT.equalsIgnoreCase(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this))) {
                    feeReceiptObjModel.name = R.string.fee_payment;
                }

                if (Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
                    userActionList.add(feePaymentObjModel);
                    userActionList.add(feeReceiptObjModel);
                }
                if (Gc.getSharedPreference(Gc.FEEDISPLAYALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
                    if (!userActionList.contains(feeReceiptObjModel))
                        userActionList.add(feeReceiptObjModel);
                    if (!Gc.STUDENTPARENT.equalsIgnoreCase(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this))) {
                        if (!userActionList.contains(updateStudentFeeObjModel))
                            updateStudentFeeObjModel.setName(R.string.fee_details);
                        userActionList.add(updateStudentFeeObjModel);
                    }

                }
                break;
            case "teachersNotes":
                getSupportActionBar().setTitle(getString(R.string.review_log));
                if (userPermissionList.contains(Gc.CreateStudentReview) || userPermissionList.contains(Gc.DisplayStudentReview)) {
                    userActionList.add(studentReviewLogObjModel);
                }
                if (userPermissionList.contains(Gc.CreateStaffReview) || userPermissionList.contains(Gc.DisplayStaffReview)) {
                    userActionList.add(staffReviewLogObjModel);
                }
                if (!Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN)) {
                    if (!userActionList.contains(myReviewLogObjModel))
                        userActionList.add(myReviewLogObjModel);
                }
                break;
            case "finance":
                getSupportActionBar().setTitle(getString(R.string.fee_account));

                if (userPermissionList.contains("FinanceSetup")) {
                    userActionList.add(feeAccountObjModel);
                }
                break;
            case "homework":
                if (userPermissionList.contains("CreateHomework")) {
                    userActionList.add(homeworkEntryObjModel);
                } else if (userPermissionList.contains("DisplayHomework")) {
                    userActionList.add(homeworkReportObjModel);
                }
                break;

            case "transport":
                if (userPermissionList.contains("DisplayLocation"))
                    userActionList.add(viewLocationObjModel);
                break;

            case "createStudent":
                getSupportActionBar().setTitle(getString(R.string.student));
                if (userPermissionList.contains("StudentSetup")) {
                    userActionList.add(createStudentObjModel);
//                    userActionList.add(changeClassObjModel);
                }
                if (userPermissionList.contains("FeesSetup")) {
                    if (!userActionList.contains(updateStudentFeeObjModel))
                        userActionList.add(updateStudentFeeObjModel);
                }
                if (userPermissionList.contains("CreateStudentAttendence")) {
                    userActionList.add(studentAttendanceEntryObjModel);
                }
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT))
                    userActionList.add(attendanceReportObjModel);
                break;

            case "leave":
                getSupportActionBar().setTitle(getString(R.string.leaves));
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF))
                    userActionList.add(assignedLeaveObjModel);
                userActionList.add(appliedLeaveObjModel);
                break;
            case "timeTable":
                getSupportActionBar().setTitle(getString(R.string.time_table));

                if (userPermissionList.contains("TimeTableSetup")) {
//                    userActionList.add(createSlotTimeTableObjModel);
//                    userActionList.add(dayWiseTimeTableObjModel);
                    userActionList.add(dateWiseTimeTableObjModel);
                    userActionList.add(viewTimeTableObjModel);
                }
                if (userPermissionList.contains("DisplayTimeTable")) {
                    if (!(userActionList.contains(viewTimeTableObjModel))) {
                        userActionList.add(viewTimeTableObjModel);
                    }
                }
                break;
            case "library":
                getSupportActionBar().setTitle(getString(R.string.library));

                if (userPermissionList.contains("DisplayIssueReturn")) {
                    userActionList.add(issuedBooksObjModel);
                }
                break;
            case "message":
                getSupportActionBar().setTitle(getString(R.string.communicate));

                if (userPermissionList.contains("Noticeboard")) {
                    userActionList.add(createNotificationObjModel);
                }
                if (userPermissionList.contains("SendMessage")) {
                    userActionList.add(sendSmsObjModel);
                }
                userActionList.add(messengerActivityObjModel);
                break;
            case "createStaff":
                getSupportActionBar().setTitle(getString(R.string.staff));

                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
                    if (userPermissionList.contains("StaffSetup")) {
                        userActionList.add(createStaffObjModel);
                    } else if (userPermissionList.contains("DisplayStaff")) {
                        if (!userActionList.contains(createStaffObjModel))
                            userActionList.add(createStaffObjModel);
                    }
                    if (userPermissionList.contains(Gc.CreateStaffAttendence)) {
                        userActionList.add(staffAttendanceObjModel);
                    }

                    if (userPermissionList.contains(Gc.DisplayStaffAttendence) && userPermissionList.contains(Gc.DisplayStaff)) {
                        if (!userActionList.contains(staffAttendanceReportObjModel))
                            userActionList.add(staffAttendanceReportObjModel);
                    }
                }

                break;
            case "exam":
                getSupportActionBar().setTitle(getString(R.string.exam));

                if (userPermissionList.contains(Gc.GradeSetup)) {
                    userActionList.add(createGradesObjModel);
                    userActionList.add(gradeMarksSetupObjModel);
                    userActionList.add(createCoScholasticAreaObjModel);
                    userActionList.add(createCoScholasticGradesObjModel);
                }
                if (userPermissionList.contains(Gc.ResultSetup)) {
                    userActionList.add(examTypeObjModel);
                }
                if (userPermissionList.contains(Gc.ExamReport)) {
                    if (!userActionList.contains(examResultReportObjModel))
                        userActionList.add(examResultReportObjModel);
                }
                if (userPermissionList.contains(Gc.ResultEntry)) {
                    if (!userActionList.contains(examResultReportObjModel))
                        userActionList.add(examResultReportObjModel);
                    if (!userActionList.contains(fillExamResultObjModel))
                        userActionList.add(fillExamResultObjModel);
                }
                break;
        }
        userActionAdapter.notifyDataSetChanged();
    }

    public void setOnClickViewAction(UserActionModel userActionModel) {
        switch (userActionModel.getActivityName()) {
            case "studentReviewLog":
                /*if (selectedStudent != null) {
                    Intent intent = new Intent(this, ViewFeedback.class);
                    intent.putExtra("student", selectedStudent);
                    startActivity(intent);
                    return;
                }*/
                startActivity(new Intent(this, StudentActivity.class).putExtra("action", "reviewLog"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "staffReviewLog":
              /*  if (selectedStaff != null) {
                    Intent intent = new Intent(this, ViewFeedback.class);
                    intent.putExtra("staff", selectedStaff);
                    startActivity(intent);
                    return;
                }*/
                startActivity(new Intent(this, StaffActivity.class).putExtra("action", "reviewLog"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
//            case "viewStudentReview":
//                startActivity(new Intent(this, ViewUserReviewActivity.class).putExtra("feedbackOf","student"/* 0 for student*/));
//                overridePendingTransition(R.anim.enter, R.anim.nothing);
//                break;
//            case "viewStaffReview":
//                startActivity(new Intent(this, ViewUserReviewActivity.class).putExtra("feedbackOf","teacher"/* 1 for staff*/));
//                overridePendingTransition(R.anim.enter, R.anim.nothing);
//                break;
            case "myReviewLog":
                startActivity(new Intent(SubActivity.this, MyReview.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createFeeType":
                startActivity(new Intent(this, FeeTypeActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "classFee":
                startActivity(new Intent(this, ClassFeeActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "assignedLeave":
                startActivity(new Intent(this, AssignedLeaveActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "applyLeave":
                startActivity(new Intent(this, LeaveActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "feeAccount":
                startActivity(new Intent(this, AccountActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "transportFee":
                startActivity(new Intent(this, TransportFeeSetupActivity.class).putExtra("id", "addTransportFee"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "feePayment":
                startActivity(new Intent(this, StudentActivity.class).putExtra("action", "feepayment"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "viewDuePayment":
                startActivity(new Intent(this, AnotherActivity.class).putExtra("id", "paymentFee").putExtra("isPermissionForPayment", isPermissionForPayment));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "feeReceipt":
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
                    if (selectedStudent != null) {
                        Intent intent = new Intent(this, StudentDueActivity.class);
                        intent.putExtra("student", selectedStudent);
                        startActivity(intent);
                        return;
                    }
                }

                if (selectedStudent != null) {
                    Intent intent = new Intent(this, FeeReceiptsActivity.class);
                    intent.putExtra("student", selectedStudent);
                    startActivity(intent);
                    return;
                }
                startActivity(new Intent(this, StudentActivity.class).putExtra("action", "feeReceipt"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
//            case "broadcast":
//                startActivity(new Intent(this, BroadcastActivity.class));
//                overridePendingTransition(R.anim.enter, R.anim.nothing);
//                break;
            case "updateStudentFee":
                if (selectedStudent != null) {

                    Intent intent = new Intent(this, StudentFeeListActivity.class);
                    intent.putExtra("student", selectedStudent);
                    startActivity(intent);
                    return;
                }
                startActivity(new Intent(this, StudentActivity.class)
                        .putExtra("action", "updateStudentFee"));

                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createStudent":
                startActivity(new Intent(this, StudentActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "changeClass":
                startActivity(new Intent(this, StudentActivity.class)
                        .putExtra("id", "transferClass"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "promoteToNextClass":
                startActivity(new Intent(this, StudentActivity.class)
                        .putExtra("id", "promoteStudent"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "studentAttendanceEntry":
                startActivity(new Intent(this, StudentDayAttendenceActivity.class));
                //startActivity(new Intent(StudentSecondPageActivity.this, AttendanceEnteryActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
           /* case "reportView":
                startActivity(new Intent(this, TeacherReportView.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;*/
            case "createStaff":
                startActivity(new Intent(this, StaffActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "staffAttendance":
                startActivity(new Intent(this, StaffAttendanceActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "staffAttendanceReport":
                startActivity(new Intent(this, StaffActivity.class)
                        .putExtra("action", "showAttendence"));


                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createGrades":
                startActivity(new Intent(this, ScholasticGradeActivity.class)
                        .putExtra("id", "createGrades"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "gradeMarksSetup":
                startActivity(new Intent(this, ScholasticGradeMarksActivity.class)
                        .putExtra("id", "GradesRange"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createExamType":
                startActivity(new Intent(this, ExamTypeActivity.class)
                        .putExtra("id", "examType"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "fillExamResult":
                /*startActivity(new Intent(this, AnotherActivity.class)
                        .putExtra("id", "examList"));*/
                startActivity(new Intent(this, EnterExamResultActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createCoScholasticArea":
                /*startActivity(new Intent(this, AnotherActivity.class)
                        .putExtra("id", "examList"));*/
                startActivity(new Intent(this, CoScholasticAreaActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "createCoScholasticGrades":
                /*startActivity(new Intent(this, AnotherActivity.class)
                        .putExtra("id", "examList"));*/
                startActivity(new Intent(this, CoScholasticGradeActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "sendSms":
//                sp.edit().putBoolean("SMSToTeacher", true).putBoolean("SMSToStudent", true).putBoolean("SMSToParent", true).commit();
                startActivity(new Intent(this, SMSInformationActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "createNotice":
                startActivity(new Intent(this, NewNotificationBarActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "messengerActivity":
                startActivity(new Intent(SubActivity.this, ChatActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createSlotTimeTable":
                startActivity(new Intent(this, CreateSlotsActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "dayWiseTimeTable":
                startActivity(new Intent(this, DayWiseActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "dateWiseTimeTable":
                startActivity(new Intent(this, DateWiseActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "viewTimeTable":
                startActivity(new Intent(this, ViewTimetableActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "schoolInformation":
//                if (locationPermission()) {
                    startActivity(new Intent(this, SchoolInformationActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
//                }
                break;
            case "schoolDetails":
                Intent intent = new Intent(this, SchoolPublicDetailsActivity.class);
                startActivityForResult(intent, AdminNavigationDrawerNew.GENERAL_SETTING_CODE);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "schoolSession":
                startActivityForResult(new Intent(this, SessionActivity.class)
                        .putExtra("AddSessionActive", getIntent().getBooleanExtra("AddSessionActive", true))
                        .putExtra("GeneralSettingObj", generalSettingData), Config.AddSessionActive);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "reportBug":
                startActivity(new Intent(this, IssueListActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "softwareDetails":
                startActivity(new Intent(this, LicenceActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "homeworkEntry":
                startActivity(new Intent(this, AssignmentMonthWiseActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "homeworkReport":
                if (selectedStudent != null) {
                    startActivity(new Intent(this, AssignmentMonthWiseActivity.class).
                            putExtra(Config.isFromNotification, true)
                            .putExtra("displayOnly", Gc.TRUE)
                            .putExtra("student", selectedStudent));
                } else {
                    startActivity(new Intent(this, AssignmentMonthWiseActivity.class)
                            .putExtra("displayOnly", Gc.TRUE));
                }
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "attendanceReport":
                if (selectedStudent != null) {
                    Intent intent1 = new Intent(this, StudentMonthAttendanceActivity.class);
                    intent1.putExtra("student", selectedStudent);
                    startActivity(intent1);
                } else {
                    startActivity(new Intent(this, StudentActivity.class));
                }
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "examResultReport":
                if (selectedStudent != null) {
                    Intent intent1 = new Intent(this, ExamResultStudentActivity.class);
                    intent1.putExtra("student", selectedStudent);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                    return;
                } else {
                    startActivity(new Intent(this, StudentActivity.class)
                            .putExtra("action", "scholasticExamResult"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
            case "howToUse":
                Intent howToUseIntent =
                        new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_HELP_LINK));
                startActivity(howToUseIntent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "faq":
                Intent faqeIntent =
                        new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_HELP_FAQ));
                startActivity(faqeIntent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "softwarePricing":
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_PRICING_LINK)));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "parentHelp":
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_PARENT_HELP_LINK)));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "studentHelp":
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_STUDENT_HELP_LINK)));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "staffHelp":
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_STAFF_HELP_LINK)));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "aboutZeroerp":
                startActivity(new Intent(this, AboutUsZeroerpActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;


//            case "MyNotification":
//                startActivity(new Intent(this, MyNotificationActivity.class));
//                overridePendingTransition(R.anim.enter, R.anim.nothing);
//                break;
            case "issuedBooks":
                startActivity(new Intent(this, Library.class).
                        putExtra(Config.isFromNotification, true));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            /*case "transportSetup":
                if (locationPermission()) {
                    startActivity(new Intent(this, TransportActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;*/
            case "viewLocation":
                if (locationPermission()) {
                    if (sp.getString("vehicleRouteId", "").equalsIgnoreCase("") || sp.getString("routeType", "").equalsIgnoreCase("") || sp.getString("trackThrough", "").equalsIgnoreCase("")) {
                        startActivity(new Intent(this, FirstScreenViewLocationActivity.class));
                        overridePendingTransition(R.anim.enter, R.anim.nothing);

                    } else {
                        startActivity(new Intent(this, TrackLocationActivity.class));
                        overridePendingTransition(R.anim.enter, R.anim.nothing);
                    }
                }
                break;
            case "addVehicle":
                if (locationPermission()) {
                    startActivity(new Intent(this, AnotherActivity.class).putExtra("id", "addVehicle"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
            case "addStop":
                if (locationPermission()) {
                    startActivity(new Intent(this, AnotherActivity.class).putExtra("id", "addStop"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent();
        intent.putExtra("isImageChanged", isImageChanged);
        intent.putExtra("isSessionChanged", isSessionChanged);
        setResult(res, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent();
//        intent.putExtra("isImageChanged", isImageChanged);
//        intent.putExtra("isSessionChanged", isSessionChanged);
//        intent.putExtra("isDataUpdated", isDataUpdated);
//        intent.putExtra("isDataInserted", isDataInserted);
//        setResult(res, intent);
//        finish();
//        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == Config.AddSessionActive)
            isSessionChanged = true;
        else if (resultCode == RESULT_OK && requestCode == AdminNavigationDrawerNew.GENERAL_SETTING_CODE) {

            if (!isDataUpdated)
                isDataUpdated = data.getBooleanExtra("isDataUpdated", false);
            if (!isDataInserted) isDataInserted = data.getBooleanExtra("isDataInserted", false);
            if (isDataUpdated || isDataInserted) {
                res = resultCode;
                fetchGeneralSettingData();
            } else res = RESULT_CANCELED;

        } else if (resultCode == RESULT_CANCELED && requestCode == AdminNavigationDrawerNew.GENERAL_SETTING_CODE)
            res = resultCode;
    }

    private void fetchConversationPermission() throws JSONException {
        if ((Gc.getSharedPreference(Gc.USERTYPETEXT, this).equalsIgnoreCase("Administrator") || sp.getString("user_type", "").equalsIgnoreCase("Admin"))) {
            sp.edit().putBoolean("MsgToTeacher", true)
                    .putBoolean("MsgToStudent", true)
                    .putBoolean("MsgToParent", true).commit();
            startActivity(new Intent(SubActivity.this, ConversationsActivity.class)
                    .putExtra("ComesFromMainActivity", true));
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        }
    }

    private void fetchGeneralSettingData() {
        progressbar.show();
        String generalSettUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "GeneralSettingApi.php?" +
                "databaseName=" + sp.getString("organization_name", "");
        Log.d("generalSettUrl", generalSettUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
                generalSettUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if (!isFinishing()) {
                            progressbar.dismiss();
                            DbHandler.longInfo(s);
                            Log.e("GeneralSettingDataRes", s);


                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(s);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(Config.GENERAL_SETTING, s);
                                editor.commit();

                                GeneralSettingData oo1 = (new Gson()).fromJson(s, new TypeToken<GeneralSettingData>() {
                                }.getType());
                                generalSettingData = oo1.getResult().get(0);
                            }
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (!isFinishing()) {
                    progressbar.dismiss();
                    Log.e("GeneralSettingDataErr", volleyError.toString());


                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request, this.getLocalClassName());

    }

    public boolean locationPermission() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                }, Config.LOCATION);
            }


            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        switch (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)) {
            case Gc.ADMIN:

                break;
            case Gc.STAFF:
                mDb.schoolStaffModel()
                        .getStaffById(Gc.getSharedPreference(Gc.STAFFID, this))
                        .observe(this, new Observer<SchoolStaffEntity>() {
                            @Override
                            public void onChanged(@Nullable SchoolStaffEntity staffEntity) {
                                selectedStaff = new Gson().toJson(staffEntity);
                            }
                        });
                break;

            case Gc.STUDENTPARENT:
                mDb.schoolStudentModel()
                        .getStudentById(Gc.getSharedPreference(Gc.USERID, this))
                        .observe(this, new Observer<SchoolStudentEntity>() {
                            @Override
                            public void onChanged(@Nullable SchoolStudentEntity schoolStudentEntity) {
                                selectedStudent = new Gson().toJson(schoolStudentEntity);
                            }
                        });
                break;

        }


        if (!(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN))) {
            mDb.userPermissionsModel()
                    .getUserPermissions()
                    .observe(this, new Observer<List<String>>() {
                        @Override
                        public void onChanged(@Nullable List<String> userPermissionsEntities) {
                            userPermissionList.clear();
                            userPermissionList.addAll(userPermissionsEntities);
                            buildUserActionList();
                        }
                    });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
