package in.junctiontech.school.schoolnew;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.owner.OrganizationDetailActivity;
import in.junctiontech.school.schoolnew.owner.OwnerSchoolData;
import in.junctiontech.school.schoolnew.owner.OwnerSchoolsActivity;
import in.junctiontech.school.schoolnew.common.Gc;

/**
 * Created by JAYDEVI BHADE on 7/2/2017.
 */

public class DemoSchoolActivity extends AppCompatActivity {
    private FirebaseRemoteConfig firebaseRemoteConfig;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RadioButton rb_demo_screen_school, rb_demo_screen_college, rb_demo_screen_coaching;
    private LinearLayout ll_demo_screen_choices;
    private boolean firstTimeAnimation = true;
    private RadioGroup rg_demo_screen_type;
    private ProgressDialog progressDialog;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private long cacheExpiration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // add for remove the title bar from activity
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_choice);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        /* --- Remote Config ---*/
        // Remote Config object instance and enables
        // developer mode to allow for frequent refreshes of the cache

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        //   progressDialog.setCancelable(false);
        progressDialog.show();
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                //.setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(remoteConfigSettings);
        cacheExpiration = 3600;

        firebaseRemoteConfig.setConfigSettingsAsync(remoteConfigSettings);

//expire the cache immediately for development mode.
        /*if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }*/
        fetchRemoteConfig();

        rg_demo_screen_type = findViewById(R.id.rg_demo_screen_type);
        rb_demo_screen_school = findViewById(R.id.rb_demo_screen_school);
        rb_demo_screen_college = findViewById(R.id.rb_demo_screen_college);
        rb_demo_screen_coaching = findViewById(R.id.rb_demo_screen_coaching);

        ll_demo_screen_choices = findViewById(R.id.ll_demo_screen_choices);

        final Animation fadout = AnimationUtils.
                loadAnimation(DemoSchoolActivity.this, R.anim.fade_out);
        fadout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rg_demo_screen_type.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        rb_demo_screen_school.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
//                ll_demo_screen_choices.setVisibility(View.VISIBLE);
                    rg_demo_screen_type.startAnimation(fadout);
                if (firstTimeAnimation) {
                    ll_demo_screen_choices.setVisibility(View.VISIBLE);
                    ll_demo_screen_choices.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this,
                            R.anim.bottom_up));
                    firstTimeAnimation = false;

                }
            }
        });
        rb_demo_screen_college.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
//                ll_demo_screen_choices.setVisibility(View.VISIBLE);

                    rg_demo_screen_type.startAnimation(fadout);
                if (firstTimeAnimation) {
                    ll_demo_screen_choices.setVisibility(View.VISIBLE);
                    ll_demo_screen_choices.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this,
                            R.anim.bottom_up));
                    firstTimeAnimation = false;

                }
            }
        });
        rb_demo_screen_coaching.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
//                ll_demo_screen_choices.setVisibility(View.VISIBLE);
                    rg_demo_screen_type.startAnimation(fadout);
                if (firstTimeAnimation) {
                    ll_demo_screen_choices.setVisibility(View.VISIBLE);
                    ll_demo_screen_choices.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this,
                            R.anim.bottom_up));
                    firstTimeAnimation = false;

                }
            }
        });

        final LinearLayout ly_demo_owner = (LinearLayout) findViewById(R.id.ly_demo_user_administrator);
        final LinearLayout ly_demo_teacher = (LinearLayout) findViewById(R.id.ly_demo_teacher);
        final LinearLayout ly_demo_parent = (LinearLayout) findViewById(R.id.ly_demo_parent);
        final LinearLayout ly_demo_student = (LinearLayout) findViewById(R.id.ly_demo_student);

        final LinearLayout ly_demo_user_owner = (LinearLayout) findViewById(R.id.ly_demo_user_owner);
        final LinearLayout ly_demo_principal = (LinearLayout) findViewById(R.id.ly_demo_principal);

        ly_demo_user_owner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_user_owner.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });
        ly_demo_principal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_principal.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });

        ly_demo_owner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_owner.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });
        ly_demo_teacher.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_teacher.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });
        ly_demo_parent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_parent.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });
        ly_demo_student.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ly_demo_student.startAnimation(AnimationUtils.loadAnimation(DemoSchoolActivity.this, R.anim.animator_for_bounce));

                return false;
            }
        });

        ly_demo_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*  Firebase Analytics */

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, firebaseRemoteConfig.getString("demo_organization_name"));
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Demo Admin");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");

                //Logs an app event.
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                //Sets whether analytics collection is enabled for this app on this device.
                mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

                //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
                // mFirebaseAnalytics.setMinimumSessionDuration(20000);

                //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
                mFirebaseAnalytics.setSessionTimeoutDuration(500);

                //Sets the user ID property.
                mFirebaseAnalytics.setUserId(firebaseRemoteConfig.getString("demo_organization_name"));

                //Sets a user property to a given value.
                mFirebaseAnalytics.setUserProperty("UserLogin", firebaseRemoteConfig.getString("demo_admin_username"));


//                Log.e("RITU", firebaseRemoteConfig.getString("demo_organization_name")+" ritu");
//                Log.e("RITU", firebaseRemoteConfig==null?"RITU":"Varsha");
//                Log.e("RITU",( firebaseRemoteConfig.getString("demo_admin_password")).equalsIgnoreCase("")?"RITU":"Varsha");

                Intent intent = new Intent(DemoSchoolActivity.this, FirstScreenActivity.class);
                if (rb_demo_screen_school.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_organization_name").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_organization_name"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_admin_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_admin_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }

                } else if (rb_demo_screen_college.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_college_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_college_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_college_admin_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_college_admin_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                } else if (rb_demo_screen_coaching.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_coaching_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_coaching_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_coaching_admin_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_coaching_admin_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                }


                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

//                submitToLogin(firebaseRemoteConfig.getString("demo_organization_name"),
//                        firebaseRemoteConfig.getString("demo_admin_username"),
//                        firebaseRemoteConfig.getString("demo_admin_password"));
            }
        });
        ly_demo_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, firebaseRemoteConfig.getString("demo_organization_name"));
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Demo Teacher");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
                //Logs an app event.
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                //Sets whether analytics collection is enabled for this app on this device.
                mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

                //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
                // mFirebaseAnalytics.setMinimumSessionDuration(20000);

                //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
                mFirebaseAnalytics.setSessionTimeoutDuration(500);

                //Sets the user ID property.
                mFirebaseAnalytics.setUserId(firebaseRemoteConfig.getString("demo_organization_name"));

                //Sets a user property to a given value.
                mFirebaseAnalytics.setUserProperty("UserLogin", firebaseRemoteConfig.getString("demo_teacher_username"));


                Intent intent = new Intent(DemoSchoolActivity.this, FirstScreenActivity.class);


                if (rb_demo_screen_school.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_organization_name").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_organization_name"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_teacher_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_teacher_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                } else if (rb_demo_screen_coaching.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_coaching_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_coaching_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_coaching_teacher_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_coaching_teacher_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                } else if (rb_demo_screen_college.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_college_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_college_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_college_teacher_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_college_teacher_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                }


                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

//                submitToLogin(firebaseRemoteConfig.getString("demo_organization_name"),
//                        firebaseRemoteConfig.getString("demo_teacher_username"),
//                        firebaseRemoteConfig.getString("demo_teacher_password"));

            }
        });

        ly_demo_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, firebaseRemoteConfig.getString("demo_organization_name"));
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Demo Parent");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");


                Intent intent = new Intent(DemoSchoolActivity.this, FirstScreenActivity.class);
                if (rb_demo_screen_school.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_organization_name").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_organization_name"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_parent_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_parent_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }

                } else if (rb_demo_screen_college.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_college_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_college_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_college_parent_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_college_parent_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                } else if (rb_demo_screen_coaching.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_coaching_organization_key").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_coaching_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_coaching_parent_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_coaching_parent_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                }


                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);


            }
        });
        ly_demo_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, firebaseRemoteConfig.getString("demo_organization_name"));
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Demo Student");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");


                //Logs an app event.
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                //Sets whether analytics collection is enabled for this app on this device.
                mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

                //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
                //mFirebaseAnalytics.setMinimumSessionDuration(20000);

                //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
                mFirebaseAnalytics.setSessionTimeoutDuration(500);

                //Sets the user ID property.
                mFirebaseAnalytics.setUserId(firebaseRemoteConfig.getString("demo_organization_name"));

                //Sets a user property to a given value.
                mFirebaseAnalytics.setUserProperty("UserLogin", firebaseRemoteConfig.getString("demo_student_username"));

//                submitToLogin(firebaseRemoteConfig.getString("demo_organization_name"),
//                        firebaseRemoteConfig.getString("demo_student_username"),
//                        firebaseRemoteConfig.getString("demo_student_password"));

                Intent intent = new Intent(DemoSchoolActivity.this, FirstScreenActivity.class);

                if (rb_demo_screen_school.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_admin_password").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_organization_name"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_student_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_student_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }

                } else if (rb_demo_screen_college.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_admin_password").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_college_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_college_student_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_college_student_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }

                } else if (rb_demo_screen_coaching.isChecked()) {
                    if (firebaseRemoteConfig.getString("demo_admin_password").equalsIgnoreCase("")) {
                        showToast();
                        setResult(RESULT_CANCELED, intent);
                    } else {
                        intent.putExtra("organization", firebaseRemoteConfig.getString("demo_coaching_organization_key"));
                        intent.putExtra("username", firebaseRemoteConfig.getString("demo_coaching_student_username"));
                        intent.putExtra("pwd", firebaseRemoteConfig.getString("demo_coaching_student_password"));
                        intent.putExtra(Gc.FROMDEMO, Gc.FROMDEMO);

                        setResult(RESULT_OK, intent);
                    }
                }


                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

            }
        });


        ly_demo_principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        startActivity(new Intent(DemoSchoolActivity.this, OrganizationDetailActivity.class).putExtra("isPrincipal", true)
                                .putExtra("data", new OwnerSchoolData("DEMOSCHOOL", "DEMOSCHOOL", "http://testeducation.zeroerp.com/upload/2F40F6F0.jpg")));
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.nothing);

                    }
                }, 3000);

            }
        });

        ly_demo_user_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        startActivity(new Intent(DemoSchoolActivity.this, OwnerSchoolsActivity.class));
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.nothing);

                    }
                }, 3000);

            }
        });


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    fetchRemoteConfig();
                }
            }
        };

    }

    private void showToast() {
        Toast.makeText(DemoSchoolActivity.this, getString(R.string.error_network_timeout) + "\n" +
                        getString(R.string.internet_not_available_please_check_your_internet_connectivity),
                Toast.LENGTH_LONG).show();
    }

    private void fetchRemoteConfig() {
        firebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(DemoSchoolActivity.this, "Fetch Succeeded",
                                    Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            // Once the config is successfully fetched it must be activated before newly fetched
                            // values are returned.
                            firebaseRemoteConfig.fetchAndActivate();
                            //firebaseRemoteConfig.activateFetched();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(DemoSchoolActivity.this, "Fetch Failed" + "\n" +
                                            getString(R.string.internet_not_available_please_check_your_internet_connectivity),
                                    Toast.LENGTH_SHORT).show();
                        }
                        //displayWelcomeMessage();
                    }
                });
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DemoSchoolActivity.this, FirstScreenActivity.class);
        setResult(RESULT_CANCELED, intent);

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onDestroy();

    }
}
