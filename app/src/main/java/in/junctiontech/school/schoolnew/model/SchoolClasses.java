package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;

/**
 * Created by deep on 28/03/18.
 */

public class SchoolClasses {
    public String ClassId;
    public String Session;
    public String ClassName;
    public String ClassStatus;
    public String DOE;
    public ArrayList<SchoolClassSectionEntity> section;

}
