package in.junctiontech.school.schoolnew.model;

import java.util.ArrayList;

/**
 * Created by deep on 27/03/18.
 */

public class ClassFees {
    public String ClassId;
    public String ClassName;
    public String SectionId;
    public String SectionName;
    public String Session;
    public ArrayList<FeeValues> fees;
    public ArrayList<TransportFeeValues> transportFees;

    public class TransportFeeValues{
        public String FeeId;
        public String FeeTypeID;
        public String FeeType;
        public String Frequency;
        public String FeeStatus;
        public String DOE;
        public String Amount;
        public String Distance;
        public String MasterEntryValue;
    }

}

