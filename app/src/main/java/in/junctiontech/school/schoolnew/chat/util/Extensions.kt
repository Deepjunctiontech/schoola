package `in`.junctiontech.school.schoolnew.chat.util

import android.content.res.Resources

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).toInt()