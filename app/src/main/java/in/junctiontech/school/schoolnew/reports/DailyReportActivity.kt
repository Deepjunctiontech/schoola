package `in`.junctiontech.school.schoolnew.reports

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class DailyReportActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null

    private lateinit var tvFeeCollection: TextView
    private lateinit var tvOtherIncome: TextView
    private lateinit var tvExpenses: TextView
    private lateinit var tvStudentAttendance: TextView
    private lateinit var tvStaffPresent: TextView
    private lateinit var tvStaffAbsent: TextView
    private lateinit var tvStaffHalfDay: TextView
    private lateinit var btnDate: Button

    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_report_daily)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initializeViews()
        setColorApp()
        getDailyReports()
        if (!listOf(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/2804617241", this, adContainer)
        }
    }

    private fun initializeViews() {

        progressBar = findViewById(R.id.progressBar)

        tvFeeCollection = findViewById(R.id.tv_fee_collection)
        tvOtherIncome = findViewById(R.id.tv_other_income)
        tvExpenses = findViewById(R.id.tv_expenses)
        tvStudentAttendance = findViewById(R.id.tv_student_attendance)
        tvStaffPresent = findViewById(R.id.tv_staff_present)
        tvStaffAbsent = findViewById(R.id.tv_staff_absent)
        tvStaffHalfDay = findViewById(R.id.tv_staff_half_day)

        btnDate = findViewById(R.id.btn_date)

        val simpleDateFormat = SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US)
        btnDate.text = simpleDateFormat.format(Date())

        val myCalendar = Calendar.getInstance()

        val date = OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar[Calendar.YEAR] = year
            myCalendar[Calendar.MONTH] = monthOfYear
            myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
            val myFormat = "dd-MM-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            btnDate.text = sdf.format(myCalendar.time)

            getDailyReports()
        }
        btnDate.setOnClickListener {
            DatePickerDialog(this, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]).show()
        }

    }

    private fun getDailyReports() {
        progressBar!!.visibility = View.VISIBLE
        val p = JSONObject()
        p.put("Date", btnDate.text.toString().trim { it <= ' ' })

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "reports/dailyReports/"
                + p)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            progressBar!!.visibility = View.GONE
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> try {
                    val expenses = job.getJSONObject("result").getJSONObject("dailyReports").optString("expenses")
                    val otherIncome = job.getJSONObject("result").getJSONObject("dailyReports").optString("otherIncome")
                    val feeCollection = job.getJSONObject("result").getJSONObject("dailyReports").optString("feeCollection")
                    val staffAttendanceHalfDay = job.getJSONObject("result").getJSONObject("dailyReports").optString("staffAttendanceHalfDay")
                    val staffAttendanceAbsent = job.getJSONObject("result").getJSONObject("dailyReports").optString("staffAttendanceAbsent")
                    val staffAttendancePresent = job.getJSONObject("result").getJSONObject("dailyReports").optString("staffAttendancePresent")
                    val studentAttendance = job.getJSONObject("result").getJSONObject("dailyReports").optString("studentAttendance")

                    tvFeeCollection.text = feeCollection
                    tvOtherIncome.text = otherIncome
                    tvExpenses.text = expenses
                    tvStudentAttendance.text = studentAttendance
                    tvStaffPresent.text = staffAttendancePresent
                    tvStaffAbsent.text = staffAttendanceAbsent
                    tvStaffHalfDay.text = staffAttendanceHalfDay

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                Gc.APIRESPONSE401 -> {
                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this)

                    val intent1 = Intent(this, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this)

                    val intent2 = Intent(this, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.top_layout))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}


