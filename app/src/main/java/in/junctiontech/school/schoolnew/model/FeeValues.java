package in.junctiontech.school.schoolnew.model;

/**
 * Created by deep on 28/03/18.
 */

public class FeeValues {
    public String FeeId;
    public String FeeTypeID;
    public String FeeType;
    public String Frequency;
    public String FeeStatus;
    public String DOE;
    public String Amount;

}
