package in.junctiontech.school.schoolnew.communicate;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class SendSMSActivity extends AppCompatActivity {

    MainDatabase mDb;
    String Language = "english";
    String gsm7bitChars = "@£$¥èéùìòÇØø\\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\\\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
    String gsm7bitExChar = "\\^\\n{}\\\\\\[~\\]|€";
    String gsm7bitRegExp = "^[" + gsm7bitChars + "]*$";
    String gsm7bitExRegExp = "^[" + gsm7bitChars + gsm7bitExChar + "]*$";
    String gsm7bitExOnlyRegExp = "^[\\" + gsm7bitExChar + "]*$";
    String GSM_7BIT = "GSM_7BIT";
    String GSM_7BIT_EX = "GSM_7BIT_EX";
    String UTF16 = "UTF16";
    ProgressBar progressBar;
    ArrayList<SMSReceiverEntity> smsReceiverEntities = new ArrayList<>();

    RadioButton rb_sms_mobile_plan, rb_sms_api_plan, rb_main_only, rb_all_numbers;

    TextView tv_receiver_count, tv_total_messages;
    EditText et_message_text;
    Button btn_send_sms;
    int receiverCount = 0;
    int currentSMSBalance, colorIs;
    String selectedSection, selectedClass;

    SMSReceiverListAdapter adapter;
    ArrayList<SchoolStudentEntity> studentListSection = new ArrayList<>();
    RecyclerView mRecyclerView;

    TextWatcher messagewatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            calculateTotalMessages();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        progressBar = findViewById(R.id.progressBar);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initializeViews();

        mRecyclerView = findViewById(R.id.rv_sms_send_mobiles);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new SMSReceiverListAdapter();
        mRecyclerView.setAdapter(adapter);

        buildReceiverListFromStudents();

        buildReceiverListFromStaff();

        selectedSection = getIntent().getStringExtra("section");
        selectedClass = getIntent().getStringExtra("ClassId");

        if (studentListSection.size() > 0)
            buildReceiverListFromClass(studentListSection);

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6162049094", this, adContainer);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    void initializeViews() {
        rb_sms_mobile_plan = findViewById(R.id.rb_sms_mobile_plan);
        rb_sms_mobile_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertSms = new AlertDialog.Builder(SendSMSActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                alertSms.setIcon(getResources().getDrawable(R.drawable.ic_coins));
                alertSms.setTitle(getString(R.string.sms_cost_alert));
                alertSms.setMessage(getString(R.string.sms_alert));
                alertSms.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkAndroidSMSPermission();
                    }
                });
                alertSms.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alertSms.show();
            }
        });

        rb_sms_api_plan = findViewById(R.id.rb_sms_api_plan);
        rb_sms_api_plan.setOnClickListener(view -> getSMSBalance());

        rb_main_only = findViewById(R.id.rb_main_only);
        rb_main_only.setOnClickListener(view -> {
            buildReceiverListFromStudents();
            buildReceiverListFromStaff();
            if (studentListSection.size() > 0) {
                ArrayList<SchoolStudentEntity> temp1 = new ArrayList<>();
                temp1.addAll(studentListSection);
                buildReceiverListFromClass(temp1);
            }

        });

        rb_all_numbers = findViewById(R.id.rb_all_numbers);
        rb_all_numbers.setOnClickListener(view -> {
            buildReceiverListFromStudents();
            buildReceiverListFromStaff();
            if (studentListSection.size() > 0) {
                ArrayList<SchoolStudentEntity> temp1 = new ArrayList<>();
                temp1.addAll(studentListSection);
                buildReceiverListFromClass(temp1);
            }
        });

        tv_receiver_count = findViewById(R.id.tv_receiver_count);

        et_message_text = findViewById(R.id.et_message_text);
        et_message_text.addTextChangedListener(messagewatcher);

        btn_send_sms = findViewById(R.id.btn_send_sms);
        btn_send_sms.setOnClickListener(view -> {
            if (rb_sms_api_plan.isChecked()) {
                try {
                    sendSMSApi();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (rb_sms_mobile_plan.isChecked())
                sendSMSMobilePlan();
        });

        tv_total_messages = findViewById(R.id.tv_total_messages);
    }

    void calculateTotalMessages() {
        int length, messages, per_message, remaining;
        String smsmsg;
        if (rb_sms_api_plan.isChecked()) {
            smsmsg = et_message_text.getText().toString().trim();
        } else {
            smsmsg = Gc.getSharedPreference(Gc.ERPINSTCODE, this) + ":" + et_message_text.getText().toString().trim();
        }
        String encoding = detectEncoding(smsmsg);
        length = smsmsg.length();

        if (encoding.equals(GSM_7BIT_EX)) {
            length += countGsm7bitEx(smsmsg);
        }

        per_message = messageLength(encoding);

        if (length > per_message) {
            per_message = multiMessageLength(encoding);
        }
        double x = (((double) length / (double) per_message));

        messages = (int) Math.ceil(x);

        remaining = per_message * messages - length;
        if (remaining == 0 && messages == 0) {
            remaining = per_message;
        }
        int messageCount = messages;
        tv_total_messages.setText("Parts: " + messageCount + " * " + tv_receiver_count.getText().toString() + " = " + messageCount * receiverCount + " \n Remaining Word : " + remaining + "/" + messageCount);
    }

    private int multiMessageLength(String encoding) {
        if (encoding.equals(GSM_7BIT)) {
            return 153;
        } else if (encoding.equals(GSM_7BIT_EX)) {
            return 153;
        } else if (encoding.equals(UTF16)) {
            return 67;
        } else {
            return 0;
        }
    }

    private int messageLength(String encoding) {
        if (encoding.equals(GSM_7BIT)) {
            return 160;
        } else if (encoding.equals(GSM_7BIT_EX)) {
            return 160;
        } else if (encoding.equals(UTF16)) {
            return 70;
        } else {
            return 0;
        }
    }

    private int countGsm7bitEx(String smsmsg) {
        String char2;
        int count = 0;
        int i, len;
        for (i = 0, len = smsmsg.length(); i < len; i++) {
            char char3 = smsmsg.charAt(i);
            char2 = String.valueOf(char3);
            if (char2.matches(gsm7bitExOnlyRegExp)) {
                count++;
            }
        }
        return count;
    }

    String detectEncoding(String text) {
        if (text.matches(gsm7bitRegExp)) {
            return GSM_7BIT;
        } else if (text.matches(gsm7bitExRegExp)) {
            return GSM_7BIT_EX;
        } else {
            Language="other";
            return UTF16;
        }
    }

    Boolean validateInput() {
        String msgText = et_message_text.getText().toString().trim();

        if ("".equals(msgText)) {
            Config.responseSnackBarHandler("write a message",
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        if (rb_sms_api_plan.isChecked())
            if (!(currentSMSBalance > 0)) {
                Config.responseSnackBarHandler("You Don't have SMS Balance",
                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
                return false;
            }


        return true;
    }

    void checkAndroidSMSPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED

        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.SEND_SMS

                }, 100);
            }
        }
    }

    void sendSMSMobilePlan() {
        if (!validateInput()) {
            return;
        }
//        checkAndroidSMSPermission();
        String smsmsg = Gc.getSharedPreference(Gc.ERPINSTCODE, this) + ":" + et_message_text.getText().toString().trim();
        et_message_text.getText().clear(); // clear text so that it cannot be send twice

     /*   SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(smsmsg);
        int messageCount = parts.size();*/

        String receiver = "";
        for (SMSReceiverEntity s : smsReceiverEntities) {
//            smsManager.sendMultipartTextMessage(s.mobile, null, parts, null, null);
            receiver = receiver + s.mobile + ";";
        }

        if (!(receiver.equalsIgnoreCase(""))) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + receiver));  // This ensures only SMS apps respond
            intent.putExtra("sms_body", smsmsg);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Config.responseSnackBarHandler(getString(R.string.default_sms_application),
                        findViewById(R.id.top_layout), R.color.fragment_first_green);
            }
        }
    }

    void sendSMSApi() throws JSONException {
        if (!validateInput()) {
            return;
        }
        final JSONObject param = new JSONObject();
        param.put("RecieverType", "Other");
        param.put("SMSContent", et_message_text.getText().toString().trim());
        param.put("Balance", currentSMSBalance);
        param.put("Language", Language);

        et_message_text.getText().clear(); // clear text so that it cannot be send twice

        final JSONArray phones = new JSONArray();
        for (SMSReceiverEntity p : smsReceiverEntities) {
            phones.put(p.mobile);
        }

        param.put("RecieverID", phones);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "sms/sendSms";

        progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");
                progressBar.setVisibility(View.GONE);
                switch (code) {
                    case "200":
                        /*try {
                            JSONObject resultjobj = job.getJSONObject("result");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }).start();*/
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                        /* } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Config.responseVolleyErrorHandler(SendSMSActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void buildReceiverListFromStudents() {
        String student = getIntent().getStringExtra("student");
        smsReceiverEntities.clear();
        receiverCount = 0;

        if (!(student == null)) {
            SchoolStudentEntity studentEntity = new SchoolStudentEntity();
            studentEntity = new Gson().fromJson(student, SchoolStudentEntity.class);
            buildSMSListforStudent(studentEntity);
            adapter.notifyDataSetChanged();
        }

        tv_receiver_count.setText(receiverCount + "");
    }

    void buildSMSListforStudent(SchoolStudentEntity studentEntity) {
        SMSReceiverEntity smsReceiverEntity = new SMSReceiverEntity();
        smsReceiverEntity.userid = studentEntity.AdmissionId;
        smsReceiverEntity.usertype = "STUDENT";
        smsReceiverEntity.mobileType = "Main";
        smsReceiverEntity.mobile = studentEntity.Mobile;
        smsReceiverEntity.name = studentEntity.StudentName;
        smsReceiverEntities.add(smsReceiverEntity);
        receiverCount += 1;

        if (!(studentEntity.AlternateMobile == null) && rb_all_numbers.isChecked()) {
            if (!(("").equals(studentEntity.AlternateMobile))) {
                SMSReceiverEntity smsReceiverEntityA = new SMSReceiverEntity();

                smsReceiverEntityA.usertype = "STUDENT";
                smsReceiverEntityA.userid = studentEntity.AdmissionId;
                smsReceiverEntityA.mobile = studentEntity.AlternateMobile;
                smsReceiverEntityA.mobileType = "Alternate";
                smsReceiverEntities.add(smsReceiverEntityA);
                receiverCount += 1;
            }
        }

        if (!(studentEntity.MotherMobile == null) && rb_all_numbers.isChecked()) {
            if (!(("").equals(studentEntity.MotherMobile))) {
                SMSReceiverEntity smsReceiverEntityM = new SMSReceiverEntity();
                smsReceiverEntityM.usertype = "PARENT";
                smsReceiverEntityM.userid = studentEntity.AdmissionId;
                smsReceiverEntityM.mobile = studentEntity.MotherMobile;
                smsReceiverEntityM.mobileType = "Mother";
                smsReceiverEntities.add(smsReceiverEntityM);
                receiverCount += 1;
            }
        }

        if (!(studentEntity.FatherMobile == null) && rb_all_numbers.isChecked()) {

            if (!(("").equals(studentEntity.FatherMobile))) {
                SMSReceiverEntity smsReceiverEntityF = new SMSReceiverEntity();

                smsReceiverEntityF.usertype = "PARENT";
                smsReceiverEntityF.userid = studentEntity.AdmissionId;
                smsReceiverEntityF.mobile = studentEntity.FatherMobile;
                smsReceiverEntityF.mobileType = "Father";
                smsReceiverEntities.add(smsReceiverEntityF);
                receiverCount += 1;

            }
        }

    }

    void buildReceiverListFromStaff() {
        String staff = getIntent().getStringExtra("staff");
        if (!(staff == null)) {
            smsReceiverEntities.clear();
            receiverCount = 0;
            SchoolStaffEntity schoolStaffEntity = new Gson().fromJson(staff, SchoolStaffEntity.class);
            SMSReceiverEntity smsReceiverEntity = new SMSReceiverEntity();
            smsReceiverEntity.usertype = schoolStaffEntity.StaffPositionValue;
            smsReceiverEntity.mobile = schoolStaffEntity.StaffMobile;
            smsReceiverEntity.mobileType = "Main";
            smsReceiverEntity.name = schoolStaffEntity.StaffName;
            smsReceiverEntities.add(smsReceiverEntity);
            receiverCount += 1;
            if (!(schoolStaffEntity.StaffAlternateMobile == null) && rb_all_numbers.isChecked()) {
                if (!(("").equals(schoolStaffEntity.StaffAlternateMobile))) {
                    SMSReceiverEntity smsReceiverEntityA = new SMSReceiverEntity();

                    smsReceiverEntityA.usertype = schoolStaffEntity.StaffPositionValue;
                    smsReceiverEntityA.userid = schoolStaffEntity.StaffId;
                    smsReceiverEntityA.mobile = schoolStaffEntity.StaffAlternateMobile;
                    smsReceiverEntityA.mobileType = "Alternate";
                    smsReceiverEntities.add(smsReceiverEntityA);
                    receiverCount += 1;
                }
            }
            adapter.notifyDataSetChanged();

        }
        tv_receiver_count.setText(receiverCount + "");

    }

    void buildReceiverListFromClass(List<SchoolStudentEntity> schoolStudentEntities) {

        studentListSection.clear();
        studentListSection.addAll(schoolStudentEntities);
        smsReceiverEntities.clear();
        receiverCount = 0;
        for (SchoolStudentEntity s : schoolStudentEntities) {
            buildSMSListforStudent(s);
        }
        adapter.notifyDataSetChanged();
        tv_receiver_count.setText(receiverCount + "");

    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolStudentModel().getAllStudentsSection(selectedSection,"Studying").observe(this, new Observer<List<SchoolStudentEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                if (schoolStudentEntities.size() > 0)
                    buildReceiverListFromClass(schoolStudentEntities);
            }
        });
        mDb.schoolStudentModel().getAllStudentsClass(selectedClass,"Studying").observe(this, new Observer<List<SchoolStudentEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                if (schoolStudentEntities.size() > 0)
                    buildReceiverListFromClass(schoolStudentEntities);
            }
        });

    }

    void getSMSBalance() {
        progressBar.setVisibility(View.VISIBLE);
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "sms/getSmsBalance";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");
                JSONObject resultjobj = null;
                try {
                    resultjobj = job.getJSONObject("result");
                    JSONObject smsBalance = resultjobj.getJSONObject("smsBalance");

                    String apibalance = getResources().getString(R.string.api) + " balance is " + smsBalance.optString("balance");
                    currentSMSBalance = Integer.parseInt(smsBalance.optString("balance"));

                    rb_sms_api_plan.setText(apibalance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                switch (code) {

                    case "200":
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                        break;
                    case "401":
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Config.responseVolleyErrorHandler(SendSMSActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public class SMSReceiverListAdapter extends RecyclerView.Adapter<SMSReceiverListAdapter.MyViewHolder> {

        @NonNull
        @Override
        public SMSReceiverListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_send_recycler, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SMSReceiverListAdapter.MyViewHolder holder, int position) {
            holder.tv_user_name.setText(smsReceiverEntities.get(position).name);
            holder.tv_user_type.setText(smsReceiverEntities.get(position).usertype);
            holder.tv_mobileType.setText(smsReceiverEntities.get(position).mobileType);
            holder.tv_sent_date.setText(smsReceiverEntities.get(position).sentDate);
            holder.tv_sent_time.setText(smsReceiverEntities.get(position).sentTime);
            holder.tv_mobile_number.setText(smsReceiverEntities.get(position).mobile);
        }

        @Override
        public int getItemCount() {
            return smsReceiverEntities.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_user_name,
                    tv_user_type,
                    tv_mobileType,
                    tv_sent_date,
                    tv_sent_time,
                    tv_mobile_number;

            public MyViewHolder(View itemView) {
                super(itemView);

                tv_user_name = itemView.findViewById(R.id.tv_user_name);
                tv_user_type = itemView.findViewById(R.id.tv_user_type);
                tv_mobileType = itemView.findViewById(R.id.tv_mobileType);
                tv_sent_date = itemView.findViewById(R.id.tv_sent_date);
                tv_sent_time = itemView.findViewById(R.id.tv_sent_time);
                tv_mobile_number = itemView.findViewById(R.id.tv_mobile_number);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);
        menu.findItem(R.id.menu_next).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void buildReceiverList() {


    }
}