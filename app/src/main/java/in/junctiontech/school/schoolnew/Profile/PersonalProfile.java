package in.junctiontech.school.schoolnew.Profile;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.cropImage.CropImageActivity;
import in.junctiontech.school.models.ProfileConstant;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class PersonalProfile extends AppCompatActivity {

    MainDatabase mDb;

    SignedInStaffInformationEntity staffInfo;
    StudentInformation studentInfo;

    private static final int IMG_RESULT = 1;
    private static final int IMG_EDIT = 2;
    private TextView tv_profile_name, tv_profile_roll_number,
            tv_profile_permanant_address, tv_profile_present_address, tv_profile_mobile_number, tv_profile_email, tv_profile_father_name, tv_profile_father_number,
            tv_profile_mother_name, tv_profile_mother_number, tv_profile_position;
    private FloatingActionButton profile_select_profile_image;
    private CircleImageView profile_image, father_profile_image, mother_profile_image;

    LinearLayout ll_profiledetails;

    private Bitmap bitmap;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private boolean isDialogShowing = false;
    private SharedPreferences sp;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration = 1000;
    private ImageView expandedImageView;
    private Gson gson;
    private ProfileConstant profileConstant;
    //   private StaffDetail staffDetailConstant;
    // private RegisteredStudent registeredStudentConstant;
    private boolean photoSelected = false;
    private File targetProfileImage;
    private int  colorIs;
    private TextView tv_greeting;
    private String greeting ;

    private void setColorApp() {

          colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        ((TextView)findViewById(R.id.tv_profile_father_name_title)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_profile_mother_name_title)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_profile_mobile_number_title)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_profile_present_address_title)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_profile_permanant_address_title)).setTextColor(colorIs);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        setContentView(in.junctiontech.school.R.layout.activity_personal_profile);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_personal_profile1);
        // setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDb  = MainDatabase.getDatabase(this);
        gson = new Gson();
        setColorApp();
        targetProfileImage = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/Profile" + "/",
                sp.getString("organization_name", "") + "_" + sp.getString("loggedUserID", "")
                        + "_" + sp.getString("user_type", "") + ".jpg");

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);
        initViews();
//        try {
//            photoSelected = true;
//            bitmap = BitmapFactory.decodeStream(new FileInputStream(targetProfileImage));
//            profile_image.setImageBitmap(bitmap);
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        switch (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE,this)){
            case Gc.ADMIN:
                break;
            case Gc.STAFF:
                mDb.signedInStaffInformationModel().getSignedInStaff().observe(this, new Observer<SignedInStaffInformationEntity>() {
                    @Override
                    public void onChanged(@Nullable SignedInStaffInformationEntity signedInStaffInformationEntity) {
                        if (signedInStaffInformationEntity == null){
                            return;
                        }
                        staffInfo = signedInStaffInformationEntity;

                        if(!(Gc.getSharedPreference(Gc.PROFILE_URL,PersonalProfile.this).equalsIgnoreCase("")))
                            Glide.with(PersonalProfile.this)
                                    .load(Gc.getSharedPreference(Gc.PROFILE_URL,PersonalProfile.this))
                                    .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                                    .apply(RequestOptions.errorOf(R.drawable.ic_single))
                                    .apply(RequestOptions.circleCropTransform())
                                    .thumbnail(0.5f)
                                    .into(profile_image);

                        tv_profile_name.setText(Strings.nullToEmpty(staffInfo.StaffName));
                        tv_profile_position.setText(Strings.nullToEmpty(staffInfo.StaffPositionValue));
                        tv_profile_present_address.setText(Strings.nullToEmpty(staffInfo.StaffPresentAddress));
                        tv_profile_permanant_address.setText(Strings.nullToEmpty(staffInfo.StaffPermanentAddress));
                        tv_profile_mobile_number.setText(Strings.nullToEmpty(staffInfo.StaffMobile));
                        tv_profile_email.setText(Strings.nullToEmpty(staffInfo.StaffEmail));
                        tv_profile_father_name.setText(staffInfo.StaffFName);
                        tv_profile_mother_name.setText(staffInfo.StaffMName);

                    }
                });
                break;

            case Gc.STUDENTPARENT:
                mDb.signedInStudentInformationModel().getSignedInStudentInformation().observe(this, new Observer<StudentInformation>() {
                    @Override
                    public void onChanged(@Nullable StudentInformation studentInformation) {
                        if (studentInformation != null) {
                            studentInfo = studentInformation;

                            if (!(Gc.getSharedPreference(Gc.PROFILE_URL, PersonalProfile.this).equalsIgnoreCase("")))
                                Glide.with(PersonalProfile.this)
                                        .load(Gc.getSharedPreference(Gc.PROFILE_URL, PersonalProfile.this))
                                        .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                                        .apply(RequestOptions.errorOf(R.drawable.ic_single))
                                        .apply(RequestOptions.circleCropTransform())
                                        .thumbnail(0.5f)
                                        .into(profile_image);

                            tv_profile_name.setText(studentInfo.StudentName);
                            tv_profile_position.setText(studentInfo.ClassName + " : " + studentInfo.SectionName);
                            tv_profile_present_address.setText(studentInfo.PresentAddress);
                            tv_profile_permanant_address.setText(studentInfo.PermanentAddress);
                            tv_profile_mobile_number.setText(studentInfo.Mobile);
                            tv_profile_email.setText(studentInfo.StudentEmail);
                            tv_profile_father_name.setText(studentInfo.FatherName);
                            tv_profile_mother_name.setText(studentInfo.MotherName);
                            tv_profile_father_number.setText(studentInfo.FatherMobile);
                            tv_profile_mother_number.setText(studentInfo.MotherMobile);
                        }
                    }
                });


                break;
        }

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/9995490505", this, adContainer);
        }
    }

    private boolean takePermission() {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);
            }
            return false;
        } else return true;
    }

    private void setProfileData() {
        //   Log.e("UserName()", profileConstant.getUserName());
        if (profileConstant != null) {

            Log.e("UserName()", profileConstant.getUserName());
            if (!(profileConstant.getUserName() == null || profileConstant.getUserName().equalsIgnoreCase("")))
                tv_profile_name.setText(profileConstant.getUserName());

            if (!(profileConstant.getPositionName() == null || profileConstant.getPositionName().equalsIgnoreCase("")))
                tv_profile_position.setText(profileConstant.getPositionName());


            if (!(sp.getString("user_type", "").equalsIgnoreCase("Parent") || sp.getString("user_type", "").equalsIgnoreCase("Student")))
                tv_profile_roll_number.setText(getString(R.string.id) + " : " + profileConstant.getUserId());
            else
                tv_profile_roll_number.setText(getString(R.string.roll_no) + " : " + profileConstant.getUserId());

            if (!(profileConstant.getFatherName() == null || profileConstant.getFatherName().equalsIgnoreCase("") ||
                    profileConstant.getFatherName().equalsIgnoreCase("0")))
                tv_profile_father_name.setText(profileConstant.getFatherName());
            else tv_profile_father_name.setText("---");

            if (!(profileConstant.getFatherMobile() == null || profileConstant.getFatherMobile().equalsIgnoreCase("") ||
                    profileConstant.getFatherMobile().equalsIgnoreCase("0")))
                tv_profile_father_number.setText(getString(R.string.call) + " : " + profileConstant.getFatherMobile());
            else tv_profile_father_number.setText(getString(R.string.call) + " : " + "---");

            if (!(profileConstant.getMotherName() == null || profileConstant.getMotherName().equalsIgnoreCase("") ||
                    profileConstant.getMotherName().equalsIgnoreCase("0")))
                tv_profile_mother_name.setText(profileConstant.getMotherName());
            else tv_profile_mother_name.setText("---");

            if (!(profileConstant.getMotherMobile() == null || profileConstant.getMotherMobile().equalsIgnoreCase("") ||
                    profileConstant.getMotherMobile().equalsIgnoreCase("0")))
                tv_profile_mother_number.setText(getString(R.string.call) + " : " + profileConstant.getMotherMobile());
            else tv_profile_mother_number.setText(getString(R.string.call) + " : " + "---");

            if (!(profileConstant.getMobile() == null || profileConstant.getMobile().equalsIgnoreCase("") ||
                    profileConstant.getMobile().equalsIgnoreCase("0")))
                tv_profile_mobile_number.setText(getString(R.string.mobile_number) + " : " + profileConstant.getMobile());
            else
                tv_profile_mobile_number.setText(getString(R.string.mobile_number) + " : " + "---");

            if (!(profileConstant.getEmail() == null || profileConstant.getEmail().equalsIgnoreCase("") ||
                    profileConstant.getEmail().equalsIgnoreCase("0")))
                tv_profile_email.setText(getString(R.string.email_id) + " : " + profileConstant.getEmail());
            else tv_profile_email.setText(getString(R.string.email_id) + " : " + "---");

            if (!(profileConstant.getPresentAddress() == null || profileConstant.getPresentAddress().equalsIgnoreCase("") ||
                    profileConstant.getPresentAddress().equalsIgnoreCase("0")))
                tv_profile_present_address.setText(profileConstant.getPresentAddress());
            else tv_profile_present_address.setText("---");

            if (!(profileConstant.getPermanentAddress() == null || profileConstant.getPermanentAddress().equalsIgnoreCase("") ||
                    profileConstant.getPermanentAddress().equalsIgnoreCase("0")))
                tv_profile_permanant_address.setText(profileConstant.getPermanentAddress());
            else tv_profile_permanant_address.setText("---");


            if (profileConstant.getProfileImageUrl() != null && !profileConstant.getProfileImageUrl().equalsIgnoreCase("")) {
                if (takePermission())
                    downloadProfileImage();
            }
        }


    }

    private void initViews() {
        expandedImageView =  findViewById(
                R.id.expanded_image);
        profile_image =  findViewById(R.id.profile_image);
        father_profile_image =  findViewById(R.id.father_profile_image);
        mother_profile_image =  findViewById(R.id.mother_profile_image);

        profile_select_profile_image =  findViewById(R.id.profile_select_profile_image);
        tv_greeting = findViewById(R.id.tv_greeting);
        tv_profile_name =  findViewById(R.id.tv_profile_name);
        tv_profile_position =  findViewById(R.id.tv_profile_position);
        tv_profile_roll_number =  findViewById(R.id.tv_profile_roll_number);
        tv_profile_present_address =  findViewById(R.id.tv_profile_present_address);
        tv_profile_permanant_address =  findViewById(R.id.tv_profile_permanant_address);
        tv_profile_mobile_number =  findViewById(R.id.tv_profile_mobile_number);
        tv_profile_email =  findViewById(R.id.tv_profile_email);
        tv_profile_father_name =  findViewById(R.id.tv_profile_father_name);
        tv_profile_father_number = findViewById(R.id.tv_profile_father_number);
        tv_profile_mother_name =  findViewById(R.id.tv_profile_mother_name);
        tv_profile_mother_number = findViewById(R.id.tv_profile_mother_number);
        ll_profiledetails = findViewById(R.id.ll_profiledetails);

        greeting = Strings.nullToEmpty(getIntent().getStringExtra(Gc.CHANNELID_GREETINGS_TEXT));
        if (!("".equalsIgnoreCase(greeting))){
            tv_greeting.setVisibility(View.VISIBLE);
            tv_greeting.setText(greeting);
        }else{
            tv_greeting.setVisibility(View.GONE);
        }


        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)
             || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)
        )
        {
            ll_profiledetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PersonalProfile.this, ContactDetail.class);
                    startActivity(intent);

                }
            });
        }

        //tv_profile_name.setText(sp.getString("loggedUserName","Anamika S. Sharma"));
        tv_profile_name.setTextColor(colorIs);
        profile_select_profile_image.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        profile_select_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (/*ContextCompat.checkSelfPermission(PersonalProfile.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED
                        &&
                        ContextCompat.checkSelfPermission(PersonalProfile.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED*/
                        takePermission()
                        )
                {
                    Intent intent = new Intent(PersonalProfile.this, CropImageActivity.class);
                    intent.putExtra("IsProfile", true);

                    if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, PersonalProfile.this)
                            .equalsIgnoreCase(Gc.STUDENTPARENT)){
                        String student = new Gson().toJson(studentInfo);
                        intent.putExtra(Gc.SIGNEDINUSERROLE, Gc.STUDENTPARENT);
                        intent.putExtra(Gc.STUDENTPARENT, student);
                    }else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, PersonalProfile.this)
                            .equalsIgnoreCase(Gc.STAFF)){
                        String staff = new Gson().toJson(staffInfo);
                        intent.putExtra(Gc.SIGNEDINUSERROLE, Gc.STAFF);
                        intent.putExtra(Gc.STAFF, staff);
                    }else return;

                    startActivityForResult(intent,Config.CROP_IMAGE_REQUEST_CODE);
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                    /*Intent intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                   intent = new Intent(Intent.ACTION_GET_CONTENT);
//                   intent.setType("image*//*");


                    Uri uriSavedImage = Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.app_name)));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                    startActivityForResult(intent, IMG_RESULT);
                    overridePendingTransition(R.anim.enter, R.anim.nothing);*/
                } /*else takePermission();*/
            }
        });

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bitmap != null) {
                    zoomImageFromThumb(profile_image, bitmap);
//                    final ImageView expandedImageView = (ImageView) findViewById(
//                            R.id.expanded_image);
                    //    expandedImageView.setVisibility(View.VISIBLE);
//                    expandedImageView.startAnimation(AnimationUtils.
//                            loadAnimation(PersonalProfile.this,R.anim.zoom_in));
//                    expandedImageView.setImageBitmap(bitmap);


//
//  startActivityForResult((new Intent(PersonalProfile.this,ImageViewer.class)
//                         .putExtra("bitmap", bitmap)),IMG_EDIT);
//                    overridePendingTransition(R.anim.zoom_in,R.anim.alhpa);
                    // zoomImageFromThumb(thumb1View, R.drawable.image1);
                }
//                else
//                    Toast.makeText(PersonalProfile.this, getString(R.string.profile_image_not_available) + " !", Toast.LENGTH_LONG).show();

            }
        });
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_RESULT && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {

            //try {
                Uri filePath = data.getData();


                /*********************************************/
                /*********************************************/
                // compressImage(getPath(filePath));

                /*********************************************/
                /*********************************************/


                //Getting the Bitmap from ImageViewer
             /*   final InputStream imageStream = getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(imageStream);
*/
                //    bitmap = decodeFile(new File(getPath(filePath)));


                /**************** Crop Image ********************/

                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = 1;
             //   o2.inJustDecodeBounds = true;
              try {

            bitmap = BitmapFactory.decodeStream(new FileInputStream(new File(getPath(filePath))), null, o2);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            bitmap=  scaleDownBitmap(bitmap,200);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] bytes = stream.toByteArray();

                startActivityForResult(new Intent(this, CropImageActivity.class)

                                /*.putExtra("BMP", bytes)*/
                                .putExtra("IsProfile", true)
                   /* .putExtra("bitmap",bitmap)*/,
                        Config.CROP_IMAGE_REQUEST_CODE);
                overridePendingTransition(R.anim.enter, R.anim.nothing);

                /************************************/




             /*   profile_image.setImageBitmap(bitmap);


                File sourceLocation = new File(getPath(filePath));


                if (sourceLocation.exists()) {


                    uploadProfile(bitmap, filePath);

                    Log.e("ritu", "Copy file successful.");

                } else {
                    Log.e("ritu", "Copy file failed. Source file missing.");
                }
*/

           /* } catch (FileNotFoundException e) {
                e.printStackTrace();
            }*/


        } else if (requestCode == Config.CROP_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            //byte[] bytes = data.getByteArrayExtra("BMP");
           // bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
           /* bitmap =
                    /*data.getParcelableExtra("bitmap");*/

           // compressImage(bitmap);
             //  uploadProfile(bitmap);

            setProfileImage();
            Log.e("bunty", "bunty");
        } else if (requestCode == IMG_EDIT && resultCode == RESULT_OK) {
            bitmap = data.getParcelableExtra("data");
            profile_image.setImageBitmap(bitmap);
        }
    }

    private void  setProfileImage() {
        try {
            bitmap= BitmapFactory.decodeStream(new FileInputStream(targetProfileImage));
            profile_image.setImageBitmap(bitmap);
            Log.e("ritu", "Downloaded Logo success.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Bitmap scaleDownBitmap(Bitmap photo, int newHeight) {

        final float densityMultiplier = getResources().getDisplayMetrics().density;

        int h= (int) (newHeight*densityMultiplier);
        int w= (int) (h * photo.getWidth()/((double) photo.getHeight()));

        photo=Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }


    private Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (expandedImageView.getVisibility() == View.VISIBLE) {
            expandedImageView.performClick();
            return false;
        } else {
            Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
            if (photoSelected)
                setResult(RESULT_OK, intent);
            else setResult(RESULT_CANCELED, intent);
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            //  overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (expandedImageView.getVisibility() == View.VISIBLE) {
            expandedImageView.performClick();
        } else {
            Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
            if (photoSelected)
                setResult(RESULT_OK, intent);
            else setResult(RESULT_CANCELED, intent);
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            super.onBackPressed();
        }
    }


    private void zoomImageFromThumb(final View thumbView, Bitmap imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.


        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }
        findViewById(R.id.ll_profile_page).setAlpha(0.5f);
        // Load the high-resolution "zoomed-in" image.

        expandedImageView.setImageBitmap(imageResId);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.personal_profile_container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        //  thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                        findViewById(R.id.ll_profile_page).setAlpha(1f);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        findViewById(R.id.ll_profile_page).setAlpha(1f);
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }


    public void uploadProfile(Bitmap stringImageBase) throws JSONException {
      /*  if (!Config.checkInternet(this)) {

            Toast.makeText(this, getString(R.string.internet_not_available_please_check_your_internet_connectivity), Toast.LENGTH_SHORT).show();

        } else {*/
        progressbar.show();
        String identity = "";
        final JSONObject param = new JSONObject();
        param.put("userType", sp.getString("user_type", ""));

          if (sp.getString("user_type", "").equalsIgnoreCase("Parent")) {
            param.put("AdmissionId", sp.getString("loggedUserID", ""));
            identity = "parent";
            param.put("parentProfileImage", getStringImage(stringImageBase));

        } else if (sp.getString("user_type", "").equalsIgnoreCase("Student")) {
            param.put("AdmissionId", sp.getString("loggedUserID", ""));
            identity = "student";
            param.put("studentProfileImage", getStringImage(stringImageBase));
        }else   {
              param.put("StaffId", sp.getString("loggedUserID", ""));
              identity = "staff";
              param.put("staffProfileImage", getStringImage(stringImageBase));

          }

        String profileUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found") + "ImageUploadApi.php?" + "databaseName=" +
                sp.getString("organization_name", "") + "&action=profileImageUpload&identity=" + identity;

        Log.e("ProfileUploadUrl", profileUrl);
        Log.e("param", param.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, profileUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("profileRes", jsonObject.toString());
                        progressbar.cancel();

                            JSONObject jsonObjectUpload = jsonObject;
                            if (jsonObjectUpload.optString("code").equalsIgnoreCase("200")
                                    ||
                                    jsonObjectUpload.optString("code").equalsIgnoreCase("201")) {

                                photoSelected = true;
                                sp.edit().putString(Gc.PROFILE_URL, jsonObjectUpload.optString("result")).apply();
                                //  generalSettingDataObj.setLogoUrl(jsonObjectUpload.optString("result"));


                                profile_image.setImageBitmap(bitmap);


                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                byte[] bitmapdata = bos.toByteArray();


                                //write the bytes in file


                                File dir = targetProfileImage.getParentFile();
                                try {
                                    if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                        File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME + "/Profile");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                            //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
                                            Log.e("Directory Created", "");
                                        }

                                    }
                                    BufferedOutputStream sos = new BufferedOutputStream(new FileOutputStream(targetProfileImage));
                                    sos.write(bitmapdata);
                                    sos.flush();
                                    sos.close();


                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                Snackbar snackbarObj = Snackbar.make(findViewById(R.id.personal_profile_container),

                                        getString(R.string.uploaded_successfully),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();
                            } else {
                                Toast.makeText(PersonalProfile.this, "Failed to upload Profile Image !", Toast.LENGTH_SHORT).show();
                                Snackbar snackbarObj = Snackbar.make(findViewById(R.id.personal_profile_container),

                                        getString(R.string.failed_to_upload),
                                        Snackbar.LENGTH_LONG).setAction("", v -> {
                                        });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }

                    }
                }, volleyError -> {
                    Log.e("profileResError", volleyError.toString());
                    Toast.makeText(PersonalProfile.this, "Failed to upload Profile Image !", Toast.LENGTH_SHORT).show();
                    progressbar.cancel();
                });

      /*  StringRequest request = new StringRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                profileUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());

                DbHandler.longInfo(new JSONObject(params).toString());
                Log.e("profileResPost", new JSONObject(params).toString());
                return params;
            }
        };
*/
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        // }
    }

    public void downloadProfileImage() {
//        if (!((Activity) this).isFinishing())
//            Glide
//                    .with(this)
//                    .load(profileConstant.getProfileImageUrl())
//                    .apply(new RequestOptions()
//                            .signature(new ObjectKey(String.valueOf(System.currentTimeMillis()))))
//                    .into(new SimpleTarget<byte[]>() {
//
//
//                        @Override
//                        public void onResourceReady(@NonNull  byte[] resource, @Nullable Transition<? super byte[]> transition) {
//                            new AsyncTask<Void, Void, Void>() {
//                                @Override
//                                protected Void doInBackground(Void... params) {
//                                    if (takePermission()) {
//
//                                        File dir = targetProfileImage.getParentFile();
//                                        try {
//                                            if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
//                                                File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME);
//                                                if (!f.exists()) {
//                                                    f.mkdirs();
//                                                    //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
//                                                    Log.e("Directory Created", "");
//                                                }
//
//                                            }
//                                            BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(targetProfileImage));
//                                            s.write(resource);
//                                            s.flush();
//                                            s.close();
//
//
//                                        } catch (IOException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//
//
//                                    return null;
//                                }
//
//
//                                @Override
//                                protected void onPostExecute(Void dummy) {
//
//                                    try {
//                                        profile_image.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(targetProfileImage)));
//                                        Log.e("profile", "Downloaded profile done");
//                                    } catch (FileNotFoundException e) {
//                                        e.printStackTrace();
//                                    }
//
//
//                                }
//                            }.execute();
//
//                        }
//
//                    })
//                    ;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {
        //   boolean res = grantResults[0] == PackageManager.PERMISSION_GRANTED;

        if (permsRequestCode == Config.IMAGE_LOGO_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //denied
                    Log.e("denied", permissions[0]);
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        Log.e("allowed", permissions[0]);


                    } else {
                        //set to never ask again
                        Log.e("set to never ask again", permissions[0]);
                        //do something here.
                        showRationale(getString(R.string.permission_denied), getString(
                                R.string.permission_denied_external_storage));

                    }
                }
            }

        }


    }

    private void showRationale(String permission, String permission_denied_location) {
        AlertDialog.Builder alertLocationInfo = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, Config.LOCATION);
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), (dialogInterface, i) -> {

        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }



    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
