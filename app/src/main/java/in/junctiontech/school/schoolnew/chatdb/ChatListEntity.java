package in.junctiontech.school.schoolnew.chatdb;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ChatListEntity {
    @PrimaryKey(autoGenerate = true)
    public int autoid;
    public String withType; // class , house , event , other groups , usertype
    public String withId;   // Title, class , house , event , other group id or staff , student, parent id
    public String withName; // In case of withtype = school , it will have title .name of the entity with whom the conversation is
    public String msgType;  // text , image , video , voice
    public String msg;      // text
    public String imagepath ;      // gallery or url
    public String imageOriginalPath;      // gallery or url
    public String senderType; // when message received it was from
    public String senderId; // received mesage was from id
    public String msgId ; // received message id
    public int byMe;  // all messages originated from here
    public int sent;  // message sent status only for messages sent byme = true
    public String sentDate; // message sent on
    public int received; // message received by other user status only for messages sent byme = true
    public String receivedDate; // message read on
    public int read;  // message read by other user status only for messages sent byme = true
    public String readDate; // message read on
    public int readByMe;  // message read by me is set where byme == false , that is message received by me
    public String enteredDate; // message created on
    public String withProfileUrl; // image url of user or group
    public String localPath; // Downloaded image, Video Path for Local Storage
    public String fileSize; // image, Video, file Size

}
