package in.junctiontech.school.schoolnew.DB.examdb;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class GradeMarksEntity {
    @PrimaryKey
    @NonNull
    public String gradeID;
    public String maxMarks;
    public String rangeStart;
    public String rangeEnd;
    public String grade;
    public String gradeResult;
    public String gradeStatus;
    public String gradeCreatedOn;
    public String gradeCreatedBy;
    public String gradeUpdatedOn;
    public String gradeUpdatedBy;
    public String gradeValue;
    public String resultValue;
    public String Username;

}
