package in.junctiontech.school.schoolnew.messaging.MsgAdapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.messaging.ConversationListActivity;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

/**
 * Created by JAYDEVI BHADE on 12/28/2016.
 */

public class ClassListAdapter  extends RecyclerView.Adapter<ClassListAdapter.MyViewHolder> {
    private ArrayList<ClassNameSectionName> sectionIdList = new ArrayList<>();


    private Context context;
    private int appColor;

    public ClassListAdapter(Context context, ArrayList<ClassNameSectionName> sectionIdList, int appColor) {
        this.sectionIdList = sectionIdList;
        this.context = context;
        this.appColor = appColor;

    }

    @Override
    public ClassListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
        return new ClassListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ClassListAdapter.MyViewHolder holder, int position) {
        ClassNameSectionName obj = sectionIdList.get(position);
      //  String next = "<font color='#727272'>" + "(" + studentObj.get() + ")" + "</font>";
   //     holder.tv_item_name.setText(Html.fromHtml(studentObj.getStudentName() .replace("_"," ")+ " " + next));
           holder.tv_item_name.setText(obj.ClassName + ": " + obj.SectionName);
//
//        holder.tv_designation.setText(context.getString(R.string.class_text)+
//                " : "+ studentObj.getClassName()+" "+studentObj.getSectionName());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return sectionIdList.size();
    }

    public void updateList(ArrayList<ClassNameSectionName> sectionIdList) {
        this.sectionIdList=sectionIdList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;
        //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
        // Button btn_item_create_class_add_section;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_slot_start_time);
            tv_item_name.setTextColor(appColor);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ConversationListActivity)context).
                            startChatClassWise(
                                    sectionIdList.get(getLayoutPosition())
                            );

//                    ((ConversationListActivity)context).
//                            startChat(
//                                    sectionIdList.get(getLayoutPosition()),
//                                    "Class",
//                                    false // we use false for we will not refresh chat list on back
//                                    //pressed
//                            );

                }
            });



        }
    }

    public void setFilter(ArrayList<ClassNameSectionName> classList) {
        this.sectionIdList = new ArrayList<>();
        this.sectionIdList.addAll(classList);
        notifyDataSetChanged();
    }


}


