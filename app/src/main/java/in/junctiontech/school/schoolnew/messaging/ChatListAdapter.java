package in.junctiontech.school.schoolnew.messaging;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;
import in.junctiontech.school.R;
import in.junctiontech.school.models.ChatList;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {
    private ArrayList<ChatListEntity> chatList = new ArrayList<>();
    private Context context;
    private int colorIs;
    private ArrayList<ChatList> filter;

    public ChatListAdapter(Context context, ArrayList<ChatListEntity> chatList, int colorIs) {
        this.chatList = chatList;
        this.context = context;
        this.colorIs = colorIs;
          }

    @Override
    public ChatListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_list, parent, false);
        return new ChatListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ChatListAdapter.MyViewHolder holder, int position) {
        ChatListEntity obj = chatList.get(position);
        holder.tv_chat_name.setText(obj.withType+" : "+obj.withId);
        holder.tv_chat_last_chat_msg.setText(obj.msg);



//        Log.e("AdapterCounter",obj.getUnReadChatCounter()+ "counter");
//        if (obj.getUnReadChatCounter()>0) {
//            holder.tv_chat_unread_counter.setText(obj.getUnReadChatCounter() + "");
//            holder.tv_chat_unread_counter.setVisibility(View.VISIBLE);
//        }else
            holder.tv_chat_unread_counter.setVisibility(View.GONE);


        /* --- set last  chat time ---*/
        Calendar calToday = Calendar.getInstance(); // today
        Calendar calYesterday = Calendar.getInstance();
        calYesterday.add(Calendar.DAY_OF_MONTH, -1); // yesterday

//        Calendar calChatDate = Calendar.getInstance();//MMM dd yyyy hh:mm a M
//        String []chatDate =  obj.enteredDate.toString().split(" ");
//        calChatDate.set(
//                Integer.parseInt(chatDate[2]),
//                Integer.parseInt(chatDate[5])-1,
//                Integer.parseInt(chatDate[1])
//
//        ); // your date


//        if (calChatDate.compareTo(calToday)==0) {
//            holder.tv_chat_last_chat_time.setText(chatDate[3]+" "+chatDate[4]);
//        }else  if (calChatDate.compareTo(calYesterday)==0)
//            holder.tv_chat_last_chat_time.setText(context.getString(R.string.yesterday));
//        else
//            holder.tv_chat_last_chat_time.setText(chatDate[1]+" "+chatDate[0]+" "+chatDate[2]);

    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public void setChatTime(TextView tv_chat_last_chat_time, String chatTime) {
     //   Log.e("ChatTime",chatTime);


    }

    public void setFilter(ArrayList<ChatListEntity> chatList) {
        this.chatList = chatList;


        notifyDataSetChanged();
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_chat_name,tv_chat_unread_counter, tv_chat_last_chat_time,tv_chat_last_chat_msg;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_chat_name = (TextView) itemView.findViewById(R.id.tv_chat_name);
            tv_chat_name.setTextColor(colorIs);
            tv_chat_unread_counter = (TextView) itemView.findViewById(R.id.tv_chat_unread_counter);
            tv_chat_last_chat_msg = (TextView) itemView.findViewById(R.id.tv_chat_last_chat_msg);
            tv_chat_last_chat_time = (TextView) itemView.findViewById(R.id.tv_chat_last_chat_time);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ((ConversationListActivity)context).
//                            continuePreviousChat(
//                                    chatList.get(getLayoutPosition())
//
//                            );
                }
            });

        }
    }
}
