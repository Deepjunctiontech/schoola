package in.junctiontech.school.schoolnew.Profile;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.StaffInformation;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class ContactDetail extends AppCompatActivity {
    private MainDatabase mDb;

    // EditText emailid, ccode, mobile;
    ProgressBar progressBar;
    StudentInformation student;
    StaffInformation staff;
    private boolean isProfileChanged;

    CircleImageView iv_profile_image;

    EditText et_name,
            et_father_name,
            et_mother_name,
            et_email,
            et_ccode,
            et_mobile_number,
            et_alt_ccode,
            et_alt_mobile_number,
            et_present_address,
            et_permanent_address;

    Button btn_date_of_birth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        mDb = MainDatabase.getDatabase(getApplicationContext());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progressBar);

        initializeInputFields();
        initializeDatePickers();

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {

            student = new Gson().fromJson(Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this), StudentInformation.class);

            String email = Strings.nullToEmpty(student.StudentEmail);
            String phone = Strings.nullToEmpty(student.Mobile);
            String altphone = Strings.nullToEmpty(student.AlternateMobile);

            if (!(Strings.nullToEmpty(student.studentProfileImage).equalsIgnoreCase(""))) {
                Glide.with(this)
                        .load(student.studentProfileImage)
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_single))
                        .apply(RequestOptions.errorOf(R.drawable.ic_single))
                        .apply(RequestOptions.circleCropTransform())
                        .into(iv_profile_image);
            }

            et_name.setText(student.StudentName);

            if (!(Strings.nullToEmpty(student.FatherName).equalsIgnoreCase(""))) {
                et_father_name.setText(student.FatherName);
            }

            if (!(Strings.nullToEmpty(student.MotherName).equalsIgnoreCase(""))) {
                et_mother_name.setText(student.MotherName);
            }

            if (!(Strings.nullToEmpty(student.DOB).equalsIgnoreCase(""))) {
                btn_date_of_birth.setText(student.DOB);
            }

            if (!("".equalsIgnoreCase(email))) {
                et_email.setText(email);
            }

            if (!("".equalsIgnoreCase(phone))) {
                HashMap<String, String> split = Gc.splitPhoneNumber(phone);
                et_ccode.setText(split.get("ccode"));
                et_mobile_number.setText(split.get("phone"));
            }

            if (!("".equalsIgnoreCase(altphone))) {
                HashMap<String, String> split = Gc.splitPhoneNumber(altphone);
                et_alt_ccode.setText(split.get("ccode"));
                et_alt_mobile_number.setText(split.get("phone"));
            }

            if (!(Strings.nullToEmpty(student.PresentAddress).equalsIgnoreCase(""))) {
                et_present_address.setText(student.PresentAddress);
            }

            if (!(Strings.nullToEmpty(student.PermanentAddress).equalsIgnoreCase(""))) {
                et_permanent_address.setText(student.PermanentAddress);
            }

            if (email.equalsIgnoreCase("") || phone.equalsIgnoreCase("")) {
                Config.responseSnackBarHandler(getString(R.string.kindally_fill_the_mandatory_fields),
                        findViewById(R.id.top_layout),
                        R.color.fragment_first_blue);
            }
        } else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
            staff = new Gson().fromJson(Gc.getSharedPreference(Gc.SIGNEDINSTAFF, this), StaffInformation.class);
            String email = Strings.nullToEmpty(staff.StaffEmail);
            String phone = Strings.nullToEmpty(staff.StaffMobile);
            String altphone = Strings.nullToEmpty(staff.StaffAlternateMobile);

            if (!(Strings.nullToEmpty(staff.staffProfileImage).equalsIgnoreCase(""))) {
                Glide.with(this)
                        .load(staff.staffProfileImage)
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_single))
                        .apply(RequestOptions.errorOf(R.drawable.ic_single))
                        .apply(RequestOptions.circleCropTransform())
                        .into(iv_profile_image);
            }

            et_name.setText(staff.StaffName);

            if (!(Strings.nullToEmpty(staff.StaffFName).equalsIgnoreCase(""))) {
                et_father_name.setText(staff.StaffFName);
            }

            if (!(Strings.nullToEmpty(staff.StaffMName).equalsIgnoreCase(""))) {
                et_mother_name.setText(staff.StaffMName);
            }

            if (!(Strings.nullToEmpty(staff.StaffDOB).equalsIgnoreCase(""))) {
                btn_date_of_birth.setText(staff.StaffDOB);
            }

            if (!("".equalsIgnoreCase(email))) {
                et_email.setText(email);
            }

            if (!("".equalsIgnoreCase(phone))) {
                HashMap<String, String> split = Gc.splitPhoneNumber(phone);
                et_ccode.setText(split.get("ccode"));
                et_mobile_number.setText(split.get("phone"));
            }

            if (!("".equalsIgnoreCase(altphone))) {
                HashMap<String, String> split = Gc.splitPhoneNumber(altphone);
                et_alt_ccode.setText(split.get("ccode"));
                et_alt_mobile_number.setText(split.get("phone"));
            }

            if (!(Strings.nullToEmpty(staff.StaffPresentAddress).equalsIgnoreCase(""))) {
                et_present_address.setText(staff.StaffPresentAddress);
            }

            if (!(Strings.nullToEmpty(staff.StaffPermanentAddress).equalsIgnoreCase(""))) {
                et_permanent_address.setText(staff.StaffPermanentAddress);
            }

            if (email.equalsIgnoreCase("") || phone.equalsIgnoreCase("")) {
                Config.responseSnackBarHandler(getString(R.string.kindally_fill_the_mandatory_fields),
                        findViewById(R.id.top_layout),
                        R.color.fragment_first_blue);
            }

        }

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3238510463", this, adContainer);
        }


    }

    private void initializeInputFields() {

        iv_profile_image = findViewById(R.id.iv_profile_image);

        iv_profile_image.setOnClickListener(view -> CropImage.activity()
                .setAspectRatio(1, 1)
                .setMaxCropResultSize(1920, 1080)
                .start(this));

        et_name = findViewById(R.id.et_name);
        et_father_name = findViewById(R.id.et_father_name);
        et_mother_name = findViewById(R.id.et_mother_name);
        et_email = findViewById(R.id.et_email);
        et_ccode = findViewById(R.id.et_ccode);
        et_mobile_number = findViewById(R.id.et_mobile_number);
        et_alt_ccode = findViewById(R.id.et_alt_ccode);
        et_alt_mobile_number = findViewById(R.id.et_alt_mobile_number);
        et_present_address = findViewById(R.id.et_present_address);
        et_permanent_address = findViewById(R.id.et_permanent_address);


    }

    void initializeDatePickers() {

        final Calendar myCalendar = Calendar.getInstance();
        // Date of Birth
        btn_date_of_birth = findViewById(R.id.btn_date_of_birth);

        final DatePickerDialog.OnDateSetListener dob = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
            btn_date_of_birth.setText(sdf.format(myCalendar.getTime()));
        };

        btn_date_of_birth.setOnClickListener(view -> new DatePickerDialog(ContactDetail.this, dob, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_save) {
            dismissKeyboard();

            if (validateForSave()) {
                try {

                    if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {

                        saveProfileStudent();
                    } else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {

                        saveProfileStaff();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private Boolean validateForSave() {
        String phone = et_ccode.getText().toString().trim()
                + "-" + et_mobile_number.getText().toString().trim();
        if (phone.length() > 15 || phone.length() < 5
                || "".equals(et_ccode.getText().toString().trim())
                || "".equals(et_mobile_number.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_mobile_mumber),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            et_mobile_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(et_email.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_email_id),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            et_email.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //getting the current time in milliseconds, and creating a Date object from it:
        Date date = new Date();

//converting it back to a milliseconds representation:
        long millis = date.getTime();
        SharedPreferences pref = getSharedPreferences(Gc.SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(Gc.REMINDEREMAILPHONE, millis);
        editor.apply();
    }

    public void saveProfileStudent() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = buildJsonForSave();

        JSONObject urlparam = new JSONObject()
                .put("Session", Gc.getSharedPreference(Gc.APPSESSION, this))
                .put("ProfileId", student.AdmissionId)
                .put("type", "Profile");

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "profile/studentProfile/"
                + urlparam;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        JSONArray studentJArr = job.getJSONObject("result").getJSONArray("studentDetails");


                        final StudentInformation studentInformation = new Gson()
                                .fromJson(studentJArr.getJSONObject(0).toString(),
                                        StudentInformation.class);

                        new Thread(() -> mDb.signedInStudentInformationModel().insertSignedInStudentInformation(studentInformation)).start();
                        HashMap<String, String> setDefaults = new HashMap<>();

                        setDefaults.put(Gc.SIGNEDINSTUDENT, new Gson().toJson(studentInformation));
                        setDefaults.put(Gc.PROFILE_URL, studentInformation.studentProfileImage);
                        Gc.setSharedPreference(setDefaults, getApplicationContext());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ContactDetail.this);

                    Intent intent1 = new Intent(ContactDetail.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ContactDetail.this);

                    Intent intent2 = new Intent(ContactDetail.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ContactDetail.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return param == null ? null : param.toString().getBytes(StandardCharsets.UTF_8);
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);


    }

    private JSONObject buildJsonForSave() throws JSONException {
        JSONObject param = new JSONObject();

        param.put("StudentName", et_name.getText().toString().trim());
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("FatherName", et_father_name.getText().toString().trim());
        param.put("MotherName", et_mother_name.getText().toString().trim());
        param.put("DOB", btn_date_of_birth.getText().toString().trim());
        param.put("Mobile", et_ccode.getText().toString().trim() + "-" + et_mobile_number.getText().toString().trim());
        param.put("AlternateMobile", et_alt_ccode.getText().toString().trim() + "-" + et_alt_mobile_number.getText().toString().trim());
        param.put("StudentEmail", et_email.getText().toString().trim());
        param.put("PresentAddress", et_present_address.getText().toString().trim());
        param.put("PermanentAddress", et_permanent_address.getText().toString().trim());

        if (isProfileChanged) {
            Bitmap image = ((BitmapDrawable) iv_profile_image.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("studentProfileImage", new JSONObject().put("Type", "jpg").put("StudentProfile", encodedImage));
        }

        return param;
    }

    public void saveProfileStaff() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = buildJsonForSaveStaff();

        JSONObject urlparam = new JSONObject().put("ProfileId", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "profile/staffProfile/"
                + urlparam;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:

                    try {
                        JSONArray staffJArr = job.getJSONObject("result").getJSONArray("staffDetails");

                        final SignedInStaffInformationEntity staff = new Gson()
                                .fromJson(staffJArr.getJSONObject(0).toString(), SignedInStaffInformationEntity.class);

                        String signedinstaff = new Gson().toJson(staff);
                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.SIGNEDINSTAFF, signedinstaff);
                        setDefaults.put(Gc.PROFILE_URL, staff.staffProfileImage);

                        Gc.setSharedPreference(setDefaults, getApplicationContext());

                        new Thread(() -> mDb.signedInStaffInformationModel().insertSignedInStaffInformation(staff)).start();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ContactDetail.this);

                    Intent intent1 = new Intent(ContactDetail.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ContactDetail.this);

                    Intent intent2 = new Intent(ContactDetail.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ContactDetail.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return param == null ? null : param.toString().getBytes(StandardCharsets.UTF_8);
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);


    }

    private JSONObject buildJsonForSaveStaff() throws JSONException {
        JSONObject param = new JSONObject();

        param.put("StaffName", et_name.getText().toString().trim());
        param.put("StaffFName", et_father_name.getText().toString().trim());
        param.put("StaffMName", et_mother_name.getText().toString().trim());
        param.put("StaffDOB", btn_date_of_birth.getText().toString().trim());
        param.put("StaffMobile", et_ccode.getText().toString().trim() + "-" + et_mobile_number.getText().toString().trim());
        param.put("StaffAlternateMobile", et_alt_ccode.getText().toString().trim() + "-" + et_alt_mobile_number.getText().toString().trim());
        param.put("StaffEmail", et_email.getText().toString().trim());
        param.put("StaffPresentAddress", et_present_address.getText().toString().trim());
        param.put("StaffPermanentAddress", et_permanent_address.getText().toString().trim());

        if (isProfileChanged) {
            Bitmap image = ((BitmapDrawable) iv_profile_image.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("staffProfileImage", new JSONObject().put("Type", "jpg").put("StaffProfile", encodedImage));
        }

        return param;
    }

    public void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
        if (null != this.getCurrentFocus())
            imm.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Uri resultUri = result.getUri();
            isProfileChanged = true;
            Glide.with(this)
                    .load(new File(resultUri.getPath())) // Uri of the picture
                    .into(iv_profile_image);
        }
    }
}
