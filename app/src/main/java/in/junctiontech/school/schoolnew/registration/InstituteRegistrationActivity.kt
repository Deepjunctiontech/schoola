package `in`.junctiontech.school.schoolnew.registration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.FirstScreenActivity
import `in`.junctiontech.school.schoolnew.OrganizationKeyEnter
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class InstituteRegistrationActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    private var code: String? = null
    private var countryName: String? = null
    private var stateName: String? = null
    private var cityName: String? = null
    private var idToken: String? = null
    private var userRole: String? = null
    private var mobile: String? = null

    private lateinit var spinnerRegistrationInstitutionType: Spinner
    private lateinit var checkFeature: Button
    private lateinit var registrationButtons: Button
    private lateinit var registrationInstituteName: EditText
    private var ckTermCondition: CheckBox? = null
    private var alertCondition: AlertDialog.Builder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_institute)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        code = intent.getStringExtra("code")
        countryName = intent.getStringExtra("countryName")
        stateName = intent.getStringExtra("stateName")
        cityName = intent.getStringExtra("cityName")
        idToken = intent.getStringExtra("idToken")
        userRole = intent.getStringExtra("userRole")
        mobile = intent.getStringExtra("mobile")

        initializeViews()
    }

    private fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)

        spinnerRegistrationInstitutionType = findViewById(R.id.spinner_registration_institutionTypes)
        spinnerRegistrationInstitutionType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {

            }

            override fun onNothingSelected(parentView: AdapterView<*>?) { // your code here
            }
        }

        registrationInstituteName = findViewById(R.id.registration_institute_name)
        ckTermCondition = findViewById(R.id.registration_term_condition)
        val checkBoxText = "I agree to all the <a href='https://goo.gl/c9l7ku' > Terms and Conditions</a>"

        ckTermCondition!!.text = Html.fromHtml(checkBoxText)
        ckTermCondition!!.movementMethod = LinkMovementMethod.getInstance()

        checkFeature = findViewById(R.id.check_feature)
        checkFeature.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.zeroerp.com/zeroerp-school-management-software-pricing/"))
            startActivity(browserIntent)
        }

        alertCondition = AlertDialog.Builder(this@InstituteRegistrationActivity)
        alertCondition!!.setTitle(getString(R.string.message))
        alertCondition!!.setIcon(getDrawable(android.R.drawable.ic_menu_help))
        alertCondition!!.setMessage(getString(R.string.please_accept_terms_and_conditions))
        alertCondition!!.setPositiveButton(getString(R.string.accept_and_continue)) { _, _ ->
            ckTermCondition!!.isChecked = true
            sendRegistrationData()
        }
        alertCondition!!.setNegativeButton(getString(R.string.cancel), null)
        alertCondition!!.setCancelable(false)

        registrationButtons = findViewById(R.id.registration_buttons)
        registrationButtons.setOnClickListener {
            Config.hideKeyboard(this@InstituteRegistrationActivity)
            if (ckTermCondition!!.isChecked) sendRegistrationData() else alertCondition!!.show()
        }

    }

    private fun sendRegistrationData() {
        if (checkMandatoryFields()) {

            progressBar!!.visibility = View.VISIBLE
            registerInstitute()
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun registerInstitute() {

        val param = JSONObject()


        try {

            param.put("organization_name", registrationInstituteName.text.toString())
            param.put("mobile", mobile)
            param.put("address", countryName)
            param.put("terms", "Accepted")
            param.put("institutionType", spinnerRegistrationInstitutionType.selectedItem.toString())
            param.put("planType", "trial")
            param.put("registrationDevice", "android")
            param.put("application_id", "School")
            param.put("addStatus", "YES")
            param.put("country", countryName)
            param.put("state", stateName)
            param.put("city", cityName)
            param.put("idToken", idToken)
            param.put("userRole", userRole)
            param.put("registeredPlan", "Free")


        } catch (e: JSONException) {
            e.printStackTrace()
        }


        Log.e("Param", param.toString())

        val url: String = (Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "registration/instituteRegistration")

        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            progressBar!!.visibility = View.GONE
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    val alert = android.app.AlertDialog.Builder(this@InstituteRegistrationActivity)
                    alert.setPositiveButton(getString(R.string.ok) + "  " + getString(R.string.login)) { _, _ ->
                        startActivity(Intent(this@InstituteRegistrationActivity,
                                OrganizationKeyEnter::class.java)
                                .putExtra("organizationKeyAvailable", true)
                                .putExtra("organizationKey", job.getJSONObject("result").optString("organizationKey"))

                        )
                        finish()
                        overridePendingTransition(R.anim.enter, R.anim.nothing)
                    }
                    alert.setTitle(getString(R.string.success))
                    alert.setIcon(getDrawable(R.drawable.ic_clickhere))
                    val loginDetails = "<B><font color='#E64A19'>" + getString(R.string.login_details) + "</font>" + "</B>"
                    val tv = TextView(this@InstituteRegistrationActivity)
                    tv.text = Html.fromHtml(
                            "<br>" + getString(R.string.registration_successfull_please_check_your_mail) + "<br>" +
                                    "<br>" +
                                    getString(R.string.you_can_alse_access_you_account_through_web_by_going_to_zeroerp) + " " +
                                    "<a href = 'https://zeroerp.com' >" + "www.zeroerp.com" + "</a >" +
                                    "<br>" + "<br>" +
                                    loginDetails + "<br>" +
                                    getString(R.string.organization_key) + " : "
                                    + "<B>" + "<font color='#E64A19'>" + job.getJSONObject("result").optString("organizationKey") + "</font>"
                                    + "</B>" +
                                    "<br>" +
                                    getString(R.string.login_id) + " : " + "<B>" +
                                    "<font color='#E64A19'>" + mobile + "</font>"
                                    + "</B>"
                    )
                    tv.textSize = 16f
                    tv.setPadding(40, 20, 20, 20)
                    tv.setTextColor(resources.getColor(android.R.color.black))
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    alert.setView(tv)
                    alert.setCancelable(false)
                    alert.show()
                }
                Gc.APIRESPONSE400 -> {
                    val alert1 = android.app.AlertDialog.Builder(this@InstituteRegistrationActivity)
                    alert1.setTitle(getString(R.string.failled))
                    alert1.setIcon(getDrawable(R.drawable.ic_clickhere))
                    alert1.setPositiveButton(R.string.ok) { _, _ ->

                        val intent = Intent(this@InstituteRegistrationActivity, FirstScreenActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }
                    alert1.setMessage(getString(R.string.registration_failed_message))
                    //  alert.setCancelable(false);
                    alert1.show()
                }
                else -> {

                    val alert2 = android.app.AlertDialog.Builder(this@InstituteRegistrationActivity)
                    alert2.setTitle(getString(R.string.failled))
                    alert2.setIcon(getDrawable(R.drawable.ic_clickhere))
                    alert2.setPositiveButton(R.string.ok) { _, _ -> }
                    alert2.setMessage(job.optString("message"))
                    //  alert.setCancelable(false);
                    alert2.show()
                }
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@InstituteRegistrationActivity, error, findViewById(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)

    }


    private fun checkMandatoryFields(): Boolean {


        if ("" == registrationInstituteName.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.institute_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green)
            registrationInstituteName.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        if ("" == registrationInstituteName.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.institute_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green)
            registrationInstituteName.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        return true
    }


}