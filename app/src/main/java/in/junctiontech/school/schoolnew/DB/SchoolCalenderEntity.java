package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by deep on 28/03/18.
 */
@Entity
public class SchoolCalenderEntity {
    @PrimaryKey
    @NonNull
    public String CalendarId;
    public String Title;
    public String Color;
    public String startdate;
    public String StartTime;
    public String enddate;
    public String EndTime;
    public String CalendarStatus;
    public String Date;

}
