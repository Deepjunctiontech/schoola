package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolSubjectsEntity {
    @PrimaryKey
    @NonNull
    public String SubjectId;
    public String Session;
    public String SubjectName;
    public String SubjectAbb;
    public String Class;
    public String SubjectStatus;
    public String DOE;
    public String DOL;
}
