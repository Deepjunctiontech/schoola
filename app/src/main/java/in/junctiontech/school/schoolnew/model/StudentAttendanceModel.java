package in.junctiontech.school.schoolnew.model;

public class StudentAttendanceModel {

    public String AdmissionID;
    public String StudentName;
    public String AdmissionNo;
    public String ProfileImage;
    public String SectionID;
    public String FatherName;
    public AttendanceStatus[] Attendance;
}

