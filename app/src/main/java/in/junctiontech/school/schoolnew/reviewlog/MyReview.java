package in.junctiontech.school.schoolnew.reviewlog;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.FeedBackModel;

public class MyReview extends AppCompatActivity {

    FloatingActionButton fab_add_feedback;
    private ProgressBar progressBar;

    private int colorIs = 0;

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //   fab_add_feedback.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        // getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fab_add_feedback.setBackgroundColor(colorIs);
    }


    ArrayList<FeedBackModel> feedbackList = new ArrayList<>();
    ArrayList<FeedBackModel> feedbackFilterList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    ListAdapter adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_feedback);

        mRecyclerView = findViewById(R.id.rv_students_of_feedback);
        fab_add_feedback = findViewById(R.id.fab_add_feedback);
        progressBar = findViewById(R.id.progressBar);

        fab_add_feedback.hide();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ListAdapter();
        mRecyclerView.setAdapter(adapter);


        getMyReviewFromServer();

        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3605953388",this,adContainer);
        }
    }

    public void getMyReviewFromServer()  {

        progressBar.setVisibility(View.VISIBLE);


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "master/myReview"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");


            switch (code) {
                case Gc.APIRESPONSE200:
                    try{
                        JSONObject resultjobj = job.getJSONObject("result");

                        ArrayList<FeedBackModel> feedbacks = new Gson()
                                .fromJson(resultjobj.optString("myReview"),
                                        new TypeToken<List<FeedBackModel>>() {}.getType());


                        feedbackList.addAll(feedbacks);
                        feedbackFilterList.addAll(feedbacks);

                        adapter.notifyDataSetChanged();

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.snackbar_add_feedback),R.color.fragment_first_green);
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE204:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback),R.color.fragment_first_blue);


                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, MyReview.this);

                    Intent intent1 = new Intent(MyReview.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, MyReview.this);

                    Intent intent2 = new Intent(MyReview.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(MyReview.this,error,findViewById(R.id.snackbar_add_feedback));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

        @NonNull
        @Override
        public ListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_item_recycler, viewGroup, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListAdapter.MyViewHolder myViewHolder, int i) {
            if(feedbackList.get(i).visibility.equalsIgnoreCase("1"))
                myViewHolder.tv_single_item.setText(feedbackList.get(i).feedbackLog);
        }

        @Override
        public int getItemCount() {
            return feedbackList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                tv_single_item = itemView.findViewById(R.id.tv_single_item);
//                tv_single_item.setTextColor(colorIs);

                itemView.setOnClickListener(v -> {
                    androidx.appcompat.app.AlertDialog.Builder choices = new androidx.appcompat.app.AlertDialog.Builder(MyReview.this);
                    final String feedback = new Gson().toJson(feedbackFilterList.get(getAdapterPosition()));

                    String[] aa = new String[]
                            { getResources().getString(R.string.edit),
                                    getResources().getString(R.string.delete)

                            };
                });
            }
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        // startActivity(newtext Intent(this, StudentMessageList.class));
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }
}
