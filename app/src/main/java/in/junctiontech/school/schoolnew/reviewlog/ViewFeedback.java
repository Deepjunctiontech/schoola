package in.junctiontech.school.schoolnew.reviewlog;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MessageData;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.FeedBackModel;

/**
 * Created by JAYDEVI BHADE on 4/2/2016.
 */
public class ViewFeedback extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private List<MessageData> list = new ArrayList<>();
    private ProgressBar progressBar;
    private SharedPreferences sharedPsreferences;
    private int colorIs = 0;
    private String feedbackOF;
    private boolean updateAllow = false;
    FeedBackModel updateDetails;
    SchoolStudentEntity student;
    SchoolStaffEntity staff;
    ArrayList<FeedBackModel> feedbackList = new ArrayList<>();
    ArrayList<FeedBackModel> feedbackFilterList = new ArrayList<>();
    ListAdapter adapter;
    FloatingActionButton fab_add_feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        setContentView(R.layout.activity_add_feedback);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        student = new Gson().fromJson(getIntent().getStringExtra("student"), SchoolStudentEntity.class);
        staff = new Gson().fromJson(getIntent().getStringExtra("staff"), SchoolStaffEntity.class);

        handleIntent(getIntent());

        RecyclerView mRecyclerView = findViewById(R.id.rv_students_of_feedback);
        fab_add_feedback = findViewById(R.id.fab_add_feedback);
        progressBar = findViewById(R.id.progressBar);

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            fab_add_feedback.hide();
        } else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
            updateAllow = true;
            fab_add_feedback.show();
        } else {
            fab_add_feedback.hide();
        }


        fab_add_feedback.setOnClickListener(view -> reviewLogCreate());

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ListAdapter();
        mRecyclerView.setAdapter(adapter);

        if (student != null) {
            feedbackOF = "student";
            getSupportActionBar().setTitle(R.string.student_review_log);
            getSupportActionBar().setSubtitle(student.StudentName);

            try {
                getFeedbackFromServerStudent();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (Gc.getSharedPreference(Gc.STUDENTREVIEWLOGCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
                updateAllow = true;
                fab_add_feedback.show();
            } else {
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
                    fab_add_feedback.show();
                } else {
                    fab_add_feedback.hide();
                }
            }

        } else if (staff != null) {
            feedbackOF = "staff";
            getSupportActionBar().setTitle(R.string.staff_review_log);
            getSupportActionBar().setSubtitle(staff.StaffName);
            try {
                getFeedbackFromServerStaff();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (Gc.getSharedPreference(Gc.STAFFREVIEWLOGCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
                updateAllow = true;
                fab_add_feedback.show();
            } else {
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
                    fab_add_feedback.show();
                } else {
                    fab_add_feedback.hide();
                }
            }
        }
        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6615260104",this,adContainer);
        }
    }

    private void reviewLogCreate() {
        updateDetails = null;
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dailog_create_reviewlog);
        dialog.setTitle(getString(R.string.add_review));

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);

        final EditText editText = dialog.findViewById(R.id.et_review_log);
        final RadioGroup radioGroup = dialog.findViewById(R.id.review_type);
        Button btnSave = dialog.findViewById(R.id.save);
        btnSave.setBackgroundColor(colorIs);
        btnSave.setOnClickListener(view -> {
            if ("".equals(editText.getText().toString().trim().toUpperCase())) {
                editText.setError(getResources().getString(R.string.error_field_required),
                        getResources().getDrawable(R.drawable.ic_alert));
                editText.requestFocus();
                return;
            }
            int id = radioGroup.getCheckedRadioButtonId();
            if (id != -1) {
                RadioButton radio = dialog.findViewById(id);

                int visibility = 0;
                if (radio.getText().toString().trim().equals("Public")) {
                    visibility = 1;
                }
                createReview(editText.getText().toString().trim(), visibility);
                dialog.dismiss();
            } else {
                Config.responseSnackBarHandler("Please Select Visibility",
                        dialog.findViewById(R.id.create_review_log),
                        R.color.fragment_first_blue);
            }
        });
        Button btnCancel = dialog.findViewById(R.id.cancel);
        btnCancel.setBackgroundColor(colorIs);
        btnCancel.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    private void createReview(String review, int visibility) {
        progressBar.setVisibility(View.VISIBLE);
        final String url;

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();

        String s_no = null;
        if (updateDetails != null) {
            s_no = updateDetails.s_no;
        }

        if (feedbackOF.equals("student")) {
            url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this) + Gc.ERPAPIVERSION
                    + "studentParent/studentReview";

            try {
                p.put("s_no", s_no);
                p.put("id", student.AdmissionId);
                p.put("visibility", visibility);
                p.put("feedbackLog", review);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this) + Gc.ERPAPIVERSION
                    + "staff/staffReview";

            try {
                p.put("s_no", s_no);
                p.put("id", staff.StaffId);
                p.put("visibility", visibility);
                p.put("feedbackLog", review);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        param.put(p);
       /* Log.e("Url ", url);
        Log.e("Review Body Log", param.toString());*/

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                job -> {
                    progressBar.setVisibility(View.GONE);
                    String code = job.optString("code");
                    switch (code) {
                        case Gc.APIRESPONSE200:
                            if (feedbackOF.equals("student")) {
                                try {
                                    getFeedbackFromServerStudent();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    getFeedbackFromServerStaff();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_green);
                            break;
                        case Gc.APIRESPONSE401:
                            HashMap<String, String> setDefaults1 = new HashMap<>();
                            setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults1, ViewFeedback.this);

                            Intent intent1 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);

                            finish();

                            break;
                        case Gc.APIRESPONSE500:
                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, ViewFeedback.this);

                            Intent intent2 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);

                            finish();

                            break;
                        default:
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.snackbar_add_feedback),
                                    R.color.fragment_first_blue);
                    }
                },
                error -> {
                    progressBar.setVisibility(View.VISIBLE);
                    VolleyLog.e("Review createError:", error.getMessage());
                    Config.responseVolleyErrorHandler(ViewFeedback.this, error, findViewById(R.id.snackbar_add_feedback));

                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                /*    Log.e("Header", headers.toString());*/
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);
    }

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //   fab_add_feedback.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        // getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fab_add_feedback.setBackgroundColor(colorIs);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            if (sharedPsreferences.getBoolean("toastVisibility", false))
                Toast.makeText(this, query, Toast.LENGTH_LONG).show();
            //txtQuery.setText("Search Query: " + query);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        // startActivity(newtext Intent(this, StudentMessageList.class));
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterFeedBack(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterFeedBack(s);
                return true;
            }
        });
        return true;
    }

    void filterFeedBack(String text) {
        feedbackList.clear();
        if (text.isEmpty()) {
            feedbackList.addAll(feedbackFilterList);
        } else {
            text = text.toLowerCase();
            for (FeedBackModel s : feedbackFilterList) {
                if (s.feedbackLog.toLowerCase().contains(text)
                        || s.date.toLowerCase().contains(text)
                ) {
                    feedbackList.add(s);
                }
            }

        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<MessageData> filteredModelList = filter(list, newText);

        setFilter(filteredModelList);
        return true;
    }

    public void setFilter(List<MessageData> studentListData) {
//        mayAdap.updateList(studentListData);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<MessageData> filter(List<MessageData> models, String query) {
        query = query.toLowerCase();
        final ArrayList<MessageData> filteredModelList = new ArrayList<>();

        for (MessageData model : models) {
            if (model != null) {

                if ((model.getFeedback() != null && model.getFeedback().toLowerCase().contains(query)) ||
                        (model.getCreated_at() != null && model.getCreated_at().toLowerCase().contains(query))) {
                    filteredModelList.add(model);

                }
            }
        }
        return filteredModelList;
    }

    public void getFeedbackFromServerStudent() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();

        p.put("id", student.AdmissionId);


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentReview/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            //   Log.i("Student Feedback ", job.toString());
            String code = job.optString("code");


            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        feedbackList.clear();
                        JSONObject resultjobj = job.getJSONObject("result");

                        ArrayList<FeedBackModel> feedbacks = new Gson()
                                .fromJson(resultjobj.optString("studentReview"),
                                        new TypeToken<List<FeedBackModel>>() {
                                        }.getType());

                        feedbackList.addAll(feedbacks);
                        feedbackFilterList.addAll(feedbacks);

                        adapter.notifyDataSetChanged();

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_green);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE204:

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_blue);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ViewFeedback.this);

                    Intent intent1 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ViewFeedback.this);

                    Intent intent2 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(ViewFeedback.this, error, findViewById(R.id.snackbar_add_feedback));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public void getFeedbackFromServerStaff() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();

        p.put("id", staff.StaffId);


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "staff/staffReview/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            //   Log.i("Staff Feedback ", job.toString());
            String code = job.optString("code");


            switch (code) {
                case "200":
                    try {
                        feedbackList.clear();
                        JSONObject resultjobj = job.getJSONObject("result");

                        ArrayList<FeedBackModel> feedbacks = new Gson()
                                .fromJson(resultjobj.optString("staffReview"),
                                        new TypeToken<List<FeedBackModel>>() {
                                        }.getType());


                        feedbackList.addAll(feedbacks);
                        feedbackFilterList.addAll(feedbacks);

                        adapter.notifyDataSetChanged();

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_green);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case "204":
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_blue);


                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(ViewFeedback.this, error, findViewById(R.id.snackbar_add_feedback));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

        @NonNull
        @Override
        public ListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_item_recycler, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListAdapter.MyViewHolder myViewHolder, int i) {
            if (feedbackList.get(i).visibility.equalsIgnoreCase("1")) {
                myViewHolder.tv_single_item.setTextColor(Color.BLACK);
            } else if (feedbackList.get(i).visibility.equalsIgnoreCase("0")) {
                myViewHolder.tv_single_item.setTextColor(Color.RED);
            }
            myViewHolder.tv_single_item.setText(feedbackList.get(i).feedbackLog);
        }

        @Override
        public int getItemCount() {
            return feedbackList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);
                if (updateAllow) {
                    itemView.setOnClickListener(v -> {
                        AlertDialog.Builder choices = new AlertDialog.Builder(ViewFeedback.this);

                        String[] aa = new String[]{
                                getResources().getString(R.string.edit),
                                getResources().getString(R.string.delete)
                        };

                        choices.setItems(aa, (dialogInterface, which) -> {
                            switch (which) {
                                case 0:
                                    reviewUpdate(feedbackList.get(getAdapterPosition()));
                                    break;
                                case 1:
                                    deleteUpdate(feedbackList.get(getAdapterPosition()));
                                    break;
                            }
                        });
                        choices.show();
                    });
                }
            }
        }
    }

    private void deleteUpdate(FeedBackModel feedBackModel) {
        progressBar.setVisibility(View.VISIBLE);
        final String url;
        final JSONObject p = new JSONObject();
        if (feedbackOF.equals("student")) {
            try {
                p.put("s_no", feedBackModel.s_no);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this) + Gc.ERPAPIVERSION
                    + "studentParent/studentReview/"
                    + p;
        } else {
            try {
                p.put("s_no", feedBackModel.s_no);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this) + Gc.ERPAPIVERSION
                    + "staff/staffReview/" + p;
        }

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(),
                job -> {
                    progressBar.setVisibility(View.GONE);
                    String code = job.optString("code");
                    switch (code) {
                        case Gc.APIRESPONSE200:
                            if (feedbackOF.equals("student")) {
                                try {
                                    getFeedbackFromServerStudent();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    getFeedbackFromServerStaff();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.snackbar_add_feedback), R.color.fragment_first_green);
                            break;
                        case Gc.APIRESPONSE401:
                            HashMap<String, String> setDefaults1 = new HashMap<>();
                            setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults1, ViewFeedback.this);

                            Intent intent1 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);

                            finish();

                            break;
                        case Gc.APIRESPONSE500:
                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, ViewFeedback.this);

                            Intent intent2 = new Intent(ViewFeedback.this, AdminNavigationDrawerNew.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);

                            finish();

                            break;
                        default:
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.snackbar_add_feedback),
                                    R.color.fragment_first_blue);
                    }
                },
                error -> {
                    progressBar.setVisibility(View.GONE);
                    VolleyLog.e("Review createError:", error.getMessage());
                    Config.responseVolleyErrorHandler(ViewFeedback.this, error, findViewById(R.id.snackbar_add_feedback));

                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                return null;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);
    }

    private void reviewUpdate(FeedBackModel feedBackModel) {

        updateDetails = feedBackModel;

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dailog_create_reviewlog);
        dialog.setTitle(getString(R.string.update_review));
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        Objects.requireNonNull(dialog.getWindow()).setLayout(width, height);

        final EditText editText = dialog.findViewById(R.id.et_review_log);
        final RadioGroup radioGroup = dialog.findViewById(R.id.review_type);

        editText.setText(feedBackModel.feedbackLog);

        if (feedBackModel.visibility.equals("1")) {
            final RadioButton publicRadioButton = dialog.findViewById(R.id.rd_public);
            publicRadioButton.setChecked(true);
        } else if (feedBackModel.visibility.equals("0")) {
            final RadioButton publicRadioButton = dialog.findViewById(R.id.rd_private);
            publicRadioButton.setChecked(true);
        }

        Button btnSave = dialog.findViewById(R.id.save);
        btnSave.setBackgroundColor(colorIs);
        btnSave.setOnClickListener(view -> {
            if ("".equals(editText.getText().toString().trim().toUpperCase())) {
                editText.setError(getResources().getString(R.string.error_field_required),
                        getResources().getDrawable(R.drawable.ic_alert));
                return;
            }
            int id = radioGroup.getCheckedRadioButtonId();
            if (id != -1) {
                RadioButton radio = dialog.findViewById(id);

                int visibility = 0;
                if (radio.getText().toString().trim().equals("Public")) {
                    visibility = 1;
                }
                createReview(editText.getText().toString().trim(), visibility);
                dialog.dismiss();
            } else {
                Config.responseSnackBarHandler("Please Select Visibility",
                        dialog.findViewById(R.id.create_review_log),
                        R.color.fragment_first_blue);
            }
        });
        Button btnCancel = dialog.findViewById(R.id.cancel);
        btnCancel.setBackgroundColor(colorIs);
        btnCancel.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

}

