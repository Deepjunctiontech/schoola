package in.junctiontech.school.schoolnew.gallery;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 03-10-2017.
 */

public class SlideshowDialogFragment extends DialogFragment {
    private String TAG = SlideshowDialogFragment.class.getSimpleName();
    private ArrayList<GalleryData> images;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private int selectedPosition = 0;
    private TextView lblCount, lblTitle;
    private File targetImage;
    private Communi comm;
    private ProgressDialog progressbar;
    private SharedPreferences sp;
    private ImageView iv_show_dialog_delete, iv_show_dialog_share;

    static SlideshowDialogFragment newInstance() {
        SlideshowDialogFragment f = new SlideshowDialogFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        View v = inflater.inflate(R.layout.fragment_image_slider, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        lblCount = (TextView) v.findViewById(R.id.lbl_count);
        lblTitle = (TextView) v.findViewById(R.id.tv_slider_title);
        iv_show_dialog_delete = (ImageView) v.findViewById(R.id.iv_show_dialog_delete);
        iv_show_dialog_share = (ImageView) v.findViewById(R.id.iv_show_dialog_share);
        iv_show_dialog_delete.setVisibility(sp.getBoolean("addGalleryPermission", false) ? View.VISIBLE : View.INVISIBLE);

        images = (ArrayList<GalleryData>) getArguments().getSerializable("images");
        selectedPosition = getArguments().getInt("position");

        progressbar = new ProgressDialog(getContext());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.deleting));

        Log.e(TAG, "position: " + selectedPosition);
        Log.e(TAG, "images size: " + images.size());

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        setCurrentItem(selectedPosition);
        iv_show_dialog_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(Environment.getExternalStorageDirectory().toString() +
                        "/" + Config.FOLDER_NAME + "/" + Config.SCHOOL_IMAGE_FOLDER_NAME, (images.get(selectedPosition)).getName());
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");


//                return FileProvider.getUriForFile(this, getPackageName()+".fileprovider", mediaFile);

                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                startActivity(Intent.createChooser(share, "Share Image"));


            }
        });

        iv_show_dialog_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    deleteImageFromGallery((images.get(selectedPosition)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        return v;
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    @Override
    public void onDestroy() {
        comm.refreshList();
        Log.e("onDestroy", "onDestroy");
        super.onDestroy();
    }

    //  page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            displayMetaInfo(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void displayMetaInfo(int position) {
        lblCount.setText((position + 1) + " of " + images.size());

        GalleryData image = images.get(position);
        lblTitle.setText(image.getName());
        //lblDate.setText(image.getTimestamp());
        selectedPosition = position;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        targetImage = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME +
                "/" + Config.SCHOOL_IMAGE_FOLDER_NAME, "c.png");
       /* progressbar = new ProgressDialog(getContext());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.deleting));*/
        sp = Prefs.with(getContext()).getSharedPreferences();
    }

    //  adapter
    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = layoutInflater.inflate(R.layout.image_fillscreen_preview, container, false);


            final ImageView imageViewPreview = view.findViewById(R.id.image_preview);
            final ImageView image_preview_error = view.findViewById(R.id.image_preview_error);
            final ProgressBar pbProcessing = view.findViewById(R.id.pbProcessing);
            pbProcessing.setVisibility(View.VISIBLE);
            final GalleryData image = images.get(position);

//            Glide.with(getActivity())
//                    .load(image.getOriginalUrl())
//                    .apply(new RequestOptions()
//                            .diskCacheStrategy(image.getName()
//                                    .contains("studentOfTheMonth.jpg") ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL))
//                    .thumbnail(0.5f)
//                    .into(new SimpleTarget<Drawable>() {
//                        @Override
//                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            Log.i("From Resource Ready","just");
//                        }
//
//                          }
//                    );

            Glide.with(getActivity())
                    .load(image.getOriginalUrl())
                    .listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    pbProcessing.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    pbProcessing.setVisibility(View.GONE);
                    return false;
                }
            }).into(imageViewPreview);

//                        @Override
//                              public void onResourceReady(GlideDrawable glideDrawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
//                                  if (getActivity() != null) {
//
//                                      if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                                              == PackageManager.PERMISSION_GRANTED
//                                              &&
//                                              ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                                                      == PackageManager.PERMISSION_GRANTED
//                                              ) {
//
//                                          File dir = targetImage.getParentFile();
//
//                                          if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
//                                              File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME);
//                                              if (!f.exists()) {
//                                                  f.mkdirs();
//                                                  //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
//                                                  Log.e("Directory Created", "");
//                                              }
//
//                                          }
//
//                                          GlideBitmapDrawable glideBitmapDrawable = (GlideBitmapDrawable) glideDrawable;
//                                          Bitmap bmp = glideBitmapDrawable.getBitmap();
//                                          imageViewPreview.setImageBitmap(bmp);
//                                          imageViewPreview.setDrawingCacheEnabled(true);
//                                          try {
//                                              FileOutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory().toString() +
//                                                      "/" + Config.FOLDER_NAME + "/" + Config.SCHOOL_IMAGE_FOLDER_NAME, image.getName()));
//                                              bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
//                                              out.flush();
//                                              out.close();
//
//                                          } catch (Exception e) {
//                                              e.printStackTrace();
//                                          }
//
//
//                                      } else {
//                                          GlideBitmapDrawable glideBitmapDrawable = (GlideBitmapDrawable) glideDrawable;
//                                          Bitmap bmp = glideBitmapDrawable.getBitmap();
//                                          imageViewPreview.setImageBitmap(bmp);
//                                          imageViewPreview.setDrawingCacheEnabled(true);
//
//                                      }
//                                      image_preview_error.setVisibility(View.INVISIBLE);
//                                  }
//                                  if (pbProcessing != null)
//                                      pbProcessing.setVisibility(View.INVISIBLE);
//                              }




            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public void removeItem(GalleryData galleryDataObj) {
            this.notifyDataSetChanged();
        }
    }

    private void deleteImageFromGallery(final GalleryData galleryDataObj) throws JSONException {
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Gallery.php?databaseName=" + sp.getString("organization_name", "") +
                "&orgkey=" + sp.getString(Config.SMS_ORGANIZATION_NAME, "")
                + "&action=delete";

        Log.e("GalleryUrldelete", url);

        JSONObject jsonObjectDELETE = new JSONObject();
        jsonObjectDELETE.put("imagename", galleryDataObj.getName());
        Log.e("GalleryRESdelete", jsonObjectDELETE.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, jsonObjectDELETE, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("GalleryRES", jsonObject.toString());
                progressbar.dismiss();
                if (jsonObject.optInt("code") == 200) {
                    int pos = images.indexOf(galleryDataObj);

                    images.remove(pos);
                    myViewPagerAdapter.notifyDataSetChanged();

                    Log.e("POS", pos + " ritu");
                    Log.e("images.size()", images.size() + " ritu");

                    if (images.size() == 0) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    } else displayMetaInfo(viewPager.getCurrentItem());


                    Toast.makeText(getContext(), jsonObject.optString("message", "failed !"), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getContext(), jsonObject.optString("message", "failed !"), Toast.LENGTH_SHORT).show();


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GalleryRES", volleyError.toString());
                progressbar.dismiss();
                Toast.makeText(getContext(), "failed !", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<String, String>();
                //  headers.put("X-HTTP-Method-Override", "DELETE");
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }


        };
        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getContext()).addToRequestQueue(jsonReq);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        comm = (Communi) getActivity();
    }
}
