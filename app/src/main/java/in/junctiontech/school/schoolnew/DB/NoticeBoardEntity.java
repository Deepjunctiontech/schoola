package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class NoticeBoardEntity {
    @PrimaryKey
    @NonNull
    public String Notify_id;
    public String Description;
    public String Link_url;
    public String Status;
    public String Date;
    public String Type;
    public String Sections;
    public String Flag;
    public String updatedBy;
    public String updatedOn;
    public String createdOn;
    public String createdBy;
}