package `in`.junctiontech.school.schoolnew.chat.fragment

import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.chat.ChatConversationActivity
import `in`.junctiontech.school.schoolnew.chat.adapter.RecentChatFragmentAdapter
import `in`.junctiontech.school.schoolnew.chatdb.ChatDatabase
import `in`.junctiontech.school.schoolnew.chatdb.ChatListEntity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson

class RecentChatFragment : Fragment() , RecentChatFragmentAdapter.ChatItemClickListner{
    var mDb: MainDatabase? = null
    private lateinit var chatDb: ChatDatabase

    private lateinit var contexts: Context

    private val chatList = ArrayList<ChatListEntity>()
    private val chatFilterList = ArrayList<ChatListEntity>()

    lateinit var mRecyclerView: RecyclerView
    var studentList = ArrayList<SchoolStudentEntity>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        mDb = MainDatabase.getDatabase(contexts)
        chatDb = ChatDatabase.getDatabase(contexts)

        val view: View = inflater.inflate(R.layout.fragment_recent_chat, container, false)
        mRecyclerView = view.findViewById(R.id.rv_recent_chat) as RecyclerView

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerView.layoutManager = layoutManager

        chatDb.chatListModel().recentChat.observe(viewLifecycleOwner, Observer<List<ChatListEntity>> { chatListEntities ->
            chatList.clear()
            chatFilterList.clear()
            chatList.addAll(chatListEntities)
            chatFilterList.addAll(chatList)
            val adapter = RecentChatFragmentAdapter(chatList, contexts, activity,this)
            //  adapter.setOnItemClickListener(onItemClickListener);
            mRecyclerView.adapter = adapter

            adapter.notifyDataSetChanged()
        })

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexts = context
    }

    override fun onChatItemClickCallback(position: Int) {
        val chat = Gson().toJson(chatList[position])
        val intent = Intent(activity, ChatConversationActivity::class.java)
        intent.putExtra("previous", chat)
        startActivity(intent)

    }
/*
    fun onChatItemClickCallback(position: Int) {
        val intent = Intent(activity, ChatActivity::class.java)

        intent.putExtra("isFromRecentChat", "yes")
        startActivity(intent)
    }*/


    companion object {
        var TAG = RecentChatFragment::class.java.simpleName
    }
}



