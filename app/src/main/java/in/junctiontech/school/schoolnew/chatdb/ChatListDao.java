package in.junctiontech.school.schoolnew.chatdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ChatListDao {
    @Query("select * from ChatListEntity where withType like :usertype and withId like :userid ")
    LiveData<List<ChatListEntity>> getChatWithUser(String usertype, String userid);

    @Query("select autoid,byMe,sent,received,read,withId, withType, withName,withProfileUrl,msg, (select count(readByMe) from ChatListEntity where byMe !=1 and (readByMe=0 or readByMe is null) and withId=t1.withId and withType=t1.withType ) as readByMe from ChatListEntity as t1 group by withId, withType order by autoid DESC")
    LiveData<List<ChatListEntity>> getRecentChat();

    @Query("select * from ChatListEntity where msgId like :msgid")
    List<ChatListEntity> getChatWithMsgId(String msgid);

    @Query("select * from ChatListEntity where msgId in (:msgid)")
    List<ChatListEntity> getChatWithMsgIds(List<String> msgid);

    @Query("select * from ChatListEntity where sent = 0 and byMe = 1 and withType like :withType and withId like :withId")
    LiveData<List<ChatListEntity>> getPendingChats(String withId , String withType);

    @Query("select autoid, msg, msgType,byMe, senderType, senderId,sent, msgId,sentDate,received,receivedDate,read,readDate,readByMe , withId, withType, withName,withProfileUrl, max(autoid) from ChatListEntity group by withId, withType order by readByMe")
    LiveData<List<ChatListEntity>> getRecentChats();

    @Query("update ChatListEntity set sent = 1 where msgId like :msgid")
    void updateSentStatus(String msgid);

    @Query("update ChatListEntity set readByMe = 1 where withType like :withType and withId like :withId")
    void updateReadStatus(String withId,String withType);

    @Query("update ChatListEntity set received = 1 , receivedDate= :receivingDate where msgId like :msgid")
    void updateReceivedStatus(String msgid, String receivingDate);

    @Query("delete from chatlistentity ")
    void deleteAll();

    @Insert()
    void insertChat(ChatListEntity chatListEntity);

    @Query("delete from chatlistentity where msgId in (:idList)")
    void deleteChat(List<String> idList);

    @Query("update ChatListEntity set msgType='text', msg=:msgs,imagepath=null,imageOriginalPath=null,localPath=null where msgId in (:msgid)")
    void chatDelete(String msgs,List<String> msgid);

    @Query("update ChatListEntity set localPath = :localPath where msgId like :msgid")
    void updateLocalPath(String msgid, String localPath);
}
