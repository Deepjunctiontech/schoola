package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class MasterEntryEntity {
    @PrimaryKey
    @NonNull
    public String MasterEntryId;
    public String MasterEntryStatus;
    public String MasterEntryName;
    public String MasterEntryValue;
}
