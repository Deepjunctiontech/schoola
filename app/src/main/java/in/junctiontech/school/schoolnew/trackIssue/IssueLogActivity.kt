package `in`.junctiontech.school.schoolnew.trackIssue

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.apache.commons.io.FilenameUtils.getPath
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.net.MalformedURLException
import java.text.SimpleDateFormat
import java.util.*

class IssueLogActivity : AppCompatActivity() {
    private var colorIs = 0
    var SELECT_PICTURE = 101
    private val alertAdmin: AlertDialog.Builder? = null

    var progressBar: ProgressBar? = null
    private var tvIssueTitle: TextView? = null
    private var et_bug_log_comments: EditText? = null
    private var tvIssueDescription: TextView? = null
    private var btnAttachment: Button? = null
    private var issueListsData: IssueListsData? = null
    private val issueLogList: ArrayList<IssueLogsData> = ArrayList<IssueLogsData>()
    private var adapter: IssueLogsAdapter? = null
    private var attachemtFile: String? = null
    private var attachmentType: String? = null
    private var bugLogMediaLinkPath: String? = null

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_issue)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        issueListsData = intent.getSerializableExtra("IssueDetails") as IssueListsData

        progressBar = findViewById(R.id.progressBar)
        tvIssueTitle = findViewById(R.id.tv_issue_title)
        et_bug_log_comments = findViewById(R.id.et_bug_log_comments)
        tvIssueDescription = findViewById(R.id.tv_issue_description)
        btnAttachment = findViewById(R.id.btnAttachment)


        btnAttachment!!.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE)
        }

        tvIssueTitle!!.text = issueListsData!!.title
        tvIssueDescription!!.text = issueListsData!!.description

        val mRecyclerView = findViewById<RecyclerView>(R.id.rv_issue_log_list)
        mRecyclerView.setBackgroundColor(resources.getColor(R.color.backgroundColor))

        mRecyclerView.setHasFixedSize(true)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager
        adapter = IssueLogsAdapter()
        mRecyclerView.adapter = adapter
        if (!listOf<String>(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase(Locale.ROOT)) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase(Locale.ROOT).equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/4118385648", this, adContainer)
        }

        setColorApp()
        getAuthToken()
    }

    fun submitBugLog(view: View) {
        if (!et_bug_log_comments!!.getText().toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            progressBar!!.visibility = View.VISIBLE
            val stringReq: JsonObjectRequest = object : JsonObjectRequest(Method.GET, "http://apiorgusrmgmt.zeroerp.in/LoginApi", null
                    , Response.Listener { jsonObject: JSONObject ->
                Log.e("responseReportQuery", jsonObject.toString())
                progressBar!!.visibility = View.GONE
                if (jsonObject.optString("code").equals("200", ignoreCase = true)) {
                    try {
                        val Token = jsonObject.optJSONObject("result").optString("Token")
                        val OrganizationKey = jsonObject.optJSONObject("result").optString("OrganizationKey")
                        progressBar!!.visibility = View.VISIBLE
                        val param = JSONObject()
                        param.put("developerUserID", "0")
                        param.put("clientEmail", Gc.getSharedPreference(Gc.USERID, getApplicationContext()))
                        param.put("bugCode", issueListsData!!.bugCode)
                        param.put("description", et_bug_log_comments!!.getText().toString().trim { it <= ' ' })
                        param.put("communication", "external")
                        param.put("dateTime", SimpleDateFormat("dd-MM-yyyy_HH:mm:ss", Locale.ENGLISH).format(Date()))
                        var status = issueListsData!!.status
                        if (issueListsData!!.status == "close") {
                             status = "reopen"
                        }
                        param.put("status", status)
                        param.put("responseUserID", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, getApplicationContext()))
                        param.put("OrganizationKey", "APIPERMISSION")

                        if (!attachemtFile.isNullOrEmpty()) {
                            val media = JSONObject()
                            media.put("file", attachemtFile)
                            media.put("Type", "jpeg")
                            param.put("media", media)
                        }

                        val Url = Gc.ISSUEREPORTURL + "/BuglogApi"

                        val string_req: JsonObjectRequest = object : JsonObjectRequest(Method.POST, Url, param
                                , Response.Listener { jsonObject: JSONObject ->
                            progressBar!!.visibility = View.GONE
                            if (jsonObject.optString("code").equals("200", ignoreCase = true) ||
                                    jsonObject.optString("code").equals("201", ignoreCase = true)) {
                                getAuthToken()
                                updateBug(status, Token, OrganizationKey)
                                et_bug_log_comments!!.setText("")
                                attachemtFile = ""
                                attachmentType = ""
                                Config.responseSnackBarHandler("Thank You For Feedback",
                                        findViewById(R.id.ll_bug_log_activity), R.color.fragment_first_blue)
                            } else {
                                Config.responseSnackBarHandler(jsonObject.optString("message"),
                                        findViewById(R.id.ll_bug_log_activity), R.color.fragment_first_blue)
                            }
                        }, Response.ErrorListener { volleyError: VolleyError ->
                            progressBar!!.visibility = View.GONE
                            Config.responseSnackBarHandler("Technical Problem Please Try After Some Time",
                                    findViewById(R.id.ll_bug_log_activity), R.color.fragment_first_blue)
                        }) {
                            override fun getHeaders(): Map<String, String> {
                                val header: HashMap<String, String> = LinkedHashMap()
                                header["Auth_Token"] = Token
                                header["Content-Type"] = "application/json"
                                header["OrganizationKey"] = OrganizationKey
                                return header
                            }
                        }
                        string_req.retryPolicy = DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }, Response.ErrorListener { volleyError: VolleyError ->
                progressBar!!.visibility = View.GONE
            }) {
                override fun getHeaders(): Map<String, String> {
                    val header: HashMap<String, String> = LinkedHashMap()
                    header["Action"] = "Authentication"
                    header["Content-Type"] = "application/json"
                    try {
                        header["filter"] = JSONObject().put("Mobile", "9876543211").put("Password", "initial").toString()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    header["OrganizationKey"] = "APIPERMISSION"
                    header["App_key"] = "SUlQRjBvTEgvRGNjSzNSOUF2cjJWUT09"
                    return header
                }
            }
            stringReq.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
            AppRequestQueueController.getInstance(this).addToRequestQueue(stringReq)
        } else {
            Config.responseSnackBarHandler("Enter some comments !",
                    findViewById(R.id.ll_bug_log_activity), R.color.fragment_first_blue)
        }
    }

    fun updateBug(status: String, Token: String, OrganizationKey: String) {

        val param = JSONObject()
        val data = JSONObject()
        data.put("status", status)

        val filter = JSONObject()
        filter.put("bugCode", issueListsData!!.bugCode)
        param.put("data", data)
        param.put("filter", filter)
        Log.e("bupda", param.toString())
        val Url = Gc.ISSUEREPORTURL + "/BugApi"

        val string_req: JsonObjectRequest = object : JsonObjectRequest(Method.PUT, Url, param
                , Response.Listener { jsonObject: JSONObject ->
            Log.e("test", jsonObject.toString())


        }, Response.ErrorListener { volleyError: VolleyError ->
            progressBar!!.visibility = View.GONE
            Config.responseSnackBarHandler("Technical Problem Please Try After Some Time",
                    findViewById(R.id.ll_bug_log_activity), R.color.fragment_first_blue)
        }) {
            override fun getHeaders(): Map<String, String> {
                val header: HashMap<String, String> = LinkedHashMap()
                header["Auth_Token"] = Token
                header["Content-Type"] = "application/json"
                header["OrganizationKey"] = OrganizationKey
                return header
            }
        }
        string_req.retryPolicy = DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppRequestQueueController.getInstance(this).addToRequestQueue(string_req)

    }

    fun getStringImage(bmp: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

    fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(uri, projection, null, null, null)
        startManagingCursor(cursor)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 101 && resultCode == Activity.RESULT_OK && data != null) {
            val uri = data.data
            val size: Long = File(getPath(uri.toString())).length() / 1024
            if (size > 1024 * 5) {
                // Config.responseSnackBarHandler("image size not more than 5 mb !", snackbar, appColor)
            } else {
                try {
                    val inputStream = contentResolver.openInputStream(uri!!)
                    val b = BitmapFactory.decodeStream(inputStream)
                    try {
                        // val filePath: String = getPath(uri.toString())
                        val filePath = getPath(uri)
                        attachemtFile = getStringImage(b)
                        //attachmentType = filePath!!.substring(filePath.lastIndexOf("/"))
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            }
        }

    }

    private fun getAuthToken() {
        progressBar!!.visibility = View.VISIBLE
        val stringReq: JsonObjectRequest = object : JsonObjectRequest(Method.GET, "http://apiorgusrmgmt.zeroerp.in/LoginApi", null
                , Response.Listener { jsonObject: JSONObject ->
            progressBar!!.visibility = View.GONE
            if (jsonObject.optString("code").equals("200", ignoreCase = true)) {
                try {
                    val Token = jsonObject.optJSONObject("result").optString("Token")
                    val OrganizationKey = jsonObject.optJSONObject("result").optString("OrganizationKey")
                    getIssueLogList(Token, OrganizationKey)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }, Response.ErrorListener { volleyError: VolleyError ->
            progressBar!!.visibility = View.GONE
        }) {
            override fun getHeaders(): Map<String, String> {
                val header: HashMap<String, String> = LinkedHashMap()
                header["Action"] = "Authentication"
                header["Content-Type"] = "application/json"
                try {
                    header["filter"] = JSONObject().put("Mobile", "9876543211").put("Password", "initial").toString()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                header["OrganizationKey"] = "APIPERMISSION"
                header["App_key"] = "SUlQRjBvTEgvRGNjSzNSOUF2cjJWUT09"
                return header
            }
        }
        stringReq.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(this).addToRequestQueue(stringReq)
    }

    private fun getIssueLogList(Token: String, OrganizationKey: String) {
        progressBar!!.visibility = View.VISIBLE
        val stringReq: JsonObjectRequest = object : JsonObjectRequest(Method.GET, Gc.ISSUEREPORTURL + "/BugDetails", null
                , Response.Listener { jsonObject: JSONObject ->
            progressBar!!.visibility = View.GONE
            if (jsonObject.optString("code").equals("200", ignoreCase = true)) {
                try {
                    bugLogMediaLinkPath = jsonObject.getJSONObject("result").optString("bugLogMediaLinkPath")
                    val obj = Gson()
                            .fromJson<ArrayList<IssueLogsData>>(jsonObject.getJSONObject("result").optString("bugLogDetails"),
                                    object : TypeToken<List<IssueLogsData>>() {}.type)
                    issueLogList.clear()
                    issueLogList.addAll(obj)
                    adapter!!.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }, Response.ErrorListener { volleyError: VolleyError ->
            progressBar!!.visibility = View.GONE
        }) {
            override fun getHeaders(): Map<String, String> {
                val header: HashMap<String, String> = LinkedHashMap()
                header["OrganizationKey"] = OrganizationKey
                header["Auth_Token"] = Token
                header["Content-Type"] = "application/json"
                try {
                    header["filter"] = JSONObject().put("bugCode", issueListsData!!.bugCode)
                            .put("communication", "external").toString()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                return header
            }
        }
        stringReq.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(this).addToRequestQueue(stringReq)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class IssueLogsAdapter : RecyclerView.Adapter<IssueLogsAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_issue_log, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val bugListData = issueLogList[position]

            val sdf = SimpleDateFormat("dd-MM-yyyy_HH:mm:ss", Locale.US)
            val date = sdf.parse(bugListData.dateTime)
            val millis = date.time
            val calendarOld = Calendar.getInstance()
            calendarOld.timeInMillis = millis
            val formatter = SimpleDateFormat("MMM dd , yyyy hh:mm:ss aaa", Locale.US)
            val enteredDate = formatter.format(calendarOld.time)
            holder.tvDateTime.text = enteredDate
            holder.tvIssueLogDescription.text = bugListData.description
            if (bugListData.clientEmail.isNullOrEmpty()) {
                holder.llZeroERP.visibility = View.VISIBLE
            } else {
                holder.llZeroERP.visibility = View.GONE
            }
        }

        override fun getItemCount(): Int {
            return issueLogList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tvDateTime: TextView = itemView.findViewById(R.id.tv_date_time)
            val tvIssueLogDescription: TextView = itemView.findViewById(R.id.tv_issue_log_description)
            val llZeroERP: LinearLayout = itemView.findViewById(R.id.ll_zeroep)

            init {
                itemView.setOnClickListener {
                    try {
                        if (!issueLogList[layoutPosition].mediaPath.isNullOrEmpty()) {
                            val choices = AlertDialog.Builder(this@IssueLogActivity)
                            val menu = ArrayList<String>()
                            menu.add("View Attachment")
                            val objNames = menu.toTypedArray()
                            choices.setItems(objNames) { _, which ->
                                if (objNames[which] == "View Attachment") {
                                    val intent = Intent(this@IssueLogActivity, FullScreenImageActivity::class.java)
                                    val stom = ArrayList<String>()
                                    stom.add(bugLogMediaLinkPath + "/" + issueLogList[layoutPosition].mediaPath)
                                    intent.putStringArrayListExtra("images", stom)
                                    intent.putExtra("Track Issue", getString(R.string.gallery))
                                    startActivity(intent)
                                }
                            }
                            choices.show()
                        }
                    } catch (e: MalformedURLException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}