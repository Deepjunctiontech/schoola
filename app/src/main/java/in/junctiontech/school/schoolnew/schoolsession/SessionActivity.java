package in.junctiontech.school.schoolnew.schoolsession;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.GeneralSettingData;
import in.junctiontech.school.models.SchoolSession;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SessionListEntity;
import in.junctiontech.school.schoolnew.DB.SignedInUserInformationEntity;
import in.junctiontech.school.schoolnew.DB.viewmodel.SessionViewModel;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.AcademicSessions;
import in.junctiontech.school.schoolnew.setup.SetupActivity;

/**
 * Created by JAYDEVI BHADE on 19/1/2017.
 * Updated and ReDesigned Version 75 .
 */

public class SessionActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private MainDatabase mDb;

    SignedInUserInformationEntity signedInUser;
    ArrayList<SessionListEntity> sessionListTemplate = new ArrayList<>();
    private SessionViewModel mSessionViewModel;


    private ProgressDialog progressbar;
    private AlertDialog.Builder alert, alertSubmission;
    private boolean isDialogShowing = false;
    private String dbName;
    private Gson gson;

    private int warning = 0;// not displayed msg
    private Calendar myCalendar;
    private Button btn_submit_session;
    private SimpleDateFormat sdf;
    private String myFormat = "dd-MM-yyyy"; //In which you need put here
    private String dateOfSession = "";
    private RadioButton rb_session_year, rb_session_half_yearly, rb_session_quarterly;
    private TextView tv_session_start_date_title;

    private int grneralSettingId;
    String sessionType = "annual";
    private FloatingActionButton fab_add_session;
    private GeneralSettingData generalSettingData;
    private TextView tv_session_start_date;
    private LinearLayout ll_session_list;
    private boolean addedSession = false;
    private boolean logoutAlert;
    private FrameLayout snackbar;
    private RelativeLayout page_demo;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private String firstTimeSetup;

    private int colorIs;
    private String  userType;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        //   getWindow().setNavigationBarColor(colorIs);

        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView) findViewById(R.id.tv_session_start_date_title)).setTextColor(colorIs);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//


        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        mDb = MainDatabase.getDatabase(getApplicationContext());

        if(Gc.getSharedPreference(Gc.SESSIONEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        firstTimeSetup= getIntent().getStringExtra(Gc.SETUP);

//        userType   = sp.getString("user_type", "");
        //LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        dbName = Gc.getSharedPreference(Gc.ERPDBNAME,this);
        setContentView(R.layout.activity_session_fullscreen);
        setColorApp();
        snackbar =  findViewById(R.id.snackbar_session_page);

        /* ********************************    Demo Screen *******************************/
//        page_demo = findViewById(R.id.rl_school_session_demo);
//        if (sp.getBoolean("DemoSchoolSession", true)) {
//            page_demo.setVisibility(View.VISIBLE);
//        } else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoSchoolSession", false).commit();
//            }
//        });
        /* ***************************************************************/


        myCalendar = Calendar.getInstance();
        sdf = new SimpleDateFormat(myFormat, Locale.US);
//        viewPager = (ViewPager) findViewById(R.id.container_pager);
////        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
//        setupViewPager(viewPager);

        gson = new Gson();

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        grneralSettingId = getIntent().getIntExtra("Id", 0);

        initViews();


//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//            schoolSessionObj = gson.fromJson(sp.getString(Config.SESSION_DATA, ""),
//                    new TypeToken<SchoolSession>() {
//                    }.getType());
//            if (schoolSessionObj!=null && schoolSessionObj.getType()!=null)
//            sessionType = schoolSessionObj.getType();
//
//
//            generalSettingData = (GeneralSettingData) getIntent().getSerializableExtra("GeneralSettingObj");
//
//            updateSessionStartDate();
//            switch (sessionType) {
//                case "annual":
//                    rb_session_year.setChecked(true);
//                    break;
//
//                case "semester":
//                    rb_session_half_yearly.setChecked(true);
//                    break;
//
//                case "quarterly":
//                    rb_session_quarterly.setChecked(true);
//                    break;
//            }
//            rb_session_year.setEnabled(false);
//            rb_session_half_yearly.setEnabled(false);
//            rb_session_quarterly.setEnabled(false);
//            tv_session_start_date.setEnabled(false);
//            (findViewById(R.id.view_line)).setVisibility(View.GONE);
//            btn_submit_session.setVisibility(View.GONE);
//
//            setSessionList(schoolSessionObj);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (getIntent().getBooleanExtra("AddSessionActive", false)) {
                        if (tv_session_start_date.getText().toString().equalsIgnoreCase(getString(R.string.select_date)))
                            updateSessionStartDate();
                    }
                }
            }
        };

        mSessionViewModel = ViewModelProviders.of(this).get(SessionViewModel.class);

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4852836853", this, adContainer);
        }

    }

    private void updateSessionStartDate() {

        if (generalSettingData != null && generalSettingData.getSessionStartDate() != null) {

            progressbar.show();
            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Config.applicationVersionNameValue
                    + "Conversion.php?type=stringToDate&date=" +
                    generalSettingData.getSessionStartDate();
            Log.d("url", url);
            StringRequest request = new StringRequest(Request.Method.GET,
                    url
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("dateResult", s);
                            progressbar.dismiss();
                            try {
                                tv_session_start_date.setText(new JSONObject(s).optString("result"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateResult", volleyError.toString());
                    progressbar.dismiss();

                    Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        }

    }

    private void initViews() {

        btn_submit_session =  findViewById(R.id.btn_submit_session);
        btn_submit_session.setTextColor(colorIs);
        fab_add_session =  findViewById(R.id.fab_add_session);
        fab_add_session.setBackgroundTintList(ColorStateList.valueOf(colorIs));


//        if (getIntent().getBooleanExtra("AddSessionActive", false) &&      (userType.equalsIgnoreCase("Administrator") || userType.equalsIgnoreCase("admin"))  ) {
//            fab_add_session.setVisibility(View.VISIBLE);
//        } else fab_add_session.setVisibility(View.GONE);

        rb_session_year =  findViewById(R.id.rb_session_year);
        rb_session_year.setTextColor(colorIs);

        tv_session_start_date_title = findViewById(R.id.tv_session_start_date_title);

        if(!(Gc.getSharedPreference(Gc.SESSIONEXISTS,this).equals(Gc.EXISTS))){
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(50); //You can manage the time of the blink with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(20);
            tv_session_start_date_title.startAnimation(anim);
        }

        tv_session_start_date =  findViewById(R.id.tv_session_start_date);
        tv_session_start_date.setTextColor(colorIs);
        ll_session_list =  findViewById(R.id.ll_session_list);

        alertSubmission = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertSubmission.setTitle(getString(R.string.confirmation));
        alertSubmission.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_save));
        alertSubmission.setMessage(getString(R.string.check_session_start_and_end_dates) + " !\n" +
                getString(R.string.academic_session_dates_cannot_be_changed_later) + "," +
                getString(R.string.if_you_agree_then_click_next));
        alertSubmission.setPositiveButton(R.string.yes_i_understand, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                warning = 1;
            }
        });

        if(Gc.getSharedPreference(Gc.CURRENTSESSIONSTART, this).equalsIgnoreCase(Gc.NOTFOUND)){

//            fab_add_session.setVisibility(View.GONE);
//            btn_submit_session.setVisibility(View.VISIBLE);

            tv_session_start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Get current date

                    // Create the DatePickerDialog instance
                    DatePickerDialog datePicker = new DatePickerDialog(SessionActivity.this,
                            myDateListenersession_start_date, myCalendar.get(Calendar.YEAR),
                            myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));

                    datePicker.setCancelable(false);
                    datePicker.setTitle(getString(R.string.select_date));
                    datePicker.show();

                }
            });


            rb_session_year.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        warning = 0;
                        sessionType = "annual";
                        fetchSessionValuesVia();
                    } else ll_session_list.removeAllViews();
                }
            });
            rb_session_half_yearly =  findViewById(R.id.rb_session_half_yearly);
            rb_session_half_yearly.setTextColor(colorIs);
            rb_session_half_yearly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        sessionType = "semester";
                        warning = 0;
                        fetchSessionValuesVia();
                    } else ll_session_list.removeAllViews();
                }
            });
            rb_session_quarterly =  findViewById(R.id.rb_session_quarterly);
            rb_session_quarterly.setTextColor(colorIs);
            rb_session_quarterly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        sessionType = "quarterly";
                        warning = 0;

                        fetchSessionValuesVia();
                    } else ll_session_list.removeAllViews();
                }
            });

        }else{

            Date initDate = null;
            try {
                initDate = new SimpleDateFormat("yyyy-mm-dd").
                        parse(Gc.getSharedPreference(Gc.CURRENTSESSIONSTART,this));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String parsedDate = formatter.format(initDate);
            System.out.println(parsedDate);

//            fab_add_session.setVisibility(View.VISIBLE);
            btn_submit_session.setVisibility(View.GONE);
            tv_session_start_date.setText(Gc.getSharedPreference(Gc.CURRENTSESSIONSTART,this));
//            sessionListTemplate = (ArrayList<SessionListEntity>) mDb.sessionListModel().getSessionList();
            Log.i("Current Session Date is", tv_session_start_date.getText().toString() );
//            setSessionList(sessionListTemplate);
        }
        alertSubmission.setCancelable(false);




        findViewById(R.id.btn_submit_session).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (warning == 1) {
                    try {
                        sendSessionsToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    getDateToString();
                } else {
                    if (!dateOfSession.equalsIgnoreCase("")) {
                        alertSubmission.show();
                    } else {
                        if (!isDialogShowing && !isFinishing()) {
                            alert.setMessage(getString(R.string.please_select_session_start_date));
                            alert.show();
                            isDialogShowing = true;
                        }
                    }

                }
            }
        });


        fab_add_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                addNewSchoolSession();
            }
        });
    }

    private void addNewSchoolSession() {

        progressbar.show();


//            Log.e("AddSessionUrl", sp.getString("HostName", "Not Found") +
//                    "SchoolSessionApi.php/createSessionTemplate?databaseName="
//                    + dbName);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionName
                + "SchoolSessionApi.php/createSessionTemplate?databaseName="
                + dbName;

        StringRequest request = new StringRequest(Request.Method.GET,
                url

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("AddSessionRes", s);
                        progressbar.cancel();
                        if (s != "") {

                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {
                                    /*--  session List Saved in SharedPrefrence ---*/
//                                        SharedPreferences.Editor editor = sp.edit();
//                                        editor.putString(Config.SESSION_DATA, s);
//                                        editor.commit();
//                                    JSONObject sessionTemplateJobj = jsonObject.getJSONObject("sessionList");
//                                    AcademicSessions academicSessions = new Gson().fromJson(sessionTemplateJobj.toString(),AcademicSessions.class);
//                                    setSessionList(academicSessions);

                                } else if (jsonObject.optInt("code") == 300) {
                                    alert.setMessage(jsonObject.optString("message"));
                                    alert.show();
                                    isDialogShowing = true;
                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(SessionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                else
                                    Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("AddSessionRes", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        //}
    }

    public DatePickerDialog.OnDateSetListener myDateListenersession_start_date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            tv_session_start_date.setText(sdf.format(myCalendar.getTime()));
            dateOfSession = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
            fetchSessionValuesVia();

        }
    };



    public void setSessionList(ArrayList<SessionListEntity> sessionListTemplate) {

        //Log.e("sessionParts", sessionType + " parts");
      //  Log.e("SessionObj", sessionListEntity.getSessionList().size() + " ritu");

        ll_session_list.removeAllViews();

        for(int i = 0; i < sessionListTemplate.size() ; i++) {

        View view_Session = getLayoutInflater().inflate(R.layout.item_session_selection_view, null);
        ((TextView) view_Session.findViewById(R.id.tv_item_session_name)).setText(
                getString(R.string.session) + " : " + sessionListTemplate.get(i).session
        );
        ((TextView) view_Session.findViewById(R.id.tv_item_session_end_date)).setText(
                sessionListTemplate.get(i).sessionEndDate
        );
        ((TextView) view_Session.findViewById(R.id.tv_item_session_start_date)).setText(
                sessionListTemplate.get(i).sessionStartDate
        );

        ll_session_list.addView(view_Session);
    }


    }

    public void fetchSessionValuesVia() {
        if (!dateOfSession.equalsIgnoreCase("")) {

          /*  if (!Config.checkInternet(this)) {

                if (!isDialogShowing && !isFinishing()) {
                    alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                    alert.show();
                    isDialogShowing = true;
                }

            } else {*/
            progressbar.show();

            final Map<String, Object> param = new LinkedHashMap<>();
            param.put("date", dateOfSession);
            param.put("type", sessionType);

//                Log.e("SessionUrl", sp.getString("HostName", "Not Found") +
//                        "SchoolSessionApi.php/createSessionTemplate?date=" + dateOfSession +
//                        "&type=" + sessionType + "&databaseName="
//                        + dbName);

            String url = Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL,SessionActivity.this)
                    + Gc.ERPAPIVERSION
                    + "academicSession/getAcademicSessionsTemplate/" + (new JSONObject(param));
            Log.d("url", url);

            StringRequest request = new StringRequest(Request.Method.GET,

                    url
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("SessionRes", s);
                            progressbar.cancel();
                            if (s != "") {

                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.optInt("code") == 200) {
                                    /*--  session List Saved in SharedPrefrence ---*/
//                                        SharedPreferences.Editor editor = sp.edit();
//                                        editor.putString(Config.SESSION_DATA, s);
//                                        editor.commit();


                                        sessionListTemplate.clear();
                                        JSONObject resultJobj = jsonObject.optJSONObject("result");
                                        JSONArray sessionTemplateJarr = resultJobj.getJSONArray("sessionList");

                                        for(int i = 0 ; i < sessionTemplateJarr.length(); i++) {
                                            SessionListEntity sessionList = new Gson().fromJson(sessionTemplateJarr.getString(i), SessionListEntity.class);
                                            sessionListTemplate.add(sessionList);
                                        }
                                        setSessionList(sessionListTemplate);


                                    } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                            ||
                                            jsonObject.optString("code").equalsIgnoreCase("511")) {
                                        if (!logoutAlert & !isFinishing()) {
                                            Config.responseVolleyHandlerAlert(SessionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                            logoutAlert = true;
                                        }
                                    } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                        Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                    else
                                        Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("SessionRes", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY", Gc.APPKEY);
                    headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

            // }
        } else {
            ll_session_list.removeAllViews();

        }

    }

    public void sendSessionsToServer() throws JSONException {
        progressbar.show();
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,SessionActivity.this)
                + Gc.ERPAPIVERSION
                +"academicSession/academicSessions";

        final JSONObject param = new JSONObject();
        param.put("username", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME,this));
        param.put("type" ,sessionType);
        param.put("sessionList", new Gson().toJson(sessionListTemplate));

        Log.i("Session URL Parameters",param.toString());


        JSONArray allDataArray = new JSONArray();

        if(!(sessionListTemplate.size() == 0)){
            for(int index = 0; index < sessionListTemplate.size(); index++) {
                String jsonText =  new Gson().toJson(sessionListTemplate.get(index));

                JSONObject object = new JSONObject(jsonText);
                allDataArray.put(object);
            }
        }
        param.put("sessionList", allDataArray);

        Log.i("Session URL Parameters",param.toString());



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.cancel();
                        try {
                            String returnedCode = response.getString("code");
                            JSONObject result = response.getJSONObject("result");
                            JSONObject sessionlist = result.getJSONObject("academicSessions");
                            if(returnedCode.equals(Gc.APIRESPONSE200)) {
                                AcademicSessions academicSessions = new Gson().fromJson(sessionlist.toString(),AcademicSessions.class);
                                HashMap<String,String> setDefaults = new HashMap<String, String>();


                                for(int i = 0; i < academicSessions.sessionList.size() ; i++) {
                                    setDefaults.put(Gc.SESSIONEXISTS, Gc.EXISTS);

                                    SessionListEntity sessionListEntity = academicSessions.sessionList.get(i);
                                    if (academicSessions.currentSession.equals(sessionListEntity.session)) {
                                        setDefaults.put(Gc.CURRENTSESSIONSTART, Gc.convertDate(sessionListEntity.sessionStartDate));
                                        setDefaults.put(Gc.CURRENTSESSIONEND, Gc.convertDate(sessionListEntity.sessionEndDate));
                                        setDefaults.put(Gc.CURRENTSESSION, sessionListEntity.session);
                                        setDefaults.put(Gc.APPSESSIONSTART, Gc.convertDate(sessionListEntity.sessionStartDate));
                                        setDefaults.put(Gc.APPSESSIONEND, Gc.convertDate(sessionListEntity.sessionEndDate));
                                        setDefaults.put(Gc.APPSESSION, sessionListEntity.session);
                                    }
                                    Gc.setSharedPreference(setDefaults,getApplicationContext());
                                }
                                final List<SessionListEntity> sessionListEntities = academicSessions.sessionList;
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.sessionListModel().insertSessionList(sessionListEntities);

                                    }
                                }).start();



                                    Intent intent = new Intent(SessionActivity.this, SetupActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                                    finish();

                            }else if (returnedCode.equals(Gc.APIRESPONSE401)){
                                Gc.logCrash(SessionActivity.this, url);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.cancel();
                Log.e("Session error",error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);

            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.fragment_first_blue));
        }
    }

    @Override
    public void onBackPressed() {
        if (Gc.getSharedPreference(Gc.SESSIONEXISTS,this).equals(Gc.EXISTS)) {
            mSessionViewModel.getSessionList().removeObservers(this);
            super.onBackPressed();
        }else{

            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.top_layout), R.string.save_session,
                    Snackbar.LENGTH_LONG).setAction("",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
//        if (addedSession) {
//            Intent intent = new Intent(SessionActivity.this, AdminNavigationDrawerNew.class);
//            setResult(RESULT_OK, intent);
//
//        } else if (getIntent().getBooleanExtra("nextTimeSession", false)) {
//            Intent intent = new Intent(SessionActivity.this, AdminNavigationDrawerNew.class);
//            if (addedSession)
//                setResult(RESULT_OK, intent);
//            else setResult(RESULT_CANCELED, intent);
//            finish();
//            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
//        }
//        finish();
//        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
//        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));

        mSessionViewModel.getSessionList().observe(this, new Observer<List<SessionListEntity>>() {
            @Override
            public void onChanged(@Nullable List<SessionListEntity> sessionListEntities) {
                sessionListTemplate = (ArrayList<SessionListEntity>) sessionListEntities;
                setSessionList(sessionListTemplate);

            }
        });

        if (Gc.getSharedPreference(Gc.CURRENTSESSION,this).equals(Gc.NOTFOUND)) {

            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.snackbar_session_page), R.string.select_session_start_date,
                    Snackbar.LENGTH_LONG).setAction("",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        }

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        mSessionViewModel.getSessionList().removeObservers(this);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);

        if(Gc.getSharedPreference(Gc.SESSIONEXISTS,this).equals(Gc.EXISTS)){
//            menu.findItem(R.id.menu_next).setVisible(false);
            menu.findItem(R.id.action_save).setVisible(false);
            menu.findItem(R.id.menu_next).setVisible(false);
        }else {
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;

            case R.id.action_save:

                if (warning == 1) {
                    try {
                        sendSessionsToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    getDateToString();
                } else {
                    if (!dateOfSession.equalsIgnoreCase("")) {
                        alertSubmission.show();
                    } else {
                        if (!isDialogShowing && !isFinishing()) {
                            alert.setMessage(getString(R.string.please_select_session_start_date));
                            alert.show();
                            isDialogShowing = true;
                        }
                    }

                }

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // All the below methods are unused and are not being called
//    These will be deleted after thorough testing
// -------------------TODO Delete all these methods
    public void getDateToString() {

      /*  if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                dateOfSession;
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
                        progressbar.dismiss();
                        try {
                            sendGeneralSettingDataUpdate(s);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        //  }


    }

    private void sendGeneralSettingDataUpdate(String date) throws JSONException {
        progressbar.show();
        final JSONObject job_filter = new JSONObject();
        final GeneralSettingData generalSettingDataObj = (GeneralSettingData) getIntent().getSerializableExtra("generalSettingDataObj");
        generalSettingDataObj.setSessionStartDate(date);

        final SchoolSession schoolSessionObj = gson.fromJson(sp.getString(Config.SESSION_DATA, ""),
                new TypeToken<SchoolSession>() {
                }.getType());

        generalSettingDataObj.setId(getIntent().getIntExtra("Id", 0) + "");
        generalSettingDataObj.setLogoUrl(null);
        generalSettingDataObj.setCurrentSession("");

        JSONObject jsonObjectUpdate = new JSONObject();
        jsonObjectUpdate.put("sessionStartDate",date);

//            generalSettingDataObj.setCurrentSession(schoolSessionObj.getCurrentSession().replaceAll(" ", "_"));
        try {
            job_filter.put("Id", getIntent().getIntExtra("Id", 0));
            Log.e("IntentId", getIntent().getIntExtra("Id", 0) + " ritu");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JSONObject param_main = new JSONObject();

            /*generalSettingDataObj.setAffiliationNo(generalSettingDataObj.getAffiliationNo().replaceAll(" ", "+"));
            generalSettingDataObj.setAffiliatedBy(generalSettingDataObj.getAffiliatedBy().replaceAll(" ", "+"));
            generalSettingDataObj.setBoard(generalSettingDataObj.getBoard().replaceAll(" ", "+"));
            generalSettingDataObj.setCity(generalSettingDataObj.getCity().replaceAll(" ", "+"));
            generalSettingDataObj.setCountry(generalSettingDataObj.getCountry().replaceAll(" ", "+"));
            generalSettingDataObj.setCurrentSession(generalSettingDataObj.getCurrentSession().replaceAll(" ", "+"));
            generalSettingDataObj.setRegistrationNo(generalSettingDataObj.getRegistrationNo().replaceAll(" ", "+"));
            generalSettingDataObj.setSchoolAddress(generalSettingDataObj.getSchoolAddress().replaceAll(" ", "+"));

            generalSettingDataObj.setSchoolMoto(generalSettingDataObj.getSchoolMoto().replaceAll(" ", "+"));
            generalSettingDataObj.setSchoolName(generalSettingDataObj.getSchoolName().replaceAll(" ", "+"));
            generalSettingDataObj.setState(generalSettingDataObj.getState().replaceAll(" ", "+"));
*/
        param_main.put("data", jsonObjectUpdate/*new JSONObject(gson.toJson(generalSettingDataObj))*/);

        param_main.put("filter", job_filter);

//            Log.e("BasicDetailResult1", new JSONObject(param_main).toString());
//            DbHandler.longInfo(sp.getString("HostName", "Not Found") + "GeneralSettingApi.php?" + "databaseName=" +
//                    sp.getString("organization_name", "") + "&data=" + (new JSONObject(param_main)));

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "GeneralSettingApi.php?" + "databaseName=" +
                sp.getString("organization_name", "") + "&data=" + param_main;
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.PUT,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("BasicDetailResult1", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {

                                sendAcademicSessionsToServer(schoolSessionObj);

                                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog
                                        .Builder(SessionActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

                                alert1.setCancelable(false);

                                alert1.setMessage(jsonObject.optString("message"));
                                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(SessionActivity.this, AdminNavigationDrawerNew.class);
                                        if (generalSettingDataObj == null)
                                            setResult(RESULT_CANCELED, intent);
                                        else if (generalSettingDataObj.getId().equalsIgnoreCase(""))
                                            setResult(RESULT_CANCELED, intent);
                                        else
                                            setResult(RESULT_OK, intent);

                                        finish();
                                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                    }
                                });
                                if (!isFinishing())
                                    alert1.show();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(SessionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else {
                                alert.setMessage(jsonObject.optString("message"));
                                if (!isFinishing())
                                    alert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("BasicDetailResult", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //  params.put("data", new JSONObject(param).toString());
                //    Log.e("BasicDetailResult", new JSONObject(param).toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    private void sendAcademicSessionsToServer(SchoolSession sessionObj) throws JSONException {

        final JSONObject param = new JSONObject();

        param.put("username",Gc.getSharedPreference(Gc.SIGNEDINUSERNAME,this));
        param.put("type",sessionType);

        if (!sp.getString(Config.SESSION_DATA, "").equalsIgnoreCase("")) {

            JSONArray jsonArraySessionList = new JSONArray();

            for (SchoolSession objSession : sessionObj.getSessionList()) {
                    jsonArraySessionList.put((new JSONObject().
                            put("sessionStartDate", objSession.getSessionStartDate())
                            .put("sessionEndDate", objSession.getSessionEndDate())
                            .put("session", objSession.getSession())));
            }
            param.put("sessionList", jsonArraySessionList);
        }

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "academicSession/academicSessions" ;

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");




                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }).start();
                            //         Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Config.responseVolleyErrorHandler(SessionActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void createSessionTableToServer(SchoolSession sessionObj) throws JSONException {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

        /* -- session list fetch via annual , half yearly , quarterly */

        final JSONObject paramfilter = new JSONObject();

        paramfilter.put("username", sp.getString("loggedUserName", ""));
        paramfilter.put("type", sessionType);

        if (!sp.getString(Config.SESSION_DATA, "").equalsIgnoreCase("")) {

            JSONArray jsonArraySessionList = new JSONArray();

            for (SchoolSession objSession : sessionObj.getSessionList()) {
                try {
                    jsonArraySessionList.put((new JSONObject().
                            put("sessionStartDate", objSession.getSessionStartDate())
                            .put("sessionEndDate", objSession.getSessionEndDate())
                            .put("session", objSession.getSession())
                    ));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            paramfilter.put("sessionListEntity", jsonArraySessionList
            );

        }


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionName
                + "SchoolSessionApi.php?databaseName=" + Gc.getSharedPreference(Gc.ERPDBNAME,this);
        Log.d("url", url);
        Log.d("paramfilter", paramfilter.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                url,paramfilter
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("SessionDataResTEST", jsonObject.toString());

                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {
                            addedSession = true;
                            Toast.makeText(SessionActivity.this, getString(R.string.session_Added_successfully), Toast.LENGTH_SHORT).show();

                        }  else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                ||
                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                            if (!logoutAlert & ! isFinishing()) {
                                Config.responseVolleyHandlerAlert(SessionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                logoutAlert = true;
                            }
                        }else if (jsonObject.optString("code").equalsIgnoreCase("502")) { addedSession = false;
                            Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                        } else{   Config.responseSnackBarHandler(getString(R.string.failed_to_add_session_try_again),snackbar);

                            addedSession = false;
                        }



                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("SessionDataCreateTable", volleyError.toString());
                addedSession = false;
                Config.responseVolleyErrorHandler(SessionActivity.this, volleyError, snackbar);

                Toast.makeText(SessionActivity.this, getString(R.string.failed_to_add_session_try_again), Toast.LENGTH_SHORT).show();
            }
        })  ;


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }
    }


}
