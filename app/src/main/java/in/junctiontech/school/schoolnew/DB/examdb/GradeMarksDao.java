package in.junctiontech.school.schoolnew.DB.examdb;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GradeMarksDao {

    @Query("select * from GradeMarksEntity where maxMarks LIKE :maximumMarks order by rangeStart desc")
    LiveData<List<GradeMarksEntity>> getGradeMarks(String maximumMarks);

    @Query("select * from GradeMarksEntity order by rangeStart desc")
    LiveData<List<GradeMarksEntity>> getGradeMarksAll();

    @Insert(onConflict = REPLACE)
    void insertGradeMarks(List<GradeMarksEntity> gradeMarksEntityList);

    @Delete
    void deleteGradeMarkbyId(GradeMarksEntity gradeMarksEntity);

    @Query("delete from GradeMarksEntity")
    void deleteAll();

}
