package in.junctiontech.school.schoolnew.registrationprocess;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.country.CountryCode;
import in.junctiontech.school.schoolnew.FirstScreenActivity;
import in.junctiontech.school.schoolnew.OrganizationKeyEnter;
import in.junctiontech.school.schoolnew.common.Gc;

//import in.junctiontech.school.SignUpActivity;

/**
 * A login screen that offers login via email/password.
 */
public class RegistrationSchoolActivity extends AppCompatActivity {
    // UI references.
    private EditText admin_registration_name,
            admin_registration_organization_key, admin_registration_institute_name,
            admin_registration_phone_number, admin_registration_email, admin_registration_password;

    private RadioGroup rg_plan_type;

    private View mProgressView;
    private View mLoginFormView;
    private SharedPreferences sp;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private boolean emailAlreadyRegister = false;
    private boolean checkUserRole = false;
    private String mandatory_fields = "";
    private Spinner spinner_registration_institutionType, spinner_registration_user_role;
    private RadioButton rb_registration_free, rb_registration_starter, rb_registration_pro;
    private boolean checkingOrganizationKey = false;
    private Button registration_button, check_features;
    private ArrayList<String> countryCodeList = new ArrayList<>();
    private AutoCompleteTextView admin_registration_country_code;
    private TextView tv_registration_pricing_details;
    private CheckBox ck_term_condition;
    private androidx.appcompat.app.AlertDialog.Builder alertCondition;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(RegistrationSchoolActivity.this).getSharedPreferences();
        //  LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_school_registration);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Set up the login form.
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        initViews();
        alertCondition = new androidx.appcompat.app.AlertDialog.Builder(RegistrationSchoolActivity.this);
        alertCondition.setTitle(getString(R.string.message));
        alertCondition.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_help));
        alertCondition.setMessage(getString(R.string.please_accept_terms_and_conditions));
        alertCondition.setPositiveButton(getString(R.string.accept_and_continue), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ck_term_condition.setChecked(true);
                sendRegistrationData();
            }
        });
        alertCondition.setNegativeButton(getString(R.string.cancel), null);
        alertCondition.setCancelable(false);


        registration_button = findViewById(R.id.registration_button);
        registration_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Config.hideKeyboard(RegistrationSchoolActivity.this);
                if (ck_term_condition.isChecked())
                    sendRegistrationData();
                else alertCondition.show();
            }
        });

        check_features = findViewById(R.id.check_features);
        check_features.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.zeroerp.com/zeroerp-school-management-software-pricing/"));
            startActivity(browserIntent);
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        admin_registration_country_code = findViewById(R.id.admin_registration_country_code);

        String countryCodeString = getSharedPreferences(Config.COUNTRY_CODE, Context.MODE_PRIVATE).getString(Config.COUNTRY_CODE, "");
        if (!countryCodeString.equalsIgnoreCase("")) {
            CountryCode obj = (new Gson()).fromJson(countryCodeString, new TypeToken<CountryCode>() {
            }.getType());
            ArrayList<CountryCode> countryCodeObjList = obj.getResult();

            for (CountryCode countryObj : countryCodeObjList)
                countryCodeList.add("+" + countryObj.getCode());
            ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, countryCodeList);
            //adapterSpinner.set
            adapterCountry.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            admin_registration_country_code.setAdapter(adapterCountry);

        } else Config.getCountryCode(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

// Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initViews() {
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alert.setCancelable(false);
        ck_term_condition = findViewById(R.id.admin_registration_term_condition);
        String checkBoxText = "I agree to all the <a href='https://goo.gl/c9l7ku' > Terms and Conditions</a>";

        ck_term_condition.setText(Html.fromHtml(checkBoxText));
        ck_term_condition.setMovementMethod(LinkMovementMethod.getInstance());

        admin_registration_email = findViewById(R.id.admin_registration_email);
        admin_registration_password = findViewById(R.id.admin_registration_password);
        admin_registration_name = findViewById(R.id.admin_registration_name);
        admin_registration_name.setText(sp.getString("AdminProfileName", ""));
        admin_registration_phone_number = findViewById(R.id.admin_registration_phone_number);
        admin_registration_email.setText(getIntent().getStringExtra("email"));
        admin_registration_email.setEnabled(false);
        admin_registration_organization_key = findViewById(R.id.admin_registration_organization_key);
        admin_registration_institute_name = findViewById(R.id.admin_registration_institute_name);

        rg_plan_type = findViewById(R.id.rg_plan_type);

        spinner_registration_institutionType = findViewById(R.id.spinner_registration_institutionType);

        spinner_registration_user_role = findViewById(R.id.spinner_registration_user_role);

        spinner_registration_user_role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String userRole = spinner_registration_user_role.getSelectedItem().toString();
                if ("Parent / Student".equals(userRole)) {
                    checkUserRole = true;
                    alert.setMessage("Registration can Only be done by Institute Owner or staff. Contact Institute to get Login details or Email us at info@zeroerp.com");
                    alert.show();
                }
                else{
                    checkUserRole = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        admin_registration_organization_key.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkOrganizationKey(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    void checkOrganizationKey(String s) {

        JSONObject p = new JSONObject();
        try {
            p.put("organizationKey", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "registration/registration/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":

                        admin_registration_organization_key.setError(
                                getString(R.string.organization_key_already_registered));
                        emailAlreadyRegister = true;
                        break;

                    default:
                        emailAlreadyRegister = false;
                        admin_registration_organization_key.setError(null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ErrorResponse", error.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);


                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    private boolean sendRegistrationData() {
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        if (!checkingOrganizationKey) {
            if (emailAlreadyRegister) {
                alert.setMessage(R.string.organization_key_already_registered);
                alert.show();
            }
            else if(checkUserRole){
                alert.setMessage("Registration can Only be done by Institue Owner or staff. Contact Institue to get Login details or Email us at info@zeroerp.com");
                alert.show();
            }
            else {
                if (checkMandatoryFields()) {

                    progressbar.show();

                    registerInstitute();

                }
//                else {
//                    alert.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + " : " +
//                            mandatory_fields);
//                    alert.show();
//                }
            }

        } else {
            progressbar.show();
        }

        return true;
    }

    private boolean checkMandatoryFields() {

        int id = rg_plan_type.getCheckedRadioButtonId();
        if (id == -1) {
            Config.responseSnackBarHandler("Plan Type " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            return false;
        }

        if ("".equals(admin_registration_organization_key.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.organization_key) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_organization_key.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(admin_registration_email.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.email_id) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_email.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(admin_registration_password.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.password) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_password.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(admin_registration_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(admin_registration_institute_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.institute_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_institute_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(admin_registration_institute_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.institute_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_institute_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        String mobile = admin_registration_country_code.getText().toString().trim()
                + "-" + admin_registration_phone_number.getText().toString().trim();
        if (mobile.length() > 15 || mobile.length() < 5
                || "".equals(admin_registration_country_code.getText().toString().trim())
                || "".equals(admin_registration_phone_number.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_Phone_mumber),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
            admin_registration_phone_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        return true;
    }

    private void registerInstitute() {
        final JSONObject param = new JSONObject();

        JSONObject param1 = new JSONObject();

        JSONObject param2 = new JSONObject();

        JSONObject param3 = new JSONObject();
        try {
            String planType = "Paid";
            int id = rg_plan_type.getCheckedRadioButtonId();
            if (id != -1) {
                RadioButton radio = (RadioButton) findViewById(id);
                if (radio.getText().toString().trim().equals("Free")) {
                    planType = "Free";
                }
            }
            param1.put("name", admin_registration_name.getText().toString().replaceAll(" ", "_"));
            param1.put("mobile", Gc.makePhoneNumber(admin_registration_country_code.getText().toString().trim(), admin_registration_phone_number.getText().toString()));
            param1.put("email", admin_registration_email.getText().toString());
            param1.put("organization_name", admin_registration_institute_name.getText().toString().replaceAll(" ", "_"));
            param1.put("password", admin_registration_password.getText().toString());
            param1.put("Username", admin_registration_email.getText().toString().replaceAll(" ", "_"));
            param1.put("organizationCreated_by", admin_registration_email.getText().toString());
            param1.put("organizationCreated_on", new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.ENGLISH).format(new Date()));
            param1.put("organizationStatus", "active");
            param1.put("terms", "Accepted");

            param.put("OrganizationData", param1);
            param2.put("name", admin_registration_name.getText().toString().replaceAll(" ", "_"));
            param2.put("mobile", Gc.makePhoneNumber(admin_registration_country_code.getText().toString().trim(), admin_registration_phone_number.getText().toString()));
            param2.put("email", admin_registration_email.getText().toString());
            param2.put("organizationKey", admin_registration_organization_key.getText().toString().replaceAll(" ", "_"));
            param2.put("Username", admin_registration_email.getText().toString().replaceAll(" ", "_"));
            param2.put("application_id", "School");
            param2.put("addStatus", "YES");

            param2.put("applicationCreated_by", admin_registration_email.getText().toString());
            param2.put("applicationCreated_on", new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.ENGLISH).format(new Date()));
            param2.put("applicationStatus", "active");
            param2.put("planType", "trial");


            param2.put("institutionType", spinner_registration_institutionType.getSelectedItem().toString());
            param2.put("registrationDevice", "android");

            param.put("ApplicationData", param2);

            param3.put("userRole",spinner_registration_user_role.getSelectedItem().toString());
            param3.put("registeredPlan",planType);

            param.put("AdditionalInformation",param3);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "registration/organizationRegistration";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressbar.cancel();
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:

                        AlertDialog.Builder alert = new AlertDialog.Builder(RegistrationSchoolActivity.this,
                                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                        alert.setPositiveButton(getString(R.string.ok) + "  " + getString(R.string.login), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                startActivity(new Intent(RegistrationSchoolActivity.this,
                                        OrganizationKeyEnter.class)
                                        .putExtra("dataAvailable", true)
                                        .putExtra("organizationKey", admin_registration_organization_key.getText().toString())
                                        .putExtra("username", admin_registration_email.getText().toString())
                                        .putExtra("password", admin_registration_password.getText().toString())
                                );
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                            }
                        });
                        alert.setTitle(getString(R.string.success));
                        alert.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));

                        String loginDetails = "<B><font color='#E64A19'>" + getString(R.string.login_details) + "</font>" + "</B>";

                        TextView tv = new TextView(RegistrationSchoolActivity.this);
                        tv.setText(Html.fromHtml(
                                "<br>" + getString(R.string.registration_successfull_please_check_your_mail) + "<br>" +
                                        "<br>" +
                                        getString(R.string.you_can_alse_access_you_account_through_web_by_going_to_zeroerp) + " " +
                                        "<a href = 'https://zeroerp.com' >" + "www.zeroerp.com" + "</a >" +

                                        "<br>" + "<br>" +
                                        loginDetails + "<br>" +
                                        getString(R.string.organization_key) + " : "
                                        + "<B>" + "<font color='#E64A19'>" + admin_registration_organization_key.getText().toString() + "</font>"
                                        + "</B>" +
                                        "<br>" +
                                        getString(R.string.login_id) + " : " + "<B>" +
                                        "<font color='#E64A19'>" + admin_registration_email.getText().toString() + "</font>"
                                        + "</B>" +
                                        "<br>" +
                                        getString(R.string.password) + " : " + "<B>" +
                                        "<font color='#E64A19'>" + admin_registration_password.getText().toString() + "</font>" + "</B>"

                        ));
                        tv.setTextSize(16);
                        tv.setPadding(40, 20, 20, 20);
                        tv.setTextColor(getResources().getColor(android.R.color.black));
                        tv.setMovementMethod(LinkMovementMethod.getInstance());

                        alert.setView(tv);

                        alert.setCancelable(false);
                        alert.show();

                        break;

                    case Gc.APIRESPONSE400:

                        AlertDialog.Builder alert1 = new AlertDialog.Builder(RegistrationSchoolActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                        alert1.setTitle(getString(R.string.failled));
                        alert1.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));
                        alert1.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearGoogleAccount();

                                Intent intent = new Intent(RegistrationSchoolActivity.this, FirstScreenActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });
                        alert1.setMessage(getString(R.string.registration_failed_message));
                        //  alert.setCancelable(false);
                        alert1.show();

                        break;

                    default:
                        AlertDialog.Builder alert2 = new AlertDialog.Builder(RegistrationSchoolActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                        alert2.setTitle(getString(R.string.failled));
                        alert2.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));
                        alert2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert2.setMessage(job.optString("message"));
                        //  alert.setCancelable(false);
                        alert2.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.cancel();
                Config.responseVolleyErrorHandler(RegistrationSchoolActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.cancel);
// Add the buttons
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                clearGoogleAccount();

                Intent intent = new Intent(RegistrationSchoolActivity.this, FirstScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.enter, R.anim.nothing);

                RegistrationSchoolActivity.super.onBackPressed();

                dialog.dismiss();
                // User clicked OK button
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                // User cancelled the dialog
            }
        });
// Set other dialog properties

// Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }


    private void clearGoogleAccount() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.cancel);
// Add the buttons
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                clearGoogleAccount();

                Intent intent = new Intent(RegistrationSchoolActivity.this, FirstScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                overridePendingTransition(R.anim.enter, R.anim.nothing);
                dialog.dismiss();

                // User clicked OK button
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                // User cancelled the dialog
            }
        });
// Set other dialog properties

// Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
        return false;
    }


}

