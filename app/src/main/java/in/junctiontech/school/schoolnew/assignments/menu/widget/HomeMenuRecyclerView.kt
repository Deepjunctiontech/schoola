package `in`.junctiontech.school.schoolnew.assignments.menu.widget

import `in`.junctiontech.school.schoolnew.assignments.menu.HomeWorkGridMenuAdapter
import `in`.junctiontech.school.schoolnew.assignments.menu.widget.decoration.HomeWorkMenuGridDecoration
import `in`.junctiontech.school.schoolnew.chat.adapter.GridMenuAdapter
import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HomeMenuRecyclerView : RecyclerView {
    private val spanCount = 3
    private val manager = GridLayoutManager(context, spanCount)
    private val adapter = HomeWorkGridMenuAdapter()
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context,attrs,defStyle)
    init {
        setHasFixedSize(true)
        layoutManager = manager
        setAdapter(adapter)
        addItemDecoration(HomeWorkMenuGridDecoration())
    }

    fun addMenuClickListener(listener: HomeWorkGridMenuAdapter.GridMenuListener) {
        adapter.listener = listener
    }
}