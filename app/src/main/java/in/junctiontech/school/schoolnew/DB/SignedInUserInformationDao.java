package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import static androidx.room.OnConflictStrategy.REPLACE;
@Dao
public interface SignedInUserInformationDao {
    @Query("select * from SignedInUserInformationEntity limit 1")
    LiveData<SignedInUserInformationEntity> getSignedInUserInformation();

    @Insert(onConflict = REPLACE)
    void insertSignedInUserInformation(SignedInUserInformationEntity signedInUserInformationEntity);
}
