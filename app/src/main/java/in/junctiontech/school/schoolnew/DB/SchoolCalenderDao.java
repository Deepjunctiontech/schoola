package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolCalenderDao {
    @Query("select * from SchoolCalenderEntity order by startdate  asc ")
    LiveData<List<SchoolCalenderEntity>> getAllCalenderEvents();

    @Query("select * from SchoolCalenderEntity where startdate like :start")
    LiveData<List<SchoolCalenderEntity>> getDayCalenderEvents(String start);

    @Insert(onConflict = REPLACE)
    void insertSchoolCalender(List<SchoolCalenderEntity> schoolCalenderEntities);

    @Query("delete from SchoolCalenderEntity where CalendarId like :calendarid")
    void deleteSingleCalendar(String calendarid);

    @Delete
    void deleteSchoolCalendar(SchoolCalenderEntity schoolCalenderEntity);
}
