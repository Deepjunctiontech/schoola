package in.junctiontech.school.schoolnew.adminpanel;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AdminLanguageSetting;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.MyServices.MyLocationService;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.menuActions.LanguageSetup;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.ExamTypeEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.DB.OrganizationPermissionEntity;
import in.junctiontech.school.schoolnew.DB.SchoolAccountsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.DB.SessionListEntity;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.DB.TransportFeeAmountEntity;
import in.junctiontech.school.schoolnew.DB.UserPermissionsEntity;
import in.junctiontech.school.schoolnew.DB.viewmodel.SessionViewModel;
import in.junctiontech.school.schoolnew.FirstScreenActivity;
import in.junctiontech.school.schoolnew.Profile.ContactDetail;
import in.junctiontech.school.schoolnew.Profile.PersonalProfile;
import in.junctiontech.school.schoolnew.attendance.StudentMonthAttendanceActivity;
import in.junctiontech.school.schoolnew.chat.ChatActivity;
import in.junctiontech.school.schoolnew.chatdb.ChatDatabase;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.elibrary.ElibraryList;
import in.junctiontech.school.schoolnew.licence.LicenceActivity;
import in.junctiontech.school.schoolnew.licence.SchoolsDetailsActivity;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;
import in.junctiontech.school.schoolnew.model.SchoolClasses;
import in.junctiontech.school.schoolnew.model.StudentInformation;
import in.junctiontech.school.schoolnew.myschedule.MyScheduleActivity;
import in.junctiontech.school.schoolnew.newgallery.GalleryMainActivity;
import in.junctiontech.school.schoolnew.notificationbar.NewNotificationBarActivity;
import in.junctiontech.school.schoolnew.onlineexam.PendingExamList;
import in.junctiontech.school.schoolnew.reports.DailyReportActivity;
import in.junctiontech.school.schoolnew.reviewlog.MyReview;
import in.junctiontech.school.schoolnew.schoolcalender.SchoolCalenderActivity;
import in.junctiontech.school.schoolnew.schoolsession.SessionActivity;
import in.junctiontech.school.schoolnew.subject.SubjectActivity;
import in.junctiontech.school.schoolnew.users.UserListActivity;
import in.junctiontech.school.staffattendance.StaffAttendanceReportActivity;

//import com.crashlytics.android.Crashlytics;

public class AdminNavigationDrawerNew extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MainDatabase mDb;
    ChatDatabase chatDb;

    SignedInStaffInformationEntity signedinStaff;
    SchoolStaffEntity staff;
    StudentInformation signedInStudent;
    SchoolStudentEntity student;

    ArrayList<String> userPermissionsList = new ArrayList<>();
    ArrayList<String> orgPermissionList = new ArrayList<>();

    TextView tv_admin_name;

    private int LANGUAGE_CODE = 101;

    // Session handling declaration
    private ArrayList<String> sessionList = new ArrayList<>();
    ArrayList<SessionListEntity> sessionEntities = new ArrayList<>();
    private androidx.appcompat.widget.AppCompatSpinner nav_header_session_list;
    private SessionViewModel mSessionViewModel;

    private SharedPreferences nsp, sharedPreferences_token;
    private AlertDialog.Builder alertSession;
    AlertDialog.Builder demoAlert;

    private ArrayList<NoticeBoardEntity> notificationList = new ArrayList<>();

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private boolean isDialogShowing = false;
    private TextView tv_admin_notification_details, tv_nav_header_school_name, tv_total_student, tv_total_staff;
    public static int GENERAL_SETTING_CODE = 333;


    private ImageView iv_school_logo;
    private String schoolEmail = Gc.ZEROERPEMAIL;
    private boolean isOnPause = false;
    private RelativeLayout snackbar_admin_main_page;

    private CircleImageView civ_other_user_profile_image;
    private String token;
    private boolean isPermissionDialogOpen = false;
    private LinearLayout ll_school_image;
    private TextView tv_admin_notification_board;
    private BottomNavigationView bottomNavigationView;
    private LinearLayout ll_admin_page_header_background, ll_invitation, ll_logo, ll_daily_reports, ll_ok;
    private int colorIs;

//    private String userTypeText = "";
    ArrayList<UserActionModel> userActionList = new ArrayList<>();
    private UserActionAdapter userActionAdapter;

    private FirebaseRemoteConfig firebaseRemoteConfig;

    private Boolean exit = false;
    Boolean setDefaultSession = false;
    Boolean checkLicence = false;

    UserActionModel myAttendanceViewObjModel;
    UserActionModel myScheduleViewObjModel;
    UserActionModel leaveViewObjModel;
    UserActionModel classObjModel;
    UserActionModel manageFeeObjModel;
    UserActionModel financeObjModel;
    UserActionModel createStudentObjModel;
    UserActionModel createSubjectObjModel;
    UserActionModel createStaffObjModel;
    UserActionModel userListObjModel;
    UserActionModel homeworkObjModel;
    UserActionModel examObjModel;
    UserActionModel messageObjModel;
    UserActionModel transportObjModel;
    UserActionModel galleryObjModel;
    UserActionModel timeTableObjModel;
    UserActionModel teachersNotesObjModel;
    UserActionModel myReviewObjModel;
    UserActionModel reportViewObjModel;
    UserActionModel libraryObjModel;
    UserActionModel eLibraryObjModel;
    UserActionModel onlineTestObjModel;
    UserActionModel helpObjModel;

    Button btn_invite_student, btn_invite_staff, btn_school_logo, btn_school_image, btn_daily_reports, btn_colspan;
    String schoolName, id, whichImage;
    private boolean isLogoChanged;
    private boolean isSchoolImageChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nsp = Prefs.with(this).getSharedPreferences();
        mDb = MainDatabase.getDatabase(getApplicationContext());
        chatDb = ChatDatabase.getDatabase(this);
        setContentView(R.layout.activity_admin_navigation_drawer);

        // for uppublic set schoolemail
        switch (BuildConfig.APPLICATION_ID) {
            case "com.zeroerp.uppublic":
                schoolEmail = "uppublicschool@gmail.com";
                break;
            case "com.zeroerp.uppre":
                schoolEmail = "uppublicschool@gmail.com";
                break;
            case "com.zeroerp.apsbpl":
                schoolEmail = "enquiry.apis@gmail.com";
                break;
        }

        buildUserActionModel();

//        Log.e("App updated", nsp.getString(Gc.APPUPDATED, "false"));

        mSessionViewModel = new ViewModelProvider(this).get(SessionViewModel.class);
        colorIs = Config.getAppColor(this, true);
        LanguageSetup.changeLang(this, (getSharedPreferences("APP_LANG", Context.MODE_PRIVATE)).getString("app_language", ""));

        intViews();

        switch (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)) {
            case Gc.ADMIN:
//                userTypeText = Gc.ADMIN;
                bottomNavigationView.inflateMenu(R.menu.menu_admin_bottom_bar);
                iv_school_logo.setClickable(true);
                ll_school_image.setClickable(true);
                tv_admin_name.setText(getString(R.string.administrator) + "\n" + Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, AdminNavigationDrawerNew.this));
                break;
            case Gc.STAFF:
                ll_school_image.setClickable(false);
                iv_school_logo.setClickable(false);
                bottomNavigationView.inflateMenu(R.menu.other_user_bottom_drawar_menu);
                if (Gc.getSharedPreference(Gc.USERTYPECODE, this).equals("7")) {//7 id is for driver
                    startGettingCurrentLocation();
                }
                mDb.signedInStaffInformationModel().getSignedInStaff()
                        .observe(this, new Observer<SignedInStaffInformationEntity>() {
                            @Override
                            public void onChanged(@Nullable SignedInStaffInformationEntity signedInStaffInformationEntity) {
                                if (signedInStaffInformationEntity == null) {
                                    Log.e("AdminNavigationDrwerNew", "getSignedInStaff: Onchanged being called with null values");
                                    return;
                                }
                                signedinStaff = signedInStaffInformationEntity;
                                tv_admin_name.setText(Gc.getSharedPreference(Gc.USERTYPETEXT, AdminNavigationDrawerNew.this) + "\n" + signedinStaff.StaffName);

                                mDb.schoolStaffModel().getStaffById(signedinStaff.StaffId)
                                        .observe(AdminNavigationDrawerNew.this, new Observer<SchoolStaffEntity>() {
                                            @Override
                                            public void onChanged(@Nullable SchoolStaffEntity staffEntity) {
                                                if (staffEntity != null)
                                                    staff = staffEntity;
                                                mDb.schoolStaffModel().getStaffById(signedinStaff.StaffId).removeObserver(this);
                                            }
                                        });

                            }
                        });


                if (!(Gc.getSharedPreference(Gc.PROFILE_URL, AdminNavigationDrawerNew.this).equalsIgnoreCase("")))
                    Glide.with(AdminNavigationDrawerNew.this)
                            .load(Gc.getSharedPreference(Gc.PROFILE_URL, AdminNavigationDrawerNew.this))
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                            .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                            .apply(RequestOptions.circleCropTransform())
                            .thumbnail(0.5f)
                            .into(civ_other_user_profile_image);

                break;

            case Gc.STUDENTPARENT:
                ll_school_image.setClickable(false);
                iv_school_logo.setClickable(false);

                bottomNavigationView.inflateMenu(R.menu.other_user_bottom_drawar_menu);

                mDb.signedInStudentInformationModel().getSignedInStudentInformation()
                        .observe(this, new Observer<StudentInformation>() {
                            @Override
                            public void onChanged(@Nullable StudentInformation studentInformation) {
                                if (studentInformation == null) {
                                    Log.e("AdminNavigationDrwerNew", "getSignedInStudentInformation: Onchanged being called with null values");
                                    return;
                                }
                                signedInStudent = studentInformation;

                                student = signedInStudent.getStudent();

                                tv_admin_name.setText(signedInStudent.StudentName + "\n" + signedInStudent.AdmissionNo);

                                mDb.signedInStudentInformationModel().getSignedInStudentInformation().removeObservers(AdminNavigationDrawerNew.this);

                            }
                        });

                if (!(Gc.getSharedPreference(Gc.PROFILE_URL, this).equalsIgnoreCase("")))
                    Glide.with(this)
                            .load(Gc.getSharedPreference(Gc.PROFILE_URL, this))
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                            .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                            .apply(RequestOptions.circleCropTransform())
                            .thumbnail(0.5f)
                            .into(civ_other_user_profile_image);
                break;

            default:
                startActivity(new Intent(this, FirstScreenActivity.class));
                finish();
                overridePendingTransition(R.anim.enter, R.anim.nothing);
        }

        if (Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equalsIgnoreCase("demo")) {
            if (nsp.getString(Gc.DEMOALERT, "").equalsIgnoreCase("")) {
                nsp.edit().putString(Gc.DEMOALERT, Gc.TRUE).apply();
                demoAlert = new AlertDialog.Builder(this);

                demoAlert.setMessage(Html.fromHtml("<Br>" + getString(R.string.this_is_demo) + "<Br>" + getString(R.string.demo_will_be_reset) + "<Br>"
                        + "<Br>" + getString(R.string.do_not_enter_confidential_information) + "<Br>"
                        + "<B>Email</B> : <font color='#ce001f'>" + Gc.ZEROERPEMAIL + "</font><Br><B>Website</B> :<font color='#ce001f'>www.zeroerp.com</font>"));
                demoAlert.setTitle(getString(R.string.information));
                demoAlert.setIcon(R.drawable.ic_clickhere);
                demoAlert.setPositiveButton(getString(R.string.yes_i_understand), (dialog, which) -> dialog.dismiss());
                demoAlert.setCancelable(true);
                demoAlert.show();

            }
        }


        /* ********************* Remote Config For App Update  *******************************/

        fetchRemoteConfig();


        /* ***********************************************************/

        FirebaseCrashlytics.getInstance().setCustomKey(Gc.ERPINSTCODE, Gc.getSharedPreference(Gc.ERPINSTCODE, this));
        FirebaseCrashlytics.getInstance().setCustomKey(Gc.SIGNEDINUSERROLE, Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this));
        FirebaseCrashlytics.getInstance().setCustomKey(Gc.SIGNEDINUSERNAME, Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this));
        FirebaseCrashlytics.getInstance().setCustomKey(Gc.USERID, Gc.getSharedPreference(Gc.USERID, this));
        FirebaseCrashlytics.getInstance().setCustomKey(Gc.USERTYPECODE, Gc.getSharedPreference(Gc.USERTYPECODE, this));
        FirebaseCrashlytics.getInstance().setCustomKey(Gc.USERTYPETEXT, Gc.getSharedPreference(Gc.USERTYPETEXT, this));


        /* **********************  Save FCM Token To Server   ***************************/

        sharedPreferences_token = getSharedPreferences("MyToken", MODE_PRIVATE);
        token = sharedPreferences_token.getString("fcmToken", "");
        Log.e("fcmToken", token);

        mSessionViewModel.getSessionList().observe(this, new Observer<List<SessionListEntity>>() {
            @Override
            public void onChanged(@Nullable List<SessionListEntity> sessionListEntities) {
                spinnerSessionHandling(sessionListEntities);
                mSessionViewModel.getSessionList().removeObserver(this);
            }
        });

        String Dates = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());

        mDb.noticeBoardModel().getNoticesActive(Gc.ACTIVE, Dates).observe(this, noticeBoardEntities -> {
            notificationList.clear();
            notificationList.addAll(noticeBoardEntities);
            updateNotification();

        });


        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, schoolDetailsEntity -> {
            if (schoolDetailsEntity != null) {
                tv_nav_header_school_name.setText(schoolDetailsEntity.SchoolName);
                schoolName = schoolDetailsEntity.SchoolName;
                id = schoolDetailsEntity.Id;

                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, AdminNavigationDrawerNew.this).equalsIgnoreCase(Gc.ADMIN)) {
                    if ((Strings.nullToEmpty(schoolDetailsEntity.Logo).equalsIgnoreCase(""))
                            || (Strings.nullToEmpty(schoolDetailsEntity.schoolImage).equalsIgnoreCase(""))) {

                        ll_logo.setVisibility(View.VISIBLE);
                        btn_colspan.setVisibility(View.VISIBLE);
                        if (Strings.nullToEmpty(schoolDetailsEntity.Logo).equalsIgnoreCase("")) {
                            btn_school_logo.setVisibility(View.VISIBLE);
                        }

                        if (Strings.nullToEmpty(schoolDetailsEntity.schoolImage).equalsIgnoreCase("")) {
                            btn_school_image.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, AdminNavigationDrawerNew.this).equalsIgnoreCase(Gc.ADMIN)) {
                    if (!(Strings.nullToEmpty(schoolDetailsEntity.Logo).equalsIgnoreCase(""))) {
                        Glide.with(AdminNavigationDrawerNew.this)
                                .load(schoolDetailsEntity.Logo)
                                .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                                .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                                .apply(RequestOptions.circleCropTransform())
                                .thumbnail(0.5f)
                                .into(civ_other_user_profile_image);
                    }
                }
            }
        });

        mDb.organizationPermissionModel().getOrganizationPermissions()
                .observe(this, strings -> {
                    if (Objects.requireNonNull(strings).size() > 0) {
                        orgPermissionList.clear();
                        orgPermissionList.addAll(strings);
                        mDb.userPermissionsModel().getUserPermissions()
                                .observe(AdminNavigationDrawerNew.this, userPermissionsEntities -> {
                                    userPermissionsList.clear();
                                    userPermissionsList.addAll(userPermissionsEntities);
                                    setPermissionsInSharedPreference();
                                    initViewActions();
                                });
                    }
                });

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, AdminNavigationDrawerNew.this).equalsIgnoreCase(Gc.ADMIN)
                || userPermissionsList.contains(Gc.FinanceReports)) {
            ll_daily_reports.setVisibility(View.VISIBLE);
            btn_colspan.setVisibility(View.VISIBLE);
        }

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2804617241", this, adContainer);
        }

    }

    private void fetchRemoteConfig() {
        int cacheExpiration = 3600;

        if (BuildConfig.DEBUG) {
            cacheExpiration = 0;
        }

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(cacheExpiration)
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(remoteConfigSettings);


        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
//                            boolean updated = task.getResult();

                        Toast.makeText(AdminNavigationDrawerNew.this, "Fetch and activate succeeded",
                                Toast.LENGTH_LONG).show();

                        Log.i("Firebase app version", firebaseRemoteConfig.getString("android_app_version") + "empty");

                        String android_app_version = firebaseRemoteConfig.getString("android_app_version");

                        if (!("".equalsIgnoreCase(android_app_version))) {
                            if (BuildConfig.VERSION_CODE < Integer.parseInt(android_app_version)) {
                                showUpdateApp();
                            }
                        }

                    } else {
                        Toast.makeText(AdminNavigationDrawerNew.this, "Fetch failed",
                                Toast.LENGTH_SHORT).show();
                    }
//                        displayWelcomeMessage();
                });

    }

    private void startGettingCurrentLocation() {
        if (checkPermissionForReadPhoneStateAndLocation()) {
            /* **********************  Save FCM Token To Server   ***************************/
            sharedPreferences_token = getSharedPreferences("MyToken", MODE_PRIVATE);

            token = sharedPreferences_token.getString("fcmToken", "");
            Log.e("fcmToken", token);

            if (token != "" || !token.equalsIgnoreCase("")) {

                try {

                    sendRegistrationTokenToServer(this, token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //**********************************************************************************


            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            nsp.edit().putString("IMEI", tm.getDeviceId()).apply();
            startService(new Intent(AdminNavigationDrawerNew.this, MyLocationService.class));
        }
    }

    private boolean checkPermissionForReadPhoneStateAndLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
        ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(new String[]{
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, Config.PERMISSION_IMEI_LOCATION_CODE);

            }
            return false;
        } else return true;

    }

    public void sendInvitation(String invitationType, String msg) {

        JSONObject p = new JSONObject();
        try {
            p.put("orgKey", Gc.getSharedPreference(Gc.ERPINSTCODE, this));
            p.put("invitationType", invitationType);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse( Gc
                        .getSharedPreference(Gc.ERPWEBHOSTURL,AdminNavigationDrawerNew.this) + "/Login/verifyOrganization/"
                        + Gc.getSharedPreference(Gc.ERPINSTCODE, this) + "?orgKey=" + p))
                .setDomainUriPrefix("https://kn3yy.app.goo.gl")
                // Open links with this app on Android
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID).build())
                // Open links with com.example.ios on iOS
                .setIosParameters(new DynamicLink.IosParameters.Builder("in.junctiontech.school")
                        .setAppStoreId("1261309387")
                        .build())
                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();


        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(dynamicLinkUri.toString()))
                .buildShortDynamicLink()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Short link created
                        if (task.getResult() != null) {

                            Uri shortLink = task.getResult().getShortLink();

                            String refererBody1 = msg + " " + shortLink;

                            Intent refererIntent1 = new Intent(Intent.ACTION_SEND);
                            refererIntent1.setType("text/plain");
                            refererIntent1.putExtra(Intent.EXTRA_TEXT, refererBody1);

                            startActivity(Intent.createChooser(refererIntent1, "Invite " + invitationType));
                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                        }
                    } else {
                        Log.e("main", " error " + task.getException());
                    }
                });

    }


    private void intViews() {
        btn_colspan = findViewById(R.id.btn_colspan);

        btn_colspan.setOnClickListener(view -> {
            String Colspan = btn_colspan.getText().toString();
            if (Colspan.equalsIgnoreCase(getResources().getString(R.string.see_less))) {
                ll_ok.setVisibility(View.GONE);
                btn_colspan.setText(getResources().getString(R.string.see_more));
            } else {
                ll_ok.setVisibility(View.VISIBLE);
                btn_colspan.setText(getResources().getString(R.string.see_less));
            }
        });

        ll_logo = findViewById(R.id.ll_logo);
        ll_daily_reports = findViewById(R.id.ll_daily_reports);

        ll_invitation = findViewById(R.id.ll_invitation);
        ll_ok = findViewById(R.id.ll_ok);
        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
            ll_invitation.setVisibility(View.VISIBLE);
            btn_colspan.setVisibility(View.VISIBLE);
        }

        tv_total_staff = findViewById(R.id.tv_total_staff);
        tv_total_student = findViewById(R.id.tv_total_student);

        btn_daily_reports = findViewById(R.id.btn_daily_reports);
        btn_daily_reports.setOnClickListener(view -> {

            startActivity(new Intent(AdminNavigationDrawerNew.this, DailyReportActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.nothing);

        });

        btn_school_logo = findViewById(R.id.btn_school_logo);
        btn_school_logo.setOnClickListener(view -> {
            if (takePermission()) {
                whichImage = "logo";
                CropImage.activity()
                        .setAspectRatio(150, 150)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(this);
            }

        });

        btn_school_image = findViewById(R.id.btn_school_image);
        btn_school_image.setOnClickListener(view -> {
            if (takePermission()) {
                whichImage = "schoolimage";
                CropImage.activity()
                        .setAspectRatio(160, 90)
                        .setMaxCropResultSize(1920, 1080)
                        .start(this);
            }

        });

        btn_invite_student = findViewById(R.id.btn_invite_student);
        btn_invite_student.setOnClickListener(view -> {
            String msg = "Dear Student, Welcome to " + schoolName;
            sendInvitation(getResources().getString(R.string.student), msg);
            /*
            sendInvitation(msg);*/
        });

        btn_invite_staff = findViewById(R.id.btn_invite_staff);
        btn_invite_staff.setOnClickListener(view -> {
            String msg = "Dear Colleagues, Welcome to " + schoolName;
            sendInvitation(getResources().getString(R.string.staff), msg);
        });

        civ_other_user_profile_image = findViewById(R.id.user_profile_image);
        drawer = findViewById(R.id.drawer_layout);
        ll_admin_page_header_background = findViewById(R.id.ll_admin_page_header_background);
        tv_admin_name = findViewById(R.id.tv_admin_name);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.inflateHeaderView(R.layout.nav_header_admin_main);

        View view = navigationView.getHeaderView(0);
        iv_school_logo = view.findViewById(R.id.iv_school_logo);
        ll_school_image = view.findViewById(R.id.ll_admin_header_back_school_image);
        tv_nav_header_school_name = view.findViewById(R.id.tv_nav_header_school_name);
        spinnerInitialize();

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {

                            case R.id.action_other_user_profile:
                                if (!(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, AdminNavigationDrawerNew.this).equalsIgnoreCase(Gc.ADMIN))) {
                                    startActivityForResult(new Intent(AdminNavigationDrawerNew.this, PersonalProfile.class),
                                            Config.ADD_PROFILE_IMAGE);
                                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                                }
                                break;

                            case R.id.action_admin_user_more:
                                drawer.openDrawer(GravityCompat.START);
                                break;

                            case R.id.action_admin_my_notification:
                                startActivity(new Intent(AdminNavigationDrawerNew.this, ChatActivity.class));
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                                break;
                            case R.id.action_other_user_more:
                                drawer.openDrawer(GravityCompat.START);
                                break;
                            case R.id.action_admin_school_detail:
                                startActivityForResult(new Intent(AdminNavigationDrawerNew.this, SubActivity.class)
                                                .putExtra("whichActionData", "schoolInformation")
                                                .putExtra("AddSessionActive", true)

                                        , Config.SCHOOL_INFO_CODE);
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                                break;

                            case R.id.action_other_user_my_notification:
                                startActivity(new Intent(AdminNavigationDrawerNew.this, ChatActivity.class));
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                                //  deleteDatabase();
                                break;
                        }
                        return true;
                    }
                });

        snackbar_admin_main_page = findViewById(R.id.snackbar_admin_main_page);

        /* ************  Notification List Data Fill *********/
        tv_admin_notification_details = findViewById(R.id.tv_admin_notification_details);

        tv_admin_notification_details.setOnClickListener(view1 -> {

//            if (notificationList.size() > 0) {
                tv_admin_notification_details.setEnabled(false);
                // startActivity(new Intent(AdminNavigationDrawerNew.this, NotificationBarActivity.class));
                startActivity(new Intent(AdminNavigationDrawerNew.this, NewNotificationBarActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
//            }
//            else {
//                Snackbar.make(snackbar_admin_main_page,
//                        getString(R.string.no_notifications_available),
//                        Snackbar.LENGTH_LONG).setAction(getString(R.string.retry),
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                            }
//                        }).show();
//            }
        });

        tv_admin_notification_board = findViewById(R.id.tv_admin_notification_board);
        tv_admin_notification_board.setOnClickListener(view12 -> tv_admin_notification_details.performClick());

        LinearLayout ll_notice_boarf = findViewById(R.id.ll_notice_boarf);
        ll_notice_boarf.setOnClickListener(v -> tv_admin_notification_details.performClick());

        alertSession = new AlertDialog.Builder(this);

        alertSession.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

            TextView errorText = (TextView) nav_header_session_list.getSelectedView();

            errorText.setError(getString(R.string.please_select_session_first));
            errorText.setTextColor(Color.WHITE);//just to highlight that this is an error
            isDialogShowing = false;
        });
        alertSession.setCancelable(true);
        alertSession.setMessage(R.string.please_select_session_first);


        File targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                Gc.getSharedPreference(Gc.ERPDBNAME, this) + "_logo" + ".jpg");
    }

    private void checkLicenceExpiry() {
        String licenceExpiry = Gc.getSharedPreference(Gc.LICENCE, this);
        if (!licenceExpiry.equalsIgnoreCase("")) {
            String[] licenceExpiryArry = licenceExpiry.split("-");
            Calendar calCurrent = Calendar.getInstance();
            Calendar calLicence = Calendar.getInstance();
            if (licenceExpiryArry.length >= 3) {
                if (licenceExpiryArry[0].length() == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[0]), (Integer.parseInt(licenceExpiryArry[1]) - 1), Integer.parseInt(licenceExpiryArry[2]));
                else if (licenceExpiryArry[2].length() == 4)
                    calLicence.set(Integer.parseInt(licenceExpiryArry[2]), Integer.parseInt(licenceExpiryArry[1]) - 1, Integer.parseInt(licenceExpiryArry[0]));

            }

            long diff = calLicence.getTimeInMillis() - calCurrent.getTimeInMillis();

            float dayCount = (float) diff / (24 * 60 * 60 * 1000);

            if ((int) dayCount <= 21) {
                if (!checkLicence && Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
                    Intent intent = new Intent(this, LicenceActivity.class);
                    startActivity(intent);
                } else if ((int) dayCount < 0) {
                    if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
                        Intent intent = new Intent(this, LicenceActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(this, SchoolsDetailsActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Snackbar snackbarObj = Snackbar.make(
                            snackbar_admin_main_page,
                            (int) dayCount + " " + getString(R.string.days_left_to_licence_expire),
                            Snackbar.LENGTH_LONG).setAction("", v -> {
                    });
                    snackbarObj.setDuration(1500);
                    snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
                    if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)
                            || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
                        if ((int) dayCount <= 15 && Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
                            snackbarObj.show();
                        } else {
                            snackbarObj.show();
                        }
                    }
                }
                checkLicence = true;
            }
        }
    }

    void buildUserActionModel() {
        myAttendanceViewObjModel = new UserActionModel(R.string.my_attendance, R.drawable.ic_attendance, false,
                0, "myAttendance");

        myScheduleViewObjModel = new UserActionModel(R.string.my_schedule, R.drawable.ic_view_timetable, false,
                0, "mySchedule");

        leaveViewObjModel = new UserActionModel(R.string.leaves, R.drawable.ic_leaves, false,
                0, "leave");

        classObjModel = new UserActionModel(R.string.class_text, R.drawable.popular, true,
                Color.parseColor("#01a499"), "createClass");

        manageFeeObjModel = new UserActionModel(R.string.manage_fees, R.drawable.ic_salary, true,
                Color.parseColor("#b400ab"), "manageFees");

        financeObjModel = new UserActionModel(R.string.finance, R.drawable.ic_coins, true,
                Color.parseColor("#b400ab"), "finance");

        createStudentObjModel = new UserActionModel(R.string.student, R.drawable.ic_student, true,
                Color.parseColor("#285fe6"), "createStudent");

        createSubjectObjModel = new UserActionModel(R.string.subject, R.drawable.ic_subject, false,
                getResources().getColor(R.color.bottomTabSelector), "createSubject");

        createStaffObjModel = new UserActionModel(R.string.staff, R.drawable.ic_teacher1, true,
                Color.parseColor("#0c6603"), "createStaff");

        userListObjModel = new UserActionModel(R.string.user_list, R.drawable.aboutusnew, true,
                Color.parseColor("#ff6262"), "userList");

        homeworkObjModel = new UserActionModel(R.string.assignment, R.drawable.ic_homework, false,
                getResources().getColor(R.color.bottomTabSelector), "homework");

        examObjModel = new UserActionModel(R.string.exam, R.drawable.ic_exam_result, true,
                Color.parseColor("#b400ab"), "exam");

        messageObjModel = new UserActionModel(R.string.communicate, R.drawable.ic_message, false,
                Color.parseColor("#0c6603"), "message");

        messageObjModel = new UserActionModel(R.string.communicate, R.drawable.ic_message, false,
                Color.parseColor("#0c6603"), "message");

        transportObjModel = new UserActionModel(R.string.transport_text, R.drawable.ic_map, false,
                0, "transport");

        galleryObjModel = new UserActionModel(R.string.gallery, R.drawable.ic_gallery, false,
                0, "gallery");

        timeTableObjModel = new UserActionModel(R.string.time_table, R.drawable.ic_time, false,
                0, "timeTable");

        teachersNotesObjModel = new UserActionModel(R.string.review_log, R.drawable.ic_fee_receipt, false,
                0, "teachersNotes");

        myReviewObjModel = new UserActionModel(R.string.review_log, R.drawable.ic_fee_receipt, false,
                0, "myReview");

        if (Gc.STUDENTPARENT.equalsIgnoreCase(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this))) {
            myReviewObjModel.name = R.string.teacher_notes;
        }

        reportViewObjModel = new UserActionModel(R.string.report_view, R.drawable.reportview, false,
                0, "reportView");

        libraryObjModel = new UserActionModel(R.string.library, R.drawable.ic_library, false,
                0, "library");

        eLibraryObjModel = new UserActionModel(R.string.elibrary, R.drawable.ic_pdf, true,
                Color.parseColor("#285fe6"), "elibrary");

        onlineTestObjModel = new UserActionModel(R.string.online_test, R.drawable.ic_test, false,
                0, "onlineTest");

        helpObjModel = new UserActionModel(R.string.help, R.drawable.ic_help, false,
                0, "help");

        RecyclerView rv_main_page_action_list = findViewById(R.id.rv_main_page_action_list);
        rv_main_page_action_list.setLayoutManager(new GridLayoutManager(this, 4));

        userActionAdapter = new UserActionAdapter(this, userActionList, colorIs, true);
        rv_main_page_action_list.setAdapter(userActionAdapter);
    }

    private void initViewActions() {
        userActionList.clear();

        String role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

        switch (role) {

            case Gc.ADMIN:
                if (orgPermissionList.contains(Gc.orgClassSection))
                    userActionList.add(classObjModel);
                if (orgPermissionList.contains(Gc.orgFees))
                    userActionList.add(manageFeeObjModel);
                if (orgPermissionList.contains(Gc.orgFinance))
                    userActionList.add(financeObjModel);
                if (orgPermissionList.contains(Gc.orgStudent)) {
                    userActionList.add(createStudentObjModel);
                }
                if (orgPermissionList.contains(Gc.orgSubject))
                    userActionList.add(createSubjectObjModel);
                if (orgPermissionList.contains(Gc.orgManageStaff)) {
                    userActionList.add(createStaffObjModel);
                    userActionList.add(teachersNotesObjModel);
                }
                if (orgPermissionList.contains(Gc.orgUserManagement))
                    userActionList.add(userListObjModel);
                if (orgPermissionList.contains(Gc.orgHomework))
                    userActionList.add(homeworkObjModel);
//                userActionList.add(examObjModel);
                if (orgPermissionList.contains(Gc.orgCommunicate))
                    userActionList.add(messageObjModel);
                if (orgPermissionList.contains(Gc.orgTransport))
                    userActionList.add(transportObjModel);
                if (orgPermissionList.contains(Gc.orgGallery))
                    userActionList.add(galleryObjModel);
                if (orgPermissionList.contains(Gc.orgTimeTable))
                    userActionList.add(timeTableObjModel);
                if (orgPermissionList.contains(Gc.orgLibrary))
                    userActionList.add(libraryObjModel);
                    userActionList.add(eLibraryObjModel);
                if (orgPermissionList.contains(Gc.orgExam)) {
                    userActionList.add(examObjModel);
//                    userActionList.add(onlineTestObjModel);
                }
                userActionList.add(helpObjModel);
                break;

            case Gc.STUDENTPARENT:
                if (!userActionList.contains(myAttendanceViewObjModel))
                    userActionList.add(myAttendanceViewObjModel);
                if (orgPermissionList.contains(Gc.orgLibrary))
                    if (userPermissionsList.contains(Gc.Library))
                        userActionList.add(libraryObjModel);
                        userActionList.add(eLibraryObjModel);
                if (orgPermissionList.contains(Gc.orgFees))
                    if (userPermissionsList.contains(Gc.Fees))
                        if (!userActionList.contains(manageFeeObjModel))
                            userActionList.add(manageFeeObjModel);
                if (orgPermissionList.contains(Gc.orgGallery))
                    if (userPermissionsList.contains(Gc.Gallery))
                        if (!userActionList.contains(galleryObjModel))
                            userActionList.add(galleryObjModel);
                if (orgPermissionList.contains(Gc.orgHomework))
                    if (userPermissionsList.contains(Gc.Homework))
                        if (!userActionList.contains(homeworkObjModel))
                            userActionList.add(homeworkObjModel);
                if (orgPermissionList.contains(Gc.orgTransport))
                    if (userPermissionsList.contains(Gc.Transport))
                        if (!userActionList.contains(transportObjModel))
                            userActionList.add(transportObjModel);
                // Parent should always see Student menu
                if (orgPermissionList.contains(Gc.orgStudent))
                    if (!userActionList.contains(createStudentObjModel)) {
                        userActionList.add(createStudentObjModel);
                    }
                if (orgPermissionList.contains(Gc.orgTimeTable))
                    if (userPermissionsList.contains(Gc.TimeTable))
                        if (!userActionList.contains(timeTableObjModel))
                            userActionList.add(timeTableObjModel);
                if (orgPermissionList.contains(Gc.orgCommunicate))
                    if (!userActionList.contains(messageObjModel))
                        userActionList.add(messageObjModel);
                if (orgPermissionList.contains(Gc.orgExam)){
                    if (!userActionList.contains(examObjModel)) {
                        userActionList.add(examObjModel);
                    }
                    if (userPermissionsList.contains(Gc.OnlineExam)) {
                        userActionList.add(onlineTestObjModel);
                    }
                }

                if (orgPermissionList.contains(Gc.orgManageStaff) || orgPermissionList.contains(Gc.orgStudent)) {
                    if (userPermissionsList.contains(Gc.CreateStaffReview) ||
                            userPermissionsList.contains(Gc.DisplayStaffReview) ||
                            userPermissionsList.contains(Gc.CreateStudentReview) ||
                            userPermissionsList.contains(Gc.DisplayStudentReview)) {
                        if (!userActionList.contains(teachersNotesObjModel))
                            userActionList.add(teachersNotesObjModel);
                    } else {
                        if (!userActionList.contains(myReviewObjModel))
                            userActionList.add(myReviewObjModel);
                    }
                }
                break;

            case Gc.STAFF:

                if (!userActionList.contains(myAttendanceViewObjModel))
                    userActionList.add(myAttendanceViewObjModel);

                if (orgPermissionList.contains(Gc.orgClassSection))
                    if (userPermissionsList.contains(Gc.ClassSection))
                        if (!userActionList.contains(classObjModel))
                            userActionList.add(classObjModel);
                if (orgPermissionList.contains(Gc.orgSubject))
                    if (userPermissionsList.contains(Gc.Subject))
                        if (!userActionList.contains(createSubjectObjModel))
                            userActionList.add(createSubjectObjModel);
                if (orgPermissionList.contains(Gc.orgFees))
                    if (userPermissionsList.contains(Gc.Fees))
                        if (!userActionList.contains(manageFeeObjModel))
                            userActionList.add(manageFeeObjModel);
                if (orgPermissionList.contains(Gc.orgFinance))
                    if (userPermissionsList.contains(Gc.Finance))
                        userActionList.add(financeObjModel);
                if (orgPermissionList.contains(Gc.orgHomework))
                    if (userPermissionsList.contains(Gc.Homework))
                        if (!userActionList.contains(homeworkObjModel))
                            userActionList.add(homeworkObjModel);
                if (orgPermissionList.contains(Gc.orgTransport))
                    if (userPermissionsList.contains(Gc.Transport))
                        if (!userActionList.contains(transportObjModel))
                            userActionList.add(transportObjModel);
                if (orgPermissionList.contains(Gc.orgStudent))
                    if (userPermissionsList.contains(Gc.Student))
                        if (!userActionList.contains(createStudentObjModel))
                            userActionList.add(createStudentObjModel);
                if (orgPermissionList.contains(Gc.orgTimeTable))
                    userActionList.add(myScheduleViewObjModel);
                if (userPermissionsList.contains(Gc.TimeTable))
                    if (!userActionList.contains(timeTableObjModel))
                        userActionList.add(timeTableObjModel);
                if (orgPermissionList.contains(Gc.orgManageStaff))
                    if (userPermissionsList.contains(Gc.ManageStaff))
                        if (!userActionList.contains(createStaffObjModel))
                            userActionList.add(createStaffObjModel);
                if (orgPermissionList.contains(Gc.orgUserManagement))
                    if (userPermissionsList.contains(Gc.UserManagement))
                        if (!userActionList.contains(userListObjModel))
                            userActionList.add(userListObjModel);
                if (orgPermissionList.contains(Gc.orgGallery))
                    if (userPermissionsList.contains(Gc.Gallery))
                        if (!userActionList.contains(galleryObjModel))
                            userActionList.add(galleryObjModel);
                if (orgPermissionList.contains(Gc.orgCommunicate))
                    if (!userActionList.contains(messageObjModel))
                        userActionList.add(messageObjModel);
                if (orgPermissionList.contains(Gc.orgLibrary))
                    if (userPermissionsList.contains(Gc.Library))
                        userActionList.add(libraryObjModel);
                        userActionList.add(eLibraryObjModel);

                if (orgPermissionList.contains(Gc.orgManageStaff) || orgPermissionList.contains(Gc.orgStudent)) {
                    if (!userActionList.contains(teachersNotesObjModel))
                        userActionList.add(teachersNotesObjModel);
                }

                if (orgPermissionList.contains(Gc.orgExam)) {
                    if (userPermissionsList.contains(Gc.Exam))
                        if (!userActionList.contains(examObjModel)) {
                            userActionList.add(examObjModel);
                        }
                }

                if (orgPermissionList.contains(Gc.orgPayroll))
                    userActionList.add(leaveViewObjModel);
                break;
        }

        userActionAdapter.notifyDataSetChanged();
    }

    public void setOnClickViewAction(UserActionModel userActionModel) {
        switch (userActionModel.getActivityName()) {
            case "myAttendance":
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
                    Intent intent = new Intent(this, StaffAttendanceReportActivity.class);
                    if (staff != null) {
                        intent.putExtra("staff", new Gson().toJson(staff));
                    } else {
                        String staffString = Gc.getSharedPreference(Gc.SIGNEDINSTAFF, this);
                        SignedInStaffInformationEntity stProfile = new Gson().fromJson(staffString, SignedInStaffInformationEntity.class);
                        SchoolStaffEntity stEntity = new SchoolStaffEntity();
                        stEntity.StaffId = stProfile.StaffId;
                        stEntity.StaffName = stProfile.StaffName;

                        intent.putExtra("staff", new Gson().toJson(stEntity));

                    }
                    startActivity(intent);
                } else if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
                    Intent intent1 = new Intent(this, StudentMonthAttendanceActivity.class);
                    intent1.putExtra("student", new Gson().toJson(student));
                    startActivity(intent1);
                }
                break;
            case "mySchedule":
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {
                    Intent intent = new Intent(this, MyScheduleActivity.class);
                    //  intent.putExtra("staff", new Gson().toJson(staff));
                    startActivity(intent);
                }
                break;
            case "leave":
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STAFF)) {

                    startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "leave"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);

                }
                break;

            case "help":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "help"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createClass":
                if (sessionSelected()) {
                    startActivity(new Intent(AdminNavigationDrawerNew.this, ClassSectionActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
            case "manageFees":

                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "manageFee"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                /* }*/

                break;

            case "finance":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "finance"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "createStudent":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "createStudent"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "createSubject":
                if (sessionSelected()) {
                    startActivity(new Intent(AdminNavigationDrawerNew.this, SubjectActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
            case "createStaff":
                   /* startActivity(new Intent(AdminNavigationDrawer.this, StudentSecondPageActivity.class)
                            .putExtra("actionType", "staff"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);*/
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "createStaff"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "userList":
                startActivity(new Intent(AdminNavigationDrawerNew.this, UserListActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "homework":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "homework"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "exam":

                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "exam"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);

                break;

            case "onlineTest":
                startActivity(new Intent(AdminNavigationDrawerNew.this, PendingExamList.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "studentAttendanceEntry":

                break;


            case "message":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "message"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);

                break;
            case "transport":
                if (locationPermission()) {
                    startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "transport"));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
                break;
            case "gallery":
                startActivity(new Intent(AdminNavigationDrawerNew.this, GalleryMainActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "timeTable":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "timeTable"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);

                break;

            case "myReview":

                startActivity(new Intent(AdminNavigationDrawerNew.this, MyReview.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case "teachersNotes":
                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "teachersNotes"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "library":

                startActivity(new Intent(AdminNavigationDrawerNew.this, SubActivity.class).putExtra("whichActionData", "library"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case "elibrary":

                startActivity(new Intent(AdminNavigationDrawerNew.this, ElibraryList.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

        }
    }

    private boolean locationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, Config.LOCATION);
            }


            return false;
        }

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AppRequestQueueController.getInstance(this).cancelRequestByTag(this.getLocalClassName());
            if (exit) {
                finish(); // finish activity
            } else {
                Toast.makeText(this, getString(R.string.press_back_again_to_exit),
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5 * 1000);

            }
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        /* **************    We short the Url *********************/

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "MySchool (Open it in Google Play Store to Download the Application)");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, Config.SCHOOL_PLAY_STORE_LINK + BuildConfig.APPLICATION_ID);


        Intent intentSetting = new Intent(getBaseContext(), in.junctiontech.school.menuActions.Settings.class);
        intentSetting.putExtra("setClass", false);
        intentSetting.putExtra("LangChange", false);

        /* **************    We short the Url www.zeroerp.com/school-management-software-blog *********************/
        Intent howToUseIntent =
                new Intent("android.intent.action.VIEW", Uri.parse(Config.SCHOOL_HELP_LINK));
        /* **************    We short the Url  www.zeroerp.com/zeroerp-school-management-software-pricing *********************/

        String reffererBody1 = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;

        if (BuildConfig.APPLICATION_ID.equalsIgnoreCase("in.junctiontech.school")) {
            reffererBody1 = reffererBody1 + "&referrer=" + Gc.getSharedPreference(Gc.ERPINSTCODE, this);
        }

        Intent reffererIntent1 = new Intent(Intent.ACTION_SEND);
        reffererIntent1.setType("text/plain");
        reffererIntent1.putExtra(Intent.EXTRA_TEXT, reffererBody1);
        switch (id) {

            case R.id.nav_school_calendar:
                startActivity(new Intent(AdminNavigationDrawerNew.this, SchoolCalenderActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case R.id.nav_school_information:
                startActivityForResult(new Intent(AdminNavigationDrawerNew.this, SubActivity.class)
                                .putExtra("whichActionData", "schoolInformation")
                                .putExtra("AddSessionActive", true)

                        , Config.SCHOOL_INFO_CODE);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case R.id.nav_language_setting:
                startActivityForResult(new Intent(this, AdminLanguageSetting.class), LANGUAGE_CODE);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case R.id.nav_rate_our_app:
                rateApp();
                break;

            case R.id.nav_share:
                startActivity(Intent.createChooser(reffererIntent1, "Share via"));
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case R.id.nav_logout:
                logout();
                break;

            case R.id.nav_setting:
                startActivityForResult(intentSetting, 676);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;

            case R.id.nav_reload_school_data:
                drawer.closeDrawers();

                try {
                    reloadProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                reloadSchoolData();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == LANGUAGE_CODE) {
            LanguageSetup.changeLang(this, (getSharedPreferences("APP_LANG", Context.MODE_PRIVATE)).getString("app_language", ""));
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Uri resultUri = Objects.requireNonNull(result).getUri();

            if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("logo")) {
                if (!Objects.requireNonNull(resultUri.getPath()).isEmpty()) {
                    isLogoChanged = true;


                    File file = new File(Objects.requireNonNull(resultUri.getPath()));

                    String getFileSize = getFileSize(file.length());
                    String[] split = getFileSize.split("_");

                    if (split.length > 1 && (Strings.nullToEmpty(split[1]).equalsIgnoreCase("B")
                            || Strings.nullToEmpty(split[1]).equalsIgnoreCase("KB")
                            || Strings.nullToEmpty(split[1]).equalsIgnoreCase("MB"))) {

                        String Size = split[0].trim();
                        long l = (long) Double.parseDouble(Size);

                        if (split[1].equals("B") || split[1].equals("KB") || (split[1].equals("MB") && l <= 1)) {

                            Glide.with(this)
                                    .load(new File(Objects.requireNonNull(resultUri.getPath()))) // Uri of the picture
                                    .into(civ_other_user_profile_image);

                            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());

                            try {
                                saveInfo(bitmap);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String message = Objects.requireNonNull(data.getExtras()).getString("message");
                            Config.responseSnackBarHandler(message,
                                    findViewById(R.id.drawer_layout), R.color.fragment_first_green);
                        } else {
                            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                            alert1.setTitle("Maximum Image Size Less Than 1 MB");
                            alert1.setNegativeButton(getString(R.string.ok), (dialog, which) -> {

                            });

                            alert1.show();
                        }
                    } else {
                        Config.responseSnackBarHandler("Invalid Image Selected",
                                findViewById(R.id.drawer_layout), R.color.fragment_first_green);
                    }
                }
            } else if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("schoolimage")) {
                if (!Objects.requireNonNull(resultUri.getPath()).isEmpty()) {
                    isSchoolImageChanged = true;


                    File file = new File(Objects.requireNonNull(resultUri.getPath()));

                    String getFileSize = getFileSize(file.length());
                    String[] split = getFileSize.split("_");

                    if (split.length > 1 && (Strings.nullToEmpty(split[1]).equalsIgnoreCase("B")
                            || Strings.nullToEmpty(split[1]).equalsIgnoreCase("KB")
                            || Strings.nullToEmpty(split[1]).equalsIgnoreCase("MB"))) {

                        String Size = split[0].trim();
                        long l = (long) Double.parseDouble(Size);

                        if (split[1].equals("B") || split[1].equals("KB") || (split[1].equals("MB") && l <= 1)) {

                            Glide.with(this)
                                    .load(new File(Objects.requireNonNull(resultUri.getPath())))
                                    .apply(RequestOptions.placeholderOf(R.drawable.s1))
                                    .apply(RequestOptions.errorOf(R.drawable.s1))
                                    .into(new SimpleTarget<Drawable>() {
                                        @Override
                                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                                            ll_school_image.setBackground(resource);
                                        }
                                    });


                            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());

                            try {
                                saveInfo(bitmap);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String message = Objects.requireNonNull(data.getExtras()).getString("message");
                            Config.responseSnackBarHandler(message,
                                    findViewById(R.id.drawer_layout), R.color.fragment_first_green);
                        } else {
                            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                            alert1.setTitle("Maximum Image Size Less Than 1 MB");
                            alert1.setNegativeButton(getString(R.string.ok), (dialog, which) -> {

                            });

                            alert1.show();
                        }
                    } else {
                        Config.responseSnackBarHandler("Invalid Image Selected",
                                findViewById(R.id.drawer_layout), R.color.fragment_first_green);
                    }
                }
            }
        }
    }

    public static String getFileSize(long size) {
        if (size <= 0)
            return "0";

        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + "_" + units[digitGroups];
    }

    private void saveInfo(Bitmap bitmap) throws JSONException {


        final JSONObject param = new JSONObject();
        if (isLogoChanged) {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("Logo", encodedImage);
        } else if (isSchoolImageChanged) {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("schoolImage", encodedImage);
        } else {

            Config.responseSnackBarHandler("Image Not Found",
                    findViewById(R.id.drawer_layout), R.color.fragment_first_blue);
            return;
        }

        JSONObject p = new JSONObject();

        p.put("id", id);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "schoolDetail/schoolDetails/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");
            if (Gc.APIRESPONSE200.equals(code)) {
                try {
                    ll_logo.setVisibility(View.GONE);
                    final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(job.getJSONObject("result").optString("schoolDetails"), SchoolDetailsEntity.class);
                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.LOGO_URL, schoolDetailsEntity.Logo);
                    setDefaults.put(Gc.SCHOOLIMAGE, schoolDetailsEntity.schoolImage);
                    Gc.setSharedPreference(setDefaults, this);
                    new Thread(() -> {
                        mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);
                    }).start();


                    Config.responseSnackBarHandler("Logo Upload Successfully",
                            findViewById(R.id.drawer_layout), R.color.fragment_first_blue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.drawer_layout), R.color.fragment_first_blue);
            }
        }, volleyError -> {
            Config.responseVolleyErrorHandler(this, volleyError, findViewById(R.id.drawer_layout));
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private void displayLogoAndSchoolImage() {
        if (!(Gc.getSharedPreference(Gc.LOGO_URL, this).equalsIgnoreCase("")))
            Glide.with(this)
                    .load(Gc.getSharedPreference(Gc.LOGO_URL, this))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                    .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                    .apply(RequestOptions.circleCropTransform())
                    .into(iv_school_logo);
        if (!(Gc.getSharedPreference(Gc.SCHOOLIMAGE, this).equalsIgnoreCase("")))
            Glide.with(this)
                    .load(Gc.getSharedPreference(Gc.SCHOOLIMAGE, this))
                    .apply(RequestOptions.placeholderOf(R.drawable.s1))
                    .apply(RequestOptions.errorOf(R.drawable.s1))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ll_school_image.setBackground(resource);
                        }
                    });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        intent.putExtra(Config.isFromNotification, false);
        super.onNewIntent(intent);
    }

    private void logout() {
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
        alert1.setMessage("\n" + getString(R.string.after_logout_you_will_not_receive_any_message_and_notices_from_school) + "\n\n"
                + getString(R.string.are_you_sure_you_want_to_logout) + "\n");
        alert1.setTitle(getString(R.string.logout));
        alert1.setIcon(R.drawable.logout1);
        alert1.setPositiveButton(getString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearData();
            }
        });
        alert1.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alert1.show();

        ArrayList<AsyncTask> asyncTasks = AppRequestQueueController.getInstance(getApplicationContext()).getAsyncTask();
        for (int i = 0; i < asyncTasks.size(); i++) {
            asyncTasks.get(i).cancel(true);
        }
    }

    private void clearData() {
        logoutActionsForServer();
        AppRequestQueueController.getInstance(this).cancelRequestByTag("getDetails");
        DbHandler.getInstance(AdminNavigationDrawerNew.this).deleteDatabase();

        SharedPreferences spColor = getApplicationContext().getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);

        spColor.edit().clear().apply(); // clear color


        nsp.edit().clear().apply();  // clear old global constants


        Gc.removeAll(this); // clear global constants

        Gc.clearid(this);  // clear unique id

        new Thread(() -> {
            mDb.clearAllTables();  // delete MainDatabase
            chatDb.clearAllTables();
        }).start();


        mSessionViewModel.getSessionList().removeObservers(this);

        File fileOrDirectory = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME);

        deleteRecursive(fileOrDirectory);

        stopService(new Intent(AdminNavigationDrawerNew.this, MyLocationService.class));
        Intent intent = new Intent(AdminNavigationDrawerNew.this, FirstScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        finish();
        overridePendingTransition(R.anim.enter, R.anim.nothing);
    }

    public static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            if (fileOrDirectory.listFiles() != null)
                for (File child : fileOrDirectory.listFiles()) {
                    Log.e("LOGO_Name", child.getName());
                    deleteRecursive(child);
                }
        }

        fileOrDirectory.delete();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);

//        if (permsRequestCode == Config.PERMISSION_IMEI_LOCATION_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
//                checkPermissionForReadPhoneStateAndLocation();
//            else
//                startGettingCurrentLocation();
//
//        } else

        if (permsRequestCode == Config.LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //denied
                    Log.e("denied", permissions[0]);
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permissions[0])
                            == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        Log.e("allowed", permissions[0]);
                    } else {
                        //set to never ask again
                        Log.e("set to never ask again", permissions[0]);
                        //do something here.
                        showRationale(getString(R.string.permission_denied), getString(R.string.permission_denied_location));

                    }
                }
            }
        } else if (permsRequestCode == Config.IMAGE_LOGO_CODE) {
            isPermissionDialogOpen = false;
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //denied
                        Log.e("denied", permissions[0]);
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                            //allowed

                        } else {
                            //set to never ask again
                            Log.e("set to never ask again", permissions[0]);
                            //do something here.
                            showRationale(getString(R.string.permission_denied), getString(
                                    R.string.permission_denied_external_storage));

                        }
                    }
                }
            }

        }
    }

    public boolean takePermission() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);
            }
            return false;
        } else return true;

    }

    private void showRationale(String permission, String permission_denied_location) {
        AlertDialog.Builder alertLocationInfo = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_alert, null));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, Config.LOCATION);
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), (dialogInterface, i) -> {

        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }

    public void rateApp() {
        try {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        } catch (ActivityNotFoundException e) {
            Intent rateIntent = rateIntentForUrl("http://play.google.com/store/apps/details");
            startActivity(rateIntent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        }
    }

    private Intent rateIntentForUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url,
                getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    public boolean sessionSelected() {
        boolean selectedSession;
        Log.i("SessionSelected", Gc
                .getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this));
        if (Gc
                .getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this)
                .equals(Gc.NOTFOUND)) {
            selectedSession = false;
            if (!isDialogShowing)
                alertSession.show();
            isDialogShowing = true;
        } else {

            selectedSession = true;
        }

        return selectedSession;
    }

    @Override
    protected void onResume() {

        super.onResume();

        colorIs = Config.getAppColor(this, true);

        setColorApp();

        displayLogoAndSchoolImage();

        checkLicenceExpiry();

        checkEmailPhoneExists();

        String permissionChanged = nsp.getString(Gc.RELOADPERMISSIONS, "");
        if (permissionChanged.equalsIgnoreCase(Gc.TRUE)) {
            nsp.edit().putString(Gc.RELOADPERMISSIONS, Gc.FALSE).apply();
            try {
                reloadProfile();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            reloadSchoolData();
        }

        String reloadDemo = nsp.getString(Gc.RELOADDEMO, "");
        if (reloadDemo.equalsIgnoreCase(Gc.TRUE)) {
            if (Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equalsIgnoreCase(Gc.DEMO)) {
                nsp.edit().putString(Gc.RELOADDEMO, Gc.FALSE).apply();

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.demo_system_is_reset) + "<Br>"
                        + "<Br>" + getString(R.string.click_the_demo_again_to_explore_zeroerp) + "<Br>"
                        + "<B>Email</B> : <font color='#ce001f'>info@zeroerp.com</font><Br><B>Contact no.</B> :<font color='#ce001f'>+91 8889997605</font>"));
                alert1.setTitle(getString(R.string.information));
                alert1.setIcon(R.drawable.ic_clickhere);
                alert1.setPositiveButton(getString(R.string.logout), (dialog, which) -> {
                    clearData();
                });
                alert1.setCancelable(false);
                alert1.show();

            }
        }

        if (Gc.getSharedPreference(Gc.ISORGANIZATIONDELETED, this).equals(Gc.TRUE)) {
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


            alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.organization_not_found) + "<Br>"
                    + "<Br>" + getString(R.string.login_again_or_contact_zeroerp) + "<Br>"
                    + "<B>Email</B> : <font color='#ce001f'>" + Gc.ZEROERPEMAIL + "</font><Br><B>" + getString(R.string.contact) + "</B> :<font color='#ce001f'>" + Gc.ZEROERPPHONE + " </font>"));
            alert1.setTitle(getString(R.string.information));
            alert1.setIcon(R.drawable.ic_clickhere);
            alert1.setPositiveButton(getString(R.string.logout), (dialog, which) -> {
                clearData();
            });
            alert1.setCancelable(false);
            alert1.show();

        }

        if (Gc.getSharedPreference(Gc.NOTAUTHORIZED, this).equals(Gc.TRUE)) {
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
// Fix for Bug id 866dce show uppublic email for their app
            alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.not_authorized) + "<Br>"
                    + "<Br>" + getString(R.string.login_again_or_contact_zeroerp) + "<Br>"
                    + "<B>Email</B> : <font color='#ce001f'>" + schoolEmail + "</font><Br>"));
            alert1.setTitle(getString(R.string.information));
            alert1.setIcon(R.drawable.ic_clickhere);
            alert1.setPositiveButton(getString(R.string.logout), (dialog, which) -> {
                clearData();
            });
            alert1.setCancelable(false);
            alert1.show();

        }

        isOnPause = false;
        tv_admin_notification_details.setEnabled(true);

        mDb.schoolStudentModel()
                .getAllStudentsCount(Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, schoolStudentEntities -> tv_total_student.setText(Objects.requireNonNull(schoolStudentEntities).studentCount));

        mDb.schoolStaffModel()
                .getAllStaffCount()
                .observe(this, schoolStaffCount -> tv_total_staff.setText(Objects.requireNonNull(schoolStaffCount).staffCount));

        mDb.masterEntryModel().getMasterEntryValuesGeneric("selfRegistration")
                .observe(this, masterEntryEntities -> {
                    if (masterEntryEntities != null && masterEntryEntities.size() > 0) {
                        if (Strings.nullToEmpty(masterEntryEntities.get(0).MasterEntryValue).equalsIgnoreCase("Allow")) {
                            ll_invitation.setVisibility(View.VISIBLE);
                            btn_colspan.setVisibility(View.VISIBLE);
                        }
                    }
                });


        ll_daily_reports.setVisibility(View.GONE);
        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, AdminNavigationDrawerNew.this).equalsIgnoreCase(Gc.ADMIN)
                || userPermissionsList.contains(Gc.FinanceReports)) {
            ll_daily_reports.setVisibility(View.VISIBLE);
            btn_colspan.setVisibility(View.VISIBLE);
        }
    }

    private void checkEmailPhoneExists() {
        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            try {
                StudentInformation student = new Gson().fromJson(Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this), StudentInformation.class);

                String email = Strings.nullToEmpty(student.StudentEmail);
                String phone = Strings.nullToEmpty(student.Mobile);

                SharedPreferences pref = getSharedPreferences(Gc.SCHOOLPREF, 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();

                if ("".equalsIgnoreCase(email) || "".equalsIgnoreCase(phone)) {

                    Long date = pref.getLong(Gc.REMINDEREMAILPHONE, 0);

                    Long currentDate = new Date().getTime();

                    Long diff = currentDate - date;

                    int days = (int) (currentDate - date) / (1000 * 60 * 60 * 24);

                    if (date == 0 || days > 1) {
                        if ("".equalsIgnoreCase(Strings.nullToEmpty(student.Mobile))) {
                            Intent intent1 = new Intent(this, ContactDetail.class);
                            startActivity(intent1);
                            return;
                        }
                        if ("".equalsIgnoreCase(Strings.nullToEmpty(student.StudentEmail))) {
                            Intent intent1 = new Intent(this, ContactDetail.class);
                            startActivity(intent1);
                        }
                    }
                } else {
                    editor.putLong(Gc.REMINDEREMAILPHONE, new Date().getTime());
                    editor.apply();
                }
            }catch (JsonSyntaxException e){

                HashMap<String, String> setDefaults1 = new HashMap<>();
                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                Gc.setSharedPreference(setDefaults1, this);

            }
        }
    }

    private void setColorApp() {
        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_checked},  // unchecked
                new int[]{android.R.attr.state_checked},   // checked
                new int[]{}                                // default
        };

        int[] colorsText = new int[]{
                Color.parseColor("#000000"),
                colorIs,
                Color.parseColor("#000000"),
        };
        int[] colorsIcon = new int[]{
                colorIs,
                colorIs,
                colorIs,
        };
        ColorStateList navigationViewColorStateList = new ColorStateList(states, colorsIcon);

        navigationView.setItemTextColor(new ColorStateList(states, colorsText));

        navigationView.setItemIconTintList(navigationViewColorStateList);

        bottomNavigationView.setItemTextColor(new ColorStateList(states, colorsText));
        bottomNavigationView.setItemIconTintList(navigationViewColorStateList);
        tv_admin_notification_board.setTextColor(colorIs);
        ll_admin_page_header_background.setBackgroundColor(colorIs);

        btn_invite_student.setTextColor(colorIs);
        btn_invite_staff.setTextColor(colorIs);
        btn_school_logo.setTextColor(colorIs);
        btn_school_image.setTextColor(colorIs);
        btn_daily_reports.setTextColor(colorIs);
        btn_colspan.setTextColor(colorIs);

        if (userActionAdapter != null)
            userActionAdapter.updateData(userActionList, colorIs);


    }

    private void updateNotification() {
        String notificationText = "";
        String next = "<font color='#ce001f'>" + "  * " + "</font>";

        for (NoticeBoardEntity noticeBoardEntity : notificationList) {

            notificationText = notificationText +
                    next + noticeBoardEntity.Description.replace("\n", "") + "      ";

        }

        tv_admin_notification_details.setText(Html.fromHtml(notificationText));

        if (notificationList.size() == 0) {
            tv_admin_notification_details.setText(getString(R.string.no_notifications_available) + " !");
        }
    }

    void logoutActionsForServer() {

        final ProgressDialog myprogress = new ProgressDialog(this);
        myprogress.setCancelable(false);
        myprogress.setMessage(getString(R.string.please_wait));

        final String ORGKEY = Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext());
        final String DBNAME = Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext());

        final String DEVICEID = Gc.id(getApplicationContext());
        final String USERID = Gc.getSharedPreference(Gc.USERID, getApplicationContext());
        final String USERTYPE = Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext());
        final String ACCESSTOKEN = Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext());

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "login/logOut";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    Log.i("Logout :", "Successful");
                    break;

                default:
                    Log.e("Logout problem :", job.optString("message"));
            }
        }, error -> {
            Log.i("Logout", "volley error");


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", ORGKEY);
                headers.put("DBNAME", DBNAME);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", DEVICEID);
                headers.put("USERID", USERID);
                headers.put("USERTYPE", USERTYPE);
                headers.put("ACCESSTOKEN", ACCESSTOKEN);
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public void sendRegistrationTokenToServer(Context context, final String token) throws JSONException {

        if (("").equals(token) || token.equalsIgnoreCase("")) {
        } else {
            final SharedPreferences sp = Prefs.with(context).getSharedPreferences();


            String imei = sp.getString("IMEI", "");
            if (imei.equalsIgnoreCase("")) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager tm = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
                    sp.edit().putString("IMEI", tm.getDeviceId()).apply();
                }
                imei = sp.getString("IMEI", "");
            }

            final String db_name = Gc.getSharedPreference(Gc.ERPDBNAME, this);
            final JSONObject param = new JSONObject();
            param.put("DB_Name", db_name);
            if ((Gc.getSharedPreference(Gc.USERTYPETEXT, this).equalsIgnoreCase("Administrator")
                    ||
                    Gc.getSharedPreference(Gc.USERTYPETEXT, this).equalsIgnoreCase("admin"))) {
                param.put("UserID", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this));
                param.put("UserType", "Administrator");
            } else {
                param.put("UserID", Gc.getSharedPreference(Gc.STAFFID, this));
                param.put("UserType", Gc.getSharedPreference(Gc.USERTYPETEXT, this));
            }

            param.put("token", token);
            param.put("IMEI", imei);
            param.put("UserDevice", "android");
            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Config.applicationVersionNameValue
                    + "gcm.php?action=saveToken";
            Log.d("url", url);
            Log.d("param", param.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.d("responseTokenSaving", jsonObject.toString() + "");
                            boolean savedToken = false;
                            if (jsonObject.optBoolean("result")) {
                                savedToken = true;
                                Log.e("savedToken", "1");
                            } else {
//                                savedToken = false;
                                Log.e("savedToken", "0");
                            }
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putBoolean("savedToken", savedToken);
                            editor.apply();
                            Log.e("savedToken", "s");
                        }
                    }, volleyError -> {
                Log.i("update IMEI Token", Objects.requireNonNull(volleyError.getMessage()));
                //            Config.responseVolleyErrorHandler(FeeTypeActivity.this,error,findViewById(R.id.ll_snackbar_create_fee_type));
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(context).addToRequestQueue(jsonObjectRequest);


        }
    }

    public void setDefaultSession() {

        setDefaultSession = true;

        if (Gc
                .getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this).equals(Gc.NOTFOUND)) {
            nav_header_session_list.setSelection(sessionList.
                    indexOf(Gc
                            .getSharedPreference(Gc.CURRENTSESSION, AdminNavigationDrawerNew.this)));
        } else {
            nav_header_session_list.setSelection(sessionList.
                    indexOf(Gc
                            .getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this)));
            int i = sessionList.
                    indexOf(Gc
                            .getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this));

        }
    }

    private void spinnerInitialize() {
        nav_header_session_list = findViewById(R.id.nav_admin_session_list);
        nav_header_session_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.i("Spinner Selected", sessionList.get(i));
                HashMap<String, String> setDefaults = new HashMap<String, String>();
                setDefaults.put(Gc.APPSESSION, sessionList.get(i));
                setDefaults.put(Gc.APPSESSIONSTART, sessionEntities.get(i).sessionStartDate);
                setDefaults.put(Gc.APPSESSIONEND, sessionEntities.get(i).sessionEndDate);
                Gc.setSharedPreference(setDefaults, AdminNavigationDrawerNew.this);


                String currentSessionText = "<B>" + "<font color='#ce001f'>"
                        + sessionList.get(i)
                        + "<br></font>" + "</B>";
                final String sessionStartDateText = "<B>" + "<font color='#ce001f'>" +
                        sessionEntities.get(i).sessionStartDate + "<br></font>" + "</B>";

                String sessionEndDateText = "<B>" + "<font color='#ce001f'>"
                        + sessionEntities.get(i).sessionEndDate + "<br></font>" + "</B>";

                final AlertDialog.Builder alertSessionInfo = new AlertDialog.Builder(AdminNavigationDrawerNew.this);
                alertSessionInfo.setTitle(getString(R.string.session_information));


                if (Gc.getSharedPreference(Gc.APPSESSION, AdminNavigationDrawerNew.this)
                        .equalsIgnoreCase(Gc.getSharedPreference(Gc.CURRENTSESSION, AdminNavigationDrawerNew.this))) {
                    alertSessionInfo.setMessage(Html.fromHtml(
                            "<br>" +
                                    getString(R.string.you_sesion_is) + " " +
                                    currentSessionText +

                                    getString(R.string.session_starts_from) + " : " +
                                    sessionStartDateText
                                    + getString(R.string.and_ends_on) + " : " + sessionEndDateText

                    ));
                } else {
                    alertSessionInfo.setMessage(Html.fromHtml(
                            "<br>" +
                                    getString(R.string.you_sesion_is) + " " +
                                    currentSessionText +

                                    getString(R.string.session_starts_from) + " : " +
                                    sessionStartDateText
                                    + getString(R.string.and_ends_on) + " : " + sessionEndDateText + "<B>" +
                                    getString(R.string.this_is_not_your_current_session) + "<B>"

                    ));
                }

                alertSessionInfo.setPositiveButton(getString(R.string.ok) + " " + getString(R.string.yes_i_understand), (dialogInterface, i1) -> {
                    if (!setDefaultSession) {
                        try {
                            reloadProfile();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        reloadSchoolData();
                    } else
                        setDefaultSession = false;

                    dialogInterface.dismiss();
                });
//                alertSessionInfo.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));
                alertSessionInfo.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_clickhere, null) );

                alertSessionInfo.show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.i("Spinner Selected", "Nothing Selected");

            }
        });
    }

    private void spinnerSessionHandling(List<SessionListEntity> sessionListEntities) {

        if (Gc
                .getSharedPreference(Gc.CURRENTSESSION, AdminNavigationDrawerNew.this)
                .equals(Gc.NOTFOUND)) {
            Intent intent = new Intent(AdminNavigationDrawerNew.this, SessionActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
            finish();
            return;
        } else {
            sessionList.clear();
            sessionEntities.clear();
            sessionEntities.addAll(sessionListEntities);
            for (SessionListEntity s : sessionEntities) {
                sessionList.add(s.session);
            }
        }

        ArrayAdapter<String> arr = new ArrayAdapter<String>(AdminNavigationDrawerNew.this,
                android.R.layout.simple_list_item_1, sessionList);
        nav_header_session_list.setAdapter(arr);
        nav_header_session_list.setEnabled(true);
        setDefaultSession();
    }

    void reloadProfile() throws JSONException {
        String url;

        final String userRole = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

        if (userRole.equalsIgnoreCase(Gc.STUDENTPARENT)) {
            url = Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "profile/studentProfile/"
                    + new JSONObject()
                    .put("Session", Gc.getSharedPreference(Gc.APPSESSION, this))
                    .put("ProfileId", Gc.getSharedPreference(Gc.STUDENTADMISSIONID, this))
            ;
        } else if (userRole.equalsIgnoreCase(Gc.STAFF)) {
            url = Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "profile/staffProfile/"
                    + new JSONObject()
                    .put("ProfileId", Gc.getSharedPreference(Gc.USERID, this))
            ;
        } else {
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        JSONObject resultjobj = job.getJSONObject("result");
                        if (userRole.equalsIgnoreCase(Gc.STUDENTPARENT)) {
                            final StudentInformation studentInformation = new Gson()
                                    .fromJson(resultjobj.optString("studentProfile"),
                                            StudentInformation.class);
                            new Thread(() -> {
                                mDb.signedInStudentInformationModel()
                                        .insertSignedInStudentInformation(studentInformation);
                            }).start();
                            HashMap<String, String> setDefaults = new HashMap<>();

                            setDefaults.put(Gc.SIGNEDINSTUDENT, new Gson().toJson(studentInformation));
                            Gc.setSharedPreference(setDefaults, AdminNavigationDrawerNew.this);

                        } else if (userRole.equalsIgnoreCase(Gc.STAFF)) {
                            final SignedInStaffInformationEntity staff = new Gson()
                                    .fromJson(resultjobj.optString("staffProfile"), SignedInStaffInformationEntity.class);

                            String signedinstaff = new Gson().toJson(staff);
                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.SIGNEDINSTAFF, signedinstaff);

                            Gc.setSharedPreference(setDefaults, getApplicationContext());

                            new Thread(() -> {
                                mDb.signedInStaffInformationModel().insertSignedInStaffInformation(staff);
                            }).start();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    Gc.logCrash(AdminNavigationDrawerNew.this, "ReloadStudentStaffProfile");

                    break;

                default:
                    Log.i("Reload Profile Data", job.optString("message"));
            }
        }, error -> Config.responseVolleyErrorHandler(AdminNavigationDrawerNew.this, error, findViewById(R.id.drawer_layout))) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void reloadSchoolData() {
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "login/reloadSchoolData/"
                + Gc.getSharedPreference(Gc.APPSESSION, this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:

                    try {
                        JSONObject resultjobj = job.getJSONObject("result");
                        saveSignUpDataToRoomDB(resultjobj);
                        getStaff();
                        getStudents();
                        getSchoolPublicDetails();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    Gc.logCrash(AdminNavigationDrawerNew.this, url);

                    break;

                default:
                    Log.i("Reload School Data", job.optString("message"));
            }
        }, error -> Config.responseVolleyErrorHandler(AdminNavigationDrawerNew.this, error, findViewById(R.id.drawer_layout))) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void getSchoolPublicDetails() {

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "public/schoolPublicDetails";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(),
                job -> {
                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200:
                            try {

                                JSONObject resultJsonObject = job.getJSONObject("result");
                                JSONArray noticeJsonArray = resultJsonObject.getJSONArray("noticeBoards");
                                final ArrayList<NoticeBoardEntity> noticeBoardEntities = new Gson().fromJson(noticeJsonArray.toString(), new TypeToken<List<NoticeBoardEntity>>() {
                                }.getType());
                                if (noticeBoardEntities.size() > 0) {
                                    new Thread(() -> mDb.noticeBoardModel().insertNotice(noticeBoardEntities)).start();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;

                        case Gc.APIRESPONSE500:
                            androidx.appcompat.app.AlertDialog.Builder alert1 = new androidx.appcompat.app.AlertDialog.Builder(AdminNavigationDrawerNew.this);
                            alert1.setMessage(Html.fromHtml("<Br>" + "Your licence is expired !<Br>For further usage contact on<Br>"
                                    + "<B>Email</B> : <font color='#ce001f'>info@zeroerp.com</font><Br><B>Or Visit</B> :<font color='#ce001f'>www.zeroerp.com</font>"));
                            alert1.setTitle(getString(R.string.information));
                            alert1.setIcon(R.drawable.ic_clickhere);
                            alert1.setPositiveButton(getString(R.string.logout), (dialog, which) -> {
                            });
                            alert1.show();
                    }
                },
                error -> {
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(req);
    }

    private void saveSignUpDataToRoomDB(JSONObject resultjobj) {

        try {

            HashMap<String, String> setDefaults = new HashMap<String, String>();
            setDefaults.put(Gc.LICENCE, resultjobj.getString("license"));
            setDefaults.put(Gc.STUDENTCOUNT, resultjobj.getString("studentCount"));
            setDefaults.put(Gc.SMSBALANCE, resultjobj.getString("SMSBalance"));

            Gc.setSharedPreference(setDefaults, this);

//            JSONObject schoolDetailsjobj = resultjobj.getJSONObject("schoolDetails");
            final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(resultjobj.optString("schoolDetails"), SchoolDetailsEntity.class);

            if (Strings.nullToEmpty(schoolDetailsEntity.SchoolName).isEmpty() || Strings.nullToEmpty(schoolDetailsEntity.Email).isEmpty()) {
                setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.NOTFOUND);
            } else {
                setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.EXISTS);
                setDefaults.put(Gc.LOGO_URL, Strings.nullToEmpty(schoolDetailsEntity.Logo));
                setDefaults.put(Gc.SCHOOLIMAGE, Strings.nullToEmpty(schoolDetailsEntity.schoolImage));
                setDefaults.put(Gc.SCHOOLNAME, schoolDetailsEntity.SchoolName);

                SharedPreferences spColor = getApplicationContext().getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);

                if (spColor.getInt(Config.APP_COLOR, 0) != Integer.parseInt(schoolDetailsEntity.themeColor)) {
                    spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();

                } else
                    spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();


                new Thread(() -> mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity)).start();
            }


            JSONObject academicSessionsjobj = resultjobj.getJSONObject("academicSessions");
            final ArrayList<SessionListEntity> sessionListEntities = new Gson()
                    .fromJson(resultjobj.getJSONObject("academicSessions")
                            .optString("sessionList"), new TypeToken<List<SessionListEntity>>() {
                    }.getType());

            if (sessionListEntities != null) {
                setDefaults.put(Gc.SESSIONEXISTS, Gc.EXISTS);
                for (SessionListEntity session : sessionListEntities) {
                    if (academicSessionsjobj.getString("currentSession").equalsIgnoreCase(session.session)) {
                        setDefaults.put(Gc.CURRENTSESSIONSTART, Gc.convertDate(session.sessionStartDate));
                        setDefaults.put(Gc.CURRENTSESSIONEND, Gc.convertDate(session.sessionEndDate));
                        setDefaults.put(Gc.CURRENTSESSION, session.session);

                    }
                }
                Gc.setSharedPreference(setDefaults, this);

                new Thread(() -> {
                    mDb.sessionListModel().insertSessionList(sessionListEntities);
                }).start();
            }


            final ArrayList<FeeTypeEntity> feeTypeEntities = new Gson()
                    .fromJson(resultjobj.optString("feeTypes"),
                            new TypeToken<List<FeeTypeEntity>>() {
                            }.getType());

            if (feeTypeEntities != null) {

                if (feeTypeEntities.size() > 0) {
                    setDefaults.put(Gc.FEETYPEEXISTS, Gc.EXISTS);
                    Gc.setSharedPreference(setDefaults, this);
                }


                new Thread(() -> {
                    mDb.feeTypeModel().deleteAll();
                    mDb.feeTypeModel().insertFeeTypes(feeTypeEntities);

                }).start();

            }

            final ArrayList<String> orgPermissions = new Gson()
                    .fromJson(resultjobj.optString("organizationPermissions"),
                            new TypeToken<List<String>>() {
                            }.getType());

            if (orgPermissions != null) {
                new Thread(() -> {

                    ArrayList<OrganizationPermissionEntity> orgpar = new ArrayList<>();
                    for (String s : orgPermissions) {
                        orgpar.add(new OrganizationPermissionEntity(s));
                    }
                    mDb.organizationPermissionModel().deleteAll();
                    mDb.organizationPermissionModel().insertOrganizationPermission(orgpar);
                }).start();
            }


            ArrayList<SchoolClasses> schoolClassesArrayList = new Gson()
                    .fromJson(resultjobj.optString("classSections"),
                            new TypeToken<List<SchoolClasses>>() {
                            }.getType());
            final ArrayList<SchoolClassEntity> classEntities = new Gson()
                    .fromJson(resultjobj.optString("classSections"),
                            new TypeToken<List<SchoolClassEntity>>() {
                            }.getType());

            final ArrayList<SchoolClassSectionEntity> sectionEntities = new ArrayList<>();

            for (SchoolClasses cl : schoolClassesArrayList) {

                sectionEntities.addAll(cl.section);
            }

            if (classEntities.size() > 0) {
                setDefaults.put(Gc.CLASSEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
            }

            new Thread(() -> {
                mDb.schoolClassModel().deleteAll();
                mDb.schoolClassSectionModel().deleteAll();
                mDb.schoolClassModel().insertSchoolClasses(classEntities);
                mDb.schoolClassSectionModel().insertSchoolClassSection(sectionEntities);
            }).start();


            JSONArray classFeesjarr = resultjobj.getJSONArray("classFees");
            if (classFeesjarr.length() > 0) {
                setDefaults.put(Gc.CLASSFEESEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);

                final ArrayList<ClassFeesEntity> classFeesEntities = MapModelToEntity.mapClassFeeModelToEntity(classFeesjarr);
                final ArrayList<ClassFeesTransportEntity> classFeesTransportEntities = MapModelToEntity.mapClassFeeTransportToEntity(classFeesjarr);

                new Thread(() -> {
                    mDb.classFeesModel().deleteAll();
                    mDb.classFeesTransportModel().deleteAll();
                    mDb.classFeesModel().insertClassFee(classFeesEntities);
                    mDb.classFeesTransportModel().insertClassFee(classFeesTransportEntities);
                }).start();
            }


            final ArrayList<TransportFeeAmountEntity> transportAmounts = new Gson()
                    .fromJson(resultjobj.getJSONObject("transportFeeAmount").optString("Fees"),
                            new TypeToken<List<TransportFeeAmountEntity>>() {
                            }.getType());

            if (transportAmounts != null && transportAmounts.size() > 0) {
                String transportFeeType = resultjobj.getJSONObject("transportFeeAmount").optString("Type");

                if (!(Strings.nullToEmpty(transportFeeType).equalsIgnoreCase(""))) {
                    setDefaults.put(Gc.TRANSPORTFEETYPE, transportFeeType);
                } else {
                    setDefaults.put(Gc.TRANSPORTFEETYPE, Gc.NOTFOUND);
                }
                Gc.setSharedPreference(setDefaults, this);

                new Thread(() -> {
                    mDb.transportFeeAmountModel().deleteAll();
                    mDb.transportFeeAmountModel().insertTransportFee(transportAmounts);
                }).start();
            }

            final ArrayList<SchoolSubjectsEntity> subjectList = new Gson()
                    .fromJson(resultjobj.optString("subjects"),
                            new TypeToken<List<SchoolSubjectsEntity>>() {
                            }.getType());

            if (subjectList != null && subjectList.size() > 0) {
                setDefaults.put(Gc.SUBJECTEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
                new Thread(() -> {
                    mDb.schoolSubjectsModel().insertSubject(subjectList);
                }).start();
            }


            final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClass =
                    new Gson().fromJson(resultjobj.optString("subjectSectionMapping"), new TypeToken<List<SchoolSubjectsClassEntity>>() {
                    }.getType());

            if (schoolSubjectsClass != null && schoolSubjectsClass.size() > 0) {

                setDefaults.put(Gc.SUBJECTCLASSEXISTS, Gc.EXISTS);

                new Thread(() -> {
                    mDb.schoolSubjectsClassModel().insertSubJectClassMapping(schoolSubjectsClass);
                }).start();
            }

            final ArrayList<SchoolAccountsEntity> schoolAccountsEntities = new Gson()
                    .fromJson(resultjobj.optString("accounts"),
                            new TypeToken<List<SchoolAccountsEntity>>() {
                            }.getType());

            if (schoolAccountsEntities != null && schoolAccountsEntities.size() > 0) {
                setDefaults.put(Gc.ACCOUNTEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
                new Thread(() -> {
                    mDb.schoolAccountsModel().insertSchoolAccount(schoolAccountsEntities);
                }).start();
            }


            final ArrayList<MasterEntryEntity> masterEntries = new Gson()
                    .fromJson(resultjobj.optString("masterEntry"),
                            new TypeToken<List<MasterEntryEntity>>() {
                            }.getType());

            if (masterEntries != null && masterEntries.size() > 0) {
                new Thread(() -> {
                    mDb.masterEntryModel().deleteAll();
                    mDb.masterEntryModel().insertMasterEntries(masterEntries);
                }).start();
            }


            final ArrayList<ExamTypeEntity> examTypes = new Gson()
                    .fromJson(resultjobj.optString("examTypes"),
                            new TypeToken<List<ExamTypeEntity>>() {
                            }.getType());

            if (examTypes != null && examTypes.size() > 0) {
                new Thread(() -> {
                    mDb.examTypeModel().deleteAll();
                    mDb.examTypeModel().insertExamTypes(examTypes);
                }).start();
            }

            final ArrayList<String> userPer = new Gson()
                    .fromJson(resultjobj.optString("userPermissions"), new TypeToken<List<String>>() {
                    }.getType());

            if (userPer != null) {

                Gc.saveArrayList(userPer, Gc.SIGNEDINUSERPERMISSION, this);

                new Thread(() -> {
                    ArrayList<UserPermissionsEntity> userPermissions = new ArrayList<>();
                    for (String s : userPer) {
                        userPermissions.add(new UserPermissionsEntity(s));
                    }
                    mDb.userPermissionsModel().deleteAll();
                    mDb.userPermissionsModel().insertUserPermission(userPermissions);
                }).start();
            }

            final ArrayList<SchoolCalenderEntity> schoolCalenderEntities = new Gson()
                    .fromJson(resultjobj.optString("calendar"),
                            new TypeToken<List<SchoolCalenderEntity>>() {
                            }.getType());

            if (schoolCalenderEntities != null && schoolCalenderEntities.size() > 0) {
                setDefaults.put(Gc.CALENDEREXISTS, Gc.EXISTS);
                new Thread(() -> {
                    mDb.schoolCalenderModel().insertSchoolCalender(schoolCalenderEntities);
                }).start();
            }

            final ArrayList<CommunicationPermissions> communicationPermissions =
                    new Gson().fromJson(resultjobj.optString("communicationPermissions"),
                            new TypeToken<List<CommunicationPermissions>>() {
                            }.getType());

            if (communicationPermissions != null) {
                new Thread(() -> {
                    mDb.communicationPermissionsModel().deleteAll();
                    mDb.communicationPermissionsModel().insertCommunicationPermissions(communicationPermissions);

                }).start();

            }
            Gc.setSharedPreference(setDefaults, this);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getStudents() {
        final JSONObject param = new JSONObject();
        try {
            param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentRegistration/"
                + param;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:

                    AsyncTaskRunner runner = new AsyncTaskRunner();
                    AppRequestQueueController.getInstance(getApplicationContext()).addToAsyncTask(runner);
                    runner.execute(job);

                    break;

                default:

                    Log.e("AdminNavNew", "Default case non 200");
            }
        }, error -> {
            Log.e("AdminNavNew", "Volley Error in get students");


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void getStaff() {

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "staff/staffRegistration";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        JSONArray staffjarr = job.getJSONObject("result").getJSONArray("staffDetails");
                        final ArrayList<SchoolStaffEntity> schoolStaffEntities =
                                new Gson().fromJson(staffjarr.toString(), new TypeToken<List<SchoolStaffEntity>>() {
                                }
                                        .getType());

                        new Thread(() -> {
                            mDb.schoolStaffModel().insertStaff(schoolStaffEntities);
                        }).start();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    Log.e("Saving staff failed", job.optString("message"));

            }
        }, error -> {
            Log.e("Saving staff failed", Objects.requireNonNull(error.getMessage()));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private class AsyncTaskRunner extends AsyncTask<JSONObject, String, String> {
        ArrayList<SchoolStudentEntity> schoolStudentEntities = new ArrayList<>();

        @Override
        protected String doInBackground(JSONObject... job) {

            try {
                JSONObject resultjobj = job[0].getJSONObject("result");
                final JSONArray studentsjarr = resultjobj.getJSONArray("studentDetails");

                schoolStudentEntities = new Gson().fromJson(studentsjarr.toString(), new TypeToken<List<SchoolStudentEntity>>() {
                }.getType());
                Log.i("Student are", schoolStudentEntities.size() + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new Thread(() -> {
                if (!isCancelled()) {
//                        mDb.schoolStudentModel().deleteAll();
                    mDb.schoolStudentModel().insertStudents(schoolStudentEntities);
                }
            }).start();
        }
    }

    private void showUpdateApp() {
        AlertDialog.Builder alertAdmin = new AlertDialog.Builder(this);
        alertAdmin.setIcon(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_download,null)) ;
        alertAdmin.setTitle(getString(R.string.update_app));

        alertAdmin.setMessage(getString(R.string.app_update_message));
        alertAdmin.setCancelable(false);
        alertAdmin.setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW);

                intent.setData(Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID));
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });

        Log.e("Force update App", firebaseRemoteConfig.getString("force_update_app") + " empty");

        String force_update = firebaseRemoteConfig.getString("force_update_app");

        if (!(force_update.equalsIgnoreCase(Gc.TRUE)))
            alertAdmin.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        if (!(this).isFinishing() || !isOnPause)
            alertAdmin.show();
    }

    void setPermissionsInSharedPreference() {

        HashMap<String, String> setDefaults = new HashMap<>();
        if (userPermissionsList.contains(Gc.CreateClass))
            setDefaults.put(Gc.CLASSSECTIONEDITALLOWED, Gc.TRUE);
        else if (userPermissionsList.contains(Gc.DisplayClass)) {
            setDefaults.put(Gc.CLASSSECTIONEDITALLOWED, Gc.FALSE);
            setDefaults.put(Gc.CLASSSECTIONDISPLAYALLOWED, Gc.TRUE);
        }
        if (userPermissionsList.contains(Gc.FeesSetup))
            setDefaults.put(Gc.FEESETUPALLOWED, Gc.TRUE);
        else
            setDefaults.put(Gc.FEESETUPALLOWED, Gc.FALSE);

        if (userPermissionsList.contains(Gc.FeesPayment))
            setDefaults.put(Gc.FEEPAYMENTALLOWED, Gc.TRUE);
        else if (userPermissionsList.contains(Gc.DisplayFees)) {
            setDefaults.put(Gc.FEEDISPLAYALLOWED, Gc.TRUE);
            setDefaults.put(Gc.FEEPAYMENTALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.CreateHomework))
            setDefaults.put(Gc.HOMEWORKCREATEALLOWED, Gc.TRUE);
        else if (userPermissionsList.contains(Gc.DisplayHomework)) {
            setDefaults.put(Gc.HOMEWORKCREATEALLOWED, Gc.FALSE);
            setDefaults.put(Gc.HOMEWORKDISPLAYALLOWED, Gc.TRUE);
        }

        if (userPermissionsList.contains(Gc.CreateSubject))
            setDefaults.put(Gc.SUBJECTCREATEALLOWED, Gc.TRUE);
        else if (userPermissionsList.contains(Gc.DisplaySubject)) {
            setDefaults.put(Gc.SUBJECTCREATEALLOWED, Gc.FALSE);
            setDefaults.put(Gc.SUBJECTDISPLAYALLOWED, Gc.TRUE);
        }

        if (userPermissionsList.contains(Gc.StudentSetup)) {
            setDefaults.put(Gc.STUDENTSETUPALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.STUDENTSETUPALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.EditGallery)) {
            setDefaults.put(Gc.GALLERYEDITALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.GALLERYEDITALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.CreateStudentReview)) {
            setDefaults.put(Gc.STUDENTREVIEWLOGCREATEALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.STUDENTREVIEWLOGCREATEALLOWED, Gc.FALSE);
        }
        if (userPermissionsList.contains(Gc.CreateStaffReview)) {
            setDefaults.put(Gc.STAFFREVIEWLOGCREATEALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.STAFFREVIEWLOGCREATEALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.SendMessage)) {
            setDefaults.put(Gc.SENDMESSAGEALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.SENDMESSAGEALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.Noticeboard)){
            setDefaults.put(Gc.CREATENOTICEALLOWED, Gc.TRUE);
        } else{
            setDefaults.put(Gc.CREATENOTICEALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.StaffSetup)) {
            setDefaults.put(Gc.STAFFSETUPALLOWED, Gc.TRUE);
        } else if (userPermissionsList.contains(Gc.DisplayStaff)) {
            setDefaults.put(Gc.STAFFSETUPALLOWED, Gc.FALSE);
            setDefaults.put(Gc.STAFFDISPLAYALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.STAFFSETUPALLOWED, Gc.FALSE);
            setDefaults.put(Gc.STAFFDISPLAYALLOWED, Gc.FALSE);
        }

        if (userPermissionsList.contains(Gc.CreateEventsCalendar)) {
            setDefaults.put(Gc.CREATEEVENTALLOWED, Gc.TRUE);
        } else {
            setDefaults.put(Gc.CREATEEVENTALLOWED, Gc.FALSE);
        }

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
            setDefaults.put(Gc.FEEPAYMENTALLOWED, Gc.TRUE);
            setDefaults.put(Gc.CLASSSECTIONEDITALLOWED, Gc.TRUE);
            setDefaults.put(Gc.FEESETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.HOMEWORKCREATEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.SUBJECTCREATEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.STUDENTSETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.GALLERYEDITALLOWED, Gc.TRUE);
            setDefaults.put(Gc.SENDMESSAGEALLOWED, Gc.TRUE);
            setDefaults.put(Gc.STAFFSETUPALLOWED, Gc.TRUE);
            setDefaults.put(Gc.CREATEEVENTALLOWED, Gc.TRUE);
            setDefaults.put(Gc.CREATENOTICEALLOWED, Gc.TRUE);
        }

        Gc.setSharedPreference(setDefaults, this);
    }

}