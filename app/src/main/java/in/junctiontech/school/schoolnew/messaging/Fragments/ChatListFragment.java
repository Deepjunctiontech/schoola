package in.junctiontech.school.schoolnew.messaging.Fragments;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.messaging.ConversationListActivity;
import in.junctiontech.school.schoolnew.messaging.ChatListAdapter;
import in.junctiontech.school.R;


/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class ChatListFragment extends
        Fragment implements SearchView.OnQueryTextListener {
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private SwipeRefreshLayout swipe_refresh_list;
    private MainDatabase mDb;
    private ArrayList<ChatListEntity>chatList;
    private ChatListAdapter adapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int  colorIs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data =  convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available =  convertView.findViewById(R.id.tv_view_all_data_not_data_available);
        swipe_refresh_list =  convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.heading, R.color.back);

        setHasOptionsMenu(true);
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        chatList = (ArrayList<ChatListEntity>) mDb.chatListModel().getRecentChats();

        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();

        if (chatList.size()==0)
        {
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
            tv_view_all_data_not_data_available.setText(getString(R.string.no_conversations_available));
        }else tv_view_all_data_not_data_available.setVisibility(View.GONE);


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent) {

                if (intent.getAction().equals(Config.chatMessage)) {
Log.e("BroadcastWork","ritu");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                            addToChatList((ChatList) intent.getSerializableExtra(Config.chatListObj));
                            }

                        }, 5);

                }
            }

        };
    }


    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);

        adapter = new ChatListAdapter(getContext(), chatList,colorIs);
        recycler_view_list_of_data.setAdapter(adapter);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = MainDatabase.getDatabase(getContext());
        colorIs = Config.getAppColor(getActivity(),false);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setFilter(ArrayList<ChatListEntity> chatList) {
       adapter.setFilter(chatList);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_chat, menu);


        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));
//
//        final MenuItem item = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(this);

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                setFilter(chatList);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                setFilter(chatList);

                return true;
            }
        });
//
//        MenuItemCompat.setOnActionExpandListener(item,
//                new MenuItemCompat.OnActionExpandListener() {
//                    @Override
//                    public boolean onMenuItemActionCollapse(MenuItem item) {
//// Do something when collapsed
//                         setFilter(chatList);
//                        return true; // Return true to collapse action view
//                    }
//
//                    @Override
//                    public boolean onMenuItemActionExpand(MenuItem item) {
//// Do something when expanded
//                        return true; // Return true to expand action view
//                    }
//                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id==R.id.menu_new_chat) {
            ((ConversationListActivity)getActivity()).setTabContacts();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<ChatListEntity> filteredModelList = filter(chatList, newText);
        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<ChatListEntity> filter(ArrayList<ChatListEntity> models, String query) {
        query = query.toLowerCase();
        final ArrayList<ChatListEntity> filteredModelList = new ArrayList<>();

        for (ChatListEntity model : models) {

            if (model.withType.toLowerCase().contains(query)||
                    model.withType.toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void updateChatList(ChatListEntity chatObj) {
        boolean alreadyExist = false;
        for (ChatListEntity model : chatList) {

            if (model.withId.equals(chatObj.withId)) {
                chatList.set(chatList.indexOf(model),chatObj);
                alreadyExist=true;
            }
        }

        if(!alreadyExist){
           chatList.add(chatObj);
        }
        adapter.setFilter(chatList);

        if (chatList.size()==0)
        {
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
            tv_view_all_data_not_data_available.setText(getString(R.string.no_conversations_available));
        }else tv_view_all_data_not_data_available.setVisibility(View.GONE);
    }

//    public void increaseCounterMessage(ChatList chatObj) {
//        for (ChatList model : this.chatList) {
//            if (model.getGroupId()==chatObj.getGroupId()) {
//              int chatCounter=  model.getUnReadChatCounter()+1;
//                chatObj.setUnReadChatCounter(chatCounter);
//                this.chatList.set(this.chatList.indexOf(model),chatObj);
//            }
//         }
//
//        adapter.setFilter(this.chatList);
//        adapter.notifyDataSetChanged();
//    }

//    public void addToChatList(ChatListEntity chatObj) {
//if (chatObj!=null ) {
//    boolean alreadyExist = false;
//    for (ChatListEntity model : chatList) {
//Log.e("getGroupId",chatObj.getGroupId()+ " ritu");
//        if (model != null && chatObj.getGroupId()!=-1 && model.getGroupId() == chatObj.getGroupId()) {
//            int chatCounter = model.getUnReadChatCounter() + 1;
//            //Log.e("chatCounter", chatCounter + " counter");
//           int indexOf = chatList.indexOf(model);
//            chatObj.setUnReadChatCounter(chatCounter);
//            chatList.set(indexOf, chatObj);
//            adapter.notifyItemChanged(indexOf);
//            alreadyExist = true;
//        }
//    }
//
//    if (!alreadyExist) {
//        chatObj.setUnReadChatCounter(1);
//        chatList.add(chatObj);
//        adapter.setFilter(chatList);
//    }
//
//}
//    }

    @Override
    public void onResume() {
        chatList = new  ArrayList<ChatListEntity>();
//        chatList = (ArrayList<ChatListEntity>) mDb.chatListModel().getRecentChats();
        setFilter(chatList);
        Log.e("ChatListFragment", "onResume");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.chatMessage));
        super.onResume();
    }


    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
