package in.junctiontech.school.schoolnew.registrationprocess;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;

public class GoogleSignInActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 232;
    private GoogleApiClient mGoogleApiClient;
    private ImageView gmail_profile_image;
    private ProgressDialog progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_sign_in);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
// Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

// Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.show();
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);


            }
        });

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        progressbar.cancel();

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("ZEROERP", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.i("ZEROERP", acct.getEmail() + " :  " + acct.getDisplayName());


            checkEmailExists(acct);
        } else {
            // Signed out, show unauthenticated UI.
//            progressbar.dismiss();
            Log.e("ZEROERP", "unauthenticated" + result.getStatus().getStatusMessage());

            AlertDialog.Builder alert = new android.app.AlertDialog.Builder(GoogleSignInActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alert.setTitle(getString(R.string.information));
            alert.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_help));
            alert.setCancelable(false);
            alert.setMessage(result.getStatus().getStatusMessage() + getString(R.string.please_check_your_internet_connectivity));
            alert.setPositiveButton(getString(R.string.retry), null);
            alert.show();
        }
    }

    private void checkEmailExists(final GoogleSignInAccount acct){
        progressbar.show();

        JSONObject p = new JSONObject();
        try {
            p.put("email", acct.getEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "registration/registration/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");
                progressbar.cancel();

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            AlertDialog.Builder alert = new android.app.AlertDialog.Builder(GoogleSignInActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                            alert.setTitle(getString(R.string.information));
                            alert.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_help));
                            alert.setCancelable(false);
                            alert.setMessage(getString(R.string.this_email_id_already_exist_try_other_one));
                            alert.setPositiveButton(getString(R.string.retry), null);
                            alert.show();
                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                    new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(Status status) {

                                        }
                                    });



//                                     Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        if (acct!=null)
                            openSuccessMessage(acct);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.cancel();
                Log.e("ErrorResponse", error.toString());

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);


                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    private void openSuccessMessage(final GoogleSignInAccount acct) {
        final Animation fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        //    Animation  fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);



        (findViewById(R.id.rl_google_sign_in_success)).setVisibility(View.VISIBLE);
         findViewById(R.id.ll_google_sign_in_success).startAnimation(fab_open);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(GoogleSignInActivity.this, RegistrationSchoolActivity.class);
                intent.putExtra("email", null==acct.getEmail()?"":acct.getEmail());
                (Prefs.with(GoogleSignInActivity.this).getSharedPreferences()).edit()
                        .putString("AdminProfileUrl", null==acct.getPhotoUrl()?"":acct.getPhotoUrl().toString()
                ).putString("AdminProfileName", null==acct.getDisplayName()?"": acct.getDisplayName()).apply();
//                intent .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        }, 3000);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Failed to Google Login", Toast.LENGTH_LONG).show();
    }

    public void otherRegisterMethod(View view) {
        startActivity(new Intent(this,
                UserVerificationActivity.class).putExtra("choice", false));
        overridePendingTransition(R.anim.enter, R.anim.nothing);
    }

    @Override
    public void onBackPressed() {
        if ((findViewById(R.id.rl_google_sign_in_success)).getVisibility()!=View.VISIBLE) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if ((findViewById(R.id.rl_google_sign_in_success)).getVisibility()!=View.VISIBLE) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }else return false;
    }

    private void emailVerify(final GoogleSignInAccount acct) {
        Log.e("EmaiVerifyUrl", Config.sendEmail);
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("email", acct.getEmail());


        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        Log.e("EmaiVerifyUrl", (new JSONObject(param1).toString()));

        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                Config.ORGANIZATION_ID_CHECK + "RegistrationApi.php?" +
                        "data=" + (new JSONObject(param1)),
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        Log.e("NumberVeryResponse", s + "    ZeroERP");

                        progressbar.cancel();
                        if (!s.equalsIgnoreCase("")) {
                            try {

                                JSONObject jsonObject = new JSONObject(s);


                                if (jsonObject.optInt("code") == 200  ) {
                                    AlertDialog.Builder alert = new android.app.AlertDialog.Builder(GoogleSignInActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                                    alert.setTitle(getString(R.string.information));
                                    alert.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_help));
                                    alert.setCancelable(false);
                                    alert.setMessage(getString(R.string.this_email_id_already_exist_try_other_one));
                                    alert.setPositiveButton(getString(R.string.retry), null);
                                    alert.show();
                                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                            new ResultCallback<Status>() {
                                                @Override
                                                public void onResult(Status status) {

                                                }
                                            });



                                } else {
                                    if (acct!=null)
                                        openSuccessMessage(acct);
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressbar.cancel();
                Log.e("ErrorResponse", volleyError.toString());
                // if (!Config.checkInternet(context)) showAlertOfInternet();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    Toast.makeText(GoogleSignInActivity.this, getString(R.string.internet_not_available_please_check_your_internet_connectivity), Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();


                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);


    }

}
