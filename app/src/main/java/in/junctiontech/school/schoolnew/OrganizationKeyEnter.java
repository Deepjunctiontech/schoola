package in.junctiontech.school.schoolnew;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.registration.GeneralInfoActivity;


public class OrganizationKeyEnter extends AppCompatActivity {

    static public SharedPreferences sp;

    private EditText instituteCode;

    private MainDatabase mDb;

    private boolean isPermissionDialogOpen = false;

    private ProgressDialog progressDialog;
    ConstraintLayout constraintLayout;
    private String registeredusername, registeredpassword, LoginOptions;

    Button btn_instcode_enter, btn_find_institute;

    ImageView organizationLogo;
    String invitationType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        sp = Prefs.with(this).getSharedPreferences();
        mDb = MainDatabase.getDatabase(getApplicationContext());

        setContentView(R.layout.activity_organization_key_enter);

        organizationLogo = findViewById(R.id.organizationLogo);

        organizationLogo.setImageDrawable(getDrawable(R.mipmap.ic_launcher_foreground));

        constraintLayout = findViewById(R.id.top_layout);
        /*   For Background display */
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            constraintLayout.setBackgroundResource(R.drawable.ic_background_landscape);
        else constraintLayout.setBackgroundResource(R.drawable.ic_background_verticle);


        instituteCode = this.findViewById(R.id.instituteCode);
        instituteCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().matches("[a-zA-Z0-9]+")) {
                    instituteCode.setError(getString(R.string.enter_alphanumeric_characters), getDrawable(R.drawable.ic_alert));
                }
            }
        });

        btn_instcode_enter = findViewById(R.id.btn_instcode_enter);
        btn_instcode_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        btn_find_institute = findViewById(R.id.btn_find_institute);
        btn_find_institute.setOnClickListener(view -> {
            Intent intent = new Intent(OrganizationKeyEnter.this, GeneralInfoActivity.class);
            intent.putExtra("findInstitute", "findInstitute");
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));

//        LanguageSetup.changeLang(this, (getSharedPreferences("APP_LANG", Context.MODE_PRIVATE)).getString("app_language", ""));

        invitationType = getIntent().getStringExtra("invitationType");

        if (getIntent().getBooleanExtra("organizationKeyAvailable", false)) {
            instituteCode.setText(getIntent().getStringExtra("organizationKey"));
            submit();
        }

        if (getIntent().getBooleanExtra("dataAvailable", true)) {
            instituteCode.setText(getIntent().getStringExtra("organizationKey"));
            registeredusername = getIntent().getStringExtra("username");
            registeredpassword = getIntent().getStringExtra("password");
        }

        if (Gc.TRUE.equalsIgnoreCase(getIntent().getStringExtra("Referrer"))) {
            instituteCode.setText(getIntent().getStringExtra("organizationKey"));
            submit();
        }

    }

    void flavourImage() {
        switch (BuildConfig.APPLICATION_ID) {
            case "com.zeroerp.uppublic":
                organizationLogo.setImageDrawable(getDrawable(R.drawable.ic_school));
                break;

            case "com.zeroerp.uppre":

                break;

        }
    }

    public void submit() {

        if (!isEmptyOrganization()) {
            this.getHostNameFromServer();
        } else {
            instituteCode.setError(getString(R.string.Institute_Code_can_not_be_blank));
        }

    }

    private void getHostNameFromServer() {

        progressDialog.setMessage(getString(R.string.searching_organization));
        progressDialog.show();

        String hostNameUrl = Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "hostUrl/host/"
                + instituteCode.getText().toString().trim().toUpperCase();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, hostNameUrl, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.cancel();
                        String code = jsonObject.optString("code");
                        switch (code) {
                            case "200":
                                try {
                                    JSONObject job = jsonObject.getJSONObject("result")
                                            .getJSONObject("organizationDetails");

                                    HashMap<String, String> setDefaults = new HashMap<>();

                                    setDefaults.put(Gc.ERPHOSTAPIURL, job.optString("apiurl"));
                                    setDefaults.put(Gc.ERPWEBHOSTURL, job.optString("hosturl"));
                                    setDefaults.put(Gc.ERPLICENCEEXPIRY, job.optString("licenceExpiry"));
                                    setDefaults.put(Gc.ERPINSTCODE, job.optString("organizationKey"));
                                    setDefaults.put(Gc.ERPDBNAME, job.optString("db_name"));
                                    setDefaults.put(Gc.ERPPLANTYPE, job.optString("planType"));
                                    setDefaults.put(Gc.ERPINSTTYPE, job.optString("institutionType"));
                                    setDefaults.put(Gc.ERPADDVERTISEMENTSTATUS, job.optString("addStatus"));
                                    setDefaults.put(Gc.ERPAPPLICATIONSTATUS, job.optString("applicationStatus"));
                                    Gc.setSharedPreference(setDefaults, OrganizationKeyEnter.this);

                                    /*                                     *  *********************If Coming from Registration
                                     *  ************Go straight to Signup ********/

//                                    Intent intent = new Intent(OrganizationKeyEnter.this, SignUpActivityNew.class);
//                                    intent.putExtra(Gc.FROMDEMO,false);
//                                        intent.putExtra("dataAvailable", true);
//                                        intent.putExtra("organizationKey", instituteCode.getText().toString());
//                                        intent.putExtra("username", registeredusername);
//                                        intent.putExtra("password", registeredpassword);
//                                        startActivity(intent);
//                                        finish();

                                    // *  *If Not Coming from Registration get public details
                                    getSchoolPublicDetails();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            case "204":
                                instituteCode.setError(jsonObject.optString("message"));
                                break;

                            default:
                                Log.e("Error Fetching Host", code + ":" + jsonObject.optString("message"));
                                instituteCode.setError(jsonObject.optString("message"));

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                VolleyLog.e("Error: ", error.getMessage());
                Config.responseVolleyErrorHandler(OrganizationKeyEnter.this, error, findViewById(R.id.top_layout));

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }


    private void getSchoolPublicDetails() {
        if (!isFinishing()) {
            progressDialog.setMessage(getString(R.string.loading) + getString(R.string.school_information));
            progressDialog.show();
        }
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "public/schoolPublicDetails";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(),
                job -> {
                    progressDialog.cancel();

                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200:
                            try {
                                JSONObject resultjobj = job.getJSONObject("result");

                                JSONObject schoolDetailsjobj = resultjobj.getJSONObject("schoolDetails");
                                LoginOptions = resultjobj.getJSONObject("schoolDetails").optString("LoginOptions");

                                HashMap<String, String> setDefault = new HashMap<>();
                                setDefault.put(Gc.LOGIN_OPTIONS, Strings.nullToEmpty(LoginOptions));

                                Gc.setSharedPreference(setDefault, OrganizationKeyEnter.this);


                                final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(schoolDetailsjobj.toString(), SchoolDetailsEntity.class);
                                if (!(schoolDetailsEntity.SchoolName == null)) {
                                    HashMap<String, String> setDefaults = new HashMap<>();
                                    setDefaults.put(Gc.LOGO_URL, Strings.nullToEmpty(schoolDetailsEntity.Logo));
                                    setDefaults.put(Gc.SCHOOLIMAGE, Strings.nullToEmpty(schoolDetailsEntity.schoolImage));
                                    setDefaults.put(Gc.SCHOOLNAME, schoolDetailsEntity.SchoolName);
                                    Gc.setSharedPreference(setDefaults, OrganizationKeyEnter.this);

                                    SharedPreferences spColor = getApplicationContext().getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);

                                    if (spColor.getInt(Config.APP_COLOR, 0) != Integer.parseInt(schoolDetailsEntity.themeColor)) {
                                        spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();

                                    } else
                                        spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);
                                        }
                                    }).start();

                                }


//                                    JSONArray galleryJarr = resultjobj.getJSONArray("galleries");
//                                    JSONArray holidayJarr = resultjobj.getJSONArray("holidays");

                                JSONArray noticeJarr = resultjobj.getJSONArray("noticeBoards");
                                final ArrayList<NoticeBoardEntity> noticeBoardEntities = new Gson().fromJson(noticeJarr.toString(), new TypeToken<List<NoticeBoardEntity>>() {
                                }.getType());
                                if (noticeBoardEntities.size() > 0) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mDb.noticeBoardModel().insertNotice(noticeBoardEntities);
                                        }
                                    }).start();
                                }


                                HashMap<String, String> setDefaults = new HashMap<>();

                                setDefaults.put(Gc.GALLERYDATA, resultjobj.optString("galleries"));
                                Gc.setSharedPreference(setDefaults, OrganizationKeyEnter.this);


                                Intent intent = new Intent(OrganizationKeyEnter.this, SignUpActivityNew.class);
                                intent.putExtra(Gc.FROMDEMO, false);


                                intent.putExtra("invitationType", invitationType);

                                if (getIntent().getBooleanExtra("dataAvailable", true)) {
                                    intent.putExtra("dataAvailable", true);
                                    intent.putExtra("organizationKey", instituteCode.getText().toString());
                                    intent.putExtra("username", registeredusername);
                                    intent.putExtra("password", registeredpassword);
                                }
                                startActivity(intent);
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;

                        case "204":
                            Intent intent = new Intent(OrganizationKeyEnter.this, SignUpActivityNew.class);
//                                intent.putExtra(Gc.FROMDEMO,false);
                            if (getIntent().getBooleanExtra("dataAvailable", false)) {
                                intent.putExtra("dataAvailable", true);
//                                    intent.putExtra("organizationKey", instituteCode.getText().toString());
                                intent.putExtra("username", registeredusername);
                                intent.putExtra("password", registeredpassword);
                            }
                            startActivity(intent);
                            finish();
                            break;

                        case Gc.APIRESPONSE500:
                            AlertDialog.Builder alert1 = new AlertDialog.Builder(OrganizationKeyEnter.this);


                            alert1.setMessage(Html.fromHtml("<Br>" + "Your licence is expired !<Br>For further usage contact on<Br>"
                                    + "<B>Email</B> : <font color='#ce001f'>info@zeroerp.com</font><Br><B>Or Visit</B> :<font color='#ce001f'>www.zeroerp.com</font>"));
                            alert1.setTitle(getString(R.string.information));
                            alert1.setIcon(R.drawable.ic_clickhere);
                            alert1.setPositiveButton(getString(R.string.logout), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
//                                alert1.setCancelable(false);
                            alert1.show();
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.cancel();
//                        VolleyLog.e("Error: ", error.getMessage());
                        Config.responseVolleyErrorHandler(OrganizationKeyEnter.this, error, findViewById(R.id.top_layout));

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(req);
    }


    private boolean isEmptyOrganization() {
        return instituteCode.getText() == null || instituteCode.getText().toString() == null
                || instituteCode.getText().toString().equals("") || instituteCode.getText().toString().isEmpty();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Gc.removeAll(this);
        new Thread(() -> mDb.clearAllTables()).start();

        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (constraintLayout != null)
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
                constraintLayout.setBackgroundResource(R.drawable.ic_background_landscape);
            else constraintLayout.setBackgroundResource(R.drawable.ic_background_verticle);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onDestroy();

    }
}

