package in.junctiontech.school.schoolnew.DB.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import java.util.List;

import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.DB.repository.SchoolClassRepository;

public class SchoolClassViewModel extends AndroidViewModel {
    private SchoolClassRepository mSchoolClassRepository;

    private LiveData<List<SchoolClassEntity>> mAllClasses;

    public SchoolClassViewModel(@NonNull Application application ) {
        super(application);
        mSchoolClassRepository = new SchoolClassRepository(application);
        mAllClasses = mSchoolClassRepository.getSchoolClassesLive();
    }


    public LiveData<List<SchoolClassEntity>> getSchoolClassesLive(){
        return  mAllClasses;
    }
    public void insert(SchoolClassEntity[] schoolClassEntities){
        mSchoolClassRepository.insert(schoolClassEntities);
    }
    public String getClassID(String className){
        return mSchoolClassRepository.getClassID(className);

    }
}
