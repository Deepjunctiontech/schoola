package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolUserEntity {
    @PrimaryKey
    @NonNull
    public String UserId;
    public String StaffId;
    public String Username;
    public String UserType;
    public String gcmToken;
    public String IMEI;
    public String UserDevice;
    public String Status;
    public String DOE;
    public String DOL;
    public String Staff_terms;
    public String DOLUsername;
    public String StaffName;
    public String UserTypeValue;
}
