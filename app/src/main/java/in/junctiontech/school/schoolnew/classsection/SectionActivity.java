package in.junctiontech.school.schoolnew.classsection;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.DB.SectionStudentCountTupple;
import in.junctiontech.school.schoolnew.DB.viewmodel.SchoolSectionViewModel;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class SectionActivity extends AppCompatActivity {

    MainDatabase mDb;
    SchoolSectionViewModel mSectionViewModel;

    TextView tv_class_name;

    private String classId;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    FloatingActionButton fb_section_add;

    private ProgressDialog progressbar;

    private ArrayList<SchoolClassSectionEntity> sectionNames = new ArrayList<>();
    HashMap<String, String > stCount = new HashMap<>();

    private int colorIs;
    private void setColorApp() {
        colorIs = Config.getAppColor(this,true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_section_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDb = MainDatabase.getDatabase(this);

        mSectionViewModel = ViewModelProviders.of(this).get(SchoolSectionViewModel.class);

        setContentView(R.layout.fragment_create_school_section);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        String className = Objects.requireNonNull(getIntent().getExtras()).getString("ClassName");
        classId = getIntent().getExtras().getString("ClassId");

        tv_class_name = findViewById(R.id.tv_class_name);

        tv_class_name.setText(className);

        fb_section_add = findViewById(R.id.fb_section_add);
        fb_section_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Gc.getSharedPreference(Gc.CLASSSECTIONEDITALLOWED, SectionActivity.this)
                        .equalsIgnoreCase(Gc.TRUE))
                showDialogForSectionCreate();
                else
                    Config.responseSnackBarHandler(getString(R.string.permission_denied),
                            findViewById(R.id.sectionlayout),
                            R.color.fragment_first_green);
            }
        });

         mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_school_class_section_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new SectionListAdapter(this, sectionNames,stCount);
        mRecyclerView.setAdapter(mAdapter);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3469610609", this, adContainer);
        }
    }

    void  showDialogForSectionCreate(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_update_class);
        dialog.setTitle(R.string.create_section);

        final EditText editText =  dialog.findViewById(R.id.et_class_name);
        editText.setHint(R.string.write_section_name);
        Button btnSave          =  dialog.findViewById(R.id.save);
        btnSave.setBackgroundColor(colorIs);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if("".equals(editText.getText().toString().trim().toUpperCase())){
                    editText.setError(getResources().getString(R.string.error_field_required),
                            getResources().getDrawable(R.drawable.ic_alert));
                    return;
                }

                try {
                    createNewSection(editText.getText().toString().trim().toUpperCase());
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        Button btnCancel        =  dialog.findViewById(R.id.cancel);
        btnCancel.setBackgroundColor(colorIs);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void createNewSection(String sectionName) throws JSONException {
        // Create a new class if Validation Succeeds
        progressbar.show();

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "classSection/manageSection";

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String parsedDate = formatter.format(new Date());

        final JSONObject param = new JSONObject();
        param.put("ClassId",classId);
        param.put("SectionName",sectionName.trim().toUpperCase());
        param.put("DOE", parsedDate);
        param.put("SectionStatus", Gc.ACTIVE);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressbar.cancel();
                        String code = job.optString("code");

                        switch (code){
                            case Gc.APIRESPONSE200:
                                try {
                                    JSONObject resultjobj = job.getJSONObject("result");
                                    final SchoolClassSectionEntity schoolClassSectionEntity = new Gson()
                                            .fromJson(resultjobj.getString("sections"),SchoolClassSectionEntity.class);

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ArrayList<SchoolClassSectionEntity> sc = new ArrayList<>();
                                            sc.add(schoolClassSectionEntity);
                                            mDb.schoolClassSectionModel().insertSchoolClassSection(sc);
                                            HashMap<String,String> setDefaults = new HashMap<String, String>();
                                            setDefaults.put(Gc.CLASSEXISTS, Gc.EXISTS);
                                            Gc.setSharedPreference(setDefaults,SectionActivity.this);
                                        }
                                    }).start();
                                    Config.responseSnackBarHandler(getString(R.string.success),
                                            findViewById(R.id.sectionlayout),
                                            R.color.fragment_first_green);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, SectionActivity.this);

                                Intent intent1 = new Intent(SectionActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, SectionActivity.this);

                                Intent intent2 = new Intent(SectionActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;


                             default:
                                 Config.responseSnackBarHandler(job.optString("message"),
                                         findViewById(R.id.sectionlayout),
                                         R.color.fragment_first_blue);                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.cancel();
                        Config.responseVolleyErrorHandler(SectionActivity.this,error,findViewById(R.id.sectionlayout));

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);

    }

    void updateSection(String sectionName, String sectionid, String sectionclassid) throws JSONException {
// Section update
        final Map<String, Object> urlparam = new LinkedHashMap<>();
        urlparam.put("SectionId",sectionid);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "classSection/manageSection/" + new JSONObject(urlparam);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String parsedDate = formatter.format(new Date());

        final JSONObject param = new JSONObject();
        param.put("ClassId",sectionclassid);
        param.put("SectionName",sectionName);
        param.put("DOE", parsedDate);
        param.put("SectionStatus", Gc.ACTIVE);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        Log.i("Success Update Section", job.toString());
                        String code = job.optString("code");

                        switch (code){
                            case Gc.APIRESPONSE200:
                                try {
                                    JSONObject resultjobj = job.getJSONObject("result");
                                    final SchoolClassSectionEntity schoolClassSectionEntity = new Gson()
                                            .fromJson(resultjobj.getString("sections"),SchoolClassSectionEntity.class);

                                    new Thread(() -> {
                                        mDb.schoolClassSectionModel().updateSchoolClassSection(schoolClassSectionEntity);
                                    }).start();
                                    Config.responseSnackBarHandler(getString(R.string.success),
                                            findViewById(R.id.sectionlayout),
                                            R.color.fragment_first_green);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, SectionActivity.this);

                                Intent intent1 = new Intent(SectionActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, SectionActivity.this);

                                Intent intent2 = new Intent(SectionActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                                default:
                                    Config.responseSnackBarHandler(job.optString("message"),
                                            findViewById(R.id.sectionlayout),
                                            R.color.fragment_first_blue);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                        Config.responseVolleyErrorHandler(SectionActivity.this,error,findViewById(R.id.sectionlayout));
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);
    }

    void deleteSection(String sectionId){

        progressbar.show();
        final Map<String, Object> urlparam = new LinkedHashMap<>();
        urlparam.put("SectionId", sectionId);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "classSection/classSection/" + new JSONObject(urlparam);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(),
                job -> {
                    progressbar.cancel();
                    String code = job.optString("code");

                    switch (code) {
                        case Gc.APIRESPONSE200:
                            new Thread(() -> {

                                mDb.schoolClassSectionModel().deleteSections(sectionId);
                                mDb.classFeesModel().deleteClassFeeForSection(sectionId);
                            }).start();

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.sectionlayout),
                                    R.color.fragment_first_green);
                            break;

                        case Gc.APIRESPONSE401:

                            HashMap<String, String> setDefaults1 = new HashMap<>();
                            setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults1, this);

                            Intent intent1 = new Intent(this, AdminNavigationDrawerNew.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);

                            finish();

                            break;

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, this);

                            Intent intent2 = new Intent(this, AdminNavigationDrawerNew.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);

                            finish();

                            break;

                        default:
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.sectionlayout),
                                    R.color.fragment_first_blue);
                    }
                },
                error -> {
                    progressbar.cancel();
                    VolleyLog.e("Class createError:", error.getMessage());
                    Config.responseVolleyErrorHandler(this, error, findViewById(R.id.sectionlayout));

                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(req);


    }

    @Override
    protected void onResume() {

        mSectionViewModel.getClassSection(classId).observe(this, new Observer<List<SchoolClassSectionEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolClassSectionEntity> schoolClassSectionEntities) {
                sectionNames.clear();
                sectionNames.addAll(schoolClassSectionEntities);
                mDb.schoolStudentModel().getNumberOfStudentsSection()
                        .observe(SectionActivity.this, new Observer<List<SectionStudentCountTupple>>() {
                    @Override
                    public void onChanged(@Nullable List<SectionStudentCountTupple> sectionStudentCountTupples) {
                        stCount.clear();
                        for(SectionStudentCountTupple st: sectionStudentCountTupples){
                            stCount.put(st.SectionId,st.stcount);
                        }

                        mDb.schoolStudentModel().getNumberOfStudentsSection().removeObservers(SectionActivity.this);
                        mAdapter.notifyDataSetChanged();

                    }
                });
                mAdapter.notifyDataSetChanged();
            }
        });

        if(sectionNames.size() == 0 ){
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.sectionlayout), R.string.create_section,
                    Snackbar.LENGTH_LONG);
            snackbarObj.setDuration(2000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_blue));
            snackbarObj.show();
//            Toast.makeText(this,"You must create one section",Toast.LENGTH_LONG);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        mSectionViewModel.getClassSection(classId).removeObservers(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(sectionNames.size() == 0){
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.sectionlayout), R.string.create_section,
                    Snackbar.LENGTH_LONG);
            snackbarObj.setDuration(2000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
//            Toast.makeText(this,"You must create one section",Toast.LENGTH_LONG);
        }else{
            mSectionViewModel.getClassSection(classId).removeObservers(this);
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if(Gc.getSharedPreference(Gc.CLASSEXISTS, this).equalsIgnoreCase(Gc.EXISTS)){
            menu.findItem(R.id.menu_next).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
