package in.junctiontech.school.schoolnew.communicate;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Arrays;
import java.util.Objects;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.staff.StaffActivity;
import in.junctiontech.school.schoolnew.student.StudentActivity;

public class SMSInformationActivity extends AppCompatActivity {

    Button btn_select_student,
            btn_select_staff,
            btn_select_class;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsinformation);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btn_select_student = findViewById(R.id.btn_select_student);
        btn_select_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SMSInformationActivity.this, StudentActivity.class);
                intent.putExtra("action","sms");
                startActivity(intent);
                finish();
            }
        });
        btn_select_staff = findViewById(R.id.btn_select_staff);
        btn_select_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SMSInformationActivity.this, StaffActivity.class);
                intent.putExtra("action","sms");
                startActivity(intent);
                finish();
            }
        });
        btn_select_class = findViewById(R.id.btn_select_class);
        btn_select_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SMSInformationActivity.this, ClassSectionActivity.class);
                intent.putExtra("action","sms");
                startActivity(intent);
                finish();
            }
        });

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2031232390", this, adContainer);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
