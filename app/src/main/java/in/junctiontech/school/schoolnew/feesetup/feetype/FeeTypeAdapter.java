package in.junctiontech.school.schoolnew.feesetup.feetype;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;

public class FeeTypeAdapter extends RecyclerView.Adapter<FeeTypeAdapter.MyViewHolder>{
    private ArrayList<FeeTypeEntity> feeList;
    private Context context;
    int colorIs;

     FeeTypeAdapter(Context context , ArrayList<FeeTypeEntity> feeList){
        this.feeList = feeList;
        this.context = context;
         colorIs = Config.getAppColor((Activity) context, true);

     }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name, tv_designation, tv_status;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name =  itemView.findViewById(R.id.tv_item_class_name);
            tv_designation = itemView.findViewById(R.id.tv_designation);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_designation.setVisibility(View.VISIBLE);
            tv_designation.setGravity(Gravity.END);
            tv_status.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder choices = new AlertDialog.Builder(context);
                    String[] aa = new String[]
                            {
                                    context.getResources().getString(R.string.update)
                            };

                    choices.setItems(aa, (dialogInterface, which) -> {
                        switch (which){
                            case 0:
//                                    updateFeeType(context,feeList.get(getAdapterPosition()));
                                ((FeeTypeActivity) context).updateFeeType(feeList.get(getAdapterPosition()));

                                break;
                        }
                    });
                    choices.show();
                }
            });
        }
    }

    @NonNull
    @Override
    public FeeTypeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull FeeTypeAdapter.MyViewHolder holder, int position) {
        holder.tv_item_name.setText(feeList.get(position).FeeType);
        holder.tv_item_name.setTextColor(colorIs);
        holder.tv_designation.setText(feeList.get(position).Frequency);
        holder.tv_designation.setTextColor(colorIs);
        holder.tv_status.setText(feeList.get(position).Status);

//        if (feeList.get(position).Status.equalsIgnoreCase(Gc.INACTIVE)) {
//            holder.tv_item_name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.tv_designation.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//        }else{
//            holder.tv_item_name.setPaintFlags(~Paint.STRIKE_THRU_TEXT_FLAG);
//            holder.tv_designation.setPaintFlags(~Paint.STRIKE_THRU_TEXT_FLAG);
//        }
    }

    @Override
    public int getItemCount() {
        return feeList.size();
    }

    public void updateFeeType(final Context context, final FeeTypeEntity feeTypeEntity){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_feetype_edit);
        dialog.setTitle(R.string.fee_type);

        final EditText et_fee_type_name = dialog.findViewById(R.id.et_fee_type_name);
        et_fee_type_name.setText(feeTypeEntity.FeeType);

        final RadioButton rb_create_monthly = dialog.findViewById(R.id.rb_create_monthly);
        if(feeTypeEntity.Frequency.equalsIgnoreCase("Monthly"))rb_create_monthly.setChecked(true);

        final RadioButton rb_create_fee_quarterly = dialog.findViewById(R.id.rb_create_fee_quarterly);
        if(feeTypeEntity.Frequency.equalsIgnoreCase("Quarterly"))rb_create_fee_quarterly.setChecked(true);

        final RadioButton rb_create_fee_session = dialog.findViewById(R.id.rb_create_fee_session);
        if(feeTypeEntity.Frequency.equalsIgnoreCase("Session"))rb_create_fee_session.setChecked(true);

        final RadioButton rb_feetype_active = dialog.findViewById(R.id.rb_feetype_active);
        if(feeTypeEntity.Status.equalsIgnoreCase("Active"))rb_feetype_active.setChecked(true);

        final RadioButton rb_feetype_inactive = dialog.findViewById(R.id.rb_feetype_inactive);
        if(feeTypeEntity.Status.equalsIgnoreCase("Inactive"))rb_feetype_inactive.setChecked(true);

        Button btnSave          =  dialog.findViewById(R.id.save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof FeeTypeActivity){
//                    try {
                    String frequency = new String();
                    String status = new String();
                    FeeTypeEntity updated = new FeeTypeEntity();
                    updated.FeeTypeID = feeTypeEntity.FeeTypeID;
                    updated.CreatedBy = feeTypeEntity.CreatedBy;
                    updated.CreatedOn = feeTypeEntity.CreatedOn;
                    updated.FeeType = et_fee_type_name.getText().toString().trim();
                    updated.generationDate = feeTypeEntity.generationDate;


                    if (rb_create_monthly.isChecked())updated.Frequency = rb_create_monthly.getText().toString();
                    if (rb_create_fee_quarterly.isChecked())updated.Frequency = "Quarterly";
                    if (rb_create_fee_session.isChecked())updated.Frequency = rb_create_fee_session.getText().toString();

                    if (rb_feetype_active.isChecked())updated.Status = rb_feetype_active.getText().toString();
                    if (rb_feetype_inactive.isChecked())updated.Status = rb_feetype_inactive.getText().toString();


                    ((FeeTypeActivity) context).updateFeeType(updated);
                        dialog.dismiss();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }

            }
        });
        Button btnCancel        =  dialog.findViewById(R.id.cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}
