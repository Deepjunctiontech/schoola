package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolStudentDao {
    @Query("select * from schoolstudententity")
    LiveData<List<SchoolStudentEntity>> getAllStudents() ;

    @Query("select * from schoolstudententity where Status = 'Studying' and studentfeeSession like :appsession")
    LiveData<List<SchoolStudentEntity>> getAllStudentsSession(String appsession) ;

    @Query("select count(AdmissionId) as studentCount from schoolstudententity where Status = 'Studying' and studentfeeSession like :appSession")
    LiveData<StudentCountTopple> getAllStudentsCount(String appSession) ;

    @Query("select * from schoolstudententity where studentfeeSession like :appsession")
    LiveData<List<SchoolStudentEntity>> getAllSessionStudents(String appsession) ;

    @Query("select * from schoolstudententity where Status = :status and SectionId like :sectionid")
    LiveData<List<SchoolStudentEntity>> getAllStudentsSection(String sectionid,String status) ;

    @Query("select * from schoolstudententity where Status = :status and ClassId like :classid")
    LiveData<List<SchoolStudentEntity>> getAllStudentsClass(String classid, String status) ;

    @Query("select * from schoolstudententity where AdmissionNo like :admissionNo and AdmissionId != :exclude")
    List<SchoolStudentEntity> searchAdmissionNo(String admissionNo,String exclude) ;

    @Query("select * from schoolstudententity where AdmissionNo like :admissionNo")
    List<SchoolStudentEntity> searchAdmissionNoDuplicate(String admissionNo) ;

    @Query("select * from schoolstudententity where ClassId like :classid and AdmissionId != :exclude")
    LiveData<List<SchoolStudentEntity>> getAllStudentsClassMessenger(String classid, String exclude) ;

    @Query("select * from schoolstudententity where AdmissionId like :admissionid")
    LiveData<SchoolStudentEntity> getStudentById(String admissionid) ;

    @Query(("select ClassId, count(ClassId) as stcount from schoolstudententity where Status = 'Studying' group by ClassId"))
    LiveData<List<ClassStudentCountTupple>> getNumberOfStudents();

    @Query(("select SectionId, count(SectionId) as stcount from schoolstudententity where Status = 'Studying' group by SectionId"))
    LiveData<List<SectionStudentCountTupple>> getNumberOfStudentsSection();

    @Query("delete from SchoolStudentEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertStudents(List<SchoolStudentEntity> schoolStudentEntities);

    @Update(onConflict = REPLACE)
    void updateStudent(SchoolStudentEntity schoolStudentEntity);


}
