package `in`.junctiontech.school.schoolnew.chat.fragment

import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity
import `in`.junctiontech.school.schoolnew.chat.ChatConversationActivity
import `in`.junctiontech.school.schoolnew.chat.adapter.StudentListFragmentAdapter
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.CommunicationPermissions
import `in`.junctiontech.school.schoolnew.model.StudentInformation
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import java.lang.Exception


class StudentListFragment : Fragment(), StudentListFragmentAdapter.StudentClickListener {

    var mDb: MainDatabase? = null
    lateinit var adapter: StudentListFragmentAdapter
    private lateinit var contexts: Context

    private var chatPermissions = ArrayList<String>()
    private var communicationPermissions = ArrayList<CommunicationPermissions>()
    private lateinit var staffInfo: SignedInStaffInformationEntity
    private lateinit var studentInfo: StudentInformation
    lateinit var mRecyclerView: RecyclerView
    var studentList = ArrayList<SchoolStudentEntity>()
    private var studentFilterList = ArrayList<SchoolStudentEntity>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val bundle = this.arguments
        if (bundle != null) {
            val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)
            if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {

                val s = bundle.getString(Gc.STUDENTPARENT)
                if (s != null) {
                    studentInfo = Gson().fromJson(s, StudentInformation::class.java)
                }
            } else if (role.equals(Gc.STAFF, ignoreCase = true)) {

                val s = bundle.getString(Gc.STAFF)
                if (s != null) {
                    staffInfo = Gson().fromJson(s, SignedInStaffInformationEntity::class.java)
                }

               // staffInfo = Gson().fromJson(bundle.getString(Gc.STAFF), SignedInStaffInformationEntity::class.java)
            }
        }

        mDb = MainDatabase.getDatabase(contexts)
        val view: View = inflater.inflate(R.layout.fragment_staff_list, container, false)
        mRecyclerView = view.findViewById(R.id.staffList) as RecyclerView
        adapter = StudentListFragmentAdapter(studentList, contexts, activity, this)
        mRecyclerView.adapter = adapter

        mDb!!.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, contexts))
                .observe(viewLifecycleOwner, Observer<List<CommunicationPermissions>> { communicationPermissions: List<CommunicationPermissions> ->
                    chatPermissions.clear()
                    for (c in communicationPermissions) {
                        chatPermissions.add(c.ToUserType)
                    }
                    initialize(view)
                })
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexts = context
    }

    override fun onStartChatStudentClickCallback(position: Int) {
        val intent = Intent(activity, ChatConversationActivity::class.java)

        val student = Gson().toJson(studentList[position])
        intent.putExtra("with", student)
        intent.putExtra("withType", "student")
        startActivity(intent)
    }

    private fun initialize(view: View) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerView.layoutManager = layoutManager
        val textViewNoRecentChat = view.findViewById<TextView>(R.id.textViewNoRecentChat)

        when (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)) {
            Gc.STAFF -> {
                if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                    mDb!!.schoolStudentModel()
                            .allStudents
                            .observe(viewLifecycleOwner, Observer<List<SchoolStudentEntity>> { schoolStudentEntities: List<SchoolStudentEntity> ->
                                studentFilterList.clear()
                                studentList.addAll(schoolStudentEntities)
                                studentFilterList.addAll(schoolStudentEntities)
//                                adapter = StudentListFragmentAdapter(studentList, contexts, activity, this)
//                                mRecyclerView.adapter = adapter
                                adapter.notifyDataSetChanged()
                            })
                } else {
                    studentList.clear()
                    studentFilterList.clear()
                    adapter.notifyDataSetChanged()
                    textViewNoRecentChat.visibility = View.VISIBLE
                    textViewNoRecentChat.text = getString(R.string.you_dont_have_permission_to_send_messages_to) + getString(R.string.student)
                }
            }
            Gc.STUDENTPARENT -> {
                if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                    mDb!!.schoolStudentModel()
                            .allStudents
                            .observe(viewLifecycleOwner, Observer<List<SchoolStudentEntity>> { schoolStudentEntities: List<SchoolStudentEntity> ->
                                studentList.clear()
                                studentFilterList.clear()
                                studentList.addAll(schoolStudentEntities)
                                studentFilterList.addAll(schoolStudentEntities)
//                                adapter = StudentListFragmentAdapter(studentList, contexts, activity, this)
//                                mRecyclerView.adapter = adapter
                                adapter.notifyDataSetChanged()
                            })
                } else {
                    studentList.clear()
                    studentFilterList.clear()
                    textViewNoRecentChat.visibility = View.VISIBLE
                    textViewNoRecentChat.text = getString(R.string.you_dont_have_permission_to_send_messages_to) + getString(R.string.student)
                     adapter.notifyDataSetChanged()
                }
            }
            Gc.ADMIN -> {
                mDb!!.schoolStudentModel()
                        .allStudents
                        .observe(viewLifecycleOwner, Observer { schoolStudentEntities: List<SchoolStudentEntity> ->
                            studentList.clear()
                            studentFilterList.clear()
                            studentList.addAll(schoolStudentEntities)
                            studentFilterList.addAll(schoolStudentEntities)

//                            adapter = StudentListFragmentAdapter(studentList, contexts, activity, this)
//                            mRecyclerView.adapter = adapter
                            adapter.notifyDataSetChanged()
                        })
            }
        }
    }

    companion object {
        var TAG = StudentListFragment::class.java.simpleName
    }
}