package `in`.junctiontech.school.schoolnew.leave

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LeaveActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar
    private var rvApplyLeaveDetails: RecyclerView? = null
    private var tv_no_apply_leave: TextView? = null
    private val leavesDetails = ArrayList<LeavesDetails>()
    private val applyLeavesDetails = ArrayList<ApplyLeavesDetails>()

    lateinit var adapterApplyLeaveDetails: ApplyLeaveDetailsAdapter
    private var colorIs: Int = 0
    private var fbApplyLeave: FloatingActionButton? = null

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
        fbApplyLeave!!.setBackgroundTintList(ColorStateList.valueOf(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)

        fbApplyLeave = findViewById(R.id.fb_apply_leave)

        tv_no_apply_leave = findViewById(R.id.tv_no_apply_leave)

        rvApplyLeaveDetails = findViewById(R.id.rv_apply_leave_details)

        fbApplyLeave?.setOnClickListener {
            showActivityForApplyLeave()
        }

        setUpRecyclerView()
        getLeaves()
        setColorApp()

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/7317356271", this, adContainer)
        }
    }

    private fun showActivityForApplyLeave() {
        val assignLeaves = Gson().toJson(leavesDetails)
        val intent = Intent(this@LeaveActivity, ApplyLeaveActivity::class.java)
        intent.putExtra("AssignLeaves", assignLeaves)
        startActivityForResult(intent, 1120)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpRecyclerView() {
        adapterApplyLeaveDetails = ApplyLeaveDetailsAdapter(this)
        val mLayoutManagers = LinearLayoutManager(this)
        rvApplyLeaveDetails!!.layoutManager = mLayoutManagers
        val itemAnimators = DefaultItemAnimator()
        itemAnimators.addDuration = 1000
        itemAnimators.removeDuration = 1000
        rvApplyLeaveDetails!!.itemAnimator = itemAnimators
        rvApplyLeaveDetails!!.adapter = adapterApplyLeaveDetails

    }

    private fun getLeaves() {
        progressBar.visibility = View.VISIBLE
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "profile/staffProfile/"
                + JSONObject().put("ProfileId", Gc.getSharedPreference(Gc.USERID, applicationContext)))

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), { job ->
            val code = job.optString("code")
            leavesDetails.clear()
            applyLeavesDetails.clear()
            progressBar.visibility = View.GONE
            when (code) {
                Gc.APIRESPONSE200 -> {
                    try {

                        val staffJArr = job.getJSONObject("result").getJSONArray("staffLeave")
                        staffJArr.getJSONObject(0).toString()
                        val ok = Gson()
                                .fromJson<ArrayList<LeavesDetails>>(staffJArr.getJSONObject(0).optString("fixedLeave"), object : com.google.gson.reflect.TypeToken<List<LeavesDetails>>() {
                                }.type)
                        val oks = Gson()
                                .fromJson<ArrayList<ApplyLeavesDetails>>(job.getJSONObject("result").optString("applyLeaves"),
                                        object : com.google.gson.reflect.TypeToken<List<ApplyLeavesDetails>>() {
                                        }.type)

                        if(ok !=null){
                            fbApplyLeave!!.show()
                            leavesDetails.addAll(ok)
                        }

                        if (oks != null) {
                            applyLeavesDetails.addAll(oks)
                            tv_no_apply_leave?.visibility = View.GONE
                        }
                        else{
                            tv_no_apply_leave?.visibility = View.VISIBLE
                        }
                        //applyLeavesDetails.addAll(oks)


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.leave_activity), R.color.fragment_first_green)
                }

                Gc.APIRESPONSE401 -> {

                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this@LeaveActivity)

                    val intent1 = Intent(this@LeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this@LeaveActivity)

                    val intent2 = Intent(this@LeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.leave_activity), R.color.fragment_first_blue)
            }
            adapterApplyLeaveDetails.notifyDataSetChanged()
        }, { error ->
            progressBar.visibility = View.VISIBLE
            Config.responseVolleyErrorHandler(this@LeaveActivity, error, findViewById(R.id.leave_activity))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return null
            }

        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1120) {
            if (resultCode == Activity.RESULT_OK) {
                getLeaves()
                val message = Objects.requireNonNull(data!!.extras)?.getString("message")
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.leave_activity), R.color.fragment_first_green)
            }
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    inner class ApplyLeaveDetailsAdapter internal constructor(internal var activity: Activity) : RecyclerView.Adapter<ApplyLeaveDetailsAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_apply_leave_details, parent, false))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvItemName.text = applyLeavesDetails[position].StaffName
            holder.tvItemLeaveType.text = applyLeavesDetails[position].leaveName
            holder.tvItemStatus.text = applyLeavesDetails[position].status
            holder.tvItemFromDate.text = applyLeavesDetails[position].fromDate
            holder.tvItemToDate.text = applyLeavesDetails[position].toDate
            holder.tvItemDays.text = applyLeavesDetails[position].dayLeaves
        }

        override fun getItemCount(): Int {
            return applyLeavesDetails.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvItemName: TextView = itemView.findViewById(R.id.tv_item_name)
            internal var tvItemLeaveType: TextView = itemView.findViewById(R.id.tv_item_leave_type)
            internal var tvItemStatus: TextView = itemView.findViewById(R.id.tv_item_status)
            internal var tvItemFromDate: TextView = itemView.findViewById(R.id.tv_item_from_date)
            internal var tvItemToDate: TextView = itemView.findViewById(R.id.tv_item_to_date)
            internal var tvItemDays: TextView = itemView.findViewById(R.id.tv_item_days)
        }
    }
}

internal class LeavesDetails {
    var leaveTypeId: String? = null
    var leaveName: String? = null
    var timeCircle: String? = null
    var type: String? = null
    var leave: String? = null
    var TotalApprove: String? = null
}

internal class ApplyLeavesDetails {

    var fromDate: String? = null
    var toDate: String? = null
    var status: String? = null
    var StaffName: String? = null
    var leaveName: String? = null
    var dayLeaves: String? = null
}