package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.annotation.NonNull;

@Entity(primaryKeys = {"FeeType","Distance"})
public class TransportFeeAmountEntity {

    public String Session;
    @NonNull
    public String FeeType;
    public String Amount;
    @NonNull
    public String Distance;

}
