package in.junctiontech.school.schoolnew.messaging.MsgAdapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.ChatData;

/**
 * Created by JAYDEVI BHADE on 12/28/2016.
 */

public class ChatConversationAdapter extends RecyclerView.Adapter<ChatConversationAdapter.MyViewHolder> {
    private ArrayList<ChatData> conversationList = new ArrayList<>();

    private String loggedUserId, loggedUserType;
    private Context context;
    String todayDate[] = Config.feedbackDateFormate.format(new Date()).split(" ");

    public ChatConversationAdapter(Context context, ArrayList<ChatData> conversationList,
                                   String loggedUserId, String loggedUserType) {
        this.conversationList = conversationList;
        this.context = context;
        this.loggedUserId = loggedUserId;
        this.loggedUserType = loggedUserType;

    }

    @Override
    public ChatConversationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_conversation, parent, false);
        return new ChatConversationAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ChatConversationAdapter.MyViewHolder holder, int position) {
        ChatData obj = conversationList.get(position);

        if (obj.getUserID().equalsIgnoreCase(loggedUserId)
                &&
                obj.getUserType().equalsIgnoreCase(loggedUserType)) {

            /* --- Logged User Layout &&
            hide the sender layout ---*/
            holder.ll_logged_user_chat_picture.setVisibility(View.GONE);
            holder.ll_sender_chat_picture.setVisibility(View.GONE);
            holder.ll_sender_chat.setVisibility(View.GONE);
            holder. iv_sender_user_alarm.setVisibility(View.GONE);
            holder. tv_sender_user_chat_time.setVisibility(View.GONE);

           /*-- fill chat data ---*/
            holder.tv_logged_user_chat_msg.setText(obj.getMsg());

            /*--- set Time ---*/
            String[] chatTime = obj.getCreatedAtMsg().split(" ");
            if (chatTime[0].equalsIgnoreCase(todayDate[0]) &&
                    chatTime[1].equalsIgnoreCase(todayDate[1]) &&
                    chatTime[2].equalsIgnoreCase(todayDate[2]))

                holder.tv_logged_user_chat_time.setText(chatTime[3] + " " + chatTime[4]);
            else holder.tv_logged_user_chat_time.setText(chatTime[0] + " " + chatTime[1] + " " + chatTime[2]);


            if (obj.isPerDayTagVisibility()) {
                holder.tv_day_name.setText(chatTime[1] + " " + chatTime[0] + " " + chatTime[2]);
                holder.tv_day_name.setVisibility(View.VISIBLE);
            } else holder.tv_day_name.setVisibility(View.GONE);

            setChatSendingTimer(holder.iv_logged_user_alarm,obj.getStatus());
            Log.e("obj.getStatus()",obj.getStatus());
        }else {
              /* --- Sender User Layout &&
            hide the Logged user layout ---*/
            holder.ll_sender_chat_picture.setVisibility(View.GONE);
            holder.ll_logged_user_chat_picture.setVisibility(View.GONE);
            holder.ll_logged_user_chat.setVisibility(View.GONE);
            holder. iv_logged_user_alarm.setVisibility(View.GONE);
            holder. tv_logged_user_chat_time.setVisibility(View.GONE);

           /*-- fill chat data ---*/
            holder.tv_sender_chat_msg.setText(obj.getMsg());

            /*--- set Time ---*/
            String[] chatTime = obj.getCreatedAtMsg().split(" ");
            if (chatTime[0].equalsIgnoreCase(todayDate[0]) &&
                    chatTime[1].equalsIgnoreCase(todayDate[1]) &&
                    chatTime[2].equalsIgnoreCase(todayDate[2]))

                holder.tv_sender_user_chat_time.setText(chatTime[3] + " " + chatTime[4]);
            else holder.tv_sender_user_chat_time.setText(chatTime[0] + " " + chatTime[1] + " " + chatTime[2]);


            if (obj.isPerDayTagVisibility()) {
                holder.tv_day_name.setVisibility(View.VISIBLE);
                holder.tv_day_name.setText(chatTime[1] + " " + chatTime[0] + " " + chatTime[2]);

            } else holder.tv_day_name.setVisibility(View.GONE);



            setChatSendingTimer(holder.iv_sender_user_alarm,obj.getStatus());
        }

    }

    private void setChatSendingTimer(ImageView iv_alarm, String status) {
        switch (status) {
            case "pending":
                iv_alarm.setImageResource(android.R.drawable.ic_menu_recent_history);
                break;

            case "send":
                iv_alarm.setImageResource(R.drawable.single_tick);
                break;

            case "sent":
                iv_alarm.setImageResource(R.drawable.ic_double_tick);
                break;
            default:
                iv_alarm.setImageResource(0);


        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }

    public void updateList(ArrayList<ChatData> lastChatConversationList) {
        this.conversationList= lastChatConversationList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_day_name, tv_sender_chat_msg, tv_sender_user_chat_time, tv_logged_user_chat_msg,
                tv_logged_user_chat_time;
        LinearLayout ll_sender_chat_picture, ll_sender_chat, ll_logged_user_chat_picture,
                ll_logged_user_chat;
        ImageView iv_sender_chat_picture, iv_sender_user_alarm, iv_logged_user_chat_picture,
                iv_logged_user_alarm;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_day_name = (TextView) itemView.findViewById(R.id.tv_chat_day_name);
            /*--- sender ids ---*/
            ll_sender_chat_picture = (LinearLayout) itemView.findViewById(R.id.ll_sender_chat_picture);
            iv_sender_chat_picture = (ImageView) itemView.findViewById(R.id.iv_sender_chat_picture);
            ll_sender_chat = (LinearLayout) itemView.findViewById(R.id.ll_sender_chat);
            tv_sender_chat_msg = (TextView) itemView.findViewById(R.id.tv_sender_chat_msg);
            iv_sender_user_alarm = (ImageView) itemView.findViewById(R.id.iv_sender_user_alarm);
            tv_sender_user_chat_time = (TextView) itemView.findViewById(R.id.tv_sender_user_chat_time);

            /*--- logged user ids ---*/
            ll_logged_user_chat_picture = (LinearLayout) itemView.findViewById(R.id.ll_logged_user_chat_picture);
            iv_logged_user_chat_picture = (ImageView) itemView.findViewById(R.id.iv_logged_user_chat_picture);
            ll_logged_user_chat = (LinearLayout) itemView.findViewById(R.id.ll_logged_user_chat);
            tv_logged_user_chat_msg = (TextView) itemView.findViewById(R.id.tv_logged_user_chat_msg);
            iv_logged_user_alarm = (ImageView) itemView.findViewById(R.id.iv_logged_user_alarm);
            tv_logged_user_chat_time = (TextView) itemView.findViewById(R.id.tv_logged_user_chat_time);



        }
    }
}
