package `in`.junctiontech.school.schoolnew.chat

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.MasterEntryEntity
import `in`.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity
import `in`.junctiontech.school.schoolnew.chat.fragment.RecentChatFragment
import `in`.junctiontech.school.schoolnew.chat.fragment.SectionGroupFragment
import `in`.junctiontech.school.schoolnew.chat.fragment.StaffListFragment
import `in`.junctiontech.school.schoolnew.chat.fragment.StudentListFragment
import `in`.junctiontech.school.schoolnew.chatdb.ChatDatabase
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.StudentInformation
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import java.util.*

class ChatActivity : AppCompatActivity() {
    private var mDb: MainDatabase? = null
    private var chatDb: ChatDatabase? = null
    //   private val chatList: ArrayList<ChatListEntity> = ArrayList()

    //  private val chatFilterList: ArrayList<ChatListEntity> = ArrayList()
    private var studentInfo: StudentInformation? = null
    private var staffInfo: SignedInStaffInformationEntity? = null
    private lateinit var tabLayout: TabLayout

    private var colorIs = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
        tabLayout.setBackgroundColor(colorIs)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        // supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mDb = MainDatabase.getDatabase(this)
        chatDb = ChatDatabase.getDatabase(this)
        tabLayout = findViewById(R.id.tabs)


        setColorApp()

        getChatMediaStorageUrl()

        val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)
        if ("" != Gc.getSharedPreference(Gc.SCHOOLNAME, this)) {
            supportActionBar!!.title = Gc.getSharedPreference(Gc.SCHOOLNAME, this)
        }
        if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {
            mDb!!.signedInStudentInformationModel().signedInStudentInformation
                    .observe(this, Observer { studentInformation ->
                        studentInfo = studentInformation
                        setViewPager()
                    })
        } else if (role.equals(Gc.STAFF, ignoreCase = true)) {
            mDb!!.signedInStaffInformationModel().signedInStaff
                    .observe(this, Observer { signedInStaffInformationEntity ->
                        staffInfo = signedInStaffInformationEntity
                        setViewPager()
                    })
        } else {
            setViewPager()
        }
        if (!listOf<String>(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase(Locale.ROOT)) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase(Locale.ROOT).equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/4118385648", this, adContainer)
        }
    }

    fun setViewPager() {
        val viewPager = findViewById(R.id.viewpager) as ViewPager
        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        val recentChatFragment = RecentChatFragment()
        val studentListFragment = StudentListFragment()
        val sectionGroupFragment = SectionGroupFragment()
        val staffListFragment = StaffListFragment()
        val bundle = Bundle()
        if (studentInfo != null) {
            bundle.putString(Gc.STUDENTPARENT, Gson().toJson(studentInfo))
        } else if (staffInfo != null) {
            bundle.putString(Gc.STAFF, Gson().toJson(staffInfo))
        }
        recentChatFragment.arguments = bundle

        studentListFragment.arguments = bundle
        sectionGroupFragment.arguments = bundle
        staffListFragment.arguments = bundle


        adapter.addFragment(recentChatFragment, resources.getString(R.string.chats))
        adapter.addFragment(staffListFragment, resources.getString(R.string.staff))
        adapter.addFragment(studentListFragment, resources.getString(R.string.student))
        adapter.addFragment(sectionGroupFragment, resources.getString(R.string.section))
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val mFragmentList: MutableList<Fragment> = ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    private fun getChatMediaStorageUrl() {
        val StorageURL = mDb?.masterEntryModel()?.getMasterEntryValues(Gc.StorageURL) as ArrayList<MasterEntryEntity?>

        if (StorageURL.size == 0) {
            Config.responseSnackBarHandler(resources.getString(R.string.sending_media_is_disabled_for_your_organization_Please_contact),
                    findViewById(R.id.chat_activity), R.color.fragment_first_blue)
            return
        }

        if(!StorageURL[0]?.MasterEntryValue.isNullOrEmpty()){
            var s = StorageURL[0]?.MasterEntryValue?.toLowerCase(Locale.getDefault())
            if (s != null) {
                s = s.substring(0, s.length - if (s.endsWith("/")) 1 else 0)
                val setDefaults = HashMap<String, String>()
                setDefaults[Gc.MEDIASTORAGEURL] = s
                Gc.setSharedPreference(setDefaults, this)
            }
        } else {
            Config.responseSnackBarHandler(resources.getString(R.string.sending_media_is_disabled_for_your_organization_Please_contact),
                    findViewById(R.id.chat_activity), R.color.fragment_first_blue)
        }


        /* val param = JSONObject()
         param.put("MasterEntryName", "MediaStorageURL")
         val url = (Gc
                 .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                 + Gc.ERPAPIVERSION
                 + "master/masterEntries")
         val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.GET, "$url/$param", JSONObject(), Response.Listener { job: JSONObject ->
             when (job.optString("code")) {
                 Gc.APIRESPONSE200 -> {
                     val resultjobj = job.getJSONObject("result")
                     val masterEntry = resultjobj.getJSONArray("masterEntry")
                     var s = masterEntry.getJSONObject(0).optString("MasterEntryValue").toLowerCase(Locale.getDefault())
                     s = s.substring(0, s.length - if (s.endsWith("/")) 1 else 0)
                     val setDefaults = HashMap<String, String>()
                     setDefaults[Gc.MEDIASTORAGEURL] = s
                     Gc.setSharedPreference(setDefaults, this@ChatActivity)
                 }
                 else -> {
                     Config.responseSnackBarHandler(job.optString("message"),
                             findViewById(R.id.chat_activity), R.color.fragment_first_blue)

                 }
             }
         }, Response.ErrorListener { }) {
             override fun getHeaders(): Map<String, String> {
                 val headers = HashMap<String, String>()
                 headers["APPKEY"] = Gc.APPKEY
                 headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                 headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                 headers["Content-Type"] = Gc.CONTENT_TYPE
                 headers["DEVICE"] = Gc.DEVICETYPE
                 headers["DEVICEID"] = Gc.id(applicationContext)
                 headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                 headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                 headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                 headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                 return headers
             }

             override fun getBodyContentType(): String {
                 return "application/json; charset=utf-8"
             }
         }
         jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
         AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)*/


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        /*   menuInflater.inflate(R.menu.menu_sort_and_search, menu)
           menu.findItem(R.id.action_save).isVisible = false
           menu.findItem(R.id.action_select_all).isVisible = false
           menu.findItem(R.id.menu_sort_by_alphabetical).isVisible = false
           menu.findItem(R.id.menu_sort_by_number).isVisible = false
           menu.findItem(R.id.action_help).isVisible = false
           menu.findItem(R.id.action_delete).isVisible = false*/
        // menu.findItem(R.id.action_delete).setVisible(true);
        // Associate searchable configuration with the SearchView
        /*  val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
          val searchView = menu.findItem(R.id.menu_action_search).actionView as SearchView
          searchView.setSearchableInfo(
                  searchManager.getSearchableInfo(componentName))
          searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
              override fun onQueryTextSubmit(s: String): Boolean {
                  //       filterConversations(s)
                  return true
              }

              override fun onQueryTextChange(s: String): Boolean {
                  //         filterConversations(s)
                  return true
              }
          })*/
        return true
    }

    /* fun filterConversations(text: String) {
         var texts = text
         chatList.clear()
         if (texts.isEmpty()) {
             chatList.addAll(chatFilterList)
         } else {
             texts = texts.toLowerCase(Locale.ROOT)
             for (c in chatFilterList) {
                 if (c.withName.toLowerCase(Locale.ROOT).contains(texts)
                         || c.withType.toLowerCase(Locale.ROOT).contains(texts)
                         || c.msg.toLowerCase(Locale.ROOT).contains(texts)) {
                     chatList.add(c)
                 }
             }
         }
         adapter.notifyDataSetChanged()
     }*/

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_help -> {
                val alert1 = AlertDialog.Builder(this)
                alert1.setMessage(HtmlCompat.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                alert1.setTitle(getString(R.string.help))
                alert1.setIcon(R.drawable.ic_help)
                alert1.setPositiveButton(getString(R.string.ok)) { _, _ -> }
                //            alert1.setCancelable(false);
                alert1.show()
            }
            R.id.action_delete -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
}