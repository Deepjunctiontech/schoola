package in.junctiontech.school.schoolnew.student;

import android.app.DatePickerDialog;
import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class ChangeClassActivity extends AppCompatActivity {

    MainDatabase mDb ;
    ProgressBar progressBar;

    SchoolStudentEntity student;

    TextView tv_change_class_student,
            tv_change_class_current;

    Button btn_transfer_class,
            btn_change_class_next_class,
            btn_fee_effective_from;

    TextView tv_change_class_total_session,
            tv_change_class_total_quarterly,
            tv_change_class_total_monthly;

    TextView tv_change_class_fee_not_available;

    LinearLayout ll_fee_list;

    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    ArrayList<String> sectionList = new ArrayList<>();
    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    int selectedSection;

    ArrayList<FeeTypeEntity> feeTypeList = new ArrayList<>();

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        tv_change_class_current.setTextColor(colorIs);
        tv_change_class_student.setTextColor(colorIs);
        btn_transfer_class.setBackgroundColor(colorIs);
        btn_change_class_next_class.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_class);
        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        student = new Gson().fromJson(getIntent().getStringExtra("student"), SchoolStudentEntity.class);

        initializeViews();
        tv_change_class_student.setText(student.StudentName);
        tv_change_class_current.setText(student.ClassName + " : "+ student.SectionName);

        initializeClassSelect();

        initializeFeeEffectiveFrom();

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2035101824", this, adContainer);
        }
    }

    void initializeFeeEffectiveFrom(){
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener feedate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MMM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

                btn_fee_effective_from.setText(sdf.format(myCalendar.getTime()));
            }

        };

        btn_fee_effective_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ChangeClassActivity.this, feedate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    JSONObject buildJsonForClassChange() throws JSONException {
        JSONObject param = new JSONObject();

        param.put("admissionId", student.AdmissionId);
        param.put("effectiveFrom", btn_fee_effective_from.getText().toString());
        param.put("session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("sectionId", classNameSectionList.get(selectedSection).SectionId);

        JSONArray feearr = new JSONArray();

        for (int k = 0; k < ll_fee_list.getChildCount(); k++){

            View view1 = ll_fee_list.getChildAt(k);

            String enteredamount = ((EditText)view1.findViewById(R.id.et_item_amount)).getText().toString();
            String feetypeid = ((TextView) view1.findViewById(R.id.tv_item_feetypeid)).getText().toString();
            String feeid = ((TextView) view1.findViewById(R.id.tv_item_feeid)).getText().toString();

            if(!("".equals(enteredamount) || "0".equals(enteredamount))) {
                feearr.put(new JSONObject().put("FeeId", feetypeid).put("StudentFeeAmount", enteredamount));
            }
        }

        param.put("fees",feearr);

        return param;
    }

    void transferClassOfStudent() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = buildJsonForClassChange();


        final String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "classSection/transferClass"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            student.ClassName = classNameSectionList.get(selectedSection).ClassName;
                            student.SectionName = classNameSectionList.get(selectedSection).SectionName;
                            student.ClassId = classNameSectionList.get(selectedSection).ClassId;
                            student.SectionId = classNameSectionList.get(selectedSection).SectionId;


                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.schoolStudentModel().updateStudent(student);
                                }
                            }).start();
                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_activity_change_class),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, ChangeClassActivity.this);

                        Intent intent1 = new Intent(ChangeClassActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, ChangeClassActivity.this);

                        Intent intent2 = new Intent(ChangeClassActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.ll_activity_change_class),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(ChangeClassActivity.this,error,findViewById(R.id.ll_activity_change_class));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this)).observe(this, new Observer<List<ClassNameSectionName>>() {
            @Override
            public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                classNameSectionList.clear();
                sectionList.clear();
                classSectionAdapter.clear();
                classNameSectionList.addAll(classNameSectionNames);
                for(int i = 0; i < classNameSectionList.size(); i++){
                    sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
                }
                classSectionAdapter.addAll(sectionList);
                classSectionAdapter.notifyDataSetChanged();
            }
        });
    }

    void initializeClassSelect(){
        classListBuilder = new AlertDialog.Builder(ChangeClassActivity.this);
        classSectionAdapter =
                new ArrayAdapter<String>(ChangeClassActivity.this, android.R.layout.select_dialog_singlechoice);

        btn_change_class_next_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                classListBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                classListBuilder.setAdapter(classSectionAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        btn_change_class_next_class.setText(classNameSectionList.get(i).ClassName + " "
                                + classNameSectionList.get(i).SectionName);
                        selectedSection = i;

                        tv_change_class_total_monthly.setText("0");
                        tv_change_class_total_quarterly.setText("0");
                        tv_change_class_total_session.setText("0");

//                        ArrayList<ClassFeesEntity> classFeesEntities = new ArrayList<>();
//                        classFeesEntities = (ArrayList<ClassFeesEntity>) mDb.classFeesModel()
//                                .getFeeforClassSection(classNameSectionList.get(i).SectionId);

                        final String sectionid = classNameSectionList.get(i).SectionId;

                        mDb.classFeesModel()
                                .getFeeforClassSectionLive(sectionid)
                                .observe(ChangeClassActivity.this, new Observer<List<ClassFeesEntity>>() {
                                    @Override
                                    public void onChanged(@Nullable List<ClassFeesEntity> classFeesEntities) {
                                        if (classFeesEntities!=null && classFeesEntities.size()>0) {
                                            buildFeeListWhenClassFeeDoesNotExist((ArrayList<ClassFeesEntity>) classFeesEntities);
                                            calculateFeesSumForDisplay();
                                        }else {
                                            ll_fee_list.removeAllViews();
                                            Config.responseSnackBarHandler(getString(R.string.please_enter_fees_for_this_class_in_manage_fees),
                                                        findViewById(R.id.ll_activity_change_class), R.color.fragment_first_blue);

                                        }
                                        mDb.classFeesModel().getFeeforClassSectionLive(sectionid).removeObserver(this);
                                    }
                                });
                    }
                });

                AlertDialog dialog = classListBuilder.create();
                dialog.show();
            }
        });

    }

    void buildFeeListWhenClassFeeDoesNotExist(  ArrayList<ClassFeesEntity> classFeesEntities){
        ll_fee_list.removeAllViews();

        for(int i = 0; i < classFeesEntities.size(); i++) {

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View view = layoutInflater.inflate(R.layout.item_add_manage_fee, null);
            TextView tv_item_fee_name = view.findViewById(R.id.tv_item_fee_name);
            tv_item_fee_name.setText(classFeesEntities.get(i).FeeType);

            TextView tv_item_fee_frequency = view.findViewById(R.id.tv_item_fee_frequency);
            tv_item_fee_frequency.setText(classFeesEntities.get(i).Frequency);

            TextView tv_item_feetypeid = view.findViewById(R.id.tv_item_feetypeid);
            tv_item_feetypeid.setText(classFeesEntities.get(i).FeeId);

            EditText et_item_amount = view.findViewById(R.id.et_item_amount);
            et_item_amount.setText(classFeesEntities.get(i).Amount);


            et_item_amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    calculateFeesSumForDisplay();

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            ll_fee_list.addView(view);
        }
    }

    void calculateFeesSumForDisplay(){
        int sumsession = 0;
        int summonthly = 0;
        int sumquarterly = 0;
        for (int k = 0; k < ll_fee_list.getChildCount(); k++){
            View view1 = ll_fee_list.getChildAt(k);
            String enteredamount = ((EditText)view1.findViewById(R.id.et_item_amount)).getText().toString();
            String frequency = ((TextView) view1.findViewById(R.id.tv_item_fee_frequency)).getText().toString();
            if ("Monthly".equalsIgnoreCase(frequency)) {
                summonthly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            }else if ("Session".equalsIgnoreCase(frequency)){
                sumsession += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            }else if ("Quarterly".equalsIgnoreCase(frequency))
                sumquarterly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
        }
        tv_change_class_total_session.setText(sumsession+"");
        tv_change_class_total_quarterly.setText(sumquarterly+"");
        tv_change_class_total_monthly.setText(summonthly+"");
    }

    void initializeViews(){
        tv_change_class_student = findViewById(R.id.tv_change_class_student);
        tv_change_class_current = findViewById(R.id.tv_change_class_current);

        btn_transfer_class = findViewById(R.id.btn_transfer_class);
        btn_transfer_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    transferClassOfStudent();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_change_class_next_class = findViewById(R.id.btn_change_class_next_class);
        btn_fee_effective_from = findViewById(R.id.btn_fee_effective_from);

        tv_change_class_total_session = findViewById(R.id.tv_change_class_total_session);
        tv_change_class_total_quarterly = findViewById(R.id.tv_change_class_total_quarterly);
        tv_change_class_total_monthly = findViewById(R.id.tv_change_class_total_monthly);
        tv_change_class_fee_not_available = findViewById(R.id.tv_change_class_fee_not_available);

        ll_fee_list = findViewById(R.id.ll_fee_list);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);

        menu.findItem(R.id.menu_next).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;

            case R.id.action_save:
                if (validateInput()) {
                    try {
                        transferClassOfStudent();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    Boolean validateInput(){
        Log.i("string",getString(R.string.select_class) );
        Log.i("Button text", btn_change_class_next_class.getText().toString());
        if (getString(R.string.select_class).equalsIgnoreCase(btn_change_class_next_class.getText().toString())){
            Config.responseSnackBarHandler(getString(R.string.class_text) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.ll_activity_change_class), R.color.fragment_first_blue);
            return false;
        }
        if (getString(R.string.fee_effective_from).equalsIgnoreCase(btn_fee_effective_from.getText().toString())){
            Config.responseSnackBarHandler(getString(R.string.fee_effective_from) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.ll_activity_change_class), R.color.fragment_first_blue);
            return false;
        }

        if (!(ll_fee_list.getChildCount()>0)){
            Config.responseSnackBarHandler(getString(R.string.please_enter_fees_for_this_class_in_manage_fees),
                    findViewById(R.id.ll_activity_change_class), R.color.fragment_first_blue);
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
