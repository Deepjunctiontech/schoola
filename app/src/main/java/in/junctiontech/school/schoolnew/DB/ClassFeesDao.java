package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ClassFeesDao {
    @Query("select * from ClassFeesEntity where SectionId LIKE :sectionid  ")
    List<ClassFeesEntity> getFeeforClassSection(String sectionid);

    @Query("select * from ClassFeesEntity where SectionId LIKE :sectionid and FeeStatus == 'Active'")
    LiveData<List<ClassFeesEntity>> getFeeforClassSectionLive(String sectionid);

    @Query("select count(*) from ClassFeesEntity where SectionId like :sectionid")
    int checkFeeExists(String sectionid);

    @Query("delete from ClassFeesEntity ")
    void deleteAll();

    @Query("delete from ClassFeesEntity where SectionId LIKE :sectionid")
    void deleteClassFeeForSection(String sectionid);

    @Query("delete from ClassFeesEntity where ClassId LIKE :classId")
    void deleteClassFee(String classId);

    @Insert(onConflict = REPLACE)
    void insertClassFee(List<ClassFeesEntity> classFeesEntity);
}
