package in.junctiontech.school.schoolnew.assignments;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Objects;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.AssignmentEntity;

public class AssignmentSubmissionActivity extends AppCompatActivity {
    TextView tv_subject,
            tv_assignment_date,
            tv_submission_date,
            tv_assignment;

    EditText et_remark;

    Button btn_upload_assignment;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_submission);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        tv_subject = findViewById(R.id.tv_subject);
        tv_assignment_date = findViewById(R.id.tv_assignment_date);
        tv_submission_date = findViewById(R.id.tv_submission_date);
        tv_assignment = findViewById(R.id.tv_assignment);
        et_remark = findViewById(R.id.et_remark);


        String assignment = getIntent().getStringExtra("assignment");

        if (!(assignment == null)) {

            AssignmentEntity assignmentEntity = new Gson().fromJson(assignment, AssignmentEntity.class);

            tv_subject.setText(assignmentEntity.SubjectName);
//            tv_assignment_date.setText(assignmentEntity.dateofhomework);
//            tv_submission_date.setText(assignmentEntity.dosubmission);
            tv_assignment.setText(assignmentEntity.homework);

        }else{
            finish();
        }
        setColorApp();
    }
}
