package in.junctiontech.school.schoolnew.SchoolLibrary;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
//import com.crashlytics.android.Crashlytics;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
//import io.fabric.sdk.android.Fabric;

public class Library extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private DbHandler db;
    private ArrayList<IssueBookDetails> libraryData = new ArrayList<>();
    private ArrayList<BooksDue> bookDueList = new ArrayList<>();

    private RecyclerView recycler_view_student_selection_layout;
    private MyRecycleAdapter madapter;

    private TextView tv_no_book_are_issued;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private boolean isDialogShowing = false;
    RadioButton rb_issued_books, rb_returned_books;
    ArrayList<BooksDue> issuedBooks = new ArrayList<>();
    ArrayList<BooksDue> returnedBooks = new ArrayList<>();

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        sharedPreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));
        setContentView(R.layout.student_list_for_selection);

        //  this.setSupportActionBar((Toolbar) findViewById(R.id.notification_toolbar));

//        if (sharedPreferences.getString("user_type", "Not Found").equalsIgnoreCase("Not Found")) {
//            startActivity(new Intent(this, FirstScreen.class));
//            finish();
//            overridePendingTransition(R.anim.enter, R.anim.nothing);
//        } else {


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
//        Fabric.with(this, new Crashlytics());


        setColorApp();

        recycler_view_student_selection_layout = findViewById(R.id.recycler_view_student_selection_layout);
        (findViewById(R.id.linear_layout_student_selection)).setVisibility(View.GONE);
        tv_no_book_are_issued = findViewById(R.id.tv_no_book_are_issued);

        progressbar = new ProgressDialog(this);
//            progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        madapter = new MyRecycleAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view_student_selection_layout.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recycler_view_student_selection_layout.setItemAnimator(itemAnimator);
        recycler_view_student_selection_layout.setAdapter(madapter);

        rb_issued_books = findViewById(R.id.rb_issued_books);
        rb_issued_books.setOnClickListener(v -> {
            bookDueList.clear();
            bookDueList.addAll(issuedBooks);
            madapter.notifyDataSetChanged();
            if (bookDueList.size() == 0)
                tv_no_book_are_issued.setVisibility(View.VISIBLE);
            else
                tv_no_book_are_issued.setVisibility(View.GONE);
        });
        rb_returned_books = findViewById(R.id.rb_returned_books);
        rb_returned_books.setOnClickListener(v -> {
            bookDueList.clear();
            bookDueList.addAll(returnedBooks);
            madapter.notifyDataSetChanged();
            if (bookDueList.size() == 0)
                tv_no_book_are_issued.setVisibility(View.VISIBLE);
            else
                tv_no_book_are_issued.setVisibility(View.GONE);
        });

        if (bookDueList.size() == 0)
            tv_no_book_are_issued.setVisibility(View.VISIBLE);

        try {
            getDueBooks();
        } catch (JSONException e) {
            e.printStackTrace();
        }
//            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context context, Intent intent) {
//
//                    if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
//                        Log.e("internet", "available");
//                        if (isDialogShowing) {
//                            isDialogShowing = false;
//
//                        }
//
//                        if (libraryData.size() == 0)
//                            try {
//                                getLibraryDataFromServer();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                    }
//                }
//            };
//        }

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1358708772", this, adContainer);
        }

    }

    @Override
    protected void onResume() {
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Config.INTERNET_AVAILABLE));

        super.onResume();


    }

    @Override
    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    void getDueBooks() throws JSONException {
        progressbar.show();

        String id;
        final String apiname;
        String role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);
        if (role.equalsIgnoreCase(Gc.STAFF)) {
            id = Gc.getSharedPreference(Gc.STAFFID, this);
            apiname = "library/staffBookIssue/";
        } else if (role.equalsIgnoreCase(Gc.STUDENTPARENT)) {
            id = Gc.getSharedPreference(Gc.STUDENTADMISSIONID, this);
            apiname = "library/studentBookIssue/";
        } else {
            Config.responseSnackBarHandler(getString(R.string.no_books_issued),
                    findViewById(R.id.student_list_for_selection_activity), R.color.fragment_first_blue);
            progressbar.cancel();
            return;
        }

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + apiname
                + new JSONObject().put("IRToDetail", id);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressbar.dismiss();
                String code = job.optString("code");
                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            bookDueList.clear();
                            issuedBooks.clear();
                            returnedBooks.clear();
                            libraryData = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("IssueBookDetails"),
                                            new TypeToken<List<IssueBookDetails>>() {
                                            }.getType());

                            for (int i = 0; i < libraryData.size(); i++) {
                                for (BookDetails b : libraryData.get(i).BookDetails) {
                                    BooksDue book = new BooksDue();
                                    book.DOD = libraryData.get(i).DOD;
                                    book.DOI = libraryData.get(i).DOI;
                                    if (Strings.nullToEmpty(b.BookReturnDate).equalsIgnoreCase("")) {
                                        book.AccessionNo = b.AccessionNo;
                                        book.AuthorName = b.AuthorName;
                                        book.BookName = b.BookName;
                                        book.BookReturnDate = b.BookReturnDate;
                                        book.Publisher = b.Publisher;
                                        book.SubjectName = b.SubjectName;
                                        book.Price = b.Price;
                                        book.PurposeValue = b.PurposeValue;
                                        issuedBooks.add(book);
                                    } else {
                                        book.AccessionNo = b.AccessionNo;
                                        book.AuthorName = b.AuthorName;
                                        book.BookName = b.BookName;
                                        book.BookReturnDate = b.BookReturnDate;
                                        book.Publisher = b.Publisher;
                                        book.SubjectName = b.SubjectName;
                                        book.Price = b.Price;
                                        book.PurposeValue = b.PurposeValue;
                                        returnedBooks.add(book);
                                    }
                                }
                            }

                            if (rb_issued_books.isChecked()) {
                                bookDueList.addAll(issuedBooks);
                            } else if (rb_returned_books.isChecked()) {
                                bookDueList.addAll(returnedBooks);
                            }

                            if (bookDueList.size() > 0)
                                tv_no_book_are_issued.setVisibility(View.GONE);
                            else tv_no_book_are_issued.setVisibility(View.VISIBLE);

                            madapter.notifyDataSetChanged();
                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.student_list_for_selection_activity), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, Library.this);

                        Intent intent1 = new Intent(Library.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, Library.this);

                        Intent intent2 = new Intent(Library.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.student_list_for_selection_activity), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(Library.this, error, findViewById(R.id.student_list_for_selection_activity));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public void getLibraryDataFromServer() throws JSONException {

//        progressbar.show();
//        JSONObject param = new JSONObject();
//        //   data.put("DB_Name", sharedPreferences.getString("organization_name", "Not Found"));
//
//      //  if (sharedPreferences.getString("user_type", "").equalsIgnoreCase("Teacher"))
//        String apiname;
//         if (Gc.getSharedPreference(Gc.USERTYPETEXT,this).equalsIgnoreCase("Student") ||
//                 Gc.getSharedPreference(Gc.USERTYPETEXT,this).equalsIgnoreCase("Parent")) {
//             param.put("IRToDetail", "AdmissionID"); // TODO
//            apiname = "library/studentBookIssue/";
//         }else  param.put("userType", "staff");
//        {
//            apiname = "library/staffBookIssue/";
//            param.put("IRToDetail", Gc.getSharedPreference(Gc.STAFFID, this));
//        }
//
//
//        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
//                + Gc.ERPAPIVERSION
//                + apiname
//                + param;
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, param
//                ,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject s) {
//                        progressbar.dismiss();
//
//                            JSONObject resJson =  s;
//                            if (resJson.optString("code").equalsIgnoreCase("200")) {
//                                BookLibrary dataBook = gson.fromJson(s.toString(), BookLibrary.class);
//                                libraryData = dataBook.getResult();
//                                madapter.updateList(libraryData);
//                                if (libraryData.size() == 0)
//                                    tv_no_book_are_issued.setVisibility(View.VISIBLE);
//                                else tv_no_book_are_issued.setVisibility(View.GONE);
//
//                            } else if (resJson.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    resJson.optString("code").equalsIgnoreCase("511")) {
//                                if ( ! isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(Library.this, resJson.optInt("code") + "", resJson.optString("message"));
//
//                                }
//
//                            }
//
//
//
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e("Library", volleyError.toString());
//                progressbar.dismiss();
//
//                String msg = "Error !";
//                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
//                    msg =  getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.internet_not_available_please_check_your_internet_connectivity);
//
//                } else if (volleyError instanceof AuthFailureError) {
//                    msg = "AuthFailureError!";
//                    //TODO
//                } else if (volleyError instanceof ServerError) {
//                    msg = "ServerError!";
//                    //TODO
//                } else if (volleyError instanceof NetworkError) {
//                    msg = "NetworkError!";
//                    //TODO
//                } else if (volleyError instanceof ParseError) {
//                    msg = "ParseError!";
//                    //TODO
//                }
//                alert.setTitle(getString(R.string.result));
//                alert.setMessage(msg);
//                 if (!isDialogShowing) {
//                        alert.show();
//                        isDialogShowing = true;
//                 }
//
//
//            }
//        })  ;
//
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }


    public class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {


//        public void updateList(List<BookLibrary> libraryData) {
//            this.libraryData = libraryData;
//            notifyDataSetChanged();
//        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_for_book_issue, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final BooksDue obj = bookDueList.get(position);

            //animate(holder);

            holder.book_issued_number.setText((position + 1) + ". ");
            holder.book_issued_name.setText(obj.BookName);
            holder.book_issued_date.setText(getString(R.string.issued_date) + "  : " + obj.DOI);

            if (Strings.nullToEmpty(obj.BookReturnDate).equalsIgnoreCase("")) {
                holder.book_issued_returned_date.setText(getString(R.string.returned_date) + "  : " + obj.DOD);
            } else {
                holder.book_issued_returned_date.setText("Return On  : " + obj.BookReturnDate);
            }

            holder.book_issued_description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alert_description = new AlertDialog.Builder(Library.this, AlertDialog.THEME_HOLO_LIGHT);
                    View view = getLayoutInflater().inflate(R.layout.activity_book_description, null);

                    ((TextView) view.findViewById(R.id.tv_accession_number)).setText(obj.AccessionNo);
                    ((TextView) view.findViewById(R.id.book_name)).setText(obj.BookName);
                    ((TextView) view.findViewById(R.id.book_authore_name)).setText(obj.AuthorName);
                    ((TextView) view.findViewById(R.id.book_subject_name)).setText(obj.SubjectName);
                    ((TextView) view.findViewById(R.id.book_publisher_name)).setText(obj.Publisher);
                    ((TextView) view.findViewById(R.id.book_price)).setText(obj.Price);

                    alert_description.setCancelable(true);
                    alert_description.setView(view);
                    alert_description.show();

                }
            });

        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(Library.this,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return bookDueList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView book_issued_description,
                    book_issued_number,
                    book_issued_name,
                    book_issued_date,
                    book_issued_returned_date;

            public MyViewHolder(View itemView) {
                super(itemView);
                book_issued_description = itemView.findViewById(R.id.book_issued_description);
                book_issued_number = itemView.findViewById(R.id.book_issued_number);
                book_issued_name = itemView.findViewById(R.id.book_issued_name);
                book_issued_date = itemView.findViewById(R.id.book_issued_date);
                book_issued_returned_date = itemView.findViewById(R.id.book_issued_returned_date);
                book_issued_returned_date.setVisibility(View.VISIBLE);
                itemView.startAnimation(AnimationUtils.loadAnimation(Library.this, R.anim.animator_for_bounce));
                book_issued_name.setTextColor(colorIs);
            }


        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(Library.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(Library.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    public void refreshActivity() {
        try {
            getDueBooks();
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Gson gson = new Gson();
//        String json = sharedPreferences.getString("libraryData", null);
//        Type type = new TypeToken<ArrayList<StudentData>>() {
//        }.getType();
//        libraryData = gson.fromJson(json, type);
//        madapter.updateList(libraryData);
//        madapter.notifyDataSetChanged();
    }

}

class IssueBookDetails {
    public String BookIssueId;
    public String Name;
    public String Mobile;
    public String Books;
    public String DOD;
    public String DOI;
    public String BookReturn;
    public String Remarks;
    public ArrayList<BookDetails> BookDetails;
}

class BookDetails {
    public String ListBookId;
    public String BookName;
    public String AuthorName;
    public String AccessionNo;
    public String BookReturnDate;
    public String Publisher;
    public String Price;
    public String SubjectName;
    public String PurposeValue;

}

class BooksDue {
    public String BookName;
    public String AuthorName;
    public String AccessionNo;
    public String DOD;
    public String DOI;
    public String BookReturnDate;
    public String Publisher;
    public String Price;
    public String SubjectName;
    public String PurposeValue;
}