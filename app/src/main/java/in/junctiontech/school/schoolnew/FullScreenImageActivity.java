package in.junctiontech.school.schoolnew;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GestureDetectorCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class FullScreenImageActivity extends AppCompatActivity {

    PhotoView imageView;

    FloatingActionButton btn_next,btn_prev;

    ArrayList<String> images = new ArrayList<>();

    CircularProgressDrawable circularProgressDrawable;

    Boolean fullScreen = false;

    int index = 0;

    File myDir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(v -> {

            displayNext();
        });
        btn_prev = findViewById(R.id.btn_prev);
        btn_prev.setOnClickListener(v -> {

            displayPrev();
        });

        myDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MySchool/");

        File myInternalDir = getFilesDir();
        Log.i("Dir: ", myInternalDir.getAbsolutePath());

        if(!myDir.exists()){
            if(!(myDir.mkdir())) {
                Log.e("FolderCreation", "Failed");
            }
        }

        if (getIntent().getCharSequenceExtra("title") != null) {
            getSupportActionBar().setTitle(getIntent().getCharSequenceExtra("title"));
        }
        images = getIntent().getStringArrayListExtra("images");

            index = getIntent().getIntExtra("index",0);

        if (images == null || images.size() == 0){
            finish();
            return;
        }

        if (images.size() > 1){
            btn_prev.setVisibility(View.VISIBLE);
            btn_next.setVisibility(View.VISIBLE);
        }else{
            btn_prev.setVisibility(View.GONE);
            btn_next.setVisibility(View.GONE);
        }

        imageView = findViewById(R.id.imageView);
        imageView.setOnClickListener(v -> {
            if (fullScreen){
                Objects.requireNonNull(getSupportActionBar()).show();
                btn_prev.show();
                btn_next.show();
                fullScreen = false;
            }else{
                Objects.requireNonNull(getSupportActionBar()).hide();
                btn_next.hide();
                btn_prev.hide();
                fullScreen = true;

            }
        });
        Glide.with(this)
                .load(images.get(index))
                .apply(RequestOptions.placeholderOf(circularProgressDrawable))
                .apply(RequestOptions.errorOf(R.drawable.ic_homework))
                .apply(RequestOptions.overrideOf(600,1200))
                .apply(RequestOptions.fitCenterTransform())
                .into(imageView);

    }

    void displayNext(){
        if ((index+1) >= (images.size() - 1)){
            index = images.size() - 1 ;
            Config.responseSnackBarHandler(getString(R.string.last),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
        }else{
            index = index +1;
        }
        Glide.with(this)
                .load(images.get(index))
                .apply(RequestOptions.placeholderOf(circularProgressDrawable))
                .apply(RequestOptions.errorOf(R.drawable.ic_homework))
                .apply(RequestOptions.overrideOf(600,1200))
                .apply(RequestOptions.fitCenterTransform())
                .into(imageView);

    }

    void displayPrev(){
        if(index <= 0 ){
            index = 0;
            Config.responseSnackBarHandler(getString(R.string.first),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
        }else {
            index = index - 1;
        }
        Glide.with(this)
                .load(images.get(index))
                .apply(RequestOptions.placeholderOf(circularProgressDrawable))
                .apply(RequestOptions.errorOf(R.drawable.ic_homework))
                .apply(RequestOptions.overrideOf(600,1200))
                .apply(RequestOptions.fitCenterTransform())
                .into(imageView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_image_actions, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.action_save:
//
//            saveImage();
//            break;
//
//            case R.id.menu_item_share:
//                Drawable dr = imageView.getDrawable();
//
//                Bitmap bm=((BitmapDrawable)imageView.getDrawable()).getBitmap();
//
//
//                Log.e("image", "found");
//                break;
            case R.id.action_rotate:
                imageView.setRotation(imageView.getRotation() + 90);

                break;

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
