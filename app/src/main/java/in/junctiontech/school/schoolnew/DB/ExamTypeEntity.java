package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class ExamTypeEntity {
    @PrimaryKey
    @NonNull
    public String ExamTypeID;
    public String Exam_Type;
    public String Exam_Status;
    public String DOC;
    public String DOU;
    public String Remarks;
    public String Duration;
}
