package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SignedInStaffInformationDao {
    @Query("select * from SignedInStaffInformationEntity limit 1")
    LiveData<SignedInStaffInformationEntity> getSignedInStaff();

    @Insert(onConflict = REPLACE)
    void insertSignedInStaffInformation(SignedInStaffInformationEntity signedInStaffInformationEntity);
}
