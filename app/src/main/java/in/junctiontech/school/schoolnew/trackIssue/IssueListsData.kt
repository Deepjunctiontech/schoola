package `in`.junctiontech.school.schoolnew.trackIssue


import java.io.Serializable
import java.util.ArrayList

/*
 * Created by LEKHPAL DANGI on 10-04-2019.
*/
class IssueListsData : Serializable {
   // lateinit var result: ArrayList<IssueListsData>
    var bugID: String = ""
    var bugCode: String = ""
    var mediaPath: String = ""
    var bugPriority: String = ""
    var applicationID: String = ""
    var email: String = ""
    var mobile: String = ""
    var databaseName: String = ""
    var reportDevice: String = ""
    var url: String = ""
    var title: String = ""
    var description: String = ""
    var status: String = ""
    var developerUserID: String = ""
    var reporterUserID: String = ""
    var openDate: String = ""
    var closeDate: String = ""
    var bugStatus: String = ""
    var username: String = ""
}