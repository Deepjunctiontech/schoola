package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;
@Dao
public interface OrganizationPermissionDao {


    @Query("Select * from OrganizationPermissionEntity")
    public LiveData<List<String>> getOrganizationPermissions();

    @Query("delete from OrganizationPermissionEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertOrganizationPermission(List<OrganizationPermissionEntity> organizationPermissionEntity);
    }

