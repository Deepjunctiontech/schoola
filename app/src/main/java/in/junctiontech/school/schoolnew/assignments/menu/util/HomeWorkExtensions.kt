package `in`.junctiontech.school.schoolnew.assignments.menu.util

import android.content.res.Resources

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).toInt()