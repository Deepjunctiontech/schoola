package in.junctiontech.school.schoolnew.transport.fee;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class TransportFeeDistanceAdapter extends RecyclerView.Adapter<TransportFeeDistanceAdapter.MyViewHolder> {
    private ArrayList<TransportDistanceAmount> masterEntryEntities;
    private Context context;
    int colorIs;

    public TransportFeeDistanceAdapter(Context context, ArrayList<TransportDistanceAmount> masterEntryEntities){
        this.context = context;
        this.masterEntryEntities = masterEntryEntities;
        colorIs = Config.getAppColor((Activity) context, true);

    }


    @NonNull
    @Override
    public TransportFeeDistanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transport_distance_edit, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransportFeeDistanceAdapter.MyViewHolder holder, final int position) {
        holder.tv_distance.setText(masterEntryEntities.get(position).MasterEntryValue);
        holder.tv_distance.setTextColor(colorIs);
        holder.et_distance_amount.setText(masterEntryEntities.get(position).Amount);
        holder.et_distance_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i("changed", editable.toString());
                masterEntryEntities.get(position).Amount = editable.toString();

            }
        });
    }

    @Override
    public int getItemCount() {
        return masterEntryEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_distance;
        EditText et_distance_amount;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            et_distance_amount = itemView.findViewById(R.id.et_distance_amount);



//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    AlertDialog.Builder choices = new AlertDialog.Builder(context);
//                    String[] aa = new String[]
//                            { context.getResources().getString(R.string.change),
//                                    context.getResources().getString(R.string.enter_amount)
//                            };
//
//                    choices.setItems(aa, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int which) {
//                            switch (which){
//                                case 0:
////                                    ((TransportFeeSetupActivity) context).updateTransportDistanceDialog(masterEntryEntities.get(getAdapterPosition()));
//
//                                    break;
//                                case 1:
//                                    updateAmount(getAdapterPosition());
//                                    break;
//                            }
//                        }
//                    });
//                    choices.show();
//                }
//            });
        }
    }

    public ArrayList<TransportDistanceAmount> getMasterEntryEntities(){
        return masterEntryEntities;
    }


//    void updateAmount(final int adapterPosition){
//        final Dialog dialog = new Dialog(context);
//        dialog.setContentView(R.layout.dialog_single_number);
//        dialog.setTitle(R.string.update);
//
//        final EditText editText =  dialog.findViewById(R.id.et_int_amount);
//        editText.setText(masterEntryEntities.get(adapterPosition).Amount);
//
//        Button btnSave          =  dialog.findViewById(R.id.save);
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    masterEntryEntities.get(adapterPosition).Amount = editText.getText().toString();
//                    dialog.dismiss();
//                notifyDataSetChanged();
//
//            }
//        });
//
//        Button btnCancel        =  dialog.findViewById(R.id.cancel);
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//
//    }
}
