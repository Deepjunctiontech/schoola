package in.junctiontech.school.schoolnew.model;

/**
 * Created by deep on 27/03/18.
 */

public class SignedInUserInformation {
    public String userID;
    public String userType  ;
    public String userTypeValue;
    public String userName;
    public StaffInformation Staff;
    public StaffInformation Admin;
    public StudentInformation Parent;
    public StudentInformation Student;
    public String accessToken ;

}
