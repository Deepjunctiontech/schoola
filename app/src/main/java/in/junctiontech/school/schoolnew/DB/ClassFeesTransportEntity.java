package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class ClassFeesTransportEntity {
    public String ClassId;
    public String ClassName;
    public String SectionId;
    public String SectionName;
    public String Session;
    @PrimaryKey
    @NonNull
    public String FeeId;
    public String FeeTypeID;
    public String FeeType;
    public String Frequency;
    public String FeeStatus;
    public String DOE;
    public String Amount;
    public String Distance;
    public String MasterEntryValue;
}
