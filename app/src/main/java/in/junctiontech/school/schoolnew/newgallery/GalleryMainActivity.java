package in.junctiontech.school.schoolnew.newgallery;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.FullScreenImageActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.SchoolGallery;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GalleryMainActivity extends AppCompatActivity {

    ArrayList<SchoolGallery> galleryList = new ArrayList<>();
    ArrayList<String> galleryImageURLs = new ArrayList<>();
    RecyclerView mRecyclerView;
    Adapter adapter;
    ImageView iv_stom;

    FloatingActionButton fab_add_gallery;

    CircularProgressDrawable circularProgressDrawable;

    ProgressBar progressBar ;

    String[] permissions;

    String whichImage  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_main);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        progressBar = findViewById(R.id.progressBar);
        mRecyclerView = findViewById(R.id.mRecyclerView);

        iv_stom = findViewById(R.id.iv_stom);
        iv_stom.setOnClickListener(v -> {

            if (Gc.getSharedPreference(Gc.GALLERYEDITALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {

                permissions = new String[]{
                        getResources().getString(R.string.show),
//                                    context.getResources().getString(R.string.evaluate),
                        getResources().getString(R.string.update)
                        //                context.getResources().getString(R.string.assignment_submission)

                };

                AlertDialog.Builder choices = new AlertDialog.Builder(GalleryMainActivity.this);

                choices.setItems(permissions, (dialogInterface, which) -> {

                    switch (which) {
                        case 0:
                            Intent intent = new Intent(GalleryMainActivity.this, FullScreenImageActivity.class);

                            ArrayList<String> stom = new ArrayList<>();
                            stom.add(Gc.getSharedPreference(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, this));

                            intent.putStringArrayListExtra("images", stom);
                            intent.putExtra("title", getString(R.string.gallery));
                            startActivity(intent);
                            break;
                        case 1:
                            if (takePermission()) {
                                whichImage = "stom";

                                CropImage.activity()
                                        .start(this);

                            } else {
                                whichImage = null;
                                Config.responseSnackBarHandler(getString(R.string.permission_denied_external_storage),
                                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
                            }

                            break;
                    }

                });
                choices.show();
            }else {
                Intent intent = new Intent(GalleryMainActivity.this, FullScreenImageActivity.class);

                ArrayList<String> stom = new ArrayList<>();
                stom.add(Gc.getSharedPreference(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, this));

                intent.putStringArrayListExtra("images", stom);
                intent.putExtra("title", getString(R.string.gallery));
                startActivity(intent);
            }

        });

        fab_add_gallery = findViewById(R.id.fab_add_gallery);
        fab_add_gallery.setOnClickListener(v -> {
            if (takePermission()) {

                permissions = new String[]{
                        getResources().getString(R.string.add_spotlight),
//                                    context.getResources().getString(R.string.evaluate),
                        getResources().getString(R.string.add_gallery)
                        //                context.getResources().getString(R.string.assignment_submission)

                };
                AlertDialog.Builder choices = new AlertDialog.Builder(GalleryMainActivity.this);

                choices.setItems(permissions, (dialogInterface, which) -> {

                    switch (which) {
                        case 0:
                            whichImage = "stom";
                            CropImage.activity()
                                    .start(this);
                            break;
                        case 1:
                            whichImage = "gallery";
                            CropImage.activity()
                                    .start(this);
                            break;
                    }

                });
                choices.show();

            }
            else {
                whichImage = null;
                Config.responseSnackBarHandler(getString(R.string.permission_denied_external_storage),
                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }

        });

        String galleryImages = Gc.getSharedPreference(Gc.GALLERYDATA, this);

        if (Gc.NOTFOUND.equalsIgnoreCase(galleryImages) || "".equalsIgnoreCase(Strings.nullToEmpty(galleryImages))) {
            Config.responseSnackBarHandler(getString(R.string.not_available),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
        }else{

            galleryList = new Gson().fromJson(galleryImages, new TypeToken<List<SchoolGallery>>() {}.getType());

            if (galleryList != null) {
                for (SchoolGallery g : galleryList) {
                    galleryImageURLs.add(g.originalpath + g.imagename);
                }
            }
        }

        setSTOM();


        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new Adapter();
        mRecyclerView.setAdapter(adapter);

        fetchImagesUrl();

        if (Gc.getSharedPreference(Gc.GALLERYEDITALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            fab_add_gallery.show();
        }else{
            fab_add_gallery.hide();
        }

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/6850708603",this,adContainer);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            CropImage.activity()
                    .start(this);

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Uri originalUri = result.getOriginalUri();
                Log.i("Uri", resultUri.toString());
                Log.i("Original uri", originalUri.toString());
                File f = new File(resultUri.getPath());
                long size = f.length();

                int compression ;
                if ( size > 0){
                    compression = Gc.findCompressRatio(size);
                }else {
                    Config.responseSnackBarHandler(getString(R.string.report_not_submitted_try_again),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    return;
                }

                if (compression == 0){
                    Config.responseSnackBarHandler(getString(R.string.not_available),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }

                try {
                    progressBar.setVisibility(View.VISIBLE);

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

                    Toast.makeText(this, "size is "+ String.valueOf(bitmap.getAllocationByteCount()),Toast.LENGTH_LONG).show();

                    String encodedImage = Gc.compressAndGetStringImage(bitmap, compression);

                    int i = encodedImage.length();

                    if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("gallery")){
                        uploadImageToServer(encodedImage, f.getName());

                    }else if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("stom")){
                        uploadHighLightImageToServer(encodedImage, f.getName());
                    }else {
                        Config.responseSnackBarHandler(getString(R.string.report_not_submitted_try_again),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    }


                } catch (IOException e) {
                    progressBar.setVisibility(View.GONE);

                    e.printStackTrace();
                }catch (JSONException j){
                    progressBar.setVisibility(View.GONE);

                    j.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }else{
                Config.responseSnackBarHandler(getString(R.string.select_your_choice),
                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }

        }

    private void uploadImageToServer(String stringImage, String nameImage) throws JSONException {
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/galleries"
                ;
        Log.e("GalleryUrl", url);

        final JSONObject param = new JSONObject();
        param.put("fileName", /*ReportQuery.randomAlphaNumeric(6)+".jpg"+*/nameImage);
        param.put("fileData", stringImage);

//        Log.e("GalleryRES", jsonObject.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), jsonObject -> {
            Log.e("GalleryRES", jsonObject.toString());
            progressBar.setVisibility(View.GONE);

            String code = jsonObject.optString("code");

            switch (code) {

                case Gc.APIRESPONSE200:
                    fetchImagesUrl();
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, GalleryMainActivity.this);

                    Intent intent1 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                    Intent intent2 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;
            }
        }, volleyError -> {
            Log.e("GalleryRES", volleyError.toString());
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(GalleryMainActivity.this,volleyError,findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);

    }


    private void uploadHighLightImageToServer(String stringImage, String nameImage) throws JSONException {
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/studentOfThaMonth"
                ;
        Log.e("GalleryUrl", url);

        final JSONObject param = new JSONObject();
        param.put("fileName", /*ReportQuery.randomAlphaNumeric(6)+".jpg"+*/nameImage);
        param.put("fileData", stringImage);
        param.put("notificationMessage", "HighLight");

//        Log.e("GalleryRES", jsonObject.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), jsonObject -> {
            Log.e("GalleryRES", jsonObject.toString());
            progressBar.setVisibility(View.GONE);

            String code = jsonObject.optString("code");

            switch (code) {

                case Gc.APIRESPONSE200:
                    fetchImagesUrl();
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, GalleryMainActivity.this);

                    Intent intent1 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                    Intent intent2 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;
                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;
            }
        }, volleyError -> {
            Log.e("GalleryRES", volleyError.toString());
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(GalleryMainActivity.this,volleyError,findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);

    }


    private void deleteImageFromServer(String nameImage) throws JSONException {
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/galleries/"
                + new JSONObject().put("imagename", nameImage)
                ;
        Log.e("GalleryUrl", url);


//        Log.e("GalleryRES", jsonObject.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), jsonObject -> {
            Log.e("GalleryRES", jsonObject.toString());
            progressBar.setVisibility(View.GONE);

            String code = jsonObject.optString("code");

            switch (code) {

                case Gc.APIRESPONSE200:
                    fetchImagesUrl();
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, GalleryMainActivity.this);

                    Intent intent1 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                    Intent intent2 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
                    break;
            }
        }, volleyError -> {
            Log.e("GalleryRES", volleyError.toString());
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(GalleryMainActivity.this,volleyError,findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);

    }



    void setSTOM(){
        if(Gc.getSharedPreference(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, this).equalsIgnoreCase(Gc.NOTFOUND)){
            iv_stom.setVisibility(View.GONE);
        }else{
            iv_stom.setVisibility(View.VISIBLE);

            if (!(GalleryMainActivity.this.isFinishing()))
            Glide.with(GalleryMainActivity.this)
                    .load(Gc.getSharedPreference(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, this))
                    .apply(RequestOptions.placeholderOf(circularProgressDrawable))
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(iv_stom);
        }
    }

    private void fetchImagesUrl() {

        progressBar.setVisibility(View.VISIBLE);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/galleries"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), jsonObject -> {
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

                switch (code){

                    case Gc.APIRESPONSE200:

                        try {
                            HashMap<String,String> setDefaults = new HashMap<>();

                            setDefaults.put(Gc.GALLERYDATA, jsonObject.getJSONObject("result").optString("galleries"));
                            Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                            galleryList.clear();
                            galleryImageURLs.clear();

                        String galleryImages = null;
                        try {
                            galleryImages = jsonObject.getJSONObject("result").optString("galleries");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if ( "".equalsIgnoreCase(Strings.nullToEmpty(galleryImages))) {
                            Config.responseSnackBarHandler(getString(R.string.not_available),
                                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
                        }else {

                            galleryList = new Gson().fromJson(galleryImages, new TypeToken<List<SchoolGallery>>() {
                            }.getType());
                            for (SchoolGallery g : galleryList) {
                                galleryImageURLs.add(g.originalpath + g.imagename);
                            }
                            adapter.notifyDataSetChanged();
                        }


                    try {
                        JSONObject stom = jsonObject.optJSONObject("result")
                                .optJSONArray("studentOfTheMonth")
                                .getJSONObject(0);

                        if (stom != null) {

                            HashMap<String,String> setDefaults = new HashMap<>();

                            setDefaults.put(Config.STUDENT_OF_THE_MONTH_IMAGE_URL,
                                    stom.optString("originalpath") + stom.optString("imagename"));

                            Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                            setSTOM();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, GalleryMainActivity.this);

                        Intent intent1 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, GalleryMainActivity.this);

                        Intent intent2 = new Intent(GalleryMainActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        progressBar.setVisibility(View.GONE);
                        Config.responseSnackBarHandler(jsonObject.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
        }, error -> {
        progressBar.setVisibility(View.GONE);

        Config.responseVolleyErrorHandler(GalleryMainActivity.this,error,findViewById(R.id.top_layout));

        }){
@Override
public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();

        headers.put("APPKEY",Gc.APPKEY);
        headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
        headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
        headers.put("Content-Type", Gc.CONTENT_TYPE);
        headers.put("DEVICE", Gc.DEVICETYPE);
        headers.put("DEVICEID",Gc.id(getApplicationContext()));
        headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
        headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
        headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
        headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

        return headers;
        }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
        }


    public class Adapter extends  RecyclerView.Adapter<Adapter.MyViewHolder> {

        @NonNull
        @Override
        public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull Adapter.MyViewHolder holder, int position) {
            Glide.with(GalleryMainActivity.this)
                    .load(galleryList.get(position).thumbnailpath + galleryList.get(position).imagename)
                    .apply(RequestOptions.placeholderOf(circularProgressDrawable))
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(holder.item_gallery_iv);
        }

        @Override
        public int getItemCount() {
            return galleryList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView item_gallery_iv/*,item_gallery_iv_share*/;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                item_gallery_iv = itemView.findViewById(R.id.item_gallery_iv);

                itemView.setOnClickListener(v -> {
                    if (Gc.getSharedPreference(Gc.GALLERYEDITALLOWED, GalleryMainActivity.this).equalsIgnoreCase(Gc.TRUE)){

                        permissions = new String[]{
                                getResources().getString(R.string.show),
//                                    context.getResources().getString(R.string.evaluate),
                                getResources().getString(R.string.delete)
                                //                context.getResources().getString(R.string.assignment_submission)

                        };

                        AlertDialog.Builder choices = new AlertDialog.Builder(GalleryMainActivity.this);

                        choices.setItems(permissions, (dialogInterface, which) -> {

                            switch (which) {
                                case 0:
                                    Intent intent = new Intent(GalleryMainActivity.this, FullScreenImageActivity.class);

                                    intent.putStringArrayListExtra("images",galleryImageURLs);
                                    intent.putExtra("index", getAdapterPosition());
                                    intent.putExtra("title", getString(R.string.gallery));
                                    startActivity(intent);
                                    break;
                                case 1:

                                    try {
                                        deleteImageFromServer(galleryList.get(getAdapterPosition()).imagename);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    break;
                            }

                        });
                        choices.show();
                    }else{
                        Intent intent = new Intent(GalleryMainActivity.this, FullScreenImageActivity.class);


                        intent.putStringArrayListExtra("images",galleryImageURLs);
                        intent.putExtra("index", getAdapterPosition());
                        intent.putExtra("title", getString(R.string.gallery));
                        startActivity(intent);
                    }


                });

            }
        }
    }

    private boolean takePermission() {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);
            }
            return false;
        } else return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
