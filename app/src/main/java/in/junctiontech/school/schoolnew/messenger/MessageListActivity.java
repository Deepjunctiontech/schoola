package in.junctiontech.school.schoolnew.messenger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.chatdb.ChatDatabase;
import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;

import static in.junctiontech.school.schoolnew.common.Gc.SCHOOLPREF;

/**
 * Created by Deep on May 14 2018.
 */

public class MessageListActivity extends AppCompatActivity {

    ChatDatabase chatDb;

    private ArrayList<ChatListEntity> chatList = new ArrayList<>();
    ArrayList<String> chatPermissions = new ArrayList<>();

    private String with, withType;

    private EditText et_message_chat;
    private RecyclerView mRecyclerView;
    private MessageListAdapter adapter;

    ClassNameSectionName selectedSection;
    SchoolStudentEntity selectedStudent;
    SchoolStaffEntity selectedStaff;

    Boolean hasPermission = true;

    private void setColorApp() {
        int colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat_layout);

        MainDatabase mDb = MainDatabase.getDatabase(this);

        chatDb = ChatDatabase.getDatabase(this);

        ActionBar actionBar = getSupportActionBar();
        Objects.requireNonNull(actionBar).setDisplayHomeAsUpEnabled(true);

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1718156640", this, adContainer);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_landscape);
        else
            findViewById(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_vartical);

        et_message_chat = findViewById(R.id.et_message_chat);
        Button btn_sendChat = findViewById(R.id.btn_sendChat);
        mRecyclerView = findViewById(R.id.recycler_view_chat_layout);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new MessageListAdapter(this, chatList);
        mRecyclerView.setAdapter(adapter);

        mDb.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, this))
                .observe(this, communicationPermissions -> {
                    chatPermissions.clear();
                    for (CommunicationPermissions c : communicationPermissions) {
                        chatPermissions.add(c.ToUserType);
                    }
                });

        btn_sendChat.setOnClickListener(view -> sendChat());

        if (Strings.nullToEmpty(getIntent().getStringExtra("previous")).equalsIgnoreCase("")) {
            String userType = getIntent().getStringExtra("withType");

            assert userType != null;
            switch (userType) {
                case "section":
                    selectedSection = new Gson()
                            .fromJson(getIntent().getStringExtra("with"), ClassNameSectionName.class);
                    with = selectedSection.SectionId;
                    withType = "section";
                    break;
                case "student":
                    selectedStudent = new Gson()
                            .fromJson(getIntent().getStringExtra("with"), SchoolStudentEntity.class);
                    with = selectedStudent.AdmissionId;
                    withType = "student";
                    break;
                case "staff":
                    selectedStaff = new Gson()
                            .fromJson(getIntent().getStringExtra("with"), SchoolStaffEntity.class);
                    with = selectedStaff.UserId;
                    withType = "staff";
                    break;
            }
        } else {
            ChatListEntity chat = new Gson().fromJson(getIntent().getStringExtra("previous"), ChatListEntity.class);
            with = chat.withId;
            withType = chat.withType;
            if (("school").equalsIgnoreCase(chat.withId)) {
                et_message_chat.setVisibility(View.GONE);
                btn_sendChat.setVisibility(View.GONE);
            }

        }

        switch (withType) {
            case "admin":
                Objects.requireNonNull(getSupportActionBar()).setTitle("admin");
                break;
            case "student":
                if (selectedStudent == null)
                    mDb.schoolStudentModel().getStudentById(with).observe(this, schoolStudentEntity -> {
                        if (schoolStudentEntity == null)
                            return;
                        selectedStudent = schoolStudentEntity;
                        Objects.requireNonNull(getSupportActionBar()).setTitle(selectedStudent.StudentName);

                    });
                else {
                    Objects.requireNonNull(getSupportActionBar()).setTitle(selectedStudent.StudentName);
                }
                break;
            case "staff":
                if (selectedStaff == null)
                    mDb.schoolStaffModel().getStaffByUserId(with).observe(this, staffEntity -> {
                        selectedStaff = staffEntity;
                        Objects.requireNonNull(getSupportActionBar()).setTitle(selectedStaff.StaffName);
                    });
                else
                    Objects.requireNonNull(getSupportActionBar()).setTitle(selectedStaff.StaffName);
                break;
            case "section":
                if (selectedSection == null) {
                    mDb.schoolClassSectionModel().getClassSectionNameSingle(with).observe(this, classNameSectionName -> {
                        if (classNameSectionName != null) {
                            selectedSection = classNameSectionName;
                            Objects.requireNonNull(getSupportActionBar())
                                    .setTitle(selectedSection.ClassName + " " + selectedSection.SectionName);
                        }
                    });
                } else
                    Objects.requireNonNull(getSupportActionBar())
                            .setTitle(selectedSection.ClassName + " " + selectedSection.SectionName);
                break;
        }

        chatDb.chatListModel().getPendingChats(with, withType).observe(this, chatListEntities -> {
            Log.i("unsent messages", chatListEntities.size() + "");
           /* if (chatListEntities.size()>0) {
//                    try {
//                        sendChatTextToServer(chatListEntities);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
            }*/
        });

        setColorApp();
    }

    private void sendChat() {
        if (et_message_chat.getText().toString().isEmpty()) return;

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        String dateToStr = format.format(today);

        final ChatListEntity chatListEntity = new ChatListEntity();
        chatListEntity.withType = withType;
        chatListEntity.withId = with;

        switch (withType) {
            case "student":
                if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
                    chatListEntity.withName = selectedStudent.StudentName;
                    chatListEntity.withProfileUrl = selectedStudent.studentProfileImage;
                    hasPermission = true;
                } else {
                    hasPermission = false;
                }
                break;
            case "staff":
                if (chatPermissions.contains(selectedStaff.UserTypeValue)
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)
                ) {
                    chatListEntity.withName = selectedStaff.StaffName;
                    chatListEntity.withProfileUrl = selectedStaff.staffProfileImage;
                    hasPermission = true;
                } else {
                    hasPermission = false;
                }
                break;
            case "admin":
                chatListEntity.withName = "admin";
                chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this);
                if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
                    if (Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equalsIgnoreCase(Gc.DEMO)) {
                        hasPermission = true;
                    } else {
                        hasPermission = false;
                    }
                } else {
                    hasPermission = true;
                }

                break;
            case "section":
                if (chatPermissions.contains("STUDENT")
                        || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)
                ) {
                    if (selectedSection == null) {
                        Toast.makeText(this, getResources().getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show();
                        hasPermission = false;
                    } else {
                        chatListEntity.withName = selectedSection.ClassName + " " + selectedSection.SectionName;
                        chatListEntity.withProfileUrl = "";
                        hasPermission = true;
                    }

                } else {
                    hasPermission = false;
                }
                break;
        }
        if (hasPermission) {
            chatListEntity.byMe = 1;
            chatListEntity.msg = et_message_chat.getText().toString();
            chatListEntity.enteredDate = dateToStr;
            chatListEntity.msgType = "text";
            chatListEntity.sent = 0;
            chatListEntity.msgId = Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                    + "-"
                    + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                    + "-"
                    + Gc.getSharedPreference(Gc.USERID, this)
                    + "-"
                    + (new Date().getTime())
            ;

            new Thread(() -> chatDb.chatListModel().insertChat(chatListEntity)).start();

            chatList.add(chatListEntity);
            adapter.notifyItemInserted(chatList.size() - 1);


            try {
                sendChatTextToServerSingle(chatListEntity);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            et_message_chat.getText().clear();
        } else {
            Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                    findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        SharedPreferences pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Gc.CHATWINDOWACTIVE, true);
        editor.apply();

        super.onResume();
        chatDb.chatListModel().getChatWithUser(withType, with).observe(this, chatListEntities -> {
            chatList.clear();
            chatList.addAll(chatListEntities);
            adapter.notifyDataSetChanged();

            if (chatList.size() > 0) {
                mRecyclerView.smoothScrollToPosition(chatList.size() - 1);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Gc.CHATWINDOWACTIVE, false);
        editor.apply();

        chatDb.chatListModel().getChatWithUser(withType, with).removeObservers(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    void sendChatTextToServerSingle(final ChatListEntity c) throws JSONException {

        final JSONArray param = new JSONArray();

        JSONObject j = new JSONObject();
        j.put("msgId", c.msgId);
        j.put("withType", withType);
        if (withType.equalsIgnoreCase("section")) {
            j.put("withName", c.withName);
        } else {
            j.put("withName", Gc.getSharedPreference(Gc.SIGNEDINUSERDISPLAYNAME, this));
        }

        j.put("withId", c.withId);
        j.put("msgType", "text");
        j.put("msg", c.msg);
        j.put("enteredDate", c.enteredDate);
        param.put(j);

        Log.e("chatParam", param.toString());

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "chat/chat";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case "200":
                    new Thread(() -> chatDb.chatListModel().updateSentStatus(c.msgId)).start();
                    break;
                case "300":
                    android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alertUpdate.setTitle(job.optString("message"));
                    alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                    alertUpdate.setPositiveButton(R.string.yes, (dialog, which) -> {
                        JSONObject p = new JSONObject();
                        try {
                            p.put("orgKey", Gc.getSharedPreference(Gc.ERPINSTCODE, this));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLink(Uri.parse("https://education.zeroerp.com/Login/verifyOrganization/"
                                        + Gc.getSharedPreference(Gc.ERPINSTCODE, this) + "?orgKey=" + p))
                                .setDomainUriPrefix("https://kn3yy.app.goo.gl")
                                // Open links with this app on Android
                                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID).build())
                                // Open links with com.example.ios on iOS
                                .setIosParameters(new DynamicLink.IosParameters.Builder("in.junctiontech.school")
                                        .setAppStoreId("1261309387")
                                        .build())
                                .buildDynamicLink();

                        Uri dynamicLinkUri = dynamicLink.getUri();

                        FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLongLink(Uri.parse(dynamicLinkUri.toString()))
                                .buildShortDynamicLink()
                                .addOnCompleteListener(this, task -> {
                                    if (task.isSuccessful()) {
                                        if (task.getResult() != null) {
                                            Uri shortLink = task.getResult().getShortLink();
                                            String refererBody1 = " " + shortLink;
                                            Intent refererIntent1 = new Intent(Intent.ACTION_SEND);
                                            refererIntent1.setType("text/plain");
                                            refererIntent1.putExtra(Intent.EXTRA_TEXT, refererBody1);

                                            startActivity(Intent.createChooser(refererIntent1, "Share App"));
                                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                                        }
                                    } else {
                                        Log.e("main", " error " + task.getException());
                                    }
                                });
                    });
                    alertUpdate.setNegativeButton(R.string.no, null);
                    alertUpdate.setCancelable(false);
                    alertUpdate.show();

                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue);
            }
        }, error -> {
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

}
