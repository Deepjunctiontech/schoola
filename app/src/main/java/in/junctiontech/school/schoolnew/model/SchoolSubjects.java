package in.junctiontech.school.schoolnew.model;

import java.util.ArrayList;

/**
 * Created by deep on 27/03/18.
 */

public class SchoolSubjects {
    public String SubjectId;
    public String Session;
    public String SubjectName;
    public String SubjectAbb;
    public String Class;
    public String SubjectStatus;
    public String DOE;
    public String DOL;
    public ArrayList<SubjectClass> classSection;

}
