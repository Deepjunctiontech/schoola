package `in`.junctiontech.school.schoolnew.chat.fragment

import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolStaffEntity
import `in`.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity
import `in`.junctiontech.school.schoolnew.chat.ChatConversationActivity
import `in`.junctiontech.school.schoolnew.chat.adapter.StaffListFragmentAdapter
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.CommunicationPermissions
import `in`.junctiontech.school.schoolnew.model.StudentInformation
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import java.util.*

class StaffListFragment : Fragment(), StaffListFragmentAdapter.StaffClickListner {
    var mDb: MainDatabase? = null
    lateinit var adapter: StaffListFragmentAdapter
    private lateinit var contexts: Context
    private var staffList = ArrayList<SchoolStaffEntity>()
    private var staffFilterList = ArrayList<SchoolStaffEntity>()
    private var chatPermissions = ArrayList<String>()
    private lateinit var staffInfo: SignedInStaffInformationEntity
    private lateinit var studentInfo: StudentInformation
    lateinit var mRecyclerView: RecyclerView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val bundle = this.arguments

            if (bundle != null) {
                val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)
                if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                    val s = bundle.getString(Gc.STUDENTPARENT)
                    if (s != null) {
                        studentInfo = Gson().fromJson(s, StudentInformation::class.java)
                    }
                } else if (role.equals(Gc.STAFF, ignoreCase = true)) {
                    val s = bundle.getString(Gc.STAFF)
                    if (s != null) {
                        staffInfo = Gson().fromJson(s, SignedInStaffInformationEntity::class.java)
                    }
                }
            }

        mDb = MainDatabase.getDatabase(contexts)
        val view: View = inflater.inflate(R.layout.fragment_staff_list, container, false)
        mRecyclerView = view.findViewById(R.id.staffList) as RecyclerView

        mDb!!.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, contexts))
                .observe(viewLifecycleOwner, Observer<List<CommunicationPermissions>> { communicationPermissions: List<CommunicationPermissions> ->
                    chatPermissions.clear()
                    for (c in communicationPermissions) {
                        chatPermissions.add(c.ToUserType)
                    }

                    initialize()
                })

        return view
    }

    override fun onStartStaffClickCallback(position: Int) {
        val intent = Intent(activity, ChatConversationActivity::class.java)
        val staff = Gson().toJson(staffList[position])
        intent.putExtra("with", staff)
        intent.putExtra("withType", "staff")
        startActivity(intent)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexts = context
    }

    private fun initialize() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerView.layoutManager = layoutManager

        mDb!!.schoolStaffModel().allStaff.observe(viewLifecycleOwner, Observer { schoolStaffEntities: List<SchoolStaffEntity> ->
            staffList.clear()
            staffFilterList.clear()
            when (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)){
                Gc.ADMIN -> {
                    staffList.addAll(schoolStaffEntities)
                    staffFilterList.addAll(schoolStaffEntities)
                    adapter = StaffListFragmentAdapter(staffList, contexts, activity, this)
                    mRecyclerView.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
                Gc.STUDENTPARENT -> {
                    for (s in Objects.requireNonNull(schoolStaffEntities)) {
                        if (chatPermissions.contains(s.UserTypeValue)) {
                            staffList.add(s)
                            staffFilterList.add(s)
                        }
                    }
                    adapter = StaffListFragmentAdapter(staffList, contexts, activity, this)
                    mRecyclerView.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
                else -> {
                    for (s in Objects.requireNonNull(schoolStaffEntities)) {
                        if (chatPermissions.contains(s.UserTypeValue) && !staffInfo.StaffId.equals(s.StaffId, ignoreCase = true)) {
                            staffList.add(s)
                            staffFilterList.add(s)
                        }
                    }
                    adapter = StaffListFragmentAdapter(staffList, contexts, activity, this)
                    mRecyclerView.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    companion object {
        var TAG = StaffListFragment::class.java.simpleName
    }
}