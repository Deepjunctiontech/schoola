package in.junctiontech.school.schoolnew.model;

/**
 * Created by deep on 27/03/18.
 */

public class SchoolAccounts {
    public String AccountId;
    public String AccountStatus;
    public String ManagedBy;
    public String AccountName;
    public String BankAccountName;
    public String AccountType;
    public String BankName;
    public String BranchName;
    public String IFSCCode;
    public String OpeningBalance;
    public String AccountBalance;
    public String AccountDate;
    public String DOE;
    public String managedByName;
    public String accountTypeName;

}
