package `in`.junctiontech.school.schoolnew.leave

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.google.common.base.Strings
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*


class ApplyLeaveActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar

    private val leavesDetails = ArrayList<LeavesDetails>()

    private var colorIs: Int = 0

    private var leaveTypeList = ArrayList<String>()
    private lateinit var leaveTypeBuilder: AlertDialog.Builder
    private lateinit var leaveTypeAdapter: ArrayAdapter<String>

    private var selectedLeaveType: Int = 0

    private lateinit var btnLeaveType: Button
    private lateinit var btnSelectFromDate: Button
    private lateinit var btnSelectToDate: Button
    private lateinit var btnCancel: Button
    private lateinit var btnApplyLeave: Button

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_apply)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)
        btnLeaveType = findViewById(R.id.btn_leave_type)

        btnSelectFromDate = findViewById(R.id.btn_select_from_date)

        btnSelectToDate = findViewById(R.id.btn_select_to_date)

        btnCancel = findViewById(R.id.btn_cancel)

        btnCancel.setOnClickListener {
            finish()
        }

        btnApplyLeave = findViewById(R.id.btn_apply_leave)
        btnApplyLeave.setOnClickListener {
            if (validateInput()) {
                try {
                    applyLeave()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

        selectLeaveTypeForApplyLeave()
        //  initializeDatePickers()
        setColorApp()

        if (!Strings.nullToEmpty(intent.getStringExtra("AssignLeaves")).equals("", ignoreCase = true)) {
            val ok = Gson()
                    .fromJson<ArrayList<LeavesDetails>>(intent.getStringExtra("AssignLeaves")
                            , object : TypeToken<List<LeavesDetails>>() {
                    }.type)
            leavesDetails.clear()
            leaveTypeList.clear()
            leavesDetails.addAll(ok)
            for (i in leavesDetails.indices) {
                leaveTypeList.add(leavesDetails[i].leaveName.toString())
            }
            leaveTypeAdapter.clear()
            leaveTypeAdapter.addAll(leaveTypeList)
            leaveTypeAdapter.notifyDataSetChanged()
        }


        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/2557011854", this, adContainer)
        }
    }

    private fun applyLeave() {
        progressBar.visibility = View.VISIBLE

        val params = JSONObject()
        params.put("staffId", Gc.getSharedPreference(Gc.STAFFID, applicationContext))
        params.put("leaveTypeId", leavesDetails[selectedLeaveType].leaveTypeId.toString().trim { it <= ' ' })
        params.put("session", Gc.getSharedPreference(Gc.APPSESSION, this))
        params.put("fromDate", btnSelectFromDate.text.toString().trim { it <= ' ' })
        params.put("toDate", btnSelectToDate.text.toString().trim { it <= ' ' })
        params.put("status", "Apply")

        val param = JSONObject()
        param.put("applyLeave", params)

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "profile/staffProfile/"
                + JSONObject().put("ProfileId", Gc.getSharedPreference(Gc.USERID, applicationContext)))

        val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), { job ->
            progressBar.visibility = View.GONE

            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> try {
                    val intent = Intent()
                     intent.putExtra("message", job.optString("message")
                    )
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                Gc.APIRESPONSE401 -> {
                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this@ApplyLeaveActivity)

                    val intent1 = Intent(this@ApplyLeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this@ApplyLeaveActivity)

                    val intent2 = Intent(this@ApplyLeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.activity_leave_apply), R.color.fragment_first_blue)
            }
        }, { error ->
            progressBar.visibility = View.VISIBLE
            Config.responseVolleyErrorHandler(this@ApplyLeaveActivity, error, findViewById(R.id.activity_leave_apply))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun initializeDatePickers() {
        if (!Strings.nullToEmpty(leavesDetails[selectedLeaveType].timeCircle).equals("", ignoreCase = true)) {

            val myCalendar = Calendar.getInstance()
            val sdf = SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US)

            when {
                leavesDetails[selectedLeaveType].timeCircle.equals("Monthly") -> {
                    btnSelectFromDate.text = ""
                    btnSelectToDate.text = ""
                    btnSelectToDate.setOnClickListener(null)
                    val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        myCalendar.set(Calendar.YEAR, year)
                        myCalendar.set(Calendar.MONTH, monthOfYear)
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                        btnSelectFromDate.text = sdf.format(myCalendar.time)

                        btnSelectToDate.text = sdf.format(myCalendar.time)
                    }

                    btnSelectFromDate.setOnClickListener {
                        val dialog = DatePickerDialog(this@ApplyLeaveActivity, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH))

                        val cal = Calendar.getInstance()
                        cal.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONSTART, this))
                        dialog.datePicker.minDate = cal.timeInMillis

                        val cal1 = Calendar.getInstance()
                        cal1.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))
                        dialog.datePicker.maxDate = cal1.timeInMillis

                        dialog.show()
                    }
                }
                leavesDetails[selectedLeaveType].timeCircle.equals("Quarterly") -> {
                    btnSelectFromDate.text = ""
                    btnSelectToDate.text = ""
                    btnSelectToDate.setOnClickListener(null)

                    val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        myCalendar.set(Calendar.YEAR, year)
                        myCalendar.set(Calendar.MONTH, monthOfYear)
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                        btnSelectFromDate.text = sdf.format(myCalendar.time)
                        btnSelectToDate.text = ""
                        toDate()
                    }

                    btnSelectFromDate.setOnClickListener {
                        val dialog = DatePickerDialog(this@ApplyLeaveActivity, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH))

                        val cal = Calendar.getInstance()
                        cal.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONSTART, this))
                        dialog.datePicker.minDate = cal.timeInMillis

                        val cal1 = Calendar.getInstance()
                        cal1.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))
                        dialog.datePicker.maxDate = cal1.timeInMillis

                        dialog.show()
                    }
                }
                leavesDetails[selectedLeaveType].timeCircle.equals("Session") -> {
                    btnSelectFromDate.text = ""
                    btnSelectToDate.text = ""
                    btnSelectToDate.setOnClickListener(null)

                    val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        myCalendar.set(Calendar.YEAR, year)
                        myCalendar.set(Calendar.MONTH, monthOfYear)
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                        btnSelectFromDate.text = sdf.format(myCalendar.time)
                        btnSelectToDate.text = ""
                        toDate()
                    }

                    btnSelectFromDate.setOnClickListener {
                        val dialog = DatePickerDialog(this@ApplyLeaveActivity, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH))

                        val cal = Calendar.getInstance()
                        cal.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONSTART, this))
                        dialog.datePicker.minDate = cal.timeInMillis

                        val cal1 = Calendar.getInstance()
                        cal1.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))
                        dialog.datePicker.maxDate = cal1.timeInMillis

                        dialog.show()
                    }
                }
                leavesDetails[selectedLeaveType].timeCircle.equals("OnOccassion") -> {
                    btnSelectFromDate.text = ""
                    btnSelectToDate.text = ""
                    btnSelectToDate.setOnClickListener(null)

                    val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        myCalendar.set(Calendar.YEAR, year)
                        myCalendar.set(Calendar.MONTH, monthOfYear)
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                        btnSelectFromDate.text = sdf.format(myCalendar.time)
                        btnSelectToDate.text = ""
                        toDate()
                    }

                    btnSelectFromDate.setOnClickListener {
                        val dialog = DatePickerDialog(this@ApplyLeaveActivity, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH))

                        val cal = Calendar.getInstance()
                        cal.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONSTART, this))
                        dialog.datePicker.minDate = cal.timeInMillis

                        val cal1 = Calendar.getInstance()
                        cal1.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))
                        dialog.datePicker.maxDate = cal1.timeInMillis

                        dialog.show()
                    }
                }
            }
        }
    }

    private fun toDate() {
        val myCalendar = Calendar.getInstance()
        val sdf = SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US)

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            btnSelectToDate.text = sdf.format(myCalendar.time)
        }

        btnSelectToDate.setOnClickListener {
            val dialog = DatePickerDialog(this@ApplyLeaveActivity, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH))

            val cal = Calendar.getInstance()
            cal.time = sdf.parse(btnSelectFromDate.text.toString())
            dialog.datePicker.minDate = cal.timeInMillis

            val cal1 = Calendar.getInstance()
            cal1.time = sdf.parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this))
            dialog.datePicker.maxDate = cal1.timeInMillis

            dialog.show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun selectLeaveTypeForApplyLeave() {
        leaveTypeBuilder = AlertDialog.Builder(this@ApplyLeaveActivity)
        leaveTypeAdapter = ArrayAdapter(this@ApplyLeaveActivity, android.R.layout.select_dialog_singlechoice)
        leaveTypeAdapter.addAll(leaveTypeList)
        btnLeaveType.setOnClickListener {
            leaveTypeBuilder.setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface.dismiss() }

            leaveTypeBuilder.setAdapter(leaveTypeAdapter) { _, i ->
                btnLeaveType.text = leaveTypeList[i]
                selectedLeaveType = i
                initializeDatePickers()
            }
            val dialog = leaveTypeBuilder.create()
            dialog.show()
        }
    }


    private fun validateInput(): Boolean {
        if (getString(R.string.leave_type) == btnLeaveType.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.leave_type) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_leave_apply), R.color.fragment_first_blue)
            btnLeaveType.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.select_date) == btnSelectFromDate.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.from_date) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_leave_apply), R.color.fragment_first_blue)
            btnSelectFromDate.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.select_date) == btnSelectToDate.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.to_date) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.activity_leave_apply), R.color.fragment_first_blue)
            btnSelectToDate.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }
        return true
    }
}