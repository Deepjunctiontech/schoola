package in.junctiontech.school.schoolnew.owner;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class OrganizationDetailActivity extends AppCompatActivity {

    private CircleImageView  civ;
    private TextView  tv_owner_name;
    private RecyclerView  rv_list;
    private int  colorIs;
    private ArrayList<OwnerViewAction>  ownerSchoolList;
    private OwnerOrganizationOptionAdapter adapter;
    private OwnerSchoolData objData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_schools);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        colorIs = Config.getAppColor(this, true);

        objData = (OwnerSchoolData) getIntent().getSerializableExtra("data");

        civ = (CircleImageView)findViewById(R.id.iv_owner_school_logo);
        tv_owner_name = (TextView)findViewById(R.id.tv_owner_name);
        (  findViewById(R.id.fbtn_add_new_owner_organization)).setVisibility(View.GONE);
        tv_owner_name.setText(objData.getOrganizationName());
        Glide.with(this).load(objData.getLogoLink())
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .thumbnail(0.5f)
                .into(civ);

        rv_list = ((RecyclerView) findViewById(R.id.rv_owner_school_list));
        ownerSchoolList = new ArrayList<OwnerViewAction>();
        if (getIntent().getBooleanExtra("isPrincipal",false)){
            ownerSchoolList.add(new OwnerViewAction("Create Admin",  R.drawable.ic_principal));

        }


        ownerSchoolList.add(new OwnerViewAction("Fees",  R.drawable.ic_coins));
        ownerSchoolList.add(new OwnerViewAction( "Salary",R.drawable.ic_salary));
        ownerSchoolList.add(new OwnerViewAction(  "Staff Attendance", R.drawable.ic_attendance));
        ownerSchoolList.add(new OwnerViewAction(  "Message", R.drawable.ic_message));

        setUpRecycler();
    }

    private void  setUpRecycler() {
        rv_list.setLayoutManager(new GridLayoutManager(this,3));
        adapter = new OwnerOrganizationOptionAdapter(this,ownerSchoolList,colorIs);
        rv_list.setAdapter(adapter);
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
