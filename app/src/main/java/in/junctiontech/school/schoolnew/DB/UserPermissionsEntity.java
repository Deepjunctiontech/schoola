package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class UserPermissionsEntity {
    @PrimaryKey
    @NonNull
    public String permission ;

    public UserPermissionsEntity(String permission){
        this.permission = permission;
    }
}
