package in.junctiontech.school.schoolnew.feesetup.feetype;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class StudentFeeListActivity extends AppCompatActivity {

    private FloatingActionButton fb_update_student_fee;

    ProgressBar progressBar;

    ArrayList<FeeStructureModel> feeStructureModels = new ArrayList<>();
    JSONArray studentFees = new JSONArray();
    String studentTransportFees ;
    RecyclerView mRecyclerView;
    StudentFeeListAdapter adapter;

    private SchoolStudentEntity student;
    String selectedStudent;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_fee_list);

        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        selectedStudent = getIntent().getStringExtra("student");
        student = new Gson().fromJson( selectedStudent, SchoolStudentEntity.class);

        fb_update_student_fee = findViewById(R.id.fb_update_student_fee);

        if (!(Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED,this).equalsIgnoreCase(Gc.TRUE))){
            fb_update_student_fee.setVisibility(View.GONE);

        }

        fb_update_student_fee.setOnClickListener(view -> {
            Intent intent = new Intent(StudentFeeListActivity.this, StudentFeeActivity.class);
            intent.putExtra("student",getIntent().getStringExtra("student"))
                    .putExtra("feestructure", studentFees.toString())
                    .putExtra("transportfees",studentTransportFees)
                    .putExtra("edit","yes");

            startActivityForResult(intent, 655); // f=6 e=5

        });

        getStudentFeeFromServer(student);

        mRecyclerView = findViewById(R.id.rv_fee_structure);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new StudentFeeListAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1626302085",this,adContainer);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case 655:
                if(resultCode== Activity.RESULT_OK)
                getStudentFeeFromServer(student);
                break;
        }
    }

    void getStudentFeeFromServer(SchoolStudentEntity student) {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();

        try {
            p.put("admissionId", student.AdmissionId);
            p.put("session",Gc.getSharedPreference(Gc.APPSESSION,this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/updateStudentFee/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {

                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            studentFees = resultjobj.getJSONObject("feeStructure").getJSONArray("fees");
                            studentTransportFees = resultjobj.getJSONObject("feeStructure").optString("transportfees");
                            feeStructureModels = new Gson()
                                    .fromJson(resultjobj.getJSONObject("feeStructure").optString("AllFeeStructure"),
                                            new TypeToken<List<FeeStructureModel>>() {}.getType());

                            adapter.notifyDataSetChanged();
                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(StudentFeeListActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    public class StudentFeeListAdapter extends RecyclerView.Adapter<StudentFeeListAdapter.MyViewHolder>{

        @NonNull
        @Override
        public StudentFeeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fee_structure_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull StudentFeeListAdapter.MyViewHolder holder, int position) {
            holder.tv_effective_from.setText(feeStructureModels.get(position).from);
            holder.tv_fee_status.setText(feeStructureModels.get(position).status);

        }

        @Override
        public int getItemCount() {
            return feeStructureModels.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_effective_from,tv_fee_status;
            public MyViewHolder(View itemView) {
                super(itemView);

                tv_fee_status = itemView.findViewById(R.id.tv_fee_status);
                tv_effective_from = itemView.findViewById(R.id.tv_effective_from);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder choices = new AlertDialog.Builder(StudentFeeListActivity.this);
                        String[] aa = new String[]
                                {
                                        getResources().getString(R.string.fee_details),
//                                        getResources().getString(R.string.delete),
                                };
                        choices.setItems(aa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i){

                                    case 0:
                                        Intent intent = new Intent(StudentFeeListActivity.this, StudentFeeActivity.class);
                                        String feestructure = new Gson().toJson(feeStructureModels.get(getAdapterPosition()).basicfee);
                                        String transportFee = new Gson().toJson(feeStructureModels.get(getAdapterPosition()).transportfees);
                                        intent.putExtra("student",selectedStudent);
                                        intent.putExtra("feestructure", feestructure);
                                        intent.putExtra("transportfees", transportFee);
                                        intent.putExtra("effectivefrom", feeStructureModels.get(getAdapterPosition()).from);
                                        intent.putExtra("edit","no");
                                        startActivity(intent);
                                        break;

                                        case 1:
                                        deleteFeeStructure(feeStructureModels.get(getAdapterPosition()));
                                        break;
                                }
                            }
                        });
                        choices.show();
                    }
                });
            }
        }
    }

    void deleteFeeStructure(FeeStructureModel feeStructureModel){
// TODO Password Dialog for delete Fee
                final JSONObject param = new JSONObject();

        try {
            param.put("studentfeeid", feeStructureModel.feestructureid);
            param.put("userName", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this) );

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        p.put("Frequency", "Monthly");


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/updateStudentFee"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");



                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Config.responseVolleyErrorHandler(StudentFeeListActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
            menu.findItem(R.id.action_save).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();
                break;

        }
        return super.onOptionsItemSelected(item);

    }
}
