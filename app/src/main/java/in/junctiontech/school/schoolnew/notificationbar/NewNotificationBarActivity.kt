package `in`.junctiontech.school.schoolnew.notificationbar

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.NoticeBoardEntity
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.communicate.CreateNoticeBoardActivity
import `in`.junctiontech.school.schoolnew.communicate.NoticeBoardReportActivity
import `in`.junctiontech.school.schoolnew.model.ClassNameSectionName
import android.app.Activity
import android.app.DatePickerDialog
import android.app.SearchManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import androidx.core.text.HtmlCompat.fromHtml
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.common.base.Strings
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import org.apache.commons.io.FilenameUtils
import org.json.JSONException
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class NewNotificationBarActivity : AppCompatActivity() {
    private val notificationList = ArrayList<NoticeBoardEntity>()
    private val notificationListFilter = ArrayList<NoticeBoardEntity>()
    private var rvNotificationList: RecyclerView? = null
    private var rg_type_of_notice: RadioGroup? = null
    private var radioButton: RadioButton? = null
    private var colorIs: Int = 0
    private var fb_noticeBoard_add : FloatingActionButton? = null
    lateinit var searchView: android.widget.SearchView
    private var mDb: MainDatabase? = null
    var btn_notice_date : Button? = null
    var  btn_select_class: Button? = null
    var progressBar : LinearLayout? = null

    var sectionList = java.util.ArrayList<String>()

    var classNameSectionList: ArrayList<ClassNameSectionName> = ArrayList<ClassNameSectionName>()
    var selectedClassSections = java.util.ArrayList<String>()
    var tv_help_noticelist : TextView? = null

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
        fb_noticeBoard_add?.setBackgroundTintList(ColorStateList.valueOf(colorIs))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notification_list_view)
        mDb = MainDatabase.getDatabase(applicationContext)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        rvNotificationList = findViewById(R.id.rv_notification_list)

        progressBar = findViewById(R.id.progressBar)

        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase())&&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/9006098659", this, adContainer)
        }

        rg_type_of_notice = findViewById(R.id.rg_type_of_notice)
        rg_type_of_notice!!.setOnCheckedChangeListener { group, checkedId ->
            run {
                // clear search query when radio button is changed
                searchView.setQuery("", false);
                searchView.setIconified(true);
                getNoticesFromDb()
            }
        }

        fb_noticeBoard_add = findViewById(R.id.fb_noticeBoard_add)
        fb_noticeBoard_add?.setOnClickListener{
            val intent = Intent(applicationContext, CreateNoticeBoardActivity::class.java)
            startActivity(intent)
        }

        tv_help_noticelist = findViewById(R.id.tv_help_noticelist)
        Handler().postDelayed(Runnable { tv_help_noticelist?.setVisibility(View.GONE) }, 5000)

        if (Gc.getSharedPreference(Gc.CREATENOTICEALLOWED, this).equals(Gc.TRUE, true)){
            fb_noticeBoard_add?.visibility = View.VISIBLE
        }else {
            fb_noticeBoard_add?.visibility = View.GONE
        }
        setupClassPicker()
        setupDatePickers()
        setUpRecyclerView()
        setColorApp()
    }

    fun setupClassPicker(){
        btn_select_class = findViewById(R.id.btn_select_class)

        btn_select_class?.setOnClickListener {
            selectClassSections()
        }
    }

    fun selectClassSections() {
        val builder = AlertDialog.Builder(this)
        builder.setIcon(R.drawable.ic_edit)
        builder.setTitle(R.string.select_class)
        val checkedItems = BooleanArray(classNameSectionList.size)
        val objNames: Array<Any> = sectionList.toTypedArray()
        val strNames = Arrays.copyOf(objNames, Objects.requireNonNull(objNames).size, Array<String>::class.java)
        for (i in classNameSectionList.indices) {
            checkedItems[i] = selectedClassSections.contains(classNameSectionList[i].SectionId)
        }
        builder.setSingleChoiceItems(strNames,-1){ dialogInterface: DialogInterface, i: Int ->
//            btn_select_class?.text = sectionList[i]

            // SearchView set focus
            searchView.onActionViewExpanded()
            // Searchview Set Query the class section name
            searchView.setQuery(sectionList[i], false)

//            searchView.clearFocus()
            dialogInterface.dismiss()
        }.setPositiveButton(R.string.cancel) { dialogInterface, i ->

            dialogInterface.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
    }


    fun setupDatePickers(){
        btn_notice_date = findViewById(R.id.btn_notice_date)

        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US)

        btn_notice_date?.text = sdf.format(Date())

        val noticeDateListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            cal.set(Calendar.YEAR,year)
            cal.set(Calendar.MONTH, month)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            searchView.setQuery("", false);
            searchView.setIconified(true);

            btn_notice_date?.text = sdf.format(cal.time)
            getNoticesFromDb()
        }

        btn_notice_date?.setOnClickListener {
            DatePickerDialog(this, noticeDateListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    override fun onResume() {
        super.onResume()
        getNoticesFromDb()

        classNameSectionList.clear()
        sectionList.clear()

        mDb!!.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).observe(this, Observer { classNameSectionNames: List<ClassNameSectionName?>? ->
            classNameSectionList.clear()
            sectionList.clear()
            assert(classNameSectionNames != null)
            classNameSectionList.addAll(classNameSectionNames!!.filterNotNull())
            for (i in classNameSectionList.indices) {
                sectionList.add(classNameSectionList[i].ClassName + " " + classNameSectionList[i].SectionName)
            }
        })
    }

    fun getNoticesFromDb(){
//        val Dates = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date())

        val selectedId = rg_type_of_notice!!.getCheckedRadioButtonId()
        radioButton = findViewById<RadioButton>(selectedId)
        var rb_type = radioButton!!.text.toString()

        // this is done so that text that is visible is not taken for database query and instead a hard coded
        // english string is used for database query , cause display will be language dependent

        if (rb_type.equals(getString(R.string.circular))){
            rb_type = "Circular"
        }else if (rb_type.equals(getString(R.string.class_diary))){
            rb_type = "Class Diary"
        }else if (rb_type.equals(getString(R.string.syllabus))){
            rb_type = "Syllabus"
        }
        mDb?.noticeBoardModel()?.getNoticesForType(Gc.ACTIVE,btn_notice_date?.text.toString(), rb_type)?.observe(this, Observer { noticeBoardEntities ->
            notificationList.clear()
            notificationListFilter.clear()
            notificationList.addAll(noticeBoardEntities!!)
            notificationListFilter.addAll(noticeBoardEntities)
            rvNotificationList!!.adapter?.notifyDataSetChanged()
             
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpRecyclerView() {
        val mLayoutManager = LinearLayoutManager(this)
        rvNotificationList!!.layoutManager = mLayoutManager
        val itemAnimator = DefaultItemAnimator()
        itemAnimator.addDuration = 1000
        itemAnimator.removeDuration = 1000
        rvNotificationList!!.itemAnimator = itemAnimator
        rvNotificationList!!.adapter = NotificationListAdapter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_search, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search1).actionView as android.widget.SearchView
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(componentName))


        searchView.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                filterResult(s)
                return true
            }

            override fun onQueryTextChange(s: String): Boolean {
                filterResult(s)
                return true
            }
        })
        return true
    }

    internal fun filterResult(text: String) {

        notificationList.clear()
        if (text.isEmpty()) {
            notificationList.addAll(notificationListFilter)
        } else {
            for (s in notificationListFilter) {
                if (s.Flag !=null && s.Flag.isNotEmpty() && s.Description.toLowerCase().contains(text.toLowerCase())) {
                    notificationList.add(s)
                }
            }
        }
        rvNotificationList?.adapter?.notifyDataSetChanged()
    }

    fun updateNotice(notice: String?) {
        val intent = Intent(this, CreateNoticeBoardActivity::class.java)
        intent.putExtra("notice", notice)
        startActivity(intent)
    }

    fun deleteNotice(notice: String?) {
        progressBar?.visibility = View.VISIBLE
        val noticeBoardEntity = Gson().fromJson(notice, NoticeBoardEntity::class.java)
        var url: String? = null
        try {
            url = (Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "noticeBoard/noticeBoards/"
                    + JSONObject().put("Notify_id", noticeBoardEntity.Notify_id))
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.DELETE, url, JSONObject(), Response.Listener { job ->
            progressBar?.visibility = View.GONE
            val code = job.optString("code")
            when (code) {
                Gc.APIRESPONSE200 -> {
                    Thread(Runnable { mDb!!.noticeBoardModel().deleteNotice(noticeBoardEntity) }).start()
                    Config.responseSnackBarHandler(getString(R.string.deleted_successfully),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue)
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar?.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun viewReport(notice: String) {
        val intent = Intent(this, NoticeBoardReportActivity::class.java)
        intent.putExtra("notice", notice)
        startActivity(intent)
    }


    inner class NotificationListAdapter internal constructor(internal var activity: Activity) : RecyclerView.Adapter<NotificationListAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_list_recycler, parent, false))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvDescription.text = fromHtml(notificationList[position].Description, FROM_HTML_MODE_LEGACY)

            if (!Strings.nullToEmpty(notificationList[position].Link_url).equals("", ignoreCase = true)) {
                try {
                    val url = URL(notificationList[position].Link_url)
                    if (FilenameUtils.getExtension(url.path).equals("PDF", ignoreCase = true)) {
                        Glide.with(activity)
                                .load(R.drawable.ic_pdf)
                                .apply(RequestOptions.errorOf(R.drawable.ic_notification))
                                .apply(RequestOptions.overrideOf(100, 100))
                                .apply(RequestOptions.fitCenterTransform())
                                .into(holder.ivNotifications)
                    } else {
                        // btnView.visibility = View.GONE
                        //  ivNotifications.visibility = View.VISIBLE
                        Glide.with(activity)
                                .load(notificationList[position].Link_url)
                                .apply(RequestOptions.errorOf(R.drawable.ic_notification))
                                .apply(RequestOptions.overrideOf(100, 100))
                                .apply(RequestOptions.fitCenterTransform())
                                .into(holder.ivNotifications)
                    }
                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                }
            }else{
                Glide.with(activity)
                        .load(R.drawable.ic_notification)
                        .apply(RequestOptions.overrideOf(100, 100))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(holder.ivNotifications)
            }

        }

        override fun getItemCount(): Int {
            return notificationList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvDescription: TextView = itemView.findViewById(R.id.tv_description)

            internal var ivNotifications: CircleImageView = itemView.findViewById(R.id.iv_notifications)

            init {
                itemView.setOnClickListener {

                    try {
                        val result = ArrayList<String>()
                        val regex = "\\(?\\b(https://|http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.]*[-A-Za-z0-9+&amp;@#/%=~_()|]"
                        val p = Pattern.compile(regex)
                        val m = p.matcher(notificationList[layoutPosition].Description)
                        while (m.find()) {
                            val urlStr = m.group()
                            //  Log.e("Urls",urlStr)
                            if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                                result.add("http://" + urlStr.substring(1, urlStr.length - 1))
                            } else if (urlStr.startsWith("https://")) {
                                result.add(urlStr)
                            } else if (urlStr.startsWith("http://")) {
                                result.add(urlStr)
                            } else {
                                result.add("http://$urlStr")
                            }
                        }

                        val htmlATagPattern = "(?i)<a([^>]+)>(.+?)</a>"
                        val htmlAHrefTagPattern = "\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))"

                        val patternTag = Pattern.compile(htmlATagPattern)
                        val patternLink = Pattern.compile(htmlAHrefTagPattern)

                        val matcherTag = patternTag.matcher(notificationList[layoutPosition].Description)
                        while (matcherTag.find()) {
                            val href = matcherTag.group(1) // href
                            //val linkText = matcherTag.group(2) // link text
                            val matcherLink = patternLink.matcher(href)
                            while (matcherLink.find()) {
                                var link = matcherLink.group(1) // link
                                link = link.replace("'", "")
                                link = link.replace("\"", "")
                                when {
                                    link.startsWith("https://") -> result.add(link)
                                    link.startsWith("http://") -> result.add(link)
                                    else -> result.add("http://$link")
                                }
                            }
                        }

                      //  Log.e("ssd","$result")

//                        if (result.isNotEmpty()) {

                            val choices = AlertDialog.Builder(this@NewNotificationBarActivity)
                            val menu = ArrayList<String>()
                            if (notificationList[layoutPosition].Link_url.isNotEmpty()) {
                                menu.add( "View Attachment")
                            }
                            val results = result.distinct().toTypedArray()
                            for (x in results) {
                                menu.add(x)
                            }

                            if (Gc.getSharedPreference(Gc.CREATENOTICEALLOWED,this@NewNotificationBarActivity ).equals(Gc.TRUE, true)){
                                menu.add( getString( R.string.update))
                                menu.add( getString( R.string.delete))
                                menu.add( getString( R.string.report_view))
                            }
                            val objNames = menu.toTypedArray()
                            choices.setItems(objNames) { _, which ->
                                val notice = Gson().toJson( notificationList[layoutPosition])

                                when(objNames[which]){
                                    "View Attachment" -> {
                                        val url = URL(notificationList[layoutPosition].Link_url)
                                        if (FilenameUtils.getExtension(url.path).equals("PDF", ignoreCase = true)) {
                                            val intent = Intent(Intent.ACTION_VIEW)
                                            intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + notificationList[layoutPosition].Link_url), "text/html")
                                            startActivity(intent)
                                        } else {
                                            val intent = Intent(this@NewNotificationBarActivity, FullScreenImageActivity::class.java)
                                            val stom = ArrayList<String>()
                                            stom.add(notificationList[layoutPosition].Link_url)
                                            intent.putStringArrayListExtra("images", stom)
                                            intent.putExtra("Notification Board", getString(R.string.gallery))
                                            startActivity(intent)
                                        }
                                    }
                                    getString(R.string.update) -> {
                                        updateNotice(notice)
                                    }
                                    getString(R.string.delete) -> {
                                        deleteNotice(notice)
                                    }
                                    getString(R.string.report_view) -> {
                                        viewReport(notice)
                                    }

                                    else -> {
                                        val openURL = Intent(Intent.ACTION_VIEW)
                                        val url = Uri.parse(if (objNames.size != results.size) {results[which-1]} else {results[which]})
                                        openURL.data = url
                                        startActivity(openURL)
                                    }
                                }

//                                if(objNames[which]=="View Attachment"){
//                                    val url = URL(notificationList[layoutPosition].Link_url)
//                                    if (FilenameUtils.getExtension(url.path).equals("PDF", ignoreCase = true)) {
//                                        val intent = Intent(Intent.ACTION_VIEW)
//                                        intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + notificationList[layoutPosition].Link_url), "text/html")
//                                        startActivity(intent)
//                                    } else {
//                                        val intent = Intent(this@NewNotificationBarActivity, FullScreenImageActivity::class.java)
//                                        val stom = ArrayList<String>()
//                                        stom.add(notificationList[layoutPosition].Link_url)
//                                        intent.putStringArrayListExtra("images", stom)
//                                        intent.putExtra("Notification Board", getString(R.string.gallery))
//                                        startActivity(intent)
//                                    }
//                                }

                               /* when (which) {
                                    0 -> {
                                        val url = URL(notificationList[layoutPosition].Link_url)
                                        if (FilenameUtils.getExtension(url.path).equals("PDF", ignoreCase = true)) {
                                            val intent = Intent(Intent.ACTION_VIEW)
                                            intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + notificationList[layoutPosition].Link_url), "text/html")
                                            startActivity(intent)
                                        } else {
                                            val intent = Intent(this@NewNotificationBarActivity, FullScreenImageActivity::class.java)
                                            val stom = ArrayList<String>()
                                            stom.add(notificationList[layoutPosition].Link_url)
                                            intent.putStringArrayListExtra("images", stom)
                                            intent.putExtra("Notification Board", getString(R.string.gallery))
                                            startActivity(intent)
                                        }
                                    }
                                    1 -> {
                                        val openURL = Intent(Intent.ACTION_VIEW)
                                        val url = Uri.parse(result[which-1])
                                        openURL.data = url
                                        startActivity(openURL)
                                    }
                                }*/
                            }
                            choices.show()
//                        }
                        // This portion should disappear because Now a menu will always be there
//                        else {
//                            if (!Strings.nullToEmpty(notificationList[layoutPosition].Link_url).equals("", ignoreCase = true)) {
//                                val url = URL(notificationList[layoutPosition].Link_url)
//                                if (FilenameUtils.getExtension(url.path).equals("PDF", ignoreCase = true)) {
//                                    val intent = Intent(Intent.ACTION_VIEW)
//                                    intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + notificationList[layoutPosition].Link_url), "text/html")
//                                    startActivity(intent)
//                                } else {
//                                    val intent = Intent(this@NewNotificationBarActivity, FullScreenImageActivity::class.java)
//
//                                    val stom = ArrayList<String>()
//                                    stom.add(notificationList[layoutPosition].Link_url)
//
//                                    intent.putStringArrayListExtra("images", stom)
//                                    intent.putExtra("Notification Board", getString(R.string.gallery))
//                                    startActivity(intent)
//                                }
//                            } else {
//                                Config.responseSnackBarHandler("Attachment Not Found",
//                                        findViewById(R.id.ll_new_notification_activity), R.color.fragment_first_blue)
//                            }
//                        }


                    } catch (e: MalformedURLException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}