package in.junctiontech.school.schoolnew.feesetup.feetype;

import android.app.Activity;
import android.app.DatePickerDialog;
import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class StudentFeeActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;
    Button btn_student_fee_effective_from;

    private SchoolStudentEntity student;

    ArrayList<StudentFeeModel> studentFeeModel = new ArrayList<>();
    ArrayList<TransportFeeModel> studentTransportFeeModel = new ArrayList<>();

    RecyclerView mRecyclerView;
    StudentFeeAdapter adapter;
    Boolean isFeeEdit = true;

    TextView tv_manage_fee_total_session,tv_manage_fee_total_quarterly,tv_manage_fee_total_monthly;

    SwitchCompat sw_transport_fee;
    Button btn_transport_distance;
    EditText et_transport_amount;

    ArrayList<ClassFeesTransportEntity> transportAmounts = new ArrayList<>();
    ArrayList<String> distanceList = new ArrayList<>();

    private int colorIs;
    private int selectedDistance;


    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_fee);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btn_student_fee_effective_from = findViewById(R.id.btn_student_fee_effective_from);
        tv_manage_fee_total_session = findViewById(R.id.tv_manage_fee_total_session);
        tv_manage_fee_total_quarterly = findViewById(R.id.tv_manage_fee_total_quarterly);
        tv_manage_fee_total_monthly = findViewById(R.id.tv_manage_fee_total_monthly);
        sw_transport_fee = findViewById(R.id.sw_transport_fee);
        btn_transport_distance = findViewById(R.id.btn_transport_distance);
        et_transport_amount = findViewById(R.id.et_transport_amount);

        student = new Gson().fromJson( getIntent().getStringExtra("student"), SchoolStudentEntity.class);



        //  When we want to display fees -- no change
        if( getIntent().getStringExtra("edit").equalsIgnoreCase("no")){
            btn_student_fee_effective_from.setEnabled(false);
            isFeeEdit = false;
            btn_student_fee_effective_from.setText(getIntent().getStringExtra("effectivefrom"));
            sw_transport_fee.setEnabled(false);
            btn_transport_distance.setEnabled(false);
            et_transport_amount.setEnabled(false);
        }
        // TODO Decide if we want to set the default Fee month
//        else {
//            // Change on 17 April 2019
//
//            String sessionStart = Gc.getSharedPreference(Gc.APPSESSIONSTART, this);
//
//            SimpleDateFormat inputdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//            SimpleDateFormat outputdf = new SimpleDateFormat("MMM-yyyy", Locale.US);
//
//            try {
//                Date inputDate = inputdf.parse(sessionStart);
//                String outputDate = outputdf.format(inputDate);
//
//                btn_student_fee_effective_from.setText(outputDate);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            // Change on 17 April 2019
//        }



        studentFeeModel = new Gson().fromJson(getIntent().getStringExtra("feestructure"),
                new TypeToken<List<StudentFeeModel>>() {}.getType());

        studentTransportFeeModel = new Gson().fromJson(getIntent().getStringExtra("transportfees"),
                new TypeToken<List<TransportFeeModel>>(){}.getType());

        if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, this).equalsIgnoreCase(Gc.DISTANCEWISE)){
            btn_transport_distance.setVisibility(View.VISIBLE);
        }else{
            btn_transport_distance.setVisibility(View.GONE);
        }


        if (studentTransportFeeModel!= null && studentTransportFeeModel.size() >0 ){
            sw_transport_fee.setChecked(true);
            et_transport_amount.setVisibility(View.VISIBLE);

            for (int i =0; i < studentTransportFeeModel.size(); i++){
                if (studentTransportFeeModel.get(i).Assign.equalsIgnoreCase("Yes")) {
                    selectedDistance = i;
                    et_transport_amount.setText(studentTransportFeeModel.get(selectedDistance).StudentFeeAmount);
                }
            }

            if (Strings.nullToEmpty(studentTransportFeeModel.get(selectedDistance).DistanceKM).equalsIgnoreCase(""))
                btn_transport_distance.setVisibility(View.GONE);
            else
                btn_transport_distance.setText(studentTransportFeeModel.get(selectedDistance).DistanceKM);

        }else{
            sw_transport_fee.setChecked(false);
            btn_transport_distance.setVisibility(View.GONE);
            et_transport_amount.setVisibility(View.GONE);
        }

        sw_transport_fee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, StudentFeeActivity.this)
                            .equalsIgnoreCase("DistanceWise"))
                        btn_transport_distance.setVisibility(View.VISIBLE);

                    et_transport_amount.setVisibility(View.VISIBLE);

                }else{
                    btn_transport_distance.setVisibility(View.GONE);
//                    btn_transport_distance.setText(R.string.distance);
//                    et_transport_amount.getText().clear();
                    et_transport_amount.setVisibility(View.GONE);
                }
            }
        });

        if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, StudentFeeActivity.this)
                .equalsIgnoreCase(Gc.FIXED)) {
            btn_transport_distance.setVisibility(View.GONE);
        }
        else if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, StudentFeeActivity.this)
                .equalsIgnoreCase(Gc.DISTANCEWISE)){
            mDb.classFeesTransportModel()
                    .getFeeforClassSectionLive(student.SectionId, Gc.getSharedPreference(Gc.APPSESSION, this))
                    .observe(this, new Observer<List<ClassFeesTransportEntity>>() {
                        @Override
                        public void onChanged(@Nullable List<ClassFeesTransportEntity> classFeesTransportEntities) {
                            transportAmounts.clear();
                            transportAmounts.addAll(classFeesTransportEntities);
                            distanceList = new ArrayList<>();
                            for (ClassFeesTransportEntity f : transportAmounts){
                                distanceList.add(f.MasterEntryValue);
                            }

                        }
                    });
        }

        btn_transport_distance.setOnClickListener(view -> {
            AlertDialog.Builder transportListBuilder = new AlertDialog.Builder(StudentFeeActivity.this);;
            ArrayAdapter<String> transportListAdapter = new ArrayAdapter<String>(StudentFeeActivity.this, android.R.layout.select_dialog_singlechoice);
            transportListAdapter.addAll(distanceList);

            transportListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });

            transportListBuilder.setAdapter(transportListAdapter, (DialogInterface dialogInterface, int i) -> {
                btn_transport_distance.setText(distanceList.get(i));
                selectedDistance = i;
                et_transport_amount.setText(transportAmounts.get(i).Amount);
//                        calculateFeesSumForDisplay();
            });

            AlertDialog dialog = transportListBuilder.create();
            dialog.show();

        });

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener feedate = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "MMM-yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            btn_student_fee_effective_from.setText(sdf.format(myCalendar.getTime()));
        };

        btn_student_fee_effective_from.setOnClickListener(view -> {
            new DatePickerDialog(StudentFeeActivity.this, feedate, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });



        mRecyclerView = findViewById(R.id.rv_student_fee_list);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(studentFeeModel.size()); //TODO relook the strategy
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new StudentFeeAdapter();
        mRecyclerView.setAdapter(adapter);

        buildSummaryFees();

        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3677750355",this,adContainer);
        }
    }

    void buildSummaryFees(){
        int quarterly=0,monthly=0,session=0;
        for(StudentFeeModel s: studentFeeModel){
            if(s.Frequency.equalsIgnoreCase("monthly"))monthly += Integer.parseInt(s.StudentFeeAmount);
            if(s.Frequency.equalsIgnoreCase("quarterly"))quarterly += Integer.parseInt(s.StudentFeeAmount);
            if(s.Frequency.equalsIgnoreCase("session"))session += Integer.parseInt(s.StudentFeeAmount);
        }
        tv_manage_fee_total_monthly.setText(monthly+"");
        tv_manage_fee_total_quarterly.setText(quarterly+"");
        tv_manage_fee_total_session.setText(session+"");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    JSONObject buildFeeListForStudent(){
        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();

        try {
            p.put("admissionId", student.AdmissionId);
            p.put("effectiveFrom", btn_student_fee_effective_from.getText());
            p.put("session", Gc.getSharedPreference(Gc.APPSESSION,this));

            for(StudentFeeModel s: studentFeeModel){
                JSONObject j1 = new JSONObject();
                j1.put("FeeId",s.FeeId);
                j1.put("StudentFeeAmount", s.StudentFeeAmount);
                j1.put("Assign","Yes");
                if(!(s.StudentFeeAmount.equalsIgnoreCase("0")))
                param.put(j1);
            }
            if (sw_transport_fee.isChecked()
                    && (!(et_transport_amount.getText().toString().equalsIgnoreCase("")))){

                JSONObject j1 = new JSONObject();
                if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, this).equalsIgnoreCase(Gc.FIXED))
                    j1.put("FeeId", studentTransportFeeModel.get(0).FeeId);
                else if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, this).equalsIgnoreCase(Gc.DISTANCEWISE))
                    j1.put("FeeId", studentTransportFeeModel.get(selectedDistance).FeeId);

                j1.put("StudentFeeAmount", et_transport_amount.getText().toString());
                j1.put("Assign","Yes");
                param.put(j1);
            }

            p.put("fees",param);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return p;
    }

    void saveStudentFeeToServer(){

        if(btn_student_fee_effective_from.getText().toString().equalsIgnoreCase(getString(R.string.fee_effective_from))){
            Config.responseSnackBarHandler(getString(R.string.fee_effective_from)+getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }
        if(sw_transport_fee.isChecked() && et_transport_amount.getText().toString().equalsIgnoreCase("")){
            Config.responseSnackBarHandler(getString(R.string.transport_fee)+getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = buildFeeListForStudent();

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/updateStudentFee"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {

                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case "200":
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");

                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
//                                     Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(StudentFeeActivity.this,error,findViewById(R.id.top_layout));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        if( getIntent().getStringExtra("edit").equalsIgnoreCase("no")){
            menu.findItem(R.id.action_save).setVisible(false);
        }

            return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();
                break;

            case R.id.action_save:
                saveStudentFeeToServer();
        }
                return super.onOptionsItemSelected(item);

    }

    public class StudentFeeAdapter extends RecyclerView.Adapter<StudentFeeAdapter.MyViewHolder>{

        @NonNull
        @Override
        public StudentFeeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_add_manage_fee, parent, false);

            return new MyViewHolder(view);        }

        @Override
        public void onBindViewHolder(@NonNull StudentFeeAdapter.MyViewHolder holder, final int position) {
            holder.et_item_amount.setText(studentFeeModel.get(position).StudentFeeAmount);
            if(isFeeEdit) holder.et_item_amount.setHint(studentFeeModel.get(position).ActualAmount);
            holder.et_item_amount.setEnabled(isFeeEdit);

            holder.et_item_amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if("".equals(editable.toString()))
                        studentFeeModel.get(position).StudentFeeAmount = "0";
                    else
                    studentFeeModel.get(position).StudentFeeAmount = editable.toString();
                    buildSummaryFees();
                }
            });
            holder.tv_item_fee_frequency.setText(studentFeeModel.get(position).Frequency);
            holder.tv_item_fee_name.setText(studentFeeModel.get(position).FeeTypeName);

        }

        @Override
        public int getItemCount() {
            return studentFeeModel.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_item_fee_name,tv_item_fee_frequency;
            EditText et_item_amount;
            public MyViewHolder(View itemView) {
                super(itemView);

                tv_item_fee_name = itemView.findViewById(R.id.tv_item_fee_name);
                tv_item_fee_frequency = itemView.findViewById(R.id.tv_item_fee_frequency);
                et_item_amount = itemView.findViewById(R.id.et_item_amount);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
