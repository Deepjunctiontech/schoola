package in.junctiontech.school.schoolnew.feesetup.feetype.receive;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.feesetup.feetype.payonline.PayOnlineActivity;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class FeeReceiptsActivity extends AppCompatActivity {


    ArrayList<FeeReceipt> feeReceipts = new ArrayList<>();

    FloatingActionButton fb_Receipt_add;
    RecyclerView mRecyclerView;
    Button btnStudentPayNow;
    String selectedStudent;
    SchoolStudentEntity studentEntity;

    FeeReceiptsAdapter adapter;

    ProgressBar progressBar2;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_receipts);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.rv_fee_receipts);
        fb_Receipt_add = findViewById(R.id.fb_Receipt_add);
        btnStudentPayNow = findViewById(R.id.btn_student_pay_now);
        progressBar2 = findViewById(R.id.progressBar2);

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            if (Gc.getArrayList(Gc.SIGNEDINUSERPERMISSION, this).contains(Gc.PayOnline)) {
                btnStudentPayNow.setVisibility(View.VISIBLE);
            }
        }

        if (Gc.getSharedPreference(Gc.FEEPAYMENTALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            fb_Receipt_add.show();
        }
        else if (Gc.getSharedPreference(Gc.FEEDISPLAYALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            if (!Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT)) {
                fb_Receipt_add.show();
                fb_Receipt_add.setImageDrawable(getDrawable(R.drawable.ic_fee_receipt));
            }
        }


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new FeeReceiptsAdapter();
        mRecyclerView.setAdapter(adapter);

        selectedStudent = getIntent().getStringExtra("student");

        // if it is coming from notification than get selected student from sharedpreference
        if ("".equalsIgnoreCase(Strings.nullToEmpty(selectedStudent)) && Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            String s = Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this);
            StudentInformation studentInformation = new Gson().fromJson(s, StudentInformation.class);
            studentEntity = new SchoolStudentEntity();
            studentEntity.SectionId = Strings.nullToEmpty(studentInformation.SectionId);
            studentEntity.AdmissionId = studentInformation.AdmissionId;
            selectedStudent = new Gson().toJson(studentEntity);
        }

        btnStudentPayNow.setOnClickListener(view -> {
            Intent intent = new Intent(FeeReceiptsActivity.this, PayOnlineActivity.class);
            intent.putExtra("student", selectedStudent);
            startActivityForResult(intent, Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE);
        });

        if (!("".equalsIgnoreCase(Strings.nullToEmpty(selectedStudent)))) {
            studentEntity = new Gson().fromJson(selectedStudent, SchoolStudentEntity.class);
            try {
                getFeeReceiptsForStudent();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            fb_Receipt_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(FeeReceiptsActivity.this, ReceiveFeeActivity.class);
                    intent.putExtra("student", selectedStudent);
                    startActivityForResult(intent, Gc.FEE_RECEIVE_REQUEST_CODE);
                }
            });
        }

        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/3422120550",this,adContainer);
        }
    }

    void getFeeReceiptsForStudent() throws JSONException {
        progressBar2.setVisibility(View.VISIBLE);

        Date date = new Date();
        String monthYear = new SimpleDateFormat("MMM-yyyy", Locale.getDefault()).format(date);
        try {
            date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    .parse(Gc.getSharedPreference(Gc.APPSESSIONEND, this));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        //  monthYear = new SimpleDateFormat("MMM-yyyy", Locale.getDefault()).format(date);


        final JSONObject p = new JSONObject();
        p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this));
        p.put("admissionId", studentEntity.AdmissionId);
        p.put("monthYear", monthYear);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/feesPayment/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                progressBar2.setVisibility(View.GONE);
                switch (code) {
                    case "200":
                        try {

                            ArrayList<FeeReceipt> existingReceipts = new Gson()
                                    .fromJson(job.getJSONObject("result").getJSONObject("studentFees")
                                            .optString("PaidFeeReciept"), new TypeToken<List<FeeReceipt>>() {
                                    }.getType());

                            if (existingReceipts.size() > 0) {
                                feeReceipts.clear();
                                feeReceipts.addAll(existingReceipts);
                                adapter.notifyDataSetChanged();
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.top_layout), R.color.fragment_first_green);
                            } else {
                                Config.responseSnackBarHandler(getString(R.string.fee_receipt) + " " + getString(R.string.not_available),
                                        findViewById(R.id.top_layout), R.color.fragment_first_green);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar2.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(FeeReceiptsActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class FeeReceiptsAdapter extends RecyclerView.Adapter<FeeReceiptsAdapter.MyViewHolder> {

        @NonNull
        @Override
        public FeeReceiptsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rv_fee_receipt, parent, false);

            return new MyViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull FeeReceiptsAdapter.MyViewHolder holder, int position) {
            holder.tv_transaction_date.setText(feeReceipts.get(position).TransactionDate);
            holder.tv_transaction_amount.setText(feeReceipts.get(position).TransactionAmount);
            holder.tv_transaction_remarks.setText(feeReceipts.get(position).TransactionRemarks);
        }

        @Override
        public int getItemCount() {
            return feeReceipts.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_transaction_date, tv_transaction_amount, tv_transaction_remarks;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_transaction_date = itemView.findViewById(R.id.tv_transaction_date);
                tv_transaction_amount = itemView.findViewById(R.id.tv_transaction_amount);
                tv_transaction_remarks = itemView.findViewById(R.id.tv_transaction_remarks);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String receipt = new Gson().toJson(feeReceipts.get(getAdapterPosition()));
                        AlertDialog.Builder choices = new AlertDialog.Builder(FeeReceiptsActivity.this);
                        String[] aa = new String[]
                                {
                                        getResources().getString(R.string.fee_receipt)
                                };

                        choices.setItems(aa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which) {
                                    case 0:
                                        Intent intent = new Intent(FeeReceiptsActivity.this, ViewFeeReceiptActivity.class);
                                        intent.putExtra("student", selectedStudent);
                                        intent.putExtra("receipt", receipt);
                                        startActivity(intent);
                                        break;
                                }

                            }
                        });
                        choices.show();
                    }
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Gc.FEE_RECEIVE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.top_layout), R.color.fragment_first_green);

                try {
                    getFeeReceiptsForStudent();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == Gc.PAYONLINE_FEE_RECEIVE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.top_layout), R.color.fragment_first_green);

                try {
                    getFeeReceiptsForStudent();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);

    }
}

class FeeReceipt {
    public String TransactionDate;
    public String TransactionId;
    public String TransactionAmount;
    public String TransactionRemarks;
}
