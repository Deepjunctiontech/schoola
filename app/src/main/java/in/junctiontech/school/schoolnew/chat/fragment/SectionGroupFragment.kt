package `in`.junctiontech.school.schoolnew.chat.fragment

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity
import `in`.junctiontech.school.schoolnew.chat.ChatConversationActivity
import `in`.junctiontech.school.schoolnew.chat.adapter.SectionGroupFragmentAdapter
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.ClassNameSectionName
import `in`.junctiontech.school.schoolnew.model.CommunicationPermissions
import `in`.junctiontech.school.schoolnew.model.StudentInformation
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.common.base.Strings
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class SectionGroupFragment : Fragment(), SectionGroupFragmentAdapter.ClassClickListener {

    var mDb: MainDatabase? = null
    private lateinit var adapter: SectionGroupFragmentAdapter
    private lateinit var contexts: Context

    private var chatPermissions = ArrayList<String>()
    private lateinit var studentInfo: StudentInformation
    private lateinit var staffInfo: SignedInStaffInformationEntity
    private var mRecyclerView: RecyclerView? = null
    var studentList = ArrayList<SchoolStudentEntity>()
    private var classList = ArrayList<ClassNameSectionName>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        val bundle = this.arguments
        if (bundle != null) {
            val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)
            if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                studentInfo = Gson().fromJson(bundle.getString(Gc.STUDENTPARENT), StudentInformation::class.java)
            } else if (role.equals(Gc.STAFF, ignoreCase = true)) {
                staffInfo = Gson().fromJson(bundle.getString(Gc.STAFF), SignedInStaffInformationEntity::class.java)
            }
        }

        mDb = MainDatabase.getDatabase(contexts)
        val view: View = inflater.inflate(R.layout.fragment_staff_list, container, false)
        mRecyclerView = view.findViewById(R.id.staffList) as RecyclerView

        mDb!!.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, contexts))
                .observe(viewLifecycleOwner, Observer<List<CommunicationPermissions>> { communicationPermissions: List<CommunicationPermissions> ->
                    chatPermissions.clear()
                    for (c in communicationPermissions) {
                        chatPermissions.add(c.ToUserType)
                    }
                    initialize(view)
                })


        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexts = context
    }

    private fun initialize(view: View) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        mRecyclerView!!.layoutManager = layoutManager

        val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, contexts)

        mDb!!.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, context))
                .observe(viewLifecycleOwner, Observer<List<ClassNameSectionName>> { classNameSectionNames: List<ClassNameSectionName> ->
                    classList.clear()
                    if (role.equals(Gc.STUDENTPARENT, ignoreCase = true)) {

                        for (c in Objects.requireNonNull(classNameSectionNames)) {
                            if (c.ClassId.equals(studentInfo.ClassId, ignoreCase = true)) classList.add(c)
                        }
                    } else if (role.equals(Gc.STAFF, ignoreCase = true)) {
                        if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")) {
                            classList.addAll(classNameSectionNames)
                        } else {
                            Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to)
                                    + getString(R.string.class_and_section),
                                    view.findViewById(R.id.staffFragment), R.color.fragment_first_blue)
                        }
                    } else {
                        classList.addAll(classNameSectionNames)
                    }

                    adapter = SectionGroupFragmentAdapter(classList, contexts, activity, this)
                    mRecyclerView!!.adapter = adapter
                    adapter.notifyDataSetChanged()
                })
    }

    override fun onStartClassChatCallback(position: Int) {
        val intent = Intent(activity, ChatConversationActivity::class.java)
        val section = Gson().toJson(classList[position])
        intent.putExtra("with", section)
        intent.putExtra("withType", "section")
        startActivity(intent)
    }

    companion object {
        var TAG = SectionGroupFragment::class.java.simpleName
    }
}