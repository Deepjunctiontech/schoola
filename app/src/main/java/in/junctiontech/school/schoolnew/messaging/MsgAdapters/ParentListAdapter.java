package in.junctiontech.school.schoolnew.messaging.MsgAdapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class ParentListAdapter extends RecyclerView.Adapter<ParentListAdapter.MyViewHolder> {
    private ArrayList<SchoolStudentEntity> studentList;

    private Context context;
    private int appColor;

    public ParentListAdapter(Context context, ArrayList<SchoolStudentEntity> studentList, int appColor) {
        this.studentList = studentList;
        this.context = context;
        this.appColor = appColor;

    }

    @Override
    public ParentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
        return new ParentListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ParentListAdapter.MyViewHolder holder, int position) {
        final SchoolStudentEntity studentObj = studentList.get(position);
        String next = "<font color='#727272'>" + "(" + studentObj.AdmissionNo + ")" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(
                studentObj.StudentName.replace("_", " ") + " " + next));

//
//        holder.tv_designation.setText(context.getString(R.string.class_text)+
//                " : "+ studentObj.getClassName()+" "+studentObj.getSectionName());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void updateList(ArrayList<SchoolStudentEntity> parentListData) {
        this.studentList=parentListData;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;
        //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
        // Button btn_item_create_class_add_section;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_slot_start_time);
            tv_item_name.setTextColor(appColor);
//            tv_designation = (TextView) itemView.findViewById(R.id.tv_designation);
//            tv_designation.setVisibility(View.VISIBLE);
//            //    btn_item_create_class_add_section = (Button) itemView.findViewById(R.id.btn_item_create_class_add_section);
//            //  ly_item_create_class_sections = (LinearLayout) itemView.findViewById(R.id.ly_item_create_class_sections);
//

           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
//                   ((ConversationListActivity)context).
//                           startChat(
//                                   studentList.get(getLayoutPosition()),
//                                   "Parent",
//                                   true // we use false for we will  refresh chat list on back
//                                   //pressed
//                           );
               }
           });



        }
    }

    public void setFilter(ArrayList<SchoolStudentEntity> studentList) {
        this.studentList = new ArrayList<>();
        this.studentList.addAll(studentList);
        notifyDataSetChanged();
    }

}