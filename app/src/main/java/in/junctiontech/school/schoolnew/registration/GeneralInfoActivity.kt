package `in`.junctiontech.school.schoolnew.registration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.common.Gc
import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class GeneralInfoActivity : AppCompatActivity() {
    private var progressBar: ProgressBar? = null
    private lateinit var progressText: TextView
    private lateinit var lLState: LinearLayout
    private lateinit var lLCity: LinearLayout
    private lateinit var lLBtnNext: LinearLayout


    private lateinit var nextButton: Button
    private lateinit var btnCountry: Button
    private lateinit var btnState: Button
    private lateinit var btnCity: Button
    lateinit var selectedStudent: String

    private var selectedCountryPosition = 0
    private var selectedStatePosition = -1
    private var selectedCityPosition = -1

    var countryNameList = ArrayList<String>()
    var countryList = ArrayList<CountryData>()
    private var countryListBuilder: AlertDialog.Builder? = null
    var countryListAdapter: ArrayAdapter<String>? = null

    var stateNameList = ArrayList<String>()
    var stateList = ArrayList<StateData>()
    private var stateListBuilder: AlertDialog.Builder? = null
    var stateListAdapter: ArrayAdapter<String>? = null

    var cityNameList = ArrayList<String>()
    var cityList = ArrayList<CityData>()
    private var cityListBuilder: AlertDialog.Builder? = null
    var cityListAdapter: ArrayAdapter<String>? = null
    private var find: String? = null
    private var countryName: String? = null
    private var findInstitute = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_info_general)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        find = intent.getStringExtra("findInstitute")
        if (!find.isNullOrEmpty() && find!!.isNotEmpty()) {
            findInstitute = true
        }
        initializeViews()

        getCountryCode()
        fetchCountry()
    }


    private fun getCountryCode() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val tm = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val countryCodeValue = tm.networkCountryIso
            if (countryCodeValue == "in") {
                countryName = "india"
            }
        } else {
            requestPermission()
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(
                    Manifest.permission.READ_PHONE_STATE
            ), Config.LOCATION)
        }
    }

    override fun onRequestPermissionsResult(permsRequestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (permsRequestCode == Config.LOCATION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) { //denied
                    Log.e("denied", permissions[0])
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) { //allowed
                        Log.e("allowed", permissions[0])
                    }
                }
            }
        }
    }

    private fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)
        progressText = findViewById(R.id.progressText)
        lLState = findViewById(R.id.ll_state)
        lLCity = findViewById(R.id.ll_city)
        lLBtnNext = findViewById(R.id.ll_btn_next)
        /*  Country Select Adapter */
        btnCountry = findViewById(R.id.btn_country)
        countryListBuilder = AlertDialog.Builder(this@GeneralInfoActivity)
        countryListAdapter = ArrayAdapter(this@GeneralInfoActivity, android.R.layout.select_dialog_singlechoice)
        countryListAdapter!!.addAll(countryNameList)
        btnCountry.setOnClickListener {
            countryListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            countryListBuilder!!.setAdapter(countryListAdapter) { _: DialogInterface?, i: Int ->
                btnCountry.text = countryNameList[i]
                selectedCountryPosition = i

                btnState.text = getString(R.string.state)
                btnCity.text = getString(R.string.city)

                selectedStatePosition = -1
                selectedCityPosition = -1

                stateList.clear()
                stateNameList.clear()
                stateListAdapter!!.clear()
                stateListAdapter!!.addAll(stateNameList)
                stateListAdapter!!.notifyDataSetChanged()

                cityList.clear()
                cityNameList.clear()
                cityListAdapter!!.clear()
                cityListAdapter!!.addAll(cityNameList)
                cityListAdapter!!.notifyDataSetChanged()

                if (countryNameList[i].toLowerCase(Locale.getDefault()).equals("india", ignoreCase = true)) {
                    lLState.visibility = View.VISIBLE
                    lLCity.visibility = View.GONE
                    lLBtnNext.visibility = View.GONE
                } else {
                    if (findInstitute) {
                        val intent = Intent(this@GeneralInfoActivity, SearchInstituteActivity::class.java)
                        intent.putExtra("code", countryList[selectedCountryPosition].code.toString())
                        intent.putExtra("countryName", countryList[selectedCountryPosition].countryName.toString())
                        startActivity(intent)
                    } else {
                        lLState.visibility = View.VISIBLE
                        lLCity.visibility = View.VISIBLE
                        lLBtnNext.visibility = View.VISIBLE
                    }
                }
                fetchState()
            }
            val dialog: AlertDialog = countryListBuilder!!.create()
            dialog.show()
        }

        /*  State Select Adapter */
        btnState = findViewById(R.id.btn_state)
        stateListBuilder = AlertDialog.Builder(this@GeneralInfoActivity)
        stateListAdapter = ArrayAdapter(this@GeneralInfoActivity, android.R.layout.select_dialog_singlechoice)
        stateListAdapter!!.addAll(stateNameList)
        btnState.setOnClickListener {
            stateListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            stateListBuilder!!.setAdapter(stateListAdapter) { _: DialogInterface?, i: Int ->
                btnState.text = stateNameList[i]
                selectedStatePosition = i
                btnCity.text = getString(R.string.city)

                selectedCityPosition = -1

                cityList.clear()
                cityNameList.clear()
                cityListAdapter!!.clear()
                cityListAdapter!!.addAll(cityNameList)
                cityListAdapter!!.notifyDataSetChanged()

                fetchCity()
                if (countryNameList[selectedCountryPosition].toLowerCase(Locale.getDefault()).equals("india", ignoreCase = true)) {
                    lLCity.visibility = View.VISIBLE
                    lLBtnNext.visibility = View.GONE
                } else {
                    lLCity.visibility = View.VISIBLE
                    lLBtnNext.visibility = View.VISIBLE
                }
            }
            val dialog: AlertDialog = stateListBuilder!!.create()
            dialog.show()
        }

        /*  City Select Adapter */
        btnCity = findViewById(R.id.btn_city)
        cityListBuilder = AlertDialog.Builder(this@GeneralInfoActivity)
        cityListAdapter = ArrayAdapter(this@GeneralInfoActivity, android.R.layout.select_dialog_singlechoice)
        cityListAdapter!!.addAll(cityNameList)
        btnCity.setOnClickListener {
            cityListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            cityListBuilder!!.setAdapter(cityListAdapter) { _: DialogInterface?, i: Int ->
                selectedCityPosition = i
                btnCity.text = cityNameList[i]
                lLBtnNext.visibility = View.VISIBLE
            }
            val dialog: AlertDialog = cityListBuilder!!.create()
            dialog.show()
        }

        nextButton = findViewById(R.id.next_button)

        nextButton.setOnClickListener {
            val intent = Intent(this@GeneralInfoActivity, SearchInstituteActivity::class.java)
            intent.putExtra("code", countryList[selectedCountryPosition].code.toString())
            intent.putExtra("countryName", countryList[selectedCountryPosition].countryName.toString())
            if (stateList.size > 0 && selectedStatePosition > -1) {
                intent.putExtra("stateName", stateList[selectedStatePosition].name.toString())
            }
            if (cityList.size > 0 && selectedCityPosition > -1) {
                intent.putExtra("cityName", cityList[selectedCityPosition].toponymName.toString())
            }
            startActivity(intent)
        }
    }

    private fun fetchCountry() {
        progressBar!!.visibility = View.VISIBLE
        progressText.visibility = View.VISIBLE

        val url = (Gc.ERPCPANELURL + "CountrysDetailApi")
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            when (code) {
                Gc.APIRESPONSE200 -> try {
                    val country = Gson()
                            .fromJson<ArrayList<CountryData>>(job.optString("result"), object : TypeToken<List<CountryData>>() {
                            }.type)
                    countryList.clear()
                    countryNameList.clear()
                    countryList.addAll(country)
                    // countryNameList.add("Select Country")
                    for (i in countryList.indices) {
                        if (countryList[i].countryName!!.toLowerCase(Locale.getDefault()).equals(countryName, ignoreCase = true)) {
                            btnCountry.text = countryList[i].countryName
                            selectedCountryPosition = i
                            fetchState()
                            lLState.visibility = View.VISIBLE
                        }
                        countryNameList.add(countryList[i].countryName.toString())
                    }
                    countryListAdapter!!.clear()
                    countryListAdapter!!.addAll(countryNameList)
                    countryListAdapter!!.notifyDataSetChanged()


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            Config.responseVolleyErrorHandler(this@GeneralInfoActivity, error, findViewById<View>(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                /*val headers = HashMap<String, String>()
                return headers*/
                return HashMap()
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun fetchState() {
        progressBar!!.visibility = View.VISIBLE
        progressText.visibility = View.VISIBLE

        val url = (Gc.ERPCPANELURL + "StatesDetailApi")
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            when (code) {
                Gc.APIRESPONSE200 -> try {
                    val state = Gson()
                            .fromJson<ArrayList<StateData>>(job.optString("result"), object : TypeToken<List<StateData>>() {

                            }.type)
                    stateList.clear()
                    stateNameList.clear()
                    stateList.addAll(state)
                    for (i in stateList.indices) {
                        stateNameList.add(stateList[i].name.toString())
                    }
                    stateListAdapter!!.clear()
                    stateListAdapter!!.addAll(stateNameList)
                    stateListAdapter!!.notifyDataSetChanged()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            Config.responseVolleyErrorHandler(this@GeneralInfoActivity, error, findViewById<View>(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val jobFilter = JSONObject()
                try {
                    jobFilter.put("geonameId", countryList[selectedCountryPosition].geonameId)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                headers["filter"] = jobFilter.toString()
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun fetchCity() {
        progressBar!!.visibility = View.VISIBLE
        progressText.visibility = View.VISIBLE
        val url = (Gc.ERPCPANELURL + "CitysDetailApi")
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            when (code) {
                Gc.APIRESPONSE200 -> try {
                    val city = Gson()
                            .fromJson<ArrayList<CityData>>(job.optString("result"), object : TypeToken<List<CityData>>() {

                            }.type)
                    cityList.clear()
                    cityNameList.clear()
                    cityList.addAll(city)
                    for (i in cityList.indices) {
                        cityNameList.add(cityList[i].toponymName.toString())
                    }
                    cityListAdapter!!.clear()
                    cityListAdapter!!.addAll(cityNameList)
                    cityListAdapter!!.notifyDataSetChanged()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            progressText.visibility = View.GONE

            Config.responseVolleyErrorHandler(this@GeneralInfoActivity, error, findViewById<View>(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val jobFilter = JSONObject()
                try {
                    jobFilter.put("citynameId", stateList[selectedStatePosition].stategeonameId)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                headers["filter"] = jobFilter.toString()
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }


        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

class CountryData {
    var code: String? = null
    var countryName: String? = null
    var geonameId: String? = null
}

class StateData {
    var stategeonameId: String? = null
    var name: String? = null
}

class CityData {
    var toponymName: String? = null
}