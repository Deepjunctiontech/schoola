package `in`.junctiontech.school.schoolnew.assignments.adpter


import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.assignments.AssignmentUpdateActivity
import `in`.junctiontech.school.schoolnew.assignments.modelhelper.EvaluateHomework
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import java.util.*

class AssignmentEvaluateListAdapter internal constructor(private val context: Context,
                                                         private var evaluateHomeworkArrayList: ArrayList<EvaluateHomework>)
    : RecyclerView.Adapter<AssignmentEvaluateListAdapter.MyViewHolder>() {

    private val homeworkCompleteColor: Int = context.resources.getColor(android.R.color.holo_green_dark)
    private val homeworkIncompleteColor: Int = context.resources.getColor(android.R.color.holo_red_dark)
    private val homeworkSubmittedColor: Int = context.resources.getColor(android.R.color.holo_orange_light)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_homework_evaluation,
                parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val obj: EvaluateHomework = evaluateHomeworkArrayList[position]
        if ("" != obj.profileUrl) Glide.with(context)
                .load(obj.profileUrl)
                .apply(RequestOptions()
                        .error(R.drawable.ic_single)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(RequestOptions.circleCropTransform())
                .thumbnail(0.5f)
                .into(holder.hivItemProfileImage) else holder.hivItemProfileImage.setImageDrawable(context.getDrawable(R.drawable.ic_single))


        holder.hckNameCheck.text = obj.studentName
        holder.htvAdmissionNumber.text = obj.AdmissionNo
        if (!obj.homeworkDetailsId.isNullOrEmpty()) {
            holder.btnViewHomework.isClickable = true
            holder.btnViewHomework.text = context.resources.getString(R.string.view_student_homework)
        } else {
            holder.btnViewHomework.isClickable = false
            holder.btnViewHomework.text =context.resources.getString(R.string.homework_not_submitted)
        }

        if (obj.status != null && obj.status.equals("C", ignoreCase = true)) {
            obj.status = "C"
            holder.rlHomeworkItemLayout.setBackgroundColor(homeworkCompleteColor)
        } else if (obj.status != null && obj.status.equals("INC", ignoreCase = true)) {
            obj.status = "INC"
            holder.rlHomeworkItemLayout.setBackgroundColor(homeworkIncompleteColor)
        } else if (obj.status != null && obj.status.equals("S", ignoreCase = true)) {
            obj.status = "S"
            holder.rlHomeworkItemLayout.setBackgroundColor(homeworkSubmittedColor)
        } else {
            obj.status = ""
            holder.rlHomeworkItemLayout.setBackgroundColor(context.resources.getColor(R.color.white))
        }


    }

    override fun getItemCount(): Int {
        return evaluateHomeworkArrayList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rlHomeworkItemLayout: RelativeLayout = itemView.findViewById(R.id.rl_evaluation)
        var hivItemProfileImage: ImageView = itemView.findViewById(R.id.hiv_item_profile_image)
        var hckNameCheck: TextView = itemView.findViewById(R.id.hck_name_check)
        var htvAdmissionNumber: TextView = itemView.findViewById(R.id.htv_admission_number)
        var btnViewHomework: Button = itemView.findViewById(R.id.btn_view_homework)

        init {

            btnViewHomework.setOnClickListener {
                val intent = Intent(context, AssignmentUpdateActivity::class.java)
                 intent.putExtra("homeworkId", evaluateHomeworkArrayList[layoutPosition].homeworkId)
                 intent.putExtra("studentId", evaluateHomeworkArrayList[layoutPosition].AdmissionID)
                 context.startActivity(intent)
            }


            rlHomeworkItemLayout.setOnClickListener {
                Log.e("Status", evaluateHomeworkArrayList[layoutPosition].toString())
                when (evaluateHomeworkArrayList[layoutPosition].status) {
                    "C" -> {
                        rlHomeworkItemLayout.setBackgroundColor(homeworkIncompleteColor)
                        evaluateHomeworkArrayList[layoutPosition].status = "INC"
                    }
                    "INC" -> {
                        rlHomeworkItemLayout.setBackgroundColor(homeworkSubmittedColor)
                        evaluateHomeworkArrayList[layoutPosition].status = "S"
                    }
                    "S" -> {
                        rlHomeworkItemLayout.setBackgroundColor(context.resources.getColor(android.R.color.white))
                        evaluateHomeworkArrayList[layoutPosition].status = ""
                    }
                    "N" -> {
                        rlHomeworkItemLayout.setBackgroundColor(homeworkCompleteColor)
                        evaluateHomeworkArrayList[layoutPosition].status = "C"
                    }
                    else -> {
                        rlHomeworkItemLayout.setBackgroundColor(homeworkCompleteColor)
                        evaluateHomeworkArrayList[layoutPosition].status = "C"
                    }
                }
                notifyDataSetChanged()
            }
        }
    }
}