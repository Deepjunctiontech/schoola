package `in`.junctiontech.school.schoolnew.assignments

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.MasterEntryEntity
import `in`.junctiontech.school.schoolnew.assignments.adpter.HomeworkDetailsAdapter
import `in`.junctiontech.school.schoolnew.assignments.menu.widget.HomeWorkMenuEditText
import `in`.junctiontech.school.schoolnew.assignments.menu.widget.HomeworkKeyBoardPopup
import `in`.junctiontech.school.schoolnew.assignments.modelhelper.HomeworkDetails
import `in`.junctiontech.school.schoolnew.chatdb.ChatListEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_update_assignment.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by lekhpal Dangi on December 22 2020.
 */

class AssignmentUpdateActivity : AppCompatActivity(), HomeWorkMenuEditText.PopupListener, HomeworkKeyBoardPopup.attachmentClickListener {

    var mDb: MainDatabase? = null

    private var btnEvaluateHomework: Button? = null
    lateinit var menuKeyboard: HomeworkKeyBoardPopup

    private lateinit var rootView: RelativeLayout
    private lateinit var etHomeworkMessageChat: HomeWorkMenuEditText

    var homeworkId: String? = null
    var sectionId: String? = null
    var userName: String? = null

    private var btnClassSection: Button? = null
    private var btnEvaluationStatus: Button? = null
    private var btnSubject: Button? = null
    var btnAssignmentDate: Button? = null
    var btnSubmissionDate: Button? = null
    var dosubmission: Long? = null
    var afterHWSubmissionDate: String? = null

    var tvAssignmentTitle: TextView? = null
    var rvHomeworkList: RecyclerView? = null
    private var btnSendHomework: ImageView? = null
    private var studentId: String? = null
    var userId: String? = null
    private var adapter: HomeworkDetailsAdapter? = null

    var homeworkDetails: ArrayList<HomeworkDetails> = ArrayList()
    private var colorIs: Int = 0
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
        btnEvaluationStatus!!.setBackgroundColor(colorIs)
    }

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_assignment)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mDb = MainDatabase.getDatabase(this)
        btnClassSection = findViewById(R.id.btn_class_section)
        btnEvaluationStatus = findViewById(R.id.btn_evaluation_status)
        btnSubject = findViewById(R.id.btn_subject)
        btnAssignmentDate = findViewById(R.id.btn_assignment_date)
        btnSubmissionDate = findViewById(R.id.btn_submission_date)
        tvAssignmentTitle = findViewById(R.id.tv_assignment_title)
        rvHomeworkList = findViewById(R.id.rv_homework_list)
        btnSendHomework = findViewById(R.id.btn_send_homework)
        btnEvaluateHomework = findViewById(R.id.btn_evaluate_homework)

        homeworkId = intent.getStringExtra("homeworkId")
        studentId = intent.getStringExtra("studentId")

        if (!studentId.isNullOrEmpty()) {
            btnEvaluateHomework!!.visibility = View.GONE
        }

        if (homeworkId.isNullOrEmpty()) {
            finish()
        }

        val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)

        when {
            role.equals(Gc.STUDENTPARENT, ignoreCase = true) -> {
                studentId = Gc.getSharedPreference(Gc.USERID, applicationContext)
            }
            role.equals(Gc.STAFF, ignoreCase = true) -> {
                userId = Gc.getSharedPreference(Gc.USERID, applicationContext)
                userName = Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this)
            }
            else -> {
                userId = Gc.getSharedPreference(Gc.USERID, applicationContext)
                userName = Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this)
            }
        }

        getHomeworkDetails(homeworkId, studentId)
        getImportantMediaStorageURL()

        btnEvaluateHomework!!.setOnClickListener {
            val intent = Intent(this, EvaluateHomeworkActivity::class.java)
            intent.putExtra("homeworkId", homeworkId)
            intent.putExtra("SectionId", sectionId)
            startActivity(intent)
        }

        if (Gc.getSharedPreference(Gc.HOMEWORKCREATEALLOWED, this).equals(Gc.TRUE, ignoreCase = true) && studentId.isNullOrEmpty()) {
            btnEvaluateHomework!!.visibility = View.VISIBLE
        }

        btnSendHomework!!.setOnClickListener { sendHomework("", "text") }

        rvHomeworkList!!.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundColor))

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = false
        rvHomeworkList!!.layoutManager = layoutManager
        adapter = HomeworkDetailsAdapter(this, homeworkDetails)
        rvHomeworkList!!.adapter = adapter
        rvHomeworkList!!.isLongClickable = true

        rootView = findViewById(R.id.rl_homework_send)
        etHomeworkMessageChat = findViewById(R.id.et_homework_message_chat)

        etHomeworkMessageChat.popupListener = this

        menuKeyboard = HomeworkKeyBoardPopup(
                this,
                rootView,
                et_homework_message_chat,
                et_homework_message_chat,
                menuHomeworkContainer,
                this
        )
        iv_homework_chat.setOnClickListener {
            toggle()
        }

        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        setColorApp()
    }

    private fun getImportantMediaStorageURL() {

        val resentList = mDb?.masterEntryModel()?.getMasterEntryValues(Gc.ImportantStorageURL) as java.util.ArrayList<MasterEntryEntity?>

        if (!resentList.isNullOrEmpty()) {
            if (!resentList[0]?.MasterEntryValue.isNullOrEmpty()) {
                var s = resentList[0]?.MasterEntryValue?.toLowerCase(Locale.getDefault())
                if (s != null) {
                    s = s.substring(0, s.length - if (s.endsWith("/")) 1 else 0)
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.IMPORTANTMEDIASTORAGEURL] = s
                    Gc.setSharedPreference(setDefaults, this)
                }

            } else {
                Config.responseSnackBarHandler(resources.getString(R.string.sending_media_is_disabled_for_your_organization_Please_contact),
                        findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
            }
        }

        val AfterHomeworkSubmissionDate: List<MasterEntryEntity> = mDb!!.masterEntryModel().getMasterEntryValues(Gc.AfterHomeworkSubmissionDate)
        if (AfterHomeworkSubmissionDate.isNotEmpty()) {
            if (!AfterHomeworkSubmissionDate[0].MasterEntryValue.isNullOrEmpty()) {
                afterHWSubmissionDate = AfterHomeworkSubmissionDate[0].MasterEntryValue?.toLowerCase(Locale.getDefault())

            } else {
                Config.responseSnackBarHandler(resources.getString(R.string.sending_media_is_disabled_for_your_organization_Please_contact),
                        findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
            }
        }
        /*
            val param = JSONObject()
            param.put("MasterEntryName", "ImportantMediaStorageURL")
            val url = (Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "master/masterEntries")
            val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.GET, "$url/$param", JSONObject(), Response.Listener { job: JSONObject ->
                when (job.optString("code")) {
                    Gc.APIRESPONSE200 -> {
                        val resultjobj = job.getJSONObject("result")
                        val masterEntry = resultjobj.getJSONArray("masterEntry")
                        var s = masterEntry.getJSONObject(0).optString("MasterEntryValue").toLowerCase(Locale.getDefault())
                        s = s.substring(0, s.length - if (s.endsWith("/")) 1 else 0)
                        val setDefaults = HashMap<String, String>()
                        setDefaults[Gc.IMPORTANTMEDIASTORAGEURL] = s
                        Gc.setSharedPreference(setDefaults, this)
                    }
                    else -> {
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)

                    }
                }
            }, Response.ErrorListener { }) {
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["APPKEY"] = Gc.APPKEY
                    headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                    headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                    headers["Content-Type"] = Gc.CONTENT_TYPE
                    headers["DEVICE"] = Gc.DEVICETYPE
                    headers["DEVICEID"] = Gc.id(applicationContext)
                    headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                    headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                    headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                    headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                    return headers
                }

                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }
            }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
            AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)*/
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun sendHomework(mediaUrl: String?, descriptionType: String?) {
        val todays = Date()
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        val todayss = sdf.format(todays)

        val tody = sdf.parse(todayss)

        val to = tody.time

        val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)

        if (role.equals(Gc.STUDENTPARENT, ignoreCase = true) && afterHWSubmissionDate?.isNotEmpty() == true && dosubmission != null && afterHWSubmissionDate.equals("disallow") && dosubmission!! < to) {
            val alertLocationInfo = androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
            alertLocationInfo.setTitle("Media Alert")
            alertLocationInfo.setIcon(R.drawable.ic_alert)
            alertLocationInfo.setMessage(R.string.after_homework_submission_date_not_allowed_to_submit_homework)
            alertLocationInfo.setNegativeButton(getString(R.string.ok)) { _: DialogInterface?, _: Int -> }
            alertLocationInfo.setCancelable(false)
            alertLocationInfo.show()
        } else {
            progressDialog.setMessage(getString(R.string.please_wait))
            progressDialog.show()
            val today = Date()
            val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            val date = enteredDateFormat.format(today)

            val param = JSONObject()
            param.put("homeworkId", homeworkId)
            param.put("studentId", studentId)
            param.put("userId", userId)
            param.put("userName", userName)
            param.put("date", date)
            if (!etHomeworkMessageChat.text.isNullOrEmpty()) {
                param.put("description", etHomeworkMessageChat.text.toString())
            }

            if (!mediaUrl.isNullOrEmpty()) {
                param.put("mediaUrl", mediaUrl)
            }

            param.put("descriptionType", descriptionType)
            val url = (Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "homework/homeworkDetails")
            val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
                progressDialog.dismiss()
                when (job.optString("code")) {
                    Gc.APIRESPONSE200 -> {
                        val addHomeworkDetails = Gson()
                                .fromJson<ArrayList<HomeworkDetails>>(job.getJSONObject("result").optString("homeworkDetails"),
                                        object : TypeToken<List<HomeworkDetails>>() {}.type)
                        homeworkDetails.addAll(addHomeworkDetails)
                        adapter!!.notifyDataSetChanged()
                        if (homeworkDetails.size > 0) {
                            val position = homeworkDetails.size - 1
                            rvHomeworkList!!.smoothScrollToPosition(position)
                        }
                        etHomeworkMessageChat.text.clear()
                    }
                    else -> Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
                }
            }, Response.ErrorListener { progressDialog.dismiss() }) {
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["APPKEY"] = Gc.APPKEY
                    headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                    headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                    headers["Content-Type"] = Gc.CONTENT_TYPE
                    headers["DEVICE"] = Gc.DEVICETYPE
                    headers["DEVICEID"] = Gc.id(applicationContext)
                    headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                    headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                    headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                    headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                    return headers
                }

                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray? {
                    return try {
                        param.toString().toByteArray(charset("utf-8"))
                    } catch (uee: UnsupportedEncodingException) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                        null
                    }
                }
            }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
            AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
        }

    }

    private fun getHomeworkDetails(homeworkId: String?, studentId: String?) {
        progressBar.visibility = View.VISIBLE
        val param = JSONObject()
        param.put("homeworkId", homeworkId)
        if (!studentId.isNullOrEmpty()) {
            param.put("studentId", studentId)
        }
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "homework/homeworkDetails/" + param)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val tem = job.getJSONObject("result").getJSONArray("homeworkDetails")
                    val tem2 = tem.getJSONObject(0)
                    val className = tem2.optString("ClassName")
                    val sectionName = tem2.optString("SectionName")
                    sectionId = tem2.optString("sectionid")
                    val evaluationstatus = tem2.optString("evaluationstatus")
                    if (evaluationstatus == "C") {
                        btnEvaluationStatus!!.visibility = View.VISIBLE
                        btnEvaluationStatus!!.text = "This Homework Is COMPLETE"
                    } else if (evaluationstatus.toLowerCase(Locale.ROOT) == "inc") {
                        btnEvaluationStatus!!.visibility = View.VISIBLE
                        btnEvaluationStatus!!.text = "This Homework Is INCOMPLETE"
                    }
                    btnClassSection!!.text = "$className $sectionName"
                    btnSubject!!.text = tem2.optString("SubjectName")
                    btnAssignmentDate!!.text = tem2.optString("dateofhomework")

                    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    val dos = sdf.parse(tem2.optString("dosubmission"))

                    dosubmission = dos.time
                    btnSubmissionDate!!.text = tem2.optString("dosubmission")
                    tvAssignmentTitle!!.text = tem2.optString("homework")
                    val addHomeworkDetails = Gson()
                            .fromJson<ArrayList<HomeworkDetails>>(tem2.optString("homeworkDetails"),
                                    object : TypeToken<List<HomeworkDetails>>() {}.type)
                    homeworkDetails.addAll(addHomeworkDetails)
                    adapter!!.notifyDataSetChanged()
                    if (homeworkDetails.size > 0) {
                        val position = homeworkDetails.size - 1
                        rvHomeworkList!!.smoothScrollToPosition(position)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.rl_update_assignment), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_update_assignment))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                Log.e("Headers", headers.toString())
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun toggle() {
        if (menuKeyboard.isShowing) {
            menuKeyboard.dismiss()
        } else {
            menuKeyboard.show()
        }
    }

    override fun onDestroy() {
        menuKeyboard.clear()
        super.onDestroy()
    }

    override fun getPopup(): PopupWindow {
        return menuKeyboard
    }

    override fun onAttachmentClickListener(menuName: String) {
        if (Gc.getSharedPreference(Gc.IMPORTANTMEDIASTORAGEURL, this).isNullOrEmpty() || Gc.getSharedPreference(Gc.IMPORTANTMEDIASTORAGEURL, this).equals(Gc.NOTFOUND, ignoreCase = true)) {
            val alertLocationInfo = androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
            alertLocationInfo.setTitle("Media Alert")
            alertLocationInfo.setIcon(R.drawable.ic_alert)
            alertLocationInfo.setMessage(R.string.sending_media_is_disabled_for_your_organization_Please_contact)
            alertLocationInfo.setNegativeButton(getString(R.string.ok)) { _: DialogInterface?, _: Int -> }
            alertLocationInfo.setCancelable(false)
            alertLocationInfo.show()
        } else {
            val todays = Date()
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)

            val todayss = sdf.format(todays)

            val tody = sdf.parse(todayss)

            val today = tody.time

            val role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)

            if (role.equals(Gc.STUDENTPARENT, ignoreCase = true) && afterHWSubmissionDate?.isNotEmpty() == true && dosubmission != null && afterHWSubmissionDate.equals("disallow") && dosubmission!! < today) {
                val alertLocationInfo = androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                alertLocationInfo.setTitle("Media Alert")
                alertLocationInfo.setIcon(R.drawable.ic_alert)
                alertLocationInfo.setMessage(R.string.after_homework_submission_date_not_allowed_to_submit_homework)
                alertLocationInfo.setNegativeButton(getString(R.string.ok)) { _: DialogInterface?, _: Int -> }
                alertLocationInfo.setCancelable(false)
                alertLocationInfo.show()
            } else {
                if (menuName == "Image") {
                    CropImage.activity()
                            .start(this)
                } else if (menuName == "PDF") {
                    val intent = Intent()
                    intent.type = "application/pdf"
                    intent.action = Intent.ACTION_GET_CONTENT
                    startActivityForResult(Intent.createChooser(intent, "Select PDF"), 3939)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val result = CropImage.getActivityResult(data)
            val imageUri = result.uri
            val imageStream = contentResolver.openInputStream(imageUri!!)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            val encodedImages = encodeImage(selectedImage)
            uploadImageToServers(encodedImages)
        } else if (requestCode == 3939 && resultCode == Activity.RESULT_OK) {
            val uRI = data!!.data;
            val pdf = convertToString(uRI)
            uploadPdf(pdf)
        }
    }

    private fun convertToString(uri: Uri?): String {
        var ansValue = ""
        val uriString = uri.toString()
        Log.d("data", "onActivityResult: uri$uriString")
        //            myFile = new File(uriString);
        //            ret = myFile.getAbsolutePath();
        //Fpath.setText(ret);
        try {
            val ins = uri?.let { contentResolver.openInputStream(it) }
            val bytes = getBytes(ins)
            //  Log.d("data", "onActivityResult: bytes size=" + bytes.length)
            Log.d("data", "onActivityResult: Base64string=" + Base64.encodeToString(bytes, Base64.DEFAULT))
            ansValue = Base64.encodeToString(bytes, Base64.DEFAULT)
        } catch (e: Exception) {
            // TODO: handle exception
            e.printStackTrace()
            Log.d("error", "onActivityResult: $e")
        }
        return ansValue
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream?): ByteArray {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream!!.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    private fun uploadPdf(pdf: String?) {
        progressDialog.setMessage(getString(R.string.please_wait))
        progressDialog.show()
        val param = JSONObject()
        param.put("fileType", "pdf")
        param.put("type", "pdf")
        param.put("fileData", pdf)
        val url = (Gc
                .getSharedPreference(Gc.IMPORTANTMEDIASTORAGEURL, this)
                + "/homework/homework")

        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
            progressDialog.dismiss()
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    try {
                        sendHomework(job.optString("result"), "pdf")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                else -> {
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
                }
            }
        }, Response.ErrorListener { progressDialog.dismiss() }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun encodeImage(bm: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
    }

    private fun uploadImageToServers(encodedImage: String) {
        progressDialog.setMessage(getString(R.string.please_wait))
        progressDialog.show()
        val param = JSONObject()
        param.put("fileType", "jpeg")
        param.put("type", "image")
        param.put("fileData", encodedImage)
        // val url = "http://mediafree1.zeroerp.com/medias/media"
        val url = (Gc
                .getSharedPreference(Gc.IMPORTANTMEDIASTORAGEURL, this)
                + "/homework/homework")

        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
            progressDialog.dismiss()
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    try {
                        sendHomework(job.optString("result"), "image")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                else -> {
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
                }
            }
        }, Response.ErrorListener { progressDialog.dismiss() }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    fun deleteHomeworkDetails(homeworker: HomeworkDetails, layoutPosition: Int) {
        progressDialog.setMessage(getString(R.string.please_wait))
        progressDialog.show()
        val param = JSONObject()
        param.put("homeworkDetailsId", homeworker.homeworkDetailsId)
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "homework/homeworkDetails/" + param)

        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.DELETE, url, JSONObject(), Response.Listener { job: JSONObject ->
            progressDialog.dismiss()
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    homeworkDetails[layoutPosition].description = "This message was deleted"
                    homeworkDetails[layoutPosition].descriptionType = "text"
                    homeworkDetails[layoutPosition].mediaUrl = ""
                    adapter!!.notifyItemChanged(layoutPosition)
                    if (homeworkDetails.size > 0) {
                        val position = homeworkDetails.size - 1
                        rvHomeworkList!!.smoothScrollToPosition(position)
                    }
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_update_assignment), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { progressDialog.dismiss() }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }
}