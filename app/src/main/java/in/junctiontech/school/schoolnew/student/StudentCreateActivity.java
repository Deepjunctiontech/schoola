package in.junctiontech.school.schoolnew.student;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class StudentCreateActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;
    Button btn_create_student_register_student;
    Button btn_select_student_class;
    Button btn_select_student_gender;
    Button btn_select_caste;
    Button btn_select_category;
    Button btn_select_student_date_of_registration;
    Button btn_select_student_fee_effective_from;
    Button btn_select_student_date_of_birth;
    private boolean isProfileChanged;
    SchoolStudentEntity studentUpdate;

    EditText et_student_admission_number,
            et_student_student_name,
            et_student_father_name,
            et_student_student_mother_name,
            et_student_country_code,
            et_student_student_contact_number,
            et_student_email,
            et_student_present_address,
            et_student_remark;

    TextView tv_transport_fee_name,
            tv_transport_fee_frequency;

    EditText et_transport_amount;

    Button btn_select_transport_distance;

    int selectedSection, selectedGender, selectedCaste, selectedCategory, selectedDistance;

    String feeStructure = new String();

    String selectedRegistrationDate, selectedFeeEffectiveDate;

    ArrayList<String> sectionList = new ArrayList<>();
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;

    ArrayList<String> genderList = new ArrayList<>();
    ArrayList<MasterEntryEntity> genderListMaster = new ArrayList<>();
    AlertDialog.Builder genderListBuilder;
    ArrayAdapter<String> genderListAdapter;


    ArrayList<String> casteList = new ArrayList<>();
    ArrayList<MasterEntryEntity> casteListMaster = new ArrayList<>();
    AlertDialog.Builder casteListBuilder;
    ArrayAdapter<String> casteListAdapter;

    ArrayList<String> categoryList = new ArrayList<>();
    ArrayList<MasterEntryEntity> categoryListMaster = new ArrayList<>();
    AlertDialog.Builder categoryListBuilder;
    ArrayAdapter<String> categoryListAdapter;


    ArrayList<MasterEntryEntity> transportDistanceList = new ArrayList<>();
    ArrayList<String> distanceList = new ArrayList<>();
    AlertDialog.Builder transportListBuilder;
    ArrayAdapter<String> transportListAdapter;

    FeeTypeEntity transportFee;

    ArrayList<ClassFeesTransportEntity> transportAmounts = new ArrayList<>();

    LinearLayout ll_student_admission_number_fee;


    TextView tv_create_student_register_total_session,
            tv_create_student_register_total_quarterly,
            tv_create_student_register_total_monthly;
    SwitchCompat switch_compact_create_student_send_sms;

    SwitchCompat switch_compact_transport;

    private int colorIs;

    CircleImageView iv_student_profile_image;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_create_student_register_student.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_create);

        ll_student_admission_number_fee = findViewById(R.id.ll_student_admission_number_fee);
        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getDataFromRoom();  // gender list

        btn_create_student_register_student = findViewById(R.id.btn_create_student_register_student);
        btn_create_student_register_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    try {
                        createStudent();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        initializeInputFields();
        initializeDatePickers();

        selectClassForStudentCreation();
        selectGenderForStudentCreation();
        selectDistanceForTransportFee();
        selectCasteDialog();
        selectCategoryDialog();
        if (!(Strings.nullToEmpty(getIntent().getStringExtra("student")).equalsIgnoreCase(""))) {
            studentUpdate = new Gson().fromJson(getIntent().getStringExtra("student"), SchoolStudentEntity.class);
            et_student_admission_number.setText(studentUpdate.AdmissionNo);
            btn_select_student_class.setText(studentUpdate.ClassName + " " + studentUpdate.SectionName);
            btn_select_student_class.setEnabled(false);
            btn_select_student_gender.setText(studentUpdate.GenderValue);
            btn_select_student_gender.setEnabled(false); // TODO This should be allowed
            btn_select_student_date_of_registration.setText(studentUpdate.DOR);
            (findViewById(R.id.ll_fee_effectve_from)).setVisibility(View.GONE);
            (findViewById(R.id.tv_fee_structure)).setVisibility(View.GONE);
            (findViewById(R.id.ll_create_student_fee_structure)).setVisibility(View.GONE);
            (findViewById(R.id.ll_total_session)).setVisibility(View.GONE);
            (findViewById(R.id.ll_total_quarterly)).setVisibility(View.GONE);
            (findViewById(R.id.ll_total_monthly)).setVisibility(View.GONE);
            (findViewById(R.id.switch_compact_transport)).setVisibility(View.GONE);
            (findViewById(R.id.btn_select_transport_distance)).setVisibility(View.GONE);
            (findViewById(R.id.ll_transport_fee)).setVisibility(View.GONE);
            et_student_remark.setVisibility(View.GONE);

            btn_create_student_register_student.setVisibility(View.GONE);
//            switch_compact_create_student_send_sms.setVisibility(View.GONE);
            et_student_student_name.setText(Strings.nullToEmpty(studentUpdate.StudentName));
            et_student_father_name.setText(Strings.nullToEmpty(studentUpdate.FatherName));
            et_student_student_mother_name.setText(Strings.nullToEmpty(studentUpdate.MotherName));
            et_student_email.setText(Strings.nullToEmpty(studentUpdate.StudentEmail));
            HashMap<String, String> split = Gc.splitPhoneNumber(Strings.nullToEmpty(studentUpdate.Mobile));
            et_student_country_code.setText(split.get("ccode"));
            et_student_student_contact_number.setText(split.get("phone"));

            btn_select_student_date_of_birth.setText(studentUpdate.DOB);
            et_student_present_address.setText(studentUpdate.PresentAddress);

            if (!(Strings.nullToEmpty(studentUpdate.CasteValue).equalsIgnoreCase(""))) {
                btn_select_caste.setText(studentUpdate.CasteValue);
            }

            if (!(Strings.nullToEmpty(studentUpdate.CategoryValue).equalsIgnoreCase(""))) {
                btn_select_category.setText(studentUpdate.CategoryValue);
            }

            (findViewById(R.id.ll_date_of_birth)).setVisibility(View.VISIBLE);
            iv_student_profile_image.setVisibility(View.VISIBLE);

            if (!(Strings.nullToEmpty(studentUpdate.studentProfileImage).equalsIgnoreCase(""))) {
                Glide.with(this)
                        .load(studentUpdate.studentProfileImage)
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_single))
                        .apply(RequestOptions.errorOf(R.drawable.ic_single))
                        .apply(RequestOptions.circleCropTransform())
                        .into(iv_student_profile_image);
            }

        } else {
            (findViewById(R.id.ll_date_of_birth)).setVisibility(View.GONE);
            (findViewById(R.id.ll_update_student)).setVisibility(View.GONE);
            iv_student_profile_image.setVisibility(View.GONE);
//            mDb.masterEntryModel().getMasterEntryValuesGeneric(Gc.DISTANCE).observe(this, new Observer<List<MasterEntryEntity>>() {
//                @Override
//                public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
//                    transportDistanceList.clear();
//                    distanceList.clear();
//                    transportListAdapter.clear();
//                    transportDistanceList.addAll(masterEntryEntities);
//                    for (MasterEntryEntity d: transportDistanceList){
//                        distanceList.add(d.MasterEntryValue);
//                    }
//                    transportListAdapter.addAll(distanceList);
//                    transportListAdapter.notifyDataSetChanged();
//                }
//            });


//            mDb.feeTypeModel().getFeeTypesTransport().observe(this, new Observer<FeeTypeEntity>() {
//                @Override
//                public void onChanged(@Nullable FeeTypeEntity feeTypeEntity) {
//                    transportFee = feeTypeEntity;
//                }
//            });
        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1999442805", this, adContainer);
        }

    }

    void initializeDatePickers() {

        final Calendar myCalendar = Calendar.getInstance();
        // Date of Birth
        btn_select_student_date_of_birth = findViewById(R.id.btn_select_student_date_of_birth);

        final DatePickerDialog.OnDateSetListener dob = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
            btn_select_student_date_of_birth.setText(sdf.format(myCalendar.getTime()));
        };

        btn_select_student_date_of_birth.setOnClickListener(view -> new DatePickerDialog(StudentCreateActivity.this, dob, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        // Date of Registration
        btn_select_student_date_of_registration = findViewById(R.id.btn_select_student_date_of_registration);
        btn_select_student_date_of_registration.setText(Gc.getSharedPreference(Gc.APPSESSIONSTART, this));
        selectedRegistrationDate = Gc.getSharedPreference(Gc.APPSESSIONSTART, this);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                btn_select_student_date_of_registration.setText(sdf.format(myCalendar.getTime()));

                SimpleDateFormat sdfeng = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                selectedRegistrationDate = sdfeng.format(myCalendar.getTime());
            }

        };

        btn_select_student_date_of_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(StudentCreateActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        // Date Month Fee Effective From

        btn_select_student_fee_effective_from = findViewById(R.id.btn_select_student_fee_effective_from);
        btn_select_student_fee_effective_from.setText(Gc.getSharedPreference(Gc.APPSESSIONSTART, this));

        selectedFeeEffectiveDate = Gc.getSharedPreference(Gc.APPSESSIONSTART, this);

        final DatePickerDialog.OnDateSetListener feedate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                btn_select_student_fee_effective_from.setText(sdf.format(myCalendar.getTime()));

                SimpleDateFormat sdfeng = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                selectedFeeEffectiveDate = sdfeng.format(myCalendar.getTime());
            }
        };

        btn_select_student_fee_effective_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(StudentCreateActivity.this, feedate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    void getDataFromRoom() {

        mDb.masterEntryModel().getMasterEntryValuesGeneric("Gender")
                .observe(this, new Observer<List<MasterEntryEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                        genderListMaster.clear();
                        genderList.clear();
                        genderListMaster.addAll(masterEntryEntities);
                        for (int i = 0; i < genderListMaster.size(); i++) {
                            genderList.add(genderListMaster.get(i).MasterEntryValue);
                        }

                        genderListAdapter.clear();
                        genderListAdapter.addAll(genderList);
                        genderListAdapter.notifyDataSetChanged();
                    }
                });

        mDb.masterEntryModel().getMasterEntryValuesGeneric("Caste")
                .observe(this, masterEntryEntities -> {
                    casteListMaster.clear();
                    casteList.clear();
                    casteListMaster.addAll(masterEntryEntities);
                    for (int i = 0; i < casteListMaster.size(); i++) {
                        casteList.add(casteListMaster.get(i).MasterEntryValue);
                    }
                    casteListAdapter.clear();
                    casteListAdapter.addAll(casteList);
                    casteListAdapter.notifyDataSetChanged();
                });

        mDb.masterEntryModel().getMasterEntryValuesGeneric("Category")
                .observe(this, masterEntryEntities -> {
                    categoryListMaster.clear();
                    categoryList.clear();
                    categoryListMaster.addAll(masterEntryEntities);
                    for (int i = 0; i < categoryListMaster.size(); i++) {
                        categoryList.add(categoryListMaster.get(i).MasterEntryValue);
                    }

                    categoryListAdapter.clear();
                    categoryListAdapter.addAll(categoryList);
                    categoryListAdapter.notifyDataSetChanged();
                });
    }

    private boolean validateInput() {
        if ("".equals(et_student_admission_number.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.admission_number) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
            et_student_admission_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if (!"".equals(et_student_admission_number.getText().toString().trim())) {
            String AdmissionId = "0";
            if (studentUpdate != null && !studentUpdate.RegistrationId.isEmpty()) {
                AdmissionId = studentUpdate.AdmissionId;
            }
            final boolean[] admissionNumberCheck = {false};

            List<SchoolStudentEntity> schoolStudentEntities = mDb.schoolStudentModel().searchAdmissionNo(et_student_admission_number.getText().toString().trim(), AdmissionId);

            if (!schoolStudentEntities.isEmpty()) {
                Config.responseSnackBarHandler(getString(R.string.admission_number) + " " + et_student_admission_number.getText() + " Already Exist",
                        findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                et_student_admission_number.setError(getString(R.string.admission_number) + " " + et_student_admission_number.getText() + " Already Exist", getDrawable(R.drawable.ic_alert));
                admissionNumberCheck[0] = true;
            }

            if (admissionNumberCheck[0]) {
                return false;
            }
        }

        if (getString(R.string.select_class).equals(btn_select_student_class.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.class_text) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
            btn_select_student_class.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if (getString(R.string.gender).equals(btn_select_student_gender.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.gender) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
            btn_select_student_gender.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if ("".equals(et_student_student_name.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.student_name) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
            et_student_student_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        String mobile = et_student_country_code.getText().toString().trim()
                + "-" + et_student_student_contact_number.getText().toString().trim();
        if (mobile.length() > 15 || mobile.length() < 5
                || "".equals(et_student_country_code.getText().toString().trim())
                || "".equals(et_student_student_contact_number.getText().toString().trim())) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_mobile_mumber),
                    findViewById(R.id.rl_student_create), R.color.fragment_first_green);
            et_student_student_contact_number.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert));
            return false;
        }

        if (studentUpdate == null) { // in case of update fee is not provided so dont check
            if (ll_student_admission_number_fee.getChildCount() < 1) {
                Config.responseSnackBarHandler(getString(R.string.class_fee_structure) + " " + getString(R.string.not_available),
                        findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                return false;
            }

            if (switch_compact_transport.isChecked()) {
                String distance = btn_select_transport_distance.getText().toString();
                if (distance.toUpperCase().equals("DISTANCE")) {
                    Config.responseSnackBarHandler("Transport Distance " + getString(R.string.field_can_not_be_blank),
                            findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                    return false;
                }
            }

            if (switch_compact_transport.isChecked() && Strings.nullToEmpty(et_transport_amount.getText().toString()).equals("")) {
                Config.responseSnackBarHandler(getString(R.string.transport_fee) + " " + getString(R.string.field_can_not_be_blank),
                        findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                return false;
            }
        }
        return true;
    }

    private void initializeInputFields() {

        iv_student_profile_image = findViewById(R.id.iv_student_profile_image);

        iv_student_profile_image.setOnClickListener(view -> CropImage.activity()
                .setAspectRatio(1, 1)
                .setMaxCropResultSize(1920, 1080)
                .start(this));

        et_student_present_address = findViewById(R.id.et_student_present_address);

        et_student_admission_number = findViewById(R.id.et_student_admission_number);
        et_student_admission_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0){
                    if(studentUpdate != null && studentUpdate.AdmissionNo.equalsIgnoreCase(s.toString())){
                        return;
                    }
                    Log.i("Admission id is", s.toString());

                    List<SchoolStudentEntity> schoolStudentEntities = mDb.schoolStudentModel().searchAdmissionNoDuplicate(et_student_admission_number.getText().toString().trim());

                    if (!schoolStudentEntities.isEmpty()) {
                        Config.responseSnackBarHandler(getString(R.string.admission_number) + " " + et_student_admission_number.getText() + " Already Exist",
                                findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                        et_student_admission_number.setError(getString(R.string.admission_number) + " " + et_student_admission_number.getText() + " Already Exist", getDrawable(R.drawable.ic_alert));
                    }
                }
            }
        });
        et_student_student_name = findViewById(R.id.et_student_student_name);
        et_student_father_name = findViewById(R.id.et_student_father_name);
        et_student_student_mother_name = findViewById(R.id.et_student_student_mother_name);
        et_student_country_code = findViewById(R.id.et_student_country_code);
        et_student_email = findViewById(R.id.et_student_email);
        et_student_student_contact_number = findViewById(R.id.et_student_student_contact_number);
        et_student_remark = findViewById(R.id.et_student_remark);
        tv_transport_fee_name = findViewById(R.id.tv_transport_fee_name);
        tv_transport_fee_frequency = findViewById(R.id.tv_transport_fee_frequency);

        et_transport_amount = findViewById(R.id.et_transport_amount);
        et_transport_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculateFeesSumForDisplay();
            }
        });

        btn_select_transport_distance = findViewById(R.id.btn_select_transport_distance);

        switch_compact_transport = findViewById(R.id.switch_compact_transport);

        switch_compact_transport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    (findViewById(R.id.ll_transport_fee)).setVisibility(View.VISIBLE);

                    if (Gc.getSharedPreference(Gc.TRANSPORTFEETYPE, StudentCreateActivity.this).equalsIgnoreCase(Gc.DISTANCEWISE)) {
                        (findViewById(R.id.btn_select_transport_distance)).setVisibility(View.VISIBLE);
                    } else {
                        et_transport_amount.setText(transportAmounts.get(0).Amount);
                    }

                } else {
                    (findViewById(R.id.btn_select_transport_distance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_transport_fee)).setVisibility(View.GONE);
                    btn_select_transport_distance.setText(R.string.distance);
                    et_transport_amount.getText().clear();

                }
            }
        });

//        switch_compact_create_student_send_sms = findViewById(R.id.switch_compact_create_student_send_sms);
//        if(Gc.getSharedPreference(Gc.SENDSMSENABLED,this).equalsIgnoreCase(Gc.TRUE)){
//            switch_compact_create_student_send_sms.setChecked(true);
//        }else {
//            switch_compact_create_student_send_sms.setChecked(false);
//        }
//            switch_compact_create_student_send_sms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(final CompoundButton compoundButton, boolean b) {
//                if(b){
//                    AlertDialog.Builder alertSms = new AlertDialog.Builder(StudentCreateActivity.this);
//                    alertSms.setIcon(getResources().getDrawable(R.drawable.ic_coins));
//                    alertSms.setTitle(getString(R.string.sms_cost_alert));
//                    alertSms.setMessage(getString(R.string.sms_alert));
//                    alertSms.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            checkAndroidSMSPermission();
//                            HashMap<String,String> setDefaults = new HashMap<>();
//                            setDefaults.put(Gc.SENDSMSENABLED, Gc.TRUE);
//                            Gc.setSharedPreference(setDefaults, StudentCreateActivity.this);
//                        }
//                    });
//                    alertSms.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            compoundButton.setChecked(false);
//                            HashMap<String,String> setDefaults = new HashMap<>();
//                            setDefaults.put(Gc.SENDSMSENABLED, Gc.FALSE);
//                            Gc.setSharedPreference(setDefaults, StudentCreateActivity.this);
//                        }
//                    });
//                    alertSms.show();
//                }
//            }
//        });

        tv_create_student_register_total_session = findViewById(R.id.tv_create_student_register_total_session);
        tv_create_student_register_total_monthly = findViewById(R.id.tv_create_student_register_total_monthly);
        tv_create_student_register_total_quarterly = findViewById(R.id.tv_create_student_register_total_quarterly);
    }

    private void selectClassForStudentCreation() {
        btn_select_student_class = findViewById(R.id.btn_select_student_class);
        classListBuilder = new AlertDialog.Builder(StudentCreateActivity.this);
        classSectionAdapter =
                new ArrayAdapter<String>(StudentCreateActivity.this, android.R.layout.select_dialog_singlechoice);

        classSectionAdapter.addAll(sectionList);
        btn_select_student_class.setOnClickListener(view -> {
            classListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            classListBuilder.setAdapter(classSectionAdapter, (dialogInterface, i) -> {
                btn_select_student_class.setText(sectionList.get(i));
                selectedSection = i;
                createClassFeeList(selectedSection);
            });

            AlertDialog dialog = classListBuilder.create();
            dialog.show();

        });

    }

    private void createClassFeeList(int selectedSection) {
        ll_student_admission_number_fee.removeAllViews();

        final int section = selectedSection;
        final ArrayList<ClassFeesEntity> classFeesEntities = new ArrayList<>();

        mDb.classFeesModel().getFeeforClassSectionLive(classNameSectionList.get(section).SectionId)
                .observe(this, classFees -> {
                    classFeesEntities.addAll(classFees);

                    for (int i = 0; i < classFeesEntities.size(); i++) {
                        LayoutInflater layoutInflater = LayoutInflater.from(StudentCreateActivity.this);
                        View view = layoutInflater.inflate(R.layout.item_add_manage_fee, null);

                        TextView tv_item_fee_name = view.findViewById(R.id.tv_item_fee_name);
                        tv_item_fee_name.setText(classFeesEntities.get(i).FeeType);

                        TextView tv_item_fee_frequency = view.findViewById(R.id.tv_item_fee_frequency);
                        tv_item_fee_frequency.setText(classFeesEntities.get(i).Frequency);

                        TextView tv_item_feetypeid = view.findViewById(R.id.tv_item_feetypeid);
                        tv_item_feetypeid.setText(classFeesEntities.get(i).FeeId);

                        EditText et_item_amount = view.findViewById(R.id.et_item_amount);
                        et_item_amount.setText(classFeesEntities.get(i).Amount);
                        et_item_amount.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                calculateFeesSumForDisplay();
                            }
                        });

                        ll_student_admission_number_fee.addView(view);
                    }

                    calculateFeesSumForDisplay();
                });


        mDb.classFeesTransportModel().getFeeforClassSectionLive(classNameSectionList.get(selectedSection).SectionId,
                Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, new Observer<List<ClassFeesTransportEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<ClassFeesTransportEntity> classFeesTransportEntities) {
                        transportAmounts.clear();
                        transportAmounts.addAll(classFeesTransportEntities);
                        if (transportAmounts.size() > 0) {
                            switch_compact_transport.setVisibility(View.VISIBLE);
                            distanceList.clear();
                            transportListAdapter.clear();
                            for (ClassFeesTransportEntity d : transportAmounts) {
                                distanceList.add(d.MasterEntryValue);
                            }
                            transportListAdapter.addAll(distanceList);
                            transportListAdapter.notifyDataSetChanged();

                        } else {
                            switch_compact_transport.setVisibility(View.GONE);
                        }

                        mDb.classFeesTransportModel()
                                .getFeeforClassSectionLive(classNameSectionList.get(section).SectionId, Gc.getSharedPreference(Gc.APPSESSION, StudentCreateActivity.this))
                                .removeObserver(this);
                    }
                });


    }

    void calculateFeesSumForDisplay() {
        int sumsession = 0;
        int sumquarterly = 0;
        int summonthly = 0;

        feeStructure = "";
        for (int k = 0; k < ll_student_admission_number_fee.getChildCount(); k++) {
            View view1 = ll_student_admission_number_fee.getChildAt(k);
            String enteredamount = ((EditText) view1.findViewById(R.id.et_item_amount)).getText().toString();
            if (!enteredamount.isEmpty()) {
                String feetypeid = ((TextView) view1.findViewById(R.id.tv_item_feetypeid)).getText().toString();

                feeStructure += feetypeid + "-" + enteredamount + ",";
            }

            String frequency = ((TextView) view1.findViewById(R.id.tv_item_fee_frequency)).getText().toString();

            if ("Monthly".equalsIgnoreCase(frequency)) {
                summonthly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            } else if ("session".equalsIgnoreCase(frequency)) {
                sumsession += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);
            } else if ("quarterly".equalsIgnoreCase(frequency))
                sumquarterly += Integer.parseInt("".equals(enteredamount) ? "0" : enteredamount);

        }
        if (!feeStructure.isEmpty()) {
            if (switch_compact_transport.isChecked() &&
                    !(Strings.nullToEmpty(et_transport_amount.getText().toString()).equals(""))) {
                feeStructure += transportAmounts.get(selectedDistance).FeeId + "-" + et_transport_amount.getText().toString() + ",";
            }
            String s1 = feeStructure.substring(0, feeStructure.length() - 1);
            feeStructure = s1;
        }

        tv_create_student_register_total_session.setText(sumsession + "");
        tv_create_student_register_total_monthly.setText(summonthly + "");
        tv_create_student_register_total_quarterly.setText(sumquarterly + "");
    }

    private void selectGenderForStudentCreation() {
        btn_select_student_gender = findViewById(R.id.btn_select_student_gender);

        genderListBuilder = new AlertDialog.Builder(StudentCreateActivity.this);
        genderListAdapter =
                new ArrayAdapter<>(StudentCreateActivity.this, android.R.layout.select_dialog_singlechoice);

        genderListAdapter.addAll(genderList);

        btn_select_student_gender.setOnClickListener(view -> {

            genderListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            genderListBuilder.setAdapter(genderListAdapter, (dialogInterface, i) -> {
                Log.i("Selected item is :", i + "");
                btn_select_student_gender.setText(genderList.get(i));
                selectedGender = i;
            });

            AlertDialog dialog = genderListBuilder.create();
            dialog.show();

        });

    }

    private void selectCasteDialog() {
        btn_select_caste = findViewById(R.id.btn_select_caste);
        casteListBuilder = new AlertDialog.Builder(StudentCreateActivity.this);
        casteListAdapter = new ArrayAdapter<>(StudentCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        casteListAdapter.addAll(casteList);
        btn_select_caste.setOnClickListener(view -> {
            casteListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            casteListBuilder.setAdapter(casteListAdapter, (dialogInterface, i) -> {
                btn_select_caste.setText(casteList.get(i));
                selectedCaste = 1 + i;
            });
            AlertDialog dialog = casteListBuilder.create();
            dialog.show();
        });
    }

    private void selectCategoryDialog() {
        btn_select_category = findViewById(R.id.btn_select_category);
        categoryListBuilder = new AlertDialog.Builder(StudentCreateActivity.this);
        categoryListAdapter = new ArrayAdapter<>(StudentCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        categoryListAdapter.addAll(categoryList);
        btn_select_category.setOnClickListener(view -> {
            categoryListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            categoryListBuilder.setAdapter(categoryListAdapter, (dialogInterface, i) -> {
                btn_select_category.setText(categoryList.get(i));
                selectedCategory = 1 + i;
            });
            AlertDialog dialog = categoryListBuilder.create();
            dialog.show();
        });
    }

    void selectDistanceForTransportFee() {
        transportListBuilder = new AlertDialog.Builder(StudentCreateActivity.this);
        transportListAdapter = new ArrayAdapter<String>(StudentCreateActivity.this, android.R.layout.select_dialog_singlechoice);
        transportListAdapter.addAll(distanceList);

        btn_select_transport_distance.setOnClickListener(view -> {
            transportListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            transportListBuilder.setAdapter(transportListAdapter, (dialogInterface, i) -> {
                btn_select_transport_distance.setText(distanceList.get(i));
                selectedDistance = i;
                et_transport_amount.setText(transportAmounts.get(i).Amount);
//                        calculateFeesSumForDisplay();
            });

            AlertDialog dialog = transportListBuilder.create();
            dialog.show();
        });


    }

    private void createStudent() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

//        final JSONArray param = new JSONArray();
        final JSONObject param = new JSONObject();
        param.put("StudentName", et_student_student_name.getText().toString().trim());
        param.put("FatherName", et_student_father_name.getText().toString().trim());
        param.put("MotherName", et_student_student_mother_name.getText().toString().trim());
        param.put("SectionId", classNameSectionList.get(selectedSection).SectionId);
        param.put("DOR", selectedRegistrationDate);
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("Gender", genderListMaster.get(selectedGender).MasterEntryId);
        param.put("AdmissionNo", et_student_admission_number.getText().toString().trim());
        param.put("StudentEmail", et_student_email.getText().toString().trim());
        param.put("Mobile", et_student_country_code.getText().toString().trim()
                + "-" + et_student_student_contact_number.getText().toString().trim());
        param.put("effectiveFrom", selectedFeeEffectiveDate);
        param.put("FeeStructure", feeStructure);
        param.put("Remarks", et_student_remark.getText().toString().trim());
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentRegistration";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");

                            final JSONArray studentsjarr = resultjobj.getJSONArray("studentDetails");
                            final ArrayList<SchoolStudentEntity> schoolStudentEntities = new ArrayList<>();

                            for (int i = 0; i < studentsjarr.length(); i++) {
                                SchoolStudentEntity schoolStudentEntity = new Gson()
                                        .fromJson(studentsjarr.get(i).toString(), SchoolStudentEntity.class);
                                schoolStudentEntities.add(schoolStudentEntity);
                            }
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.schoolStudentModel().insertStudents(schoolStudentEntities);
                                }
                            }).start();

//                            if (switch_compact_create_student_send_sms.isChecked())
//                                sendSMS(schoolStudentEntities.get(0).StudentsPassword);
//                                     Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.rl_student_create),R.color.fragment_first_green);
                            Intent intent = new Intent();
                            intent.putExtra("message", getString(R.string.success)
                                    + " : "
                                    + et_student_student_name.getText().toString()
                            );
                            setResult(RESULT_OK, intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StudentCreateActivity.this);

                        Intent intent1 = new Intent(StudentCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StudentCreateActivity.this);

                        Intent intent2 = new Intent(StudentCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;
                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
                }
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(StudentCreateActivity.this, error, findViewById(R.id.rl_student_create));
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            Uri resultUri = result.getUri();
            isProfileChanged = true;
            Glide.with(this)
                    .load(new File(resultUri.getPath())) // Uri of the picture
                    .into(iv_student_profile_image);
        }
    }

    void updateStudent() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
//        final JSONArray param = new JSONArray();
        final JSONObject param = new JSONObject();
        param.put("StudentName", et_student_student_name.getText().toString().trim());
        param.put("FatherName", et_student_father_name.getText().toString().trim());
        param.put("MotherName", et_student_student_mother_name.getText().toString().trim());
        param.put("DOR", selectedRegistrationDate);
        param.put("DOB", btn_select_student_date_of_birth.getText().toString().trim());
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("AdmissionNo", et_student_admission_number.getText().toString().trim());
        param.put("StudentEmail", et_student_email.getText().toString().trim());
        param.put("Mobile", et_student_country_code.getText().toString().trim()
                + "-" + et_student_student_contact_number.getText().toString().trim());

        param.put("PresentAddress", et_student_present_address.getText().toString().trim());

        if (selectedCaste > 0) {
            param.put("Caste", casteListMaster.get(selectedCaste - 1).MasterEntryId);
        }

        if (selectedCategory > 0) {
            param.put("Category", categoryListMaster.get(selectedCategory - 1).MasterEntryId);
        }

        if (isProfileChanged) {
            Bitmap image = ((BitmapDrawable) iv_student_profile_image.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("studentProfileImage", new JSONObject().put("Type", "jpg").put("StudentProfile", encodedImage));
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentRegistration/"
                + new JSONObject().put("RegistrationId", studentUpdate.RegistrationId);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");
            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        JSONObject resultjobj = job.getJSONObject("result");
                        final JSONArray studentsjarr = resultjobj.getJSONArray("studentDetails");
                        final ArrayList<SchoolStudentEntity> schoolStudentEntities = new ArrayList<>();
                        for (int i = 0; i < studentsjarr.length(); i++) {
                            SchoolStudentEntity schoolStudentEntity = new Gson()
                                    .fromJson(studentsjarr.get(i).toString(), SchoolStudentEntity.class);
                            schoolStudentEntities.add(schoolStudentEntity);
                        }
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                mDb.schoolStudentModel().insertStudents(schoolStudentEntities);
                            }
                        }).start();
                        Intent intent = new Intent();
                        intent.putExtra("message", getString(R.string.success)
                                + " : "
                                + et_student_student_name.getText().toString()
                        );
                        setResult(RESULT_OK, intent);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case Gc.APIRESPONSE401:
                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, StudentCreateActivity.this);
                    Intent intent1 = new Intent(StudentCreateActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    finish();
                    break;
                case Gc.APIRESPONSE500:
                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, StudentCreateActivity.this);

                    Intent intent2 = new Intent(StudentCreateActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();
                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.rl_student_create), R.color.fragment_first_blue);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(StudentCreateActivity.this, error, findViewById(R.id.rl_student_create));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            if (validateInput()) {
                if (studentUpdate == null) {
                    try {
                        createStudent();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        updateStudent();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        classNameSectionList.clear();
        sectionList.clear();

        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).observe(this, new Observer<List<ClassNameSectionName>>() {
            @Override
            public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                classNameSectionList.clear();
                sectionList.clear();
                classSectionAdapter.clear();
                classNameSectionList.addAll(classNameSectionNames);
                for (int i = 0; i < classNameSectionList.size(); i++) {
                    sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName);
                }
                classSectionAdapter.addAll(sectionList);
                classSectionAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this)).removeObservers(this);
    }

    void checkAndroidSMSPermission() {

        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                        != PackageManager.PERMISSION_GRANTED

        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.SEND_SMS

                }, 100);
            }
        }
    }

    void sendSMS(String password) {
        String shareBody =
                getString(R.string.link) + " - "
                        + Gc.SCHOOL_PLAY_STORE_LINK + "&referrer=" + Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                        + "\n" + getString(R.string.key) + "-" + Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                        + "\n" + getString(R.string.login_id) + "-" + et_student_admission_number.getText().toString()
                        + "\n" + getString(R.string.password) + "-" + password;

        SmsManager smsManager = SmsManager.getDefault();

        ArrayList<String> parts = smsManager.divideMessage(shareBody);
        int messageCount = parts.size();

        String mobile = Gc.makePhoneNumber(et_student_country_code.getText().toString(),
                et_student_student_contact_number.getText().toString());

        smsManager.sendMultipartTextMessage(mobile, null, parts, null, null);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
