package in.junctiontech.school.schoolnew.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.provider.Telephony;
import android.util.Base64;

import androidx.annotation.NonNull;

//import com.crashlytics.android.Crashlytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class Gc {

//    private static Context gcontext;

    public static final String NOTFOUND = "NotFound";

    public static final String APPKEY = "pOcY8y-LTEexqkROIm64WAXn1mwB1lyECcvZAZnl6hRQMZFtQeHwcUa74L_jfCRDOgrknWBWucbkOKs5g7p82OkwHhdemJMKUkGPMVqI4agzEwYt0Jt8_5c0CgQXosf82q4A5n8nPSrt30CFf30mePcDYQiBoE_x8ht0-MIAVAtrxxxg6cYEYs6nlEKznKJ_jZWokAOxuInyAqH0aXF8EHZGjV6A";
    public static final String ERPAPIVERSION = "V5/";
    public static String CPANELURL = "https://apicpanel.zeroerp.com/";
    public static final String ERPCPANELURL = "https://apierpcpanel.zeroerp.in/";
    public static final String ISSUEREPORTURL = "https://bugsapi.zeroerp.in";
    public static final String CPANELGETHOST = CPANELURL + "HostApi.php?";

    public static final String TESTBANNERID = "ca-app-pub-3940256099942544/6300978111";

    public static final String[] STRING_ARRAY = {"paid"};

    public static final String PLANTYPEFREE = "FREE";
    public static final String SIGNEDINUSERPERMISSION = "SIGNEDINUSERPERMISSION";
    public static final String SIGNEDINUSERNAME = "username";
    public static final String SIGNEDINUSERDISPLAYNAME = "SIGNEDINUSERDISPLAYNAME";

    public static final String SIGNEDINUSERPASSWORD = "password";
    public static final String STAFF = "STAFF";
    public static final String ADMIN = "ADMIN";
    public static final String STUDENTPARENT = "STUDENTPARENT";

    public static final String SIGNEDINUSERROLE = "SIGNEDINUSERROLE";
    public static final String USERTYPECODE = "UserTypeId";
    public static final String USERTYPETEXT = "userTypeValue";
    public static final String USERID = "USERID";
    public static final String STAFFID = "STAFFID";

    public static final String LICENCE = "license";
    public static final String STUDENTCOUNT = "studentCount";

    public static final String ACCESSTOKEN = "accessToken";

    public static final String CURRENTSESSION = "currentsession";
    public static final String CURRENTSESSIONSTART = "currentsessionStartDate";
    public static final String CURRENTSESSIONEND = "currentsessionEndDate";

    public static final String SCHOOLPREF = "SchoolPref";
    public static final String APPSESSION = "APPSESSION";
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";

    public static final String ERPHOSTAPIURL = "HostName";
    public static final String MEDIASTORAGEURL = "MediaStorageURL";
    public static final String IMPORTANTMEDIASTORAGEURL = "ImportantMediaStorageURL";


    public static final String ERPLICENCEEXPIRY = "licenceExpiry";
    public static final String ERPINSTCODE = "organizationKey";
    public static final String ERPDBNAME = "organization_name";
    public static final String FROMDEMO = "FROMDEMO";
    public static final String DEMO = "demo";


    public static final String PASSWORD = "PASSWORD";

    public static final String SESSIONEXISTS = "SESSIONEXISTS";
    public static final String SCHOOLDETAILEXISTS = "SCHOOLDETAILEXISTS";
    public static final String CLASSEXISTS = "CLASSEXISTS";
    public static final String FEETYPEEXISTS = "FEETYPEEXISTS";
    public static final String CLASSFEESEXISTS = "CLASSFEESEXISTS";
    public static final String SUBJECTEXISTS = "SUBJECTEXISTS";
    public static final String CALENDEREXISTS = "CALENDEREXISTS";
    public static final String ACCOUNTEXISTS = "ACCOUNTEXISTS";
    public static final String EXISTS = "EXISTS";
    public static final String DEFAULTCLASS = "DEFAULTCLASS";
    public static final String DEFAULTSECTION = "DEFAULTSECTION";
    public static final String SUBJECTCLASSEXISTS = "SUBJECTCLASSEXISTS";
    public static final String LOGO_URL = "LOGO_URL";
    public static final String LOGIN_OPTIONS = "LOGIN_OPTIONS";
    public static final String SCHOOLIMAGE = "SCHOOLIMAGE";

    public static final String UPDATE_ASSIGNMENT = "UPDATE_ASSIGNMENT";
    public static final String APPSESSIONSTART = "APPSESSIONSTART";
    public static final String APPSESSIONEND = "APPSESSIONEND";
    public static final String DEVICETYPE = "ANDROID";
    public static final String CONTENT_TYPE = "application/json";
    public static final String FCMTOKEN = "FCMTOKEN";
    public static final String NOTLOGINVALUES = "NOTLOGINVALUES";
    public static final String SMSBALANCE = "SMSBALANCE";
    public static final String ERPWEBHOSTURL = "ERPWEBHOSTURL";
    public static final String ERPINSTTYPE = "ERPINSTTYPE";
    public static final String ERPADDVERTISEMENTSTATUS = "ERPADDVERTISEMENTSTATUS";
    public static final String ERPAPPLICATIONSTATUS = "ERPAPPLICATIONSTATUS";
    public static final String SETUP = "SETUP";
    public static final String ISORGANIZATIONDELETED = "ISORGANIZATIONDELETED";
    public static final String TRUE = "TRUE";
    public static final String FALSE = "FALSE";
    public static final String SENDSMSENABLED = "SENDSMSENABLED";
    public static final String SCHOOL_PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=in.junctiontech.school";
    public static final String PROFILE_URL = "PROFILE_URL";
    public static final Object UPDATEAPP = "UPDATEAPP";
    public static final String STUDENTADMISSIONID = "STUDENTADMISSIONID";
    public static final String RELOADPERMISSIONS = "RELOADPERMISSIONS";
    public static final String SCHOOLNAME = "SCHOOLNAME";
    public static final String DISTANCE = "Distance";
    public static final String TRANSPORTFEETYPE = "TRANSPORTFEETYPE";
    public static final String DISTANCEWISE = "DistanceWise";
    public static final String FIXED = "Fixed";
    public static final String APPUPDATED = "APPUPDATED";
    public static final String APPVERSION = "APPVERSION";
    public static final String SP_PERSISTENT = "in.junctiontech.SP_PERSISTENT";
    public static final String LOGOUT_AFTER_UPDATE = "LOGOUT_AFTER_UPDATE";
    public static final String RELOADDEMO = "RELOADDEMO";
    public static final String DEMOALERT = "DEMOALERT";
    public static final String SIGNEDINSTUDENT = "SIGNEDINSTUDENT";
    public static final String SIGNEDINSTAFF = "SIGNEDINSTAFF";
    public static final String REMINDEREMAILPHONE = "REMINDEREMAILPHONE";
    public static final String CHATWINDOWACTIVE = "CHATWINDOWACTIVE";
    public static final String ONLINETESTRUNNING = "ONLINETESTRUNNING";
    public static final String GALLERYDATA = "galleryData";
    public static final String NOTAUTHORIZED = "NOTAUTHORIZED";
    public static final String GRADE = "Grade";
    public static final String ZEROERPEMAIL = "info@zeroerp.com";
    public static final String ZEROERPWEBSITE = "https://www.zeroerp.com";
    public static final String ZEROERPFACEBOOK = "https://www.facebook.com/zeroerp";
    public static final String ZEROERPYOUTUBE = "https://www.youtube.com/channel/UCTPN4fWdrvpiXh6GoCOL1-g";
    public static final String ZEROERPLINKEDIN = "https://in.linkedin.com/in/zero-erp-0b6418138";
    public static final String ZEROERPPHONE = "+91 8889997605";
    public static final String RESULT = "Result";
    public static final String ImportantStorageURL = "ImportantMediaStorageURL";
    public static final String StorageURL = "MediaStorageURL";
    public static final String AfterHomeworkSubmissionDate = "AfterHomeworkSubmissionDate";


    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public static String ERPPLANTYPE = "ERPPLANTYPE";
    public static String CPANELAPIVERSION = "V5/";
    public static String DATEINPUTFORMAT = "yyyy-MM-dd";
    public static String MONTHYEAR = "MMM-yyyy";

    public static final int ASSIGNMENT_REQUEST_CODE = 1111;
    public static final int STAFF_CREATE_REQUEST_CODE = 1112;
    public static final int STUDENT_CREATE_REQUEST_CODE = 1113;
    public static final int FEE_RECEIVE_REQUEST_CODE = 1114;
    public static final int FEE_CREATE_REQUEST_CODE = 1115;
    public static final int EVENT_CREATE_REQUEST_CODE = 1116;
    public static final int NOTICE_CREATE_REQUEST_CODE = 1117;
    public static final int PAYONLINE_FEE_RECEIVE_REQUEST_CODE = 1118;
    public static final int STUDENT_TERMINATE_REQUEST_CODE = 1119;

    public static final String APIRESPONSE500 = "500";
    public static final String APIRESPONSE200 = "200";
    public static final String APIRESPONSE204 = "204";
    public static final String APIRESPONSE400 = "400";
    public static final String APIRESPONSE401 = "401";
    public static final String APIRESPONSE403 = "403";

// modules

    public static final String ClassSection = "ClassSection";
    public static final String CreateClass = "CreateClass";
    public static final String DisplayClass = "DisplayClass";
    public static final String CLASSSECTIONEDITALLOWED = "CLASSSECTIONEDITALLOWED";
    public static final String CLASSSECTIONDISPLAYALLOWED = "CLASSSECTIONDISPLAYALLOWED";

    public static final String Fees = "Fees";
    public static final String DisplayFees = "DisplayFees";
    public static final String FeesPayment = "FeesPayment";
    public static final String FeesSetup = "FeesSetup";
    public static final String FEESETUPALLOWED = "FEESETUPALLOWED";
    public static final String FEEPAYMENTALLOWED = "FEEPAYMENTALLOWED";
    public static final String FEEDISPLAYALLOWED = "FEEDISPLAYALLOWED";
    public static final String HOMEWORKCREATEALLOWED = "HOMEWORKCREATEALLOWED";
    public static final String HOMEWORKDISPLAYALLOWED = "HOMEWORKDISPLAYALLOWED";

    public static final String Student = "Student";
    public static final String StudentSetup = "StudentSetup";
    public static final String CreateStudentAttendence = "CreateStudentAttendence";
    public static final String DisplayStudentAttendance = "DisplayStudentAttendance";
    public static final String DisplayStudent = "DisplayStudent";
    public static final String STUDENTSETUPALLOWED = "STUDENTSETUPALLOWED";


    public static final String Subject = "Subject";
    public static final String CreateSubject = "CreateSubject";
    public static final String DisplaySubject = "DisplaySubject";
    public static final String SUBJECTCREATEALLOWED = "SUBJECTCREATEALLOWED";
    public static final String SUBJECTDISPLAYALLOWED = "SUBJECTDISPLAYALLOWED";


    public static final String Finance = "Finance";
    public static final String FinanceReports = "FinanceReports";
    public static final String FinanceSetup = "FinanceSetup";
    public static final String FINANCESETUPALLOWED = "FINANCESETUPALLOWED";

    public static final String ManageStaff = "ManageStaff";
    public static final String StaffSetup = "StaffSetup";
    public static final String DisplayStaff = "DisplayStaff";
    public static final String STAFFSETUPALLOWED = "STAFFSETUPALLOWED";
    public static final String STAFFDISPLAYALLOWED = "STAFFDISPLAYALLOWED";


    public static final String DisplayStaffAttendence = "DisplayStaffAttendence";
    public static final String CreateStaffAttendence = "CreateStaffAttendence";


    public static final String Library = "Library";
    public static final String DisplayIssueReturn = "DisplayIssueReturn";

    public static final String Communicate = "Communicate";
    public static final String CommunicateSetup = "CommunicateSetup";
    public static final String Noticeboard = "Noticeboard";
    public static final String SendMessage = "SendMessage";
    public static final String SENDMESSAGEALLOWED = "SENDMESSAGEALLOWED";
    public static final String CREATENOTICEALLOWED = "CREATENOTICEALLOWED";


    public static final String BasicDetails = "BasicDetails";
    public static final String CreateBasicDetails = "CreateBasicDetails";

    public static final String Events = "Events";
    public static final String CreateEventsCalendar = "CreateEventsCalendar";
    public static final String DisplayEventsCalendar = "DisplayEventsCalendar";
    public static final String CREATEEVENTALLOWED = "CREATEEVENTALLOWED";

    public static final String Homework = "Homework";
    public static final String CreateHomework = "CreateHomework";
    public static final String DisplayHomework = "DisplayHomework";
    public static final String HomeworkReports = "HomeworkReports";

    public static final String Exam = "Exam";
    public static final String GradeSetup = "GradeSetup";
    public static final String ResultEntry = "ResultEntry";
    public static final String ResultSetup = "ResultSetup";
    public static final String ViewMarkSheet = "ViewMarkSheet";
    public static final String ExamReport = "ExamReport";

    public static final String Transport = "Transport";
    public static final String CreateTransportAttedence = "CreateTransportAttedence";
    public static final String DisplayBasicDetails = "DisplayBasicDetails";

    public static final String Gallery = "Gallery";
    public static final String DisplayGallery = "DisplayGallery";
    public static final String EditGallery = "EditGallery";
    public static final String GALLERYEDITALLOWED = "GALLERYEDITALLOWED";

    public static final String TimeTable = "TimeTable";
    public static final String DisplayTimeTable = "DisplayTimeTable";
    public static final String TimeTableSetup = "TimeTableSetup";
    public static final String TransportSetup = "TransportSetup";
    public static final String OnlineExam = "OnlineExam";

    public static final String ResetUserPassword = "ResetUserPassword";

    public static final String DisplayLocation = "DisplayLocation";

    public static final String UserManagement = "UserManagement";
    public static final String DisplayUser = "DisplayUser";

    public static final String PayOnline = "PayOnline";

    public static final String CreateStudentReview = "CreateStudentReview";
    public static final String DisplayStudentReview = "DisplayStudentReview";
    public static final String CreateStaffReview = "CreateStaffReview";
    public static final String DisplayStaffReview = "DisplayStaffReview";
    public static final String STUDENTREVIEWLOGCREATEALLOWED = "STUDENTREVIEWLOGCREATEALLOWED";
    public static final String STAFFREVIEWLOGCREATEALLOWED = "STAFFREVIEWLOGCREATEALLOWED";

    public static final String orgAdmissionProcess = "AdmissionProcess";
    public static final String orgAttendance = "Attendance";
    public static final String orgBasicDetails = "BasicDetails";
    public static final String orgClassSection = "ClassSection";
    public static final String orgCommunicate = "Communicate";
    public static final String orgEvents = "Events";
    public static final String orgExam = "Exam";
    public static final String orgFees = "Fees";
    public static final String orgFinance = "Finance";
    public static final String orgFrontOffice = "FrontOffice";
    public static final String orgGallery = "Gallery";
    public static final String orgHomework = "Homework";
    public static final String orgLibrary = "Library";
    public static final String orgManageStaff = "ManageStaff";
    public static final String orgMasterEntry = "MasterEntry";
    public static final String orgOnlinePayment = "OnlinePayment";
    public static final String orgPayroll = "Payroll";
    public static final String orgStudent = "Student";
    public static final String orgSubject = "Subject";
    public static final String orgTimeTable = "TimeTable";
    public static final String orgTransport = "Transport";
    public static final String orgUserManagement = "UserManagement";

    //Notifiation Channels

    public static final String CHANNEL_GROUPID_IMPORTANT = "COM.ZEROERP.CHANNEL_GROUP_IMPORTANT";
    public static final String CHANNEL_GROUPID_MESSAGING = "COM.ZEROERP.CHANNEL_GROUP_MESSAGING";
    public static final String CHANNEL_GROUPID_INFORMATION = "COM.ZEROERP.CHANNEL_GROUP_INFORMATION";

    public static final String CHANNELID_NOTICEBOARD = "COM.ZEROERP.CHANNEL_NOTICEBOARD";
    public static final String CHANNELID_NOTICEBOARD_TEXT = "NoticeBoard";

    public static final String CHANNELID_ATTENDANCE = "COM.ZEROERP.CHANNEL_ATTENDANCE";
    public static final String CHANNELID_ATTENDANCE_TEXT = "Attendance Information";

    public static final String CHANNELID_HOMEWORK = "COM.ZEROERP.CHANNEL_HOMEWORK";
    public static final String CHANNELID_HOMEWORK_TEXT = "Homework Information";

    public static final String CHANNELID_HOMEWORK_EVALUATION = "COM.ZEROERP.CHANNEL_HOMEWORK_EVALUATION";
    public static final String CHANNELID_HOMEWORK_EVALUATION_TEXT = "Homework Evaluation";

    public static final String CHANNELID_LIBRARY = "COM.ZEROERP.CHANNEL_LIBRARY";
    public static final String CHANNELID_LIBRARY_TEXT = "Library Information";

    public static final String CHANNELID_EXAM = "COM.ZEROERP.CHANNEL_EXAM";
    public static final String CHANNELID_EXAM_TEXT = "Exam Information";

    public static final String CHANNELID_FINE = "COM.ZEROERP.CHANNEL_FINE";
    public static final String CHANNELID_FINE_TEXT = "Penalty Information";

    public static final String CHANNELID_EVENTS = "COM.ZEROERP.CHANNEL_EVENTS";
    public static final String CHANNELID_EVENTS_TEXT = "Event Information";

    public static final String CHANNELID_ONLINEEXAM = "COM.ZEROERP.CHANNEL_ONLINEEXAM";
    public static final String CHANNELID_ONLINEEXAM_TEXT = "Online Exam Information";

    public static final String CHANNELID_GREETINGS = "COM.ZEROERP.CHANNELID_GREETINGS";
    public static final String CHANNELID_GREETINGS_TEXT = "Personal Greetings";
    public static final String BIRTHDAY_GREETINGS_TEXT = "BirthDay Greetings";

    public static final String CHANNELID_FEE_REMINDER = "COM.ZEROERP.CHANNEL_FEE_REMINDER";
    public static final String CHANNELID_FEE_REMINDER_TEXT = "Fee Reminder";

    public static final String CHANNELID_FEE_RECEIPT = "COM.ZEROERP.CHANNEL_FEE_RECEIPT";
    public static final String CHANNELID_FEE_RECEIPT_TEXT = "Fee Receipt";

    public static final String CHANNELID_GALLERY_ADD = "COM.ZEROERP.CHANNEL_GALLERY_ADD";
    public static final String CHANNELID_GALLERY_ADD_TEXT = "Gallery Image Added";

    public static final String CHANNELID_CLASS_GALLERY_ADD = "COM.ZEROERP.CHANNEL_GALLERY_ADD";
    public static final String CHANNELID_CLASS_GALLERY_ADD_TEXT = "Class Wise Image Added";

    public static final String CHANNELID_GROUP_MESSAGE = "COM.ZEROERP.CHANNELID_GROUP_MESSAGE";
    public static final String CHANNELID_GROUP_MESSAGE_TEXT = "Group Message";

    public static final String CHANNELID_121_MESSAGE = "COM.ZEROERP.CHANNELID_121_MESSAGE";
    public static final String CHANNELID_121_MESSAGE_TEXT = "Personal Messages";

    public static final String CHANNELID_SCHOOL_MESSAGE = "COM.ZEROERP.CHANNELID_SCHOOL_MESSAGE";
    public static final String CHANNELID_SCHOOL_MESSAGE_TEXT = "School Message";

    public static final String CHANNELID_OTHER_INFO = "COM.ZEROERP.CHANNELID_OTHER_INFO";
    public static final String CHANNELID_OTHER_INFO_TEXT = "School Information";

    public static final String CHANNELID_SURVEY = "COM.ZEROERP.CHANNELID_SURVEY";
    public static final String CHANNELID_SURVEY_TEXT = "Survey Information";


    public static final String[] channelListImportant = {
            CHANNELID_ATTENDANCE,
            CHANNELID_HOMEWORK,
            CHANNELID_HOMEWORK_EVALUATION,
            CHANNELID_LIBRARY,
            CHANNELID_FEE_REMINDER,
            CHANNELID_FEE_RECEIPT,
            CHANNELID_EXAM,
            CHANNELID_EVENTS,
            CHANNELID_FINE
    };

    public static final String[] channelListInformation = {
            CHANNELID_ONLINEEXAM,
            CHANNELID_GREETINGS,
            CHANNELID_GALLERY_ADD
    };

    public synchronized static String id(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, 0);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }
        return uniqueID;
    }

    public synchronized static void clearid(Context context) {
        uniqueID = null;
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, 0);
        sharedPrefs.edit().clear().apply();
    }

    public static void setSharedPreference(HashMap<String, String> sharedPreference, Context context) {

//        gcontext = context;

        SharedPreferences pref = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        for (String key : sharedPreference.keySet()) {
            editor.putString(key, sharedPreference.get(key));
        }
        editor.apply();

    }

    public static void saveArrayList(ArrayList<String> list, String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<String> getArrayList(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static String getSharedPreference(String key, Context context) {
        SharedPreferences pref = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode

        return pref.getString(key, NOTFOUND);
    }


    public static void setSharedPreferenceSet(HashMap<String, Set<String>> sharedPreference, Context context) {
        SharedPreferences pref = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        for (String key : sharedPreference.keySet()) {
            editor.putStringSet(key, sharedPreference.get(key));
        }
        editor.apply();
    }

    public static Set<String> getSharedPreferenceSet(String key, Context context) {
        SharedPreferences pref = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode

        return pref.getStringSet(key, null);
    }

    public static void removeAll(Context context) {
        SharedPreferences pref = context.getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
        pref.edit().clear().apply();
    }

    public static HashMap<String, String> splitPhoneNumber(String phone) {

        HashMap<String, String> split = new HashMap<>();

        if (phone == null) {
            split.put("ccode", "");
            split.put("phone", "");
            return split;
        }

        if (phone.length() > 2 && phone.contains("-")) {
            phone = phone.replaceAll("\\s+", "");
//            phone.matches("[0-9]+") &&
//            String countrycode = String.valueOf((Integer.parseInt(phone.substring(0, phone.indexOf("-")))));
            String countrycode = phone.substring(0, phone.indexOf("-"));
            String phonenumber;

            String ph = phone.substring(phone.indexOf("-") + 1, phone.length());
            if (ph.length() == 0) {
                phonenumber = "";
            } else {
                Long phi = Long.parseLong(ph);
                phonenumber = String.valueOf(phi);
            }
            split.put("ccode", countrycode);
            split.put("phone", phonenumber);
            return split;
        } else {
            split.put("ccode", "");
            split.put("phone", "");
            return split;
        }
    }

    public static String makePhoneNumber(String ccode, String phone) {
        return ccode + "-" + phone;
    }

    public static String[] findMonthsInSession(String sessionstartdate) {
        String startdate = sessionstartdate;

        DateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat outputFormater = new SimpleDateFormat("MMM-yyyy");

        Calendar beginCalendar = Calendar.getInstance();
        Calendar finishCalendar = Calendar.getInstance();

        String[] monthList = new String[12];

        try {
            beginCalendar.setTime(formater.parse(startdate));

            finishCalendar.setTime(formater.parse(startdate));
//            finishCalendar.add(Calendar.MONTH, -1);
            finishCalendar.add(Calendar.YEAR, 1);

            String begin_month_year = outputFormater.format(beginCalendar.getTime());
            String end_month_year = outputFormater.format(finishCalendar.getTime());

            int i = 0;
            do {
                monthList[i] = begin_month_year;
                i++;
                beginCalendar.add(Calendar.MONTH, 1);
                begin_month_year = outputFormater.format(beginCalendar.getTime());
            } while (!(begin_month_year.equalsIgnoreCase(end_month_year)));


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return monthList;
    }

    public static String convertDate(String yyyy_mm_dd) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputDateStr = yyyy_mm_dd;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDate = outputFormat.format(date);
        return outputDate;
    }

    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.getDefault());
        return sdf.format(new Date());
    }

    public static String getMonthForInt(int month) {
        switch (month) {
            case 0:
                return "01";
            case 1:
                return "02";
            case 2:
                return "03";
            case 3:
                return "04";
            case 4:
                return "05";
            case 5:
                return "06";
            case 6:
                return "07";
            case 7:
                return "08";
            case 8:
                return "09";
            case 9:
                return "10";
            case 10:
                return "11";
            case 11:
                return "12";
        }
        return null;
    }

    public static void logCrash(Context context, String url) {

        FirebaseCrashlytics.getInstance().setCustomKey(Gc.APIRESPONSE401, url);
        ;
        FirebaseCrashlytics.getInstance().setCustomKey("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, context));
        FirebaseCrashlytics.getInstance().setCustomKey("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, context));
        FirebaseCrashlytics.getInstance().setCustomKey("DEVICEID", Gc.id(context));
        FirebaseCrashlytics.getInstance().setCustomKey("USERID", Gc.getSharedPreference(Gc.USERID, context));
        FirebaseCrashlytics.getInstance().setCustomKey("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, context));
        FirebaseCrashlytics.getInstance().setCustomKey("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, context));
        FirebaseCrashlytics.getInstance().setCustomKey("APPKEY", Gc.APPKEY);
        FirebaseCrashlytics.getInstance().setCustomKey("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, context));

//     Crashlytics.logException(new Exception("401" + url));
    }

    public static String compressAndGetStringImage(Bitmap bmp, int compression) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bmp.compress(Bitmap.CompressFormat.JPEG, compression, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }

    public static int findCompressRatio(long imageSize) {
        int compression = 0;
        int mb = 1000000;

        if (imageSize > 1 && imageSize <= mb) {
            compression = 90;

        } else if (imageSize > mb && imageSize <= 2 * mb) {
            compression = 80;
        } else if (imageSize > 2 * mb && imageSize <= 3 * mb) {
            compression = 70;

        } else if (imageSize > 3 * mb && imageSize <= 4 * mb) {
            compression = 60;

        } else if (imageSize > 4 * mb && imageSize <= 5 * mb) {
            compression = 50;
        } else {
            return 0;
        }
        return compression;
    }

    public static String getDefaultSmsAppPackageName(@NonNull Context context) {
        String defaultSmsPackageName;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context);
        return defaultSmsPackageName;
//        }
//        else {
//            Intent intent = new Intent(Intent.ACTION_VIEW)
//                    .addCategory(Intent.CATEGORY_DEFAULT).setType("vnd.android-dir/mms-sms");
//            final List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
//            if (resolveInfos != null && !resolveInfos.isEmpty())
//                return resolveInfos.get(0).activityInfo.packageName;
//
//        }
//        return null;
    }

    public static String getDate(long milliSeconds) {
        Calendar calendarOld = Calendar.getInstance();
        calendarOld.setTimeInMillis(milliSeconds);

        Calendar calendarLatestDate = Calendar.getInstance();
        calendarLatestDate.setTimeInMillis(new Date().getTime());

        long diff = calendarLatestDate.getTimeInMillis() - calendarOld.getTimeInMillis();
        long days = diff / (24 * 60 * 60 * 1000);

        DateFormat writeDateFormat = new SimpleDateFormat("dd MM yyyy ", Locale.US);
        if (writeDateFormat.format(calendarLatestDate.getTime()).equals(writeDateFormat.format(calendarOld.getTime()))) {
            return "Today";
        } else {
            Calendar calYestarday = Calendar.getInstance();
            calYestarday.add(Calendar.DATE, -1);
            if (writeDateFormat.format(calYestarday.getTime()).equals(writeDateFormat.format(calendarOld.getTime()))) {
                return "Yesterday";
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                return formatter.format(calendarOld.getTime());
            }
        }
    }

    @NotNull
    public static Long getMiliseconds(@Nullable String enteredDate, Boolean changeDateFormate) {
        if (changeDateFormate) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                Date date = sdf.parse(enteredDate);
                long millis = date.getTime();
                return millis;
            } catch (Exception w) {
                w.printStackTrace();
                return null;
            }
        } else {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US);
                assert enteredDate != null;
                Date date = sdf.parse(enteredDate);
                long millis = date.getTime();
                return millis;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

