package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import in.junctiontech.school.schoolnew.model.StudentInformation;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SignedInStudentInformationDao {
    @Query("select * from StudentInformation limit 1")
    LiveData<StudentInformation> getSignedInStudentInformation();

    @Insert(onConflict = REPLACE)
    void insertSignedInStudentInformation(StudentInformation studentInformation);
}
