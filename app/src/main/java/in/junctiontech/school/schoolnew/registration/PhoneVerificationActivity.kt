package `in`.junctiontech.school.schoolnew.registration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.OrganizationKeyEnter
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.tasks.TaskExecutors
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

class PhoneVerificationActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    private var code: String? = null
    private var countryName: String? = null
    private var stateName: String? = null
    private var cityName: String? = null
    lateinit var adapter: PhoneVerificationActivityAdapter
    lateinit var mRecyclerView: RecyclerView
    var mVerificationId: String? = null
    var oTP: String? = null
    var idToken: String? = null

    private val searchOrganizationList = ArrayList<SearchOrganization>()
    private val organizationList = ArrayList<SearchOrganization>()

    private lateinit var spinnerRegistrationUserRole: Spinner

    private lateinit var etPhoneNumber: EditText
    private lateinit var etOtp: EditText
    private lateinit var etCountryCode: AutoCompleteTextView
    private lateinit var btnVerifyPhoneNumber: Button
    private lateinit var btnVerifyPhoneOtp: Button
    private lateinit var llInstituteList: LinearLayout
    private lateinit var rvInstituteList: RecyclerView
    private lateinit var llOtp: LinearLayout
    private lateinit var llVerifyOtp: LinearLayout
    private lateinit var llVerifyPhoneBtn: LinearLayout
    private var phoneAuthProvider: PhoneAuthProvider? = null
    private var alert: AlertDialog.Builder? = null
    private var checkUserRole = false
    private lateinit var rl: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_verification_phone)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        code = intent.getStringExtra("code")
        countryName = intent.getStringExtra("countryName")
        stateName = intent.getStringExtra("stateName")
        cityName = intent.getStringExtra("cityName")

        phoneAuthProvider = PhoneAuthProvider.getInstance()
        initializeViews()

        if (code!!.isNotEmpty() && countryName!!.isNotEmpty()) {
            etCountryCode.setText(code)
            etCountryCode.isEnabled = false
        }
    }

    private fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)

        alert = AlertDialog.Builder(this)
        alert!!.setPositiveButton(R.string.ok) { _, _ -> }
        alert!!.setCancelable(false)
        spinnerRegistrationUserRole = findViewById(R.id.spinner_registration_user_role)

        spinnerRegistrationUserRole.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val userRole = spinnerRegistrationUserRole.selectedItem.toString()
                if ("Parent / Student" == userRole) {
                    checkUserRole = true
                    alert!!.setMessage("Registration can Only be done by Institute Owner or staff. Contact Institute to get Login details or Email us at info@zeroerp.com")
                    alert!!.show()
                } else {
                    checkUserRole = false
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        }

        rl = findViewById(R.id.top_layout)
        etPhoneNumber = findViewById(R.id.et_phone_number)
        etCountryCode = findViewById(R.id.et_country_code)
        btnVerifyPhoneNumber = findViewById(R.id.btn_verify_phone_number)
        etOtp = findViewById(R.id.et_otp)
        btnVerifyPhoneOtp = findViewById(R.id.btn_verify_phone_otp)
        llInstituteList = findViewById(R.id.ll_institute_list)
        rvInstituteList = findViewById(R.id.rv_institute_lists)
        llOtp = findViewById(R.id.ll_otp)
        llVerifyOtp = findViewById(R.id.ll_verify_otp)
        llVerifyPhoneBtn = findViewById(R.id.ll_verify_phone_btn)

        mRecyclerView = findViewById(R.id.rv_institute_lists)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = PhoneVerificationActivityAdapter()
        mRecyclerView.adapter = adapter
    }

    fun sendVerificationOTP(v: View) {
        Config.hideKeyboard(this)
        val b1: Boolean = isEmptyMobile
        if (b1) {
            etCountryCode.error = getString(R.string.country_code_cannot_be_blank)
            etPhoneNumber.error = getString(R.string.mobile_number_cannot_be_blank)
        } else {
            if (Pattern.matches("^[1-9][0-9]*$", etPhoneNumber.text.toString())) {
                val phoneNumber= "+" + etCountryCode.text + etPhoneNumber.text
                val options = PhoneAuthOptions.newBuilder(FirebaseAuth.getInstance())
                        .setPhoneNumber(phoneNumber)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                        .build()
                PhoneAuthProvider.verifyPhoneNumber(options)

                /*phoneAuthProvider!!.verifyPhoneNumber(
                        "+" + etCountryCode.text + etPhoneNumber.text,
                        120,
                        TimeUnit.SECONDS,
                        TaskExecutors.MAIN_THREAD,
                        mCallbacks)*/
                progressBar!!.visibility = View.VISIBLE

            } else {
                etPhoneNumber.error = "Mobile Number Cannot Begin With 0"
            }
        }
    }

    private val mCallbacks: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) { //Getting the code sent by SMS
                oTP = phoneAuthCredential.smsCode
                if (oTP != null) {
                    etOtp.setText(oTP)
                    verify()
                }
        }

        override fun onVerificationFailed(e: FirebaseException) {
            Toast.makeText(this@PhoneVerificationActivity, e.message, Toast.LENGTH_LONG).show()
            progressBar!!.visibility = View.GONE
        }

        override fun onCodeSent(verificationId: String, token: ForceResendingToken) {
            progressBar!!.visibility = View.GONE
            mVerificationId = verificationId
            llOtp.visibility = View.VISIBLE
            llVerifyOtp.visibility = View.VISIBLE
            llVerifyPhoneBtn.visibility = View.GONE
        }
    }

    fun verifyOtp(v: View) {
        Config.hideKeyboard(this)
        verify()
    }

    private fun verify() {
        val b1 = isEmptyMobile
        val b2 = isEmptyOtp
        etPhoneNumber.error = null
        etCountryCode.error = null
        etOtp.error = null
        if (b1 && b2) {
            Snackbar.make(rl, R.string.one_or_more_field_are_blank, Snackbar.LENGTH_LONG).setAction(R.string.dismiss) { }.show()
        } else {
            if (b2 || b1) {
                if (b1) {
                    etCountryCode.error = getString(R.string.country_code_cannot_be_blank)
                    etPhoneNumber.error = getString(R.string.mobile_number_cannot_be_blank)
                }
                if (b2) {
                    etOtp.error = getString(R.string.otp_cannot_be_blank)
                }
            } else {
                try {
                    if (checkUserRole) {
                        alert!!.setMessage("Registration can Only be done by Institue Owner or staff. Contact Institue to get Login details or Email us at info@zeroerp.com")
                        alert!!.show()
                    } else {
                        progressBar!!.visibility = View.VISIBLE
                        verifyVerificationCode()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }

    }

    @Throws(JSONException::class)
    private fun verifyVerificationCode() {
        val param = JSONObject()
        param.put("mobile", etCountryCode.text.toString() + "-" + etPhoneNumber.text.toString().replace("#".toRegex(), ""))
        param.put("otp", etOtp.text.toString())
        param.put("token", mVerificationId)

        val verifyUrl: String = (Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "verification/verifyPhoneNumber")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, verifyUrl, param,
                Response.Listener { job: JSONObject ->
                    progressBar!!.visibility = View.GONE
                    when (job.optString("code")) {
                        Gc.APIRESPONSE200 -> {
                            try {
                                if (!job.getJSONObject("result").optString("organizationDetails").isNullOrEmpty()) {
                                    val searchOrganizationDetails = Gson()
                                            .fromJson<ArrayList<SearchOrganization>>(job.getJSONObject("result").optString("organizationDetails"),
                                                    object : TypeToken<List<SearchOrganization>>() {
                                                    }.type)

                                    searchOrganizationList.clear()
                                    organizationList.clear()

                                    organizationList.addAll(searchOrganizationDetails)
                                    searchOrganizationList.addAll(searchOrganizationDetails)
                                    adapter.notifyDataSetChanged()

                                    llOtp.visibility = View.GONE
                                    llVerifyOtp.visibility = View.GONE
                                    llVerifyPhoneBtn.visibility = View.VISIBLE
                                    llInstituteList.visibility = View.VISIBLE

                                } else {

                                    llInstituteList.visibility = View.GONE
                                    idToken = job.getJSONObject("result").getJSONObject("verify").optString("idToken")
                                    val intent = Intent(this@PhoneVerificationActivity, InstituteRegistrationActivity::class.java)
                                    intent.putExtra("code", code)
                                    intent.putExtra("countryName", countryName)
                                    intent.putExtra("stateName", stateName)
                                    intent.putExtra("cityName", cityName)
                                    intent.putExtra("idToken", idToken)
                                    intent.putExtra("mobile", etCountryCode.text.toString() + "-" + etPhoneNumber.text.toString().replace("#".toRegex(), ""))
                                    intent.putExtra("userRole", spinnerRegistrationUserRole.selectedItem.toString())
                                    startActivity(intent)

                                }

                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                        else -> {
                            llInstituteList.visibility = View.GONE
                            llOtp.visibility = View.GONE
                            llVerifyOtp.visibility = View.GONE
                            llVerifyPhoneBtn.visibility = View.VISIBLE
                        }
                    }
                },
                Response.ErrorListener { volleyError: VolleyError? ->
                    progressBar!!.visibility = View.GONE
                    Config.responseVolleyErrorHandler(this, volleyError, rl)
                }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private val isEmptyMobile: Boolean
        get() = (etPhoneNumber.text == null || etPhoneNumber.text.isNullOrBlank() || etPhoneNumber.text.toString() == "" || etPhoneNumber.text.toString().isEmpty()
                || etCountryCode.text == null || etCountryCode.text.isNullOrEmpty() || etCountryCode.text.toString() == "" || etCountryCode.text.toString().isEmpty())


    private val isEmptyOtp: Boolean
        get() = etOtp.text == null || etOtp.text.isNullOrEmpty() || etOtp.text.toString() == "" || etOtp.text.toString().isEmpty()

    inner class PhoneVerificationActivityAdapter : RecyclerView.Adapter<PhoneVerificationActivityAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_for_search_institute, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvItemInstituteName.text = searchOrganizationList[position].organization_name
            if ("" != searchOrganizationList[position].logo) Glide.with(this@PhoneVerificationActivity)
                    .load(searchOrganizationList[position].logo)
                    .apply(RequestOptions()
                            .error(R.drawable.ic_junction)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(holder.tvItemInstituteLogo) else holder.tvItemInstituteLogo.setImageDrawable(getDrawable(R.drawable.ic_junction))

        }

        override fun getItemCount(): Int {
            return searchOrganizationList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tvItemInstituteName: TextView = itemView.findViewById(R.id.tv_item_institute_name)
            var tvItemInstituteLogo: CircleImageView = itemView.findViewById(R.id.tv_item_institute_logo)

            init {
                tvItemInstituteLogo.visibility = View.VISIBLE
                itemView.setOnClickListener {
                    val position = adapterPosition
                    val intent = Intent(this@PhoneVerificationActivity, OrganizationKeyEnter::class.java)
                            .putExtra("organizationKeyAvailable", true)
                            .putExtra("organizationKey", searchOrganizationList[position].organizationKey.toString())

                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}