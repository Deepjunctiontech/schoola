package `in`.junctiontech.school.schoolnew.elibrary

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.assignments.AssignmentUpdateActivity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ElibraryList : AppCompatActivity() {
    // private val progressbar: ProgressBar? = null
    private var madapter: ElibraryAdapter? = null
    var eLibraryList: ArrayList<Elibrary> = ArrayList()
    private var rl_elibrary: RecyclerView? = null

    private var colorIs: Int = 0
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elibrary_list)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        rl_elibrary = findViewById(R.id.rl_elibrary)

        madapter = ElibraryAdapter()
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        rl_elibrary!!.layoutManager = mLayoutManager
        val itemAnimator: ItemAnimator = DefaultItemAnimator()
        itemAnimator.addDuration = 1000
        itemAnimator.removeDuration = 1000
        rl_elibrary!!.itemAnimator = itemAnimator
        rl_elibrary!!.adapter = madapter

        getElibraryList()
        setColorApp()
    }

    private fun getElibraryList() {
        val url: String = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "library/eLibrary")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            when (code) {
                Gc.APIRESPONSE200 -> {
                    try {
                        eLibraryList.clear()

                        val libraryData = Gson()
                                .fromJson<ArrayList<Elibrary>>(job.getJSONObject("result").optString("eLibrary"),
                                        object : TypeToken<List<Elibrary>>() {}.type)

                        eLibraryList.addAll(libraryData)
                        madapter!!.notifyDataSetChanged()
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.rl_elibarys), R.color.fragment_first_green)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
                Gc.APIRESPONSE401 -> {
                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this)
                    val intent1 = Intent(this, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)
                    finish()
                }
                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this)
                    val intent2 = Intent(this, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)
                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_elibarys), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            //progressbar.dismiss()
            Config.responseVolleyErrorHandler(this, error, findViewById(R.id.student_list_for_selection_activity))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class ElibraryAdapter : RecyclerView.Adapter<ElibraryAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_for_elibrary, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val obj: Elibrary = eLibraryList[position]
            holder.tv_item_title.text = obj.title
        }

        fun animate(viewHolder: RecyclerView.ViewHolder) {
            val animAnticipateOvershoot = AnimationUtils.loadAnimation(this@ElibraryList,
                    R.anim.animator_for_bounce)
            viewHolder.itemView.animation = animAnticipateOvershoot
        }

        override fun getItemCount(): Int {
            return eLibraryList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tv_item_title: TextView

            init {
                tv_item_title = itemView.findViewById(R.id.tv_item_title)

                itemView.setOnClickListener {
                    if (eLibraryList[layoutPosition].fileType == "Image") {
                        val eLibraryImageURLs = ArrayList<String>()
                        var i = 0
                        var viewPosition = 0
                        for (g in eLibraryList) {
                            if (!(g.filePath.isNullOrEmpty()) && "Image" == g.fileType) {
                                eLibraryImageURLs.add(g.filePath!!)
                                if (g.filePath == eLibraryList[layoutPosition].filePath) {
                                    viewPosition = i
                                }
                                i++
                            }
                        }
                        val intent = Intent(this@ElibraryList, FullScreenImageActivity::class.java)
                        intent.putStringArrayListExtra("images", eLibraryImageURLs)
                        intent.putExtra("index", viewPosition)
                        intent.putExtra("title", String.format("%s : %s", "Homework", "Image"))
                        startActivity(intent)
                    }
                    else if(eLibraryList[layoutPosition].fileType == "Video"){
                        val intent =  Intent(Intent.ACTION_VIEW, Uri.parse(eLibraryList[layoutPosition].filePath));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setPackage("com.google.android.youtube");
                        startActivity(intent)
                    }
                    else {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + eLibraryList[layoutPosition].filePath), "text/html")
                        startActivity(intent)
                    }
                }

            }
        }
    }
}

class Elibrary {
    var eLibraryId: String? = null
    var title: String? = null
    var filePath: String? = null
    var fileType: String? = null
}
