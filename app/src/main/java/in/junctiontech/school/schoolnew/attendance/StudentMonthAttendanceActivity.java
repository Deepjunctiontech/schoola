package in.junctiontech.school.schoolnew.attendance;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.HolidayEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.AttendanceStatus;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class StudentMonthAttendanceActivity extends AppCompatActivity {

    private Button btn_select_month;
    ProgressBar progressBar;

    TextView tv_days_present, tv_days_absent, tv_days_leave, tv_days_holiday;

    SchoolStudentEntity student;

    RecyclerView mRecyclerView;
    CalendarAdapter adapter;

    private int colorIs;

    ArrayList<String> dayList = new ArrayList<>();
    HashMap<String, String> monthAtt = new HashMap<>();

    String yearMonth;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_month_attendance);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progressBar);

        tv_days_present = findViewById(R.id.tv_days_present);
        tv_days_absent = findViewById(R.id.tv_days_absent);
//        tv_days_leave = findViewById(R.id.tv_days_leave);
        tv_days_holiday = findViewById(R.id.tv_days_holiday);
        btn_select_month = findViewById(R.id.btn_select_month);

        mRecyclerView = findViewById(R.id.rv_attendence_report);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 7);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new CalendarAdapter();
        mRecyclerView.setAdapter(adapter);

        String st = getIntent().getStringExtra("student");
        if (!("".equalsIgnoreCase(Strings.nullToEmpty(st)))) {
            student = new Gson().fromJson(st, SchoolStudentEntity.class);
        } else {
            if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
                String s = Gc.getSharedPreference(Gc.SIGNEDINSTUDENT, this);
                StudentInformation studentInformation = new Gson().fromJson(s, StudentInformation.class);
                student = studentInformation.getStudent();
            } else {

                // This activity should not be entered if student is null
                finish();
                return;
            }
        }

        String date = new SimpleDateFormat("MM-yyyy", Locale.US).format(new Date());
        yearMonth = new SimpleDateFormat("yyyy-MM", Locale.US).format(new Date());
        String firstdate = "01-" + date;

        try {
            Date firstdayDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate);

            Calendar c = Calendar.getInstance();
            c.setTime(firstdayDate); // yourdate is an object of type Date

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            int maxDays = c.getActualMaximum(Calendar.DATE);

            buildCalenderForMonth(dayOfWeek, maxDays);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        btn_select_month.setText(date);
        try {
            if (student == null){
                finish();
            }else {
                fetchHolidayAndDisplay();
                fetchAttendenceAndDisplay();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        selectAttendanceDate();

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2920854047",this,adContainer);
        }
    }

    void buildCalenderForMonth(int dayOfWeek, int maxDays) {
        dayList.clear();
        dayList.add("Sun");
        dayList.add("Mon");
        dayList.add("Tue");
        dayList.add("Wed");
        dayList.add("Thu");
        dayList.add("Fri");
        dayList.add("Sat");
        switch (dayOfWeek) {
            case 1:
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;

            case 2:
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;
            case 3:
                dayList.add("");
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;

            case 4:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;
            case 5:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;
            case 6:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;
            case 7:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i = 1; i <= maxDays; i++) {
                    dayList.add(Integer.toString(i));
                }
                break;
        }
        adapter.notifyDataSetChanged();

    }

    void selectAttendanceDate() {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "MM-yyyy"; //In which you need put here
//                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                yearMonth = new SimpleDateFormat("yyyy-MM", Locale.US).format(myCalendar.getTime());

                String date = sdf.format(myCalendar.getTime());

                String firstdate = "01-" + date;

                try {
                    Date firstdayDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate);

                    Calendar c = Calendar.getInstance();
                    c.setTime(firstdayDate); // yourdate is an object of type Date

                    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                    int maxDays = c.getActualMaximum(Calendar.DATE);

                    buildCalenderForMonth(dayOfWeek, maxDays);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                btn_select_month.setText(sdf.format(new Date()));
                btn_select_month.setText(date);

                try {
                    monthAtt.clear();
                    if (student == null){
                        finish();
                    }else {
                        fetchHolidayAndDisplay();
                        fetchAttendenceAndDisplay();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };

        btn_select_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(StudentMonthAttendanceActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    void fetchAttendenceAndDisplay() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();


        p.put("SectionID", student.SectionId);
        p.put("MonthYear", btn_select_month.getText().toString());
        p.put("AdmissionID", student.AdmissionId); //

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "attendance/studentAttendanceEntry/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            JSONArray attendancejarr = job.getJSONObject("result").getJSONArray("studentAttendance");
                            if (attendancejarr.length() > 0) {

                                ArrayList<AttendanceStatus> attendanceMonthly = new Gson()
                                        .fromJson(attendancejarr.getJSONObject(0).getJSONArray("Attendance").toString(), new TypeToken<List<AttendanceStatus>>() {
                                        }.getType());


                                for (AttendanceStatus a : attendanceMonthly) {
                                    if (!(a.AttStatus.equalsIgnoreCase("N")))
                                        monthAtt.put(a.Date, a.AttStatus);
                                }
                                if (monthAtt.size() > 0) {
                                    adapter.notifyDataSetChanged();
                                    fillTotalAttendance();

                                    Config.responseSnackBarHandler(job.optString("message"),
                                            findViewById(R.id.top_layout), R.color.fragment_first_green);
                                } else {
                                    Config.responseSnackBarHandler(getString(R.string.attendance_not_available),
                                            findViewById(R.id.top_layout), R.color.fragment_first_green);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StudentMonthAttendanceActivity.this);

                        Intent intent1 = new Intent(StudentMonthAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StudentMonthAttendanceActivity.this);

                        Intent intent2 = new Intent(StudentMonthAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(StudentMonthAttendanceActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void fetchHolidayAndDisplay() throws JSONException {

        if (student == null) {
            return;
        } else {
            if (student.SectionId == null) {
                return;
            }
        }

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();
        p.put("section", student.SectionId);
        p.put("monthYear", btn_select_month.getText().toString());

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "holiday/holidays/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

//                while( monthAtt.values().remove("C") );

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<HolidayEntity> holidays = new Gson()
                                .fromJson(job.getJSONObject("result").optString("holidays"),
                                        new TypeToken<List<HolidayEntity>>() {
                                        }.getType());

                        for (HolidayEntity h : holidays) {
                            if (!(monthAtt.containsKey(h.Date)))
                                monthAtt.put(h.Date, "C");
                        }

                        adapter.notifyDataSetChanged();
                        fillTotalAttendance();


                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, StudentMonthAttendanceActivity.this);

                    Intent intent1 = new Intent(StudentMonthAttendanceActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, StudentMonthAttendanceActivity.this);

                    Intent intent2 = new Intent(StudentMonthAttendanceActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    tv_days_holiday.setText(String.valueOf(0));

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout), R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(StudentMonthAttendanceActivity.this, error, findViewById(R.id.top_layout));

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    void fillTotalAttendance() {
        int present = 0, absent = 0, leave = 0, holidays = 0;

        for (Map.Entry<String, String> a : monthAtt.entrySet()) {
            switch (a.getValue().toUpperCase()) {
                case "P":
                    ++present;
                    break;
                case "A":
                    ++absent;
                    break;
                case "C":
                    ++holidays;
                    break;
            }
        }
        tv_days_present.setText(present + "");
        tv_days_absent.setText(absent + "");
        tv_days_holiday.setText(holidays + "");

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {

        @NonNull
        @Override
        public CalendarAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calender_date, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CalendarAdapter.MyViewHolder holder, int position) {
            String dateName = dayList.get(position);
            if (position < 7) {
                holder.tv_cal_date.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                holder.tv_cal_date.setText(dateName);
            } else {
                if (dayList.get(position).equalsIgnoreCase("")) {
                    holder.tv_cal_date.setBackground(null);
                    holder.tv_cal_date.setText(null);
                } else {
                    holder.tv_cal_date.setText(dateName);
                    int dateInt = Integer.parseInt(dateName);
                    if (dateInt > 0 && dateInt < 10) {
                        String key = yearMonth + "-0" + dateInt;
                        if (monthAtt.containsKey(key)) {
                            if (monthAtt.get(key).equalsIgnoreCase("P"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("A"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.red_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("C"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.yellow_circular_layout));
                            else
                                holder.tv_cal_date.setBackground(null);
                        } else {
                            holder.tv_cal_date.setBackground(null);
                        }
                    } else {
                        String key = yearMonth + "-" + dateInt;
                        if (monthAtt.containsKey(key)) {
                            if (monthAtt.get(key).equalsIgnoreCase("P"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("A"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.red_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("C"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.yellow_circular_layout));
                            else
                                holder.tv_cal_date.setBackground(null);
                        } else {
                            holder.tv_cal_date.setBackground(null);
                        }
                    }

                }
            }
        }

        @Override
        public int getItemCount() {
            return dayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_cal_date;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_cal_date = itemView.findViewById(R.id.tv_cal_date);
            }
        }
    }
}
