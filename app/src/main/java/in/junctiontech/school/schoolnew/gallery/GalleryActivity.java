package in.junctiontech.school.schoolnew.gallery;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class GalleryActivity extends AppCompatActivity implements Communi, View.OnClickListener {
    private RecyclerView recycler_view_list_of_data;
    private SwipeRefreshLayout swipe_refresh_list;
    private SharedPreferences sp;
    private int appColor;
    private ArrayList<GalleryData> galleryListData = new ArrayList<>();
    private GalleryAdapter adapter;
    private String dbName;
    private ProgressDialog progressbar;
    private View snackbar;
    private ImageView iv_student_of_the_year;
    private FloatingActionButton fab;
    private FloatingActionButton fbtn_add_gallery, fab_add_sotm;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private boolean isFabOpen;
    private BroadcastReceiver  mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_all_list);
        sp = Prefs.with(this).getSharedPreferences();
        dbName = Gc.getSharedPreference(Gc.ERPDBNAME,this);
        appColor = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(appColor));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        snackbar = findViewById(R.id.fl_fragment_all_list);
        recycler_view_list_of_data = (RecyclerView) findViewById(R.id.recycler_view_list_of_data);
        swipe_refresh_list = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.heading, R.color.back);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                closeSwipe();
            }
        });


            fetchImagesUrl();
            setupRecycler();

            progressbar = new ProgressDialog(this);
            progressbar.setCancelable(false);
            progressbar.setMessage(getString(R.string.uploading));

            fab =  findViewById(R.id.fbtn_all_list);

            if (Gc.getSharedPreference(Gc.GALLERYEDITALLOWED, this).equalsIgnoreCase(Gc.TRUE)){
                fab.setVisibility(View.VISIBLE);
            }else {
                fab.setVisibility(View.GONE);
            }
            fbtn_add_gallery =  findViewById(R.id.fbtn_all_list_add_gallery_image);

            fab_add_sotm =  findViewById(R.id.fbtn_all_list_add_student_of_the_month);
            fab.setBackgroundTintList(ColorStateList.valueOf(appColor));
            fbtn_add_gallery.setBackgroundTintList(ColorStateList.valueOf(appColor));
            fab_add_sotm.setBackgroundTintList(ColorStateList.valueOf(appColor));

            fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
            fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
            rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
            rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
            fab.setOnClickListener(this);
            fbtn_add_gallery.setOnClickListener(this);
            fab_add_sotm.setOnClickListener(this);


//            setStom();


            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    if (intent.getAction().equalsIgnoreCase(Config.STUDENT_OF_THE_MONTH_MESSAGE)) {
                        setStom();
                        fetchImagesUrl();
                    }
                }
            };


    }

    private void setStom() {

        if (!sp.getString(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, "").equalsIgnoreCase("")) {
            View rl_student_of_the_year = findViewById(R.id.rl_student_of_the_year);
            rl_student_of_the_year.setVisibility(View.VISIBLE);
            iv_student_of_the_year = findViewById(R.id.iv_student_of_the_year);
            setBitmapfromUrl(sp.getString(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, ""));
            TextView tv_student_of_the_year =  findViewById(R.id.tv_student_of_the_year);
            tv_student_of_the_year.setText(sp.getString(Config.STUDENT_OF_THE_MONTH_MESSAGE, ""));
            tv_student_of_the_year.setTextColor(appColor);
            rl_student_of_the_year.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(GalleryActivity.this, FullInfoStudentOfMonthActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });
        }
    }

    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fbtn_add_gallery.startAnimation(fab_close);
            fab_add_sotm.startAnimation(fab_close);
            fbtn_add_gallery.setClickable(false);
            fab_add_sotm.setClickable(false);
            isFabOpen = false;
            Log.d("Ritu", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fbtn_add_gallery.startAnimation(fab_open);

            fab_add_sotm.startAnimation(fab_open);
            fbtn_add_gallery.setClickable(true);

            fab_add_sotm.setClickable(true);
            isFabOpen = true;
            Log.d("Ritu", "open");

        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fbtn_all_list:
                animateFAB();
                break;
            case R.id.fbtn_all_list_add_gallery_image:
                fab.performClick();
//                if (sp.getBoolean("addGalleryPermission", false)) {
//                    if (takePermission())
                        addImageToGallery();

//                } else
//                    Toast.makeText(this, "Permission not Available !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fbtn_all_list_add_student_of_the_month:
                fab.performClick();
//                if (sp.getBoolean("addGalleryPermission", false)) {
//
//                    if (takePermission())
                        addSOTMImageToGallery();
//                } else
//                    Toast.makeText(this, "Permission not Available !", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    private void addSOTMImageToGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 555);
    }

    public void setBitmapfromUrl(String imageUrl) {

        File targetImage = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME +
                "/" + Config.SCHOOL_IMAGE_FOLDER_NAME, sp.getString(Config.STUDENT_OF_THE_MONTH_PATH, ""));

        if (targetImage.exists()) {
            try {
                iv_student_of_the_year.setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(targetImage)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Glide
                    .with(getApplicationContext())
                    .load(imageUrl).apply(new RequestOptions()
                        .placeholder(R.drawable.ic_happy_smile)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .into(iv_student_of_the_year);
        }

    }

    private void fetchImagesUrl() {
//        String url = sp.getString("HostName", "Not Found")
//                + sp.getString(Config.applicationVersionName, "Not Found")
//                + "Gallery.php?databaseName=" + dbName + "&orgkey=" + sp.getString(Config.SMS_ORGANIZATION_NAME, "");


                String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/galleries"
                ;

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {


                sp.edit().putString("galleryData", jsonObject.toString()).apply();

                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                    JSONArray imagesArray = jsonObject.optJSONObject("result").optJSONArray("galleries");
                    String thumbnailpath = jsonObject.optString("thumbnailpath");
                    String originalpath = jsonObject.optString("originalpath");

                    try {
                        JSONObject stom = jsonObject.optJSONObject("result")
                                .optJSONArray("studentOfTheMonth")
                                .getJSONObject(0);

                        String stom_image  = stom.optString("imagename");
                        String stom_thumbnail = stom.optString("thumbnailpath");
                        String stom_originalpath = stom.optString("originalpath");
                        sp.edit().putString(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, stom_originalpath+stom_image)
                                .putString(Config.STUDENT_OF_THE_MONTH_PATH, stom_originalpath).apply();

                        setStom();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    galleryListData.clear();
                    for (int i = 0; i < imagesArray.length(); i++) {
                        JSONObject job = imagesArray.optJSONObject(i);
                        galleryListData.add(
                                new GalleryData(job.optString("imagename"), job.optString("thumbnailpath") + job.optString("imagename"), job.optString("originalpath") + job.optString("imagename"), job.optString("imagesize")));
                    }
                }
                adapter.updateList(galleryListData);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GalleryRES", volleyError.toString());
                Config.responseVolleyErrorHandler(GalleryActivity.this, volleyError, snackbar);
//                try {
//                    setGallery();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);
    }

    private void setGallery() throws JSONException {

        JSONObject jsonObject = new JSONObject(sp.getString("galleryData", ""));
        if (jsonObject != null) {
            JSONArray imagesArray = jsonObject.optJSONObject("result") != null ? jsonObject.optJSONObject("result").optJSONArray("images") : new JSONArray();
            String thumbnailpath = jsonObject.optString("thumbnailpath");
            String originalpath = jsonObject.optString("originalpath");
            for (int i = 0; i < imagesArray.length(); i++) {
                JSONObject job = imagesArray.optJSONObject(i);
                galleryListData.add(
                        new GalleryData(job.optString("imagename"), thumbnailpath + job.optString("imagename"), originalpath + job.optString("imagename"), job.optString("imagesize")));
            }

            adapter.updateList(galleryListData);
        }
    }

    public void closeSwipe() {
        if (swipe_refresh_list.isRefreshing()) swipe_refresh_list.setRefreshing(false);
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recycler_view_list_of_data.setLayoutManager(mLayoutManager);
        recycler_view_list_of_data.setItemAnimator(new DefaultItemAnimator());

        adapter = new GalleryAdapter(this, galleryListData);
        recycler_view_list_of_data.setAdapter(adapter);
        recycler_view_list_of_data.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(GalleryActivity.this, recycler_view_list_of_data, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", galleryListData);
                bundle.putInt("position", position);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                ft.addToBackStack("");
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // getMenuInflater().inflate(R.menu.menu_add, menu);
        return /*sp.getBoolean("addGalleryPermission", false)*/false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add_new_data) {

            if (takePermission()) addImageToGallery();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addImageToGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 444);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 444 && resultCode == RESULT_OK && data != null) {

            Uri uri = data.getData();

            long size = new File(getPath(uri)).length() / 1024;
            if (size > (1024 * 5)) {
                Config.responseSnackBarHandler("image size not more than 5 mb !", snackbar, appColor);
            } else {

                try {

                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    Bitmap b = BitmapFactory.decodeStream(inputStream);
                    try {

                        Log.e("SIZE", size + " ritu");
                        String filePath = getPath(uri);
                        uploadImageToServer(getStringImage(b), filePath.substring(filePath.lastIndexOf("/")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == 555 && resultCode == RESULT_OK && data != null) {

            Uri uri = data.getData();

            long size = new File(getPath(uri)).length() / 1024;
            if (size > (1024 * 5)) {
                Config.responseSnackBarHandler("image size not more than 5 mb !", snackbar, appColor);
            } else {

                try {

                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    final Bitmap b = BitmapFactory.decodeStream(inputStream);

                    Log.e("SIZE", size + " ritu");
                    final String filePath = getPath(uri);


                    AlertDialog.Builder alertUpdate = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    View view_update = getLayoutInflater().inflate(R.layout.layout_entry_msg_st_of_t_month, null);
                    final ImageView et_stom = (ImageView) view_update.findViewById(R.id.iv_sotm);

                    et_stom.setImageBitmap(b);
                    final EditText et_name = (EditText) view_update.findViewById(R.id.et_sotm_msg);

                    alertUpdate.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                uploadSTOMImageToServer(getStringImage(b), filePath.substring(filePath.lastIndexOf("/")), et_name.getText().toString().trim());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }

                    });
                    alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_hand_arrow));
                    alertUpdate.setTitle("Student Of the Month");
                    alertUpdate.setView(view_update);
                    alertUpdate.setCancelable(false);
                    alertUpdate.show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void uploadSTOMImageToServer(String stringImage, String nameImage, String contentMsg) throws JSONException {
        progressbar.show();
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/studentOfThaMonth"
                ;
        Log.e("GalleryUrl", url);

        final JSONObject param = new JSONObject();
        param.put("fileName", /*ReportQuery.randomAlphaNumeric(6)+".jpg"+*/nameImage);
        param.put("notificationMessage", contentMsg);
        param.put("fileData", stringImage);

        Log.e("GalleryRES", param.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("GalleryRES", jsonObject.toString());
                progressbar.dismiss();
                if (jsonObject.optInt("code") == 200) {
                    Config.responseSnackBarHandler(jsonObject.optString("message", "failed !"), snackbar,getResources().getColor(android.R.color.holo_green_dark));
                } else
                    Config.responseSnackBarHandler(jsonObject.optString("message", "failed !"), snackbar,getResources().getColor(android.R.color.holo_red_dark));

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GalleryRES", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(GalleryActivity.this, volleyError, snackbar);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void uploadImageToServer(String stringImage, String nameImage) throws JSONException {
        progressbar.show();
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "gallery/galleries"
                ;
        Log.e("GalleryUrl", url);

        final JSONObject param = new JSONObject();
        param.put("fileName", /*ReportQuery.randomAlphaNumeric(6)+".jpg"+*/nameImage);
        param.put("fileData", stringImage);

//        Log.e("GalleryRES", jsonObject.toString());

        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("GalleryRES", jsonObject.toString());
                progressbar.dismiss();
                if (jsonObject.optInt("code") == 200) {
                    fetchImagesUrl();
                    Config.responseSnackBarHandler(jsonObject.optString("message", "failed !"), snackbar,getResources().getColor(android.R.color.holo_green_dark));
                } else
                    Config.responseSnackBarHandler(jsonObject.optString("message", "failed !"), snackbar,getResources().getColor(android.R.color.holo_red_dark));
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GalleryRES", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(GalleryActivity.this, volleyError, snackbar);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);


    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        return encodedImage;
    }

    private boolean takePermission() {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);
            }
            return false;
        } else return true;
    }

    @Override
    public void onBackPressed() {
        Log.e("bbbbbbbbbbbb", "bbbbbbb");
        if (getIntent().getBooleanExtra("isFromActivity", true)) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            startActivity(new Intent(GalleryActivity.this, AdminNavigationDrawerNew.class));
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getIntent().getBooleanExtra("isFromActivity", true)) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            startActivity(new Intent(GalleryActivity.this, AdminNavigationDrawerNew.class));
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return super.onSupportNavigateUp();
    }

    @Override
    public void refreshList() {
        adapter.updateList(galleryListData);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.STUDENT_OF_THE_MONTH_MESSAGE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
