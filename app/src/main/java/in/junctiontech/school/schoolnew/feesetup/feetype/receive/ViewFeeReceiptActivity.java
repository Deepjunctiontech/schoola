package in.junctiontech.school.schoolnew.feesetup.feetype.receive;

import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class ViewFeeReceiptActivity extends AppCompatActivity {

    String selectedReceipt ;
    String selectedStudent;
    SchoolStudentEntity studentEntity;


    CircleImageView civ_fee_receipt_recipient;

    TextView tv_fee_receipt_recipient_receipt_date;
    TextView tv_fee_receipt_recipient_name;
    TextView tv_fee_receipt_recipient_subtext;

    LinearLayout ll_fee_receipt_items;

    TextView tv_fee_receipt_recipient_paying_total;

    ProgressBar progressBar;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_receipt);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initializeViews();

        selectedReceipt = getIntent().getStringExtra("receipt");
        selectedStudent = getIntent().getStringExtra("student");


        if(!("".equalsIgnoreCase(Strings.nullToEmpty(selectedReceipt)))){

            if(!("".equalsIgnoreCase(Strings.nullToEmpty(selectedReceipt)))){
                studentEntity = new Gson().fromJson(selectedStudent, SchoolStudentEntity.class);
                tv_fee_receipt_recipient_name.setText(studentEntity.StudentName);
                tv_fee_receipt_recipient_subtext.setText(studentEntity.ClassName+" : "+ studentEntity.SectionName);
            }

            FeeReceipt feeReceipt = new Gson().fromJson(selectedReceipt, FeeReceipt.class);

            try {
                getFeeReceiptDetails(feeReceipt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2875325645",this,adContainer);
        }
    }

    void initializeViews(){
        civ_fee_receipt_recipient = findViewById(R.id.civ_fee_receipt_recipient);
        tv_fee_receipt_recipient_receipt_date = findViewById(R.id.tv_fee_receipt_recipient_receipt_date);
        tv_fee_receipt_recipient_name = findViewById(R.id.tv_fee_receipt_recipient_name);
        tv_fee_receipt_recipient_subtext = findViewById(R.id.tv_fee_receipt_recipient_subtext);
        ll_fee_receipt_items = findViewById(R.id.ll_fee_receipt_items);
        tv_fee_receipt_recipient_paying_total = findViewById(R.id.tv_fee_receipt_recipient_paying_total);
        progressBar = findViewById(R.id.progressBar);

        if(!(Gc.getSharedPreference(Gc.LOGO_URL,this).equalsIgnoreCase("")))

            Glide.with(this)
                    .load(Gc.getSharedPreference(Gc.LOGO_URL,this))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                    .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                    .apply(RequestOptions.circleCropTransform())
                    .into(civ_fee_receipt_recipient);
    }

    void getFeeReceiptDetails(FeeReceipt feeReceipt) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();
        p.put("TransactionId", feeReceipt.TransactionId);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "fee/getFeesReciept/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");
        progressBar.setVisibility(View.GONE);
                switch (code) {
                    case "200":
                        try{

                            StudentFeeReceiptDetail receiptDetail = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("studentFeeReciept"),StudentFeeReceiptDetail.class);

                            ArrayList<FeePaidDetails> feePaidDetails = new Gson()
                                    .fromJson(job.getJSONObject("result").getJSONObject("studentFeeReciept")
                                            .optString("FeePaidDetails"), new TypeToken<List<FeePaidDetails>>(){}.getType());

                            if(feePaidDetails.size()>0){

                                displayFeeReceipt(receiptDetail,feePaidDetails );

                            }

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.activity_fee_receipt),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.activity_fee_receipt),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(ViewFeeReceiptActivity.this,error,findViewById(R.id.activity_fee_receipt));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void displayFeeReceipt(StudentFeeReceiptDetail receiptDetail, ArrayList<FeePaidDetails> feePaidDetails){

        int total = 0;

        tv_fee_receipt_recipient_receipt_date.setText(receiptDetail.TransactionDate);
        tv_fee_receipt_recipient_name.setText(receiptDetail.StudentName);
        tv_fee_receipt_recipient_subtext.setText(receiptDetail.ClassName+" : "+ receiptDetail.SectionName);
        ll_fee_receipt_items.setVisibility(View.VISIBLE);

        for (FeePaidDetails fee: feePaidDetails) {
            View view = getLayoutInflater().inflate(R.layout.item_fee_receipt, null);

            ((TextView) view.findViewById(R.id.tv_item_fee_number))
                    .setText(fee.MonthYear);
            ((TextView) view.findViewById(R.id.tv_item_fee_name))
                    .setText(fee.FeeName);
            ((TextView) view.findViewById(R.id.tv_item_fee_total_amount))
                    .setText(fee.AmountPaid);

            total+= Integer.parseInt(fee.AmountPaid);

            ll_fee_receipt_items.addView(view);
        }
        tv_fee_receipt_recipient_paying_total.setText(total+"");
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

class FeePaidDetails {
    public String FeeName;
    public String AmountPaid;
    public String MonthYear;
}

class StudentFeeReceiptDetail{
    public String TransactionId;
    public String AdmissionNo;
    public String StudentName;
    public String FatherName;
    public String MotherName;
    public String Mobile;
    public String ClassName;
    public String SectionName;
    public String TransactionSession;
    public String TransactionDate;
    public String TotalAmountPaid;
}
