package `in`.junctiontech.school.schoolnew.myschedule

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MyScheduleActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar
    lateinit var adapter: MyScheduleAdapter
    private val staffSubjectAllocation = ArrayList<StaffSubjectAllocation>()

    private var colorIs: Int = 0
    private var rvMySchedule: RecyclerView? = null
    private var dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)

    private var btnDate: Button? = null

    private var tvDay:TextView?=null
    private var tvTime:TextView?=null

    lateinit var cal: Calendar
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_schedule_my)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)
        cal = Calendar.getInstance()

        btnDate = findViewById<Button>(R.id.btn_timetable_mapping_activity_from_date)
        tvDay = findViewById(R.id.tv_day)
        tvTime = findViewById(R.id.tv_time)

        tvTime!!.text=SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(cal.time)
        tvDay!!.text=SimpleDateFormat("EEEE", Locale.ENGLISH).format(cal.time)
        btnDate!!.text = dateFormat.format(cal.time)
        btnDate!!.setOnClickListener {
            val calTemp = Calendar.getInstance()
            try {
                calTemp.time = dateFormat.parse(btnDate!!.text.toString())
            } catch (e: ParseException) {
                calTemp.timeInMillis = cal.timeInMillis
                e.printStackTrace()
            }
            val timePickerDialog = DatePickerDialog(this@MyScheduleActivity,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        cal.set(Calendar.YEAR, year)
                        cal.set(Calendar.MONTH, month)
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        btnDate!!.text = dateFormat.format(cal.time)

                        tvDay!!.text=SimpleDateFormat("EEEE", Locale.ENGLISH).format(cal.time)

                        val tempTime = Calendar.getInstance()
                        tvTime!!.text=SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(tempTime.time)

                        try {
                            getMySchedule()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    calTemp.get(Calendar.YEAR), calTemp.get(Calendar.MONTH), calTemp.get(Calendar.DAY_OF_MONTH))
            timePickerDialog.show()
        }

        rvMySchedule = findViewById(R.id.rv_my_schedule)
        setUpRecyclerView()
        getMySchedule()
        setColorApp()
        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/8053715268", this, adContainer)
        }
    }

    private fun getMySchedule() {
        progressBar.visibility = View.VISIBLE
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "timeTable/myScheduleTimeTable/"
                + JSONObject().put("Date", btnDate!!.text.toString()))

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), { job ->
            val code = job.optString("code")
            staffSubjectAllocation.clear()
            progressBar.visibility = View.GONE
            when (code) {
                Gc.APIRESPONSE200 -> {
                    try {

                        val staffSubjectAllocations = Gson()
                                .fromJson<ArrayList<StaffSubjectAllocation>>(job.getJSONObject("result").getJSONObject("staffSubjectAllocation").optString("timeTableSlots"), object : com.google.gson.reflect.TypeToken<List<StaffSubjectAllocation>>() {
                                }.type)

                        val dateFormats = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                        val dates = dateFormats.format(dateFormat.parse(btnDate!!.text.toString()))
                        for (i in staffSubjectAllocations.indices) {
                            if (staffSubjectAllocations[i].Date == dates) {
                                staffSubjectAllocation.add(staffSubjectAllocations[i])
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.my_schedule_activity), R.color.fragment_first_green)
                }

                Gc.APIRESPONSE401 -> {

                  val setDefaults1 = HashMap<String, String>()
                      setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                      Gc.setSharedPreference(setDefaults1, this@MyScheduleActivity)

                      val intent1 = Intent(this@MyScheduleActivity, AdminNavigationDrawerNew::class.java)
                      intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                      startActivity(intent1)

                      finish()
                }

                Gc.APIRESPONSE500 -> {
                     val setDefaults = HashMap<String, String>()
                     setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                     Gc.setSharedPreference(setDefaults, this@MyScheduleActivity)

                     val intent2 = Intent(this@MyScheduleActivity, AdminNavigationDrawerNew::class.java)
                     intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                     startActivity(intent2)

                     finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.my_schedule_activity), R.color.fragment_first_blue)
            }
            adapter.notifyDataSetChanged()
        }, { error ->
            progressBar.visibility = View.VISIBLE
            Config.responseVolleyErrorHandler(this@MyScheduleActivity, error, findViewById(R.id.my_schedule_activity))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return null
            }

        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpRecyclerView() {
        adapter = MyScheduleAdapter(this)
        val mLayoutManager = LinearLayoutManager(this)
        rvMySchedule!!.layoutManager = mLayoutManager
        val itemAnimator = DefaultItemAnimator()
        itemAnimator.addDuration = 1000
        itemAnimator.removeDuration = 1000
        rvMySchedule!!.itemAnimator = itemAnimator
        rvMySchedule!!.adapter = adapter
    }

    inner class MyScheduleAdapter internal constructor(internal var activity: Activity) : RecyclerView.Adapter<MyScheduleAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.my_schedule_list_recycler, parent, false))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val startEndTime="${staffSubjectAllocation[position].StartTime} ${staffSubjectAllocation[position].EndTime}"
            val classSectionsName=staffSubjectAllocation[position].ClassName.toString() + " " + staffSubjectAllocation[position].SectionName.toString()
            holder.tvItemStartEndTime.text = startEndTime
            holder.tvMyScheduleSubjectName.text = staffSubjectAllocation[position].SubjectName
            holder.tvItemMyScheduleClassSection.text = classSectionsName
            holder.tvItemSlotName.text = staffSubjectAllocation[position].SlotName
        }

        override fun getItemCount(): Int {
            return staffSubjectAllocation.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvItemStartEndTime: TextView = itemView.findViewById(R.id.tv_item_start_end_time)
            internal var tvMyScheduleSubjectName: TextView = itemView.findViewById(R.id.tv_my_schedule_subject_name)
            internal var tvItemMyScheduleClassSection: TextView = itemView.findViewById(R.id.tv_item_my_schedule_class_section)
            internal var tvItemSlotName: TextView = itemView.findViewById(R.id.tv_item_slot_name)
        }
    }
}

internal class StaffSubjectAllocation {
    var Date: String? = null
    var SlotName: String? = null
    var StartTime: String? = null
    var EndTime: String? = null
    var SubjectName: String? = null
    var StaffName: String? = null
    var ClassName: String? = null
    var SectionName: String? = null
}