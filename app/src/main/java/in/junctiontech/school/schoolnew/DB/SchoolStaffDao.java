package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolStaffDao {
    @Query("select * from SchoolStaffEntity where StaffStatus = 'Active'")
    LiveData<List<SchoolStaffEntity>> getAllStaff();

    @Query("select count(StaffId) as staffCount from SchoolStaffEntity where StaffStatus = 'Active'")
    LiveData<SchoolStaffCountTopple> getAllStaffCount();

    @Query("select * from SchoolStaffEntity where StaffId like :staffid")
    LiveData<SchoolStaffEntity> getStaffById(String staffid);

    @Query("select * from SchoolStaffEntity where UserId like :userid")
    LiveData<SchoolStaffEntity> getStaffByUserId(String userid);

    @Query("delete from SchoolStaffEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertStaff(List<SchoolStaffEntity> schoolStaffEntities);
}
