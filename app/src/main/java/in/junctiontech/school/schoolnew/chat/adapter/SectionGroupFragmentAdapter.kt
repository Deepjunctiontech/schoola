package `in`.junctiontech.school.schoolnew.chat.adapter

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.model.ClassNameSectionName
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView

class SectionGroupFragmentAdapter(sectionGroup: ArrayList<ClassNameSectionName>, private val context: Context, activity: FragmentActivity?,val listener:ClassClickListener) : RecyclerView.Adapter<SectionGroupFragmentAdapter.ViewHolder>() {
    private var sectionGroupListInfo: ArrayList<ClassNameSectionName> = sectionGroup

    private val colorIs = Config.getAppColor(activity, true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listener)
        holder.tvChatName.setTextColor(colorIs)

        holder.tvChatName.text = (sectionGroupListInfo[position].ClassName
                + " "
                + sectionGroupListInfo[position].SectionName)
        holder.icContact.setImageDrawable(getDrawable(context, R.drawable.ic_single))
    }

    override fun getItemCount(): Int {
        return sectionGroupListInfo.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvChatName: TextView = itemView.findViewById(R.id.tv_chat_name)
        var icContact: CircleImageView = itemView.findViewById(R.id.ic_contact)
fun bind(listener: ClassClickListener){
    itemView.setOnClickListener {
        listener.onStartClassChatCallback(adapterPosition)
    }
}
        /*init {
            itemView.setOnClickListener { view: View ->
                val intent = Intent(this@StaffListFragment, MessageListActivity::class.java)
                val staff = Gson().toJson(staffListInfo.get(adapterPosition))
                intent.putExtra("with", staff)
                intent.putExtra("withType", "staff")
                startActivity(intent)
                finish()
            }
        }*/
    }

    interface ClassClickListener{
        fun onStartClassChatCallback(position: Int)
    }
}
