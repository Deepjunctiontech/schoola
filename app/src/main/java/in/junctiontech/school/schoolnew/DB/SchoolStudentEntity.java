package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolStudentEntity {
    @PrimaryKey
    @NonNull
    public String RegistrationId;
    @NonNull
    public String studentfeeSession;
    public String Status;
    public String registrationSession;
    public String StudentName;
    public String StudentEmail;
    public String FatherName;
    public String FatherMobile;
    public String studentProfileImage;
    public String parentProfileImage;
    public String FatherDateOfBirth;
    public String FatherEmail;
    public String FatherQualification;
    public String FatherOccupation;
    public String FatherDesignation;
    public String FatherOrganization;
    public String MotherName;
    public String MotherMobile;
    public String MotherDateOfBirth;
    public String MotherEmail;
    public String MotherQualification;
    public String MotherOccupation;
    public String MotherDesignation;
    public String MotherOrganization;
    public String Mobile;
    public String DOB;
    public String DOR;
    public String Landline;
    public String AlternateMobile;
    public String PresentAddress;
    public String PermanentAddress;
    public String BloodGroup;
    public String Caste;
    public String Category;
    public String Gender;
    public String Nationality;
    public String parentGcmTokenId;
    public String StudentsPassword;
    public String ParentsPassword;
    public String studentGcmTokenId;
    public String studentIMEI;
    public String parentIMEI;
    public String ParentDevice;
    public String StudentDevice;
    public String DateOfTermination;
    public String TerminationReason;
    public String TerminationRemarks;
    public String Pterms;
    public String Sterms;
    public String AdmissionId;
    public String AdmissionNo;
    public String SectionId;
    public String SectionName;
    public String ClassName;
    public String ClassId;
    public String BloodGroupValue;
    public String CasteValue;
    public String CategoryValue;
    public String GenderValue;
    public String TerminationReasonValue;
    public String DOE;

}
