package in.junctiontech.school.schoolnew.registrationprocess;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

import static com.android.volley.Request.*;

public class UserVerificationActivity extends AppCompatActivity {

    private EditText et_numberverfy, et_numberverfy_otp;

    String request_id = "";
    private Button btn_otp, btn_verify;
    private String key = "2d2c27e9";
    private String secretKey = "efd216702e859cf5";
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private static UserVerificationActivity context;
    private String OTP;
    private boolean numVerify = false;
    private String mobileNumber ="", otp="";
    private AlertDialog.Builder alert1;
    private TextView tv_verification_emailId;

    public static UserVerificationActivity getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_number_varification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        initViews();


    }

    private void initViews() {
        btn_verify = (Button) findViewById(R.id.btn_verify);
        btn_otp = (Button) findViewById(R.id.btn_otp);
        tv_verification_emailId = (TextView)findViewById(R.id.tv_verification_emailId);
        et_numberverfy = (EditText) findViewById(R.id.et_numberverfy);
        et_numberverfy_otp = (EditText) findViewById(R.id.et_numberverfy_otp);
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setCancelable(false);
    }


    public void verifyNumber(final View view) {
       Config.hideKeyboard(this);

        if (et_numberverfy.getText().toString().trim().equalsIgnoreCase("")
                || et_numberverfy.getText().toString().trim().equalsIgnoreCase("@.")
                ||
                !et_numberverfy.getText().toString().trim().contains("@")||
                !et_numberverfy.getText().toString().trim().contains(".")
                ){
            et_numberverfy.setError(getString(R.string.please_enter_valid_email_id));
        }else {

            alert1 = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alert1.setTitle(getString(R.string.confirmation));
            alert1.setIcon(getResources().getDrawable(android.R.drawable.ic_menu_help));

            alert1.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    numberVerify(view);
                }
            });
            alert1.setNegativeButton(R.string.edit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert1.setCancelable(false);
            alert1.setTitle(getString(R.string.confirm_email));
//        alert1.setMessage(getString(R.string.is)+" "+et_numberverfy_country_code.getText().toString()+
//        et_numberverfy.getText().toString()+" "+ getString(R.string.your_mobile_number));

            alert1.setMessage(Html.fromHtml(
                    getString(R.string.is)  +" <B>"+
                            "<font size='7' color='#E64A19'>" + et_numberverfy.getText().toString()  + "</font>" +"</B> "+ getString(R.string.your_email_id)+ "<br>"));

            alert1.show();
        }

//        JSONObject obj = new JSONObject();
//        try {
//            obj.put("number", et_numberverfy.getText().toString());
//            obj.put("apiKey","a236b800");
//            obj.put("api_secret","cf543c69a0231344");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        RequestQueue queue = Volley.newRequestQueue(this.getApplicationContext());
//        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,
//                url,
//                obj,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                     //   System.out.println(response);
//Log.e("NumberVeryResponse",response.toString());
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.e("ErrorResponse",error.toString());
//                    }
//                });
//        queue.add(jsObjRequest);

    }

    private void numberVerify(final View view) {
Log.e("EmaiVerifyUrl",Config.sendEmail);
            progressbar.show();
            StringRequest request = new StringRequest(Method.POST,
                    Config.sendEmail,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String s) {
                            Log.e("NumberVeryResponse", s+"    ritu");

                            progressbar.cancel();
                          if(! s.equalsIgnoreCase("")) {
                              try {

                                  JSONObject jsonObject = new JSONObject(s);
                                  request_id = jsonObject.optString("request_id");
                                  alert.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info));
                                  alert.setTitle(getString(R.string.result));
                                  if (jsonObject.optInt("code") == 200||
                                          jsonObject.optInt("code")== 203) {

                                      tv_verification_emailId.startAnimation(AnimationUtils.loadAnimation(
                                              context,R.anim.zoom_from_in
                                      ));
                                      tv_verification_emailId.setText(getString(R.string.email_id)+" : "+
                                              et_numberverfy.getText().toString());
                                      btn_verify.setVisibility(View.GONE);
                                      et_numberverfy.setVisibility(View.GONE);

                                      btn_otp.setVisibility(View.VISIBLE);
                                      et_numberverfy_otp.setVisibility(View.VISIBLE);
                                      numVerify = true;
                                      //enterOtp();

                                      alert.setMessage(Html.fromHtml(getString(R.string.otp_sent_successfully_on)+
                                              " "+" <B>"  +"<font size='8' color='#E64A19'>" + et_numberverfy.getText().toString() + "</font>" +"</B>"+"\n"+
                                              getString(R.string.please_check_your_email)));
                                  }else alert.setMessage(jsonObject.optString("result"));

                                  alert.show();
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }

                          }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressbar.cancel();
                    Log.e("ErrorResponse", volleyError.toString());
                   // if (!Config.checkInternet(context)) showAlertOfInternet();
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                       showAlertOfInternet();
                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    // params.put("apiKey", key);
                    params.put("email", et_numberverfy.getText().toString());
                    // params.put("apiSecret", secretKey);

                    Map<String, String> param = new LinkedHashMap<String, String>();
                    param.put("json", new JSONObject(params).toString());
                    Log.d("loginJson", new JSONObject(param).toString());

                    return param;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);


    }

    public void enterOtp(View view) {
        Config.hideKeyboard(this);
        if (!et_numberverfy_otp.getText().toString().trim().equalsIgnoreCase("")) {

            progressbar.setMessage(getString(R.string.verifying));
            progressbar.show();
            Log.e("OTPUrl",  Config.verifyOtp);

            StringRequest request = new StringRequest(Method.POST, Config.verifyOtp,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String s) {
                            Log.e("OTPResponse", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optInt("code") ==200) {
                                   Intent intent = new Intent(UserVerificationActivity.this, RegistrationSchoolActivity.class);
                                    intent.putExtra("email",et_numberverfy.getText().toString());
                                    //intent .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter,R.anim.nothing);
                                } else
                                     {
                                    alert.setMessage(jsonObject.optString("result"));
                                    alert.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    progressbar.cancel();
                  //  if (!Config.checkInternet(context)) showAlertOfInternet();
                    Log.e("OTPErrorResponse", volleyError.toString());
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                        showAlertOfInternet();
                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    //   params.put("apiKey", key);
                    params.put("code", et_numberverfy_otp.getText().toString());
                    //  params.put("apiSecret", secretKey);
                    params.put("email",  et_numberverfy.getText().toString());

                    Map<String, String> param = new LinkedHashMap<String, String>();
                    param.put("json", new JSONObject(params).toString());

                    Log.d("loginJson", new JSONObject(param).toString());

                    return param;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        } else {
            alert.setMessage(getString(R.string.please_enter_otp));
            alert.show();

        }
    }

//    public void searchNumber(View view) {
//        RequestQueue queue = Volley.newRequestQueue(this.getApplicationContext());
//
//        StringRequest request = new StringRequest(Method.POST, searchOtp,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String s) {
//                        Log.e("searchOtpResponse", s);
//                        //  verifyControl();
//
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                if (!Config.checkInternet(context)) showAlertOfInternet();
//                Log.e("searchOtpErrorResponse", volleyError.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> header = new LinkedHashMap<String, String>();
//                header.put("Content-Type", "application/x-www-form-urlencoded");
//                return super.getHeaders();
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new LinkedHashMap<String, String>();
//                //  params.put("apiKey", key);
//                //  params.put("apiSecret", secretKey);
//                params.put("requestID", request_id);
//                Log.d("loginJson", new JSONObject(params).toString());
//
//                return params;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//    }
//
//    public void verifyControl(View view) {
//        StringRequest request = new StringRequest(Method.POST, verifyControl,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String s) {
//                        Log.e("verifyControlResponse", s);
//
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                if (!Config.checkInternet(context)) showAlertOfInternet();
//                Log.e("verifyControlesponse", volleyError.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> header = new LinkedHashMap<String, String>();
//                header.put("Content-Type", "application/x-www-form-urlencoded");
//                return super.getHeaders();
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new LinkedHashMap<String, String>();
//                //  params.put("apiKey", key);
//                params.put("requestID", request_id);
//                //   params.put("apiSecret", secretKey);
//                Log.d("loginJson", new JSONObject(params).toString());
//
//                return params;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//    }

    public void setOTP(String otp) {
        et_numberverfy_otp.setText(otp);
        enterOtp(new View(this));
    }

    public void showAlertOfInternet() {
        alert.setMessage(R.string.internet_not_available_please_check_your_internet_connectivity);
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getBooleanExtra("choice", false)){
            finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

    } else {
//            startActivity(new Intent(this, FirstScreen.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
            overridePendingTransition(R.anim.enter,R.anim.nothing);
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("request_id", request_id);
        outState.putString("mobileNumber", et_numberverfy.getText().toString());
        outState.putBoolean("numVerify", numVerify);
        outState.putString("otp",et_numberverfy_otp.getText().toString());
    }


}
