package in.junctiontech.school.schoolnew.DB.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import java.util.List;

import in.junctiontech.school.schoolnew.DB.SessionListEntity;
import in.junctiontech.school.schoolnew.DB.repository.SessionRepository;

public class SessionViewModel extends AndroidViewModel {
    private SessionRepository mRepository;

    private LiveData<List<SessionListEntity>> mAllSession;


    public SessionViewModel(@NonNull Application application) {
        super(application);
        mRepository = new SessionRepository(application)   ;
        mAllSession = mRepository.getSessionList();
    }

    public LiveData<List<SessionListEntity>> getSessionList(){
        return mAllSession;
    }

    public void insert(SessionListEntity[] sessionListEntities){
        mRepository.insert(sessionListEntities);
    }
}
