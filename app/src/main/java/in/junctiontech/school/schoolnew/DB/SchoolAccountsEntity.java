package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolAccountsEntity {
    @PrimaryKey
    @NonNull
    public String AccountId;
    public String AccountStatus;
    public String ManagedBy;
    public String AccountName;
    public String BankAccountName;
    public String AccountType;
    public String BankName;
    public String BranchName;
    public String IFSCCode;
    public String OpeningBalance;
    public String AccountBalance;
    public String AccountDate;
    public String DOE;
    public String managedByName;
    public String accountTypeName;
}
