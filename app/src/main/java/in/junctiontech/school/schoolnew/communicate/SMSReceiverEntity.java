package in.junctiontech.school.schoolnew.communicate;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class SMSReceiverEntity {
    @PrimaryKey(autoGenerate = true)
    public int autoid;
    public String usertype ;
    public String userid;
    public String name ;
    public String mobile;
    public String mobileType;
   public String sentDate;
   public String sentTime;
   public boolean delivered;
    public String deliveredDate;
    public String deliveredTime;
}
