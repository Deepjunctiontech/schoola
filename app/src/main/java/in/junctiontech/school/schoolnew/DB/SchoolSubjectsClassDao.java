package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolSubjectsClassDao {
    @Query("select SectionId from SchoolSubjectsClassEntity where SubjectId LIKE :subjectid")
    List<String> getClassForSubject(String subjectid);


    @Query("select SchoolSubjectsEntity.*"+
            "from SchoolSubjectsClassEntity, SchoolSubjectsEntity " +
            "where SchoolSubjectsClassEntity.SectionId LIKE :sectionid " +
            "and SchoolSubjectsClassEntity.SubjectId=SchoolSubjectsEntity.SubjectId")
    LiveData<List<SchoolSubjectsEntity>> getSubjectsForSection(String sectionid);

    @Query("select * from SchoolSubjectsClassEntity where ClassId LIKE :classid ")
    List<SchoolSubjectsClassEntity> getSubjectsForClass(String classid);

    @Insert(onConflict = REPLACE)
    void insertSubJectClassMapping(List<SchoolSubjectsClassEntity> schoolSubjectsClassEntity);

    @Query("delete from SchoolSubjectsClassEntity where SubjectId like :subjectid")
    void deleteAll(String subjectid);


}
