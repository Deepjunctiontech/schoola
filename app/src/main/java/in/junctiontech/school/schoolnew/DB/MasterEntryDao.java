package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MasterEntryDao {
    @Query("select * from MasterEntryEntity where MasterEntryName LIKE :masterentryname")
    List<MasterEntryEntity> getMasterEntryValuesForKey(String masterentryname);

    @Query("select * from MasterEntryEntity where MasterEntryName LIKE :masterentryname and MasterEntryValue not in ('STUDENT', 'PARENTS','VENDOR') order by MasterEntryValue")
    LiveData<List<MasterEntryEntity>> getMasterEntryValuesForKeyLive(String masterentryname);

    @Query("select * from MasterEntryEntity where MasterEntryName LIKE :masterentryname order by MasterEntryValue")
    LiveData<List<MasterEntryEntity>> getMasterEntryValuesGeneric(String masterentryname);

    @Query("select * from MasterEntryEntity where MasterEntryName LIKE :masterentryname")
    List<MasterEntryEntity> getMasterEntryValues(String masterentryname);

    @Query("delete from MasterEntryEntity")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertMasterEntry(MasterEntryEntity masterEntryEntity);

    @Insert(onConflict = REPLACE)
    void insertMasterEntries(List<MasterEntryEntity> masterEntryEntities);
}
