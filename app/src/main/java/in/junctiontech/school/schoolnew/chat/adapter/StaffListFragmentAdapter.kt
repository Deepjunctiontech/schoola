package `in`.junctiontech.school.schoolnew.chat.adapter

import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.SchoolStaffEntity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

class StaffListFragmentAdapter(staffInfo: java.util.ArrayList<SchoolStaffEntity>, private val context: Context, activity: FragmentActivity?,val listener: StaffClickListner) : RecyclerView.Adapter<StaffListFragmentAdapter.ViewHolder>() {
    private var staffListInfo: ArrayList<SchoolStaffEntity> = staffInfo

    private val colorIs = Config.getAppColor(activity, true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listener)
        holder.tvChatName.setTextColor(colorIs)
        holder.tvChatName.text = (staffListInfo[position].StaffName
                + " "
                + staffListInfo[position].UserTypeValue)
        if ("" != staffListInfo[position].staffProfileImage) Glide.with(context)
                .load(staffListInfo[position].staffProfileImage)
                .apply(RequestOptions()
                        .error(R.drawable.ic_single)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .thumbnail(0.5f)
                .into(holder.icContact) else holder.icContact.setImageDrawable(getDrawable(context, R.drawable.ic_single))
    }

    override fun getItemCount(): Int {
        return staffListInfo.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvChatName: TextView = itemView.findViewById(R.id.tv_chat_name)
        var icContact: CircleImageView = itemView.findViewById(R.id.ic_contact)
        fun bind(listener: StaffClickListner) {




            // this is the click listener. It calls the onItemClicked interface method implemented in the Activity
            itemView.setOnClickListener {
                listener.onStartStaffClickCallback(adapterPosition)
            }
        }
        /*init {
            itemView.setOnClickListener { view: View ->
                val intent = Intent(this@StaffListFragment, MessageListActivity::class.java)
                val staff = Gson().toJson(staffListInfo.get(adapterPosition))
                intent.putExtra("with", staff)
                intent.putExtra("withType", "staff")
                startActivity(intent)
                finish()
            }
        }*/
    }
    interface StaffClickListner {
        fun onStartStaffClickCallback(position: Int)

    }
}
