package `in`.junctiontech.school.schoolnew.leave

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class AssignedLeaveActivity : AppCompatActivity() {
    lateinit var progressBar: ProgressBar
    private var rvLeaveDetails: RecyclerView? = null
    private var tv_no_assigned_leave: TextView? = null
    private val leavesDetails = ArrayList<LeavesDetails>()
    lateinit var adapterLeaveDetails: LeaveDetailsAdapter

    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_assigned)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)
        tv_no_assigned_leave = findViewById(R.id.tv_no_assigned_leave)
        rvLeaveDetails = findViewById(R.id.rv_leave_details)

        setUpRecyclerView()
        getLeaves()
        setColorApp()
        if (!Arrays.asList(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase()) && Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/8658358102", this, adContainer)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpRecyclerView() {
        adapterLeaveDetails = LeaveDetailsAdapter(this)
        val mLayoutManager = LinearLayoutManager(this)
        rvLeaveDetails!!.layoutManager = mLayoutManager
        val itemAnimator = DefaultItemAnimator()
        itemAnimator.addDuration = 1000
        itemAnimator.removeDuration = 1000
        rvLeaveDetails!!.itemAnimator = itemAnimator
        rvLeaveDetails!!.adapter = adapterLeaveDetails
    }

    private fun getLeaves() {
        progressBar.visibility = View.VISIBLE
        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "profile/staffProfile/"
                + JSONObject().put("ProfileId", Gc.getSharedPreference(Gc.USERID, applicationContext)))

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), { job ->
            val code = job.optString("code")
            leavesDetails.clear()

            progressBar.visibility = View.GONE
            when (code) {
                Gc.APIRESPONSE200 -> {
                    try {
                        val staffJArr = job.getJSONObject("result").getJSONArray("staffLeave")
                        staffJArr.getJSONObject(0).toString()
                        val ok = Gson()
                                .fromJson<ArrayList<LeavesDetails>>(staffJArr.getJSONObject(0).optString("fixedLeave"), object : com.google.gson.reflect.TypeToken<List<LeavesDetails>>() {
                                }.type)

                        if (ok != null) {
                            leavesDetails.addAll(ok)
                            tv_no_assigned_leave?.visibility = View.GONE
                        }
                        else{
                            tv_no_assigned_leave?.visibility = View.VISIBLE
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.leave_activity), R.color.fragment_first_green)
                }

                Gc.APIRESPONSE401 -> {

                    val setDefaults1 = HashMap<String, String>()
                    setDefaults1[Gc.NOTAUTHORIZED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults1, this@AssignedLeaveActivity)

                    val intent1 = Intent(this@AssignedLeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent1)

                    finish()
                }

                Gc.APIRESPONSE500 -> {
                    val setDefaults = HashMap<String, String>()
                    setDefaults[Gc.ISORGANIZATIONDELETED] = Gc.TRUE
                    Gc.setSharedPreference(setDefaults, this@AssignedLeaveActivity)

                    val intent2 = Intent(this@AssignedLeaveActivity, AdminNavigationDrawerNew::class.java)
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent2)

                    finish()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.leave_activity), R.color.fragment_first_blue)
            }
            adapterLeaveDetails.notifyDataSetChanged()
        }, { error ->
            progressBar.visibility = View.VISIBLE
            Config.responseVolleyErrorHandler(this@AssignedLeaveActivity, error, findViewById(R.id.leave_activity))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)

                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return null
            }

        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1120) {
            if (resultCode == Activity.RESULT_OK) {
                getLeaves()
                val message = Objects.requireNonNull(data!!.extras)?.getString("message")
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.leave_activity), R.color.fragment_first_green)
            }
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    inner class LeaveDetailsAdapter internal constructor(internal var activity: Activity) : RecyclerView.Adapter<LeaveDetailsAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_leave_details, parent, false))
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvLeaveType.text = leavesDetails[position].leaveName
            holder.tvItemTimeCircle.text = leavesDetails[position].timeCircle
            holder.tvItemType.text = leavesDetails[position].type
            holder.tvItemLeaves.text = leavesDetails[position].leave
            holder.tvItemTotalApply.text = leavesDetails[position].TotalApprove
        }

        override fun getItemCount(): Int {
            return leavesDetails.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var tvLeaveType: TextView = itemView.findViewById(R.id.tv_leave_type)
            internal var tvItemTimeCircle: TextView = itemView.findViewById(R.id.tv_item_time_circle)
            internal var tvItemType: TextView = itemView.findViewById(R.id.tv_item_type)
            internal var tvItemLeaves: TextView = itemView.findViewById(R.id.tv_item_leaves)
            internal var tvItemTotalApply: TextView = itemView.findViewById(R.id.tv_item_total_apply)
        }
    }
}
