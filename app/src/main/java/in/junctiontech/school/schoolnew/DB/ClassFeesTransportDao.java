package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ClassFeesTransportDao {
    @Insert(onConflict = REPLACE)
    void insertClassFee(List<ClassFeesTransportEntity> classFeesTransportEntities);

    @Query("select * from ClassFeesTransportEntity where SectionId LIKE :sectionid and FeeStatus == 'Active' and Session like :session")
    LiveData<List<ClassFeesTransportEntity>> getFeeforClassSectionLive(String sectionid, String session );

    @Query("delete from ClassFeesTransportEntity ")
    void deleteAll();
}
