package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssignmentDao {
    @Query("select * from AssignmentEntity")
    LiveData<List<AssignmentEntity>> getAllAssignments();

    @Query("select * from AssignmentEntity where  dateofhomework between  :from and :to and sectionid like :section ")
    LiveData<List<AssignmentEntity>> getAssignmentsBetweenDates(String section ,Date from , Date to);

    @Query("select * from AssignmentEntity where  dateofhomework like :homeworkDate and sectionid like :section ")
    LiveData<List<AssignmentEntity>> getAssignmentsForDate(String section ,Date homeworkDate );

    @Insert(onConflict = REPLACE)
    void insertAssignment(List<AssignmentEntity> assignmentEntities);

    @Query("delete from AssignmentEntity where dateofhomework between  :from and :to and  sectionid like :section")
    void deleteMonthForSection(String section , Date from , Date to );

    @Query("delete from AssignmentEntity where dateofhomework like :homeworkdate and subjectid like:subject and  sectionid like :section")
    void deleteSingleAssignment(String section , String homeworkdate , String subject );

    @Delete
    void deleteAssignment(AssignmentEntity assignmentEntity);
}
