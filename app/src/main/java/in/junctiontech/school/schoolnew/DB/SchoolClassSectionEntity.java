package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SchoolClassSectionEntity {
    @PrimaryKey
    @NonNull
    public String SectionId;
    public String SectionName;
    public String ClassId;
    public String SectionStatus;
    public String DOE;
}
