package in.junctiontech.school.schoolnew.feesetup.feetype;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class FeeTypeCreateActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;

    RadioButton rb_create_monthly, rb_create_fee_quarterly, rb_create_fee_session;
    RadioButton rb_feetype_active, rb_feetype_inactive;
    EditText et_fee_type_name;

    FeeTypeEntity fee;

    String[] monthList;
    ArrayList<String> excludedMonths = new ArrayList<>();

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_feetype_edit);

        mDb = MainDatabase.getDatabase(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        monthList = Gc.findMonthsInSession(Gc.getSharedPreference(Gc.CURRENTSESSIONSTART, this));

        rb_feetype_active = findViewById(R.id.rb_feetype_active);
        rb_feetype_inactive = findViewById(R.id.rb_feetype_inactive);

        rb_create_fee_quarterly = findViewById(R.id.rb_create_fee_quarterly);
        rb_create_fee_quarterly.setVisibility(View.GONE);
        rb_create_fee_session = findViewById(R.id.rb_create_fee_session);
        rb_create_monthly = findViewById(R.id.rb_create_monthly);
        rb_create_monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getExcludedMonths();
            }
        });

        if (Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toUpperCase().equals(Gc.PLANTYPEFREE)) {
            LinearLayout linearLayout9 = findViewById(R.id.linearLayout9);
            linearLayout9.setVisibility(View.GONE);
            rb_create_fee_session.setChecked(true);
        }

        et_fee_type_name = findViewById(R.id.et_fee_type_name);
        progressBar = findViewById(R.id.progressBar);

        String feeUpdate = getIntent().getStringExtra("fee");
        if (feeUpdate != null) {
            fee = new Gson().fromJson(feeUpdate, FeeTypeEntity.class);
            if (fee.Frequency.equalsIgnoreCase("Monthly"))
                rb_create_monthly.setChecked(true);
            else if (fee.Frequency.equalsIgnoreCase("Quarterly"))
                rb_create_fee_quarterly.setChecked(true);
            else if (fee.Frequency.equalsIgnoreCase("Session"))
                rb_create_fee_session.setChecked(true);

            if (fee.Status.equalsIgnoreCase(Gc.ACTIVE))
                rb_feetype_active.setChecked(true);
            else if (fee.Status.equalsIgnoreCase("Inactive"))
                rb_feetype_inactive.setChecked(true);

            et_fee_type_name.setText(fee.FeeType);
            if (!("".equalsIgnoreCase(Strings.nullToEmpty(fee.ExMonths))))
                excludedMonths.addAll(Arrays.asList(fee.ExMonths.split(",")));

        } else {
            fee = new FeeTypeEntity();
        }

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/9558368628", this, adContainer);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.menu_next).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_save:
                if (validateInput()) {
                    try {
                        addFeeType(fee.FeeType, fee.FeeTypeID, fee.Frequency, fee.Status);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void getExcludedMonths() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.exclude_months);

        final boolean[] checkedItems = new boolean[monthList.length];

        for (int i = 0; i < monthList.length; i++) {
            if (excludedMonths.contains(monthList[i]))
                checkedItems[i] = false;
            else
                checkedItems[i] = true;
        }

        builder.setMultiChoiceItems(monthList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                if (b) {
                    if (excludedMonths.contains(monthList[i]))
                        excludedMonths.remove(monthList[i]);
                } else {
                    if (!(excludedMonths.contains(monthList[i])))
                        excludedMonths.add(monthList[i]);

                }
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addFeeType(String feeType, String feeTypeId, String frequency, String status) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/feeTypes";

        int method = Request.Method.POST;

        if (!("".equalsIgnoreCase(Strings.nullToEmpty(feeTypeId)))) {
            url = url + "/" + new JSONObject().put("FeeTypeID", feeTypeId);
            method = Request.Method.PUT;
        }

        p.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        p.put("FeeType", feeType);
        p.put("Frequency", frequency);
        p.put("Status", status);

        JSONArray exmontharr = new JSONArray();
        for (String s : excludedMonths) {
            exmontharr.put(s);
        }

        if (!(excludedMonths.isEmpty()) && frequency.equalsIgnoreCase("Monthly")) {
            p.put("monthYear", exmontharr);
        }

        param.put(p);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressBar.setVisibility(View.GONE);
                        String code = job.optString("code");

                        switch (code) {
                            case Gc.APIRESPONSE200:
                                progressBar.setVisibility(View.GONE);
                                try {
//                                    JSONObject resultjobj = job.getJSONObject("result");
//                                    JSONArray feeTypesjarr = resultjobj.getJSONArray("feeTypes");
                                    final ArrayList<FeeTypeEntity> feeTypeEntity = new Gson()
                                            .fromJson(job.getJSONObject("result").optString("feeTypes"),
                                                    new TypeToken<List<FeeTypeEntity>>() {
                                                    }.getType());

                                    new Thread(() -> {
                                        mDb.feeTypeModel().insertFeeTypes(feeTypeEntity);
                                    }).start();


                                    Intent intent = new Intent();
                                    intent.putExtra("message", getString(R.string.success));
                                    setResult(RESULT_OK, intent);
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, FeeTypeCreateActivity.this);

                                Intent intent1 = new Intent(FeeTypeCreateActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, FeeTypeCreateActivity.this);

                                Intent intent2 = new Intent(FeeTypeCreateActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                            default:
                                progressBar.setVisibility(View.GONE);

                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.top_layout), R.color.fragment_first_blue);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                VolleyLog.e("Error: ", error.getMessage());
                Config.responseVolleyErrorHandler(FeeTypeCreateActivity.this, error, findViewById(R.id.top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    Boolean validateInput() {

        if (rb_create_monthly.isChecked()) fee.Frequency = "Monthly";
        if (rb_create_fee_quarterly.isChecked()) fee.Frequency = "Quarterly";
        if (rb_create_fee_session.isChecked()) fee.Frequency = "Session";

        if ("".equalsIgnoreCase(Strings.nullToEmpty(fee.Frequency))) {
            Config.responseSnackBarHandler(getString(R.string.select_fee_frequency),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        if (rb_feetype_active.isChecked()) fee.Status = Gc.ACTIVE;
        if (rb_feetype_inactive.isChecked()) fee.Status = Gc.INACTIVE;

        if ("".equalsIgnoreCase(Strings.nullToEmpty(fee.Status))) {
            Config.responseSnackBarHandler(getString(R.string.status),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }

        fee.FeeType = et_fee_type_name.getText().toString().toUpperCase().trim();
        if ("".equalsIgnoreCase(Strings.nullToEmpty(fee.FeeType))) {
            Config.responseSnackBarHandler(getString(R.string.status),
                    findViewById(R.id.top_layout), R.color.fragment_first_blue);
            return false;
        }
        return true;
    }

}
