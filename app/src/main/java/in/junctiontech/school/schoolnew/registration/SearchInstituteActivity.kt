package `in`.junctiontech.school.schoolnew.registration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.BuildConfig
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.OrganizationKeyEnter
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class SearchInstituteActivity : AppCompatActivity() {
    private lateinit var progressBar: LinearLayout

    private var code: String? = null
    private var countryName: String? = null
    private var stateName: String? = null
    private var cityName: String? = null
    private lateinit var searchInstitute: EditText
    private lateinit var myInstituteNotListedButton: Button
    lateinit var adapter: SearchInstituteActivityAdapter
    lateinit var mRecyclerView: RecyclerView

    private val searchOrganizationList = ArrayList<SearchOrganization>()
    private val organizationList = ArrayList<SearchOrganization>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_institute_search)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        code = intent.getStringExtra("code")
        countryName = intent.getStringExtra("countryName")
        stateName = intent.getStringExtra("stateName")
        cityName = intent.getStringExtra("cityName")

        initializeViews()

        if (code!!.isNotEmpty() && countryName!!.isNotEmpty()) {
            searchInstitute()
        }

    }

    private fun searchInstitute() {

        progressBar.visibility = View.VISIBLE

        val p = JSONObject()
        p.put("country", countryName)
        p.put("state", stateName)
        p.put("city", cityName)

        val url = (Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "registration/searchOrganization/" + p)

        Log.e("url", url)
        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val searchOrganizationDetails = Gson()
                            .fromJson<ArrayList<SearchOrganization>>(job.getJSONObject("result").optString("searchOrganizationDetails"), object : TypeToken<List<SearchOrganization>>() {

                            }.type)
                    searchOrganizationList.clear()
                    organizationList.clear()
                    organizationList.addAll(searchOrganizationDetails)
                    searchOrganizationList.addAll(searchOrganizationDetails)
                    adapter.notifyDataSetChanged()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.top_layout), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this@SearchInstituteActivity, error, findViewById<View>(R.id.top_layout))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun initializeViews() {

        progressBar = findViewById(R.id.progressLayout)


        myInstituteNotListedButton = findViewById(R.id.my_institute_not_listed_button)
        if (BuildConfig.APPLICATION_ID.equals("com.zeroerp.itutor",true)){
            myInstituteNotListedButton.visibility = View.GONE
        }
        searchInstitute = findViewById(R.id.search_institute)

        searchInstitute.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                filterInstitute(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })

        myInstituteNotListedButton.setOnClickListener {
            val intent = Intent(this@SearchInstituteActivity, PhoneVerificationActivity::class.java)
            intent.putExtra("code", code)
            intent.putExtra("countryName", countryName)
            intent.putExtra("stateName", stateName)
            intent.putExtra("cityName", cityName)
            startActivity(intent)
        }

        mRecyclerView = findViewById(R.id.rv_institute_list)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = layoutManager

        adapter = SearchInstituteActivityAdapter()
        mRecyclerView.adapter = adapter
    }

    private fun filterInstitute(text: String) {

        searchOrganizationList.clear()
        if (text.isEmpty()) {
            searchOrganizationList.addAll(organizationList)
        } else {
            val texts = text.toLowerCase(Locale.getDefault())
            for (s in organizationList) {
                if (s.organization_name.toString().toLowerCase(Locale.getDefault()).contains(texts)) {
                    searchOrganizationList.add(s)
                }
            }
        }
        adapter.notifyDataSetChanged()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    inner class SearchInstituteActivityAdapter : RecyclerView.Adapter<SearchInstituteActivityAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_for_search_institute, parent, false)

            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            holder.tvItemInstituteName.text = searchOrganizationList[position].organization_name

            if ("" != searchOrganizationList[position].logo) Glide.with(this@SearchInstituteActivity)
                    .load(searchOrganizationList[position].logo)
                    .apply(RequestOptions()
                            .error(R.drawable.ic_junction)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(holder.tvItemInstituteLogo) else holder.tvItemInstituteLogo.setImageDrawable(getDrawable(R.drawable.ic_junction))

        }

        override fun getItemCount(): Int {
            return searchOrganizationList.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tvItemInstituteName: TextView = itemView.findViewById(R.id.tv_item_institute_name)


            var tvItemInstituteLogo: CircleImageView = itemView.findViewById(R.id.tv_item_institute_logo)

            init {
                tvItemInstituteLogo.visibility = View.VISIBLE

                itemView.setOnClickListener {

                    val position = adapterPosition

                    val intent = Intent(this@SearchInstituteActivity, OrganizationKeyEnter::class.java)
                            .putExtra("organizationKeyAvailable", true)
                            .putExtra("organizationKey", searchOrganizationList[position].organizationKey.toString())
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}

class SearchOrganization {
    var organizationKey: String? = null
    var organization_name: String? = null
    var logo: String? = null
}