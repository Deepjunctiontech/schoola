package in.junctiontech.school.schoolnew.exam;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

import static in.junctiontech.school.schoolnew.common.Gc.GRADE;

public class ScholasticGradeActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    FloatingActionButton fb_scholastic_grades;
    RecyclerView mRecyclerView;


    ArrayList<MasterEntryEntity> scholasticGradeList = new ArrayList<>();

    ScholasticGradeAdapter adapter;


    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_scholastic_grades.setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholastic_grade);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        fb_scholastic_grades = findViewById(R.id.fb_scholastic_grades);
        mRecyclerView = findViewById(R.id.rv_scholastic_grades);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ScholasticGradeAdapter();
        mRecyclerView.setAdapter(adapter);

        fb_scholastic_grades.setOnClickListener(view -> {
                showDialogForGradeCreate();
        });

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/7949294551",this,adContainer);
        }
    }

    void showDialogForGradeCreate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.create_scholastic_grades);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_grade_name = view.findViewById(R.id.et_subject_name);
        et_grade_name.setHint(R.string.grade);
        et_grade_name.setFilters(new InputFilter[] { new InputFilter.LengthFilter(4) });
        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_grade_name.getText().toString().trim().isEmpty()){
                et_grade_name.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {
                createGrade(et_grade_name.getText().toString().trim().toUpperCase());
                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }

    void showDialogForGradeUpdate(MasterEntryEntity masterEntryEntity){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.create_scholastic_grades);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_grade_name = view.findViewById(R.id.et_subject_name);
        et_grade_name.setHint(R.string.grade);
        et_grade_name.setFilters(new InputFilter[] { new InputFilter.LengthFilter(4) });
        et_grade_name.setText(masterEntryEntity.MasterEntryValue);


        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_grade_name.getText().toString().trim().isEmpty()){
                et_grade_name.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {
                try {
                    updateGrade(et_grade_name.getText().toString().trim().toUpperCase(), masterEntryEntity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }


    void createGrade(final String grade){


        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();

        progressBar.setVisibility(View.VISIBLE);
        try {
            p.put("MasterEntryStatus", Gc.ACTIVE);
            p.put("MasterEntryName",GRADE);
            p.put("MasterEntryValue",grade);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

        param.put(p);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<MasterEntryEntity> masterEntryEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("masterEntry"),new TypeToken<List<MasterEntryEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.masterEntryModel().insertMasterEntries(masterEntryEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                        Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ScholasticGradeActivity.this);

                    Intent intent1 = new Intent(ScholasticGradeActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ScholasticGradeActivity.this);

                    Intent intent2 = new Intent(ScholasticGradeActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ScholasticGradeActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void updateGrade(final String grade, MasterEntryEntity masterEntry) throws JSONException {


//        final JSONArray param = new JSONArray();
        final JSONObject param = new JSONObject();

        progressBar.setVisibility(View.VISIBLE);
        try {
            param.put("MasterEntryStatus", Gc.ACTIVE);
            param.put("MasterEntryName",GRADE);
            param.put("MasterEntryValue",grade);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

//        param.put(p);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/"
                + new JSONObject().put("MasterEntryId",masterEntry.MasterEntryId)
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<MasterEntryEntity> masterEntryEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("masterEntry"),new TypeToken<List<MasterEntryEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.masterEntryModel().insertMasterEntries(masterEntryEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ScholasticGradeActivity.this);

                    Intent intent1 = new Intent(ScholasticGradeActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ScholasticGradeActivity.this);

                    Intent intent2 = new Intent(ScholasticGradeActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ScholasticGradeActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();

        mDb.masterEntryModel().getMasterEntryValuesGeneric(GRADE).observe(this, masterEntryEntities -> {
            if (masterEntryEntities == null || masterEntryEntities.size() == 0) {
                Config.responseSnackBarHandler(getString(R.string.create_scholastic_grades),
                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                return;
            }
            scholasticGradeList.clear();
            scholasticGradeList.addAll(masterEntryEntities);
            adapter.notifyDataSetChanged();

        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.masterEntryModel().getMasterEntryValuesGeneric(GRADE).removeObservers(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class ScholasticGradeAdapter extends RecyclerView.Adapter<ScholasticGradeAdapter.MyViewHolder>{

        @NonNull
        @Override
        public ScholasticGradeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ScholasticGradeAdapter.MyViewHolder holder, int position) {
            holder.tv_single_item.setText(scholasticGradeList.get(position).MasterEntryValue);
        }

        @Override
        public int getItemCount() {
            return scholasticGradeList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);

                itemView.setOnClickListener(v -> {
                    showDialogForGradeUpdate(scholasticGradeList.get(getAdapterPosition()));
                });
            }
        }
    }
}
