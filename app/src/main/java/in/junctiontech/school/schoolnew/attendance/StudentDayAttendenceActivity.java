package in.junctiontech.school.schoolnew.attendance;

import android.app.DatePickerDialog;
import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.Attendance;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.StudentAttendanceModel;

public class StudentDayAttendenceActivity extends AppCompatActivity {
    MainDatabase mDb;

    ProgressBar progressBar;

    Button btn_select_class;
    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    ArrayList<String> sectionList = new ArrayList<>();
    int selectedSection;
    String selectedSectionId;
    String selectedDate ;

    int selectAll = 0;

    Button btn_select_date;


    private RecyclerView rv_student_day_attendance;
    private StudentAttendanceListAdapter madapter;
    private ArrayList<Attendance> attendanceArrayList = new ArrayList();

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_select_date.setBackgroundColor(colorIs);
        btn_select_class.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_day_attendence);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        selectClassButton();
        selectAttendanceDate();
        setuprecycler();

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/8927813238", this, adContainer);
        }
    }

    void setuprecycler(){
        rv_student_day_attendance = findViewById(R.id.rv_student_day_attendance);

        madapter = new StudentAttendanceListAdapter(this, attendanceArrayList);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            rv_student_day_attendance.setLayoutManager(gridLayoutManager);
        } else {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);

            rv_student_day_attendance.setLayoutManager(gridLayoutManager);
        }

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        rv_student_day_attendance.setItemAnimator(itemAnimator);
        rv_student_day_attendance.setAdapter(madapter);

    }

    void selectAttendanceDate(){
        final Calendar myCalendar = Calendar.getInstance();

        btn_select_date = findViewById(R.id.btn_select_date);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);

                btn_select_date.setText(sdf.format(myCalendar.getTime()));

                SimpleDateFormat sdfeng = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                selectedDate = sdfeng.format(myCalendar.getTime());

                try {
                    fetchAttendenceAndDisplay(selectedSection);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };

        btn_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(StudentDayAttendenceActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    void selectClassButton(){
        btn_select_class = findViewById(R.id.btn_select_class);
        classListBuilder = new AlertDialog.Builder(StudentDayAttendenceActivity.this);
        classSectionAdapter =
                new ArrayAdapter<String>(StudentDayAttendenceActivity.this, android.R.layout.select_dialog_singlechoice);

        btn_select_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classListBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                classListBuilder.setAdapter(classSectionAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        btn_select_class.setText(classNameSectionList.get(i).ClassName + " "
                                + classNameSectionList.get(i).SectionName);
                        selectedSection = i;
                        selectedSectionId = classNameSectionList.get(i).SectionId;
                        try {
                            fetchAttendenceAndDisplay(selectedSection);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                AlertDialog dialog = classListBuilder.create();
                dialog.show();
            }
        });
    }

    void fetchAttendenceAndDisplay(int selectedSection) throws JSONException {
        if (btn_select_class.getText().toString().equals(getString(R.string.select_class))){
            Config.responseSnackBarHandler(getString(R.string.select_class),
                    findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_blue);
            return;
        }

        if (btn_select_date.getText().toString().equals(getString(R.string.select_date))){
            Config.responseSnackBarHandler(getString(R.string.select_date),
                    findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_blue);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        String selectedsectionid = classNameSectionList.get(selectedSection).SectionId;
        final JSONObject urlparam = new JSONObject();

        urlparam.put("SectionID",selectedsectionid);

        urlparam.put("Date",selectedDate);

//        p.put("Frequency", "Monthly");

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "attendance/studentAttendanceEntry/"
                + urlparam
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");
                            ArrayList<StudentAttendanceModel> sa = new Gson()
                                    .fromJson(resultjobj.getString("studentAttendance"),new TypeToken<List<StudentAttendanceModel>>(){}
                                            .getType());

                            displayAttendanceforDay(sa);


                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_green);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;


                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StudentDayAttendenceActivity.this);

                        Intent intent1 = new Intent(StudentDayAttendenceActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StudentDayAttendenceActivity.this);

                        Intent intent2 = new Intent(StudentDayAttendenceActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    case Gc.APIRESPONSE204:
                        attendanceArrayList.clear();
                        madapter.notifyDataSetChanged();

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_blue);
                        break;

                    default:
                        attendanceArrayList.clear();
                        madapter.notifyDataSetChanged();
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(StudentDayAttendenceActivity.this,error,findViewById(R.id.cl_student_day_attendance));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this)).observe(this, new Observer<List<ClassNameSectionName>>() {
            @Override
            public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                classNameSectionList.clear();
                sectionList.clear();
                classSectionAdapter.clear();
                classNameSectionList.addAll(classNameSectionNames);
                for(int i = 0; i < classNameSectionList.size(); i++){
                    sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
                }
                classSectionAdapter.addAll(sectionList);
                classSectionAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this))
                .removeObservers(this);
    }

    void displayAttendanceforDay(ArrayList<StudentAttendanceModel> sa){
        attendanceArrayList.clear();
        for ( int i = 0; i <sa.size(); i++){
            Attendance a = new Attendance(sa.get(i).AdmissionID
                    ,sa.get(i).StudentName
                    ,sa.get(i).Attendance[0].AttStatus
                    , sa.get(i).ProfileImage, sa.get(i).AdmissionNo);
            attendanceArrayList.add(a);
        }
        madapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sort_and_search, menu);

        menu.findItem(R.id.menu_action_search).setVisible(false);
        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_help).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.menu_sort_by_alphabetical:
                sortAlphabatically();
                break;

            case R.id.menu_sort_by_number:
                sortByAdmissionID();
                break;

            case R.id.action_help:
                showHelp();
                break;

            case R.id.action_select_all:
                selectUnSelectAll();

                break;

            case R.id.action_save:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(getString(R.string.send_notification) + "??");
                alert1.setTitle(getString(R.string.fill_attendance));
                alert1.setIcon(R.drawable.ic_attendance);
                alert1.setPositiveButton(getString(R.string.save_and_send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveAttendance(true);
                    }
                });
                alert1.setNegativeButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveAttendance(false);
                    }
                });
//                alert1.setCancelable(false);
                alert1.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            rv_student_day_attendance.setLayoutManager(gridLayoutManager);
            madapter.notifyDataSetChanged();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            rv_student_day_attendance.setLayoutManager(gridLayoutManager);
            madapter.notifyDataSetChanged();
        }
    }

    void selectUnSelectAll(){
        if(attendanceArrayList.size()<1)return; // if its empty , dont do anything
        switch(selectAll){
            case 0:
                for(Attendance a: attendanceArrayList){
                    a.presentStatus = "P";
                }
                madapter.notifyDataSetChanged();
                selectAll =1;
                break;
            case 1:
                for(Attendance a: attendanceArrayList){
                    a.presentStatus = "A";
                }
                madapter.notifyDataSetChanged();
                selectAll = 2;
                break;
            case 2:
                for(Attendance a: attendanceArrayList){
                    a.presentStatus = "";
                }
                madapter.notifyDataSetChanged();
                selectAll=0;
            break;
        }
    }

    void sortAlphabatically(){
        if(attendanceArrayList.size()<1)return;
        Collections.sort(attendanceArrayList, new Comparator<Attendance>() {
            @Override
            public int compare(Attendance item, Attendance t1) {
                String s1 = item.getStudentName();
                String s2 = t1.getStudentName();
                return s1.compareToIgnoreCase(s2);
            }

        });
        madapter.notifyDataSetChanged();
    }

    void sortByAdmissionID(){
        if(attendanceArrayList.size()<1)return;
        Collections.sort(attendanceArrayList, new Comparator<Attendance>() {
            @Override
            public int compare(Attendance item, Attendance t1) {
                String s1 = item.getAdmissionID();
                String s2 = t1.getAdmissionID();
                return s1.compareToIgnoreCase(s2);
            }

        });
        madapter.notifyDataSetChanged();
    }

    void showHelp(){
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


        alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                ":<font color='#ce001f'>Again</font>"));
        alert1.setTitle(getString(R.string.help));
        alert1.setIcon(R.drawable.ic_help);
        alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
//            alert1.setCancelable(false);
        alert1.show();
    }

    JSONObject  buildJsonForSave(Boolean sendData)  {
        JSONObject param = new JSONObject();

        try {

        JSONArray inner = new JSONArray();
        for(Attendance a : attendanceArrayList){
            inner.put(new JSONObject().put("admissionID",a.AdmissionID).put("attribute",a.presentStatus));
        }

        JSONArray outerarr = new JSONArray();
        JSONObject outerjob = new JSONObject();

            outerjob.put("Type","fullDay");

        outerjob.put("SectionId", selectedSectionId);
        if(sendData)
        outerjob.put("send","true");

        outerjob.put("Date", btn_select_date.getText().toString());
        outerjob.put("StudentData",inner);

        outerarr.put(outerjob);

        param.put("userName", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this));
        param.put("AttendanceData", outerarr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;

    }

    void saveAttendance(Boolean sendData)  {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = buildJsonForSave(sendData);

                final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "attendance/studentAttendanceEntry"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_green);

                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StudentDayAttendenceActivity.this);

                        Intent intent1 = new Intent(StudentDayAttendenceActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StudentDayAttendenceActivity.this);

                        Intent intent2 = new Intent(StudentDayAttendenceActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.cl_student_day_attendance),R.color.fragment_first_blue);
                    Log.e("Response",job.optString("message"));
                }
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(StudentDayAttendenceActivity.this,error,findViewById(R.id.cl_student_day_attendance));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
