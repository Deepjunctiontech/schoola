package in.junctiontech.school.schoolnew.adminpanel;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 10-08-2017.
 */

public class UserActionModel implements Serializable {
String activityName;
    int iconName, name, tintColor;
    boolean tintNeeded;

    public UserActionModel(int name, int iconName, boolean tintNeeded, int tintColor,String activityName) {
        this.name = name;
        this.iconName = iconName;
        this.tintNeeded = tintNeeded;
        this.tintColor = tintColor;
        this.activityName = activityName;

    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getTintColor() {
        return tintColor;
    }

    public void setTintColor(int tintColor) {
        this.tintColor = tintColor;
    }

    public boolean isTintNeeded() {
        return tintNeeded;
    }

    public void setTintNeeded(boolean tintNeeded) {
        this.tintNeeded = tintNeeded;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getIconName() {
        return iconName;
    }

    public void setIconName(int iconName) {
        this.iconName = iconName;
    }
}
