package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SchoolClassSectionDao {

    @Query("Select * from SchoolClassSectionEntity")
    LiveData<List<SchoolClassSectionEntity>> getAllSection();

    @Query("Select * from SchoolClassSectionEntity where ClassId LIKE :classid")
    LiveData<List<SchoolClassSectionEntity>> getClassSectionLive(String classid);

    @Query("Select * from SchoolClassSectionEntity where ClassId LIKE :classid")
    List<SchoolClassSectionEntity> getClassSections(String classid);


    @Query("Select SectionName from SchoolClassSectionEntity where ClassId LIKE :classid")
    List<String> getSectionName(String classid);

    @Query("Select SectionId from SchoolClassSectionEntity where SectionName LIKE :sectionName limit 1")
    String getSectionId(String sectionName);

    @Query("Select ClassId from SchoolClassSectionEntity where SectionName LIKE :sectionName limit 1")
    String getClassId(String sectionName);

    @Query("SELECT SchoolClassSectionEntity.*,ClassName FROM SchoolClassSectionEntity,SchoolClassEntity " +
            "where SchoolClassSectionEntity.ClassId=SchoolClassEntity.ClassId and Session like :session order by ClassName , SectionName")
    LiveData<List<ClassNameSectionName>> getClassNameSectionList(String session);

    @Query("SELECT SchoolClassSectionEntity.*,ClassName FROM SchoolClassSectionEntity,SchoolClassEntity " +
            "where SchoolClassSectionEntity.ClassId=SchoolClassEntity.ClassId and SectionId like :sectionid")
    LiveData<ClassNameSectionName> getClassSectionNameSingle(String sectionid);

    @Insert(onConflict = REPLACE)
    void insertSchoolClassSection(List<SchoolClassSectionEntity> sectionEntity);

    @Query("delete from SchoolClassSectionEntity")
    void deleteAll();

    @Query("delete from SchoolClassSectionEntity where ClassId LIKE :classid")
    void deleteClassSections(String classid);

    @Query("delete from SchoolClassSectionEntity where SectionId LIKE :sectionId")
    void deleteSections(String sectionId);

    @Update
    void updateSchoolClassSection(SchoolClassSectionEntity sectionEntity);


}
