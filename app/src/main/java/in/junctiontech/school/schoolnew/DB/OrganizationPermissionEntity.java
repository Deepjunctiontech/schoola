package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class OrganizationPermissionEntity {
    @PrimaryKey
    @NonNull
    public String module;

    public OrganizationPermissionEntity(String module){
        this.module = module;
    }

}
