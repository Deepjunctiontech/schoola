package in.junctiontech.school.schoolnew.exam;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ExamTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.DB.examdb.ScholasticResultEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.subject.SubjectActivity;

public class EnterExamResultActivity extends AppCompatActivity {

    MainDatabase mDb ;
    ProgressBar progressBar;
    Button btn_select_class;
    Button btn_select_subject;
    Button btn_select_exam;
    Button btn_max_marks;

    ArrayList<String> sectionList = new ArrayList<>();
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    int selectedSection;

    AlertDialog.Builder subjectListBuilder;
    ArrayAdapter<String> subjectAdapter;
    ArrayList<String> subjectList = new ArrayList<>();
    private ArrayList<SchoolSubjectsEntity> schoolSubjects = new ArrayList<>();
    int selectedSubject;


    ArrayList<String> examTypeList = new ArrayList<>();
    ArrayList<ExamTypeEntity> examTypeEntities = new ArrayList<>();
    String selecteExamType;
    int selecteExamTypeIndex;

    ArrayAdapter<String>  examTypeListAdapter;
    AlertDialog.Builder examTypeListBuilder;

    ArrayList<GradeMarksEntity> gradeMarksList = new ArrayList<>();
    ArrayList<String> maxMarksList = new ArrayList<>();
    String selectedMaxMarks;

    ArrayAdapter<String>  maxMarksListAdapter;
    AlertDialog.Builder maxMarksListBuilder;

    ArrayList<ScholasticResultEntity> scholasticResultNotFilled = new ArrayList<>();


    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_select_class.setBackgroundColor(colorIs);
        btn_select_subject.setBackgroundColor(colorIs);
        btn_select_exam.setBackgroundColor(colorIs);
        btn_max_marks.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_exam_result);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);

        progressBar = findViewById(R.id.progressBar);

        btn_select_subject = findViewById(R.id.btn_select_subject);
        btn_select_class = findViewById(R.id.btn_select_class);
        btn_max_marks = findViewById(R.id.btn_max_marks);

        selectMaxMarks();

        selectClassForEnterResult();

        selectSubjectForassignment();

        initializeExamTypeButton();

        mDb.examTypeModel().getExamTypes().observe(this, examTypes -> {
            examTypeList.clear();
            examTypeEntities.clear();
            examTypeListAdapter.clear();

            for (ExamTypeEntity e: examTypes){
                examTypeList.add(e.Exam_Type);
            }
            examTypeEntities.addAll(examTypes);
            examTypeListAdapter.addAll(examTypeList);
            examTypeListAdapter.notifyDataSetChanged();
        });

        getMaxMarksGradeFromServer();
        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/5594593959", this, adContainer);
        }
    }

    void selectMaxMarks(){

        maxMarksListBuilder  = new AlertDialog.Builder(EnterExamResultActivity.this);
        maxMarksListAdapter =
                new ArrayAdapter<>(EnterExamResultActivity.this, android.R.layout.select_dialog_singlechoice);
        maxMarksListAdapter.addAll(maxMarksList);

        btn_max_marks.setOnClickListener(view -> {
            maxMarksListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            maxMarksListBuilder.setAdapter(maxMarksListAdapter, (dialogInterface, i) -> {
                if (btn_select_class.getText().equals(getString(R.string.select_class))){
                    Config.responseSnackBarHandler(getResources().getString(R.string.select_class),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                    return;
                }
                if ( btn_select_subject.getText().equals(getString(R.string.select_subject))){
                    Config.responseSnackBarHandler(getResources().getString(R.string.select_subject),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                    return;
                }
                if( btn_select_exam.getText().equals(getString(R.string.exam_type_list))){
                    Config.responseSnackBarHandler(getResources().getString(R.string.exam_type),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                    return;
                }
                btn_max_marks.setText(maxMarksList.get(i));

                Intent intent = new Intent(EnterExamResultActivity.this, EnterMarksForClass.class);
                intent.putExtra("sectionid", classNameSectionList.get(selectedSection).SectionId);
                intent.putExtra("classsectionname", classNameSectionList.get(selectedSection).ClassName + classNameSectionList.get(selectedSection).SectionName);
                intent.putExtra("subjectid", schoolSubjects.get(selectedSubject).SubjectId);
                intent.putExtra("subjectname", schoolSubjects.get(selectedSubject).SubjectName);
                intent.putExtra("examtype", btn_select_exam.getText());
                intent.putExtra("examtypeid", examTypeEntities.get(selecteExamTypeIndex).ExamTypeID);
                intent.putExtra("result", new Gson().toJson(scholasticResultNotFilled));

                intent.putExtra("maxmarks", btn_max_marks.getText());

                startActivity(intent);

            });

            AlertDialog dialog = maxMarksListBuilder.create();
            dialog.show();

        });
    }

    void getMaxMarksGradeFromServer(){

        progressBar.setVisibility(View.VISIBLE);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/gradeMarks"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), jsonObject -> {
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

            switch (code){

                case Gc.APIRESPONSE200:

                    try {
                        ArrayList<GradeMarksEntity> gradeMarksEntities = new Gson()
                                .fromJson(jsonObject.getJSONObject("result").optString("gradeMarks"),new TypeToken<List<GradeMarksEntity>>(){}.getType());

                        if (gradeMarksEntities != null){
                            maxMarksList.clear();
                            for (GradeMarksEntity g : gradeMarksEntities){
                                if (!(maxMarksList.contains(g.maxMarks))){
                                    maxMarksList.add(g.maxMarks);
                                }
                            }

                            new Thread(() -> {
                                mDb.gradeMarksModel().deleteAll();
                                mDb.gradeMarksModel().insertGradeMarks(gradeMarksEntities);
                            }).start();

                            maxMarksListAdapter.clear();
                            maxMarksListAdapter.addAll(maxMarksList);
                            maxMarksListAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, EnterExamResultActivity.this);

                    Intent intent1 = new Intent(EnterExamResultActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, EnterExamResultActivity.this);

                    Intent intent2 = new Intent(EnterExamResultActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    progressBar.setVisibility(View.GONE);
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(EnterExamResultActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void getMarksForClassSubjectExam(){
        progressBar.setVisibility(View.VISIBLE);

        JSONObject urlparam = new JSONObject();
        try {
            urlparam.put("Section_Id", classNameSectionList.get(selectedSection).SectionId);
            urlparam.put("Subject_Id",schoolSubjects.get(selectedSubject).SubjectId );
            urlparam.put("Exam_Type", examTypeEntities.get(selecteExamTypeIndex).ExamTypeID);
            urlparam.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/resultEntryScholastic/"
                + urlparam
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), jsonObject -> {
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

            switch (code){

                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<ScholasticResultEntity> scholasticResultEntities = new Gson()
                                .fromJson(jsonObject.getJSONObject("result").optString("resultEntryScholastic"),
                                        new TypeToken<List<ScholasticResultEntity>>(){}.getType());

                        if (scholasticResultEntities != null && scholasticResultEntities.size() > 0){

                            String maxMarksFilled = "" ;
                            for (ScholasticResultEntity s : scholasticResultEntities){
                                if (s.Max_Marks != null){
                                    maxMarksFilled = s.Max_Marks;
                                }
                            }
                            if ("".equalsIgnoreCase(maxMarksFilled)){
                                scholasticResultNotFilled.clear();
                                scholasticResultNotFilled.addAll(scholasticResultEntities);
                                btn_max_marks.setVisibility(View.VISIBLE);
                            }else{
                                btn_max_marks.setVisibility(View.GONE);

                                Intent intent = new Intent(EnterExamResultActivity.this, EnterMarksForClass.class);
                                intent.putExtra("sectionid", classNameSectionList.get(selectedSection).SectionId);
                                intent.putExtra("classsectionname", classNameSectionList.get(selectedSection).ClassName + classNameSectionList.get(selectedSection).SectionName);
                                intent.putExtra("subjectid", schoolSubjects.get(selectedSubject).SubjectId);
                                intent.putExtra("subjectname", schoolSubjects.get(selectedSubject).SubjectName);
                                intent.putExtra("examtype", btn_select_exam.getText());
                                intent.putExtra("examtypeid", examTypeEntities.get(selecteExamTypeIndex).ExamTypeID);
                                intent.putExtra("result", new Gson().toJson(scholasticResultEntities));

                                intent.putExtra("maxmarks", maxMarksFilled);

                                startActivity(intent);
                            }


                        }else{
                            Config.responseSnackBarHandler(getString(R.string.students_not_available),
                                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, EnterExamResultActivity.this);

                    Intent intent1 = new Intent(EnterExamResultActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, EnterExamResultActivity.this);

                    Intent intent2 = new Intent(EnterExamResultActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    progressBar.setVisibility(View.GONE);
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(EnterExamResultActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void initializeExamTypeButton(){

        btn_select_exam = findViewById(R.id.btn_select_exam);

        examTypeListBuilder = new AlertDialog.Builder(EnterExamResultActivity.this);
        examTypeListAdapter =
                new ArrayAdapter<>(EnterExamResultActivity.this, android.R.layout.select_dialog_singlechoice);

        examTypeListAdapter.addAll(examTypeList);
        btn_select_exam.setOnClickListener(view -> {
            examTypeListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            examTypeListBuilder.setAdapter(examTypeListAdapter, (dialogInterface, i) -> {
                btn_select_exam.setText(examTypeList.get(i));
                selecteExamType = examTypeList.get(i);
                selecteExamTypeIndex = i;

                getMarksForClassSubjectExam();

//                btn_max_marks.setVisibility(View.VISIBLE);

            });

            resetSelection(2);
            AlertDialog dialog = examTypeListBuilder.create();
            dialog.show();

        });
    }

    void selectSubjectForassignment(){
        subjectListBuilder = new AlertDialog.Builder(this);
        subjectAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice );
        subjectAdapter.addAll(subjectList);

        btn_select_subject.setOnClickListener(view -> {
            if(btn_select_class.getText().equals(getResources().getString(R.string.select_class))){
                Config.responseSnackBarHandler(getResources().getString(R.string.select_class),
                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
                return;
            }
            subjectListBuilder.setNegativeButton(R.string.cancel, (DialogInterface dialogInterface, int i) -> {
                dialogInterface.dismiss();
            });

            subjectListBuilder.setAdapter(subjectAdapter, (dialogInterface, i) -> {
                btn_select_subject.setText(subjectList.get(i));
                selectedSubject = i;
                btn_select_exam.setVisibility(View.VISIBLE);
            });

            resetSelection(1);
            AlertDialog dialog = subjectListBuilder.create();
            dialog.show();
        });
    }

    private void selectClassForEnterResult(){
        classListBuilder = new AlertDialog.Builder(EnterExamResultActivity.this);
        classSectionAdapter =
                new ArrayAdapter<>(EnterExamResultActivity.this, android.R.layout.select_dialog_singlechoice);

        classSectionAdapter.addAll(sectionList);
        btn_select_class.setOnClickListener(view -> {
            classListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            classListBuilder.setAdapter(classSectionAdapter, (dialogInterface, i) -> {
                btn_select_class.setText(sectionList.get(i));
                selectedSection = i;
                resetSelection(0); // reset subject and exam when class changes

                btn_select_subject.setVisibility(View.VISIBLE);

                mDb.schoolSubjectsClassModel()
                        .getSubjectsForSection(classNameSectionList.get(i).SectionId)
                        .observe(this, new Observer<List<SchoolSubjectsEntity>>() {
                            @Override
                            public void onChanged(@Nullable List<SchoolSubjectsEntity> schoolSubjectsEntities) {
                                if(Objects.requireNonNull(schoolSubjectsEntities).size()<1){

                                    AlertDialog.Builder alert1 = new AlertDialog.Builder(EnterExamResultActivity.this);
                                    alert1.setTitle(getString(R.string.subject_for_classes) + getString(R.string.not_available));
                                    alert1.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));
                                    alert1.setPositiveButton(R.string.ok, (dialog, which) -> {
                                        Intent intent = new Intent(EnterExamResultActivity.this, SubjectActivity.class);
                                        startActivity(intent);
                                        finish();
                                    });
                                    alert1.setMessage(getString(R.string.assign_subject_to_class));
                                    //  alert.setCancelable(false);
                                    alert1.show();

                                    return;
                                }

                                schoolSubjects.clear();
                                subjectList.clear();
                                schoolSubjects.addAll(schoolSubjectsEntities);
                                for(SchoolSubjectsEntity s: schoolSubjects){
                                    subjectList.add(s.SubjectName);
                                }
                                subjectAdapter.clear();
                                subjectAdapter.addAll(subjectList);
                                subjectAdapter.notifyDataSetChanged();
                                mDb.schoolSubjectsClassModel()
                                        .getSubjectsForSection(classNameSectionList.get(i).SectionId)
                                        .removeObserver(this);
                            }
                        });


            });

            AlertDialog dialog = classListBuilder.create();
            dialog.show();

        });

        classNameSectionList.clear();
        sectionList.clear();

        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this))
                .observe(this, classNameSectionNames -> {
            classNameSectionList.clear();
            sectionList.clear();
            classSectionAdapter.clear();
            classNameSectionList.addAll(classNameSectionNames);
            for(int i = 0; i < classNameSectionList.size(); i++){
                sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
            }
            classSectionAdapter.addAll(sectionList);
            classSectionAdapter.notifyDataSetChanged();


        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void resetSelection(int i){
        switch (i){
            case 0:
                btn_select_subject.setText(R.string.select_subject);
                btn_select_exam.setText(R.string.exam_type_list);
                btn_max_marks.setText(R.string.enter_maximum_marks);
                break;

            case 1:
                btn_select_exam.setText(R.string.exam_type_list);
                btn_max_marks.setText(R.string.enter_maximum_marks);
                break;

            case 2:
                btn_max_marks.setText(R.string.enter_maximum_marks);
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        btn_max_marks.setText(R.string.enter_maximum_marks);
        btn_max_marks.setVisibility(View.GONE);
        btn_select_exam.setText(R.string.exam_type_list);
    }
}
