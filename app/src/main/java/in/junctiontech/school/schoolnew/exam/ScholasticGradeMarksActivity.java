package in.junctiontech.school.schoolnew.exam;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.messenger.StartNewConversationActivity;

import static in.junctiontech.school.schoolnew.common.Gc.GRADE;

public class ScholasticGradeMarksActivity extends AppCompatActivity {

    MainDatabase mDb;

    ProgressBar progressBar;
    FloatingActionButton fb_scholastic_grade_marks;

    RecyclerView mRecyclerView;
    GradeMarksAdapter adapter;

    ArrayList<String> maxMarksList = new ArrayList<>();
    ArrayList<GradeMarksEntity> gradeMarkListAll = new ArrayList<>();

    String[] permissions;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_scholastic_grade_marks.setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholastic_grade_marks);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        fb_scholastic_grade_marks = findViewById(R.id.fb_scholastic_grade_marks);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        mRecyclerView = findViewById(R.id.rv_scholastic_grade_marks);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new GradeMarksAdapter();
        mRecyclerView.setAdapter(adapter);

        fb_scholastic_grade_marks.setOnClickListener((View view) -> {
            showDialogForMaxMarksCreate();
        });


        mDb.gradeMarksModel().getGradeMarksAll().observe(this, gradeMarksEntities -> {
            if (gradeMarksEntities != null){
                gradeMarkListAll.clear();
                maxMarksList.clear();

                gradeMarkListAll.addAll(gradeMarksEntities);

                for (GradeMarksEntity g : gradeMarksEntities){
                    if (!(maxMarksList.contains(g.maxMarks))){
                        maxMarksList.add(g.maxMarks);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        getMaxMarksGradeFromServer();

        setColorApp();

        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/7456957076",this,adContainer);
        }
    }

    void showDialogForMaxMarksCreate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.enter_maximum_marks);

        LayoutInflater inflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_max_marks = view.findViewById(R.id.et_subject_name);
        et_max_marks.setHint(R.string.maximum_marks);
        et_max_marks.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_max_marks.setFilters(new InputFilter[] { new InputFilter.LengthFilter(4) });

//        et_max_marks.setFilters(new InputFilter[]{(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) -> {
//            Character c = source.charAt(0);
//
//            if (Character.isDigit(c)){
//                return null;
//            }else {
//                return "";
//            }
//
//        }});
        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_max_marks.getText().toString().trim().isEmpty()){
                et_max_marks.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {

                if (maxMarksList.contains(et_max_marks.getText().toString().trim())){
                    wantToCloseDialog = true;

                    Config.responseSnackBarHandler(getString(R.string.already_exist),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }else {

                    Intent intent = new Intent(ScholasticGradeMarksActivity.this, CreateGradeMarksActivity.class);
                    intent.putExtra("action", "create");
                    intent.putExtra("maxmarks", et_max_marks.getText().toString().trim());
                    startActivityForResult(intent, 501);

                    wantToCloseDialog = true;
                }
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }

    void showAlertForGradeCreate(){
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

        alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.create_scholastic_grades) + "<Br>"));
        alert1.setTitle(getString(R.string.information));
        alert1.setIcon(R.drawable.ic_clickhere);
        alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

            startActivity(new Intent(ScholasticGradeMarksActivity.this, ScholasticGradeActivity.class));
            finish();


        });
        alert1.show();
        alert1.setCancelable(false);
        Config.responseSnackBarHandler(getString(R.string.create_scholastic_grades),
                findViewById(R.id.top_layout),R.color.fragment_first_blue);
    }


    void getMaxMarksGradeFromServer(){

        progressBar.setVisibility(View.VISIBLE);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/gradeMarks"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), jsonObject -> {
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

            switch (code){

                case Gc.APIRESPONSE200:

                    try {
                        ArrayList<GradeMarksEntity> gradeMarksEntities = new Gson()
                                .fromJson(jsonObject.getJSONObject("result").optString("gradeMarks"),new TypeToken<List<GradeMarksEntity>>(){}.getType());

                        if (gradeMarksEntities != null){
                            gradeMarkListAll.clear();
                            maxMarksList.clear();

                            gradeMarkListAll.addAll(gradeMarksEntities);

                            for (GradeMarksEntity g : gradeMarksEntities){
                                if (!(maxMarksList.contains(g.maxMarks))){
                                    maxMarksList.add(g.maxMarks);
                                }
                            }

                            new Thread(() -> {
                                mDb.gradeMarksModel().deleteAll();
                                mDb.gradeMarksModel().insertGradeMarks(gradeMarksEntities);
                            }).start();

                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ScholasticGradeMarksActivity.this);

                    Intent intent1 = new Intent(ScholasticGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ScholasticGradeMarksActivity.this);

                    Intent intent2 = new Intent(ScholasticGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    progressBar.setVisibility(View.GONE);
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(ScholasticGradeMarksActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


    public class GradeMarksAdapter extends RecyclerView.Adapter<GradeMarksAdapter.MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.tv_single_item.setText(maxMarksList.get(position));

        }

        @Override
        public int getItemCount() {
            return maxMarksList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);

                itemView.setOnClickListener(v -> {
                    permissions = new String[]{
                            getResources().getString(R.string.update),
//                                    context.getResources().getString(R.string.evaluate),
//                            getResources().getString(R.string.delete)
                            //                context.getResources().getString(R.string.assignment_submission)

                    };


                    android.app.AlertDialog.Builder choices = new android.app.AlertDialog.Builder(ScholasticGradeMarksActivity.this);

                    choices.setItems(permissions, (dialogInterface, which) -> {
                        switch (which) {
                            case 0:

                                Intent intent = new Intent(ScholasticGradeMarksActivity.this, CreateGradeMarksActivity.class);

                                intent.putExtra("maxmarks", maxMarksList.get(getAdapterPosition()));
                                ArrayList<GradeMarksEntity> gradeMarksForSelectMaxMark = new ArrayList<>();

                                for (GradeMarksEntity g: gradeMarkListAll ){
                                    if (g.maxMarks.equalsIgnoreCase(maxMarksList.get(getAdapterPosition())))
                                    gradeMarksForSelectMaxMark.add(g);
                                }

                                intent.putExtra("action", "update");
                                intent.putExtra("grademarkslist", new Gson().toJson(gradeMarksForSelectMaxMark));
                                startActivityForResult(intent, 501);

                                break;
                            case 1:



                                break;
                        }

                    });
                    choices.show();
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
