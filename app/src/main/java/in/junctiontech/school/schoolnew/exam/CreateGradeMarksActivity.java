package in.junctiontech.school.schoolnew.exam;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.examdb.GradeMarksEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class CreateGradeMarksActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    FloatingActionButton fb_scholastic_grade_marks_row;

    String action;

    ArrayList<MasterEntryEntity> scholasticGradeList = new ArrayList<>();
    ArrayList<String> gradeNameList = new ArrayList<>();
    ArrayList<String> gradeIdList = new ArrayList<>();

    ArrayList<GradeMarksEntity> gradeMarksForMaxMarks = new ArrayList<>();


    ArrayList<MasterEntryEntity> gradeResultMasterList = new ArrayList<>();
    ArrayList<String> resultNameList = new ArrayList<>();
    ArrayList<String> resultIdList = new ArrayList<>();

    String maxMarks;

    RecyclerView mRecyclerView;
    GradeMarksRangeAdapter adapter;

    String[] permissions;



    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_scholastic_grade_marks_row.setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_grade_marks);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        maxMarks = getIntent().getStringExtra("maxmarks");


        getSupportActionBar().setTitle(maxMarks);
        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);
        fb_scholastic_grade_marks_row = findViewById(R.id.fb_scholastic_grade_marks_row);
        fb_scholastic_grade_marks_row.setOnClickListener(v -> {

            int size = gradeMarksForMaxMarks.size();

            if (gradeMarksForMaxMarks.size() == 0){
                Config.responseSnackBarHandler(getString(R.string.zeroerp_email),
                        findViewById(R.id.top_layout),R.color.fragment_first_blue);

            }else {
                if (gradeMarksForMaxMarks.get(size -1).rangeEnd.equalsIgnoreCase("0")){

                    AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                    alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.first_modify_end_range_of_last_row) + "<Br>"));
                    alert1.setTitle(getString(R.string.information));
                    alert1.setIcon(R.drawable.ic_clickhere);
                    alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

                    });
                    alert1.show();
                    Config.responseSnackBarHandler(getString(R.string.first_modify_end_range_of_last_row),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }else{

                    int maxrows = gradeNameList.size();
                    GradeMarksEntity gradeMarksEntity = new GradeMarksEntity();
                    int newrownum = gradeMarksForMaxMarks.size() ;

                    if(newrownum >= maxrows){
                        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                        alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.all_grades_are_entered) + "<Br>"));
                        alert1.setTitle(getString(R.string.information));
                        alert1.setIcon(R.drawable.ic_clickhere);
                        alert1.setPositiveButton(getString(R.string.ok), (dialog, which) -> {

                        });
                        alert1.show();
                        Config.responseSnackBarHandler(getString(R.string.first_modify_end_range_of_last_row),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                        return;
                    }

                    gradeMarksEntity.gradeValue = gradeNameList.get(newrownum);
                    gradeMarksEntity.grade = gradeIdList.get(newrownum);
                    gradeMarksEntity.rangeStart = String.valueOf (Integer.parseInt(gradeMarksForMaxMarks.get(newrownum-1).rangeEnd) - 1);
                    gradeMarksEntity.rangeEnd = "0";
                    if (resultNameList.size() > 0){
                        gradeMarksEntity.gradeResult = resultIdList.get(0);
                        gradeMarksEntity.resultValue = resultNameList.get(0);
                    }
                    gradeMarksForMaxMarks.add(gradeMarksEntity);

                    adapter.notifyDataSetChanged();
                }

            }
        });


        action = Strings.nullToEmpty(getIntent().getStringExtra("action"));

        if (action.equalsIgnoreCase("")){
            Log.e("CreateGradeMarksActivit", "You Should not be here" + " in On Create");
            return;
        }

        mRecyclerView = findViewById(R.id.mRecyclerView);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new GradeMarksRangeAdapter();
        mRecyclerView.setAdapter(adapter);

        gradeResultMasterList = (ArrayList<MasterEntryEntity>) mDb.masterEntryModel().getMasterEntryValues(Gc.RESULT);
        if (gradeResultMasterList == null)
            return;


        resultNameList.clear();
        resultIdList.clear();
        for(MasterEntryEntity m : gradeResultMasterList){
            resultNameList.add(m.MasterEntryValue);
            resultIdList.add(m.MasterEntryId);
        }

            mDb.masterEntryModel().getMasterEntryValuesGeneric(Gc.GRADE).observe(this, masterEntryEntities -> {
            if (masterEntryEntities == null || masterEntryEntities.size() == 0){
                Intent intent = new Intent(CreateGradeMarksActivity.this, ScholasticGradeActivity.class);
                startActivity(intent);
                finish();

            }else{
                scholasticGradeList.clear();
                gradeNameList.clear();
                gradeIdList.clear();
                scholasticGradeList.addAll(masterEntryEntities);
                for (MasterEntryEntity m : scholasticGradeList){
                    gradeNameList.add(m.MasterEntryValue);
                    gradeIdList.add(m.MasterEntryId);
                }
                if (action.equalsIgnoreCase("create")){

                    GradeMarksEntity gradeMarksEntity = new GradeMarksEntity();
                    gradeMarksEntity.gradeValue = gradeNameList.get(0);
                    gradeMarksEntity.grade = gradeIdList.get(0);
                    gradeMarksEntity.rangeStart = maxMarks;
                    gradeMarksEntity.rangeEnd = "0";
                    if (resultNameList.size() > 0){
                        gradeMarksEntity.resultValue = resultNameList.get(0);
                        gradeMarksEntity.gradeResult = resultIdList.get(0);
                    }
                    gradeMarksForMaxMarks.add(gradeMarksEntity);

                    adapter.notifyDataSetChanged();

                }

            }
        });
        if (action.equalsIgnoreCase("create")) {

        }if (action.equalsIgnoreCase("update")) {

            gradeMarksForMaxMarks = new Gson()
                    .fromJson(getIntent().getStringExtra("grademarkslist"),
                            new TypeToken<List<GradeMarksEntity>>(){}.getType());
            adapter.notifyDataSetChanged();

            mDb.gradeMarksModel().getGradeMarks(maxMarks).observe(this, gradeMarksEntities -> {
                if (gradeMarksEntities != null){

                    Collections.sort(gradeMarksEntities, (o1, o2) -> {
                        if (Integer.parseInt(o1.rangeStart) ==
                                Integer.parseInt(o2.rangeStart))
                        {
                            return 0;
                        }
                        else if (Integer.parseInt(o1.rangeStart) >
                                Integer.parseInt(o2.rangeStart))
                        {
                            return -1;
                        }
                        return 1;
                    });
                    gradeMarksForMaxMarks.clear();

                    gradeMarksForMaxMarks.addAll(gradeMarksEntities);
                    adapter.notifyDataSetChanged();

                }

            });
        }


        setColorApp();


        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4692575826", this, adContainer);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class GradeMarksRangeAdapter extends RecyclerView.Adapter<GradeMarksRangeAdapter.MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.create_grade_range, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.tv_grade.setText(gradeMarksForMaxMarks.get(position).gradeValue);
            holder.tv_range_start.setText(gradeMarksForMaxMarks.get(position).rangeStart);
            holder.tv_range_end.setText(gradeMarksForMaxMarks.get(position).rangeEnd);
            holder.tv_result.setText(gradeMarksForMaxMarks.get(position).resultValue);

        }

        @Override
        public int getItemCount() {
            return gradeMarksForMaxMarks.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_grade,tv_range_start,tv_range_end,tv_result;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_grade = itemView.findViewById(R.id.tv_grade);
                tv_grade.setTextColor(colorIs);

                tv_range_start = itemView.findViewById(R.id.tv_range_start);
                tv_range_start.setTextColor(colorIs);

                tv_range_end = itemView.findViewById(R.id.tv_range_end);
                tv_range_end.setTextColor(colorIs);

                tv_result = itemView.findViewById(R.id.tv_result);
                tv_result.setTextColor(colorIs);

                itemView.setOnClickListener(v -> {
                    permissions = new String[]{
                            getResources().getString(R.string.update),
//                                    context.getResources().getString(R.string.evaluate),
                            getResources().getString(R.string.delete)
                            //                context.getResources().getString(R.string.assignment_submission)

                    };


                    android.app.AlertDialog.Builder choices = new android.app.AlertDialog.Builder(CreateGradeMarksActivity.this);

                    choices.setItems(permissions, (dialogInterface, which) -> {
                        switch (which) {
                            case 0:

                                if (getAdapterPosition() != ( gradeMarksForMaxMarks.size() -1) ){
                                    AlertDialog.Builder alert1 = new AlertDialog.Builder(CreateGradeMarksActivity.this);

                                    alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.delete_below_entries_first)  + "<Br>"));
                                    alert1.setTitle(getString(R.string.information));
                                    alert1.setIcon(R.drawable.ic_clickhere);
                                    alert1.setPositiveButton(getString(R.string.ok), (dialog, which1) -> {

                                    });
                                    alert1.show();
                                    Config.responseSnackBarHandler(getString(R.string.delete_below_entries_first),
                                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                                }else {

                                    Intent intent = new Intent(CreateGradeMarksActivity.this, EditGradeMarkRowActivity.class);

                                    intent.putExtra("grademarkrow", new Gson().toJson(gradeMarksForMaxMarks.get(getAdapterPosition())));
                                    intent.putExtra("maxmarks", maxMarks);
                                    intent.putExtra("rownumber", String.valueOf(getAdapterPosition()));
                                    intent.putStringArrayListExtra("gradenamelist", gradeNameList);
                                    intent.putStringArrayListExtra("gradeidlist", gradeIdList);
                                    intent.putStringArrayListExtra("resultnamelist", resultNameList);
                                    intent.putStringArrayListExtra("resultidlist", resultIdList);
                                    intent.putExtra("grademarkslist", new Gson().toJson(gradeMarksForMaxMarks));

                                    startActivityForResult(intent, 423);
                                }
                                break;
                            case 1:
                                if (getAdapterPosition() != ( gradeMarksForMaxMarks.size() -1) ){
                                    AlertDialog.Builder alert1 = new AlertDialog.Builder(CreateGradeMarksActivity.this);

                                    alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.delete_below_entries_first)  + "<Br>"));
                                    alert1.setTitle(getString(R.string.information));
                                    alert1.setIcon(R.drawable.ic_clickhere);
                                    alert1.setPositiveButton(getString(R.string.ok), (dialog, which1) -> {

                                    });
                                    alert1.show();
                                    Config.responseSnackBarHandler(getString(R.string.delete_below_entries_first),
                                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
                                }else{

                                        String gradeid = Strings.nullToEmpty(gradeMarksForMaxMarks.get(getAdapterPosition()).gradeID);


                                        AlertDialog.Builder alert1 = new AlertDialog.Builder(CreateGradeMarksActivity.this);

                                        alert1.setMessage(Html.fromHtml("<Br>" + getString(R.string.are_you_sure_you_want_to_delete) + "<Br>"));
                                        alert1.setTitle(getString(R.string.delete));
                                        alert1.setIcon(R.drawable.ic_alert);
                                        alert1.setNegativeButton(getString(R.string.cancel), (dialog, which12) -> {

//                                            dialog.dismiss();
                                        });


                                        alert1.setPositiveButton(getString(R.string.ok), (dialog, which1) -> {
                                            if ("".equalsIgnoreCase(gradeid)) {
                                                gradeMarksForMaxMarks.remove(getAdapterPosition());
                                                adapter.notifyDataSetChanged();

                                            } else {
                                                try {
                                                    deleteGradeMarksOnServer(gradeMarksForMaxMarks.get(getAdapterPosition()), getAdapterPosition());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                        alert1.show();

                                }
                                break;
                        }

                    });
                    choices.show();
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == 423 && resultCode == RESULT_OK && data != null){
            GradeMarksEntity g = new Gson().fromJson(data.getStringExtra("grademarkrow"), GradeMarksEntity.class);


            int rownumber = Integer.parseInt( data.getStringExtra("rownumber"));
            gradeMarksForMaxMarks.set(rownumber,g);

            adapter.notifyDataSetChanged();



            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save:
                if (validateInput()){
                    try {
                        saveGradeMarksOnServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveGradeMarksOnServer() throws JSONException {
            progressBar.setVisibility(View.VISIBLE);

            final JSONArray param = new JSONArray();

            for (GradeMarksEntity g: gradeMarksForMaxMarks){
                final JSONObject p = new JSONObject();

                p.put("gradeID", g.gradeID);
                p.put("grade", g.grade);
                p.put("result", g.gradeResult);
                p.put("rangeStart", g.rangeStart);
                p.put("rangeEnd", g.rangeEnd);
                p.put("maxMarks", maxMarks);

                param.put(p);
            }



            String url = Gc
                    .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "exam/gradeMarks"
                    ;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
                String code = job.optString("code");
                progressBar.setVisibility(View.GONE);

                switch (code) {
                    case Gc.APIRESPONSE200:
                        String resultjobj = job.optString("result");


                        try {
                            ArrayList<GradeMarksEntity> gradeMarksEntities = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("gradeMarks"),new TypeToken<List<GradeMarksEntity>>(){}.getType());

                            if (gradeMarksEntities != null) {
                                new Thread(() -> {
                                    mDb.gradeMarksModel().insertGradeMarks(gradeMarksEntities);
                                }).start();
                            }
                            } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_green);

                        finish();

                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, CreateGradeMarksActivity.this);

                        Intent intent1 = new Intent(CreateGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, CreateGradeMarksActivity.this);

                        Intent intent2 = new Intent(CreateGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                }
            }, error -> {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(CreateGradeMarksActivity.this, error, findViewById(R.id.top_layout));
            }){
                @Override
                public Map<String, String> getHeaders()  {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() {
                    return param.toString().getBytes(StandardCharsets.UTF_8);
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

        }


    private void deleteGradeMarksOnServer(GradeMarksEntity g, int i) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

//        final JSONArray param = new JSONArray();
//        final JSONObject p = new JSONObject();
//
//        p.put("gradeID", g.gradeID);
//        p.put("grade", g.grade);
//        p.put("result", g.gradeResult);
//        p.put("rangeStart", g.rangeStart);
//        p.put("rangeEnd", g.rangeEnd);
//        p.put("maxMarks", maxMarks);
//
//        param.put(p);



        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/gradeMarks/"
                + new JSONObject().put("gradeID", g.gradeID)
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    String resultjobj = job.optString("result");

                    GradeMarksEntity toDelete = gradeMarksForMaxMarks.get(i); // copy to another variable because deletion is on another thread

                    new Thread(() -> {
                        mDb.gradeMarksModel().deleteGradeMarkbyId(toDelete);
                    }).start();
                    gradeMarksForMaxMarks.remove(i);
                    adapter.notifyDataSetChanged();

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, CreateGradeMarksActivity.this);

                    Intent intent1 = new Intent(CreateGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, CreateGradeMarksActivity.this);

                    Intent intent2 = new Intent(CreateGradeMarksActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(CreateGradeMarksActivity.this, error, findViewById(R.id.top_layout));
        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//            @Override
//            public byte[] getBody() {
//                return param.toString().getBytes(StandardCharsets.UTF_8);
//            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    Boolean validateInput(){

        int size = gradeMarksForMaxMarks.size();

        if (size == 0){
            Config.responseSnackBarHandler(getString(R.string.nothing_to_save),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if (!(gradeMarksForMaxMarks.get(size -1).rangeEnd.equalsIgnoreCase("0"))){
            Config.responseSnackBarHandler(getString(R.string.last_row_must_have_zero_as_range_end, gradeMarksForMaxMarks.get(size -1).rangeEnd ),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        if (!(gradeMarksForMaxMarks.get(0).rangeStart.equalsIgnoreCase(maxMarks))){
            Config.responseSnackBarHandler(getString(R.string.first_row_should_begin_with_maximum_marks, maxMarks),
                    findViewById(R.id.top_layout),R.color.fragment_first_blue);
            return false;
        }

        return true;
    }
}
