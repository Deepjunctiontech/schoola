package in.junctiontech.school.schoolnew.users;

import android.app.SearchManager;
import androidx.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class UserListActivity extends AppCompatActivity {

    ProgressBar progressBar;
    MainDatabase mDb ;

    RadioButton rb_show_student, rb_show_staff;

    TextView tv_user_count, tv_label_total;

    ArrayList<SchoolStaffEntity> staffList = new ArrayList<>();
    ArrayList<SchoolStaffEntity> staffFilterList = new ArrayList<>();

    ArrayList<SchoolStudentEntity> studentList = new ArrayList<>();
    ArrayList<SchoolStudentEntity> studentfilterList = new ArrayList<>();

    RecyclerView mRecyclerView;
    UserListAdapter adapter;

    private int colorIs;


    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);

        initializeViews();


        mRecyclerView = findViewById(R.id.rv_user_list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new UserListAdapter();
        mRecyclerView.setAdapter(adapter);

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2901460935", this, adContainer);
        }
    }

    void initializeViews(){
        progressBar = findViewById(R.id.progressBar);
        rb_show_student = findViewById(R.id.rb_show_student);
        rb_show_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_label_total.setVisibility(View.VISIBLE);
                tv_label_total.setText(R.string.student);
                tv_user_count.setVisibility(View.VISIBLE);
                mDb.schoolStudentModel().getAllStudents()
                        .observe(UserListActivity.this, new Observer<List<SchoolStudentEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<SchoolStudentEntity> schoolStudentEntities) {
                        studentList.clear();
                        studentfilterList.clear();
                        studentList.addAll(schoolStudentEntities);
                        studentfilterList.addAll(schoolStudentEntities);
                        tv_user_count.setText(String.valueOf(studentList.size()));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        rb_show_staff = findViewById(R.id.rb_show_staff);
        rb_show_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_label_total.setVisibility(View.VISIBLE);
                tv_label_total.setText(R.string.staff);
                tv_user_count.setVisibility(View.VISIBLE);
                mDb.schoolStaffModel().getAllStaff().observe(UserListActivity.this, new Observer<List<SchoolStaffEntity>>() {
                    @Override
                    public void onChanged(@Nullable List<SchoolStaffEntity> schoolStaffEntities) {
                        staffList.clear();
                        staffFilterList.clear();
                        staffList.addAll(schoolStaffEntities);
                        staffFilterList.addAll(schoolStaffEntities);
                        tv_user_count.setText(staffList.size()+"");
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        tv_user_count = findViewById(R.id.tv_user_count);
        tv_label_total = findViewById(R.id.tv_label_total);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));



        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterUser(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterUser(s);
                return true ;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_help:
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);

                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void filterUser(String text){

        if (rb_show_student.isChecked()){

            studentList.clear();
            if(text.isEmpty()) {
                studentList.addAll(studentfilterList);
            }else {
                text = text.toLowerCase();
                for (SchoolStudentEntity s: studentfilterList){
                    if (s.StudentName.toLowerCase().contains(text)
                            || s.ClassName.toLowerCase().contains(text)
                            || s.AdmissionNo.toLowerCase().contains(text)
                            ){
                        studentList.add(s);
                    }
                }
            }
        } else if (rb_show_staff.isChecked()){

            staffList.clear();
            if(text.isEmpty()) {
                staffList.addAll(staffFilterList);
            }else {
                text = text.toLowerCase();
                for (SchoolStaffEntity s: staffFilterList){
                    if (s.StaffName.toLowerCase().contains(text)
                            || s.StaffPositionValue.toLowerCase().contains(text)
                            ){
                        staffList.add(s);
                    }
                }

            }
        }
        adapter.notifyDataSetChanged();

    }


    public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.MyViewHolder>{

        @NonNull
        @Override
        public UserListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull UserListAdapter.MyViewHolder holder, int position) {
            holder.tv_single_item.setTextColor(colorIs);
            if (rb_show_staff.isChecked()) {
                holder.tv_single_item.setText(staffList.get(position).StaffName
                        + " "
                + staffList.get(position).StaffPositionValue);
            }else {
                holder.tv_single_item.setText(studentList.get(position).StudentName
                + " "
                + studentList.get(position).ClassName
                        + " "
                        + getString(R.string.user_name)
                        + ": "
                + studentList.get(position).AdmissionNo
                + " "
                        + getString(R.string.password)
                        + ": "
                        + studentList.get(position).StudentsPassword
                );
            }

        }

        @Override
        public int getItemCount() {
            if (rb_show_staff.isChecked()){
                return staffList.size();
            }else {
                return studentList.size();
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);

                itemView.setOnClickListener(view -> {
                    AlertDialog.Builder choices = new AlertDialog.Builder(UserListActivity.this);
                    String[] aa = new String[]
                            {
                                    getString(R.string.reset_password),
                                    getString(R.string.share)+" "+
                                            getString(R.string.login_details)
                            };

                    choices.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            String login ;
                            String password;


                                switch (which){
                                case 0:
                                    if(Gc.getSharedPreference(Gc.ERPPLANTYPE,UserListActivity.this).equalsIgnoreCase(Gc.DEMO)) {
                                        Config.responseSnackBarHandler(getString(R.string.permission_denied),
                                                findViewById(R.id.top_layout),R.color.fragment_first_blue);
                                    }else {
                                        updatePasswordForUser(getAdapterPosition());
                                    }
                                    break;
                                case 1:
                                    String shareBody = getString(R.string.link) + " - " +
                                            Config.SCHOOL_PLAY_STORE_LINK + BuildConfig.APPLICATION_ID
                                            + "\n" +
                                            getString(R.string.organization_key) + "-" +
                                            Gc.getSharedPreference(Gc.ERPINSTCODE, UserListActivity.this) + "\n" + getString(R.string.login_id) + "-" ;

                                    if (rb_show_staff.isChecked()) {
                                        login = staffList.get(getAdapterPosition()).Username;
                                        password = "";

                                        shareBody = shareBody
                                                + login
                                                +"\n"
                                                + getString(R.string.contact_administrator_for_password);
                                    }else {
                                        login = studentList.get(getAdapterPosition()).AdmissionNo;
                                        password = studentList.get(getAdapterPosition()).StudentsPassword;

                                        shareBody = shareBody
                                                + login + "\n"
                                                + getString(R.string.password)
                                                + "-"
                                                + password
                                        ;
                                    }

                                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT,
                                            "MySchool (Open it in Google Play Store to Download the Application)");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);

                                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }
                    });
                    choices.show();

                });
            }
        }
    }

    void updatePasswordForUser(final int position){

        if (rb_show_student.isChecked()){

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_edit);
            builder.setTitle(R.string.reset_password);

            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.student_password_reset, null);

            final EditText et_parent_password = view.findViewById(R.id.et_staff_password);
            et_parent_password.setText(studentList.get(position).ParentsPassword);
            final EditText et_student_password = view.findViewById(R.id.et_user_name);
            et_student_password.setText(studentList.get(position).StudentsPassword);



            builder.setView(view);

            builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });

            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            final AlertDialog dialog = builder.create();

            dialog.show();

            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean wantToCloseDialog = false;
                    String parentPassword = et_parent_password.getText().toString().trim();

                    String studentPassword = et_student_password.getText().toString().trim();


                    if ("".equalsIgnoreCase(parentPassword)){
                        et_parent_password.setError(getString(R.string.password_can_not_be_blank),getResources()
                                .getDrawable(R.drawable.ic_alert));

                    }else if ("".equalsIgnoreCase(studentPassword)) {
                        et_student_password.setError(getString(R.string.password_can_not_be_blank), getResources()
                                .getDrawable(R.drawable.ic_alert));
                    }else if (parentPassword.equalsIgnoreCase(studentPassword)){
                        et_parent_password.setError(getString(R.string.password_can_not_be_same) ,getDrawable(R.drawable.ic_alert));
                    }else{
                        wantToCloseDialog = true;

                        try {
                            sendStudentPasswordToServer(parentPassword, studentPassword, studentList.get(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if(wantToCloseDialog)
                        dialog.dismiss();
                }
            });

        }

        else if (rb_show_staff.isChecked()){

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.ic_edit);
            builder.setTitle(R.string.reset_password);

            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.staff_password_reset, null);

            final EditText et_user_name = view.findViewById(R.id.et_user_name);
            et_user_name.setText(staffList.get(position).Username);

            final EditText et_staff_password = view.findViewById(R.id.et_staff_password);

            builder.setView(view);

            builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });

            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            final AlertDialog dialog = builder.create();

            dialog.show();

            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    boolean wantToCloseDialog = false;
                    String staffUser = et_user_name.getText().toString().trim();

                    String staffPassword = et_staff_password.getText().toString().trim();


                    if ("".equalsIgnoreCase(staffUser)){
                        et_user_name.setError(getString(R.string.field_can_not_be_blank),getResources()
                                .getDrawable(R.drawable.ic_alert));

                    }else if ("".equalsIgnoreCase(staffPassword)) {
                        et_staff_password.setError(getString(R.string.password_can_not_be_blank), getResources()
                                .getDrawable(R.drawable.ic_alert));
                    }else{
                        wantToCloseDialog = true;

                        try {
                            sendStaffPasswordToServer(staffUser, staffPassword, staffList.get(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if(wantToCloseDialog)
                        dialog.dismiss();

                }
            });

        }


    }

    void sendStaffPasswordToServer(String staffUser, String staffPassword, SchoolStaffEntity staffEntity ) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = new JSONObject();
        param.put("Username", staffUser);
        param.put("Password", staffPassword);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "user/user/"
                + new JSONObject().put("StaffId",staffEntity.StaffId )
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {

                        final ArrayList<SchoolStaffEntity> staffEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("staffDetails"),
                                        new TypeToken<List<SchoolStaffEntity>>(){}.getType());


                        new Thread(() -> {
                            mDb.schoolStaffModel().insertStaff(staffEntities);
                        }).start();


                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, UserListActivity.this);

                    Intent intent1 = new Intent(UserListActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, UserListActivity.this);

                    Intent intent2 = new Intent(UserListActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(UserListActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void sendStudentPasswordToServer( String parentPassword, String studentPassword, SchoolStudentEntity student ) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONObject param = new JSONObject();
        param.put("ParentsPassword", parentPassword);
        param.put("StudentsPassword", studentPassword);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentRegistration/"
                + new JSONObject().put("RegistrationId",student.RegistrationId )
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);

            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {

                        final ArrayList<SchoolStudentEntity> schoolStudentEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("studentDetails"),
                                        new TypeToken<List<SchoolStudentEntity>>(){}.getType());


                    new Thread(() -> {
                        mDb.schoolStudentModel().insertStudents(schoolStudentEntities);
                    }).start();


                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, UserListActivity.this);

                    Intent intent1 = new Intent(UserListActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, UserListActivity.this);

                    Intent intent2 = new Intent(UserListActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(UserListActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }
}

