package `in`.junctiontech.school.schoolnew.chat.adapter

import `in`.junctiontech.school.BuildConfig
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.FullScreenImageActivity
import `in`.junctiontech.school.schoolnew.chat.modelhelper.ChatMainArrayList
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.playvideo.PlayVideo
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class ChatConversationAdapter internal constructor(private val context: Context, private val chatListEntities: ArrayList<ChatMainArrayList>,
                                                   private val chatListener: ChatClickListener) : RecyclerView.Adapter<ChatConversationAdapter.MyViewHolder>() {

    companion object {
        var longClick = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_conversation_new,
                parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if ("" != chatListEntities[position].dateHeader) {
            holder.tvChatDate.text = chatListEntities[position].dateHeader
            holder.tvChatDate.visibility = View.VISIBLE
        } else {
            holder.tvChatDate.visibility = View.GONE
        }

        if (chatListEntities[position].isSelected) {
            holder.rlChatLayout.setBackgroundColor(Color.parseColor("#887895AC"))
        } else {
            holder.rlChatLayout.setBackgroundColor(Color.parseColor("#00ffffff"))
        }

        holder.rlChatReceivedLayout.visibility = View.GONE
        holder.rlChatReceivedImageLayout.visibility = View.GONE
        holder.rlChatReceivedPdfLayout.visibility = View.GONE
        holder.rlReceivedVideo.visibility = View.GONE
        holder.rlReceivedChat.visibility = View.GONE
        holder.llWithMsgPicTime.visibility = View.GONE
        holder.llWithoutMsgPicTime.visibility = View.GONE

        holder.rlSendChatLayout.visibility = View.GONE
        holder.rlSendChatPictureLayout.visibility = View.GONE
        holder.rlChatSendPdfLayout.visibility = View.GONE
        holder.rlSendVideosLayout.visibility = View.GONE
        holder.llLoggedUserChat.visibility = View.GONE

        val millisecondsDate = Gc.getMiliseconds(chatListEntities[position].enteredDate, false)
        val calendarOld = Calendar.getInstance()
        calendarOld.timeInMillis = millisecondsDate
        val formatter = SimpleDateFormat("hh:mm aaa", Locale.US)
        val enteredDate = formatter.format(calendarOld.time)

        if (chatListEntities[position].byMe == 1) {
            holder.rlSendChatLayout.visibility = View.VISIBLE
            if ("image" == chatListEntities[position].msgType && "" != chatListEntities[position].localPath && chatListEntities[position].localPath != null) {

                holder.rlSendChatPictureLayout.visibility = View.VISIBLE
                holder.tvSendChatImageTime.text = enteredDate

                val file = File(chatListEntities[position].localPath)
                if (file.exists()) {
                    Glide.with(context)
                            .load(chatListEntities[position].localPath)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.ivSendChatPictures)
                }

                if (chatListEntities[position].sent == 0) {
                    holder.sendImage.visibility = View.GONE
                }

                if (chatListEntities[position].sent == 2) {
                    holder.sendImage.visibility = View.VISIBLE
                }

                if (chatListEntities[position].sent == 1) {
                    holder.sendImage.visibility = View.GONE
                    holder.imageSendProgressBar.visibility = View.GONE
                    holder.tvSendChatImageTimeTick.setImageResource(R.drawable.single_tick)
                }

                if (chatListEntities[position].received == 1) {
                    holder.tvSendChatImageTimeTick.setImageResource(R.drawable.ic_double_tick)
                }
            } else if ("video" == chatListEntities[position].msgType && "" != chatListEntities[position].imagepath && chatListEntities[position].imagepath != null) {
                holder.rlSendVideosLayout.visibility = View.VISIBLE
                holder.tvSendChatVideoTime.text = enteredDate

                Glide.with(context)
                        .load(chatListEntities[position].imagepath)
                        .apply(RequestOptions()
                                .error(R.drawable.ic_single)
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .thumbnail(0.5f)
                        .into(holder.vvSendVideo)

                if (chatListEntities[position].sent == 0) {
                    holder.btnSendVideo.visibility = View.GONE
                }

                if (chatListEntities[position].sent == 2) {
                    holder.btnSendVideo.visibility = View.VISIBLE
                }

                if (chatListEntities[position].sent == 1) {
                    holder.btnSendVideo.visibility = View.GONE
                    holder.pbSendVideo.visibility = View.GONE
                    holder.ivSendVideoTick.setImageResource(R.drawable.single_tick)
                }

                if (chatListEntities[position].received == 1) {
                    holder.ivSendVideoTick.setImageResource(R.drawable.ic_double_tick)
                }

            } else if ("pdf" == chatListEntities[position].msgType && "" != chatListEntities[position].imagepath && chatListEntities[position].imagepath != null) {
                holder.rlChatSendPdfLayout.visibility = View.VISIBLE

                val path = chatListEntities[position].imagepath
                val pdfName = path!!.substring(path.lastIndexOf('/') + 1)

                holder.tvSendPdfTime.text = enteredDate
                holder.tvSendPdfName.text = pdfName

                if (chatListEntities[position].sent == 1) {
                    holder.ivSendPdfAlarm.setImageResource(R.drawable.single_tick)
                }

                if (chatListEntities[position].received == 1) {
                    holder.ivSendPdfAlarm.setImageResource(R.drawable.ic_double_tick)
                }

            } else {
                holder.llLoggedUserChat.visibility = View.VISIBLE
                holder.tvLoggedUserChatMsg.text = chatListEntities[position].msg
                holder.tvLoggedUserChatTime.text = enteredDate

                if (chatListEntities[position].sent == 1) {
                    holder.ivLoggedUserAlarm.setImageResource(R.drawable.single_tick)
                }

                if (chatListEntities[position].received == 1) {
                    holder.ivLoggedUserAlarm.setImageResource(R.drawable.ic_double_tick)
                }
            }
        } else {
            holder.rlChatReceivedLayout.visibility = View.VISIBLE
            if ("image" == chatListEntities[position].msgType && "" != chatListEntities[position].imagepath && chatListEntities[position].imagepath != null) {
                holder.rlChatReceivedImageLayout.visibility = View.VISIBLE
                holder.ivReceivedChatPicture.isEnabled = false;
                holder.downloadImage.visibility = View.VISIBLE

                if ("" == chatListEntities[position].msg) {
                    holder.llWithoutMsgPicTime.visibility = View.VISIBLE
                    holder.tvRvPicTime.text = enteredDate

                } else {
                    holder.llWithMsgPicTime.visibility = View.VISIBLE
                    holder.tvImgSms.text = chatListEntities[position].msg
                    holder.tvWithMsgTime.text = enteredDate
                }

                if (!chatListEntities[position].localPath.isNullOrEmpty()) {
                    val file = File(chatListEntities[position].localPath)
                    if (file.exists()) {
                        holder.downloadImage.visibility = View.GONE
                        holder.ivReceivedChatPicture.isEnabled = true
                        Glide.with(context)
                                .load(chatListEntities[position].localPath)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop())
                                .thumbnail(1f)
                                .into(holder.ivReceivedChatPicture)
                    } else {
                        holder.downloadImage.text = chatListEntities[position].fileSize
                        Glide.with(context)
                                .load(chatListEntities[position].imagepath)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop())
                                .thumbnail(1f)
                                .into(holder.ivReceivedChatPicture)
                    }
                } else {
                    holder.downloadImage.text = chatListEntities[position].fileSize
                    Glide.with(context)
                            .load(chatListEntities[position].imagepath)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop())
                            .thumbnail(1f)
                            .into(holder.ivReceivedChatPicture)
                }
            } else if ("pdf" == chatListEntities[position].msgType && "" != chatListEntities[position].imagepath && chatListEntities[position].imagepath != null) {
                holder.rlChatReceivedPdfLayout.visibility = View.VISIBLE
                if (!chatListEntities[position].localPath.isNullOrEmpty()) {
                    val file = File(chatListEntities[position].localPath)
                    if (file.exists()) {
                        holder.ivReceivedChatPdf.visibility = View.GONE
                        holder.downloadPdf.visibility = View.GONE
                    } else {
                        holder.ivReceivedChatPdf.visibility = View.VISIBLE
                        holder.downloadPdf.visibility = View.VISIBLE
                        holder.downloadPdf.text = " " + chatListEntities[position].fileSize
                    }
                } else {
                    holder.ivReceivedChatPdf.visibility = View.VISIBLE
                    holder.downloadPdf.visibility = View.VISIBLE
                    holder.downloadPdf.text = " " + chatListEntities[position].fileSize
                }
                val path = chatListEntities[position].imagepath
                val pdfName = path!!.substring(path.lastIndexOf('/') + 1)
                holder.tvRvPdfName.text = pdfName
            } else if ("video" == chatListEntities[position].msgType && "" != chatListEntities[position].imagepath && chatListEntities[position].imagepath != null) {
                holder.rlReceivedVideo.visibility = View.VISIBLE
                holder.tvReceivedVideoTime.text = enteredDate
                if (!chatListEntities[position].localPath.isNullOrEmpty()) {
                    val file = File(chatListEntities[position].localPath)
                    if (file.exists()) {
                        holder.downloadVideo.visibility = View.GONE
                        // holder.senderPlayVideo.visibility = View.VISIBLE
                        Glide.with(context)
                                .load(chatListEntities[position].imagepath)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .thumbnail(0.5f)
                                .into(holder.vvReceivedVideos)
                    } else {
                        holder.downloadVideo.text = " " + chatListEntities[position].fileSize
                        Glide.with(context)
                                .load(chatListEntities[position].imagepath)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .thumbnail(0.5f)
                                .into(holder.vvReceivedVideos)
                    }
                } else {
                    holder.downloadVideo.text = " " + chatListEntities[position].fileSize
                    Glide.with(context)
                            .load(chatListEntities[position].imagepath)
                            .apply(RequestOptions()
                                    .error(R.drawable.ic_single)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .thumbnail(0.5f)
                            .into(holder.vvReceivedVideos)
                }
            } else {
                holder.rlReceivedChat.visibility = View.VISIBLE
                holder.tvRvSms.text = chatListEntities[position].msg
                holder.tvRvMsgTime.text = enteredDate
            }
        }
    }

    override fun getItemCount(): Int {
        return chatListEntities.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        /* Headers Views */
        var rlChatLayout: RelativeLayout = itemView.findViewById(R.id.rl_chat_layout)
        var tvChatDate: TextView = itemView.findViewById(R.id.tv_chat_date)

        /* Received Chats Views */
        var rlChatReceivedLayout: RelativeLayout = itemView.findViewById(R.id.rl_chat_received_layout)

        var rlChatReceivedImageLayout: RelativeLayout = itemView.findViewById(R.id.rl_chat_received_image_layout)
        var ivReceivedChatPicture: ImageView = itemView.findViewById(R.id.iv_received_chat_picture)
        var downloadImageProgressBar: ProgressBar = itemView.findViewById(R.id.download_image_progress_bar)
        var downloadImage: Button = itemView.findViewById(R.id.downloadImage)
        var llWithMsgPicTime: LinearLayout = itemView.findViewById(R.id.ll_with_msg_pic_time)
        var tvImgSms: TextView = itemView.findViewById(R.id.tv_img_sms)
        var tvWithMsgTime: TextView = itemView.findViewById(R.id.tv_with_msg_time)
        var llWithoutMsgPicTime: LinearLayout = itemView.findViewById(R.id.ll_without_msg_pic_time)
        var tvRvPicTime: TextView = itemView.findViewById(R.id.tv_rv_pic_time)

        var rlChatReceivedPdfLayout: RelativeLayout = itemView.findViewById(R.id.rl_chat_received_pdf_layout)
        var downloadPdfProgressBar: ProgressBar = itemView.findViewById(R.id.download_Pdf_progress_bar)
        var downloadPdf: Button = itemView.findViewById(R.id.downloadPdf)
        var ivReceivedChatPdf: ImageView = itemView.findViewById(R.id.iv_received_chat_pdf)
        var llWithoutMsgPdfName: LinearLayout = itemView.findViewById(R.id.ll_without_msg_pdf_name)
        var tvRvPdfName: TextView = itemView.findViewById(R.id.tv_rv_pdf_name)

        var rlReceivedVideo: RelativeLayout = itemView.findViewById(R.id.rl_received_video)
        var vvReceivedVideos: ImageView = itemView.findViewById(R.id.vv_received_videos)
        var downloadVideoProgressBar: ProgressBar = itemView.findViewById(R.id.download_video_progress_bar)
        var downloadVideo: Button = itemView.findViewById(R.id.downloadVideo)
        var tvReceivedVideoTime: TextView = itemView.findViewById(R.id.tv_received_video_time)

        var rlReceivedChat: LinearLayout = itemView.findViewById(R.id.rl_received_chat)
        var tvRvSms: TextView = itemView.findViewById(R.id.tv_rv_sms)
        var tvRvMsgTime: TextView = itemView.findViewById(R.id.tv_rv_msg_time)

        /* Send Chats Views */

        var rlSendChatLayout: RelativeLayout = itemView.findViewById(R.id.rl_send_chat_Layout)

        var rlChatSendPdfLayout: RelativeLayout = itemView.findViewById(R.id.rl_chat_send_pdf_layout)
        var ivSendChatPdf: ImageView = itemView.findViewById(R.id.iv_Send_chat_pdf)
        var tvSendPdfName: TextView = itemView.findViewById(R.id.tv_send_pdf_name)
        var tvSendPdfTime: TextView = itemView.findViewById(R.id.tv_send_pdf_time)
        var ivSendPdfAlarm: ImageView = itemView.findViewById(R.id.iv_send_pdf_alarm)

        var rlSendChatPictureLayout: RelativeLayout = itemView.findViewById(R.id.rl_send_chat_picture_layout)
        var ivSendChatPictures: ImageView = itemView.findViewById(R.id.iv_send_chat_pictures)
        var imageSendProgressBar: ProgressBar = itemView.findViewById(R.id.image_send_progress_bar)
        var sendImage: Button = itemView.findViewById(R.id.sendImage)
        var tvSendChatImageTime: TextView = itemView.findViewById(R.id.tv_send_chat_image_time)
        var tvSendChatImageTimeTick: ImageView = itemView.findViewById(R.id.tv_send_chat_image_time_tick)

        var rlSendVideosLayout: RelativeLayout = itemView.findViewById(R.id.rl_send_videos_layout)
        var vvSendVideo: ImageView = itemView.findViewById(R.id.vv_send_video)
        var pbSendVideo: ProgressBar = itemView.findViewById(R.id.pb_send_video)
        var btnSendVideo: Button = itemView.findViewById(R.id.btn_send_video)
        var tvSendChatVideoTime: TextView = itemView.findViewById(R.id.tv_send_chat_video_time)
        var ivSendVideoTick: ImageView = itemView.findViewById(R.id.iv_send_video_tick)

        var llLoggedUserChat: LinearLayout = itemView.findViewById(R.id.ll_logged_user_chat)
        var tvLoggedUserChatMsg: TextView = itemView.findViewById(R.id.tv_logged_user_chat_msg)
        var tvLoggedUserChatTime: TextView = itemView.findViewById(R.id.tv_logged_user_chat_time)
        var ivLoggedUserAlarm: ImageView = itemView.findViewById(R.id.iv_logged_user_alarm)

        init {
            itemView.setOnLongClickListener {
                chatListener.onLongItemPressed(layoutPosition, true)
                longClick = true
                true
            }

            ivSendChatPictures.setOnLongClickListener {
                chatListener.onLongItemPressed(layoutPosition, true)
                longClick = true
                true
            }

            vvSendVideo.setOnLongClickListener {
                chatListener.onLongItemPressed(layoutPosition, true)
                longClick = true
                true
            }

            itemView.setOnClickListener {
                if (longClick) {
                    if (chatListEntities[layoutPosition].isSelected) {
                        chatListener.onLongItemPressed(layoutPosition, false)
                    } else {
                        chatListener.onLongItemPressed(layoutPosition, true)
                    }
                }
            }

            sendImage.setOnClickListener {
                sendImage.visibility = View.GONE
                imageSendProgressBar.visibility = View.VISIBLE
                chatListEntities[layoutPosition].msgId?.let { it1 -> chatListener.onResent(it1) }
            }

            tvRvPdfName.setOnClickListener {
                val file = File(chatListEntities[layoutPosition].localPath)
                val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)
                val intent = Intent(Intent.ACTION_VIEW).setDataAndType(uri, "application/pdf")
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                context.startActivity(intent)
            }

            tvSendPdfName.setOnClickListener {
                val file = File(chatListEntities[layoutPosition].localPath)
                val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)
                val intent = Intent(Intent.ACTION_VIEW).setDataAndType(uri, "application/pdf")
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                context.startActivity(intent)
            }

            btnSendVideo.setOnClickListener {
                btnSendVideo.visibility = View.GONE
                pbSendVideo.visibility = View.VISIBLE
                chatListEntities[layoutPosition].msgId?.let { it1 -> chatListener.onResent(it1) }
            }

            ivReceivedChatPicture.setOnClickListener {
                val galleryImageURLs = ArrayList<String>()
                var i = 0
                var viewPosition = 0
                for (g in chatListEntities) {
                    if (!(g.imageOriginalPath.isNullOrEmpty()) && "image" == g.msgType) {
                        galleryImageURLs.add(g.imageOriginalPath!!)
                        if (g.imageOriginalPath == chatListEntities[layoutPosition].imageOriginalPath) {
                            viewPosition = i
                        }
                        i++
                    }
                }
                val intent = Intent(context, FullScreenImageActivity::class.java)
                intent.putStringArrayListExtra("images", galleryImageURLs)
                intent.putExtra("index", viewPosition)
                intent.putExtra("title", String.format("%s : %s", chatListEntities[layoutPosition].withType, chatListEntities[layoutPosition].withName))
                context.startActivity(intent)
            }

            downloadImage.setOnClickListener {
                downloadImage.visibility = View.GONE
                downloadImageProgressBar.visibility = View.VISIBLE
                chatListEntities[layoutPosition].msgId?.let { it1 -> chatListEntities[layoutPosition].imageOriginalPath?.let { it2 -> chatListener.onClickDownload(it2, it1, downloadImageProgressBar, "image") } }
            }

            downloadPdf.setOnClickListener {
                downloadPdf.visibility = View.GONE
                downloadPdfProgressBar.visibility = View.VISIBLE
                chatListEntities[layoutPosition].msgId?.let { it1 -> chatListEntities[layoutPosition].imageOriginalPath?.let { it2 -> chatListener.onClickDownload(it2, it1, downloadPdfProgressBar, "pdf") } }
            }

            downloadVideo.setOnClickListener {
                downloadVideo.visibility = View.GONE
                downloadVideoProgressBar.visibility = View.VISIBLE
                chatListEntities[layoutPosition].msgId?.let { it1 -> chatListEntities[layoutPosition].imageOriginalPath?.let { it2 -> chatListener.onClickDownload(it2, it1, downloadVideoProgressBar, "video") } }
            }

            ivSendChatPictures.setOnClickListener {
                if (longClick) {
                    if (chatListEntities[layoutPosition].isSelected) {
                        chatListener.onLongItemPressed(layoutPosition, false)
                    } else {
                        chatListener.onLongItemPressed(layoutPosition, true)
                    }
                    val p = layoutPosition
                    println("Click: $p")
                } else {
                    val galleryImageURLs = ArrayList<String>()
                    var i = 0
                    var viewPosition = 0
                    for (g in chatListEntities) {
                        if (!(g.localPath.isNullOrEmpty()) && "image" == g.msgType) {
                            galleryImageURLs.add(g.localPath!!)
                            if (g.localPath == chatListEntities[layoutPosition].localPath) {
                                viewPosition = i
                            }
                            i++
                        }
                    }
                    val intent = Intent(context, FullScreenImageActivity::class.java)
                    intent.putStringArrayListExtra("images", galleryImageURLs)
                    intent.putExtra("index", viewPosition)
                    intent.putExtra("title", String.format("%s : %s", chatListEntities[layoutPosition].withType, chatListEntities[layoutPosition].withName))
                    context.startActivity(intent)
                }
            }

            vvSendVideo.setOnClickListener {
                if (longClick) {
                    if (chatListEntities[layoutPosition].isSelected) {
                        chatListener.onLongItemPressed(layoutPosition, false)
                    } else {
                        chatListener.onLongItemPressed(layoutPosition, true)
                    }
                } else {
                    val viewPosition = 0
                    /*val galleryImageURLs = ArrayList<String>()
                var i = 0
                for (g in chatListEntities) {
                    if (!(g.imageOriginalPath.isNullOrEmpty())){
                        galleryImageURLs.add(g.imageOriginalPath!!)
                        if (g.imageOriginalPath == chatListEntities[layoutPosition].imageOriginalPath) {
                            viewPosition = i
                        }
                        i++
                    }
                }*/
                    val intent = Intent(context, PlayVideo::class.java)
                    // intent.putStringArrayListExtra("videoUrls", galleryImageURLs)
                    intent.putExtra("videoUrl", chatListEntities[layoutPosition].imageOriginalPath)
                    intent.putExtra("index", viewPosition)
                    intent.putExtra("title", String.format("%s : %s", chatListEntities[layoutPosition].withType, chatListEntities[layoutPosition].withName))
                    context.startActivity(intent)
                }
            }

            vvReceivedVideos.setOnClickListener {
                val viewPosition = 0
                /*val galleryImageURLs = ArrayList<String>()
                var i = 0
                for (g in chatListEntities) {
                    if (!(g.imageOriginalPath.isNullOrEmpty())){
                        galleryImageURLs.add(g.imageOriginalPath!!)
                        if (g.imageOriginalPath == chatListEntities[layoutPosition].imageOriginalPath) {
                            viewPosition = i
                        }
                        i++
                    }
                }*/
                val intent = Intent(context, PlayVideo::class.java)
                // intent.putStringArrayListExtra("videoUrls", galleryImageURLs)
                intent.putExtra("videoUrl", chatListEntities[layoutPosition].localPath)
                intent.putExtra("index", viewPosition)
                intent.putExtra("title", String.format("%s : %s", chatListEntities[layoutPosition].withType, chatListEntities[layoutPosition].withName))
                context.startActivity(intent)
            }
        }
    }

    interface ChatClickListener {
        fun onLongItemPressed(position: Int, isSelected: Boolean)
        fun onResent(msgId: String)
        fun onClickDownload(url: String, msgId: String, progressBar: ProgressBar, fileType: String)
    }
}
