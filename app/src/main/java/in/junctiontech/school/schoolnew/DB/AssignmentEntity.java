package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.TypeConverters;
import androidx.annotation.NonNull;

import java.util.Date;

import in.junctiontech.school.schoolnew.common.Converters;


@Entity(primaryKeys = {"sectionid","subjectid","dateofhomework"})
public class AssignmentEntity {
    public String homeworkId;
    public String classid;
    @NonNull
    public String sectionid;
    @NonNull
    public String subjectid;
    public String homework;
    @NonNull
    @TypeConverters(Converters.class)
    public Date dateofhomework;
    @TypeConverters(Converters.class)
    public Date dosubmission ;
//    public String studentstatus;
    public String session;
    public String Image;
    public String createdon;
    public String createdby;
    public String updatedon;
    public String updatedby;
    public String ClassName;
    public String SectionName;
    public String SubjectName;
}
