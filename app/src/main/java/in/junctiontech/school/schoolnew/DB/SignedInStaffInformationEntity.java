package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class SignedInStaffInformationEntity {
    @PrimaryKey
    @NonNull
    public String StaffId;
    public String gcmToken;
    public String IMEI;
    public String UserDevice;
    public String Status;
    public String Staff_terms;
    public String StaffStatus;
    public String StaffPosition;
    public String StaffName;
    public String StaffMobile;
    public String staffProfileImage;
    public String StaffEmail;
    public String StaffAlternateMobile;
    public String StaffFName;
    public String StaffMName;
    public String StaffDOJ;
    public String StaffDOB;
    public String StaffPresentAddress;
    public String StaffPermanentAddress;
    public String StaffPositionValue;
}
