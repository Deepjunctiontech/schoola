package in.junctiontech.school.schoolnew.schoolcalender;

import android.app.SearchManager;
import androidx.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class SchoolCalenderActivity extends AppCompatActivity {

    private FloatingActionButton fb_event_calender_add;

    private ProgressBar progressBar;

    RecyclerView mRecyclerView;

    private ArrayList<SchoolCalenderEntity> calenderEventList = new ArrayList<>();
    HashMap<String,String> eventDays = new HashMap<>();
    private SchoolCalenderAdapter adapter;

    MainDatabase mDb ;

    CalendarAdapter calAdapter;
    RecyclerView rv_calendar;
    ArrayList<String> dayList = new ArrayList<>();

    TextView tv_display_month;
    Button btn_next_month, btn_prev_month;
    String firstdate;
    String monthYear;
    String lastdate;

    Date firstDayDate;
    Date lastDayDate;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_event_calender_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        btn_prev_month.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        btn_next_month.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        tv_display_month.setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_calender);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressBar  = findViewById(R.id.progressBar);

        fb_event_calender_add = findViewById(R.id.fb_event_calender_add);
        tv_display_month = findViewById(R.id.tv_display_month);
        btn_next_month = findViewById(R.id.btn_next_month);
        btn_next_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayMonth(1);
            }
        });
        btn_prev_month =  findViewById(R.id.btn_prev_month);
        btn_prev_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayMonth(-1);
            }
        });

        if (!(Gc.getSharedPreference(Gc.CREATEEVENTALLOWED, this).equalsIgnoreCase(Gc.TRUE))){
            fb_event_calender_add.hide();
        }
        fb_event_calender_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SchoolCalenderActivity.this, CreateEventCalendarActivity.class);
                startActivityForResult(intent, Gc.EVENT_CREATE_REQUEST_CODE);
            }
        });

         mRecyclerView = findViewById(R.id.rv_event_calender_list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new SchoolCalenderAdapter(this, calenderEventList);
        mRecyclerView.setAdapter(adapter);

        rv_calendar = findViewById(R.id.rv_calendar);
        RecyclerView.LayoutManager calLayoutManager = new GridLayoutManager(this, 7);
        rv_calendar.setLayoutManager(calLayoutManager);

        calAdapter = new CalendarAdapter();
        rv_calendar.setAdapter(calAdapter);

        monthYear = new SimpleDateFormat("MM-yyyy", Locale.US).format(new Date());
        firstdate = "01-" + monthYear;

        try {
            firstDayDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate);

            Calendar c = Calendar.getInstance();
            c.setTime(firstDayDate); // yourdate is an object of type Date

            String monthName = (String)android.text.format.DateFormat.format("MMMM", firstDayDate);
            int year = c.get(Calendar.YEAR);

            tv_display_month.setText(monthName + year);

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            int maxDays = c.getActualMaximum(Calendar.DATE);
            lastdate = maxDays+"-"+ new SimpleDateFormat("MM-yyyy", Locale.US).format(new Date()) ;

            buildCalenderForMonth(dayOfWeek, maxDays);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mDb.schoolCalenderModel().getAllCalenderEvents().observe(this, new Observer<List<SchoolCalenderEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolCalenderEntity> schoolCalenderEntities) {
                calenderEventList.clear();
                eventDays.clear();
                calenderEventList.addAll(schoolCalenderEntities);
                for (SchoolCalenderEntity s: calenderEventList){
                    eventDays.put(s.startdate, s.Color);
                }
                calAdapter.notifyDataSetChanged();
                adapter.notifyDataSetChanged();
                adapter.updateSearchItems(schoolCalenderEntities);

                mDb.schoolCalenderModel().getAllCalenderEvents().removeObservers(SchoolCalenderActivity.this);
            }
        });

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4197751804", this, adContainer);
        }
    }

    void buildCalenderForMonth(int dayOfWeek, int maxDays){
        dayList.clear();
        dayList.add("Sun");
        dayList.add("Mon");
        dayList.add("Tue");
        dayList.add("Wed");
        dayList.add("Thu");
        dayList.add("Fri");
        dayList.add("Sat");
        switch (dayOfWeek){
            case 1:
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 2:
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 3:
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 4:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 5:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 6:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 7:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
        }
        calAdapter.notifyDataSetChanged();

    }

    void displayMonth(int addmonth){
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate));

            c.add(Calendar.MONTH, addmonth);
            firstDayDate = c.getTime();

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            int maxDays = c.getActualMaximum(Calendar.DATE);

            c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
            lastDayDate = c.getTime();

            firstdate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(firstDayDate);
            lastdate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(lastDayDate);
            monthYear = new SimpleDateFormat("MM-yyyy", Locale.US).format(firstDayDate);

            String monthName = (String)android.text.format.DateFormat.format("MMMM", firstDayDate);
            int year = c.get(Calendar.YEAR);

            tv_display_month.setText(monthName + year);
            buildCalenderForMonth(dayOfWeek, maxDays);

            mDb.schoolCalenderModel().getAllCalenderEvents().observe(this, new Observer<List<SchoolCalenderEntity>>() {
                @Override
                public void onChanged(@Nullable List<SchoolCalenderEntity> schoolCalenderEntities) {
                    calenderEventList.clear();
                    eventDays.clear();
                    calenderEventList.addAll(schoolCalenderEntities);
                    for (SchoolCalenderEntity s: calenderEventList){
                        eventDays.put(s.startdate, s.Color);
                    }
                    calAdapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    adapter.updateSearchItems(schoolCalenderEntities);

                    mDb.schoolCalenderModel().getAllCalenderEvents().removeObservers(SchoolCalenderActivity.this);
                }
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));



        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.filter(s);
                return true ;
            }
        });
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Gc.EVENT_CREATE_REQUEST_CODE){
            if (resultCode == RESULT_OK) {

                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.ll_event_calender_activity),R.color.fragment_first_green);
            }
        }else
            super.onActivityResult(requestCode, resultCode, data);    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void updateEventCalander( SchoolCalenderEntity updateEvent){
        String event = new Gson().toJson(updateEvent);
        Intent intent = new Intent(this, CreateEventCalendarActivity.class);
        intent.putExtra("event", event);
        startActivityForResult(intent , Gc.EVENT_CREATE_REQUEST_CODE);
    }

    void deleteEventCalander(final SchoolCalenderEntity deleteEvent){

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "calendar/calendar"
                ;

        try {
            url = url + "/" + new JSONObject().put("CalendarId",deleteEvent.CalendarId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        progressBar.setVisibility(View.VISIBLE);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                Log.i("Fee Type ", job.toString());
                String code = job.optString("code");

                switch (code) {
                    case "200":

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolCalenderModel().deleteSchoolCalendar(deleteEvent);
                                    }
                                }).start();

                                     Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_event_calender_activity),R.color.fragment_first_green);

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.ll_event_calender_activity),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(SchoolCalenderActivity.this,error,findViewById(R.id.ll_event_calender_activity));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder>{

        @NonNull
        @Override
        public CalendarAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calender_date, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CalendarAdapter.MyViewHolder holder, int position) {
            String dateName = dayList.get(position);
            if (position < 7 ){
                holder.tv_cal_date.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                holder.tv_cal_date.setText(dateName);
            }else{
                if (dayList.get(position).equalsIgnoreCase("")){
                    holder.tv_cal_date.setBackground(null);
                    holder.tv_cal_date.setText(null);
                }else {
                    holder.tv_cal_date.setText(dateName);
                    int dateInt = Integer.parseInt(dateName);
                        if (dateInt>0 && dateInt < 10){
                        String key = "0"+dateInt+"-" + monthYear;
                        if (eventDays.containsKey(key)){
                            holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                        }else{
                            holder.tv_cal_date.setBackground(null);
                        }
                    }else{
                        String key = dateInt+"-"+ monthYear;
                        if (eventDays.containsKey(key)){
                            holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));

                        }else{
                            holder.tv_cal_date.setBackground(null);
                        }
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return dayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_cal_date;
            public MyViewHolder(View itemView) {
                super(itemView);
                tv_cal_date = itemView.findViewById(R.id.tv_cal_date);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = getAdapterPosition();
                        String dateName = dayList.get(position);
                        if (position < 7 ){
                            Toast.makeText(SchoolCalenderActivity.this,dayList.get(position),Toast.LENGTH_SHORT).show();
                        }else {
                            if (dayList.get(position).equalsIgnoreCase("")){
                                Toast.makeText(SchoolCalenderActivity.this,"Empty",Toast.LENGTH_SHORT).show();
                            }else{
                                int dateInt = Integer.parseInt(dateName);
                                final String key ;
                                if (dateInt>0 && dateInt < 10){
                                     key = "0"+dateInt+"-" + monthYear;
                                }else{
                                     key = dateInt+"-"+ monthYear;
                                }
                                mDb.schoolCalenderModel()
                                        .getDayCalenderEvents(key)
                                        .observe(SchoolCalenderActivity.this, new Observer<List<SchoolCalenderEntity>>() {
                                    @Override
                                    public void onChanged(@Nullable List<SchoolCalenderEntity> schoolCalenderEntities) {
                                        calenderEventList.clear();
                                        calenderEventList.addAll(schoolCalenderEntities);
                                        adapter.notifyDataSetChanged();
                                        adapter.updateSearchItems(schoolCalenderEntities);
                                        mDb.schoolCalenderModel()
                                                .getDayCalenderEvents(key).removeObserver(this);
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    }

}
