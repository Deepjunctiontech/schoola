package in.junctiontech.school.schoolnew;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.base.Strings;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.PasswordResetActivity;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ClassFeesEntity;
import in.junctiontech.school.schoolnew.DB.ClassFeesTransportEntity;
import in.junctiontech.school.schoolnew.DB.ExamTypeEntity;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.DB.OrganizationPermissionEntity;
import in.junctiontech.school.schoolnew.DB.SchoolAccountsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsClassEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.DB.SessionListEntity;
import in.junctiontech.school.schoolnew.DB.SignedInStaffInformationEntity;
import in.junctiontech.school.schoolnew.DB.SignedInUserInformationEntity;
import in.junctiontech.school.schoolnew.DB.TransportFeeAmountEntity;
import in.junctiontech.school.schoolnew.DB.UserPermissionsEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.model.AdminInformationEntity;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;
import in.junctiontech.school.schoolnew.model.OrganizationPermissionApi;
import in.junctiontech.school.schoolnew.model.SchoolClasses;
import in.junctiontech.school.schoolnew.model.SignedInUserInformation;
import in.junctiontech.school.schoolnew.model.StaffInformation;
import in.junctiontech.school.schoolnew.model.StudentInformation;
import in.junctiontech.school.schoolnew.selfregistration.StaffSelfRegistrationActivity;
import in.junctiontech.school.schoolnew.selfregistration.StudentSelfRegistrationActivity;
import in.junctiontech.school.schoolnew.setup.SetupActivity;

public class SignUpActivityNew extends AppCompatActivity {

    private static MainDatabase mDb;

    private TextInputLayout user_text, pass_text, organization_text;
    private EditText username, password, organizationname;
    private LinearLayout rl, ll_school_image;
    private ProgressDialog progressDialog;


    private TextView tv_sign_up_reset_password;
    private String blockCharacterSet = "#";
    ImageView civ_sign_up_page_school_logo;
    private TextView footer_url, footer_email, tv_lg_school_name;
    SchoolDetailsEntity schoolDetails;


    RadioGroup rg_type_login;
    private RadioButton radioButton;
    private LinearLayout loginSubmitOtp, loginSubmit;
    private Button btn_get_otp, btn_verify_otp, btn_resend_otp;
    private EditText otp_edit, user_mobile_edit, user_country_edit;
    private TextInputLayout otp_text;
    private PhoneAuthProvider phoneAuthProvider;
    private LinearLayout ll_login_type, ll_school_name;
    String mVerificationId, code, idToken;
    ArrayList<VerifyUsers> verifyUsersList = new ArrayList<>();
    private AlertDialog dialog;
    String invitationType;

    /**
     * Created by Junction Software on 17-Oct-15.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mDb = MainDatabase.getDatabase(getApplicationContext());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        civ_sign_up_page_school_logo = findViewById(R.id.civ_sign_up_page_school_logo);
        rl = findViewById(R.id.rl);
        ll_school_image = findViewById(R.id.ll_school_image);
        ll_login_type = findViewById(R.id.ll_login_type);

        footer_url = findViewById(R.id.footer_url);
        footer_email = findViewById(R.id.footer_email);
        ll_school_name = findViewById(R.id.ll_school_name);
        tv_lg_school_name = findViewById(R.id.tv_lg_school_name);


        otp_edit = findViewById(R.id.otp_edit);
        user_mobile_edit = findViewById(R.id.user_mobile_edit);
        user_country_edit = findViewById(R.id.user_country_edit);

        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, schoolDetailsEntity -> {
            if (schoolDetailsEntity != null) {
                schoolDetails = schoolDetailsEntity;
                if (footer_email != null)
                    footer_email.setText(Strings.nullToEmpty(schoolDetails.Email));
                if (footer_url != null)
                    footer_url.setText(Strings.nullToEmpty(schoolDetails.Website));

                if (!Strings.nullToEmpty(schoolDetails.SchoolName).equalsIgnoreCase("")) {
                    ll_school_name.setVisibility(View.VISIBLE);
                    tv_lg_school_name.setText(schoolDetails.SchoolName);
                }

                if (!Strings.nullToEmpty(schoolDetails.Mobile).equalsIgnoreCase("")) {
                    HashMap<String, String> split = Gc.splitPhoneNumber(Strings.nullToEmpty(schoolDetails.Mobile));
                    user_country_edit.setText(split.get("ccode"));
                }

            }

        });

        setSchoolLogo();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        user_text = findViewById(R.id.user_text);
        pass_text = findViewById(R.id.pass_text);
        organization_text = findViewById(R.id.organization_text);
        Button submitbutton = findViewById(R.id.btn_signup_login);

        username = findViewById(R.id.user_edit);
        username.setFilters(new InputFilter[]{(source, start, end, dest, dstart, dend) -> {


            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }

            return null;
        },
                new InputFilter.LengthFilter(getResources().getInteger(R.integer.normal_text))});


        password = findViewById(R.id.pass_edit);
        organizationname = findViewById(R.id.organization_edit);

        password.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                Config.hideKeyboard(SignUpActivityNew.this);
                submit();
                handled = true;
            }
            return handled;
        });

        organizationname.setText(Gc.getSharedPreference(Gc.ERPINSTCODE, this));
        organizationname.setEnabled(false);
        invitationType = getIntent().getStringExtra("invitationType");
        if (getIntent().getBooleanExtra("dataAvailable", false)) {
            username.setText(getIntent().getStringExtra("username"));
            password.setText(getIntent().getStringExtra("password"));
        }


        /* ****  When Coming from DEMO******/
        if (getIntent().getBooleanExtra(Gc.FROMDEMO, false)) {
            organizationname.setVisibility(View.INVISIBLE);
            username.setText(getIntent().getStringExtra(Gc.USERID));
            username.setVisibility(View.INVISIBLE);
            password.setText(getIntent().getStringExtra(Gc.PASSWORD));
            password.setVisibility(View.INVISIBLE);
            submitbutton.setText(getString(R.string.loading));

            submit();
        }


        /* **************  Reset Password   *****************/
        tv_sign_up_reset_password = findViewById(R.id.tv_sign_up_reset_password);
        tv_sign_up_reset_password.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_sign_up_reset_password.setOnClickListener(v -> {
            startActivity(new Intent(SignUpActivityNew.this, PasswordResetActivity.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        });
        /* **********/


        /* **************  Mobile Number Login   *****************/
        phoneAuthProvider = PhoneAuthProvider.getInstance();


        loginSubmitOtp = findViewById(R.id.loginSubmitOtp);
        loginSubmit = findViewById(R.id.loginSubmit);

        btn_get_otp = findViewById(R.id.btn_get_otp);
        btn_verify_otp = findViewById(R.id.btn_verify_otp);
        btn_resend_otp = findViewById(R.id.btn_resend_otp);


        otp_text = findViewById(R.id.otp_text);

        rg_type_login = findViewById(R.id.rg_type_login);
        rg_type_login.setOnCheckedChangeListener((group, checkedId) -> {
            radioButton = findViewById(checkedId);
            if (radioButton.getText().equals("Login With Mobile")) {
                loginSubmitOtp.setVisibility(View.VISIBLE);
                loginSubmit.setVisibility(View.GONE);
                btn_get_otp.setVisibility(View.VISIBLE);
                otp_edit.setText("");
                otp_text.setVisibility(View.GONE);
                btn_verify_otp.setVisibility(View.GONE);
                btn_resend_otp.setVisibility(View.GONE);
            } else if (radioButton.getText().equals("Login With Password")) {
                loginSubmit.setVisibility(View.VISIBLE);
                loginSubmitOtp.setVisibility(View.GONE);
                btn_get_otp.setVisibility(View.GONE);
                otp_edit.setText("");
                otp_text.setVisibility(View.GONE);
                btn_verify_otp.setVisibility(View.GONE);
                btn_resend_otp.setVisibility(View.GONE);

            } else {
                Toast.makeText(SignUpActivityNew.this, "Invalid Selection", Toast.LENGTH_LONG).show();
            }
        });
        /* **************  Mobile Number Login   *****************/
        setColorApp();


    }

    public void sendVerificationCode(View V) {
        Config.hideKeyboard(this);

        boolean b1 = isEmptyMobile();
        if (b1) {
            user_country_edit.setError(getString(R.string.country_code_cannot_be_blank));
            user_mobile_edit.setError(getString(R.string.mobile_number_cannot_be_blank));
        } else {

            String phoneNumber="+" + user_country_edit.getText() + user_mobile_edit.getText();

            PhoneAuthOptions options =
                    PhoneAuthOptions.newBuilder(FirebaseAuth.getInstance())
                            .setPhoneNumber(phoneNumber)       // Phone number to verify
                            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                            .setActivity(this)                 // Activity (for callback binding)
                            .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                            .build();
            PhoneAuthProvider.verifyPhoneNumber(options);
/*
            phoneAuthProvider.verifyPhoneNumber(
                    "+" + user_country_edit.getText() + user_mobile_edit.getText(),
                    120,
                    TimeUnit.SECONDS,
                    TaskExecutors.MAIN_THREAD,
                    mCallbacks);*/

            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.show();
        }

    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            code = phoneAuthCredential.getSmsCode();
            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                otp_edit.setText(code);
              /*  progressDialog.setMessage(getString(R.string.please_wait));
                progressDialog.show();*/
                verify();
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(SignUpActivityNew.this, e.getMessage(), Toast.LENGTH_LONG).show();
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.

            // Save verification ID and resending token so we can use them later

            Log.e("Token", verificationId);

            mVerificationId = verificationId;
            otp_text.setVisibility(View.VISIBLE);
            btn_get_otp.setVisibility(View.GONE);
            btn_verify_otp.setVisibility(View.VISIBLE);
            btn_resend_otp.setVisibility(View.VISIBLE);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    };

    public void verify(View v) {
        Config.hideKeyboard(this);
        verify();
    }

    private void verify() {
        boolean b1 = isEmptyMobile();
        boolean b2 = isEmptyOtp();
        boolean b3 = isEmptyOrganization();

        user_mobile_edit.setError(null);
        user_country_edit.setError(null);
        otp_edit.setError(null);
        organization_text.setError(null);

        if (b3 && b1 && b2) {
            Snackbar.make(rl, R.string.one_or_more_field_are_blank, Snackbar.LENGTH_LONG).setAction(R.string.dismiss, v -> {

            }).show();

        } else {
            if (b3 || b2 || b1) {
                if (b3) {
                    organization_text.setError(getString(R.string.Institute_Code_can_not_be_blank));
                }
                if (b1) {
                    user_country_edit.setError(getString(R.string.country_code_cannot_be_blank));
                    user_mobile_edit.setError(getString(R.string.mobile_number_cannot_be_blank));
                }
                if (b2) {
                    otp_edit.setError(getString(R.string.otp_cannot_be_blank));
                }
            } else {
                if (!isFinishing()) {
                    progressDialog.setMessage(getString(R.string.please_wait));
                    progressDialog.show();
                }
                try {
                    verifyVerificationCode();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void verifyVerificationCode() throws JSONException {

        final JSONObject param = new JSONObject();
        param.put("mobile", user_country_edit.getText().toString() + "-" + user_mobile_edit.getText().toString().replaceAll("#", ""));
        param.put("otp", otp_edit.getText().toString());
        param.put("token", mVerificationId);

        final String verifyUrl = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, SignUpActivityNew.this)
                + Gc.ERPAPIVERSION
                + "login/verifyPhoneNumber";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, verifyUrl, param,
                job -> {
                    otp_edit.setText("");
                    code = null;
                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200: {
                            try {
                                idToken = job.getJSONObject("result").optString("idToken");

                                final ArrayList<VerifyUsers> verifyUsers = new Gson().fromJson(job.getJSONObject("result").optString("users"), new TypeToken<List<VerifyUsers>>() {
                                }.getType());
                                if (verifyUsers.size() > 0) {
                                    verifyUsersList.clear();
                                    verifyUsersList.addAll(verifyUsers);
                                    if (verifyUsers.size() == 1) {
                                        phoneNumberLogin(0);
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivityNew.this);
                                        RecyclerView recyclerView = new RecyclerView(SignUpActivityNew.this);
                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SignUpActivityNew.this);
                                        recyclerView.setLayoutManager(layoutManager);
                                        recyclerView.setPadding(0, 20, 0, 20);
                                        VerifyUsersListAdapter userAdapter = new VerifyUsersListAdapter(verifyUsersList);
                                        recyclerView.setAdapter(userAdapter);
                                        builder.setTitle("Login into which user ?");
                                        builder.setInverseBackgroundForced(false);
                                        builder.setCancelable(false);

                                        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
                                            loginSubmitOtp.setVisibility(View.VISIBLE);
                                            loginSubmit.setVisibility(View.GONE);
                                            btn_get_otp.setVisibility(View.VISIBLE);
                                            otp_edit.setText("");
                                            otp_text.setVisibility(View.GONE);
                                            btn_verify_otp.setVisibility(View.GONE);
                                            btn_resend_otp.setVisibility(View.GONE);
                                            dialog.dismiss();
                                        });

                                        builder.setView(recyclerView);
                                        dialog = builder.create();
                                        dialog.show();
                                    }
                                }


                                if (progressDialog != null && progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case "202": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            try {
                                idToken = job.getJSONObject("result").optString("idToken");
                                boolean selfRegistration = job.getJSONObject("result").optBoolean("selfRegistration");
                                if (selfRegistration) {

                                    if (invitationType != null && !invitationType.isEmpty()) {

                                        if (Strings.nullToEmpty(invitationType).equalsIgnoreCase(getResources().getString(R.string.staff))) {

                                            Intent intent = new Intent(SignUpActivityNew.this, StaffSelfRegistrationActivity.class);
                                            intent.putExtra("mobile", user_country_edit.getText().toString() + "-"
                                                    + user_mobile_edit.getText().toString().replaceAll("#", ""));

                                            intent.putExtra("idToken", idToken);
                                            startActivityForResult(intent, 2011);
                                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                                        }

                                        if (Strings.nullToEmpty(invitationType).equalsIgnoreCase(getResources().getString(R.string.student))) {

                                            Intent studentIntent = new Intent(SignUpActivityNew.this, StudentSelfRegistrationActivity.class);
                                            studentIntent.putExtra("mobile", user_country_edit.getText().toString() + "-"
                                                    + user_mobile_edit.getText().toString().replaceAll("#", ""));
                                            studentIntent.putExtra("idToken", idToken);
                                            startActivityForResult(studentIntent, 2011);
                                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                                        }


                                    } else {
                                        AlertDialog.Builder choices = new AlertDialog.Builder(SignUpActivityNew.this);
                                        String[] aa = new String[]{
                                                getResources().getString(R.string.staff),
                                                getResources().getString(R.string.student)
                                        };
                                        choices.setTitle("Registration As ?");

                                        choices.setItems(aa, (dialogInterface, which) -> {
                                            switch (which) {
                                                case 0:
                                                    Intent intent = new Intent(SignUpActivityNew.this, StaffSelfRegistrationActivity.class);
                                                    intent.putExtra("mobile", user_country_edit.getText().toString() + "-"
                                                            + user_mobile_edit.getText().toString().replaceAll("#", ""));

                                                    intent.putExtra("idToken", idToken);
                                                    startActivityForResult(intent, 2011);
                                                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                                                    break;
                                                case 1:
                                                    Intent studentIntent = new Intent(SignUpActivityNew.this, StudentSelfRegistrationActivity.class);
                                                    studentIntent.putExtra("mobile", user_country_edit.getText().toString() + "-"
                                                            + user_mobile_edit.getText().toString().replaceAll("#", ""));
                                                    studentIntent.putExtra("idToken", idToken);
                                                    startActivityForResult(studentIntent, 2011);
                                                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                                                    break;
                                            }
                                        });
                                        choices.show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case "300": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);
                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                            });
                            alertAdmin.show();
                            break;
                        }
                        case Gc.APIRESPONSE401: {

                            Gc.logCrash(SignUpActivityNew.this, verifyUrl);

                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                                if (getIntent().getBooleanExtra(Gc.FROMDEMO, true)) {
                                    finish();
                                }
                            });
                            alertAdmin.show();
                            break;
                        }

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, SignUpActivityNew.this);
                            finish();
                        case "511": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertLogout = new AlertDialog.Builder(
                                    SignUpActivityNew.this, AlertDialog.THEME_TRADITIONAL);
                            alertLogout.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                            alertLogout.setTitle(getString(R.string.alert));
                            alertLogout.setMessage(job.optString("message") + "\n" +
                                    getString(R.string.contact_message_to_junction_tech));
                            alertLogout.setPositiveButton(R.string.ok, (dialog, which) -> {

                            });
                            alertLogout.setCancelable(false);
                            alertLogout.show();

                            break;
                        }
                        default:
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (!isFinishing()) {

                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                                alertAdmin.setMessage(job.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                                    loginSubmitOtp.setVisibility(View.VISIBLE);
                                    loginSubmit.setVisibility(View.GONE);
                                    btn_get_otp.setVisibility(View.VISIBLE);
                                    otp_edit.setText("");
                                    otp_text.setVisibility(View.GONE);
                                    btn_verify_otp.setVisibility(View.GONE);
                                    btn_resend_otp.setVisibility(View.GONE);
                                });
                                alertAdmin.show();
                            }
                    }

                },

                volleyError -> {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Config.responseVolleyErrorHandler(SignUpActivityNew.this, volleyError, rl);
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    private void phoneNumberLogin(int position) throws JSONException {

        SharedPreferences sharedPreferences = getSharedPreferences(Gc.NOTLOGINVALUES, MODE_PRIVATE);
        String token = sharedPreferences.getString(Gc.FCMTOKEN, "");

        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();

        final JSONObject param = new JSONObject();
        param.put("userId", verifyUsersList.get(position).userId);
        param.put("mobile", verifyUsersList.get(position).mobile);
        param.put("userType", verifyUsersList.get(position).userType);
        param.put("idToken", idToken);
        param.put("fcmtoken", token);
        Log.e("loginParam", param.toString());
        final String verifyUrl = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, SignUpActivityNew.this)
                + Gc.ERPAPIVERSION
                + "login/phoneNumberLogin";

        Log.e("verifyUrl", verifyUrl);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, verifyUrl, param,
                job -> {

                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200: {

                            try {
                                JSONObject resultjobj = job.getJSONObject("result");

                                String userName = resultjobj.getJSONObject("userInformation").optString("userName");

                                resultjobj.optString("userName");

                                HashMap<String, String> setDefaults = new HashMap<String, String>();
                                setDefaults.put(Gc.SIGNEDINUSERNAME, userName);
                                //setDefaults.put(Gc.SIGNEDINUSERPASSWORD, param.get("password").toString());
                                Gc.setSharedPreference(setDefaults, getApplicationContext());

                                saveSignUpDataToRoomDB(resultjobj);

                                getStudents();

                                getStaff();

                                getSchoolPublicDetails();

                                if (progressDialog != null && progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                checkSetupForCompletion();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            overridePendingTransition(R.anim.enter, R.anim.exit);

                            break;
                        }

                        case "300": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);
                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                            });
                            alertAdmin.show();
                            break;
                        }
                        case Gc.APIRESPONSE401: {

                            Gc.logCrash(SignUpActivityNew.this, verifyUrl);

                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                                if (getIntent().getBooleanExtra(Gc.FROMDEMO, true)) {
                                    finish();
                                }
                            });
                            alertAdmin.show();
                            break;
                        }

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, SignUpActivityNew.this);
                            finish();
                        case "511": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertLogout = new AlertDialog.Builder(
                                    SignUpActivityNew.this, AlertDialog.THEME_TRADITIONAL);
                            alertLogout.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                            alertLogout.setTitle(getString(R.string.alert));
                            alertLogout.setMessage(job.optString("message") + "\n" +
                                    getString(R.string.contact_message_to_junction_tech));
                            alertLogout.setPositiveButton(R.string.ok, (dialog, which) -> {

                            });
                            alertLogout.setCancelable(false);
                            alertLogout.show();

                            break;
                        }
                        default:
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {

                                loginSubmitOtp.setVisibility(View.VISIBLE);
                                loginSubmit.setVisibility(View.GONE);
                                btn_get_otp.setVisibility(View.VISIBLE);
                                otp_edit.setText("");
                                otp_text.setVisibility(View.GONE);
                                btn_verify_otp.setVisibility(View.GONE);
                                btn_resend_otp.setVisibility(View.GONE);

                            });
                            alertAdmin.show();
                    }

                },

                volleyError -> {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Config.responseVolleyErrorHandler(SignUpActivityNew.this, volleyError, rl);
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));
                Log.e("loginHeader", headers.toString());
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((Gc.getSharedPreference(Gc.LOGIN_OPTIONS, this).equalsIgnoreCase("Mobile"))) {
            ll_login_type.setVisibility(View.GONE);
        }
    }

    private void setColorApp() {
        // FOR NAVIGATION VIEW ITEM TEXT COLOR
        int colorIs = Config.getAppColor(this, true);
        tv_sign_up_reset_password.setTextColor(colorIs);
        findViewById(R.id.btn_signup_login).setBackgroundColor(colorIs);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {

        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);
        if (permsRequestCode == Config.IMAGE_LOGO_CODE) {

            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //denied
                        Log.e("denied", permissions[0]);
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                            //allowed
                            Log.e("allowed", permissions[0]);
                            setSchoolLogo();

                        } else {
                            //set to never ask again
                            Log.e("set to never ask again", permissions[0]);
                            //do something here.
                            showRationale(getString(R.string.permission_denied), getString(
                                    R.string.permission_denied_external_storage));

                        }
                    }
                } else {
                    setSchoolLogo();

                }
            }


        }


    }

    private void setSchoolLogo() {

        if (!(Gc.getSharedPreference(Gc.LOGO_URL, this).equalsIgnoreCase("")))

            Glide.with(this)
                    .load(Gc.getSharedPreference(Gc.LOGO_URL, this))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                    .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                    .apply(RequestOptions.circleCropTransform())
                    .into(civ_sign_up_page_school_logo);


        if (!(Gc.getSharedPreference(Gc.SCHOOLIMAGE, this).equalsIgnoreCase("")))

            Glide.with(this)
                    .load(Gc.getSharedPreference(Gc.SCHOOLIMAGE, this))
                    .apply(RequestOptions.placeholderOf(R.drawable.s1))
                    .apply(RequestOptions.errorOf(R.drawable.s1))
                    .apply(new RequestOptions().fitCenter())
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ll_school_image.setBackground(resource);
                        }
                    });
    }

    private void showRationale(String permission, String permission_denied_location) {
        androidx.appcompat.app.AlertDialog.Builder alertLocationInfo = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, Config.LOCATION);
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), (dialogInterface, i) -> {

        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 2011) {
            if (!Objects.requireNonNull(data.getStringExtra("idToken")).isEmpty()) {
                idToken = data.getStringExtra("idToken");
                JSONArray usersArrList = new JSONArray();
                JSONObject users = new JSONObject();
                try {
                    users.put("userId", data.getStringExtra("userId"));
                    users.put("name", data.getStringExtra("name"));
                    users.put("mobile", data.getStringExtra("mobile"));
                    users.put("userType", data.getStringExtra("userType"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                usersArrList.put(users);
                try {
                    final ArrayList<VerifyUsers> verifyUser = new Gson().fromJson(usersArrList.toString(), new TypeToken<List<VerifyUsers>>() {
                    }.getType());
                    verifyUsersList.clear();
                    verifyUsersList.addAll(verifyUser);
                    phoneNumberLogin(0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void submit(View v) {
        Config.hideKeyboard(this);
        submit();
    }

    private void submit() {

        boolean b1 = isEmptyEmail();
        boolean b2 = isEmptyPassword();
        boolean b3 = isEmptyOrganization();

        user_text.setError(null);
        pass_text.setError(null);
        organization_text.setError(null);

        if (b3 && b1 && b2) {
            Snackbar.make(rl, R.string.one_or_more_field_are_blank, Snackbar.LENGTH_LONG).setAction(R.string.dismiss, v -> {

            }).show();

        } else {
            if (b3 || b2 || b1) {
                if (b3) {
                    organization_text.setError(getString(R.string.Institute_Code_can_not_be_blank));
                }
                if (b1) {
                    user_text.setError(getString(R.string.login_id_can_not_be_blank));
                }
                if (b2) {
                    pass_text.setError(getString(R.string.password_can_not_be_blank));
                }
            } else {

                progressDialog.setMessage(getString(R.string.please_wait));
                progressDialog.show();
                try {
                    loginToSchool();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void loginToSchool() throws JSONException {

        SharedPreferences sharedPreferences = getSharedPreferences(Gc.NOTLOGINVALUES, MODE_PRIVATE);
        String token = sharedPreferences.getString(Gc.FCMTOKEN, "");

        final JSONObject param = new JSONObject();
        param.put("username", username.getText().toString().replaceAll("#", ""));
        param.put("password", password.getText().toString());
        param.put("fcmtoken", token);

        final String loginUrl = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, SignUpActivityNew.this)
                + Gc.ERPAPIVERSION
                + "login/login";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, loginUrl, param,
                job -> {

                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200: {

                            try {
                                JSONObject resultjobj = job.getJSONObject("result");

                                HashMap<String, String> setDefaults = new HashMap<String, String>();
                                setDefaults.put(Gc.SIGNEDINUSERNAME, param.get("username").toString());
                                setDefaults.put(Gc.SIGNEDINUSERPASSWORD, param.get("password").toString());
                                Gc.setSharedPreference(setDefaults, getApplicationContext());

                                saveSignUpDataToRoomDB(resultjobj);

                                getStudents();

                                getStaff();

                                getSchoolPublicDetails();

                                if (progressDialog != null && progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                                checkSetupForCompletion();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            break;
                        }

                        case "300": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);
                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                            });
                            alertAdmin.show();
                            break;
                        }
                        case Gc.APIRESPONSE401: {

                            Gc.logCrash(SignUpActivityNew.this, loginUrl);

                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                                if (getIntent().getBooleanExtra(Gc.FROMDEMO, true)) {
                                    finish();
                                }
                            });
                            alertAdmin.show();
                            break;
                        }

                        case Gc.APIRESPONSE500:

                            HashMap<String, String> setDefaults = new HashMap<>();
                            setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                            Gc.setSharedPreference(setDefaults, SignUpActivityNew.this);
                            finish();
                        case "511": {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertLogout = new AlertDialog.Builder(
                                    SignUpActivityNew.this, AlertDialog.THEME_TRADITIONAL);
                            alertLogout.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                            alertLogout.setTitle(getString(R.string.alert));
                            alertLogout.setMessage(job.optString("message") + "\n" +
                                    getString(R.string.contact_message_to_junction_tech));
                            alertLogout.setPositiveButton(R.string.ok, (dialog, which) -> {

                            });
                            alertLogout.setCancelable(false);
                            alertLogout.show();

                            break;
                        }
                        default:
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivityNew.this);

                            alertAdmin.setMessage(job.optString("message"));
                            alertAdmin.setPositiveButton(R.string.ok, (dialog, which) -> {
                            });
                            alertAdmin.show();
                    }

                },

                volleyError -> {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Config.responseVolleyErrorHandler(SignUpActivityNew.this, volleyError, rl);
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    private void getStudents() {
        final JSONObject param = new JSONObject();
        try {
            param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "studentParent/studentRegistration/"
                + param;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case "200":

                    AsyncTaskRunner runner = new AsyncTaskRunner();
                    AppRequestQueueController.getInstance(getApplicationContext()).addToAsyncTask(runner);
                    runner.execute(job);

                    break;

                default:
                    Log.e("SignupActivityResponse", job.optString("code"));


            }
        }, error -> {
            Log.e("SignupActivityError", "API ERROR Saving Student failed");

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void getStaff() {

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "staff/staffRegistration";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            String code = job.optString("code");

            switch (code) {
                case "200":
                    try {
                        JSONArray staffjarr = job.getJSONObject("result").getJSONArray("staffDetails");
                        final ArrayList<SchoolStaffEntity> schoolStaffEntities =
                                new Gson().fromJson(staffjarr.toString(), new TypeToken<List<SchoolStaffEntity>>() {
                                }
                                        .getType());


                        new Thread(() -> {
                            mDb.schoolStaffModel().insertStaff(schoolStaffEntities);
                        }).start();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                default:
                    Log.e("SignupActivitySaveStaff", job.optString("message"));

            }
        }, error -> {
            Log.e("SignupActivity", "API ERROR Saving staff failed");


        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void getSchoolPublicDetails() {

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "public/schoolPublicDetails";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(),
                job -> {
                    switch (job.optString("code")) {
                        case Gc.APIRESPONSE200:
                            try {

                                JSONObject resultJsonObject = job.getJSONObject("result");
                                JSONArray noticeJsonArray = resultJsonObject.getJSONArray("noticeBoards");
                                final ArrayList<NoticeBoardEntity> noticeBoardEntities = new Gson().fromJson(noticeJsonArray.toString(), new TypeToken<List<NoticeBoardEntity>>() {
                                }.getType());
                                if (noticeBoardEntities.size() > 0) {
                                    new Thread(() -> mDb.noticeBoardModel().insertNotice(noticeBoardEntities)).start();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;

                        case Gc.APIRESPONSE500:
                            androidx.appcompat.app.AlertDialog.Builder alert1 = new androidx.appcompat.app.AlertDialog.Builder(SignUpActivityNew.this);
                            alert1.setMessage(Html.fromHtml("<Br>" + "Your licence is expired !<Br>For further usage contact on<Br>"
                                    + "<B>Email</B> : <font color='#ce001f'>info@zeroerp.com</font><Br><B>Or Visit</B> :<font color='#ce001f'>www.zeroerp.com</font>"));
                            alert1.setTitle(getString(R.string.information));
                            alert1.setIcon(R.drawable.ic_clickhere);
                            alert1.setPositiveButton(getString(R.string.logout), (dialog, which) -> {
                            });
                            alert1.show();
                    }
                },
                error -> {
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(req);
    }

    private boolean isEmptyOrganization() {
        return organizationname.getText() == null || organizationname.getText() == null
                || organizationname.getText().toString().equals("") || organizationname.getText().toString().isEmpty();
    }

    private boolean isEmptyEmail() {
        return username.getText() == null || username.getText().toString() == null
                || username.getText().toString().equals("") || username.getText().toString().isEmpty();
    }

    private boolean isEmptyMobile() {
        return user_mobile_edit.getText() == null || user_mobile_edit.getText().toString() == null
                || user_mobile_edit.getText().toString().equals("") || user_mobile_edit.getText().toString().isEmpty()
                || user_country_edit.getText() == null || user_country_edit.getText().toString() == null
                || user_country_edit.getText().toString().equals("") || user_country_edit.getText().toString().isEmpty();
    }

    private boolean isEmptyPassword() {
        return password.getText() == null || password.getText().toString() == null
                || password.getText().toString().equals("") || password.getText().toString().isEmpty();
    }

    private boolean isEmptyOtp() {
        return otp_edit.getText() == null || otp_edit.getText().toString() == null
                || otp_edit.getText().toString().equals("") || otp_edit.getText().toString().isEmpty();
    }

    @SuppressLint("WrongThread")
    @Override
    public void onBackPressed() {
        Gc.removeAll(this);
        mDb.clearAllTables();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
        super.onBackPressed();
    }

    private void mapSignedInUserInformationToEntity(SignedInUserInformation signedInUserInformation) {
        SignedInUserInformationEntity signedInUserInformationEntity = new SignedInUserInformationEntity();
        signedInUserInformationEntity.userID = signedInUserInformation.userID;
        signedInUserInformationEntity.userName = signedInUserInformation.userName;
        signedInUserInformationEntity.userType = signedInUserInformation.userType;
        signedInUserInformationEntity.userTypeValue = signedInUserInformation.userTypeValue;
        signedInUserInformationEntity.accessToken = signedInUserInformation.accessToken;
        mDb.signedInUserInformationModel().insertSignedInUserInformation(signedInUserInformationEntity);
    }

    private void mapSignedInStaffInformationToEntity(StaffInformation staffInformation) {
        final SignedInStaffInformationEntity signedInStaffInformationEntity = new SignedInStaffInformationEntity();
        signedInStaffInformationEntity.StaffId = staffInformation.StaffId;
        signedInStaffInformationEntity.gcmToken = staffInformation.gcmToken;
        signedInStaffInformationEntity.IMEI = staffInformation.IMEI;
        signedInStaffInformationEntity.Status = staffInformation.Status;
        signedInStaffInformationEntity.UserDevice = staffInformation.UserDevice;
        signedInStaffInformationEntity.Staff_terms = staffInformation.Staff_terms;
        signedInStaffInformationEntity.StaffStatus = staffInformation.StaffStatus;
        signedInStaffInformationEntity.StaffPosition = staffInformation.StaffPosition;
        signedInStaffInformationEntity.StaffName = staffInformation.StaffName;
        signedInStaffInformationEntity.StaffMobile = staffInformation.StaffMobile;
        signedInStaffInformationEntity.staffProfileImage = staffInformation.staffProfileImage;
        signedInStaffInformationEntity.StaffEmail = staffInformation.StaffEmail;
        signedInStaffInformationEntity.StaffAlternateMobile = staffInformation.StaffAlternateMobile;
        signedInStaffInformationEntity.StaffFName = staffInformation.StaffFName;
        signedInStaffInformationEntity.StaffMName = staffInformation.StaffMName;
        signedInStaffInformationEntity.StaffDOJ = staffInformation.StaffDOJ;
        signedInStaffInformationEntity.StaffDOB = staffInformation.StaffDOB;
        signedInStaffInformationEntity.StaffPresentAddress = staffInformation.StaffPresentAddress;
        signedInStaffInformationEntity.StaffPermanentAddress = staffInformation.StaffPermanentAddress;
        signedInStaffInformationEntity.StaffPositionValue = staffInformation.StaffPositionValue;

        String signedinstaff = new Gson().toJson(signedInStaffInformationEntity);
        HashMap<String, String> setDefaults = new HashMap<String, String>();
        setDefaults.put(Gc.SIGNEDINSTAFF, signedinstaff);

        Gc.setSharedPreference(setDefaults, getApplicationContext());


        new Thread(() -> {
            mDb.signedInStaffInformationModel().insertSignedInStaffInformation(signedInStaffInformationEntity);
        }).start();

    }

    private void saveSignUpDataToRoomDB(JSONObject resultJobObj) {

        try {

            HashMap<String, String> setDefaults = new HashMap<String, String>();
            setDefaults.put(Gc.LICENCE, resultJobObj.getString("license"));
            setDefaults.put(Gc.STUDENTCOUNT, resultJobObj.getString("studentCount"));
            setDefaults.put(Gc.SMSBALANCE, resultJobObj.getString("SMSBalance"));


            JSONObject userInformationJob = resultJobObj.getJSONObject("userInformation");
            SignedInUserInformation signedInUserInformation = new Gson().fromJson(userInformationJob.toString(), SignedInUserInformation.class);

            mapSignedInUserInformationToEntity(signedInUserInformation);

            setDefaults.put(Gc.ACCESSTOKEN, signedInUserInformation.accessToken);
            setDefaults.put(Gc.USERTYPECODE, signedInUserInformation.userType);
            setDefaults.put(Gc.USERTYPETEXT, signedInUserInformation.userTypeValue);
            setDefaults.put(Gc.USERID, signedInUserInformation.userID);

            Gc.setSharedPreference(setDefaults, this);

            if (signedInUserInformation.Staff != null) {
                StaffInformation staffInformation = new StaffInformation();
                staffInformation = signedInUserInformation.Staff;
                setDefaults.put(Gc.STAFFID, staffInformation.StaffId);
                setDefaults.put(Gc.SIGNEDINUSERROLE, Gc.STAFF);
                setDefaults.put(Gc.PROFILE_URL, staffInformation.staffProfileImage);
                setDefaults.put(Gc.SIGNEDINUSERDISPLAYNAME, staffInformation.StaffName);
                mapSignedInStaffInformationToEntity(staffInformation);
            } else if (signedInUserInformation.Admin != null) {
                AdminInformationEntity adminInformationEntity = new AdminInformationEntity();
                mDb.signedInAdminInformationModel().insertSignedInAdminInformation(adminInformationEntity);
                setDefaults.put(Gc.SIGNEDINUSERROLE, Gc.ADMIN);
                setDefaults.put(Gc.SIGNEDINUSERDISPLAYNAME, "Administrator");

            } else if (signedInUserInformation.Parent != null || signedInUserInformation.Student != null) {

                setDefaults.put(Gc.SIGNEDINUSERROLE, Gc.STUDENTPARENT);
                if (signedInUserInformation.userTypeValue.equalsIgnoreCase("Parents")) {
                    final StudentInformation studentInformation = signedInUserInformation.Parent;

                    new Thread(() -> {
                        mDb.signedInStudentInformationModel().insertSignedInStudentInformation(studentInformation);
                    }).start();

                    setDefaults.put(Gc.PROFILE_URL, studentInformation.studentProfileImage);
                    setDefaults.put(Gc.SIGNEDINUSERDISPLAYNAME, studentInformation.StudentName);
                    setDefaults.put(Gc.STUDENTADMISSIONID, studentInformation.AdmissionId);
                    setDefaults.put(Gc.SIGNEDINSTUDENT, new Gson().toJson(studentInformation));

                } else if (signedInUserInformation.userTypeValue.equalsIgnoreCase("Student")) {
                    final StudentInformation studentInformation = signedInUserInformation.Student;
                    new Thread(() -> {
                        mDb.signedInStudentInformationModel().insertSignedInStudentInformation(studentInformation);
                    }).start();

                    setDefaults.put(Gc.PROFILE_URL, studentInformation.studentProfileImage);
                    setDefaults.put(Gc.SIGNEDINUSERDISPLAYNAME, studentInformation.StudentName);
                    setDefaults.put(Gc.STUDENTADMISSIONID, studentInformation.AdmissionId);
                    setDefaults.put(Gc.SIGNEDINSTUDENT, new Gson().toJson(studentInformation));
                }
            } else {
                Log.e("FatalError", "User information not fetched");
            }

            Gc.setSharedPreference(setDefaults, this);

            final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(resultJobObj.optString("schoolDetails"), SchoolDetailsEntity.class);

            if (Strings.nullToEmpty(schoolDetailsEntity.SchoolName).isEmpty() || Strings.nullToEmpty(schoolDetailsEntity.Email).isEmpty()) {
                setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.NOTFOUND);
            } else {
                setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.EXISTS);
                new Thread(() -> {
                    mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);
                }).start();
            }


            JSONObject academicSessionsjobj = resultJobObj.getJSONObject("academicSessions");
            final ArrayList<SessionListEntity> sessionListEntities = new Gson()
                    .fromJson(resultJobObj.getJSONObject("academicSessions")
                            .optString("sessionList"), new TypeToken<List<SessionListEntity>>() {
                    }.getType());

            if (sessionListEntities != null) {
                setDefaults.put(Gc.SESSIONEXISTS, Gc.EXISTS);
                for (SessionListEntity session : sessionListEntities) {
                    if (academicSessionsjobj.getString("currentSession").equalsIgnoreCase(session.session)) {
                        setDefaults.put(Gc.CURRENTSESSIONSTART, Gc.convertDate(session.sessionStartDate));
                        setDefaults.put(Gc.CURRENTSESSIONEND, Gc.convertDate(session.sessionEndDate));
                        setDefaults.put(Gc.CURRENTSESSION, session.session);
                        setDefaults.put(Gc.APPSESSION, session.session);
                        setDefaults.put(Gc.APPSESSIONSTART, session.sessionStartDate);
                        setDefaults.put(Gc.APPSESSIONEND, session.sessionEndDate);
                    }
                }
                Gc.setSharedPreference(setDefaults, this);

                new Thread(() -> {
                    mDb.sessionListModel().insertSessionList(sessionListEntities);
                }).start();
            }

            final ArrayList<FeeTypeEntity> feeTypeEntities = new Gson()
                    .fromJson(resultJobObj.optString("feeTypes"),
                            new TypeToken<List<FeeTypeEntity>>() {
                            }.getType());

            if (feeTypeEntities != null) {

                if (feeTypeEntities.size() > 0) {
                    setDefaults.put(Gc.FEETYPEEXISTS, Gc.EXISTS);
                    Gc.setSharedPreference(setDefaults, this);
                }

                new Thread(() -> {
                    mDb.feeTypeModel().insertFeeTypes(feeTypeEntities);
                }).start();
            }


            JSONArray organizationPermissionsjarr = resultJobObj.getJSONArray("organizationPermissions");
            OrganizationPermissionApi organizationPermissions = new OrganizationPermissionApi();

            final ArrayList<String> orgPermissions = new Gson()
                    .fromJson(resultJobObj.optString("organizationPermissions"),
                            new TypeToken<List<String>>() {
                            }.getType());

            if (orgPermissions != null) {
                new Thread(() -> {

                    ArrayList<OrganizationPermissionEntity> orgpar = new ArrayList<>();
                    for (String s : orgPermissions) {
                        orgpar.add(new OrganizationPermissionEntity(s));
                    }

                    mDb.organizationPermissionModel().insertOrganizationPermission(orgpar);
                }).start();
            }

            final ArrayList<String> userPer = new Gson()
                    .fromJson(resultJobObj.optString("userPermissions"), new TypeToken<List<String>>() {
                    }.getType());

            if (userPer != null) {
                Gc.saveArrayList(userPer, Gc.SIGNEDINUSERPERMISSION, this);
                new Thread(() -> {
                    ArrayList<UserPermissionsEntity> userPermissions = new ArrayList<>();
                    for (String s : userPer) {
                        userPermissions.add(new UserPermissionsEntity(s));
                    }
                    mDb.userPermissionsModel().insertUserPermission(userPermissions);
                }).start();
            }

            ArrayList<SchoolClasses> schoolClassesArrayList = new Gson()
                    .fromJson(resultJobObj.optString("classSections"),
                            new TypeToken<List<SchoolClasses>>() {
                            }.getType());
            final ArrayList<SchoolClassEntity> classEntities = new Gson()
                    .fromJson(resultJobObj.optString("classSections"),
                            new TypeToken<List<SchoolClassEntity>>() {
                            }.getType());

            final ArrayList<SchoolClassSectionEntity> sectionEntities = new ArrayList<>();

            for (SchoolClasses cl : schoolClassesArrayList) {

                sectionEntities.addAll(cl.section);
            }

            if (classEntities.size() > 0) {
                setDefaults.put(Gc.CLASSEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
            }

            new Thread(() -> {
                mDb.schoolClassModel().insertSchoolClasses(classEntities);
                mDb.schoolClassSectionModel().insertSchoolClassSection(sectionEntities);
            }).start();


            JSONArray classFeesjarr = resultJobObj.getJSONArray("classFees");
            if (classFeesjarr.length() > 0) {
                setDefaults.put(Gc.CLASSFEESEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);

                final ArrayList<ClassFeesEntity> classFeesEntities = MapModelToEntity.mapClassFeeModelToEntity(classFeesjarr);
                final ArrayList<ClassFeesTransportEntity> classFeesTransportEntities = MapModelToEntity.mapClassFeeTransportToEntity(classFeesjarr);

                new Thread(() -> {
                    mDb.classFeesModel().insertClassFee(classFeesEntities);
                    mDb.classFeesTransportModel().insertClassFee(classFeesTransportEntities);
                }).start();
            }


            final ArrayList<TransportFeeAmountEntity> transportAmounts = new Gson()
                    .fromJson(resultJobObj.getJSONObject("transportFeeAmount").optString("Fees"),
                            new TypeToken<List<TransportFeeAmountEntity>>() {
                            }.getType());

            if (transportAmounts != null && transportAmounts.size() > 0) {
                String transportFeeType = resultJobObj.getJSONObject("transportFeeAmount").optString("Type");

                if (!(Strings.nullToEmpty(transportFeeType).equalsIgnoreCase(""))) {
                    setDefaults.put(Gc.TRANSPORTFEETYPE, transportFeeType);
                } else {
                    setDefaults.put(Gc.TRANSPORTFEETYPE, Gc.NOTFOUND);
                }

                new Thread(() -> {
                    mDb.transportFeeAmountModel().deleteAll();
                    mDb.transportFeeAmountModel().insertTransportFee(transportAmounts);
                }).start();
            } else {
                setDefaults.put(Gc.TRANSPORTFEETYPE, Gc.NOTFOUND);
            }
            Gc.setSharedPreference(setDefaults, this);


            final ArrayList<SchoolSubjectsEntity> subjectList = new Gson()
                    .fromJson(resultJobObj.optString("subjects"),
                            new TypeToken<List<SchoolSubjectsEntity>>() {
                            }.getType());

            if (subjectList != null && subjectList.size() > 0) {
                setDefaults.put(Gc.SUBJECTEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
                new Thread(() -> {
                    mDb.schoolSubjectsModel().insertSubject(subjectList);
                }).start();
            }


            final ArrayList<SchoolSubjectsClassEntity> schoolSubjectsClass =
                    new Gson().fromJson(resultJobObj.optString("subjectSectionMapping"), new TypeToken<List<SchoolSubjectsClassEntity>>() {
                    }.getType());

            if (schoolSubjectsClass != null && schoolSubjectsClass.size() > 0) {

                setDefaults.put(Gc.SUBJECTCLASSEXISTS, Gc.EXISTS);

                new Thread(() -> {
                    mDb.schoolSubjectsClassModel().insertSubJectClassMapping(schoolSubjectsClass);
                }).start();
            }

            final ArrayList<SchoolAccountsEntity> schoolAccountsEntities = new Gson()
                    .fromJson(resultJobObj.optString("accounts"),
                            new TypeToken<List<SchoolAccountsEntity>>() {
                            }.getType());

            if (schoolAccountsEntities != null && schoolAccountsEntities.size() > 0) {
                setDefaults.put(Gc.ACCOUNTEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, this);
                new Thread(() -> {
                    mDb.schoolAccountsModel().insertSchoolAccount(schoolAccountsEntities);
                }).start();
            }


            final ArrayList<MasterEntryEntity> masterEntries = new Gson()
                    .fromJson(resultJobObj.optString("masterEntry"),
                            new TypeToken<List<MasterEntryEntity>>() {
                            }.getType());

            if (masterEntries != null && masterEntries.size() > 0) {
                new Thread(() -> {
                    mDb.masterEntryModel().insertMasterEntries(masterEntries);
                }).start();
            }


            final ArrayList<ExamTypeEntity> examTypes = new Gson()
                    .fromJson(resultJobObj.optString("examTypes"),
                            new TypeToken<List<ExamTypeEntity>>() {
                            }.getType());

            if (examTypes != null && examTypes.size() > 0) {
                new Thread(() -> {
                    mDb.examTypeModel().insertExamTypes(examTypes);
                }).start();
            }


            final ArrayList<SchoolCalenderEntity> schoolCalenderEntities = new Gson()
                    .fromJson(resultJobObj.optString("calendar"),
                            new TypeToken<List<SchoolCalenderEntity>>() {
                            }.getType());

            if (schoolCalenderEntities != null && schoolCalenderEntities.size() > 0) {
                setDefaults.put(Gc.CALENDEREXISTS, Gc.EXISTS);
                new Thread(() -> {
                    mDb.schoolCalenderModel().insertSchoolCalender(schoolCalenderEntities);
                }).start();
            }

            final ArrayList<CommunicationPermissions> communicationPermissions =
                    new Gson().fromJson(resultJobObj.optString("communicationPermissions"),
                            new TypeToken<List<CommunicationPermissions>>() {
                            }.getType());

            if (communicationPermissions != null && communicationPermissions.size() > 0) {
                new Thread(() -> mDb.communicationPermissionsModel().insertCommunicationPermissions(communicationPermissions)).start();

            }
            Gc.setSharedPreference(setDefaults, this);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkSetupForCompletion() {

        Intent intent = new Intent(SignUpActivityNew.this, SetupActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);


    }

    private static class AsyncTaskRunner extends AsyncTask<JSONObject, String, String> {
        ArrayList<SchoolStudentEntity> schoolStudentEntities = new ArrayList<>();

        @Override
        protected String doInBackground(JSONObject... job) {

            try {
                JSONObject resultjobj = job[0].getJSONObject("result");
                final JSONArray studentsjarr = resultjobj.getJSONArray("studentDetails");

                schoolStudentEntities = new Gson().fromJson(studentsjarr.toString(), new TypeToken<List<SchoolStudentEntity>>() {
                }.getType());
                Log.i("Student are", schoolStudentEntities.size() + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new Thread(() -> {
                if (!isCancelled()) {
                    mDb.schoolStudentModel().insertStudents(schoolStudentEntities);
                }
            }).start();
        }
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onDestroy();

    }

    public class VerifyUsersListAdapter extends RecyclerView.Adapter<VerifyUsersListAdapter.MyViewHolder> {
        private ArrayList<VerifyUsers> verifiedUsers;

        VerifyUsersListAdapter(ArrayList<VerifyUsers> usersList) {
            this.verifiedUsers = usersList;
        }

        @NonNull
        @Override
        public VerifyUsersListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.layout_user, parent, false);
            return new VerifyUsersListAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final VerifyUsersListAdapter.MyViewHolder holder, int position) {
            VerifyUsers user = verifiedUsers.get(position);
            String upperString = user.userType.substring(0, 1).toUpperCase() + user.userType.substring(1);

            holder.tv_users_type.setText(upperString);
            holder.tv_users_name.setText(user.name);
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return verifiedUsers.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private final TextView tv_users_type, tv_users_name;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_users_type = itemView.findViewById(R.id.tv_users_type);
                tv_users_name = itemView.findViewById(R.id.tv_users_name);
                itemView.setOnClickListener(v -> {
                    dialog.dismiss();
                    try {
                        phoneNumberLogin(getLayoutPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }
}

class VerifyUsers {
    String userId;
    public String name;
    public String mobile;
    public String userType;

}
