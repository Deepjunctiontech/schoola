package in.junctiontech.school.schoolnew.feesetup.feetype;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.FeeTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.common.MapModelToEntity;
import in.junctiontech.school.schoolnew.model.FeeType;

public class FeeTypeActivity extends AppCompatActivity {

    MainDatabase mDb;
    private FeeTypeAdapter adapter;
    private ArrayList<FeeTypeEntity> feeList = new ArrayList<>();

    FloatingActionButton fb_feetype_add;

    private ProgressDialog progressBar;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_feetype_add.setBackgroundTintList(ColorStateList.valueOf(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_create_fee_type);

        mDb = MainDatabase.getDatabase(this);

        if (Gc.getSharedPreference(Gc.FEETYPEEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view_fee_type_list);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new FeeTypeAdapter(this, feeList);
        mRecyclerView.setAdapter(adapter);


        fb_feetype_add = findViewById(R.id.fb_feetype_add);
        fb_feetype_add.setOnClickListener(view -> {
            Intent intent = new Intent(FeeTypeActivity.this, FeeTypeCreateActivity.class);
            startActivityForResult(intent, Gc.FEE_CREATE_REQUEST_CODE);
        });

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage(getString(R.string.please_wait));

        mDb.feeTypeModel().getFeeTypes().observe(this, feeTypeEntities -> {
            feeList.clear();
            if (feeTypeEntities.size() > 0) {
                HashMap<String, String> setDefaults = new HashMap<>();
                setDefaults.put(Gc.FEETYPEEXISTS, Gc.EXISTS);
                Gc.setSharedPreference(setDefaults, FeeTypeActivity.this);
            }
//            else {
//                HashMap<String,String> setDefaults = new HashMap<>();
//                setDefaults.put(Gc.FEETYPEEXISTS, Gc.NOTFOUND);
//                Gc.setSharedPreference(setDefaults, FeeTypeActivity.this);
//            }
            feeList.addAll(feeTypeEntities);
            adapter.notifyDataSetChanged();
        });
        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/7123776970", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
//        mDb.feeTypeModel().getFeeTypesExcludeTransport().removeObservers(this);
    }

    @Override
    public void onBackPressed() {

        if (Gc.getSharedPreference(Gc.FEETYPEEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            super.onBackPressed();
//            mDb.feeTypeModel().getFeeTypesExcludeTransport().removeObservers(this);
        } else {
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.ll_snackbar_create_fee_type), R.string.fees_not_available,
                    Snackbar.LENGTH_LONG);

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_blue));
            snackbarObj.show();
        }

    }

    private void addFeeType(String feeType, String feeTypeId, String frequency, String status) throws JSONException {
        progressBar.show();

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();
        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "fee/feeTypes";

        int method = Request.Method.POST;

        if (!("".equalsIgnoreCase(Strings.nullToEmpty(feeTypeId)))) {
            url = url + "/" + new JSONObject().put("FeeTypeID", feeTypeId);
            method = Request.Method.PUT;
        }

        p.put("FeeType", feeType);
        p.put("Frequency", frequency);
        p.put("Status", status);

        param.put(p);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressBar.cancel();
                        String code = job.optString("code");

                        switch (code) {
                            case "200":

                                try {
                                    JSONObject resultjobj = job.getJSONObject("result");
                                    JSONArray feeTypesjarr = resultjobj.getJSONArray("feeTypes");
                                    ArrayList<FeeType> feeTypes = new ArrayList<>();
                                    for (int i = 0; i < feeTypesjarr.length(); i++) {
                                        FeeType feeType = new Gson().fromJson(feeTypesjarr.getString(i), FeeType.class);
                                        final FeeTypeEntity feeTypeEntity = MapModelToEntity.mapFeeTypeModelToEntity(feeType);

                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mDb.feeTypeModel().insertFeeType(feeTypeEntity);
                                            }
                                        }).start();
                                    }

                                    Config.responseSnackBarHandler(job.optString("message"),
                                            findViewById(R.id.ll_snackbar_create_fee_type), R.color.fragment_first_green);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                            default:
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.ll_snackbar_create_fee_type), R.color.fragment_first_blue);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
                VolleyLog.e("Error: ", error.getMessage());
                Config.responseVolleyErrorHandler(FeeTypeActivity.this, error, findViewById(R.id.ll_snackbar_create_fee_type));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void updateFeeType(FeeTypeEntity updatedFeeType) {

//            addFeeType(updatedFeeType.FeeType,updatedFeeType.FeeTypeID, updatedFeeType.Frequency, updatedFeeType.Status);

        String fee = new Gson().toJson(updatedFeeType);
        Intent intent = new Intent(this, FeeTypeCreateActivity.class);
        intent.putExtra("fee", fee);
        startActivityForResult(intent, Gc.FEE_CREATE_REQUEST_CODE);

    }

    void showDialogForFeeTypeCreate() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_feetype_edit);
        dialog.setTitle(R.string.fee_type);

        final EditText et_fee_type_name = dialog.findViewById(R.id.et_fee_type_name);
        et_fee_type_name.setFilters(new InputFilter[]
                {Config.filter, new InputFilter.LengthFilter(getResources()
                        .getInteger(R.integer.normal_text))});

        final RadioButton rb_create_monthly = dialog.findViewById(R.id.rb_create_monthly);
        final RadioButton rb_create_fee_quarterly = dialog.findViewById(R.id.rb_create_fee_quarterly);
        final RadioButton rb_create_fee_session = dialog.findViewById(R.id.rb_create_fee_session);

        final RadioButton rb_feetype_active = dialog.findViewById(R.id.rb_feetype_active);
        final RadioButton rb_feetype_inactive = dialog.findViewById(R.id.rb_feetype_inactive);

        Button btnSave = dialog.findViewById(R.id.save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    try {
                String frequency = new String();
                String status = new String();

                if (rb_create_monthly.isChecked())
                    frequency = rb_create_monthly.getText().toString();
                if (rb_create_fee_quarterly.isChecked()) frequency = "Quarterly";
                if (rb_create_fee_session.isChecked())
                    frequency = rb_create_fee_session.getText().toString();

                if (rb_feetype_active.isChecked()) status = rb_feetype_active.getText().toString();
                if (rb_feetype_inactive.isChecked())
                    status = rb_feetype_inactive.getText().toString();


                try {
                    addFeeType(et_fee_type_name.getText().toString(), "", frequency, status);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }


            }
        });
        Button btnCancel = dialog.findViewById(R.id.cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help_save_next, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if (Gc.getSharedPreference(Gc.FEETYPEEXISTS, this).equalsIgnoreCase(Gc.EXISTS)) {
            menu.findItem(R.id.menu_next).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_next:
                onBackPressed();
                break;

            case R.id.action_help:

                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);


                alert1.setMessage(Html.fromHtml("<Br>" + " School Details !<Br>School Name<Br>"
                        + "<B>Email</B> + <B>Phone</B> + <B>Country</B> :" +
                        " <font color='#ce001f'>Are Mandatory</font><Br><B>Click help at top to see this message</B> " +
                        ":<font color='#ce001f'>Again</font>"));
                alert1.setTitle(getString(R.string.help));
                alert1.setIcon(R.drawable.ic_help);
                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//            alert1.setCancelable(false);
                alert1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Gc.FEE_CREATE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                String message = Objects.requireNonNull(data.getExtras()).getString("message");
                Config.responseSnackBarHandler(message,
                        findViewById(R.id.top_layout), R.color.fragment_first_green);
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

}

//    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//
//        }
//    }, new Response.ErrorListener() {
//        @Override
//        public void onErrorResponse(VolleyError error) {
//
//        }
//    }){
//        @Override
//        public Map<String, String> getHeaders() throws AuthFailureError {
//            HashMap<String, String> headers = new HashMap<>();
//
//            headers.put("APPKEY",Gc.APPKEY);
//            headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
//            headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
//            headers.put("Content-Type", "application/json");
//            headers.put("DEVICE", "WEB");
//            headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
//            headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
//            headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
//
//            return headers;
//        }
//        @Override
//        public String getBodyContentType() {
//            return "application/json; charset=utf-8";
//        }
//        @Override
//        public byte[] getBody() {
//            try {
//                return param == null ? null : param.toString().getBytes("utf-8");
//            } catch (UnsupportedEncodingException uee) {
//                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
//                return null;
//            }
//        }
//    };
//        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
//                AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
