package in.junctiontech.school.schoolnew;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.stetho.Stetho;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.menuActions.LanguageSetup;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.registration.GeneralInfoActivity;
import in.junctiontech.school.schoolnew.setup.SetupActivity;

public class FirstScreenActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private Button btn_demo, btn_register, btn_login;
    private RelativeLayout ll_for_snackbar;

    Boolean isDemo = false;

    private SharedPreferences sharedPrefLang;

    private TextView tv_first_screen_school_name;
    private ImageView civ_first_screen_school_logo;

    MainDatabase mDb;
String invitationType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initizlizeChannels();

        mDb = MainDatabase.getDatabase(getApplicationContext());
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);  // for device databse view
        }

        Log.e("Application id is", BuildConfig.APPLICATION_ID);

        progressDialog = new ProgressDialog(this);


        String signedinuser = Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this);
        SharedPreferences sharedPref = getSharedPreferences(Gc.SP_PERSISTENT, Context.MODE_PRIVATE);

//        if (Gc.TRUE.equalsIgnoreCase(sharedPref.getString(Gc.LOGOUT_AFTER_UPDATE, ""))){
//            submitToLogin(sharedPref.getString(Gc.ERPINSTCODE,""),
//                    sharedPref.getString(Gc.SIGNEDINUSERNAME, ""),
//                    sharedPref.getString(Gc.SIGNEDINUSERPASSWORD, ""));
//            return;
//        }

        if (signedinuser.equals(Gc.NOTFOUND)) {
            sharedPref.edit().putInt(Gc.APPVERSION, BuildConfig.VERSION_CODE).apply();

            Locale local_obj = Resources.getSystem().getConfiguration().locale;
            sharedPrefLang = getSharedPreferences("APP_LANG", Context.MODE_PRIVATE);
            if (sharedPrefLang.getString("app_language", "").equalsIgnoreCase(""))
                LanguageSetup.changeLang(this, local_obj.getLanguage());
            else LanguageSetup.changeLang(this, sharedPrefLang.getString("app_language", ""));

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
//                Locale locale = getResources().getConfiguration().getLocales().get(0);
//                Log.e("Locale", locale.getLanguage());
//            } else{
//                //noinspection deprecation
//                Locale locale = getResources().getConfiguration().locale;
//                Log.e("Locale", locale.getLanguage());
//            }


            setContentView(R.layout.first_screen);
            initViews();
            /*   For Background display */
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                ll_for_snackbar.setBackgroundResource(R.drawable.ic_background_landscape);
            else ll_for_snackbar.setBackgroundResource(R.drawable.ic_background_verticle);

            loginForFlavor();

        } else {
            startActivity(new Intent(this, SetupActivity.class));
            finish();
            return;
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.DATA_LOADING_COMPLETE)) {
                    progressDialog.dismiss();
                    startActivity(new Intent(FirstScreenActivity.this, SetupActivity.class));
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);


                } else if (intent.getAction().equalsIgnoreCase("ReferrerOrganizationCode")) {
                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ERPINSTCODE, intent.getStringExtra("OrganizationCode"));
                    Gc.setSharedPreference(setDefaults, FirstScreenActivity.this);

                    goToLoginForReferrer(intent.getStringExtra("OrganizationCode"));
                }

            }
        };
        getFirebaseReferral();
    }

    private void getFirebaseReferral() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, pendingDynamicLinkData -> {
                    Uri deepLink = null;
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.getLink();
                        if (deepLink != null) {
                            String referralLink = deepLink.toString();
                            try {
                                referralLink = referralLink.substring(referralLink.lastIndexOf("=") + 1);
                                String json = URLDecoder.decode(referralLink, "UTF-8");
                                JSONObject obj = new JSONObject(json);
                                if (!obj.getString("orgKey").isEmpty()) {

                                    if(!obj.getString("invitationType").isEmpty()){
                                        invitationType=obj.getString("invitationType");
                                    }

                                    goToLoginForReferrer(obj.getString("orgKey"));
                                }
                            } catch (Exception e) {
                                Log.e("referralLink", " error " + e.toString());
                            }
                        }
                    }
                })
                .addOnFailureListener(this, e -> Log.w("getDynamicLink", "getDynamicLink:onFailure", e));
    }

    private void loginForFlavor() {

        String orgKey = "";

        switch (BuildConfig.APPLICATION_ID) {
            case "com.zeroerp.uppublic":
                orgKey = "UPPUBLICSCHOOL";
                break;

            case "com.zeroerp.uppre":
                orgKey = "UPPRESCHOOL";
                break;
            case "com.zeroerp.apsbpl":
                orgKey = "17509";
                break;
            case "com.zeroerp.itutor":
                orgKey = "";
                Gc.CPANELURL = "https://apicpanel.myschool-et.com/" ;
                break;

        }
        if (!(BuildConfig.APPLICATION_ID.equalsIgnoreCase("in.junctiontech.school"))) {
            goToLoginForReferrer(orgKey);
            finish();
        }
    }

    void goToLoginForReferrer(String instCode) {
        Log.i("Institute Code", instCode);

        Intent intent = new Intent(FirstScreenActivity.this, OrganizationKeyEnter.class);
        intent.putExtra("Referrer", Gc.TRUE);
        intent.putExtra("organizationKey", instCode.toUpperCase().trim());
        intent.putExtra("invitationType",invitationType);
        startActivity(intent);
    }

    private void initViews() {
        btn_demo = findViewById(R.id.btn_demo);
        btn_demo.setOnClickListener(v -> {
            startActivityForResult(new Intent(FirstScreenActivity.this, DemoSchoolActivity.class), Config.DEMO_SCHOOL_REQUEST_CODE);
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        });
        btn_register = findViewById(R.id.btn_register);
        btn_register.setVisibility(View.GONE);
        btn_register.setOnClickListener(v -> {
            startActivity(new Intent(FirstScreenActivity.this, GeneralInfoActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        });
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(view -> startActivity(new Intent(FirstScreenActivity.this, OrganizationKeyEnter.class)));
        ll_for_snackbar = findViewById(R.id.scrollView_first_fragment);
        //  videoView = (VideoView) findViewById(R.id.videoView);

        final RadioButton rb_first_screen_english = findViewById(R.id.rb_first_screen_english);
        final RadioButton rb_first_screen_hindi = findViewById(R.id.rb_first_screen_hindi);
        final RadioButton rb_first_screen_urdu = findViewById(R.id.rb_first_screen_urdu);
        final RadioButton rb_first_screen_marathi = findViewById(R.id.rb_first_screen_marathi);
        final RadioButton rb_first_screen_spanish = findViewById(R.id.rb_first_screen_spanish);
        final RadioButton rb_first_screen_arabic = findViewById(R.id.rb_first_screen_arabic);
        final RadioButton rb_first_screen_french = findViewById(R.id.rb_first_screen_french);

        tv_first_screen_school_name = findViewById(R.id.tv_first_screen_school_name);
        civ_first_screen_school_logo = findViewById(R.id.civ_first_screen_school_logo);

        switch (sharedPrefLang.getString("app_language", "")) {
            case "en":
                rb_first_screen_english.setChecked(true);
                //setBackground(rb_first_screen_english);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_english);
                break;
            case "hi":
                rb_first_screen_hindi.setChecked(true);
                // setBackground(rb_first_screen_hindi);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_hindi);
                break;
            case "ur":
                rb_first_screen_urdu.setChecked(true);
                //   setBackground(rb_first_screen_urdu);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_urdu);
                break;
            case "mr":
                rb_first_screen_marathi.setChecked(true);
                //setBackground(rb_first_screen_marathi);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_marathi);
                break;
            case "es":
                rb_first_screen_spanish.setChecked(true);
                //setBackground(rb_first_screen_spanish);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_spanish);
                break;
            case "ar":
                rb_first_screen_arabic.setChecked(true);
                //setBackground(rb_first_screen_spanish);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_arabic);
                break;

            case "fr":
                rb_first_screen_french.setChecked(true);
                //setBackground(rb_first_screen_spanish);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_french);
                break;
            default:
                rb_first_screen_english.setChecked(true);
                //setBackground(rb_first_screen_english);
                setLanguage(sharedPrefLang.getString("app_language", ""), rb_first_screen_english);
                break;

        }
        rb_first_screen_english.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("en", rb_first_screen_english);
        });
        rb_first_screen_hindi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("hi", rb_first_screen_hindi);
        });
        rb_first_screen_urdu.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("ur", rb_first_screen_urdu);
        });
        rb_first_screen_marathi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("mr", rb_first_screen_marathi);
        });
        rb_first_screen_spanish.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("es", rb_first_screen_spanish);
        });

        rb_first_screen_arabic.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("ar", rb_first_screen_arabic);
        });
        rb_first_screen_french.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) setLanguage("fr", rb_first_screen_french);
        });
    }

    private void setLanguage(String lan, RadioButton rb_first_screen_checked) {


        if (LanguageSetup.changeLang(this, lan)) {

            Locale locale = new Locale(lan);
            Locale.setDefault(locale);
            Configuration config = getResources().getConfiguration();
            config.setLocale(locale);
            createConfigurationContext(config);
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());

            restartActivity();
//            btn_demo.setText(getString(R.string.demo));
//            btn_register.setText(getString(R.string.register));
//            btn_login.setText(getString(R.string.login));
//            tv_first_screen_school_name.setText(getString(R.string.zeroerp_school));
        }
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (ll_for_snackbar != null)
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
                ll_for_snackbar.setBackgroundResource(R.drawable.ic_background_landscape);
            else ll_for_snackbar.setBackgroundResource(R.drawable.ic_background_verticle);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == Config.DEMO_SCHOOL_REQUEST_CODE) {
            HashMap<String, String> setDefaults = new HashMap<>();
            setDefaults.put(Gc.ERPINSTCODE, data.getStringExtra("organization").toUpperCase());
            Gc.setSharedPreference(setDefaults, this);
//            sp.edit().putString(Config.SMS_ORGANIZATION_NAME,
//                    data.getStringExtra("organization").toUpperCase()).commit();

            submitToLogin(data.getStringExtra("organization"),
                    data.getStringExtra("username"),
                    data.getStringExtra("pwd"));
            if (data.getStringExtra(Gc.FROMDEMO).equalsIgnoreCase(Gc.FROMDEMO))
                isDemo = true;

        }
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("ReferrerOrganizationCode"));
        super.onResume();
    }

    @Override
    protected void onPause() {
        // videoView.start();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void submitToLogin(String instCode, String userID, String pwd) {

        getHostNameFromServer(instCode, userID, pwd);

    }

    public void getHostNameFromServer(final String instcode, final String userID, final String pwd) {

        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();

        final String hostNameUrl = Gc.CPANELURL
                + Gc.CPANELAPIVERSION
                + "hostUrl/host/"
                + instcode.trim().toUpperCase();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, hostNameUrl, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        String code = jsonObject.optString("code");
                        switch (code) {
                            case Gc.APIRESPONSE200:

                                try {
                                    JSONObject job = jsonObject.getJSONObject("result")
                                            .getJSONObject("organizationDetails");

                                    HashMap<String, String> setDefaults = new HashMap<>();

                                    setDefaults.put(Gc.ERPHOSTAPIURL, job.optString("apiurl"));
                                    setDefaults.put(Gc.ERPWEBHOSTURL, job.optString("hosturl"));
                                    setDefaults.put(Gc.ERPLICENCEEXPIRY, job.optString("licenceExpiry"));
                                    setDefaults.put(Gc.ERPINSTCODE, job.optString("organizationKey"));
                                    setDefaults.put(Gc.ERPDBNAME, job.optString("db_name"));
                                    setDefaults.put(Gc.ERPPLANTYPE, job.optString("planType"));
                                    setDefaults.put(Gc.ERPINSTTYPE, job.optString("institutionType"));
                                    setDefaults.put(Gc.ERPADDVERTISEMENTSTATUS, job.optString("addStatus"));
                                    setDefaults.put(Gc.ERPAPPLICATIONSTATUS, job.optString("applicationStatus"));
                                    setDefaults.put(Gc.USERID, userID);
                                    setDefaults.put(Gc.PASSWORD, pwd);
                                    Gc.setSharedPreference(setDefaults, FirstScreenActivity.this);


                                    getSchoolPublicDetails();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;

                            case Gc.APIRESPONSE401:

                                Gc.logCrash(FirstScreenActivity.this, hostNameUrl);

                                break;
                            default:
                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(FirstScreenActivity.this);

                                alertAdmin.setMessage(jsonObject.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(FirstScreenActivity.this, FirstScreenActivity.class));
                                        finish();
                                    }
                                });
                                if (!isFinishing())
                                    alertAdmin.show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressDialog.dismiss();
                Config.responseVolleyErrorToast(FirstScreenActivity.this, error);
//                Config.responseVolleyErrorHandler(FirstScreenActivity.this, error, findViewById(R.id.scrollView_first_fragment));

            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }

    private void getSchoolPublicDetails() {
//        final Map<String, Object> param = new LinkedHashMap<>();
//        param.put("APPKEY", "pOcY8y-LTEexqkROIm64WAXn1mwB1lyECcvZAZnl6hRQMZFtQeHwcUa74L_jfCRDOgrknWBWucbkOKs5g7p82OkwHhdemJMKUkGPMVqI4agzEwYt0Jt8_5c0CgQXosf82q4A5n8nPSrt30CFf30mePcDYQiBoE_x8ht0-MIAVAtrxxxg6cYEYs6nlEKznKJ_jZWokAOxuInyAqH0aXF8EHZGjV6A");
//        param.put("ORGKEY", sp.getString(Config.DECODED_ORGANIZATION_KEY, ""));
//        param.put("DBNAME", sp.getString("organization_name", ""));
//        param.put("Content-Type", "application/json");
//        param.put("DEVICE", "WEB");

        if (!FirstScreenActivity.this.isFinishing())
            progressDialog.show();

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "public/schoolPublicDetails";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressDialog.dismiss();
                        try {
                            JSONObject resultjobj = job.getJSONObject("result");

//                            sp.edit().putString(Config.applicationVersionName, Config.applicationVersionNameValue).commit();

                            JSONObject schoolDetailsjobj = resultjobj.getJSONObject("schoolDetails");
                            final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(schoolDetailsjobj.toString(), SchoolDetailsEntity.class);
                            if (!(schoolDetailsEntity.SchoolName == null)) {
                                HashMap<String, String> setDefaults = new HashMap<String, String>();
                                setDefaults.put(Gc.LOGO_URL, schoolDetailsEntity.Logo);
                                setDefaults.put(Gc.SCHOOLIMAGE, schoolDetailsEntity.schoolImage);
                                setDefaults.put(Gc.SCHOOLNAME, schoolDetailsEntity.SchoolName);
                                Gc.setSharedPreference(setDefaults, FirstScreenActivity.this);

                                SharedPreferences spColor = getApplicationContext().getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);

                                if (spColor.getInt(Config.APP_COLOR, 0) != Integer.parseInt(schoolDetailsEntity.themeColor)) {
                                    spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();

                                } else
                                    spColor.edit().putInt(Config.APP_COLOR, Integer.parseInt(schoolDetailsEntity.themeColor)).apply();


                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);
                                    }
                                }).start();

                            }

                            HashMap<String, String> setDefaults = new HashMap<>();

                            setDefaults.put(Gc.GALLERYDATA, resultjobj.optString("galleries"));
                            Gc.setSharedPreference(setDefaults, FirstScreenActivity.this);


                            JSONArray noticeJarr = resultjobj.getJSONArray("noticeBoards");
                            final ArrayList<NoticeBoardEntity> noticeBoardEntities = new Gson().fromJson(noticeJarr.toString(), new TypeToken<List<NoticeBoardEntity>>() {
                            }.getType());
                            if (noticeBoardEntities.size() > 0) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.noticeBoardModel().insertNotice(noticeBoardEntities);
                                    }
                                }).start();
                            }

                            JSONArray holidayJarr = resultjobj.getJSONArray("holidays");


                            Intent intent = new Intent(FirstScreenActivity.this, SignUpActivityNew.class);
                            intent.putExtra(Gc.FROMDEMO, true);
                            intent.putExtra(Gc.ERPINSTCODE, Gc.getSharedPreference(Gc.ERPINSTCODE, FirstScreenActivity.this));
                            intent.putExtra(Gc.USERID, Gc.getSharedPreference(Gc.USERID, FirstScreenActivity.this));
                            intent.putExtra(Gc.PASSWORD, Gc.getSharedPreference(Gc.PASSWORD, FirstScreenActivity.this));

                            startActivity(intent);
//                            finish();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        VolleyLog.e("Error: ", error.getMessage());
                        Config.responseVolleyErrorToast(FirstScreenActivity.this, error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(req);
    }

    public void initizlizeChannels() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


            notificationManager.createNotificationChannelGroup(new NotificationChannelGroup(Gc.CHANNEL_GROUPID_IMPORTANT, getString(R.string.important_messages)));
            notificationManager.createNotificationChannelGroup(new NotificationChannelGroup(Gc.CHANNEL_GROUPID_INFORMATION, getString(R.string.school_information)));
            notificationManager.createNotificationChannelGroup(new NotificationChannelGroup(Gc.CHANNEL_GROUPID_MESSAGING, getString(R.string.chat_and_messaging)));

            NotificationChannel attchannel = new NotificationChannel(Gc.CHANNELID_ATTENDANCE, Gc.CHANNELID_ATTENDANCE_TEXT, NotificationManager.IMPORTANCE_HIGH);
            attchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(attchannel);

            NotificationChannel hwchannel = new NotificationChannel(Gc.CHANNELID_HOMEWORK, Gc.CHANNELID_HOMEWORK_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            hwchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(hwchannel);

            NotificationChannel hwevalchannel = new NotificationChannel(Gc.CHANNELID_HOMEWORK_EVALUATION, Gc.CHANNELID_HOMEWORK_EVALUATION_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            hwevalchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(hwevalchannel);

            NotificationChannel librarychannel = new NotificationChannel(Gc.CHANNELID_LIBRARY, Gc.CHANNELID_LIBRARY_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            librarychannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(librarychannel);

            NotificationChannel examchannel = new NotificationChannel(Gc.CHANNELID_EXAM, Gc.CHANNELID_EXAM_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            examchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(examchannel);

            NotificationChannel finechannel = new NotificationChannel(Gc.CHANNELID_FINE, Gc.CHANNELID_FINE_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            finechannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(finechannel);

            NotificationChannel eventchannel = new NotificationChannel(Gc.CHANNELID_EVENTS, Gc.CHANNELID_EVENTS_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            eventchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(eventchannel);

            NotificationChannel onlineexamchannel = new NotificationChannel(Gc.CHANNELID_ONLINEEXAM, Gc.CHANNELID_ONLINEEXAM_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            onlineexamchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(onlineexamchannel);

            NotificationChannel feereminderchannel = new NotificationChannel(Gc.CHANNELID_FEE_REMINDER, Gc.CHANNELID_FEE_REMINDER_TEXT, NotificationManager.IMPORTANCE_HIGH);
            feereminderchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(feereminderchannel);


            NotificationChannel feereceiptchannel = new NotificationChannel(Gc.CHANNELID_FEE_RECEIPT, Gc.CHANNELID_FEE_RECEIPT_TEXT, NotificationManager.IMPORTANCE_HIGH);
            feereceiptchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(feereceiptchannel);

            NotificationChannel noticeboardchannel = new NotificationChannel(Gc.CHANNELID_NOTICEBOARD, Gc.CHANNELID_NOTICEBOARD_TEXT, NotificationManager.IMPORTANCE_HIGH);
            noticeboardchannel.setGroup(Gc.CHANNEL_GROUPID_IMPORTANT);
            notificationManager.createNotificationChannel(noticeboardchannel);


            // Messaging Group

            NotificationChannel groupmessagechannel = new NotificationChannel(Gc.CHANNELID_GROUP_MESSAGE, Gc.CHANNELID_GROUP_MESSAGE_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            groupmessagechannel.setGroup(Gc.CHANNEL_GROUPID_MESSAGING);
            notificationManager.createNotificationChannel(groupmessagechannel);

            NotificationChannel personalmessagechannel = new NotificationChannel(Gc.CHANNELID_121_MESSAGE, Gc.CHANNELID_121_MESSAGE_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            personalmessagechannel.setGroup(Gc.CHANNEL_GROUPID_MESSAGING);
            notificationManager.createNotificationChannel(personalmessagechannel);

            NotificationChannel schoolmessagechannel = new NotificationChannel(Gc.CHANNELID_SCHOOL_MESSAGE, Gc.CHANNELID_SCHOOL_MESSAGE_TEXT, NotificationManager.IMPORTANCE_HIGH);
            schoolmessagechannel.setGroup(Gc.CHANNEL_GROUPID_MESSAGING);
            notificationManager.createNotificationChannel(schoolmessagechannel);

            // Information Group


            NotificationChannel otherinfochannel = new NotificationChannel(Gc.CHANNELID_OTHER_INFO, Gc.CHANNELID_OTHER_INFO_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            otherinfochannel.setGroup(Gc.CHANNEL_GROUPID_INFORMATION);
            notificationManager.createNotificationChannel(otherinfochannel);


            NotificationChannel greetingschannel = new NotificationChannel(Gc.CHANNELID_GREETINGS, Gc.CHANNELID_GREETINGS_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            greetingschannel.setGroup(Gc.CHANNEL_GROUPID_INFORMATION);
            notificationManager.createNotificationChannel(greetingschannel);


            NotificationChannel galleryaddchannel = new NotificationChannel(Gc.CHANNELID_GALLERY_ADD, Gc.CHANNELID_GALLERY_ADD_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            galleryaddchannel.setGroup(Gc.CHANNEL_GROUPID_INFORMATION);
            notificationManager.createNotificationChannel(galleryaddchannel);

            NotificationChannel classGalleryAddChannel = new NotificationChannel(Gc.CHANNELID_CLASS_GALLERY_ADD, Gc.CHANNELID_CLASS_GALLERY_ADD_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            galleryaddchannel.setGroup(Gc.CHANNEL_GROUPID_INFORMATION);
            notificationManager.createNotificationChannel(classGalleryAddChannel);

            NotificationChannel surveyChannel = new NotificationChannel(Gc.CHANNELID_SURVEY, Gc.CHANNELID_SURVEY_TEXT, NotificationManager.IMPORTANCE_DEFAULT);
            surveyChannel.setGroup(Gc.CHANNEL_GROUPID_INFORMATION);
            notificationManager.createNotificationChannel(surveyChannel);
        }
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onDestroy();

    }

}
