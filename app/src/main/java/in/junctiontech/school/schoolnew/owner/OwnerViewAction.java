package in.junctiontech.school.schoolnew.owner;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 05-07-2017.
 */

public class OwnerViewAction   implements Serializable {
String label;
    int iconId;

    public OwnerViewAction(String label, int iconId) {
        this.label = label;
        this.iconId = iconId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
