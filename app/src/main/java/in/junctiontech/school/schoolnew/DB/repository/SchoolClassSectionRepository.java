package in.junctiontech.school.schoolnew.DB.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionDao;
import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;

public class SchoolClassSectionRepository {

    private SchoolClassSectionDao mSectionDao;
//    private LiveData<List<SchoolClassSectionEntity>> mAllSection;

    public SchoolClassSectionRepository(Application app){
        MainDatabase mDb = MainDatabase.getDatabase(app);
        mSectionDao = mDb.schoolClassSectionModel();
//        mAllSection = mSectionDao.getAllSection();
    }


    public LiveData<List<SchoolClassSectionEntity>> getClassSection(String classid){
        return mSectionDao.getClassSectionLive(classid);
    }

    public void insert(SchoolClassSectionEntity[] schoolClassSectionEntities){
        new insertAsyncTask(mSectionDao).execute(schoolClassSectionEntities);
    }

    private static class insertAsyncTask extends AsyncTask<SchoolClassSectionEntity, Void, Void> {
        private SchoolClassSectionDao mSchoolSectionDao;
        insertAsyncTask(SchoolClassSectionDao dao){
            mSchoolSectionDao = dao;
        }
        @Override
        protected Void doInBackground(SchoolClassSectionEntity... schoolClassSectionEntities) {
            ArrayList<SchoolClassSectionEntity> sectionEntities = new ArrayList<>();
            for (int i=0; i < schoolClassSectionEntities.length; i++){
                sectionEntities.add(schoolClassSectionEntities[i]);
            }
            mSchoolSectionDao.insertSchoolClassSection(sectionEntities);
            return null;
        }
    }
}
