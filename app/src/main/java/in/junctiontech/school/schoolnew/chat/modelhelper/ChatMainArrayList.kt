package `in`.junctiontech.school.schoolnew.chat.modelhelper

class ChatMainArrayList {
    var autoid = 0
    var withType // class , house , event , other groups , usertype
            : String? = null
    var withId // class , house , event , other group id or staff , student, parent id
            : String? = null
    var withName // name of the entity with whom the conversation is
            : String? = null
    var msgType // text , image , video , voice
            : String? = null
    var msg // text
            : String? = null
    var imagepath // gallery or url
            : String? = null
    var imageOriginalPath // gallery or url
            : String? = null
    var senderType // when message received it was from
            : String? = null
    var senderId // received mesage was from id
            : String? = null
    var msgId // received message id
            : String? = null
    var byMe // all messages originated from here
            = 0
    var sent // message sent status
            = 0
    var received // message received by other user
            = 0
    var read // message read by other user
            = 0
    var readByMe // message read by me
            = 0
    var enteredDate // message created on
            : String? = null
    var sentDate // message sent on
            : String? = null
    var receivedDate // message read on
            : String? = null
    var readDate // message read on
            : String? = null
    var withProfileUrl // image url of user or group
            : String? = null

    var localPath //  Downloaded image, Video Path for Local Storage
            : String? = null
    var fileSize //   image, Video, file Size
            : String? = null

    var dateHeader:String?=""

    var isSelected:Boolean = false
}