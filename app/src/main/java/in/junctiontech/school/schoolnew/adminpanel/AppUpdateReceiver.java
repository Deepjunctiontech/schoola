package in.junctiontech.school.schoolnew.adminpanel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import in.junctiontech.school.Prefs;
import in.junctiontech.school.schoolnew.common.Gc;

// it does not work consistently across devices hence , this is commented in manifest
// Once we get some clarity on update logic , we will use it

public class AppUpdateReceiver extends BroadcastReceiver {

    // android.intent.action.MY_PACKAGE_REPLACED


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String ac = intent.getAction();
        Log.e("Update Found", intent.getAction());
        if (intent.getAction().equalsIgnoreCase("android.intent.action.MY_PACKAGE_REPLACED")) {
            if (bundle != null) {
                SharedPreferences sp = Prefs.with(context).getSharedPreferences();

                sp.edit().putString(Gc.APPUPDATED, Gc.TRUE).apply();
            }
        }

        if (intent.getAction().equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {
            try {
                PackageManager pm = context.getPackageManager();
                PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);

                String versionName = info.versionName;
                int versionCode = info.versionCode;

                Toast.makeText(context, "[My Package replaced] name: " + versionName + " code: " + versionCode, Toast.LENGTH_LONG).show();

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
