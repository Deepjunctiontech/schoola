package in.junctiontech.school.schoolnew.permissions.communication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class MessengerPermissionsActivity extends AppCompatActivity {

    MainDatabase mDb;
    private View snackbar;
    ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private ArrayList<MessengerPermission> chatPermissionList = new ArrayList<>();
    HashMap<String, ArrayList<String>> userPermissions = new HashMap<>();

    private RecyclerView recycler_view;
    MessagePermissionAdapter adapter;
    private ArrayList<MasterEntryEntity> masterEntryPosList = new ArrayList<>();
    private String[] masterEntryValues;

    Boolean save_before_exit = false;

    private void setColorApp() {
        int colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_all_list);
        mDb = MainDatabase.getDatabase(this);
        setColorApp();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        snackbar = findViewById(R.id.fl_fragment_all_list);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));

        progressBar = findViewById(R.id.progressBar);

        recycler_view = findViewById(R.id.recycler_view_list_of_data);
//        ((SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list)).setRefreshing(false);
        setupRecycler();

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.ADMIN)) {
            mDb.masterEntryModel().getMasterEntryValuesGeneric("UserType").observe(this, masterEntryEntities -> {
                if (masterEntryEntities != null) {
                    masterEntryPosList.clear();
                    masterEntryValues = null;
                    masterEntryPosList.addAll(masterEntryEntities);

                    masterEntryValues = new String[masterEntryPosList.size()];
                    for (int i = 0; i < masterEntryPosList.size(); i++) {
                        masterEntryValues[i] = masterEntryPosList.get(i).MasterEntryValue;
                    }
                    adapter.notifyDataSetChanged();
                }
            });
            getMessengerPermission();
        } else {
            finish();
        }

    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);
        adapter = new MessagePermissionAdapter();
        recycler_view.setAdapter(adapter);
    }


    private void getMessengerPermission() {
        progressBar.setVisibility(View.VISIBLE);
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "permission/communicationPermissions";

        JsonObjectRequest jsonObjectReq = new JsonObjectRequest(Request.Method.GET, url, null, jsonObject -> {

            String code = jsonObject.optString("code");

            progressBar.setVisibility(View.GONE);
            switch (code) {
                case Gc.APIRESPONSE200:

                    try {
                        chatPermissionList = new Gson().fromJson(jsonObject.getJSONObject("result").optString("communicationPermissions"), new TypeToken<List<MessengerPermission>>() {
                        }.getType());

                        collectUserTypeWisePermission();

                        Config.responseSnackBarHandler(jsonObject.optString("message"),
                                findViewById(R.id.fl_fragment_all_list), R.color.fragment_first_green);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, MessengerPermissionsActivity.this);

                    Intent intent1 = new Intent(MessengerPermissionsActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, MessengerPermissionsActivity.this);

                    Intent intent2 = new Intent(MessengerPermissionsActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.fl_fragment_all_list), R.color.fragment_first_green);

                    break;
            }

        }, volleyError -> {
            progressBar.setVisibility(View.GONE);
            Log.e("MessengerPermissionData", volleyError.toString());
            Config.responseVolleyErrorHandler(MessengerPermissionsActivity.this, volleyError, snackbar);

        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };

        jsonObjectReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectReq, "MessengerPermission");
    }


    private void setPermissionToServer() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);

        final JSONArray param = new JSONArray();
        for (MasterEntryEntity mainObj : masterEntryPosList) {
            ArrayList<String> assignedPermissions = userPermissions.get(mainObj.MasterEntryValue);
            for (String s : assignedPermissions) {
                param.put(new JSONObject()
                        .put("FromUserType", mainObj.MasterEntryValue)
                        .put("ToUserType", s));
            }
        }


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "permission/communicationPermissions";


        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), jsonObject -> {
            Log.e("Communi permission", jsonObject.toString());
            progressBar.setVisibility(View.GONE);
            String code = jsonObject.optString("code");

            switch (code) {

                case Gc.APIRESPONSE200:
                    save_before_exit = false;
                    Toast.makeText(this, getString(R.string.save_successfully),
                            Toast.LENGTH_SHORT).show();
//                        Config.responseSnackBarHandler(jsonObject.optString("message"),
//                                findViewById(R.id.fl_fragment_all_list), R.color.fragment_first_green);

                    finish();
                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, MessengerPermissionsActivity.this);

                    Intent intent1 = new Intent(MessengerPermissionsActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, MessengerPermissionsActivity.this);

                    Intent intent2 = new Intent(MessengerPermissionsActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(jsonObject.optString("message"),
                            findViewById(R.id.fl_fragment_all_list), R.color.fragment_first_blue);
                    break;
            }
        }, volleyError -> {
            Log.e("GalleryRES", volleyError.toString());
            progressBar.setVisibility(View.GONE);
            Config.responseVolleyErrorHandler(MessengerPermissionsActivity.this, volleyError, findViewById(R.id.fl_fragment_all_list));
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonReq);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (save_before_exit) {
            AlertDialog.Builder alertUpdate = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alertUpdate.setTitle(getString(R.string.verification));
            alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_alert));


            alertUpdate.setPositiveButton(R.string.save, (dialog, which) -> {
                showPasswordVerification();

            });
            alertUpdate.setNegativeButton(R.string.exit, (dialog, which) -> {
                super.onBackPressed();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

            });
            alertUpdate.setCancelable(false);
            alertUpdate.show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_fee_receipt, menu);

        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                showPasswordVerification();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPasswordVerification() {
        AlertDialog.Builder alertUpdate = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setTitle("Are You Sure To Change Messenger Permissions ?");
        alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_alert));
//        View view_update = getLayoutInflater().inflate(R.layout.layout_password_verification, null);
//
//        final EditText et_name = view_update.findViewById(R.id.et_password_verification_userid);
//        final EditText et_pwd = view_update.findViewById(R.id.et_password_verification_password);

        alertUpdate.setPositiveButton(R.string.yes, (dialog, which) -> {
/*
            if (et_name.getText().toString().trim().length() == 0 || et_pwd.getText().toString().trim().length() == 0) {
                Toast.makeText(MessengerPermissionsActivity.this,
                        getString(R.string.login_id_can_not_be_blank), Toast.LENGTH_SHORT).show();
            } else {
                try {*/
            // checkUserValidation(et_name.getText().toString(), et_pwd.getText().toString());
            try {
                setPermissionToServer();
            } catch (JSONException e) {
                e.printStackTrace();
            }
               /* } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/

        });
        alertUpdate.setNegativeButton(R.string.no, null);
        //   alertUpdate.setView(view_update);
        alertUpdate.setCancelable(false);
        alertUpdate.show();
    }

    private void checkUserValidation(String userId, String pwd) throws JSONException {
        progressDialog.show();

        final JSONObject param = new JSONObject();
        param.put("username", userId);
        param.put("password", pwd);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "login/userAuthenticate";


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param,
                job -> {
                    progressDialog.cancel();


                    if (job.optString("code").equalsIgnoreCase("200")) {
                        try {
                            setPermissionToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else Config.responseSnackBarHandler(job.optString("message"), snackbar);

                }, volleyError -> {
            progressDialog.cancel();

            Config.responseVolleyErrorHandler(MessengerPermissionsActivity.this, volleyError, findViewById(R.id.fl_fragment_all_list));
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void collectUserTypeWisePermission() {

        userPermissions.clear();

        for (MasterEntryEntity mainObj : masterEntryPosList) {
            ArrayList<String> toPermissions = new ArrayList<>();
            for (MessengerPermission subObj : chatPermissionList) {

                if (mainObj.MasterEntryValue.equalsIgnoreCase(subObj.FromUserType)) {
                    toPermissions.add(subObj.ToUserType);
                }
            }
            userPermissions.put(mainObj.MasterEntryValue, toPermissions);
        }
    }

    public class MessagePermissionAdapter extends RecyclerView.Adapter<MessagePermissionAdapter.MyViewHolder> {

        @NonNull
        @Override
        public MessagePermissionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_messenger_permissions, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MessagePermissionAdapter.MyViewHolder holder, int position) {
            holder.tv_user_type.setText(masterEntryPosList.get(position).MasterEntryValue);
        }

        @Override
        public int getItemCount() {
            return masterEntryPosList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_user_type;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_user_type = itemView.findViewById(R.id.tv_user_type);

                itemView.setOnClickListener(view ->
                        assignPermissionToUserType(masterEntryPosList.get(getAdapterPosition())));
            }
        }
    }

    void assignPermissionToUserType(final MasterEntryEntity masterEntryEntity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.messenger_permission);

        final ArrayList<String> assignedPermissions = userPermissions.get(masterEntryEntity.MasterEntryValue);

        if (assignedPermissions != null) {
            boolean[] checkedItems = new boolean[masterEntryPosList.size()];

            for (int i = 0; i < masterEntryPosList.size(); i++) {
                checkedItems[i] = assignedPermissions.contains(masterEntryPosList.get(i).MasterEntryValue);
            }


            builder.setMultiChoiceItems(masterEntryValues, checkedItems, (dialogInterface, i, b) -> {
                save_before_exit = true;
                if (b) {
                    assignedPermissions.add(masterEntryPosList.get(i).MasterEntryValue);
                } else if (assignedPermissions.contains(masterEntryPosList.get(i).MasterEntryValue)) {
                    assignedPermissions.remove(masterEntryPosList.get(i).MasterEntryValue);
                }
            }).setPositiveButton(R.string.change, (dialogInterface, i) ->
                    userPermissions.put(masterEntryEntity.MasterEntryValue, assignedPermissions))
                    .setNegativeButton(R.string.cancel, (dialog, id) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Config.responseSnackBarHandler(getString(R.string.some_error_occurred_try_again_later),
                    findViewById(R.id.fl_fragment_all_list), R.color.fragment_first_blue);
        }
    }


}
