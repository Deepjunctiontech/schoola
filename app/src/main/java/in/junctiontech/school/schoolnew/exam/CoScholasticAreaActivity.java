package in.junctiontech.school.schoolnew.exam;

import androidx.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class CoScholasticAreaActivity extends AppCompatActivity {

    MainDatabase mDb;
    ProgressBar progressBar;

    FloatingActionButton fb_coscholastic_area;
    RecyclerView mRecyclerView;

    final String COSCHOLAREA = "Scarea";

    ArrayList<MasterEntryEntity> coScholasticAreaList = new ArrayList<>();

    CoScholasticAreaAdapter adapter;

    private int colorIs;
    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        fb_coscholastic_area.setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_co_scholastic_area);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        fb_coscholastic_area = findViewById(R.id.fb_coscholastic_area);
        mRecyclerView = findViewById(R.id.rv_coscholastic_area);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new CoScholasticAreaAdapter();
        mRecyclerView.setAdapter(adapter);

        fb_coscholastic_area.setOnClickListener(view -> {
                showDialogForCoScholasticAreaCreate();
        });

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/8147222219", this, adContainer);
        }
    }


    void showDialogForCoScholasticAreaCreate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.create_coscholastic_area);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_coscholastic_area = view.findViewById(R.id.et_subject_name);
        et_coscholastic_area.setHint(R.string.create_coscholastic_area);
        et_coscholastic_area.setFilters(new InputFilter[] { new InputFilter.LengthFilter(50) });

        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_coscholastic_area.getText().toString().trim().isEmpty()){
                et_coscholastic_area.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {
                createCoScholasticArea(et_coscholastic_area.getText().toString().trim().toUpperCase());
                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }

    void showDialogForCoScholasticAreaUpdate(MasterEntryEntity masterEntryEntity){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_edit);
        builder.setTitle(R.string.create_coscholastic_area);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.subject_create, null);

        final EditText et_coscholastic_area = view.findViewById(R.id.et_subject_name);
        et_coscholastic_area.setHint(R.string.create_coscholastic_area);
        et_coscholastic_area.setFilters(new InputFilter[] { new InputFilter.LengthFilter(50) });
        et_coscholastic_area.setText(masterEntryEntity.MasterEntryValue);

        final EditText et_subject_abbr = view.findViewById(R.id.et_subject_abbr);
        et_subject_abbr.setVisibility(View.GONE);

        builder.setView(view);

        builder.setPositiveButton(R.string.add, (dialogInterface, i) -> Log.i("Positive button called",""));

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

        });
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            boolean wantToCloseDialog = false;

            if(et_coscholastic_area.getText().toString().trim().isEmpty()){
                et_coscholastic_area.setError(getString(R.string.error_field_required),getResources().getDrawable(R.drawable.ic_alert));}
            else {
                updateCoScholasticArea(et_coscholastic_area.getText().toString().trim().toUpperCase(), masterEntryEntity);
                wantToCloseDialog = true;
            }
            if(wantToCloseDialog)
                dialog.dismiss();
        });
    }


    void updateCoScholasticArea(final String coscholasticarea, MasterEntryEntity masterEntryEntity){

//        final JSONArray param = new JSONArray();
        final JSONObject param = new JSONObject();
        final JSONObject urlparam = new JSONObject();


        try {
            param.put("MasterEntryStatus", masterEntryEntity.MasterEntryStatus);
            param.put("MasterEntryName",masterEntryEntity.MasterEntryName);
            param.put("MasterEntryValue",coscholasticarea);

            urlparam.put("MasterEntryId", masterEntryEntity.MasterEntryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

//        param.put(p);
        progressBar.setVisibility(View.VISIBLE);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/"
                + urlparam
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<MasterEntryEntity> masterEntryEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("masterEntry"),new TypeToken<List<MasterEntryEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.masterEntryModel().insertMasterEntries(masterEntryEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, CoScholasticAreaActivity.this);

                    Intent intent1 = new Intent(CoScholasticAreaActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, CoScholasticAreaActivity.this);

                    Intent intent2 = new Intent(CoScholasticAreaActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.VISIBLE);

            Config.responseVolleyErrorHandler(CoScholasticAreaActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void createCoScholasticArea(final String coscholasticarea){

        final JSONArray param = new JSONArray();
        final JSONObject p = new JSONObject();


        try {
            p.put("MasterEntryStatus", Gc.ACTIVE);
            p.put("MasterEntryName",COSCHOLAREA);
            p.put("MasterEntryValue",coscholasticarea);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

        param.put(p);
        progressBar.setVisibility(View.VISIBLE);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/"
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            String code = job.optString("code");
            progressBar.setVisibility(View.GONE);

            switch (code) {
                case Gc.APIRESPONSE200:
                    try {
                        ArrayList<MasterEntryEntity> masterEntryEntities = new Gson()
                                .fromJson(job.getJSONObject("result").optString("masterEntry"),new TypeToken<List<MasterEntryEntity>>(){}.getType());

                        new Thread(() -> {
                            mDb.masterEntryModel().insertMasterEntries(masterEntryEntities);
                        }).start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, CoScholasticAreaActivity.this);

                    Intent intent1 = new Intent(CoScholasticAreaActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, CoScholasticAreaActivity.this);

                    Intent intent2 = new Intent(CoScholasticAreaActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.VISIBLE);

            Config.responseVolleyErrorHandler(CoScholasticAreaActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();

        mDb.masterEntryModel().getMasterEntryValuesGeneric(COSCHOLAREA).observe(this, masterEntryEntities -> {
            coScholasticAreaList.clear();
            coScholasticAreaList.addAll(masterEntryEntities);
            adapter.notifyDataSetChanged();

        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.masterEntryModel().getMasterEntryValuesGeneric(COSCHOLAREA).removeObservers(this);
    }

    public class CoScholasticAreaAdapter extends RecyclerView.Adapter<CoScholasticAreaAdapter.MyViewHolder>{

        @NonNull
        @Override
        public CoScholasticAreaAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_recycler, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CoScholasticAreaAdapter.MyViewHolder holder, int position) {
            holder.tv_single_item.setText(coScholasticAreaList.get(position).MasterEntryValue);
        }

        @Override
        public int getItemCount() {
            return coScholasticAreaList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_single_item;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_single_item = itemView.findViewById(R.id.tv_single_item);
                tv_single_item.setTextColor(colorIs);

                itemView.setOnClickListener(view -> {
                    showDialogForCoScholasticAreaUpdate(coScholasticAreaList.get(getAdapterPosition()));
                });
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
