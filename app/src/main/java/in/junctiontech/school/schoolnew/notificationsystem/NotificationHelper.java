package in.junctiontech.school.schoolnew.notificationsystem;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.Profile.PersonalProfile;
import in.junctiontech.school.schoolnew.SchoolLibrary.Library;
import in.junctiontech.school.schoolnew.assignments.AssignmentMonthWiseActivity;
import in.junctiontech.school.schoolnew.chat.ChatActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.feesetup.feetype.receive.FeeReceiptsActivity;
import in.junctiontech.school.schoolnew.newgallery.GalleryMainActivity;
import in.junctiontech.school.schoolnew.notificationbar.NewNotificationBarActivity;
import in.junctiontech.school.schoolnew.schoolcalender.SchoolCalenderActivity;

public class NotificationHelper {

    public static void showNotificationSimple(Context context, String textTitle, String textContent,
                                              String channelid, int notificationIcon) {

        String GROUP_KEY = "in.junctiontech.school";
        int SUMMARY_ID = 0;

        Intent myNotificationIntent = new Intent();

        switch (channelid) {

            case Gc.CHANNELID_ATTENDANCE:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_HOMEWORK:

                myNotificationIntent = new Intent(context, AssignmentMonthWiseActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_EVENTS:

                myNotificationIntent = new Intent(context, SchoolCalenderActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case Gc.CHANNELID_NOTICEBOARD:

                myNotificationIntent = new Intent(context, NewNotificationBarActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_EXAM:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_FINE:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_FEE_RECEIPT:
                myNotificationIntent = new Intent(context, FeeReceiptsActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_FEE_REMINDER:
                myNotificationIntent = new Intent(context, FeeReceiptsActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_GROUP_MESSAGE:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_121_MESSAGE:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case Gc.CHANNELID_SURVEY:
                myNotificationIntent = new Intent(context, ChatActivity.class);
//            myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;

            case Gc.CHANNELID_LIBRARY:
                myNotificationIntent = new Intent(context, Library.class);
                break;


        }

//    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myNotificationIntent, 0);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(myNotificationIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelid)
                .setSmallIcon(notificationIcon)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                // .setGroup(GROUP_KEY)
                .setContentIntent(pendingIntent).setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

      /*  NotificationCompat.Builder summaryNotification =
                new NotificationCompat.Builder(context, channelid)
                        .setSmallIcon(notificationIcon)
                        //build summary info into InboxStyle template
                        .setStyle(new NotificationCompat.InboxStyle()
                                .setSummaryText("this is testing"))
                        //specify which group this notification belongs to
                        .setGroup(GROUP_KEY)
                        //set this notification as the summary for the group
                        .setGroupSummary(true);*/

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        //notificationManager.notify(SUMMARY_ID, summaryNotification.build());
        notificationManager.notify(getNextNotifId(context), mBuilder.build());
    }

    public static void showNotificationBitmap(String message, Bitmap image, String textTitle, Context context, String channelid) {

        Intent myNotificationIntent = new Intent();

        switch (channelid) {
            case Gc.CHANNELID_GALLERY_ADD:
                myNotificationIntent = new Intent(context, GalleryMainActivity.class);
                break;

            case Gc.CHANNELID_GREETINGS:
                myNotificationIntent = new Intent(context, PersonalProfile.class);
                myNotificationIntent.putExtra(Gc.CHANNELID_GREETINGS_TEXT, context.getString(R.string.happy_birth_day));
                break;
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, myNotificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelid)
                .setLargeIcon(image)/*Notification icon image*/
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(textTitle)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(image))/*Notification with Image*/
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(getNextNotifIdGallary(context), mBuilder.build());
    }

    private static final String PREFERENCE_LAST_NOTIF_ID = "PREFERENCE_LAST_NOTIF_ID";

    private static final String PREFERENCE_GALLERY_NOTIF_ID = "PREFERENCE_GALLERY_NOTIF_ID";

    private static int getNextNotifIdGallary(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int id = sharedPreferences.getInt(PREFERENCE_GALLERY_NOTIF_ID, 5000) + 1;
        if (id == Integer.MAX_VALUE) {
            id = 5000;
        } // isn't this over kill ??? hahaha!!  ^_^
        sharedPreferences.edit().putInt(PREFERENCE_GALLERY_NOTIF_ID, id).apply();
        return id;
    }

    private static int getNextNotifId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int id = sharedPreferences.getInt(PREFERENCE_LAST_NOTIF_ID, 0) + 1;
        if (id == Integer.MAX_VALUE) {
            id = 0;
        } // isn't this over kill ??? hahaha!!  ^_^
        sharedPreferences.edit().putInt(PREFERENCE_LAST_NOTIF_ID, id).apply();
        return id;
    }
}
