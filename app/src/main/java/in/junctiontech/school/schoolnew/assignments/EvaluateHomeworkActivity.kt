package `in`.junctiontech.school.schoolnew.assignments

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.assignments.adpter.AssignmentEvaluateListAdapter
import `in`.junctiontech.school.schoolnew.assignments.modelhelper.EvaluateHomework
import `in`.junctiontech.school.schoolnew.assignments.modelhelper.HomeworkEvaluateListModel
import `in`.junctiontech.school.schoolnew.common.Gc
import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_update_assignment.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

/**
 * Created by lekhpal Dangi on December 22 2020.
 */

class EvaluateHomeworkActivity : AppCompatActivity() {
    var homeworkId: String? = null
    var sectionId: String? = null
    var btnHomeworkEvaluate: Button? = null
    var rvStudentEvaluateList: RecyclerView? = null
    private val evaluateHomeworkArrayList: ArrayList<EvaluateHomework> = ArrayList()
    private var adapter: AssignmentEvaluateListAdapter? = null
    private var colorIs: Int = 0

    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(colorIs))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homework_evaluate)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        rvStudentEvaluateList = findViewById(R.id.rv_student_evaluate_list)
        btnHomeworkEvaluate = findViewById(R.id.btn_homework_evaluate)

        homeworkId = intent.getStringExtra("homeworkId")
        sectionId = intent.getStringExtra("SectionId")
        if (homeworkId.isNullOrEmpty() || sectionId.isNullOrEmpty()) {
            finish()
        } else {
            getStudentListEvaluation(homeworkId, sectionId)
        }
        setColorApp()
        rvStudentEvaluateList!!.setBackgroundColor(resources.getColor(R.color.backgroundColor))

        if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val gridLayoutManager = StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL)
            rvStudentEvaluateList!!.layoutManager = gridLayoutManager
        } else {
            val gridLayoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
            rvStudentEvaluateList!!.layoutManager = gridLayoutManager
        }

        adapter = AssignmentEvaluateListAdapter(this, evaluateHomeworkArrayList)
        rvStudentEvaluateList!!.adapter = adapter
        rvStudentEvaluateList!!.isLongClickable = true

        btnHomeworkEvaluate!!.setOnClickListener {
            saveHomeworkEvaluate()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun getStudentListEvaluation(homeworkId: String?, SectionId: String?) {
        progressBar.visibility = View.VISIBLE
        val param = JSONObject()
        param.put("homeworkId", homeworkId)
        param.put("SectionId", SectionId)

        val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "homework/evaluationHomework/" + param)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar.visibility = View.GONE
            when (code) {
                "200" -> try {

                    val he = Gson()
                            .fromJson<ArrayList<HomeworkEvaluateListModel>>(job.getJSONObject("result").optString("homeworkEvaluationDetails"),
                                    object : TypeToken<List<HomeworkEvaluateListModel>>() {}.type)
                    displayEvaluateHomework(he)

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.rl_homework_evaluate), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_update_assignment))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    fun displayEvaluateHomework(sa: ArrayList<HomeworkEvaluateListModel>) {
        evaluateHomeworkArrayList.clear()
        for (i in sa.indices) {
            val a = EvaluateHomework(sa[i].AdmissionID, sa[i].StudentName, sa[i].Homework?.get(0)?.status,
                    sa[i].ProfileImage, sa[i].AdmissionNo, homeworkId, sa[i].Homework?.get(0)?.homeworkDetailsId)
            evaluateHomeworkArrayList.add(a)
        }
        adapter!!.notifyDataSetChanged()
    }

    private fun saveHomeworkEvaluate() {
        val param: JSONArray = buildJsonForSave()
        if (param.length() > 0) {
            val url = (Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                    + Gc.ERPAPIVERSION
                    + "homework/evaluationHomework")

            val jsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
                val code = job.optString("code")
                progressBar.visibility = View.GONE
                when (code) {
                    "200" -> {
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById<View>(R.id.rl_homework_evaluate), R.color.fragment_first_blue)
                        finish()
                    }
                    else -> Config.responseSnackBarHandler(job.optString("message"),
                            findViewById<View>(R.id.rl_homework_evaluate), R.color.fragment_first_blue)
                }
            }, Response.ErrorListener { error ->
                progressBar.visibility = View.GONE
                Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_update_assignment))
            }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["APPKEY"] = Gc.APPKEY
                    headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                    headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                    headers["Content-Type"] = Gc.CONTENT_TYPE
                    headers["DEVICE"] = Gc.DEVICETYPE
                    headers["DEVICEID"] = Gc.id(applicationContext)
                    headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                    headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                    headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                    headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                    return headers
                }

                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray? {
                    return try {
                        param.toString().toByteArray(charset("utf-8"))
                    } catch (uee: UnsupportedEncodingException) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                        null
                    }
                }
            }
            jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
            AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
        } else {
            Config.responseSnackBarHandler(resources.getString(R.string.message),
                    findViewById<View>(R.id.rl_homework_evaluate), R.color.fragment_first_blue)
        }
    }

    private fun buildJsonForSave(): JSONArray {
        val inner = JSONArray()
        try {
            for (a in evaluateHomeworkArrayList) {
                if (!a.status.isNullOrEmpty()) {
                    inner.put(JSONObject().put("homeworkId", a.homeworkId)
                            .put("studentId", a.AdmissionID)
                            .put("status", a.status))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return inner
    }
}