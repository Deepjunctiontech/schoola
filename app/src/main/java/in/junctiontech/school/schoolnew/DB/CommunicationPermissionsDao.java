package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import in.junctiontech.school.schoolnew.model.CommunicationPermissions;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CommunicationPermissionsDao {
    @Query("select * from CommunicationPermissions")
    LiveData<List<CommunicationPermissions>> getCommunicationPermissions();

    @Query("select * from CommunicationPermissions where FromUserType Like :usertype")
    LiveData<List<CommunicationPermissions>> getCommunicationPermissionsForUserType(String usertype);

    @Query("delete from CommunicationPermissions ")
    void deleteAll();

    @Insert(onConflict = REPLACE)
    void insertCommunicationPermissions(List<CommunicationPermissions> communicationPermissions);
}
