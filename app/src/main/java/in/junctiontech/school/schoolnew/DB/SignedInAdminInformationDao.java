package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import in.junctiontech.school.schoolnew.model.AdminInformationEntity;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SignedInAdminInformationDao {
    @Query("select * from AdminInformationEntity limit 1")
    LiveData<AdminInformationEntity> getSignedInAdminInformation();

    @Insert(onConflict = REPLACE)
    void insertSignedInAdminInformation(AdminInformationEntity adminInformationEntity);
}
