package in.junctiontech.school.schoolnew.gallery;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 03-10-2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {
    GalleryActivity  galleryActivity;
    ArrayList<GalleryData> galleryListData;

    public GalleryAdapter(GalleryActivity  galleryActivity, ArrayList<GalleryData> galleryListData ) {
        this.galleryActivity = galleryActivity;
        this.galleryListData = galleryListData;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery,parent,false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GalleryData imageData = galleryListData.get(position);

        File f = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/" +Config.SCHOOL_IMAGE_FOLDER_NAME , imageData.getName());
        if (f.exists()){
            holder.ll_item_gallery_download.setVisibility(View.INVISIBLE);
           // holder.item_gallery_iv_share.setVisibility(View.VISIBLE);
            try {
                holder.item_gallery_iv.
                        setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(f)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else {
         //   holder.item_gallery_iv_share.setVisibility(View.INVISIBLE);
            holder.ll_item_gallery_download.setVisibility( View.GONE);
            holder.tv_item_gallery_download_size.setText(imageData.getSize()==null?"":imageData.getSize());

            Glide.with(galleryActivity.getApplicationContext()).load(imageData.getThumbnailUrl())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .thumbnail(0.5f)
                    .into(holder.item_gallery_iv);
        }
    }

    @Override
    public int getItemCount() {
        return galleryListData.size();
    }

    public void updateList(ArrayList<GalleryData> galleryListData) {
        this.galleryListData = galleryListData;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView item_gallery_iv/*,item_gallery_iv_share*/;
        private LinearLayout ll_item_gallery_download;
        private TextView tv_item_gallery_download_size;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_gallery_iv = (ImageView)itemView.findViewById(R.id.item_gallery_iv);
            /*item_gallery_iv_share = (ImageView)itemView.findViewById(R.id.item_gallery_iv_share);*/
            ll_item_gallery_download = (LinearLayout)itemView.findViewById(R.id.ll_item_gallery_download);
            tv_item_gallery_download_size = (TextView)itemView.findViewById(R.id.tv_item_gallery_download_size);
           /* item_gallery_iv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File f = new File(Environment.getExternalStorageDirectory().toString() +
                            "/" + Config.FOLDER_NAME + "/" +Config.SCHOOL_IMAGE_FOLDER_NAME , galleryListData.get(getLayoutPosition()).getName());
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/jpeg");

                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                    galleryActivity.startActivity(Intent.createChooser(share, "Share Image"));


                }
            });*/
        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private GalleryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final GalleryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
