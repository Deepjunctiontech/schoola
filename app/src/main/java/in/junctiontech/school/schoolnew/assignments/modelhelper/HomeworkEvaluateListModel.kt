package `in`.junctiontech.school.schoolnew.assignments.modelhelper

class HomeworkEvaluateListModel {
    var AdmissionID: String? = null
    var StudentName: String? = null
    var AdmissionNo: String? = null
    var ProfileImage: String? = null
    var SectionId: String? = null
    var FatherName: String? = null
    var Homework: Array<HomeworkStatus>?=null
}