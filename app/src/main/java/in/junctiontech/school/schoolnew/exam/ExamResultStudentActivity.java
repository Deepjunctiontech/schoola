package in.junctiontech.school.schoolnew.exam;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
//import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.ExamResultEntity;
import in.junctiontech.school.schoolnew.DB.ExamTypeEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class ExamResultStudentActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    ProgressBar progressBar;
    MainDatabase mDb;


    SchoolStudentEntity student;

    ArrayList<ExamResultEntity> examResultList = new ArrayList<>();
    ArrayList<ExamResultEntity> examResultFilter = new ArrayList<>();
    ArrayList<String> examTypeList = new ArrayList<>();
    String selecteExamType;

    ArrayAdapter<String>  examTypeListAdapter;
    AlertDialog.Builder examTypeListBuilder;

    ExamResultAdapter adapter;

    android.widget.SearchView searchView;

    Button btn_select_exam;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        btn_select_exam.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_result_student);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDb = MainDatabase.getDatabase(this);

        progressBar = findViewById(R.id.progressBar);
        mRecyclerView = findViewById(R.id.rv_exam_result);

        student = new Gson().fromJson( getIntent().getStringExtra("student"), SchoolStudentEntity.class);
        if (student == null ){
            finish();
           // Crashlytics.setString("CrashExamReason", "Student is Null");
        }else{
            if (student.SectionId == null){
                finish();
              //  Crashlytics.setString("CrashExamReason", "Student.StudentID is Null, Student Structure is not");
            }
            if (student.AdmissionId == null){
                finish();
              //  Crashlytics.setString("CrashExamReason", "Student.AdmissionID is Null, Student Structure is not");
            }
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ExamResultAdapter();
        mRecyclerView.setAdapter(adapter);

        initializeExamTypeButton();


        mDb.examTypeModel().getExamTypesName().observe(this, examTypeEntities -> {
            examTypeList.clear();
            examTypeListAdapter.clear();
            examTypeList.addAll(examTypeEntities);
            examTypeListAdapter.addAll(examTypeList);
            examTypeListAdapter.notifyDataSetChanged();
        });


        try {
            getScholasticExamMarks();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setColorApp();
        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4751158870",this,adContainer);
        }
    }

    private void initializeExamTypeButton(){

        btn_select_exam = findViewById(R.id.btn_select_exam);

        examTypeListBuilder = new AlertDialog.Builder(ExamResultStudentActivity.this);
        examTypeListAdapter =
                new ArrayAdapter<>(ExamResultStudentActivity.this, android.R.layout.select_dialog_singlechoice);

        examTypeListAdapter.addAll(examTypeList);
        btn_select_exam.setOnClickListener(view -> {
            examTypeListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

            examTypeListBuilder.setAdapter(examTypeListAdapter, (dialogInterface, i) -> {
                btn_select_exam.setText(examTypeList.get(i));
                selecteExamType = examTypeList.get(i);

/*    You can use setQuery() to change the text in the textbox.
     However, setQuery() method triggers the focus state of a search view,
     so a keyboard will show on the screen after this method has been invoked.
     To fix this problem, just call searchView.clearFocus() after setQuery()
     method to unfocus it and the keyboard will not show on the screen.*/

                searchView.setQuery(selecteExamType, false);
                searchView.clearFocus();

                adapter.notifyDataSetChanged();

            });

            AlertDialog dialog = examTypeListBuilder.create();
            dialog.show();

        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void getScholasticExamMarks() throws JSONException {
        if (student == null ){
            finish();
            return;
            // Crashlytics.setString("CrashExamReason", "Student is Null");
        }else{
            if (student.SectionId == null){
                finish();
                return;
                //  Crashlytics.setString("CrashExamReason", "Student.StudentID is Null, Student Structure is not");
            }
            if (student.AdmissionId == null){
                finish();
                return;
                //  Crashlytics.setString("CrashExamReason", "Student.AdmissionID is Null, Student Structure is not");
            }
        }

        progressBar.setVisibility(View.VISIBLE);


        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "exam/scholasticGradeResult/"
                + new JSONObject().put("Section_Id", student.SectionId).put("Student_Id", student.AdmissionId)
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            Log.i("Fee Type ", job.toString());
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try{
                        String resultjobj = job.getJSONObject("result")
                                .getJSONObject("scholasticGradeResult")
                                .optString(student.AdmissionId);


                        ArrayList<ExamResultEntity> examResults = new Gson().fromJson(resultjobj, new TypeToken<List<ExamResultEntity>>(){}.getType());
                        if (examResults.size() > 0){
                            examResultList.clear();
                            examResultFilter.clear();
                            examResultList.addAll(examResults);
                            examResultFilter.addAll(examResults);


                            adapter.notifyDataSetChanged();

                            Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }else{
                            Config.responseSnackBarHandler(getString(R.string.result_not_available),
                                    findViewById(R.id.top_layout),R.color.fragment_first_green);
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, ExamResultStudentActivity.this);

                    Intent intent1 = new Intent(ExamResultStudentActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, ExamResultStudentActivity.this);

                    Intent intent2 = new Intent(ExamResultStudentActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;

                default:
                Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
        Config.responseVolleyErrorHandler(ExamResultStudentActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =
                (android.widget.SearchView) menu.findItem(R.id.action_search1).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));



        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filterResult(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterResult(s);
                return true ;
            }
        });
        return true;
    }


    void filterResult(String text){

        examResultList.clear();
        if(text.isEmpty()) {
            selecteExamType = null;
            btn_select_exam.setText(R.string.exam_type_list);
            examResultList.addAll(examResultFilter);
        }else {
            text = text.toLowerCase();
            for (ExamResultEntity s: examResultFilter){
                if (s.Exam_Type.toLowerCase().contains(text)
                        || s.SubjectName.toLowerCase().contains(text)
                        || s.DateOfExam.toLowerCase().contains(text)
                        ){
                    examResultList.add(s);
                }
            }

        }
        adapter.notifyDataSetChanged();

    }

    public class ExamResultAdapter extends RecyclerView.Adapter<ExamResultAdapter.MyViewHolder>{

        @NonNull
        @Override
        public ExamResultAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_result_list_recycler, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ExamResultAdapter.MyViewHolder holder, int position) {
            if (selecteExamType == null){
                holder.tv_exam_type.setVisibility(View.VISIBLE);
                holder.tv_exam_date.setVisibility(View.VISIBLE);

            }else {
                holder.tv_exam_type.setVisibility(View.GONE);
                holder.tv_exam_date.setVisibility(View.GONE);
            }
            holder.tv_exam_type.setText(examResultList.get(position).Exam_Type);
            holder.tv_exam_date.setText(examResultList.get(position).DateOfExam);
            holder.tv_exam_subject.setText(examResultList.get(position).SubjectName);
            holder.tv_grade.setText(examResultList.get(position).Grade);
            holder.tv_marks.setText(examResultList.get(position).Marks_Obtain + "/"+ examResultList.get(position).Max_Marks);
        }

        @Override
        public int getItemCount() {
            return examResultList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_exam_type,tv_exam_subject,tv_marks,tv_grade,tv_exam_date;
            public MyViewHolder(View itemView) {
                super(itemView);
                tv_exam_type = itemView.findViewById(R.id.tv_exam_type);
                tv_exam_type.setTextColor(colorIs);
                tv_exam_subject = itemView.findViewById(R.id.tv_exam_subject);
                tv_exam_subject.setTextColor(colorIs);
                tv_marks = itemView.findViewById(R.id.tv_marks);
                tv_marks.setTextColor(colorIs);
                tv_grade = itemView.findViewById(R.id.tv_grade);
                tv_grade.setTextColor(colorIs);
                tv_exam_date = itemView.findViewById(R.id.tv_exam_date);

            }
        }
    }
}
