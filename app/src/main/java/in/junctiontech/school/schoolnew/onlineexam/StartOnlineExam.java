package in.junctiontech.school.schoolnew.onlineexam;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.FullScreenImageActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.onlineexam.db.OnlineExamModel;

import static in.junctiontech.school.schoolnew.common.Gc.SCHOOLPREF;

public class StartOnlineExam extends AppCompatActivity {

    private CountDownTimer totalcountDownTimer;
    private String time_duration = "";

    SharedPreferences pref;

    ProgressBar progressBar;
    OnlineExamModel exam;

    RecyclerView mRecyclerView;
    ListAdapter adapter;

    NestedScrollView exam_scroll_view_summary;
    ScrollView exam_scroll_view_questions;

    Button btn_goto_summary,
            btn_goto_questions,
            btn_submit_exam;

    Button prevQuestion, notAttemptedQuestion, nextQuestion;

    TextView tv_exam_name, tv_duration, tv_max_marks, tv_questions, tv_total_test_timer;

    TextView tv_test_question;

    TextView tv_test_s_no;

    ImageView imageViewMainQuestionHeading;

    LinearLayout ll_checkboxes;
    ArrayList<CheckBox> checkBoxes = new ArrayList<>();

    OnlineExamModel.ExamSectionDetails selectedSection;
    int selectedSectionIndex = 0;
    OnlineExamModel.ExamSectionDetails.SectionQuestion displayedQuestion;
    int selectedQuestionIndex = 0;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        tv_exam_name.setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_online_exam);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for   private mode

        progressBar = findViewById(R.id.progressBar);
        String runningExam = getIntent().getStringExtra("exam");
        if ("".equalsIgnoreCase(runningExam)) {
            finish();
            return;
        } else {
            exam = new Gson().fromJson(runningExam, OnlineExamModel.class);
        }

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2781253248", this, adContainer);
        }

        tv_exam_name = findViewById(R.id.tv_exam_name);
        tv_exam_name.setText(exam.onlineExamName);

        tv_duration = findViewById(R.id.tv_duration);
        tv_duration.setText(exam.examDuration);

        tv_max_marks = findViewById(R.id.tv_max_marks);
        tv_max_marks.setText(exam.examMaxMarks);

        tv_questions = findViewById(R.id.tv_questions);
        tv_questions.setText(exam.numberOfQuestions);

        tv_total_test_timer = findViewById(R.id.tv_total_test_timer);

        exam_scroll_view_summary = findViewById(R.id.exam_scroll_view_summary);
        exam_scroll_view_summary.setVisibility(View.GONE);

        exam_scroll_view_questions = findViewById(R.id.exam_scroll_view_questions);
        tv_test_question = findViewById(R.id.tv_test_question);
        tv_test_s_no = findViewById(R.id.tv_test_s_no);
        ll_checkboxes = findViewById(R.id.ll_checkboxes);
        imageViewMainQuestionHeading = findViewById(R.id.imageViewQuestionsHeading);

        btn_goto_summary = findViewById(R.id.btn_goto_summary);
        btn_goto_summary.setOnClickListener(v -> {
            exam_scroll_view_questions.setVisibility(View.GONE);
            exam_scroll_view_summary.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        });

        btn_submit_exam = findViewById(R.id.btn_submit_exam);
        btn_submit_exam.setOnClickListener(v -> {

            submitExam();

        });

        btn_goto_questions = findViewById(R.id.btn_goto_questions);
        btn_goto_questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exam_scroll_view_summary.setVisibility(View.GONE);
                exam_scroll_view_questions.setVisibility(View.VISIBLE);
            }
        });

        notAttemptedQuestion = findViewById(R.id.notAttemptedQuestion);
        notAttemptedQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OnlineExamModel.ExamSectionDetails.SectionQuestion question = findNextNotAttemptedQuestion();

                if (question == null) {

                    AlertDialog.Builder alert1 = new AlertDialog.Builder(StartOnlineExam.this);
                    alert1.setTitle(getString(R.string.all_questions_finished));
                    alert1.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_green_colour, null));
                    alert1.setPositiveButton(R.string.summary, (dialog, which) -> {
                        Config.responseSnackBarHandler(getString(R.string.completed),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);
                        exam_scroll_view_questions.setVisibility(View.GONE);
                        exam_scroll_view_summary.setVisibility(View.VISIBLE);
                        adapter.notifyDataSetChanged();
                    });
                    alert1.show();

                } else {
                    buildAndDisplayQuestion();
                }
            }
        });

        prevQuestion = findViewById(R.id.prevQuestion);
        prevQuestion.setOnClickListener(v -> buildAndDisplayPreviousQuestion());

        nextQuestion = findViewById(R.id.nextQuestion);
        nextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findNextQuestion();
                buildAndDisplayQuestion();
            }
        });

        mRecyclerView = findViewById(R.id.rv_exam_sections);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new ListAdapter();

        mRecyclerView.setAdapter(adapter);

        displayedQuestion = findNextNotAttemptedQuestion();

        if (displayedQuestion != null) {
            buildAndDisplayQuestion();
            startTimer();
        } else {
            exam_scroll_view_summary.setVisibility(View.VISIBLE);
            exam_scroll_view_questions.setVisibility(View.GONE);
            selectedSectionIndex = 0;
            selectedQuestionIndex = 0;
            buildAndDisplayQuestion();
            startTimer();
        }

        setColorApp();
    }

    void submitExam() {
        displayedQuestion = findNextNotAttemptedQuestion();
        if (displayedQuestion != null) {
            AlertDialog.Builder alert1 = new AlertDialog.Builder(StartOnlineExam.this);
            alert1.setTitle(getString(R.string.not_attempted_question));
            alert1.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_alert, null));
            alert1.setPositiveButton(R.string.yes_i_understand, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        saveCompletedExamToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            alert1.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
            alert1.setMessage(getString(R.string.after_submission_you_will_not_be_able_to_make_any_change));
            alert1.show();
        } else {
            AlertDialog.Builder alert1 = new AlertDialog.Builder(StartOnlineExam.this);
            alert1.setTitle(getString(R.string.finish));
            alert1.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_green_colour, null));
            String positive = getResources().getString(R.string.finish_online_exam, "!!");
            alert1.setPositiveButton(positive, (dialog, which) -> {
                try {
                    saveCompletedExamToServer();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
            alert1.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
            alert1.setMessage(getString(R.string.after_submission_you_will_not_be_able_to_make_any_change));
            alert1.show();
        }
    }

    private void startTimer() {
        Calendar cal_current = Calendar.getInstance();
        Calendar cal_start_test = Calendar.getInstance();

        if (!exam.examTimer.equals("")) {
            String[] aa = exam.examTimer.split(":");

            if (aa.length > 0 && Integer.parseInt(aa[0]) != 0) {
                cal_start_test.add(Calendar.MINUTE, Integer.parseInt(aa[0]));
            }
            // 1 index for second
            if (aa.length > 1 && Integer.parseInt(aa[1]) != 0) {
                cal_start_test.add(Calendar.SECOND, Integer.parseInt(aa[1]));
            }
        } else {
            cal_start_test.add(Calendar.MINUTE, Integer.parseInt(exam.examDuration));
        }

        long timer = cal_start_test.getTimeInMillis() - cal_current.getTimeInMillis();

        totalcountDownTimer = new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                //   mybuilder.setContentText()
                if (((millisUntilFinished / (1000 * 60 * 60)) % 24) != 0) {
                    time_duration = ((millisUntilFinished / (1000 * 60 * 60)) % 24) + ":" + ((millisUntilFinished / (1000 * 60)) % 60) + ":" +
                            (millisUntilFinished / 1000) % 60;
                } else {
                    time_duration = ((millisUntilFinished / (1000 * 60)) % 60) + ":" +
                            (millisUntilFinished / 1000) % 60;
                }
                tv_total_test_timer.setText(getString(R.string.total_time) + " : " + time_duration);
            }

            public void onFinish() {
                if (!isFinishing()) {
                    tv_total_test_timer.setText(getString(R.string.time_is_over));

                    AlertDialog.Builder alert1 = new AlertDialog.Builder(StartOnlineExam.this);
                    alert1.setTitle(getString(R.string.finish));
                    alert1.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_green_colour, null));
                    alert1.setPositiveButton(R.string.yes_i_understand, (dialog, which) -> {
                        try {
                            saveCompletedExamToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    });
                    alert1.setMessage(getString(R.string.your_exam_time_is_over));
                    alert1.setCancelable(false);
                    alert1.show();
                }
            }
        }.start();
    }

    void buildAndDisplayQuestion() {
        tv_test_question.setText(exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).qustion);
        tv_test_s_no.setText(String.valueOf(selectedSectionIndex + 1) + "-" + String.valueOf(selectedQuestionIndex + 1));

        String imageUrl = exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).imageUrl;
        if (imageUrl != null) {
            if (imageUrl.trim().length() > 0) {
                imageViewMainQuestionHeading.setVisibility(View.VISIBLE);
                Glide.with(this).load(imageUrl).into(imageViewMainQuestionHeading);

                ArrayList<String> galleryImageURLs = new ArrayList<>();

                imageViewMainQuestionHeading.setOnClickListener(v -> {

                    galleryImageURLs.add(imageUrl);

                    Intent intent = new Intent(this, FullScreenImageActivity.class);
                    intent.putStringArrayListExtra("images", galleryImageURLs);
                    intent.putExtra("index", 0);
                    intent.putExtra("title", String.format("%s : %s", "Online", "Image"));
                    startActivity(intent);
                });
            } else {
                imageViewMainQuestionHeading.setVisibility(View.GONE);
            }
        } else {
            imageViewMainQuestionHeading.setVisibility(View.GONE);
        }

        ll_checkboxes.removeAllViews();

        if (exam.examSectionDetails
                .get(selectedSectionIndex).sectionQuestions
                .get(selectedQuestionIndex).questionType.equalsIgnoreCase("MultipleChoice")) {

            ArrayList<String> selectedOptions = new ArrayList<>();
            if (exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer != null) {
                selectedOptions = new ArrayList<>(Arrays.asList(exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer.split(",")));
            }

            checkBoxes.clear();

            int loopCount = exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).qus_option.size();

            for (int i = 0; i < loopCount; i++) {

                String option = exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).qus_option.get(i).Options;
                CheckBox ch = new CheckBox(StartOnlineExam.this);
                ch.setClickable(false);
                LinearLayout linearLayoutMain = new LinearLayout(this);
                LinearLayout.LayoutParams linearLayoutMainParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayoutMainParam.setMargins(10, 10, 10, 10);
                linearLayoutMain.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.rectangle_box, null));
                linearLayoutMain.setLayoutParams(linearLayoutMainParam);
                linearLayoutMain.setPadding(5, 5, 5, 5);
                linearLayoutMain.setOrientation(LinearLayout.HORIZONTAL);
                linearLayoutMain.setGravity(Gravity.CENTER_VERTICAL);


                ch.setGravity(Gravity.CENTER_VERTICAL);
                if (selectedOptions.contains(String.valueOf(i))) {
                    ch.setChecked(true);
                }

                linearLayoutMain.setOnClickListener(v -> {
                    ch.performClick();
                    updateAnswerForMCQ(ll_checkboxes.indexOfChild(v), ch.isChecked());
                });

                linearLayoutMain.addView(ch);

                LinearLayout linearLayoutSub = new LinearLayout(this);
                LinearLayout.LayoutParams linearLayoutSubParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearLayoutSubParam.setMargins(10, 0, 0, 0);
                linearLayoutSub.setLayoutParams(linearLayoutSubParam);
                linearLayoutSub.setOrientation(LinearLayout.VERTICAL);

                //if option string is empty then only checkbox without text
                if (option != null) {
                    if (option.trim().length() > 0) {
                        TextView textViewCheckBox = new TextView(this);
                        LinearLayout.LayoutParams textViewParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        textViewParam.setMargins(0, 20, 0, 0);
                        textViewCheckBox.setText(option);
                        linearLayoutSub.addView(textViewCheckBox);

                    }
                }

                String optionsImageUrl = exam.examSectionDetails
                        .get(selectedSectionIndex)
                        .sectionQuestions
                        .get(selectedQuestionIndex)
                        .qus_option.get(i)
                        .OptionImageURL;

                if (optionsImageUrl != null) {
                    if (optionsImageUrl.trim().length() > 0) {
                        RelativeLayout rl = new RelativeLayout(this);
                        rl.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        rl.setGravity(Gravity.CENTER_VERTICAL);

                        ImageView imageViewOptionUrl = new ImageView(this);
                        imageViewOptionUrl.setLayoutParams(new LinearLayout.LayoutParams(300, 200));
                        Glide.with(this).load(optionsImageUrl).into(imageViewOptionUrl);

                        imageViewOptionUrl.setOnClickListener(v -> {
                            ArrayList<String> galleryImageURLs = new ArrayList<>();
                            galleryImageURLs.add(optionsImageUrl);
                            Intent intent = new Intent(this, FullScreenImageActivity.class);
                            intent.putStringArrayListExtra("images", galleryImageURLs);
                            intent.putExtra("index", 0);
                            intent.putExtra("title", String.format("%s : %s", "Online Option", "Image"));
                            startActivity(intent);
                        });
                        rl.addView(imageViewOptionUrl);
                        linearLayoutSub.addView(rl);
                    }
                }

                linearLayoutMain.addView(linearLayoutSub);

                ll_checkboxes.addView(linearLayoutMain);
            }
        } else {
            final EditText txt = new EditText(this);
            txt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (getCurrentFocus() == txt) {

                        if (String.valueOf(s).trim().equalsIgnoreCase("")) {
                            exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).status = "NotAttempted";
                        } else {
                            exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).status = "Attempted";
                        }

                        exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = Strings.nullToEmpty(s.toString());
//                        updateExamStatusInSharedPrefernce();
                    }
                }
            });
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
            txt.setLayoutParams(lp);
            txt.setSingleLine(false);
            txt.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            txt.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            if (exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer != null) {
                txt.setText(exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer);
            }
            ll_checkboxes.addView(txt);
        }
    }

    void updateAnswerForMCQ(int option, Boolean checked) {
        if (exam.examSectionDetails
                .get(selectedSectionIndex).sectionQuestions
                .get(selectedQuestionIndex).questionType.equalsIgnoreCase("MultipleChoice")) {
            ArrayList<String> selectedOptions;

            String studentAns = exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer;

            if (studentAns == null || "".equalsIgnoreCase(studentAns)) {
                selectedOptions = new ArrayList<>();
            } else {
                selectedOptions = new ArrayList<>(Arrays.asList(studentAns.split(",")));
            }

            if (selectedOptions.size() > 1 && checked) {
                if (!(selectedOptions.contains(String.valueOf(option))))
                    selectedOptions.add(String.valueOf(option));
                exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = StringUtil.join(selectedOptions, ",");
            } else if (selectedOptions.size() > 1 && !checked) {
                if (selectedOptions.contains(String.valueOf(option)))
                    selectedOptions.remove(String.valueOf(option));
                exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = StringUtil.join(selectedOptions, ",");
            } else if (selectedOptions.size() == 1 && !checked) {
                exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = null;
            } else if (selectedOptions.size() == 1 && checked) {
                if (!(selectedOptions.contains(String.valueOf(option))))
                    selectedOptions.add(String.valueOf(option));
                exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = StringUtil.join(selectedOptions, ",");
            } else if (selectedOptions.size() == 0 && checked) {
                exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer = String.valueOf(option);
            }
        }

        if (exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer != null
                && !("".equalsIgnoreCase(exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).studentAnswer))) {
            exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).status = "Attempted";
        } else {
            exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.get(selectedQuestionIndex).status = "NotAttempted";
        }
    }

    void findNextQuestion() {
        if ((selectedQuestionIndex + 1) < exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.size()) {
            selectedQuestionIndex++;
        } else if ((selectedSectionIndex + 1) < exam.examSectionDetails.size()) {
            selectedSectionIndex++;
            selectedQuestionIndex = 0;
        } else {
            Config.responseSnackBarHandler(getString(R.string.last_question),
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
        }
    }

    void buildAndDisplayPreviousQuestion() {
        if (selectedQuestionIndex > 0) {
            --selectedQuestionIndex;
        } else if (selectedSectionIndex > 0) {
            --selectedSectionIndex;
            if (exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.size() > 1)
                selectedQuestionIndex = exam.examSectionDetails.get(selectedSectionIndex).sectionQuestions.size() - 1;
        }

        buildAndDisplayQuestion();
        if (selectedSectionIndex == 0 && selectedQuestionIndex == 0) {
            Config.responseSnackBarHandler("No Previous question",
                    findViewById(R.id.top_layout), R.color.fragment_first_green);
        }
    }

    OnlineExamModel.ExamSectionDetails.SectionQuestion findNextNotAttemptedQuestion() {

        int totalSections = exam.examSectionDetails.size();
        selectedSectionIndex = 0;
        for (OnlineExamModel.ExamSectionDetails section : exam.examSectionDetails) {
            selectedQuestionIndex = 0;
            int totalQuestionInSection = section.sectionQuestions.size();
            for (OnlineExamModel.ExamSectionDetails.SectionQuestion question : section.sectionQuestions) {
                if ("NotAttempted".equalsIgnoreCase(question.status)) {
                    selectedSection = section;
                    return question;
                }
                if (selectedQuestionIndex < (totalQuestionInSection - 1))
                    selectedQuestionIndex++;
            }
            if (selectedSectionIndex < (totalSections - 1))
                selectedSectionIndex++;
        }

        return null;
    }

    private class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {
        @NonNull
        @Override
        public ListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.online_exam_section, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListAdapter.MyViewHolder myViewHolder, int i) {
            myViewHolder.exam_section_name.setText(exam.examSectionDetails.get(i).sectionName);
            myViewHolder.exam_section_subject.setText(exam.examSectionDetails.get(i).SubjectName);
            myViewHolder.section_total_questions.setText(exam.examSectionDetails.get(i).numberOfQuestions);
            myViewHolder.section_maximum_marks.setText(exam.examSectionDetails.get(i).maxMarks);
            int attempted = 0;
            for (OnlineExamModel.ExamSectionDetails.SectionQuestion question : exam.examSectionDetails.get(i).sectionQuestions) {
                if ("NotAttempted".equalsIgnoreCase(question.status)) {

                } else {
                    attempted = attempted + 1;
                }
            }
            myViewHolder.section_completed_questions.setText(String.valueOf(attempted));
        }

        @Override
        public int getItemCount() {
            return exam.examSectionDetails.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView exam_section_name,
                    exam_section_subject,
                    section_total_questions,
                    section_completed_questions,
                    section_maximum_marks;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                exam_section_name = itemView.findViewById(R.id.exam_section_name);
                exam_section_name.setTextColor(colorIs);

                exam_section_subject = itemView.findViewById(R.id.exam_section_subject);
                exam_section_subject.setTextColor(colorIs);

                section_total_questions = itemView.findViewById(R.id.section_total_questions);

                section_completed_questions = itemView.findViewById(R.id.section_completed_questions);

                section_maximum_marks = itemView.findViewById(R.id.section_maximum_marks);
                section_maximum_marks.setTextColor(colorIs);
            }
        }
    }

    JSONObject buildJsonForSave() throws JSONException {
        JSONObject param = new JSONObject();
        JSONArray answers = new JSONArray();

        param.put("onlineExamAssignToStudentId", exam.onlineExamAssignToStudentId);
        param.put("Session", Gc.getSharedPreference(Gc.APPSESSION, this));
        param.put("examTimer", exam.examDuration);

        for (OnlineExamModel.ExamSectionDetails section : exam.examSectionDetails) {
            for (OnlineExamModel.ExamSectionDetails.SectionQuestion question : section.sectionQuestions) {
                String studentAnswer = "";
                if ("MultipleChoice".equalsIgnoreCase(question.questionType)) {
                    if (question.studentAnswer != null) {
                        if (question.studentAnswer.length() > 1) {
                            ArrayList<String> selectedOptions = new ArrayList<>(Arrays.asList(question.studentAnswer.split(",")));

                            ArrayList<String> selectedOptionPlus1 = new ArrayList<>();
                            for (String s : selectedOptions) {
                                selectedOptionPlus1.add(String.valueOf(Integer.parseInt(s) + 1));
                            }
                            studentAnswer = StringUtil.join(selectedOptionPlus1, ",");
                            // question.studentAnswer = StringUtil.join(selectedOptionPlus1, ",");
                        } else if (question.studentAnswer.length() == 1) { //Single Response
                            studentAnswer = String.valueOf(Integer.parseInt(question.studentAnswer) + 1);
                            // question.studentAnswer = String.valueOf(Integer.parseInt(question.studentAnswer) + 1);
                        }
                    }
                }
                answers.put(new JSONObject()
                        .put("stu_To_Qust_Id", question.stu_To_Qust_Id)
                        .put("answer", studentAnswer)
                        .put("status", question.status));

            }
        }
        param.put("answers", answers);
        return param;
    }

    void saveCompletedExamToServer() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject param = buildJsonForSave();
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "onlineExam/finishOnlineExam";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        SharedPreferences.Editor editor = pref.edit();
                        try {
                            JSONObject resultjobj = job.getJSONObject("result").getJSONObject("studentOnlineExamResult");
                            if (resultjobj.length() == 0) {

                                Handler handler = new Handler();
                                handler.postDelayed(new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }), 3000);

                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.top_layout), R.color.fragment_first_green);
                            } else {
                                String result = resultjobj.toString();
                                Intent intent = new Intent(StartOnlineExam.this, OnlineExamResult.class);
                                intent.putExtra("studentOnlineExamResult", result);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:
                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StartOnlineExam.this);

                        Intent intent1 = new Intent(StartOnlineExam.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StartOnlineExam.this);

                        Intent intent2 = new Intent(StartOnlineExam.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;
                    case Gc.APIRESPONSE403:
                        Handler handler = new Handler();
                        handler.postDelayed(new Thread(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }), 3000);

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_green);

                        break;
                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.top_layout), R.color.fragment_first_blue);
                }
            }
        }, error -> {
            progressBar.setVisibility(View.VISIBLE);
            Config.responseVolleyErrorHandler(StartOnlineExam.this, error,
                    findViewById(R.id.top_layout));
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert1 = new AlertDialog.Builder(StartOnlineExam.this);

        String title = getResources().getString(R.string.finish_online_exam, "??");
        alert1.setTitle(title);
        alert1.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_clickhere, null));

        String positive = getResources().getString(R.string.finish_online_exam, "!!");
        alert1.setPositiveButton(positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitExam();
            }
        });
        alert1.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert1.setMessage(getString(R.string.exam_will_be_finished));
        alert1.setCancelable(false);
        alert1.show();
    }
}