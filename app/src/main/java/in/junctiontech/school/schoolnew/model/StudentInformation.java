package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import in.junctiontech.school.schoolnew.DB.SchoolStudentEntity;

/**
 * Created by deep on 28/03/18.
 */
@Entity
public class StudentInformation {
    @PrimaryKey
    @NonNull
    public String AdmissionId;
    public String AdmissionNo;
    public String RegistrationId;
    public String Status;
    public String StudentName;
    public String StudentEmail;
    public String FatherName;
    public String FatherMobile;
    public String studentProfileImage;
    public String parentProfileImage;
    public String FatherDateOfBirth;
    public String FatherEmail;
    public String FatherQualification;
    public String FatherOccupation;
    public String FatherDesignation;
    public String FatherOrganization;
    public String MotherName;
    public String MotherMobile;
    public String MotherDateOfBirth;
    public String MotherEmail;
    public String MotherQualification;
    public String MotherOccupation;
    public String MotherDesignation;
    public String MotherOrganization;
    public String Mobile;
    public String DOB;
    public String Landline;
    public String AlternateMobile;
    public String PresentAddress;
    public String PermanentAddress;
    public String BloodGroup;
    public String Caste;
    public String Category;
    public String Gender;
    public String Nationality;
    public String parentGcmTokenId;
    public String studentGcmTokenId;
    public String studentIMEI;
    public String parentIMEI;
    public String ParentDevice;
    public String StudentDevice;
    public String DateOfTermination;
    public String TerminationReason;
    public String TerminationRemarks;
    public String Pterms;
    public String Sterms;
    public String SectionId;
    public String SectionName;
    public String ClassName;
    public String ClassId;
    public String BloodGroupValue;
    public String CasteValue;
    public String CategoryValue;
    public String GenderValue;

    public SchoolStudentEntity getStudent(){
        SchoolStudentEntity student = new SchoolStudentEntity();

        student.AdmissionId = AdmissionId;
        student.AdmissionNo = AdmissionNo;

        student.RegistrationId = RegistrationId;
        student.Status = Status;
        student.StudentName = StudentName;
        student.StudentEmail = StudentEmail;
        student.FatherName = FatherName;
        student.FatherMobile = FatherMobile;
        student.studentProfileImage = studentProfileImage;
        student.parentProfileImage = parentProfileImage;
        student.FatherDateOfBirth = FatherDateOfBirth;
        student.FatherEmail = FatherEmail;
        student.FatherQualification = FatherQualification;
        student.FatherOccupation = FatherOccupation;
        student.FatherDesignation = FatherDesignation;
        student.FatherOrganization = FatherOrganization;
        student.MotherName = MotherName;
        student.MotherMobile = MotherMobile;
        student.MotherDateOfBirth = MotherDateOfBirth;
        student.MotherEmail = MotherEmail;
        student.MotherQualification = MotherQualification;
        student.MotherOccupation = MotherOccupation;
        student.MotherDesignation = MotherDesignation;
        student.MotherOrganization = MotherOrganization;
        student.Mobile = Mobile;
        student.DOB = DOB;
        student.Landline = Landline;
        student.AlternateMobile = AlternateMobile;
        student.PresentAddress = PresentAddress;
        student.PermanentAddress = PermanentAddress;
        student.BloodGroup = BloodGroup;
        student.Caste = Caste;
        student.Category = Category;
        student.Gender = Gender;
        student.Nationality = Nationality;
        student.parentGcmTokenId = parentGcmTokenId;
        student.studentGcmTokenId = studentGcmTokenId;
        student.studentIMEI = studentIMEI;
        student.parentIMEI = parentIMEI;
        student.ParentDevice = ParentDevice;
        student.StudentDevice = StudentDevice;
        student.DateOfTermination = DateOfTermination;
        student.TerminationReason = TerminationReason;
        student.TerminationRemarks = TerminationRemarks;
        student.Pterms = Pterms;
        student.Sterms = Sterms;
        student.SectionId = SectionId;
        student.SectionName = SectionName;
        student.ClassName = ClassName;
        student.ClassId = ClassId;
        student.BloodGroupValue = BloodGroupValue;
        student.CasteValue = CasteValue;
        student.CategoryValue = CategoryValue;
        student.GenderValue = GenderValue;

        return student;
    }
}
