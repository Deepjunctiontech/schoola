package in.junctiontech.school.schoolnew.DB.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import java.util.List;

import in.junctiontech.school.schoolnew.DB.SchoolClassSectionEntity;
import in.junctiontech.school.schoolnew.DB.repository.SchoolClassSectionRepository;

public class SchoolSectionViewModel extends AndroidViewModel {

    private SchoolClassSectionRepository mSchoolClassSectionRepository;

    public SchoolSectionViewModel(@NonNull Application application) {
        super(application);
        mSchoolClassSectionRepository = new SchoolClassSectionRepository(application);
    }

    public LiveData<List<SchoolClassSectionEntity>> getClassSection(String classid){
        return  mSchoolClassSectionRepository.getClassSection(classid);
    }
    public void insert(SchoolClassSectionEntity[] schoolClassSectionEntities){
        mSchoolClassSectionRepository.insert(schoolClassSectionEntities);
    }
}
