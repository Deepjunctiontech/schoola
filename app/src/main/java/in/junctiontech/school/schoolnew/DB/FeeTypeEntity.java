package in.junctiontech.school.schoolnew.DB;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class FeeTypeEntity {
    @PrimaryKey
    @NonNull
    public String FeeTypeID;
    public String FeeType;
    public String Frequency;
    public String generationDate;
    public String Status;
    public String CreatedOn;
    public String CreatedBy;
    public String UpdatedBy;
    public String UpdatedOn;
    public String ExMonths;

}
