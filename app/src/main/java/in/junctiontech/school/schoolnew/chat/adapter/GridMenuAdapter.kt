package `in`.junctiontech.school.schoolnew.chat.adapter


import `in`.junctiontech.school.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_manu.view.*

class GridMenuAdapter() : RecyclerView.Adapter<GridMenuAdapter.MenuViewHolder>() {
    var listener: GridMenuListener? = null
    private val menus = arrayListOf(
            Menu(
                    "Image",
                    R.drawable.ic_gallery
            ),
            Menu(
                    "PDF",
                    R.drawable.ic_pdf
            )
    )

    interface GridMenuListener {
        fun dismissPopup()
        fun selectAttachment(menuName: String)
    }

    private val data = ArrayList<Menu>().apply {
        addAll(menus)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return MenuViewHolder.create(parent,viewType)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.bind(data[position], listener)
    }

    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(menu: Menu, listener: GridMenuListener?) {
            with(itemView) {
                tvTitle.text = menu.name
                ivIcon.setImageDrawable(ContextCompat.getDrawable(context, menu.drawable))
                itemView.setOnClickListener {
                    listener?.selectAttachment(menu.name)
                    listener?.dismissPopup()
                }
            }
        }

        companion object {
            val LAYOUT = R.layout.item_manu
            fun create(parent: ViewGroup, viewType: Int): MenuViewHolder {
                return MenuViewHolder(
                        LayoutInflater.from(parent.context).inflate(LAYOUT,parent,false)
                )
            }
        }
    }

    data class Menu(val name: String, @DrawableRes val drawable: Int) {}
}