package in.junctiontech.school.schoolnew.adminpanel;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 10-08-2017.
 */

public class UserActionAdapter extends RecyclerView.Adapter<UserActionAdapter.MyViewHoler> {
    private Context context;
    private ArrayList<UserActionModel> ownerSchoolList;
    private int colorIs;
    int[][] states = new int[][]{
            new int[]{-android.R.attr.state_checked},  // unchecked
            new int[]{android.R.attr.state_checked},   // checked
            new int[]{}                                // default
    };
    private float textSise = 0;
    private float layoutHeight = 0;
    private float layoutWidth = 0;
    // Fill in color corresponding to state defined in state
    int[] colorsText;
    private boolean isComesAdminDrawer;

    public UserActionAdapter(Context context,
                             ArrayList<UserActionModel> ownerSchoolList, int colorIs, boolean isComesAdminDrawer/*true if comes from AdminNavigation Drawer*/) {
        TypedValue out = new TypedValue();
        context.getResources().getValue(R.dimen.linear_layout_option_text_size, out, true);
        float floatResource = out.getFloat();

        this.context = context;
        this.ownerSchoolList = ownerSchoolList;
        this.colorIs = colorIs;
        this.isComesAdminDrawer = isComesAdminDrawer;
        this.textSise = floatResource;
        this.layoutHeight = context.getResources().getDimension(R.dimen.linear_layout_option_height);

    }

    @Override
    public UserActionAdapter.MyViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_action_view, parent, false);
        return new UserActionAdapter.MyViewHoler(view);
    }


    @Override
    public void onBindViewHolder(UserActionAdapter.MyViewHoler holder, int position) {
        UserActionModel obj = ownerSchoolList.get(position);
        holder.owner_item_organization_logo.setBackgroundResource(obj.getIconName());
        colorsText = new int[]{
                obj.getTintColor(),
                obj.getTintColor(),
                obj.getTintColor(),
        };
        if (obj.isTintNeeded()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                holder.owner_item_organization_logo.setBackgroundTintList(new ColorStateList(states, colorsText));
        }
        holder.owner_item_organization_name.setText(obj.getName());
        holder.owner_item_organization_name.setTextColor(colorIs);
    }

    @Override
    public int getItemCount() {
        return ownerSchoolList.size();
    }

    public void updateData(ArrayList<UserActionModel> userActionList, int colorIs) {
        this.ownerSchoolList = userActionList;
        this.colorIs = colorIs;
        notifyDataSetChanged();
    }

    public class MyViewHoler extends RecyclerView.ViewHolder {
        public ImageView owner_item_organization_logo;
        public TextView owner_item_organization_name;

        public MyViewHoler(View itemView) {
            super(itemView);
            owner_item_organization_logo = itemView.findViewById(R.id.iv_user_action_title);
            owner_item_organization_name = itemView.findViewById(R.id.tv_user_action_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isComesAdminDrawer)
                        ((AdminNavigationDrawerNew) context).setOnClickViewAction(ownerSchoolList.get(getLayoutPosition()));
                    else
                        ((SubActivity) context).setOnClickViewAction(ownerSchoolList.get(getLayoutPosition()));
                }
            });
            if (!isComesAdminDrawer) {
                owner_item_organization_name.setTextSize(16);
                LinearLayout layout = itemView.findViewById(R.id.ll_user_action_title);
                ViewGroup.LayoutParams params = layout.getLayoutParams();
// Changes the height and width to the specified *pixels*
                params.height = (int) layoutHeight;
                layout.setLayoutParams(params);
            }
        }
    }
}
