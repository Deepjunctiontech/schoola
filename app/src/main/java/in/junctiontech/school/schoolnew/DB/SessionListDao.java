package in.junctiontech.school.schoolnew.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface SessionListDao {
    @Query("select * from SessionListEntity")
    public LiveData<List<SessionListEntity>> getSessionList();

    @Query("select * from SessionListEntity where session LIKE :thissession")
    public SessionListEntity getSession(String thissession);

    @Insert(onConflict = REPLACE)
    void insertSession(SessionListEntity sessionListEntity);

    @Insert(onConflict = REPLACE)
    void insertSessionList(List<SessionListEntity> sessionListEntity);

    @Insert(onConflict = REPLACE)
    void insertSessions(SessionListEntity... sessionListEntities);
}
