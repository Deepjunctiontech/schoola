package in.junctiontech.school.schoolnew.messaging.Fragments;

import android.app.SearchManager;
import androidx.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.messaging.MsgAdapters.TeacherListAdapter;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 12/27/2016.
 */

public class StaffListFragment extends
        Fragment implements SearchView.OnQueryTextListener{
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private SwipeRefreshLayout swipe_refresh_list;
    private MainDatabase mDb;
    private ArrayList<SchoolStaffEntity> teacherListData= new ArrayList<>();
    private TeacherListAdapter adapter;
    private boolean  msgToTeacher = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data = convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available =  convertView.findViewById(R.id.tv_view_all_data_not_data_available);
        swipe_refresh_list =convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);
        if (!msgToTeacher) {
            tv_view_all_data_not_data_available.setText(getString(R.string.you_dont_have_permission_to_send_messages_to) + " " +
                    getString(R.string.staff));
        }
        setHasOptionsMenu(true);
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (msgToTeacher) {
            mDb.schoolStaffModel().getAllStaff().observe(this, new Observer<List<SchoolStaffEntity>>() {
                @Override
                public void onChanged(@Nullable List<SchoolStaffEntity> schoolStaffEntities) {
                    teacherListData.clear();
                    teacherListData.addAll(schoolStaffEntities);
                    adapter.notifyDataSetChanged();
                }
            });
        }
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();
    }


    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new TeacherListAdapter(getContext(), teacherListData,   Config.getAppColor(getActivity(),false));
        recycler_view_list_of_data.setAdapter(adapter);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = MainDatabase.getDatabase(getContext());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setFilter(ArrayList<SchoolStaffEntity> teacherListData) {
        if (adapter!=null) adapter.setFilter(teacherListData);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_sort_and_search, menu);

        SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        android.widget.SearchView searchView =
                (android.widget.SearchView) menu.findItem(R.id.menu_action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                setFilter(teacherListData);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                setFilter(teacherListData);
                return true;
            }
        });

//        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);
//
//        /* --  toolbar search ---*/
//        final MenuItem item = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(this);
//
//        MenuItemCompat.setOnActionExpandListener(item,
//                new MenuItemCompat.OnActionExpandListener() {
//                    @Override
//                    public boolean onMenuItemActionCollapse(MenuItem item) {
//// Do something when collapsed
//                           setFilter(teacherListData);
//                        return true; // Return true to collapse action view
//                    }
//
//                    @Override
//                    public boolean onMenuItemActionExpand(MenuItem item) {
//// Do something when expanded
//                        return true; // Return true to expand action view
//                    }
//                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<SchoolStaffEntity> filteredModelList = filter(teacherListData, newText);
        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<SchoolStaffEntity> filter(ArrayList<SchoolStaffEntity> models, String query) {
        query = query.toLowerCase();final ArrayList<SchoolStaffEntity> filteredModelList = new ArrayList<>();

        for (SchoolStaffEntity model : models) {

            if (model.StaffName.toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

}
