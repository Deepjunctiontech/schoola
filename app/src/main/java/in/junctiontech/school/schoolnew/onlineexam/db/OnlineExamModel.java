package in.junctiontech.school.schoolnew.onlineexam.db;


import java.util.ArrayList;

public class OnlineExamModel {

    public String onlineExamAssignToStudentId;
    public String onlineExamId;
    public String AdmissionId;
    public String StudentName;
    public String onlineExamName;
    public String status;
    public String examOpenDate;
    public String examOpenTime;
    public String examCloseDate;
    public String examCloseTime;
    public String examDuration;
    public String examTimer;
    public String examMaxMarks;
    public String numberOfQuestions;
    public String TotalAttempted;
    public String TotalNotAttempted;
    public String TotalAttemptedReviewed;
    public String TotalNotAttemptedReviewed;
    public String showResult;
    public String showHint;
    public String showSolution;
    public String createdBy;
    public String createdOn;
    public String updatedBy;
    public String updatedOn;
    public ArrayList<ExamSectionDetails> examSectionDetails;

    public class ExamSectionDetails{
        public String onlineExamSectionId;
        public String ClassName;
        public String sectionName;
        public String examType;
        public String numberOfQuestions;
        public String SubjectName;
        public String topic;
        public String book;
        public String maxMarks;
        public String obtainMarks;
        public String sectionTimeDuration;
        public ArrayList<SectionQuestion> sectionQuestions;

        public class SectionQuestion{
            public String stu_To_Qust_Id;
            public String onlineExamAssignToStudentId;
            public String qust_id;
            public String qustion;
            public String status;
            public String answer;
            public String studentAnswer;
            public String qust_ans_description;
            public String qust_solution;
            public String questionType;
            public String marks;
            public String obtainMarks;
            public String numberOfCharactersAns;
            public String imageUrl;
            public ArrayList<QuestionOption> qus_option;

            public class QuestionOption{
                public String OptionNumber;
                public String Options;
                public String OptionImageURL;

            }

        }

    }
}
