package `in`.junctiontech.school.schoolnew.chat

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.BuildConfig
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MainDatabase
import `in`.junctiontech.school.schoolnew.DB.SchoolStaffEntity
import `in`.junctiontech.school.schoolnew.DB.SchoolStudentEntity
import `in`.junctiontech.school.schoolnew.chat.adapter.ChatConversationAdapter
import `in`.junctiontech.school.schoolnew.chat.modelhelper.ChatMainArrayList
import `in`.junctiontech.school.schoolnew.chat.widget.MenuEditText
import `in`.junctiontech.school.schoolnew.chat.widget.SoftKeyBoardPopup
import `in`.junctiontech.school.schoolnew.chatdb.ChatDatabase
import `in`.junctiontech.school.schoolnew.chatdb.ChatListEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import `in`.junctiontech.school.schoolnew.model.ClassNameSectionName
import `in`.junctiontech.school.schoolnew.model.CommunicationPermissions
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Base64.DEFAULT
import android.util.Base64.encodeToString
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.tasks.Task
import com.google.common.base.Strings
import com.google.firebase.dynamiclinks.DynamicLink.AndroidParameters
import com.google.firebase.dynamiclinks.DynamicLink.IosParameters
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.chat_layout.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by lekhpal Dangi on March 04 2020.
 */
class ChatConversationActivity : AppCompatActivity(), MenuEditText.PopupListener,
        ChatConversationAdapter.ChatClickListener, SoftKeyBoardPopup.attachmentClickListener {
    var chatDb: ChatDatabase? = null
    private val chatList: ArrayList<ChatListEntity> = ArrayList()
    private val chatMainList: ArrayList<ChatMainArrayList> = ArrayList()
    private var chatPermissions = ArrayList<String>()
    private var with: String? = null
    private var withType: String? = null
    lateinit var rootView: ConstraintLayout
    lateinit var etMessageChat: MenuEditText
    private var mRecyclerView: RecyclerView? = null
    private var adapter: ChatConversationAdapter? = null
    var selectedSection: ClassNameSectionName? = null
    var selectedStudent: SchoolStudentEntity? = null
    private var selectedStaff: SchoolStaffEntity? = null
    private var hasPermission = true

    private var textViewUserName: TextView? = null
    private var textViewSelectedMsgCount: TextView? = null

    private var toolbar2: androidx.appcompat.widget.Toolbar? = null
    private var llMenuLayout: LinearLayout? = null
    private var llUser: LinearLayout? = null
    private var rlUserImage: RelativeLayout? = null
    private var llSelectedMessageCount: LinearLayout? = null
    private var imageViewUserImage: CircleImageView? = null

    private var relativeLayoutDeleteButton: RelativeLayout? = null
    private var imageViewBackArrow: ImageView? = null
    private var btn_sendChat: ImageView? = null

    private var ivCopy: ImageView? = null
    private var iv_delete: ImageView? = null
    var deleteForEverOne = true
    var colorIs = 0
    private fun setColorApp() {
        colorIs = Config.getAppColor(this, true)
        toolbar2!!.setBackgroundColor(colorIs)
    }

    lateinit var menuKeyboard: SoftKeyBoardPopup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_layout)
        intView()
        val mDb: MainDatabase = MainDatabase.getDatabase(this)
        chatDb = ChatDatabase.getDatabase(this)

        if (!listOf(*Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext).toLowerCase(Locale.ROOT)) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase(Locale.ROOT).equals("yes", ignoreCase = true)) {
            val adContainer = findViewById<View>(R.id.adMobView)
            Config.adsInitialize("ca-app-pub-1890254643259173/1718156640", this, adContainer)
        }
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) findViewById<View>(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_landscape) else findViewById<View>(R.id.rl_chat_activity).setBackgroundResource(R.drawable.wallpaper_chat_vartical)
        etMessageChat = findViewById(R.id.et_message_chat)

        etMessageChat.popupListener = this
        btn_sendChat = findViewById(R.id.btn_sendChat)
        val btnSendChat = findViewById<ImageView>(R.id.btn_sendChat)
        mRecyclerView = findViewById(R.id.recycler_view_chat_layout)
        rootView = findViewById(R.id.rl_chat_activity)

        mRecyclerView!!.setBackgroundColor(resources.getColor(R.color.backgroundColor))

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = false
        mRecyclerView!!.layoutManager = layoutManager
        adapter = ChatConversationAdapter(this, chatMainList, this)
        mRecyclerView!!.adapter = adapter
        mRecyclerView!!.isLongClickable = true

        mDb.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPETEXT, this))
                .observe(this, Observer<List<CommunicationPermissions>> { communicationPermissions: List<CommunicationPermissions> ->
                    chatPermissions.clear()
                    for (c in communicationPermissions) {
                        chatPermissions.add(c.ToUserType)
                    }
                })
        btnSendChat.setOnClickListener { sendChat() }
        if (Strings.nullToEmpty(intent.getStringExtra("previous")).equals("", ignoreCase = true)) {
            when (intent.getStringExtra("withType")!!) {
                "section" -> {
                    selectedSection = Gson()
                            .fromJson(intent.getStringExtra("with"), ClassNameSectionName::class.java)
                    with = selectedSection!!.SectionId
                    withType = "section"
                }
                "student" -> {
                    selectedStudent = Gson()
                            .fromJson(intent.getStringExtra("with"), SchoolStudentEntity::class.java)
                    with = selectedStudent!!.AdmissionId
                    withType = "student"
                }
                "staff" -> {
                    selectedStaff = Gson()
                            .fromJson(intent.getStringExtra("with"), SchoolStaffEntity::class.java)
                    with = selectedStaff!!.UserId
                    withType = "staff"
                }
            }
        } else {
            val chat: ChatListEntity = Gson().fromJson(intent.getStringExtra("previous"), ChatListEntity::class.java)
            with = chat.withId
            withType = chat.withType
            if ("school".equals(chat.withType, ignoreCase = true)) {
                menu_chat.visibility = View.GONE
                btn_sendChat!!.visibility = View.GONE
                etMessageChat.visibility = View.GONE
            }
        }
        when (withType) {
            "admin" -> {
                //supportActionBar!!.title="admin"
                textViewUserName!!.text = "admin"
                imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
            }
            "student" -> {
                if (selectedStudent == null) {
                    mDb.schoolStudentModel().getStudentById(with).observe(this, Observer { schoolStudentEntity: SchoolStudentEntity? ->
                        if (schoolStudentEntity == null) {
                            return@Observer
                        } else {
                            selectedStudent = schoolStudentEntity
                            //supportActionBar!!.title = selectedStudent!!.StudentName
                            textViewUserName!!.text = selectedStudent!!.StudentName
                            if (!"".equals(selectedStudent!!.studentProfileImage, ignoreCase = true)) {
                                Glide.with(this)
                                        .load(selectedStudent!!.studentProfileImage)
                                        .apply(RequestOptions()
                                                .error(R.drawable.ic_single)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                                        .thumbnail(0.5f)
                                        .into(imageViewUserImage!!)
                            } else {
                                imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
                            }
                        }
                    })
                } else {
                    // supportActionBar!!.title = selectedStudent!!.StudentName
                    textViewUserName!!.text = selectedStudent!!.StudentName

                    if (!"".equals(selectedStudent!!.studentProfileImage, ignoreCase = true)) {
                        Glide.with(this)
                                .load(selectedStudent!!.studentProfileImage)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .thumbnail(0.5f)
                                .into(imageViewUserImage!!)
                    } else {
                        imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
                    }
                }
            }
            "staff" -> {
                if (selectedStaff == null) {
                    mDb.schoolStaffModel().getStaffByUserId(with).observe(this, Observer { staffEntity: SchoolStaffEntity? ->
                        if (staffEntity != null) {
                            selectedStaff = staffEntity
                            //supportActionBar!!.title = selectedStaff!!.StaffName
                            textViewUserName!!.text = selectedStaff!!.StaffName

                            if (!"".equals(selectedStaff!!.staffProfileImage, ignoreCase = true)) {
                                Glide.with(this)
                                        .load(selectedStaff!!.staffProfileImage)
                                        .apply(RequestOptions()
                                                .error(R.drawable.ic_single)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                                        .thumbnail(0.5f)
                                        .into(imageViewUserImage!!)
                            } else {
                                imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
                            }
                        }
                    })
                } else {
                    //supportActionBar!!.title = selectedStaff!!.StaffName
                    textViewUserName!!.text = selectedStaff!!.StaffName
                    if (!"".equals(selectedStaff!!.staffProfileImage, ignoreCase = true)) {
                        Glide.with(this)
                                .load(selectedStaff!!.staffProfileImage)
                                .apply(RequestOptions()
                                        .error(R.drawable.ic_single)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .thumbnail(0.5f)
                                .into(imageViewUserImage!!)
                    } else {
                        imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
                    }
                }
            }
            "section" -> if (selectedSection == null) {
                mDb.schoolClassSectionModel().getClassSectionNameSingle(with).observe(this, Observer { classNameSectionName: ClassNameSectionName? ->
                    if (classNameSectionName != null) {
                        selectedSection = classNameSectionName
                        // supportActionBar!!.title = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                        textViewUserName!!.text = selectedSection!!.ClassName + " " + selectedSection!!.SectionName

                        imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))

                    }
                })
            } else {
                imageViewUserImage!!.setImageDrawable(getDrawable(R.drawable.ic_single))
                //supportActionBar!!.title = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                textViewUserName!!.text = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
            }
            else -> {
                imageViewUserImage!!.visibility = View.GONE
                //supportActionBar!!.title = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                textViewUserName!!.text = Gc.getSharedPreference(Gc.SCHOOLNAME, this)
            }
        }

        chatDb!!.chatListModel().getPendingChats(with, withType).observe(this, Observer<List<ChatListEntity?>> { chatListEntities: List<ChatListEntity?> -> Log.i("unsent messages", chatListEntities.size.toString() + "") })

        menuKeyboard = SoftKeyBoardPopup(
                this,
                rootView,
                etMessageChat,
                etMessageChat,
                menuChatContainer,
                this
        )

        menu_chat.setOnClickListener {
            toggle()
        }
        setColorApp()
    }

    private fun toggle() {

        if (menuKeyboard.isShowing) {
            menuKeyboard.dismiss()
        } else {
            menuKeyboard.show()
        }
    }

    override fun onDestroy() {
        menuKeyboard.clear()
        super.onDestroy()
    }

    override fun getPopup(): PopupWindow {
        return menuKeyboard
    }

    private fun intView() {

        textViewUserName = findViewById(R.id.textViewUserName)
        toolbar2 = findViewById(R.id.toolbar_chat_Layout)
        llUser = findViewById(R.id.ll_user)
        rlUserImage = findViewById(R.id.rl_user_image)
        llMenuLayout = findViewById(R.id.linearLayoutCopyAndDeletebutton)
        imageViewUserImage = findViewById(R.id.imageViewUserImage)

        llSelectedMessageCount = findViewById(R.id.llSelectedMessageCount)
        textViewSelectedMsgCount = findViewById(R.id.textViewSelectedMsgCount)

        imageViewBackArrow = findViewById(R.id.imageViewBackArrow)
        imageViewBackArrow!!.setOnClickListener {
            onBackPressed()
        }

        relativeLayoutDeleteButton = findViewById(R.id.relativeLayoutDeleteButton)
        ivCopy = findViewById(R.id.iv_copy)
        ivCopy!!.setOnClickListener {
            copyMessage()
        }
        iv_delete = findViewById(R.id.iv_delete)
        iv_delete!!.setOnClickListener {
            deleteMessage()
        }
    }

    private fun deleteMessage() {
        if (chatMainList.size > 0) {
            val msgIds = ArrayList<String>()
            val param = JSONArray()
            val j = JSONObject()
            j.put("withType", withType)
            j.put("withId", with)
            var deleteMessageCount = 0
            for (i in chatMainList.indices) {
                if (chatMainList[i].isSelected) {
                    deleteMessageCount++
                    chatMainList[i].isSelected = false
                    param.put(chatMainList[i].msgId)
                    msgIds.add(chatMainList[i].msgId.toString())
                }
            }
            j.put("msgId", param)
            if (deleteForEverOne) {
                val viewGroup: ViewGroup = findViewById(android.R.id.content)
                val dialogView: View = LayoutInflater.from(this).inflate(R.layout.dialog_delete_chat_msg, viewGroup, false)
                val builder = AlertDialog.Builder(this)

                builder.setView(dialogView)

                val alertDialog = builder.create()
                alertDialog.show()

                val deleteMessage = dialogView.findViewById<TextView>(R.id.deleteMessage)

                deleteMessage.text = resources.getString(R.string.delete_message)

                val deleteForMe = dialogView.findViewById<TextView>(R.id.deleteForMe)
                deleteForMe.setTextColor(colorIs)
                deleteForMe.setOnClickListener {
                    try {
                        // createNewClass(editText.text.toString().trim { it <= ' ' }.toUpperCase())
                        alertDialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                val btnDeleteForMeEverOne = dialogView.findViewById<TextView>(R.id.deleteForEverOne)

                btnDeleteForMeEverOne.setTextColor(colorIs)
                btnDeleteForMeEverOne.setOnClickListener {
                    try {
                        deleteForEverOne(j, msgIds)
                        alertDialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                val msgDeleteCancel = dialogView.findViewById<TextView>(R.id.msgDeleteCancel)

                msgDeleteCancel.setTextColor(colorIs)
                msgDeleteCancel.setOnClickListener {
                    try {
                        // createNewClass(editText.text.toString().trim { it <= ' ' }.toUpperCase())
                        alertDialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            } else {
                val alertUpdate = AlertDialog.Builder(this)
                alertUpdate.setTitle(resources.getString(R.string.delete) + " " + deleteMessageCount + " " + resources.getString(R.string.messages))

                alertUpdate.setPositiveButton(R.string.delete_for_me) { dialog, which ->
                    deleteMessageFromLocal(msgIds)
                }
                alertUpdate.setNegativeButton(R.string.cancel, null)
                alertUpdate.setCancelable(false)
                alertUpdate.show()
            }
        }
    }

    private fun deleteForEverOne(param: JSONObject, msgIds: ArrayList<String>) {
        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "chat/chatDelete")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    val msg = "You deleted this message"
                    Thread { chatDb!!.chatListModel().chatDelete(msg, msgIds) }.start()
                }
                "300" -> {
                    val alertUpdate = AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                    alertUpdate.setTitle(job.optString("message"))
                    alertUpdate.setIcon(R.drawable.ic_alert)
                    alertUpdate.setPositiveButton(R.string.yes) { dialog, which ->
                        val p = JSONObject()
                        try {
                            p.put("orgKey", Gc.getSharedPreference(Gc.ERPINSTCODE, this))
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        val dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLink(Uri.parse("https://education.zeroerp.com/Login/verifyOrganization/"
                                        + Gc.getSharedPreference(Gc.ERPINSTCODE, this) + "?orgKey=" + p))
                                .setDomainUriPrefix("https://kn3yy.app.goo.gl") // Open links with this app on Android
                                .setAndroidParameters(AndroidParameters.Builder(BuildConfig.APPLICATION_ID).build()) // Open links with com.example.ios on iOS
                                .setIosParameters(IosParameters.Builder("in.junctiontech.school")
                                        .setAppStoreId("1261309387")
                                        .build())
                                .buildDynamicLink()
                        val dynamicLinkUri = dynamicLink.uri
                        FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLongLink(Uri.parse(dynamicLinkUri.toString()))
                                .buildShortDynamicLink()
                                .addOnCompleteListener(this) { task: Task<ShortDynamicLink?> ->
                                    if (task.isSuccessful) {
                                        if (task.result != null) {
                                            val shortLink = task.result!!.shortLink
                                            val refererBody1 = " $shortLink"
                                            val refererIntent1 = Intent(Intent.ACTION_SEND)
                                            refererIntent1.type = "text/plain"
                                            refererIntent1.putExtra(Intent.EXTRA_TEXT, refererBody1)
                                            startActivity(Intent.createChooser(refererIntent1, "Share App"))
                                            overridePendingTransition(R.anim.enter, R.anim.nothing)
                                        }
                                    } else {
                                        Log.e("main", " error " + task.exception)
                                    }
                                }
                    }
                    alertUpdate.setNegativeButton(R.string.no, null)
                    alertUpdate.setCancelable(false)
                    if (!isFinishing()) {
                        alertUpdate.show()
                    }
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun deleteMessageFromLocal(msgIds: ArrayList<String>) {
        Thread(Runnable { chatDb!!.chatListModel().deleteChat(msgIds) }).start()
        notifyAdapterAfterAllOperation()
    }

    private fun copyMessage() {
        var copyMessage = ""
        if (chatMainList.size > 0) {
            var isFirstMessageCopy = true
            for (i in chatMainList.indices) {
                if (chatMainList[i].isSelected) {
                    if (isFirstMessageCopy) {
                        isFirstMessageCopy = false
                        chatMainList[i].isSelected = false
                        copyMessage += chatMainList[i].msg

                    } else {
                        chatMainList[i].isSelected = false
                        copyMessage = copyMessage + "\n" + chatMainList[i].msg
                    }
                }
            }
        }

        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", copyMessage)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Message Copied", Toast.LENGTH_SHORT).show()

        ChatConversationAdapter.longClick = false
        llMenuLayout!!.visibility = View.GONE

        adapter!!.notifyDataSetChanged()

    }

    private fun sendChat() {
        if (etMessageChat!!.text.toString().isEmpty()) return
        val today = Date()
        //  val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
        // val dateToStr = format.format(today)

        val enteredDate = enteredDateFormat.format(today)
        val chatListEntity = ChatListEntity()
        chatListEntity.withType = withType
        chatListEntity.withId = with
        when (withType) {
            "student" -> if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                    || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                chatListEntity.withName = selectedStudent!!.StudentName
                chatListEntity.withProfileUrl = selectedStudent!!.studentProfileImage
                hasPermission = true
            } else {
                hasPermission = false
            }
            "staff" -> if (chatPermissions.contains(selectedStaff!!.UserTypeValue)
                    || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                chatListEntity.withName = selectedStaff!!.StaffName
                chatListEntity.withProfileUrl = selectedStaff!!.staffProfileImage
                hasPermission = true
            } else {
                hasPermission = false
            }
            "admin" -> {
                chatListEntity.withName = "admin"
                chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this)
                hasPermission = if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                    Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equals(Gc.DEMO, ignoreCase = true)
                } else {
                    true
                }
            }
            "section" -> if (chatPermissions.contains("STUDENT")
                    || chatPermissions.contains("PARENTS")
                    || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                if (selectedSection == null) {
                    Toast.makeText(this, resources.getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show()
                    hasPermission = false
                } else {
                    chatListEntity.withName = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                    chatListEntity.withProfileUrl = ""
                    hasPermission = true
                }
            } else {
                hasPermission = false
            }
        }
        if (hasPermission) {
            chatListEntity.byMe = 1
            chatListEntity.msg = etMessageChat!!.text.toString()
            chatListEntity.enteredDate = enteredDate
            chatListEntity.msgType = "text"
            chatListEntity.sent = 0
            chatListEntity.msgId = (Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                    + "-"
                    + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                    + "-"
                    + Gc.getSharedPreference(Gc.USERID, this)
                    + "-"
                    + Date().time)
            Thread(Runnable { chatDb!!.chatListModel().insertChat(chatListEntity) }).start()
            //chatList.add(chatListEntity)
            //adapter!!.notifyItemInserted(chatList.size - 1)
            notifyAdapterAfterAllOperation()
            try {
                sendChatTextToServerSingle(chatListEntity)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            etMessageChat!!.text.clear()
        } else {
            Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                    findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Thread { chatDb!!.chatListModel().updateReadStatus(with, withType) }.start()
        finish()
    }

    override fun onResume() {
        val pref = getSharedPreferences(Gc.SCHOOLPREF, 0) // 0 - for private mode
        val editor = pref.edit()
        editor.putBoolean(Gc.CHATWINDOWACTIVE, true)
        editor.apply()
        super.onResume()
        notifyAdapterAfterAllOperation()
    }

    private fun notifyAdapterAfterAllOperation() {
        Thread(Runnable { chatDb!!.chatListModel().updateReadStatus(with, withType) }).start()
        chatDb!!.chatListModel().getChatWithUser(withType, with).observe(this, Observer<List<ChatListEntity>>
        { chatListEntities: List<ChatListEntity>? ->
            chatList.clear()
            chatList.addAll(chatListEntities!!)

            var messageDivideOld = ""
            var position = 0
            chatMainList.clear()
            for (i in chatList.indices) {
                if (chatList[i].byMe != 1 && chatList[i].readByMe == 0 && position == 0) {
                    position = i
                }
                val chatListMains = ChatMainArrayList()
                chatListMains.autoid = chatList[i].autoid
                chatListMains.byMe = chatList[i].byMe
                chatListMains.enteredDate = chatList[i].enteredDate
                chatListMains.imageOriginalPath = chatList[i].imageOriginalPath
                chatListMains.imagepath = chatList[i].imagepath
                chatListMains.msg = chatList[i].msg
                chatListMains.msgId = chatList[i].msgId
                chatListMains.msgType = chatList[i].msgType
                chatListMains.read = chatList[i].read
                chatListMains.readByMe = chatList[i].readByMe
                chatListMains.readDate = chatList[i].readDate
                chatListMains.received = chatList[i].received
                chatListMains.receivedDate = chatList[i].receivedDate
                chatListMains.senderId = chatList[i].senderId
                chatListMains.senderType = chatList[i].senderType
                chatListMains.sentDate = chatList[i].sentDate
                chatListMains.withId = chatList[i].withId
                chatListMains.withProfileUrl = chatList[i].withProfileUrl
                chatListMains.withName = chatList[i].withName
                chatListMains.withType = chatList[i].withType
                chatListMains.sent = chatList[i].sent
                chatListMains.localPath = chatList[i].localPath
                chatListMains.fileSize = chatList[i].fileSize

                val millisecondsDates = Gc.getMiliseconds(chatList[i].enteredDate, false)

                val calendarOld = Calendar.getInstance()
                calendarOld.timeInMillis = millisecondsDates

                val formatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)
                val enteredDate = formatter.format(calendarOld.time)

                val millisecondsDate = Gc.getMiliseconds(enteredDate, true)

                if (null != millisecondsDate) {
                    val messageDivideNew: String = Gc.getDate(millisecondsDate)
                    if (messageDivideOld != messageDivideNew) {
                        messageDivideOld = messageDivideNew
                        chatListMains.dateHeader = messageDivideNew
                    }
                }

                chatMainList.add(chatListMains)
            }

            adapter!!.notifyDataSetChanged()
            if (chatMainList.size > 0) {
                if (position == 0) {
                    position = chatMainList.size - 1
                }
                mRecyclerView!!.smoothScrollToPosition(position)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        val pref = getSharedPreferences(Gc.SCHOOLPREF, 0) // 0 - for private mode
        val editor = pref.edit()
        editor.putBoolean(Gc.CHATWINDOWACTIVE, false)
        editor.apply()
        chatDb!!.chatListModel().getChatWithUser(withType, with).removeObservers(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //   getMenuInflater().inflate(R.menu.menu_chats, menu);
        return true
    }

    @Throws(JSONException::class)
    fun sendChatTextToServerSingle(c: ChatListEntity) {
        val param = JSONArray()
        val j = JSONObject()
        j.put("msgId", c.msgId)
        j.put("withType", withType)
        if (withType.equals("section", ignoreCase = true)) {
            j.put("withName", c.withName)
        } else {
            j.put("withName", Gc.getSharedPreference(Gc.SIGNEDINUSERDISPLAYNAME, this))
        }
        j.put("withId", c.withId)
        j.put("msgType", c.msgType)
        j.put("msg", c.msg)
        j.put("enteredDate", c.enteredDate)
        if (!c.imagepath.isNullOrEmpty()) {
            j.put("imagepath", c.imagepath)
        }
        if (!c.imageOriginalPath.isNullOrEmpty()) {
            j.put("imageOriginalPath", c.imageOriginalPath)
        }
        if (!c.fileSize.isNullOrEmpty()) {
            j.put("fileSize", c.fileSize)
        }
        param.put(j)
        Log.e("param", param.toString())
        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "chat/chat")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    Thread(Runnable { chatDb!!.chatListModel().updateSentStatus(c.msgId) }).start()
                    notifyAdapterAfterAllOperation()
                }
                "300" -> {
                    val alertUpdate = AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                    alertUpdate.setTitle(job.optString("message"))
                    alertUpdate.setIcon(resources.getDrawable(R.drawable.ic_alert, null))
                    alertUpdate.setPositiveButton(R.string.yes) { dialog, which ->
                        val p = JSONObject()
                        try {
                            p.put("orgKey", Gc.getSharedPreference(Gc.ERPINSTCODE, this))
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        val dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLink(Uri.parse("https://education.zeroerp.com/Login/verifyOrganization/"
                                        + Gc.getSharedPreference(Gc.ERPINSTCODE, this) + "?orgKey=" + p))
                                .setDomainUriPrefix("https://kn3yy.app.goo.gl") // Open links with this app on Android
                                .setAndroidParameters(AndroidParameters.Builder(BuildConfig.APPLICATION_ID).build()) // Open links with com.example.ios on iOS
                                .setIosParameters(IosParameters.Builder("in.junctiontech.school")
                                        .setAppStoreId("1261309387")
                                        .build())
                                .buildDynamicLink()
                        val dynamicLinkUri = dynamicLink.uri
                        FirebaseDynamicLinks.getInstance().createDynamicLink()
                                .setLongLink(Uri.parse(dynamicLinkUri.toString()))
                                .buildShortDynamicLink()
                                .addOnCompleteListener(this) { task: Task<ShortDynamicLink?> ->
                                    if (task.isSuccessful) {
                                        if (task.result != null) {
                                            val shortLink = task.result!!.shortLink
                                            val refererBody1 = " $shortLink"
                                            val refererIntent1 = Intent(Intent.ACTION_SEND)
                                            refererIntent1.type = "text/plain"
                                            refererIntent1.putExtra(Intent.EXTRA_TEXT, refererBody1)
                                            startActivity(Intent.createChooser(refererIntent1, "Share App"))
                                            overridePendingTransition(R.anim.enter, R.anim.nothing)
                                        }
                                    } else {
                                        Log.e("main", " error " + task.exception)
                                    }
                                }
                    }
                    alertUpdate.setNegativeButton(R.string.no, null)
                    alertUpdate.setCancelable(false)
                    if (!isFinishing()) {
                        alertUpdate.show()
                    }
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onLongItemPressed(position: Int, isSelected: Boolean) {
        chatMainList[position].isSelected = isSelected
        var isANyItemSelected = false
        var selectedMsgCount = 0
        deleteForEverOne = true
        for (i in chatMainList.indices) {
            if (chatMainList[i].isSelected) {
                isANyItemSelected = true

                if (chatMainList[i].byMe != 1) {
                    deleteForEverOne = false
                }
                selectedMsgCount++
            }
        }

        if (isANyItemSelected) {
            /*  ll_user!!.visibility=View.GONE
              rl_user_image!!.visibility=View.GONE
              llSelectedMessageCount!!.visibility=View.VISIBLE
              textViewSelectedMsgCount!!.text=selectedMsgCount.toString()*/
            llMenuLayout!!.visibility = View.VISIBLE
        } else {
            ChatConversationAdapter.longClick = false
            deleteForEverOne = true
            llMenuLayout!!.visibility = View.GONE
            /*  ll_user!!.visibility=View.VISIBLE
              rl_user_image!!.visibility=View.VISIBLE
              llSelectedMessageCount!!.visibility=View.GONE
              textViewSelectedMsgCount!!.text="0"*/
        }

        if (!deleteForEverOne) {
            relativeLayoutDeleteButton!!.visibility = View.GONE
        } else {
            relativeLayoutDeleteButton!!.visibility = View.VISIBLE
        }
        adapter!!.notifyItemChanged(position)
    }

    override fun onClickDownload(url: String, msgId: String, progressBar: ProgressBar, fileType: String) {
        when (fileType) {
            "pdf" -> {
                if (takePermission()) {
                    val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Pdf/")
                    if (!dirMain.exists()) {
                        dirMain.mkdirs()
                    }
                    val PB = ProgressBack(this, dirMain, chatDb, msgId, progressBar, "pdf")
                    PB.execute(url)
                }
            }
            "image" -> {
                if (takePermission()) {
                    //  progressBar.visibility=View.GONE
                    val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Image/")
                    if (!dirMain.exists()) {
                        dirMain.mkdirs()
                    }
                    DownloadImage(this, dirMain, chatDb, msgId, progressBar).execute(url)
                }
            }
            "video" -> {
                if (takePermission()) {
                    val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Video/")
                    if (!dirMain.exists()) {
                        dirMain.mkdirs()
                    }
                    val PB = ProgressBack(this, dirMain, chatDb, msgId, progressBar, "video")
                    PB.execute(url)
                }
            }
        }
    }

    override fun onResent(msgId: String) {
        val resentList: List<ChatListEntity> = chatDb!!.chatListModel().getChatWithMsgId(msgId)
        if (resentList.isNotEmpty()) {
            val today = Date()
            //  val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
            // val dateToStr = format.format(today)
            val enteredDate = enteredDateFormat.format(today)
            val chatListEntity = ChatListEntity()
            var localPath = ""
            var msgType = ""
            var fileType = ""
            for (i in resentList.indices) {
                chatListEntity.withType = resentList[i].withType
                chatListEntity.withId = resentList[i].withId
                chatListEntity.withName = resentList[i].withName
                chatListEntity.withProfileUrl = resentList[i].withProfileUrl
                chatListEntity.byMe = resentList[i].byMe
                chatListEntity.msg = resentList[i].msg
                chatListEntity.enteredDate = enteredDate
                chatListEntity.msgType = resentList[i].msgType
                chatListEntity.imagepath = resentList[i].imagepath
                chatListEntity.imageOriginalPath = resentList[i].imagepath
                chatListEntity.sent = resentList[i].sent
                chatListEntity.msgId = resentList[i].msgId
                chatListEntity.localPath = resentList[i].localPath
                localPath = resentList[i].localPath
                msgType = resentList[i].msgType
                chatListEntity.fileSize = resentList[i].fileSize
            }

            fileType = when (msgType) {
                "image" -> {
                    "jpeg"
                }
                "video" -> {
                    "mp4"
                }
                else -> {
                    "pdf"
                }
            }
            val encoded = getBase64FromPath(localPath)
            uploadMediaToServer(fileType, msgType, encoded, chatListEntity)
        }

    }

    private fun getBase64FromPath(path: String): String {
        var base64: String = ""
        try {
            val file = File(path)
            val buffer = ByteArray(file.length().toInt() + 100)
            val length = FileInputStream(file).read(buffer)
            base64 = encodeToString(buffer, 0, length,
                    DEFAULT)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return base64
    }

    override fun onAttachmentClickListener(menuName: String) {
        //Toast.makeText(applicationContext, "Menu ${menuName} clicked", Toast.LENGTH_SHORT).show()
        if (Gc.getSharedPreference(Gc.MEDIASTORAGEURL, this).isNullOrEmpty() ||
                Gc.getSharedPreference(Gc.MEDIASTORAGEURL, this) == "NotFound") {
            val alertLocationInfo = androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
            alertLocationInfo.setTitle("Media Alert")
            alertLocationInfo.setIcon(R.drawable.ic_alert)
            alertLocationInfo.setMessage(R.string.sending_media_is_disabled_for_your_organization_Please_contact)
            alertLocationInfo.setNegativeButton(getString(R.string.ok)) { _: DialogInterface?, _: Int -> }
            alertLocationInfo.setCancelable(false)
            alertLocationInfo.show()
        } else {
            if (menuName == "Image") {
                CropImage.activity()
                        //.setAspectRatio(1, 1)
                        // .setMaxCropResultSize(1920, 1080)
                        .start(this)
            } else if (menuName == "Video") {
                val intent = Intent()
                intent.type = "video/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Video"), 3939)
            } else if (menuName == "PDF") {
                val intent = Intent()
                intent.type = "application/pdf"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select PDF"), 3940)
            }
        }
    }

    private fun encodeImage(bm: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.WEBP, 0, byteArrayOutputStream)
        return encodeToString(byteArrayOutputStream.toByteArray(), DEFAULT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val result = CropImage.getActivityResult(data)
            //  Log.e("test",result.uri.path)
            val imageUri = result.uri
            val imageStream = contentResolver.openInputStream(imageUri!!)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            val encodedImage = encodeImage(selectedImage)
            if (takePermission()) {
                createImage(selectedImage, encodedImage)
            }

        } else if (requestCode == 3939 && resultCode == Activity.RESULT_OK) {
            val URI = data!!.data;
            val encodedVideo = this.convertToString(URI)
            createVideo(encodedVideo, URI)
        } else if (requestCode == 3940 && resultCode == Activity.RESULT_OK) {
            val uRI = data!!.data;
            val pdf = this.convertToString(uRI)
            createPdf(pdf, uRI)
        }
    }

    private fun createPdf(pdf: String, uri: Uri?) {
        val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Pdf/Sent")
        if (!dirMain.exists()) {
            dirMain.mkdirs()
            val nomedia = File(dirMain.toString() + "/.nomedia")
            try {
                nomedia.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        val videoName = "pdf-" + System.currentTimeMillis().toString() + ".pdf";
        val pdfPath = "$dirMain/$videoName"
        val input: InputStream = contentResolver.openInputStream(uri!!)!!
        val f = FileOutputStream(File(dirMain, videoName))
        val buffer = ByteArray(1024)
        var len1 = 0
        while (input.read(buffer).also { len1 = it } > 0) {
            f.write(buffer, 0, len1)
        }
        f.close()
        val files = File(pdfPath)
        if (files.exists()) {
            val fileSize: Int = java.lang.String.valueOf(files.length() / 1024).toInt()
            val fileSizes = size(fileSize)

            val today = Date()
            val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
            val enteredDate = enteredDateFormat.format(today)
            val chatListEntity = ChatListEntity()

            if (hasPermission) {
                chatListEntity.withType = withType
                chatListEntity.withId = with
                when (withType) {
                    "student" -> if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                            || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                        chatListEntity.withName = selectedStudent!!.StudentName
                        chatListEntity.withProfileUrl = selectedStudent!!.studentProfileImage
                        hasPermission = true
                    } else {
                        hasPermission = false
                    }
                    "staff" -> if (chatPermissions.contains(selectedStaff!!.UserTypeValue)
                            || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                        chatListEntity.withName = selectedStaff!!.StaffName
                        chatListEntity.withProfileUrl = selectedStaff!!.staffProfileImage
                        hasPermission = true
                    } else {
                        hasPermission = false
                    }
                    "admin" -> {
                        chatListEntity.withName = "admin"
                        chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this)
                        hasPermission = if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                            Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equals(Gc.DEMO, ignoreCase = true)
                        } else {
                            true
                        }
                    }
                    "section" -> if (chatPermissions.contains("STUDENT")
                            || chatPermissions.contains("PARENTS")
                            || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                        if (selectedSection == null) {
                            Toast.makeText(this, resources.getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show()
                            hasPermission = false
                        } else {
                            chatListEntity.withName = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                            chatListEntity.withProfileUrl = ""
                            hasPermission = true
                        }
                    } else {
                        hasPermission = false
                    }
                }
                chatListEntity.byMe = 1
                chatListEntity.msg = etMessageChat.text.toString()
                chatListEntity.enteredDate = enteredDate
                chatListEntity.msgType = "pdf"
                chatListEntity.imagepath = pdfPath
                chatListEntity.imageOriginalPath = pdfPath
                chatListEntity.sent = 0
                chatListEntity.msgId = (Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERID, this)
                        + "-"
                        + Date().time)

                chatListEntity.localPath = pdfPath
                chatListEntity.fileSize = fileSizes
                Thread { chatDb!!.chatListModel().insertChat(chatListEntity) }.start()
                notifyAdapterAfterAllOperation()
                etMessageChat.text.clear()
                uploadMediaToServer("pdf", "pdf", pdf, chatListEntity)
            } else {
                Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }
    }

    private fun uploadMediaToServer(fileType: String, type: String, fileData: String, chatListEntity: ChatListEntity) {
        val param = JSONObject()
        param.put("fileType", fileType)
        param.put("type", type)
        param.put("fileData", fileData)
        val url = (Gc
                .getSharedPreference(Gc.MEDIASTORAGEURL, this)
                + "/medias/media")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job: JSONObject ->
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                    try {
                        chatListEntity.imagepath = job.optString("result")
                        chatListEntity.imageOriginalPath = job.optString("result")
                        sendChatTextToServerSingle(chatListEntity)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    /*
                     val today = Date()
                     //  val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
                     val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
                     // val dateToStr = format.format(today)
                     val enteredDate = enteredDateFormat.format(today)
                     val chatListEntity = ChatListEntity()
                     chatListEntity.withType = withType
                     chatListEntity.withId = with
                     when (withType) {
                         "student" -> if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                                 || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                             chatListEntity.withName = selectedStudent!!.StudentName
                             chatListEntity.withProfileUrl = selectedStudent!!.studentProfileImage
                             hasPermission = true
                         } else {
                             hasPermission = false
                         }
                         "staff" -> if (chatPermissions.contains(selectedStaff!!.UserTypeValue)
                                 || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                             chatListEntity.withName = selectedStaff!!.StaffName
                             chatListEntity.withProfileUrl = selectedStaff!!.staffProfileImage
                             hasPermission = true
                         } else {
                             hasPermission = false
                         }
                         "admin" -> {
                             chatListEntity.withName = "admin"
                             chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this)
                             hasPermission = if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                                 Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equals(Gc.DEMO, ignoreCase = true)
                             } else {
                                 true
                             }
                         }
                         "section" -> if (chatPermissions.contains("STUDENT")
                                 || chatPermissions.contains("PARENTS")
                                 || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                             if (selectedSection == null) {
                                 Toast.makeText(this, resources.getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show()
                                 hasPermission = false
                             } else {
                                 chatListEntity.withName = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                                 chatListEntity.withProfileUrl = ""
                                 hasPermission = true
                             }
                         } else {
                             hasPermission = false
                         }
                     }
                     if (hasPermission) {
                         chatListEntity.byMe = 1
                         chatListEntity.msg = etMessageChat!!.text.toString()
                         chatListEntity.enteredDate = enteredDate
                         chatListEntity.msgType = "video"
                         chatListEntity.imagepath = job.optString("result")
                         chatListEntity.imageOriginalPath = job.optString("result")
                         chatListEntity.sent = 0
                         chatListEntity.msgId = (Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                                 + "-"
                                 + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                                 + "-"
                                 + Gc.getSharedPreference(Gc.USERID, this)
                                 + "-"
                                 + Date().time)
                         Thread(Runnable { chatDb!!.chatListModel().insertChat(chatListEntity) }).start()
                         notifyAdapterAfterAllOperation()
                         try {
                             sendChatTextToServerSingle(chatListEntity)
                         } catch (e: JSONException) {
                             e.printStackTrace()
                         }
                         etMessageChat!!.text.clear()
                     } else {
                         Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                                 findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
                     }*/
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun createVideo(encodedVideo: String, uri: Uri?) {
        val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Video/Sent")
        if (!dirMain.exists()) {
            dirMain.mkdirs()
            val nomedia = File(dirMain.toString() + "/.nomedia")
            try {
                nomedia.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        val videoName = "VID-" + System.currentTimeMillis().toString() + ".mp4";
        val videoPath = "$dirMain/$videoName"
        val input: InputStream = contentResolver.openInputStream(uri!!)!!
        val f = FileOutputStream(File(dirMain, videoName))
        val buffer = ByteArray(1024)
        var len1 = 0
        while (input.read(buffer).also { len1 = it } > 0) {
            f.write(buffer, 0, len1)
        }
        f.close()
        val files = File(videoPath)
        if (files.exists()) {
            val file_size: Int = java.lang.String.valueOf(files.length() / 1024).toInt()
            val file_sizes = size(file_size)

            val today = Date()
            //  val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
            // val dateToStr = format.format(today)
            val enteredDate = enteredDateFormat.format(today)
            val chatListEntity = ChatListEntity()
            chatListEntity.withType = withType
            chatListEntity.withId = with
            when (withType) {
                "student" -> if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    chatListEntity.withName = selectedStudent!!.StudentName
                    chatListEntity.withProfileUrl = selectedStudent!!.studentProfileImage
                    hasPermission = true
                } else {
                    hasPermission = false
                }
                "staff" -> if (chatPermissions.contains(selectedStaff!!.UserTypeValue)
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    chatListEntity.withName = selectedStaff!!.StaffName
                    chatListEntity.withProfileUrl = selectedStaff!!.staffProfileImage
                    hasPermission = true
                } else {
                    hasPermission = false
                }
                "admin" -> {
                    chatListEntity.withName = "admin"
                    chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this)
                    hasPermission = if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                        Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equals(Gc.DEMO, ignoreCase = true)
                    } else {
                        true
                    }
                }
                "section" -> if (chatPermissions.contains("STUDENT")
                        || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    if (selectedSection == null) {
                        Toast.makeText(this, resources.getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show()
                        hasPermission = false
                    } else {
                        chatListEntity.withName = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                        chatListEntity.withProfileUrl = ""
                        hasPermission = true
                    }
                } else {
                    hasPermission = false
                }
            }
            if (hasPermission) {
                chatListEntity.byMe = 1
                chatListEntity.msg = etMessageChat.text.toString()
                chatListEntity.enteredDate = enteredDate
                chatListEntity.msgType = "video"
                chatListEntity.imagepath = videoPath
                chatListEntity.imageOriginalPath = videoPath
                chatListEntity.sent = 0
                chatListEntity.msgId = (Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERID, this)
                        + "-"
                        + Date().time)

                chatListEntity.localPath = videoPath
                chatListEntity.fileSize = file_sizes
                Thread({ chatDb!!.chatListModel().insertChat(chatListEntity) }).start()
                notifyAdapterAfterAllOperation()
                etMessageChat.text.clear()

                uploadMediaToServer("mp4", "video", encodedVideo, chatListEntity)


            } else {
                Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }
    }

    private fun createImage(selectedImage: Bitmap?, encodedImage: String) {
        val dirMain = File(Environment.getExternalStorageDirectory().toString() + "/MySchool/Media/MySchool Image/Sent")
        if (!dirMain.exists()) {
            dirMain.mkdirs()
            val nomedia = File(dirMain.toString() + "/.nomedia")
            try {
                nomedia.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        val file = File(dirMain, "IMG-" + System.currentTimeMillis().toString() + ".jpg")
        val fos = FileOutputStream(file)
        selectedImage!!.compress(Bitmap.CompressFormat.WEBP, 0, fos)
        fos.flush()
        fos.close()
        val newImagePath = file.path
        MediaScannerConnection.scanFile(this, arrayOf(file.path), arrayOf("image/jpeg"), null)
        val files = File(newImagePath)
        if (files.exists()) {
            val file_size: Int = java.lang.String.valueOf(files.length() / 1024).toInt()
            val file_sizes = size(file_size)
            val today = Date()
            val enteredDateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm aaa", Locale.US)
            val enteredDate = enteredDateFormat.format(today)
            val chatListEntity = ChatListEntity()
            chatListEntity.withType = withType
            chatListEntity.withId = with
            when (withType) {
                "student" -> if (chatPermissions.contains("STUDENT") || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    chatListEntity.withName = selectedStudent!!.StudentName
                    chatListEntity.withProfileUrl = selectedStudent!!.studentProfileImage
                    hasPermission = true
                } else {
                    hasPermission = false
                }
                "staff" -> if (chatPermissions.contains(selectedStaff!!.UserTypeValue)
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    chatListEntity.withName = selectedStaff!!.StaffName
                    chatListEntity.withProfileUrl = selectedStaff!!.staffProfileImage
                    hasPermission = true
                } else {
                    hasPermission = false
                }
                "admin" -> {
                    chatListEntity.withName = "admin"
                    chatListEntity.withProfileUrl = Gc.getSharedPreference(Gc.SCHOOLIMAGE, this)
                    hasPermission = if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.STUDENTPARENT, ignoreCase = true)) {
                        Gc.getSharedPreference(Gc.ERPPLANTYPE, this).equals(Gc.DEMO, ignoreCase = true)
                    } else {
                        true
                    }
                }
                "section" -> if (chatPermissions.contains("STUDENT")
                        || chatPermissions.contains("PARENTS")
                        || Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equals(Gc.ADMIN, ignoreCase = true)) {
                    if (selectedSection == null) {
                        Toast.makeText(this, resources.getString(R.string.this_section_does_not_belong_to_current_session), Toast.LENGTH_LONG).show()
                        hasPermission = false
                    } else {
                        chatListEntity.withName = selectedSection!!.ClassName + " " + selectedSection!!.SectionName
                        chatListEntity.withProfileUrl = ""
                        hasPermission = true
                    }
                } else {
                    hasPermission = false
                }
            }
            if (hasPermission) {
                var msg = "image"
                if (etMessageChat.text.toString().isNotEmpty()) {
                    msg = etMessageChat.text.toString()
                }
                chatListEntity.byMe = 1
                chatListEntity.msg = etMessageChat.text.toString()
                chatListEntity.enteredDate = enteredDate
                chatListEntity.msgType = "image"
                chatListEntity.imagepath = newImagePath
                chatListEntity.imageOriginalPath = newImagePath
                chatListEntity.sent = 0
                chatListEntity.msgId = (Gc.getSharedPreference(Gc.ERPINSTCODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERTYPECODE, this)
                        + "-"
                        + Gc.getSharedPreference(Gc.USERID, this)
                        + "-"
                        + Date().time)

                chatListEntity.localPath = newImagePath
                chatListEntity.fileSize = file_sizes
                Thread(Runnable { chatDb!!.chatListModel().insertChat(chatListEntity) }).start()
                notifyAdapterAfterAllOperation()
                etMessageChat.text.clear()
                //uploadImageToServer(encodedImage, chatListEntity)
                uploadMediaToServer("jpeg", "image", encodedImage, chatListEntity)
            } else {
                Config.responseSnackBarHandler(getString(R.string.you_dont_have_permission_to_send_messages_to) + chatListEntity.withType,
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }
    }

    fun size(size: Int): String {
        var hrSize = ""
        val m = size / 1024.0
        val dec = DecimalFormat("0.00")
        hrSize = if (m > 1) {
            dec.format(m) + " MB"
        } else {
            dec.format(size) + " KB"
        }
        return hrSize
    }

    private fun convertToString(uri: Uri?): String {
        return try {
            val input: InputStream = contentResolver.openInputStream(uri!!)!!
            val bytes = getBytes(input)
            encodeToString(bytes, DEFAULT)
        } catch (e: Exception) {
            e.printStackTrace()
            "null"
        }
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray? {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    private inner class DownloadImage(private val context: Context, private val dirMain: File,
                                      private val chatDb: ChatDatabase?, private val msgId: String,
                                      private val progressBar: ProgressBar) : AsyncTask<String?, String?, String?>() {
        private var ImageUrl: URL? = null
        private var bmImg: Bitmap? = null
        private var newImagePath: String? = null
        private var idStr: String = ""

        override fun doInBackground(vararg args: String?): String? {
            var `is`: InputStream? = null
            try {
                ImageUrl = URL(args[0])
                val conn = ImageUrl!!
                        .openConnection() as HttpURLConnection
                conn.doInput = true
                conn.connect()
                `is` = conn.inputStream
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.RGB_565
                bmImg = BitmapFactory.decodeStream(`is`, null, options)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            try {
                val path = ImageUrl!!.path
                idStr = path.substring(path.lastIndexOf('/') + 1)
                val file = File(dirMain, idStr)
                val fos = FileOutputStream(file)
                bmImg!!.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.flush()
                fos.close()
                newImagePath = file.path
                MediaScannerConnection.scanFile(context, arrayOf(file.path), arrayOf("image/jpeg"), null)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                if (`is` != null) {
                    try {
                        `is`.close()
                    } catch (e: Exception) {
                    }
                }
            }
            return null
        }

        override fun onPostExecute(args: String?) {
            if (bmImg == null) {
                Toast.makeText(context, "Image still loading...",
                        Toast.LENGTH_SHORT).show()
            } else {
                progressBar.visibility = View.GONE
                Toast.makeText(context, "Image Successfully Saved",
                        Toast.LENGTH_SHORT).show()
                Thread { chatDb!!.chatListModel().updateLocalPath(msgId, newImagePath) }.start()
                notifyAdapterAfterAllOperation()
                if (withType == "student") {
                    deleteImageFromServer(idStr, "image")
                }
            }
        }
    }

    private fun deleteImageFromServer(fileName: String, fileType: String) {
        val param = JSONObject()
        param.put("fileName", fileName)
        param.put("fileType", fileType)
        val url = (Gc
                .getSharedPreference(Gc.MEDIASTORAGEURL, this)
                + "/medias/media")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.DELETE, "$url/$param", JSONObject(), Response.Listener { job: JSONObject ->
            when (job.optString("code")) {
                Gc.APIRESPONSE200 -> {
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_chat_activity), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2F)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private inner class ProgressBack(private val context: Context, private val dirMain: File,
                                     private val chatDb: ChatDatabase?, private val msgId: String,
                                     private val progressBar: ProgressBar, private val fileType: String) : AsyncTask<String?, String?, String?>() {
        private var newImagePaths: String? = null
        private var idStrs: String = ""

        override fun doInBackground(vararg params: String?): String? {
            val url = URL(params[0])
            val path = url.path
            idStrs = path.substring(path.lastIndexOf('/') + 1)
            try {
                val c = url.openConnection() as HttpURLConnection
                c.requestMethod = "GET"
                c.doOutput = true
                c.connect()
                val f = FileOutputStream(File(dirMain,
                        idStrs))
                val `in` = c.inputStream
                val buffer = ByteArray(1024)
                var len1 = 0
                while (`in`.read(buffer).also { len1 = it } > 0) {
                    f.write(buffer, 0, len1)
                }
                f.close()
                newImagePaths = "$dirMain/$idStrs"
                Thread { chatDb!!.chatListModel().updateLocalPath(msgId, newImagePaths) }.start()
            } catch (e: IOException) {
                Log.d("Error....", e.toString())
            }
            return null
        }

        override fun onPostExecute(args: String?) {
            progressBar.visibility = View.GONE
            Toast.makeText(context, "Image Successfully Saved",
                    Toast.LENGTH_SHORT).show()

            notifyAdapterAfterAllOperation()
            if (withType == "student") {
                deleteImageFromServer(idStrs, fileType)
            }
        }
    }

    private fun takePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), Config.IMAGE_LOGO_CODE)
            }
            false
        } else true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //denied
                Log.e("denied", permissions[0])
            } else {
                if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                } else {
                    //do something here.
                    showRationale(getString(R.string.permission_denied), getString(R.string.permission_denied_external_storage))
                }
            }
        }
    }

    private fun showRationale(permission: String, permission_denied_location: String) {
        val alertLocationInfo = androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
        alertLocationInfo.setTitle(permission)
        alertLocationInfo.setIcon(R.drawable.ic_alert)
        alertLocationInfo.setMessage("""
    $permission_denied_location
    ${getString(R.string.setting_permission_path_on)}
    """.trimIndent())
        alertLocationInfo.setPositiveButton(R.string.retry) { _, _ ->
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivityForResult(intent, Config.LOCATION)
        }
        alertLocationInfo.setNegativeButton(getString(R.string.deny)) { _: DialogInterface?, _: Int -> }
        alertLocationInfo.setCancelable(false)
        alertLocationInfo.show()
    }

}

