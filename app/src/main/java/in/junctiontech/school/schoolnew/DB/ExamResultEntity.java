package in.junctiontech.school.schoolnew.DB;

public class ExamResultEntity {
    public String AdmissionId;
    public String AdmissionNo;
    public String StudentName;
    public String DateOfExam;
    public String ExamTypeID;
    public String Exam_Type;
    public String Subject_Id;
    public String SubjectName;
    public String Marks_Obtain;
    public String Max_Marks;
    public String Grade;
    public String Result;
    public String Section_Id;
}
