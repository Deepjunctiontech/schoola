package in.junctiontech.school.schoolnew.messaging;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.junctiontech.school.admin.msgpermission.ChatPermission;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.FCM.MyFCMPushReceiver;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.ChatList;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.messaging.Fragments.ChatListFragment;
import in.junctiontech.school.schoolnew.messaging.Fragments.ClassListForMessagingFragment;
import in.junctiontech.school.schoolnew.messaging.Fragments.ContactListFragment;
import in.junctiontech.school.schoolnew.messenger.MessageListActivity;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.CommunicationPermissions;

public class ConversationListActivity extends AppCompatActivity {

    MainDatabase mDb;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ChatListFragment chatListFragment;
    private ContactListFragment contactListFragment;
    private ClassListForMessagingFragment classListFragment;
    private boolean selectedTab;
    private DbHandler db;


    private Config configInstance;
    private SharedPreferences sp;
    public ArrayList<ChatPermission> chatPermissionList = new ArrayList<>();
    private AlertDialog.Builder alertMsg;
    private boolean isAdmin = false;

    public ArrayList<String> messengerPermissions = new ArrayList<>();

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        tabLayout.setBackgroundColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_student2);
        db = DbHandler.getInstance(this);
        sp = Prefs.with(this).getSharedPreferences();

        mDb = MainDatabase.getDatabase(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        configInstance = (Config) getApplication();


            viewPager =  findViewById(R.id.container);
            setupViewPager(viewPager);
            chatListFragment.setMenuVisibility(true);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            chatListFragment.setMenuVisibility(true);
                            contactListFragment.setCustumMenuVisibility(false);
                            getSupportActionBar().setTitle(getString(R.string.chats));
                            break;

                        case 1:
                            chatListFragment.setMenuVisibility(false);
                            contactListFragment.setFirstMenuVisibility(true);
                            contactListFragment.setCustumMenuVisibility(true);
                            getSupportActionBar().setTitle(getString(R.string.contacts) + " " +
                                    getString(R.string.list));
                            break;
                        case 2:
                            chatListFragment.setMenuVisibility(false);
                            contactListFragment.setFirstMenuVisibility(false);
                            contactListFragment.setCustumMenuVisibility(false);
                            getSupportActionBar().setTitle(getString(R.string.class_text) + " " +
                                    getString(R.string.list));
                            break;

                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            tabLayout =  findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            contactListFragment.setFirstMenuVisibility(false);


            if (!(Gc.getSharedPreference(Gc.USERTYPETEXT,this).equalsIgnoreCase("Administrator") || sp.getString("user_type", "").equalsIgnoreCase("Admin"))) {
                if (!sp.getString("MessagePermissionResult", "").equalsIgnoreCase("")) {
                    ChatPermission chatPermissionData = (new Gson()).fromJson(sp.getString("MessagePermissionResult", ""), new TypeToken<ChatPermission>() {
                    }.getType());
                    chatPermissionList = chatPermissionData.getResult();
                }
            } else isAdmin = true;

            alertMsg = new AlertDialog.Builder(this);
            alertMsg.setPositiveButton(R.string.ok, null);
            alertMsg.setTitle(getString(R.string.information));
            alertMsg.setIcon(getResources().getDrawable(R.drawable.ic_alert));

        setColorApp();

        mDb.communicationPermissionsModel()
                .getCommunicationPermissionsForUserType(Gc.getSharedPreference(Gc.USERTYPECODE,this))
                .observe(this, new Observer<List<CommunicationPermissions>>() {
                    @Override
                    public void onChanged(@Nullable List<CommunicationPermissions> communicationPermissions) {
                        HashSet<String> permissions = new HashSet<>();
                        for(CommunicationPermissions c: communicationPermissions){
                            permissions.add(c.ToUserType);
                        }
                        HashMap<String, Set<String>> setDefaults = new HashMap<>();
                        setDefaults.put("chatpermissions",permissions);
                        Gc.setSharedPreferenceSet(setDefaults,ConversationListActivity.this);

                    }
                });

    }

    private void setupViewPager(ViewPager viewPager) {
        ConversationListActivity.ViewPagerAdapter adapter = new ConversationListActivity.
                ViewPagerAdapter(getSupportFragmentManager());

        chatListFragment = new ChatListFragment();
        contactListFragment = new ContactListFragment();
        classListFragment = new ClassListForMessagingFragment();

        adapter.addFragment(chatListFragment, getString(R.string.chats));
        adapter.addFragment(contactListFragment, getString(R.string.contacts));
        adapter.addFragment(classListFragment, getString(R.string.class_text));
        viewPager.setAdapter(adapter);

    }

    public boolean getSelectedTab() {
        if (viewPager.getCurrentItem() == 0)
            return false;
        else return true;
    }

    public void startChat(StudentData dataObject, String userType, boolean refresh) {
        Log.e("startChat","startChat");
  /*      boolean isPermission = false;
        String userTypeReceiver = "";
         if (userType.equalsIgnoreCase("Teacher")){
             userType = dataObject.getStudentName().split("\\(")[1].split("\\)")[0];
         }
        if (isAdmin || !userType.equalsIgnoreCase("Teacher")) isPermission = true;
        else {
            userTypeReceiver = dataObject.getStudentName().split("\\(")[1].split("\\)")[0];
            Log.e("userTypeReceiver", userTypeReceiver);

            for (ChatPermission chatObj : chatPermissionList) {

                if (chatObj.getToUserType().equalsIgnoreCase(userTypeReceiver))
                    isPermission = true;

            }
        }
        if (isPermission) {
*/
  if (userType.equalsIgnoreCase("Teacher")) {
      userType =  dataObject.getUserType();
  }
            Intent intent = new Intent(ConversationListActivity.this, ChatActivity.class);
            int groupID = db.createChat(dataObject.getStudentID(), dataObject.getStudentName(), userType);
            intent.putExtra("groupID", groupID);
            intent.putExtra("refresh", refresh); // use for chat list will refresh on back pressed
            intent.putExtra("userType", userType);
            intent.putExtra("data", dataObject);
            startActivityForResult(intent, 101);
       /* } else {
            alertMsg.setMessage("You have not Permission to send message :" + userTypeReceiver);
            alertMsg.show();
        }*/
    }

    public void startChatClassWise(ClassNameSectionName sectionIdObj) {
        Intent intent = new Intent(ConversationListActivity.this, MessageListActivity.class);

//        int groupID = db.createChat(sectionIdObj.getSectionId(), sectionIdObj.getSectionName(), "Class");
        intent.putExtra("with", sectionIdObj.SectionId);

        intent.putExtra("withType", "Section");
        startActivityForResult(intent, 201);
    }

    public void continuePreviousChat(ChatList chatList) {

        //  if (chatList.getUnReadChatCounter()>0) {
         /* notification tray update and remove this item from notification tray */
        //  Config.noOfMsgs = Config.noOfMsgs - chatList.getUnReadChatCounter();
        //  Config.groupIds.remove(chatList.getGroupId());
        // }
        configInstance.decreaseNumberOfMessageCounter(chatList.getUnReadChatCounter());
        configInstance.removeGroupIdChatList(chatList.getGroupId());

        Intent intent = new Intent(ConversationListActivity.this, ChatActivity.class);
        intent.putExtra("groupID", chatList.getGroupId());
        intent.putExtra("refresh", true);
        intent.putExtra("userType", chatList.getGroupMembersType());
        intent.putExtra("data", new StudentData(chatList.getGroupName(),
                chatList.getGroupMembers()));
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 101) {

//            chatListFragment.updateChatList((ChatList) data.getSerializableExtra("data"));

        }
    }

    public void setTabContacts() {
        viewPager.setCurrentItem(1);
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);


        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);

        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ConversationListAct", "onResume");
        // registering the receiver for newtext notification
        // LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
        //        new IntentFilter(Config.chatMessage));

        // clearing the notification tray
        MyFCMPushReceiver.clearNotifications(this);
    }

    @Override
    protected void onPause() {
        Log.e("ConversationListAct", "onPause");
        //  LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

}
