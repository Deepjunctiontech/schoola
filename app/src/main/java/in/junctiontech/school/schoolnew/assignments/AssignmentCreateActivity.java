package in.junctiontech.school.schoolnew.assignments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.AssignmentEntity;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.FullScreenImageActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.subject.SubjectActivity;

public class AssignmentCreateActivity extends AppCompatActivity {

    int SELECT_PICTURE = 101;

    MainDatabase mDb;

    ProgressBar progressBar;

    Button btn_class_section,
            btn_subject,
            btn_assignment_date,
            btn_submission_date,
            btn_select_image,
            btn_save_next;


    EditText et_assignment_text;

    ImageView iv_assignment;

    Button btn_pdf;

    AlertDialog.Builder classListBuilder;
    ArrayAdapter<String> classSectionAdapter;
    ArrayList<String> sectionList = new ArrayList<>();
    ArrayList<ClassNameSectionName> classNameSectionList = new ArrayList<>();
    int selectedSection, selectedSubject;
    String selectedSectionID;


    AlertDialog.Builder subjectListBuilder;
    ArrayAdapter<String> subjectAdapter;
    ArrayList<String> subjectList = new ArrayList<>();
    private ArrayList<SchoolSubjectsEntity> schoolSubjects = new ArrayList<>();

    String assignment, selectedAssignmentDate, selectedSubmissionDate;
    AssignmentEntity assignmentEntity;

    private int colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_create);

        mDb = MainDatabase.getDatabase(this);

        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btn_class_section = findViewById(R.id.btn_class_section);
        btn_class_section.setEnabled(false);

        btn_save_next = findViewById(R.id.btn_save_next);

        btn_save_next.setOnClickListener(view -> {
            if (validateInput()) {
                try {
                    saveHomework();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_select_image = findViewById(R.id.btn_select_image);

        btn_subject = findViewById(R.id.btn_subject);
        btn_assignment_date = findViewById(R.id.btn_assignment_date);
        btn_assignment_date.setEnabled(false);
        btn_submission_date = findViewById(R.id.btn_submission_date);
        et_assignment_text = findViewById(R.id.et_assignment_text);
        iv_assignment = findViewById(R.id.iv_assignment);
        btn_pdf = findViewById(R.id.btn_pdf);

       /* btn_submithomework = findViewById(R.id.btn_submithomework);

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)) {
            btn_submithomework.setVisibility(View.VISIBLE);
            btn_submithomework.setOnClickListener(v -> {
                submitAssignment(assignment);
            });
        } else {
            btn_submithomework.setVisibility(View.GONE);

        }*/

        btn_pdf.setOnClickListener(v -> {
            iv_assignment.setVisibility(View.GONE);

            Intent intent = new Intent(Intent.ACTION_VIEW);

            intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + assignmentEntity.Image), "text/html");

            startActivity(intent);

        });

        iv_assignment.setOnClickListener(v -> {
            if (assignmentEntity == null) {
                Config.responseSnackBarHandler(getString(R.string.save_before_open),
                        findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
                return;
            }
            if (!(Strings.nullToEmpty(assignmentEntity.Image).equalsIgnoreCase(""))) {
                Intent intent = new Intent(AssignmentCreateActivity.this, FullScreenImageActivity.class);

                ArrayList<String> images = new ArrayList<>();
                images.add(assignmentEntity.Image);
                intent.putExtra("images", images);
                intent.putExtra("title", getString(R.string.assignment));
                startActivity(intent);
            }
        });

        if (Strings.nullToEmpty(getIntent().getStringExtra("createAssignment"))
                .equalsIgnoreCase(Gc.TRUE)) {
            btn_class_section.setText(getIntent().getStringExtra("selectedSectionname"));
            btn_assignment_date.setText(getIntent().getStringExtra("assignmentdate"));
            selectedSectionID = getIntent().getStringExtra("selectedSectionID");

            mDb.schoolSubjectsClassModel()
                    .getSubjectsForSection(selectedSectionID)
                    .observe(this, new Observer<List<SchoolSubjectsEntity>>() {
                        @Override
                        public void onChanged(@Nullable List<SchoolSubjectsEntity> schoolSubjectsEntities) {
                            if (schoolSubjectsEntities.size() < 1) {

                                AlertDialog.Builder alert1 = new AlertDialog.Builder(AssignmentCreateActivity.this);
                                alert1.setTitle(getString(R.string.subject_for_classes) + getString(R.string.not_available));
                                alert1.setIcon(getResources().getDrawable(R.drawable.ic_clickhere));
                                alert1.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(AssignmentCreateActivity.this, SubjectActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alert1.setMessage(getString(R.string.assign_subject_to_class));
                                //  alert.setCancelable(false);
                                alert1.show();

                                return;
                            }

                            schoolSubjects.clear();
                            subjectList.clear();
                            schoolSubjects.addAll(schoolSubjectsEntities);
                            for (SchoolSubjectsEntity s : schoolSubjects) {
                                subjectList.add(s.SubjectName);
                            }
                            subjectAdapter.clear();
                            subjectAdapter.addAll(subjectList);
                            subjectAdapter.notifyDataSetChanged();
                            mDb.schoolSubjectsClassModel()
                                    .getSubjectsForSection(selectedSectionID)
                                    .removeObserver(this);
                        }
                    });
        }

        assignment = getIntent().getStringExtra("assignment");

        if (!(assignment == null)) {
            assignmentEntity = new Gson().fromJson(assignment, AssignmentEntity.class);
            String role = Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this);

            if (role.equalsIgnoreCase(Gc.STUDENTPARENT)) {
                Intent intent = new Intent(AssignmentCreateActivity.this, AssignmentUpdateActivity.class);
                intent.putExtra("homeworkId", assignmentEntity.homeworkId);
                startActivity(intent);
                finish();
            }

            btn_class_section.setText(assignmentEntity.ClassName + "-" + assignmentEntity.SectionName);
            btn_class_section.setEnabled(false);

            btn_subject.setText(assignmentEntity.SubjectName);
            btn_subject.setEnabled(false);

            btn_assignment_date.setText(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(assignmentEntity.dateofhomework));
            btn_assignment_date.setEnabled(false);

            btn_submission_date.setText(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(assignmentEntity.dosubmission));
            et_assignment_text.setText(Html.fromHtml(assignmentEntity.homework));
        }

        selectSubjectForassignment();
        selectAssignmentDate();
        selectSubmissionDate();
        selectImageFromGallery();
        if (!(Gc.getSharedPreference(Gc.HOMEWORKCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE))) {
            btn_class_section.setEnabled(false);
            btn_subject.setEnabled(false);
            btn_assignment_date.setEnabled(false);

            btn_select_image.setEnabled(false);
            btn_select_image.setVisibility(View.GONE);
            btn_select_image.setClickable(false);

            btn_submission_date.setEnabled(false);
            et_assignment_text.setFocusable(false);
        }

        setColorApp();
        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/8688186042", this, adContainer);
        }
    }

    void selectAssignmentDate() {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);

                btn_assignment_date.setText(sdf.format(myCalendar.getTime()));

                SimpleDateFormat sdfeng = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                selectedAssignmentDate = sdfeng.format(myCalendar.getTime());

//                try {
//                    fetchAttendenceAndDisplay(selectedSection);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }

        };

        btn_assignment_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AssignmentCreateActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
    }

    void selectSubmissionDate() {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);

                btn_submission_date.setText(sdf.format(myCalendar.getTime()));

                SimpleDateFormat sdfeng = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                selectedSubmissionDate = sdfeng.format(myCalendar.getTime());

//                try {
//                    fetchAttendenceAndDisplay(selectedSection);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }

        };

        btn_submission_date.setOnClickListener(view -> new DatePickerDialog(AssignmentCreateActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());
    }

    void selectImageFromGallery() {
        btn_select_image.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        });
    }

    void selectSubjectForassignment() {
        subjectListBuilder = new AlertDialog.Builder(this);
        subjectAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        subjectAdapter.addAll(subjectList);

        btn_subject.setOnClickListener(view -> {
            if (btn_class_section.getText().equals(getResources().getString(R.string.select_class))) {
                Config.responseSnackBarHandler(getResources().getString(R.string.select_class),
                        findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
                return;
            }
            subjectListBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });

            subjectListBuilder.setAdapter(subjectAdapter, (dialogInterface, i) -> {
                btn_subject.setText(subjectList.get(i));
                selectedSubject = i;
            });

            AlertDialog dialog = subjectListBuilder.create();
            dialog.show();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


//        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this)).observe(this, new Observer<List<ClassNameSectionName>>() {
//            @Override
//            public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
//                classNameSectionList.clear();
//                sectionList.clear();
//                classSectionAdapter.clear();
//                classNameSectionList.addAll(classNameSectionNames);
//                for(int i = 0; i < classNameSectionList.size(); i++){
//                    sectionList.add(classNameSectionList.get(i).ClassName + " " + classNameSectionList.get(i).SectionName );
//                }
//                classSectionAdapter.addAll(sectionList);
//                classSectionAdapter.notifyDataSetChanged();
//            }
//        });


    }

    @Override
    protected void onPause() {
        super.onPause();
//        mDb.schoolClassSectionModel().getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION,this)).removeObservers(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_assignment_create, menu);
        menu.findItem(R.id.action_save).setVisible(false);

        if (Gc.getSharedPreference(Gc.HOMEWORKCREATEALLOWED, this).equalsIgnoreCase(Gc.TRUE)) {
            menu.findItem(R.id.action_save).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save:
                if (validateInput()) {
                    try {
                        saveHomework();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return true;
                }

//            case R.id.action_add_image:

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    Boolean validateInput() {
        if (btn_class_section.getText().equals(getResources().getString(R.string.select_class))) {
            Config.responseSnackBarHandler(getResources().getString(R.string.select_class),
                    findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
            return false;
        }
        if (btn_subject.getText().equals(getResources().getString(R.string.select_subject))) {
            Config.responseSnackBarHandler(getResources().getString(R.string.select_subject),
                    findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
            return false;
        }
        if (btn_assignment_date.getText().equals(getResources().getString(R.string.assignment_date))) {
            Config.responseSnackBarHandler(getResources().getString(R.string.assignment_date),
                    findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
            return false;
        }
        if (btn_submission_date.getText().equals(getResources().getString(R.string.submission_date))) {
            Config.responseSnackBarHandler(getResources().getString(R.string.submission_date),
                    findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
            return false;
        }
        if (et_assignment_text.getText().toString().equals("")) {
            Config.responseSnackBarHandler(getResources().getString(R.string.fill_assignment),
                    findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                //Get ImageURi and load with help of picasso
                //Uri selectedImageURI = data.getData();
                Glide.with(this)
                        .load(data.getData())
                        .apply(RequestOptions.overrideOf(600, 1200))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(iv_assignment);

//                Picasso.with(MainActivity1.this).load(data.getData()).noPlaceholder().centerCrop().fit()
//                        .into((ImageView) findViewById(R.id.imageView1));
            }

        }
    }

    JSONObject buildJsonForSave() throws JSONException {
        final JSONObject p = new JSONObject();

        if (assignmentEntity == null) {
            p.put("sectionid", selectedSectionID);
            p.put("subjectid", schoolSubjects.get(selectedSubject).SubjectId);


        } else {
            p.put("sectionid", assignmentEntity.sectionid);
            p.put("subjectid", assignmentEntity.subjectid);

        }
        p.put("createdby", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this));
        p.put("dateofhomework", btn_assignment_date.getText().toString());
        p.put("homework", et_assignment_text.getText().toString());
        p.put("dosubmission", btn_submission_date.getText().toString());
        p.put("session", Gc.getSharedPreference(Gc.APPSESSION, this));

        // TODO add a button to upload a image
        if (iv_assignment.getDrawable() != null) {
            Bitmap image = ((BitmapDrawable) iv_assignment.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            JSONObject i = new JSONObject();
            i.put("Image", encodedImage);
            i.put("Type", "jpg");
            p.put("image", i);
        }

        return p;
    }

    void saveHomework() throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        final JSONArray param = new JSONArray();
        final JSONObject p = buildJsonForSave();

        param.put(p);

        int meth = Request.Method.POST;

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "homework/homework";


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(meth, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        ArrayList<AssignmentEntity> assignmentEntities = new ArrayList<>();
                        String homeworkId = null;
                        try {
                            JSONArray homeworkDetails = job.getJSONObject("result").getJSONArray("homeworkDetails");
                            homeworkId = homeworkDetails.getJSONObject(0).optString("homeworkId");
                            assignmentEntities = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("homeworkDetails"),
                                            new TypeToken<List<AssignmentEntity>>() {
                                            }.getType());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (assignmentEntities.size() > 0) {
                            mDb.assignmentModel().insertAssignment(assignmentEntities);
                        }
                        Intent intent = new Intent(AssignmentCreateActivity.this, AssignmentUpdateActivity.class);
                        intent.putExtra("homeworkId", homeworkId);
                        startActivity(intent);
                        finish();
                       /* Intent intent = new Intent();
                        intent.putExtra("message", job.optString("message"));
                        setResult(RESULT_OK, intent);
                        finish();*/
                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, AssignmentCreateActivity.this);

                        Intent intent2 = new Intent(AssignmentCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();
                        break;
                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, AssignmentCreateActivity.this);

                        Intent intent1 = new Intent(AssignmentCreateActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.cv_assignment_top_layout), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

                Config.responseVolleyErrorHandler(AssignmentCreateActivity.this, error, findViewById(R.id.cv_assignment_top_layout));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void submitAssignment(String assignment) {
        Intent intent = new Intent(AssignmentCreateActivity.this, AssignmentSubmissionActivity.class);
        intent.putExtra("assignment", assignment);
        startActivity(intent);
    }

}
