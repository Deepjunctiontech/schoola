package in.junctiontech.school.schoolnew.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class AdminInformationEntity {
    @PrimaryKey(autoGenerate = true)
    public int autoid;
    public String gcmToken;
    public String IMEI;
    public String UserDevice;
    public String Status;
    public String Staff_terms;

}
