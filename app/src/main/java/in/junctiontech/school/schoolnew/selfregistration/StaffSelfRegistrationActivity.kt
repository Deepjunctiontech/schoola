package `in`.junctiontech.school.schoolnew.selfregistration

import `in`.junctiontech.school.AppRequestQueueController
import `in`.junctiontech.school.FCM.Config
import `in`.junctiontech.school.R
import `in`.junctiontech.school.schoolnew.DB.MasterEntryEntity
import `in`.junctiontech.school.schoolnew.common.Gc
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class StaffSelfRegistrationActivity : AppCompatActivity() {

    var progressBar: ProgressBar? = null
    private lateinit var et_staff_name: EditText
    private lateinit var btn_staff_user_type: Button
    private lateinit var btn_staff_gender: Button
    private lateinit var btn_staff_registration: Button

    private var mobile: String? = null
    private var idToken: String? = null

    var userTypeList = ArrayList<String>()
    var userTypeListMaster: ArrayList<MasterEntryEntity> = ArrayList<MasterEntryEntity>()
    var userTypeListBuilder: AlertDialog.Builder? = null
    var userTypeListAdapter: ArrayAdapter<String>? = null


    var genderList = ArrayList<String>()
    var genderListMaster: ArrayList<MasterEntryEntity> = ArrayList<MasterEntryEntity>()
    var genderListBuilder: AlertDialog.Builder? = null
    var genderListAdapter: ArrayAdapter<String>? = null

    var selectedUserType = 0
    var selectedGender = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_self_staff)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mobile = intent.getStringExtra("mobile")
        idToken = intent.getStringExtra("idToken")

        initializeViews()
        getMasterEntries()
    }

    private fun initializeViews() {
        progressBar = findViewById(R.id.progressBar)
        et_staff_name = findViewById(R.id.et_staff_name)
        btn_staff_user_type = findViewById(R.id.btn_staff_user_type)

        /*  User Type Select Adapter */

        userTypeListBuilder = AlertDialog.Builder(this)
        userTypeListAdapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice)
        userTypeListAdapter!!.addAll(userTypeList)

        btn_staff_user_type.setOnClickListener {
            userTypeListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            userTypeListBuilder!!.setAdapter(userTypeListAdapter) { _: DialogInterface?, i: Int ->
                btn_staff_user_type.text = userTypeList[i]
                selectedUserType = i
            }
            val dialog: AlertDialog = userTypeListBuilder!!.create()
            dialog.show()
        }

        /*  Gender Select Adapter */
        btn_staff_gender = findViewById(R.id.btn_staff_gender)
        genderListBuilder = AlertDialog.Builder(this)
        genderListAdapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice)
        genderListAdapter!!.addAll(genderList)
        btn_staff_gender.setOnClickListener {
            genderListBuilder!!.setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            genderListBuilder!!.setAdapter(genderListAdapter) { _: DialogInterface?, i: Int ->
                btn_staff_gender.text = genderList[i]
                selectedGender = i
            }
            val dialog: AlertDialog = genderListBuilder!!.create()
            dialog.show()
        }

        btn_staff_registration = findViewById(R.id.btn_staff_registration)


        btn_staff_registration.setOnClickListener {
            if (validateInput()) {
                try {
                    staffSelfRegistration()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun validateInput(): Boolean {
        if ("" == et_staff_name.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.name_can_not_be_blank),
                    findViewById(R.id.rl_staff_self_registration), R.color.fragment_first_blue)
            et_staff_name.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.select_user_type) == btn_staff_user_type.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.select_user_type) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_staff_self_registration), R.color.fragment_first_blue)
            btn_staff_user_type.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (getString(R.string.gender) == btn_staff_gender.text.toString().trim { it <= ' ' }) {
            Config.responseSnackBarHandler(getString(R.string.gender) + " " + getString(R.string.field_can_not_be_blank),
                    findViewById(R.id.rl_staff_self_registration), R.color.fragment_first_blue)
            btn_staff_gender.setError(getString(R.string.error_field_required), getDrawable(R.drawable.ic_alert))
            return false
        }

        if (mobile.isNullOrEmpty() || mobile!!.length > 15 || mobile!!.length < 5) {
            Config.responseSnackBarHandler(getString(R.string.please_enter_valid_mobile_mumber),
                    findViewById(R.id.rl_staff_self_registration), R.color.fragment_first_green)
            return false
        }

        return true
    }

    @Throws(JSONException::class)
    private fun staffSelfRegistration() {
        progressBar!!.visibility = View.VISIBLE
        val param = JSONObject()
        param.put("staffGender", genderListMaster[selectedGender].MasterEntryId)
        param.put("UserType", userTypeListMaster[selectedUserType].MasterEntryId)
        param.put("StaffName", et_staff_name.text.toString().trim { it <= ' ' })
        param.put("StaffMobile", mobile)
        // Log.e("param", param.toString());
        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "selfRegistration/staffSelfRegistration")
        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(Method.POST, url, JSONObject(), Response.Listener { job ->
            progressBar!!.visibility = View.GONE
            val code = job.optString("code")
            when (code) {
                Gc.APIRESPONSE200 -> try {
                    val intent = Intent()
                    intent.putExtra("userId", job.optString("result"))
                    intent.putExtra("name", et_staff_name.text.toString().trim { it <= ' ' })
                    intent.putExtra("mobile", mobile)
                    intent.putExtra("userType", "staff")
                    intent.putExtra("idToken", idToken)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById(R.id.rl_staff_self_registration), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error: VolleyError? ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById(R.id.rl_staff_self_registration))
        }) {
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray? {
                return try {
                    param.toString().toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8")
                    null
                }
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    private fun getMasterEntries() {
        progressBar!!.visibility = View.VISIBLE

        val masterEntryName = JsonArray()
        masterEntryName.add("Gender")
        masterEntryName.add("UserType")

        val p = JSONObject()
        p.put("MasterEntryName", masterEntryName)

        val url = (Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "master/masterEntries/" + p)

        val jsonObjectRequest = object : JsonObjectRequest(Method.GET, url, JSONObject(), Response.Listener { job ->
            val code = job.optString("code")
            progressBar!!.visibility = View.GONE
            when (code) {
                "200" -> try {
                    val masterEntity = Gson()
                            .fromJson<ArrayList<MasterEntryEntity>>(job.getJSONObject("result").optString("masterEntry"), object : TypeToken<List<MasterEntryEntity>>() {

                            }.type)
                    genderListMaster.clear()
                    genderList.clear()

                    userTypeList.clear()
                    userTypeListMaster.clear()

                    for (i in masterEntity.indices) {
                        if ("UserType" == masterEntity[i].MasterEntryName) {
                            userTypeListMaster.add(masterEntity[i])
                            userTypeList.add(masterEntity[i].MasterEntryValue)
                        } else if ("Gender" == masterEntity[i].MasterEntryName) {
                            genderListMaster.add(masterEntity[i])
                            genderList.add(masterEntity[i].MasterEntryValue)
                        }
                    }

                    userTypeListAdapter!!.clear()
                    userTypeListAdapter!!.addAll(userTypeList)
                    userTypeListAdapter!!.notifyDataSetChanged()

                    genderListAdapter!!.clear()
                    genderListAdapter!!.addAll(genderList)
                    genderListAdapter!!.notifyDataSetChanged()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                else -> Config.responseSnackBarHandler(job.optString("message"),
                        findViewById<View>(R.id.rl_staff_self_registration), R.color.fragment_first_blue)
            }
        }, Response.ErrorListener { error ->
            progressBar!!.visibility = View.GONE
            Config.responseVolleyErrorHandler(this, error, findViewById<View>(R.id.rl_staff_self_registration))
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["APPKEY"] = Gc.APPKEY
                headers["ORGKEY"] = Gc.getSharedPreference(Gc.ERPINSTCODE, applicationContext)
                headers["DBNAME"] = Gc.getSharedPreference(Gc.ERPDBNAME, applicationContext)
                headers["Content-Type"] = Gc.CONTENT_TYPE
                headers["DEVICE"] = Gc.DEVICETYPE
                headers["DEVICEID"] = Gc.id(applicationContext)
                headers["USERID"] = Gc.getSharedPreference(Gc.USERID, applicationContext)
                headers["USERTYPE"] = Gc.getSharedPreference(Gc.USERTYPECODE, applicationContext)
                headers["ACCESSTOKEN"] = Gc.getSharedPreference(Gc.ACCESSTOKEN, applicationContext)
                headers["PLANTYPE"] = Gc.getSharedPreference(Gc.ERPPLANTYPE, applicationContext)
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(3000, 2, 2f)
        AppRequestQueueController.getInstance(applicationContext).addToRequestQueue(jsonObjectRequest)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

