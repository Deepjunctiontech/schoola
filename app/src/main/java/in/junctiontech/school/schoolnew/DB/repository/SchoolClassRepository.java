package in.junctiontech.school.schoolnew.DB.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolClassDao;
import in.junctiontech.school.schoolnew.DB.SchoolClassEntity;
import in.junctiontech.school.schoolnew.common.Gc;


public class SchoolClassRepository {
    private SchoolClassDao mClassDao;
    private LiveData<List<SchoolClassEntity>> mAllClass;
    private String appsession ;

    public SchoolClassRepository(Application app){
        MainDatabase mDb = MainDatabase.getDatabase(app);
        mClassDao = mDb.schoolClassModel();
        appsession = Gc.getSharedPreference(Gc.APPSESSION,app);
        mAllClass = mClassDao.getSchoolClassesLive(appsession);
    }

    public LiveData<List<SchoolClassEntity>> getSchoolClassesLive(){
        return mAllClass;
    }

    public String getClassID(String className){
        String classid = mClassDao.getClassId(className,appsession);
        return classid;
    }

    public void insert(SchoolClassEntity[] schoolClassEntities){
        new insertAsyncTask(mClassDao).execute(schoolClassEntities);
    }

    private static class insertAsyncTask extends AsyncTask<SchoolClassEntity, Void, Void>{
        private SchoolClassDao mSchoolClassDao;
        insertAsyncTask(SchoolClassDao dao){
            mSchoolClassDao = dao;
        }
        @Override
        protected Void doInBackground(SchoolClassEntity... schoolClassEntities) {
            ArrayList<SchoolClassEntity> classEntities = new ArrayList<>();
            for (int i=0; i < schoolClassEntities.length; i++){
                classEntities.add(schoolClassEntities[i]);
            }
            mSchoolClassDao.insertSchoolClasses(classEntities);
            return null;
        }
    }
}
