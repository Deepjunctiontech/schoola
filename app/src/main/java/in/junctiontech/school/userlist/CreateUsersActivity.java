package in.junctiontech.school.userlist;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.models.RegisteredStudent;
import in.junctiontech.school.models.StaffDetail;

public class CreateUsersActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private SharedPreferences sp;
    private String dbName;
    private Gson gson;
    private Type typeMasterEntry, typeStudent, typeStaffDetail;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    private AlertDialog.Builder alert;
    private ProgressDialog progressbar;
    private ArrayList<StaffDetail> staffListData = new ArrayList<>();

    private UserStaffFragment fragmentTeacher;
    private UserStudentFragment fragmentStudent;
    private UserParentFragment fragmentParent;
    private ArrayList<RegisteredStudent> studentListData = new ArrayList<>();
    private boolean isDialogShowing = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView tv_create_student_current_session;

    private boolean logoutAlert = false;
    private FrameLayout snackbar;

    private int colorIs;

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {
        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        tabLayout.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //  LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_create_student2);

        snackbar = (FrameLayout) findViewById(R.id.snackbar_view_pager);
        dbName = sp.getString("organization_name", "");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //  classListData = new ArrayList<>();
        tv_create_student_current_session = (TextView) findViewById(R.id.tv_create_student_current_session);

        tv_create_student_current_session.startAnimation(AnimationUtils.
                loadAnimation(this, R.anim.blink));
        tv_create_student_current_session.setText(getString(R.string.for_session) + " : "
                + sp.getString(Config.SESSION, ""));
        gson = new Gson();
        typeMasterEntry = new TypeToken<MasterEntry>() {
        }.getType();
        typeStudent = new TypeToken<RegisteredStudent>() {
        }.getType();
        typeStaffDetail = new TypeToken<StaffDetail>() {
        }.getType();


        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setColorApp();
        setupTabIcons();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle(getString(R.string.staff));
                       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);

                            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
                            tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        }*/
                        break;

                    case 1:

                        getSupportActionBar().setTitle(getString(R.string.student));
                      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                            tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);

                            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
                            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        }*/
                        break;

                    case 2:
                        getSupportActionBar().setTitle(getString(R.string.parent));
                       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);

                            tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
                            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        }*/
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));


        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        /*fetchMasterEntryData();*/
        getStaffDataFromServer();
        getStudentDataFromServer();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                  /*  if (masterEntryPosList.size() == 0)
                        fetchMasterEntryData();*/

                    if (staffListData.size() == 0)
                        getStaffDataFromServer();

                    if (studentListData.size() == 0)
                        getStudentDataFromServer();


                }
            }
        };
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void resetPassword(final StaffDetail staffDetailObj) {
        View password_reset_layout = getLayoutInflater().inflate(R.layout.password_reset, null);
        final EditText et_new_pwd = (EditText) password_reset_layout.findViewById(R.id.et_reset_password_new_password);

        AlertDialog.Builder alertResetPwd = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertResetPwd.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertResetPwd.setView(password_reset_layout);
        alertResetPwd.setCancelable(false);
        alertResetPwd.setTitle(getString(R.string.reset_password));
        ((TextView) password_reset_layout.findViewById(R.id.tv_reset_password_user_name)).setText(
                staffDetailObj.getStaffName()
        );
//        final EditText et_confirm_pwd = (EditText) password_reset_layout.findViewById(R.id.et_reset_password_confirm_password);
//
//
////        et_confirm_pwd.addTextChangedListener(new TextWatcher() {
////            @Override
////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////
////            }
////
////            @Override
////            public void onTextChanged(CharSequence s, int start, int before, int count) {
////                if( !et_new_pwd.getText().toString().equalsIgnoreCase(s.toString())){
////                    et_confirm_pwd.setTextColor(Color.RED);
////                }else et_confirm_pwd.setTextColor(Color.GREEN);
////            }
////
////            @Override
////            public void afterTextChanged(Editable s) {
////
////            }
////        });
        alertResetPwd.setPositiveButton(R.string.reset, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!et_new_pwd.getText().toString().trim().equalsIgnoreCase("")) {
                    convertPassword(et_new_pwd.getText().toString().trim(),
                            staffDetailObj);

                }
            }
        });


        alertResetPwd.show();
    }

    private void resetTeacherPassword(String newPwd, StaffDetail staffDetailObj) throws JSONException {

        final JSONObject param = new JSONObject();
        param.put("Password", newPwd);
        param.put("StaffId", staffDetailObj.getStaffId());

        String updatePwd = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "UserApi.php?action=userUpdate&databaseName=" + dbName /*+
                    "&data=" + (new JSONObject(param_main))*/;
        Log.d("updatePwd", updatePwd);
        Log.d("param", param.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                 updatePwd,param
                 ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("UpdatePwd", jsonObject.toString());
                        progressbar.cancel();

                        alert.setMessage(jsonObject.optString("message"));
                        if (!isFinishing())
                            alert.show();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdatePwd", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(CreateUsersActivity.this, volleyError, snackbar);
            }
        }) ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    private void convertPassword(String pwd, final StaffDetail staffDetailObj) {
     /*   if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

            progressbar.show();
            String convertPwd = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "Conversion.php?type=passwordConvertor&password=" +
                    pwd;

            Log.d("convertPwd", convertPwd);
            StringRequest request = new StringRequest(Request.Method.GET,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    convertPwd
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("convertedPassword", s);

                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                resetTeacherPassword(jsonObject.optString("result"), staffDetailObj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressbar.cancel();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("Result", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(CreateUsersActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
      //  }

    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_teacher1);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_student);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_parent);

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        }*/
    }

    private void setupViewPager(ViewPager viewPager) {
        CreateUsersActivity.ViewPagerAdapter adapter = new CreateUsersActivity.ViewPagerAdapter(getSupportFragmentManager());

        fragmentTeacher = new UserStaffFragment();
        adapter.addFragment(fragmentTeacher, "");
        fragmentStudent = new UserStudentFragment();
        adapter.addFragment(fragmentStudent, "");
        fragmentParent = new UserParentFragment();
        adapter.addFragment(fragmentParent, "");

        viewPager.setAdapter(adapter);
    }

    public void getStaffDataFromServer() {
/*
        if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
           /* String getStaffUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "StaffApi.php?databaseName=" + dbName +
                    "&action=join";
            Log.e("getStaffData", getStaffUrl);*/

            /*final Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("UserType!","0");
            final Map<String, Object> param1 = new LinkedHashMap<>();
            param1.put("filter",new JSONObject(param));
*/
        String getStaffUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "UserApi.php?databaseName=" + dbName +
                "&action=join"/*&data="+new JSONObject(param1)*/;
        Log.e("getUserStaffData", getStaffUrl);

        StringRequest request = new StringRequest(Request.Method.GET,
                getStaffUrl
                  /*  sp.getString("HostName", "Not Found") + "StaffApi.php?databaseName=" +
                            dbName+  "&action=join"*/

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStaffData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                StaffDetail obj = gson.fromJson(s, typeStaffDetail);
                                staffListData = obj.getResult();
                                if (!isFinishing())
                                    fragmentTeacher.updateStaffList(staffListData/*, masterEntryPosList*/);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateUsersActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStaffData", volleyError.toString());
                progressbar.cancel();
                if (viewPager.getCurrentItem() == 0)
                    fragmentTeacher.closeSwipe();

                Config.responseVolleyErrorHandler(CreateUsersActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

   /* private void fetchMasterEntryData() {

        if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {
            Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("MasterEntryName", "StaffPosition");

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));


            Log.e("MasterEntryData", sp.getString("HostName", "Not Found") + "MasterEntryApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param1)));

            StringRequest request = new StringRequest(Request.Method.GET,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    sp.getString("HostName", "Not Found") + "MasterEntryApi.php?databaseName=" + dbName +
                            "&data=" + (new JSONObject(param1))
                    //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("MasterEntryData", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {
                                    MasterEntry obj = gson.fromJson(s, typeMasterEntry);
                                    masterEntryPosList = obj.getResult();
                                } else {
                                    //   alert.setMessage(jsonObject.optString("message"));
                                    //   alert.show();

                                }

                                //   fragmentTeacher.updateMasterEntryList(masterEntryPosList);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("MasterEntryData", volleyError.toString());

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("data", (new JSONObject(param1)).toString());
                    //  Log.d("loginjson", new JSONObject(param).toString());
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        }
    }*/

    public ArrayList<StaffDetail> getStaffListData() {
        return staffListData;
    }

   /* public ArrayList<MasterEntry> getMasterEntry() {
        return masterEntryPosList;
    }*/

    public ArrayList<RegisteredStudent> getStudentListData() {
        return studentListData;
    }

    public void resetParentPassword(final String whoes, final RegisteredStudent registeredStudentObj) {
        View password_reset_layout = getLayoutInflater().inflate(R.layout.password_reset, null);
        final EditText et_new_pwd = (EditText) password_reset_layout.findViewById(R.id.et_reset_password_new_password);

        AlertDialog.Builder alertResetPwdParent = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertResetPwdParent.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertResetPwdParent.setView(password_reset_layout);
        alertResetPwdParent.setCancelable(false);
        alertResetPwdParent.setTitle(getString(R.string.reset_password));

        ((TextView) password_reset_layout.findViewById(R.id.tv_reset_password_user_name)).setText(
                registeredStudentObj.getAdmissionNo()
        );
//        final EditText et_confirm_pwd = (EditText) password_reset_layout.findViewById(R.id.et_reset_password_confirm_password);
//
//
////        et_confirm_pwd.addTextChangedListener(new TextWatcher() {
////            @Override
////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////
////            }
////
////            @Override
////            public void onTextChanged(CharSequence s, int start, int before, int count) {
////                if( !et_new_pwd.getText().toString().equalsIgnoreCase(s.toString())){
////                    et_confirm_pwd.setTextColor(Color.RED);
////                }else et_confirm_pwd.setTextColor(Color.GREEN);
////            }
////
////            @Override
////            public void afterTextChanged(Editable s) {
////
////            }
////        });


        alertResetPwdParent.setPositiveButton(R.string.reset, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!et_new_pwd.getText().toString().trim().equalsIgnoreCase("")) {
                    sendPassword(whoes, et_new_pwd.getText().toString().trim(),
                            registeredStudentObj);

                }
            }
        });


        alertResetPwdParent.show();

    }


    private void sendPassword(final String whoes, final String newPwd, final RegisteredStudent registeredStudentObj) {
     /*   if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

        final Map<String, String> param = new LinkedHashMap<String, String>();
        if (whoes.equalsIgnoreCase("P"))
            param.put("ParentsPassword", newPwd);
        else param.put("StudentsPassword", newPwd);

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("RegistrationId", registeredStudentObj.getRegistrationId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        Log.e("UpdatePwd", new JSONObject(param_main).toString());

        String updatePWD = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StudentRegistrationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("updatePWD", updatePWD);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",

                updatePWD
                //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdatePwd", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (whoes.equalsIgnoreCase("P"))
                                fragmentParent.updateParentPassword(newPwd, registeredStudentObj);
                            else
                                fragmentStudent.updateStudentPassword(newPwd, registeredStudentObj);

                            alert.setMessage(jsonObject.optString("message"));
                            if (!isFinishing())
                                alert.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdatePwd", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(CreateUsersActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

  /*  public void deleteStudent(final RegisteredStudent registeredStudentObj) {
        AlertDialog.Builder alertDelete = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertDelete.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (!Config.checkInternet(CreateUsersActivity.this)) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }

                } else {

                    progressbar.show();


                    JSONObject job_filter = new JSONObject();
                    try {
                        job_filter.put("RegistrationId", registeredStudentObj.getRegistrationId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                    //  param_main.put("data", new JSONObject(param));
                    param_main.put("filter", job_filter);


                    Log.e("deleteResult", new JSONObject(param_main).toString());
                    String deleteRes = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "StudentRegistrationApi.php?databaseName=" + dbName +
                            "&data=" + (new JSONObject(param_main));

                    Log.d("deleteRes", deleteRes);
                    StringRequest request = new StringRequest(Request.Method.DELETE,
                            //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                            deleteRes

                            //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                            ,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String s) {
                                    DbHandler.longInfo(s);
                                    Log.e("deleteResult", s);
                                    progressbar.cancel();
                                    try {
                                        JSONObject jsonObject = new JSONObject(s);

                                        if (jsonObject.optInt("code") == 200 && !isFinishing()) {

                                            studentListData.remove(registeredStudentObj);
                                            fragmentStudent.updateStudentList(studentListData);
                                            fragmentParent.updateParentList(studentListData);

                                        }

                                        alert.setMessage(jsonObject.optString("message"));
                                        if (!isFinishing())
                                            alert.show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("deleteResultClass", volleyError.toString());
                            progressbar.cancel();
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {

                            Map<String, String> header = new LinkedHashMap<String, String>();
                            header.put("Content-Type", "application/x-www-form-urlencoded");
                            return super.getHeaders();
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new LinkedHashMap<String, String>();
                            //   params.put("data",( new JSONObject(param)).toString());

                            //  params.put("className", et_create_class_new_class_name.getText().toString());
                            return params;
                        }
                    };

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    AppRequestQueueController.getInstance(CreateUsersActivity.this).addToRequestQueue(request);
                }

            }
        });

        alertDelete.setMessage(R.string.are_you_sure_you_want_to_delete);
        alertDelete.show();
    }*/

    public void refreshStaffList() {
        getStaffDataFromServer();
    }

    public void refreshStudentList() {
        getStudentDataFromServer();
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }


    public void getStudentDataFromServer() {

       /* if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Session", sp.getString(Config.SESSION, ""));

        String studentFetUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "StudentRegistrationApi.php?databaseName=" +
                dbName + "&action=studentParentGet&data=" + new JSONObject(param);

        Log.e("getStudentData", studentFetUrl/*sp.getString("HostName", "Not Found") + "UserApi.php?databaseName=" +
                    dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/);
        StringRequest request = new StringRequest(Request.Method.GET,
                   /* sp.getString("HostName", "Not Found") + "UserApi.php?databaseName=" +
                            dbName + "&action=join&session=" + sp.getString(Config.SESSION, "")*/
                studentFetUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStudentData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                RegisteredStudent obj = gson.fromJson(s, typeStudent);
                                studentListData = obj.getResult();
                                if (!isFinishing())
                                    fragmentStudent.updateStudentList(studentListData);

                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateUsersActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }


                            //   fragmentParent.updateStaffList(studentListData);

                            if (viewPager.getCurrentItem() == 1)
                                fragmentStudent.closeSwipe();
                            else if (viewPager.getCurrentItem() == 2)
                                fragmentParent.closeSwipe();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStaffData", volleyError.toString());
                progressbar.cancel();

                if (viewPager.getCurrentItem() == 1)
                    fragmentStudent.closeSwipe();
                else if (viewPager.getCurrentItem() == 2)
                    fragmentParent.closeSwipe();


                Config.responseVolleyErrorHandler(CreateUsersActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }
}
