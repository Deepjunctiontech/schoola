package in.junctiontech.school.userlist;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.RegisteredStudent;


public class UserParentFragment extends Fragment implements SearchView.OnQueryTextListener{
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private ArrayList<RegisteredStudent> studentListData = new ArrayList<>();
    private UserParentAdapter adapter;
    private ArrayList<RegisteredStudent> data;
    private SwipeRefreshLayout swipe_refresh_list;
    private SharedPreferences sp;
    private RelativeLayout  page_demo;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data = (RecyclerView) convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available = (TextView) convertView.findViewById(R.id.tv_view_all_data_not_data_available);

        swipe_refresh_list =(SwipeRefreshLayout)convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);

        /*********************************    Demo Screen *******************************/
        sp = Prefs.with(getActivity()).getSharedPreferences();
        page_demo =(RelativeLayout)convertView.findViewById(R.id.rl_view_all_list_item_demo);
        if (sp.getBoolean("DemoUserListPage",true)){
            page_demo.setVisibility(View.VISIBLE);
        }else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoUserListPage",false).commit();
            }
        });
        /****************************************************************/
        setHasOptionsMenu(true);
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });

        if (studentListData.size() == 0) {
            tv_view_all_data_not_data_available.setText(R.string.no_parents_available);
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
        } else tv_view_all_data_not_data_available.setVisibility(View.GONE);

        setupRecycler();
    }

    private void refreshList() {
        ((CreateUsersActivity) getActivity()).refreshStudentList();
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new UserParentAdapter(getContext(), studentListData,appColor);
        recycler_view_list_of_data.setAdapter(adapter);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentListData = ((CreateUsersActivity) getActivity()).getStudentListData();
        appColor = ((CreateUsersActivity) getActivity()).getAppColor();
    }
    public void closeSwipe() {
        if (swipe_refresh_list.isRefreshing()) swipe_refresh_list.setRefreshing(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void updateParentList(ArrayList<RegisteredStudent> studentListData ) {
        this.studentListData = studentListData;
        this.adapter.updateList(studentListData);
        if (studentListData.size() == 0) {
            tv_view_all_data_not_data_available.setText(R.string.no_parents_available);
            tv_view_all_data_not_data_available.setVisibility(View.VISIBLE);
        } else tv_view_all_data_not_data_available.setVisibility(View.GONE);
    }


    public void updateParentPassword(String newPwd, RegisteredStudent registeredStudentObj) {
        studentListData.get(studentListData.indexOf(registeredStudentObj)).setParentsPassword(newPwd);
        this.adapter.updateList(studentListData);
    }



    public void setFilter(ArrayList<RegisteredStudent> studentListData) {
        adapter.updateList(studentListData);

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        setFilter(studentListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<RegisteredStudent> filteredModelList = filter(studentListData, newText);

        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<RegisteredStudent> filter(ArrayList<RegisteredStudent> models, String query) {
        query = query.toLowerCase();final ArrayList<RegisteredStudent> filteredModelList = new ArrayList<>();

         for (RegisteredStudent model : models) {

            if (model.getStudentName().toLowerCase().contains(query)||
                    model.getPositionName().toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

}
