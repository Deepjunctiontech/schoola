package in.junctiontech.school.userlist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.RegisteredStudent;

/**
 * Created by JAYDEVI BHADE on 10/21/2016.
 */

public class UserParentAdapter extends RecyclerView.Adapter<UserParentAdapter.MyViewHolder> {
    private ArrayList<RegisteredStudent> studentList;
    private Context context;
    private int appColor;

    public UserParentAdapter(Context context, ArrayList<RegisteredStudent> studentList, int appColor) {
        this.studentList = studentList;
        this.context = context;
        this.appColor = appColor;

    }

    @Override
    public UserParentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_for_list_data, parent, false);
        return new UserParentAdapter.MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final UserParentAdapter.MyViewHolder holder, int position) {
        final RegisteredStudent studentObj = studentList.get(position);
        holder.tv_item_name.setText(studentObj.getStudentName()/*+"\n"+
        context.getString(R.string.fathers_name)+" : "+
        studentObj.getFatherName()*/);

       // holder.tv_designation.setVisibility(View.VISIBLE);
        holder.tv_designation.setText(context.getString(R.string.class_text)+
                " : "+studentObj.getPositionName() /*studentObj.getClassName()+" "+studentObj.getSectionName()*/
                +"\n"+
                context.getString(R.string.user_name)+" : "+studentObj.getAdmissionNo()+"\n"+
                context.getString(R.string.password)+" : "+studentObj.getParentsPassword()

        );
     //animate(holder);
        Glide.with(context).load(studentObj.getParentProfileImage()==null?"":studentObj.getParentProfileImage())
                .apply(new RequestOptions()
                        .error(R.drawable.ic_single)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .thumbnail(0.5f)
                .into(holder.iv_item_icon);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void updateList(ArrayList<RegisteredStudent> studentList) {
        this.studentList = studentList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name, tv_designation;
        //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
        // Button btn_item_create_class_add_section;
        CircleImageView iv_item_icon;

        public MyViewHolder(final View itemView) {
            super(itemView);
            iv_item_icon   = (CircleImageView) itemView.findViewById(R.id.tv_item_profile_image);
            iv_item_icon.setVisibility(View.VISIBLE);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_title);
            tv_item_name.setTextColor(appColor);
            tv_designation = (TextView) itemView.findViewById(R.id.tv_item_subtitle);
            tv_designation.setVisibility(View.VISIBLE);
            tv_designation.setMaxLines(3);
            tv_designation.setSingleLine(false);
            //    btn_item_create_class_add_section = (Button) itemView.findViewById(R.id.btn_item_create_class_add_section);
            //  ly_item_create_class_sections = (LinearLayout) itemView.findViewById(R.id.ly_item_create_class_sections);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemView.performLongClick();
                }
            });
         /*   itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RegisteredStudent studentObj = studentList.get(getLayoutPosition());
                    if(studentObj.isOnClick()){
                        tv_designation.setText(context.getString(R.string.class_text)+
                                " : "+studentObj.getPositionName() *//*studentObj.getClassName()+" "+studentObj.getSectionName()*//*
                                +"\n"+
                                context.getString(R.string.user_name)+" : "+studentObj.getAdmissionNo()+"\n"+
                                context.getString(R.string.password)+" : "+studentObj.getParentsPassword()
                        );
                        studentObj.setOnClick(false);
                    }else {
                        tv_designation.setText(context.getString(R.string.class_text)+
                                " : "+ studentObj.getPositionName()*//*studentObj.getClassName()+" "+studentObj.getSectionName()*//*

                        );
                        studentObj.setOnClick(true);
                    }
                }
            });*/

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final String[] aa = new String[]{
                            context.getString(R.string.reset_password),
                            context.getString(R.string.copy_login_details),
                            context.getString(R.string.share)+" "+
                                    context.getString(R.string.login_details)
                    };
                    alert.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

//                            if (which == 0) {
//
////                                ((CreateNotificationActivity) context).updateNotification(notificationObj);
//
//                            } else
                            if (which == 0) {

                                ((CreateUsersActivity) context).resetParentPassword("P",studentList.get(getLayoutPosition()));
                            }
                            else if (which==1){
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                clipboard.setText(
                                        context.getString(R.string.organization_key) + "-" +
                                        Prefs.with(context).getSharedPreferences()
                                                .getString(Config.SMS_ORGANIZATION_NAME, "") + "\n" +
                                        context.getString(R.string.login_id) + "-" +
                                        studentList.get(getLayoutPosition()).getAdmissionNo() + "\n" +

                                        context.getString(R.string.password) + "-" +
                                        studentList.get(getLayoutPosition()).getParentsPassword()
                                )
                                ;
                            }else if (which==2){

                                String shareBody =context.getString(R.string.link) + " - " +
                                        Config.SCHOOL_PLAY_STORE_LINK
                                        + "\n" +
                                        context.getString(R.string.organization_key) + "-" +
                                        Prefs.with(context).getSharedPreferences()
                                                .getString(Config.SMS_ORGANIZATION_NAME, "") + "\n" +
                                        context.getString(R.string.login_id) + "-" +
                                        studentList.get(getLayoutPosition()).getAdmissionNo() + "\n" +

                                        context.getString(R.string.password) + "-" +
                                        studentList.get(getLayoutPosition()).getParentsPassword()
                                        ;
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                        "MySchool (Open it in Google Play Store to Download the Application)");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);

                                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                ((CreateUsersActivity)context).overridePendingTransition(R.anim.enter, R.anim.exit);
                            }


                        }
                    });

                    alert.show();


                    return false;
                }
            });
            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));


        }
    }
}
