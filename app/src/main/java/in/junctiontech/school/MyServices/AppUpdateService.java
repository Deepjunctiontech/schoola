package in.junctiontech.school.MyServices;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import org.jsoup.Jsoup;

import java.io.IOException;

import in.junctiontech.school.Prefs;

/**
 * Created by JAYDEVI BHADE on 10/4/2016.
 * No longer useful TODO Remove
 */

public class AppUpdateService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        VersionChecker versionChecker = new VersionChecker();
        versionChecker.execute();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public class VersionChecker extends AsyncTask<String, String, String> {

        String newVersion = "";

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion=  Jsoup.connect("https://play.google.com/store/apps/details?id=" + "in.junctiontech.school" + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

        @Override
        protected void onPostExecute(String latestVersion) {
          //  Log.e("newVersion", latestVersion);
            try {

                Log.e("newVersion", latestVersion);

                    PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    String currentVersion = packageInfo.versionName;

                    Log.e("currentVersion", currentVersion + "");
                    //       Log.e("latestVersion", Double.parseDouble(latestVersion) + "");

                    if (!latestVersion.equalsIgnoreCase("")) {

                        SharedPreferences.Editor editor = Prefs.with(getBaseContext()).getSharedPreferences().edit();
                        if (!currentVersion.equalsIgnoreCase(latestVersion)) {


                            editor.putBoolean("updateRequired", true);


                        }else  editor.putBoolean("updateRequired", false);

                        editor.commit();
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

        }
    }


//    public static final String SMS_BUNDLE = "pdus";
//    public String code;
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
////        Bundle data  = intent.getExtras();
////
////        Object[] pdus = (Object[]) data.get("pdus");
////
////        for(int i=0;i<pdus.length;i++){
////            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
////
////            String sender = smsMessage.getDisplayOriginatingAddress();
////            //You must check here if the sender is your provider and not another one with same text.
////
////            String messageBody = smsMessage.getMessageBody();
////
////            Log.e("MessageBody",messageBody);
////            Toast.makeText(context,messageBody,Toast.LENGTH_LONG).show();
////
////            //Pass on the text to our listener.
////           // mListener.messageReceived(messageBody);
////        }
//
//
//        Bundle intentExtras = intent.getExtras();
//        if (intentExtras != null) {
//            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
//            String smsMessageStr = "";
//            for (int i = 0; i < sms.length; ++i) {
//                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
//
//                String smsBody = smsMessage.getMessageBody().toString();
//
//                if (smsBody.contains("School ERP App")) {
//                    String[] magarr = smsBody.split(": ");
//
//                    code = magarr[1].split(". V")[0];
//                    Log.i("Bunty", code);
//
//
//                    NumberVerification inst = NumberVerification.getContext();
//                    if (!inst.isFinishing())
//                    inst.setOTP(code);
//
//                }
//            }
//          //  Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();
//         //   Log.v("Varsha",smsMessageStr+ "  ritu");
//            //this will update the UI with message
//
//        }
//    }
}
