package in.junctiontech.school.attendance;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.Attendance;

public class FillAttendance extends AppCompatActivity {
    //  private GridView gview;
    private String section_id, date_name;

    private boolean selectAllIsTrue = true;
    private Button button_selectall;
    private DbHandler db;
    private SharedPreferences sharedPsreferences;
    private RecyclerView rv_student_attendance_list;
    private MyRecycleAdapter madapter;
    private ArrayList<Attendance> attendanceArrayList = new ArrayList();
    private int colorIs;
    private ProgressDialog p1;
    private String subjectId="0";
    private boolean slotWise;
    private SharedPreferences sharedPreferences;
    private String db_name;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));
        setContentView(R.layout.activity_students_attandence);
        db = DbHandler.getInstance(this);

        section_id = this.getIntent().getStringExtra("section");
        date_name = this.getIntent().getStringExtra("date");
        sharedPreferences = Prefs.with(this).getSharedPreferences();
        db_name = sharedPreferences.getString("organization_name", "Not Found");
        getSupportActionBar().setTitle(R.string.fill_attendance);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        p1 = new ProgressDialog(this);
        p1.setMessage(getString(R.string.fetching_attendance));


       /* if (getIntent().getBooleanExtra("subjectWiseOrNot", false))
            getSupportActionBar().setSubtitle(this.getIntent().getStringExtra("className")   + " : " +
                    this.getIntent().getStringExtra("subjectName"));
        else*/
        getSupportActionBar().setSubtitle(getString(R.string.class_text) + " : " + this.getIntent().getStringExtra("className"));

        //  gview = (GridView) findViewById(R.id.gv_students_attandence);
        rv_student_attendance_list = (RecyclerView) findViewById(R.id.rv_student_attendance_list);

//        attendanceArrayList = db.getStudentsList(section_id, date_name,
//                "");
        // subjectId = getIntent().getStringExtra("subjectId");
        slotWise = getIntent().getBooleanExtra("slotWise", false);


        try {

            getAttendance(attendanceArrayList, date_name);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (attendanceArrayList.size() == 0) {
            Toast.makeText(this, getString(R.string.no_students_available), Toast.LENGTH_LONG).show();
        }

        /* -------- Recycler view        -----------*/
        madapter = new MyRecycleAdapter(this, attendanceArrayList);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            rv_student_attendance_list.setLayoutManager(gridLayoutManager);
        } else {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);

            rv_student_attendance_list.setLayoutManager(gridLayoutManager);
        }

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        rv_student_attendance_list.setItemAnimator(itemAnimator);
        rv_student_attendance_list.setAdapter(madapter);

        button_selectall = (Button) findViewById(R.id.button_selectall);
    }


    private void getAttendance(final ArrayList<Attendance> studentList, final String date_name) throws JSONException {
        p1.show();
        p1.setCancelable(false);

        JSONArray studentIdsArray = new JSONArray();
        for (int i = 0; i < studentList.size(); i++)
            studentIdsArray.put(studentList.get(i).getAdmissionID());

        final JSONObject param = new JSONObject();

        param.put("studentID", studentIdsArray);
        param.put("monthName", date_name);
        if (slotWise) {

            param.put("SlotID", getIntent().getStringExtra("SlotId"));
        } else {

             param.put("userType", "Teacher");
              param.put("viewRequest", "AttendanceSubjectWise");
        }

        String attUrl = sharedPreferences.getString("HostName", "Not Found")
                + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                + (slotWise?"StudentAttendanceDateWiseApi":"StudentAttendanceApi")+".php?action=teacher&databaseName=" + db_name;
        Log.e("attUrl", attUrl);
        Log.e("param", param.toString());

        //  Log.e("attFetchUrl",sharedPreferences.getString("HostName","Not Found")+"reportview.php");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                attUrl, param
//                sharedPreferences.getString("HostName","Not Found")+"reportview.php"
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject resJson) {

                Log.d("TeacherAttRes", resJson.toString());
//         /       Toast.makeText(ViewReportOfAttendance.this, s, Toast.LENGTH_LONG).show();
                p1.dismiss();

                if (resJson.optString("code").equalsIgnoreCase("201")) {

                    JSONArray job = null;

                    try {
                        if (slotWise)
                            job = (new JSONObject(resJson.optString("result")).getJSONArray("attendance"));
                        else  job = new JSONArray(resJson.optString("result"));
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }


                    if (job.length() != 0) {
                        if (slotWise) {
                            for (int i = 0; i < job.length(); i++) {
                                JSONObject jsonObjStudent = job.optJSONObject(i);
                                for (Attendance attObj : attendanceArrayList) {
                                    if (attObj.getAdmissionID().equalsIgnoreCase(jsonObjStudent.optString("StudentID"))) {
                                        attObj.setPresentStatus(jsonObjStudent.optString("presentStatus"));
                                        break;
                                    }
                                }
                            }
                            madapter.updateList(attendanceArrayList);
                        } else {
                            db.saveAttendanceFromServer(section_id, date_name, subjectId, job
                            );
                        }
                    }


                } else if (resJson.optString("code").equalsIgnoreCase("503")
                        ||
                        resJson.optString("code").equalsIgnoreCase("511")) {
                    if (!isFinishing()) {
                        Config.responseVolleyHandlerAlert(FillAttendance.this, resJson.optInt("code") + "", resJson.optString("message"));

                    }
                }


                if (!slotWise)
                    setAttendance();
                else  madapter.updateList(attendanceArrayList);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                p1.dismiss();
                if (!slotWise)
                    setAttendance();
                if (volleyError instanceof TimeoutError) {
                    Toast.makeText(FillAttendance.this, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof AuthFailureError) {
                    Toast.makeText(FillAttendance.this, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(FillAttendance.this, "ServerError", Toast.LENGTH_LONG).show();
                } else if (volleyError instanceof NetworkError) {
                    Toast.makeText(FillAttendance.this, "NetworkError", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(FillAttendance.this, "ParseError", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (volleyError instanceof NoConnectionError) {
                    Toast.makeText(FillAttendance.this, "NoConnectionError", Toast.LENGTH_LONG).show();
                    //TODO
                }


            }
        });


        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }

    public void setAttendance() {
//        attendanceArrayList = db.getStudentsList(section_id, date_name,
//                subjectId);
        madapter.updateList(attendanceArrayList);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        rv_student_attendance_list.setLayoutManager(new GridLayoutManager(this,
                newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 6 : 3));

        super.onConfigurationChanged(newConfig);
    }

    public class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {
        Context context;
        ArrayList<Attendance> attendanceArrayList;
        int selectedStudentColor;

        public MyRecycleAdapter(Context context, ArrayList<Attendance> attendanceArrayList) {
            this.context = context;
            this.attendanceArrayList = attendanceArrayList;
            selectedStudentColor = context.getResources().getColor(android.R.color.holo_green_dark);
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_all_student, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final Attendance obj = attendanceArrayList.get(position);
            String next = "<font color='#727272'>" + "(" + obj.getAdmissionID() + ")" + "</font>";
            holder.name_check.setText(Html.fromHtml(obj.getStudentName() + " " + next));

//            holder.name_check.setText(st_name[position]
//            +" ("+id[position]+")");
            //  holder.item_id.setText( id[position]);
            //  st_name[position] = holder.name_check.getText().toString();
       /*     if (position == 0 || position == 1)
                Glide.with(context).load(position == 0 ? "http://testapischoolerp.zeroerp.com/images/profileImages/student/2F40F6F0-Student-2.jpg"
                        : "http://testapischoolerp.zeroerp.com/images/profileImages/parent/2F40F6F0-Parent-2.jpg")
                        .thumbnail(0.5f)
                        .error(R.drawable.ic_single)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)

                        .into(holder.civ_item_profile_image);
            else Glide.with(context).load(obj.getProfileUrl())
                    .thumbnail(0.5f)
                    .error(R.drawable.ic_student)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.civ_item_profile_image);*/

            holder.name_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        obj.setPresentStatus("P");
                        holder.ly_view_all_student.setBackgroundColor(selectedStudentColor);

                    } else {
                        obj.setPresentStatus("A");
                        holder.ly_view_all_student.setBackgroundColor(getResources().getColor(R.color.white));

                    }
                }
            });

            if (obj.getPresentStatus() != null && obj.getPresentStatus().equalsIgnoreCase("P")) {
                holder.name_check.setChecked(true);
                obj.setPresentStatus("P");
                holder.ly_view_all_student.setBackgroundColor(selectedStudentColor);

            } else {
                holder.name_check.setChecked(false);
                obj.setPresentStatus("A");
               holder.ly_view_all_student.setBackgroundColor(getResources().getColor(R.color.white));

            }
        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return attendanceArrayList.size();
        }

        public void updateList(ArrayList<Attendance> attendanceArrayList) {
            this.attendanceArrayList = attendanceArrayList;
            this.notifyDataSetChanged();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

           // private final CircleImageView civ_item_profile_image;
            CheckBox name_check;
            //  TextView item_id;
            LinearLayout ly_view_all_student;

            public MyViewHolder(View itemView) {
                super(itemView);
                name_check = (CheckBox) itemView.findViewById(R.id.ck_name_check);
                /*civ_item_profile_image = (CircleImageView) itemView.findViewById(R.id.civ_item_profile_image);
                civ_item_profile_image.setVisibility(View.VISIBLE);*/
             //   name_check.setTextColor(getResources().getColor(R.color.white));
                //    item_id = (TextView) itemView.findViewById(R.id.item_id);
                ly_view_all_student = (LinearLayout) itemView.findViewById(R.id.ly_view_all_student);
                // item_id.setVisibility(View.VISIBLE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (name_check.isChecked()) {
                            name_check.setChecked(false);
                            ly_view_all_student.setBackgroundColor(getResources().getColor(android.R.color.white));
                            attendanceArrayList.get(getLayoutPosition()).setPresentStatus("A");
                        } else {
                            name_check.setChecked(true);
                            ly_view_all_student.setBackgroundColor(selectedStudentColor);
                            attendanceArrayList.get(getLayoutPosition()).setPresentStatus("P");

                        }
                    }
                });
            }


        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    public void save_attandence(View v) {
        try {
            if (slotWise) {
                sendAttendanceSlotWiseToServer();
            } else {
                db.saveAttendanceOfStudent(section_id, date_name, attendanceArrayList, subjectId);
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void  sendAttendanceSlotWiseToServer() throws JSONException {
        JSONArray json_array_section = new JSONArray();
        JSONArray json_array_date = new JSONArray();
        JSONArray prasent_student_array = new JSONArray();
        JSONArray apsent_student_array = new JSONArray();
        Map<String, Object> date_map = new LinkedHashMap<>();
        date_map.put("Date", getIntent().getStringExtra("date"));
        date_map.put("SlotID",getIntent().getStringExtra("SlotId"));

        date_map.put("staffID",getIntent().getStringExtra("staffID"));
        date_map.put("SubjectId",getIntent().getStringExtra("subjectId"));

        for (Attendance attObj : attendanceArrayList) {
            if (attObj.getPresentStatus()!=null && attObj.getPresentStatus().equalsIgnoreCase("P"))prasent_student_array.put(attObj.getAdmissionID());
            else apsent_student_array.put(attObj.getAdmissionID());
        }
        date_map.put("PresentStudentId", prasent_student_array);
        date_map.put("AbsentStudentId", apsent_student_array);

        json_array_date.put(new JSONObject(date_map));
        Map<String, Object> section_map = new LinkedHashMap<>();
        section_map.put("SectionId", getIntent().getStringExtra("section"));
        section_map.put("SectionData", json_array_date);

        json_array_section.put(new JSONObject(section_map));

        Map<String, Object> school_map = new LinkedHashMap<>();
        school_map.put("SchoolAttendance", json_array_section);

        final JSONObject param = new JSONObject();
        //  param.put("DB_Name", db_name);
        param.put("userName", sharedPreferences.getString("loggedUserID", "")/*+"-"+
                sharedPreferences.getString("loggedUserName", "")+"-"+
                sharedPreferences.getString("user_type", "")*/);
        param.put("session", sharedPreferences.getString("session", ""));
        param.put("SchoolData", new JSONObject(school_map));

        //   RequestQueue rqueue = Volley.newRequestQueue(context);

        String attUrl = sharedPreferences.getString("HostName", "Not Found")
                + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                +
                "StudentAttendanceDateWiseApi.php?action=attendanceInsert&databaseName=" + db_name;
        Log.d("AttendanceUrl", attUrl);
        Log.d("param", param.toString());


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,attUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());

                        JSONObject resJson = jsonObject;
                        if (resJson.optString("code").equalsIgnoreCase("201")) {

                                if (sharedPreferences.getBoolean("toastVisibility", false))
                                    Config.myToast(FillAttendance.this, "ic_attendance", "Attendance Uploaded Successfully", Gravity.TOP);
                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (sharedPreferences.getBoolean("toastVisibility", false))
                            Config.myToast(FillAttendance.this, "ic_attendance", "Attendance Not Send \n" +
                                    "Some Error Occurred \n" +
                                    "Try Again Later ....!! ", Gravity.TOP);
                    }
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    public void selectAll(View v) {
        if (selectAllIsTrue) {
            button_selectall.setText(getString(R.string.un_select_all));
            for (Attendance obj : attendanceArrayList)
                obj.setPresentStatus("P");

            selectAllIsTrue = false;

        } else {
            button_selectall.setText(getString(R.string.select_all));
            selectAllIsTrue = true;
            for (Attendance obj : attendanceArrayList)
                obj.setPresentStatus("A");
        }
        madapter.updateList(attendanceArrayList);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_sort_by_alphabetical:
                Toast.makeText(this, getString(R.string.sort_by_alphabetical), Toast.LENGTH_LONG).show();
                Collections.sort(attendanceArrayList, new Comparator<Attendance>() {
                    @Override
                    public int compare(Attendance o1, Attendance o2) {
                        String lname = o1.getStudentName().toLowerCase();
                        String rname = o2.getStudentName().toLowerCase();

                        //  Log.e("compareTo",lname.compareTo(rname)+" ritu");
                        //  Log.e("ArrayListSort","INTEGER");
                        return lname.compareTo(rname);


                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                madapter.notifyDataSetChanged();
                break;

            case R.id.menu_sort_by_number:
                Toast.makeText(this, getString(R.string.sort_by_number), Toast.LENGTH_LONG).show();

                Collections.sort(attendanceArrayList, new Comparator<Attendance>() {
                    @Override
                    public int compare(Attendance o1, Attendance o2) {
                        String lid = o1.getAdmissionID();
                        String rid = o2.getAdmissionID();

                        try {
                            int ld = Integer.parseInt(lid);
                            int rd = Integer.parseInt(rid);
                            //  Log.e("ArrayListSort","INTEGER");
                            if (ld > rd)
                                return 1;
                            else if (ld < rd)
                                return -1;
                            else
                                return 0;

                        } catch (NumberFormatException e) {
                            // Log.e("ArrayListSort","String");
                            return lid.compareTo(rid);
                        }

                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                madapter.notifyDataSetChanged();
                break;


        }

        return super.onOptionsItemSelected(item);
    }

}
