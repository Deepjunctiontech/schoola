package in.junctiontech.school.attendance;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import in.junctiontech.school.schoolnew.adminpanel.SubActivity;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.createtimetable.CreateSlotsActivity;
import in.junctiontech.school.createtimetable.SlotsDetails;
import in.junctiontech.school.models.StaffDetail;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.models.Subject;

public class AttendanceFirstActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private View snackbar;
    private DbHandler db;
    private int colorIs;
    private RadioButton rb_full_day, rb_period;
    private Spinner sp_first_class, sp_slot, sp_subject, sp_teacher;
    private ArrayAdapter<String> clsAdapter;
    private ArrayList<StudentData> classSectionList = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>();
    private Calendar cal;
    private Button btn_date;
    private LinearLayout ll_periodwise_attendance;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private ArrayList<SlotsDetails> slotList = new ArrayList<>();
    private ArrayList<StaffDetail> staffListData = new ArrayList<>();
    private ArrayList<Subject> subjectListData = new ArrayList<>();
    private boolean logoutAlert;
    private final Integer[] drawableListArray = CreateSlotsActivity.drawableListArray,
            nameDrawableListArray = CreateSlotsActivity.nameDrawableListArray;
    private final ArrayList<String> nameEnglishListArray = new ArrayList<>();
    private LectureAdapter<Object> lectureTypeAdapter;
    private ArrayList<String> subjectNameList = new ArrayList<>();
    private ArrayList<String> staffNameList = new ArrayList<>();
    private ArrayAdapter<String> adapterStaffName = null;

    private ArrayList<StaffDetail> staffList = new ArrayList<>();
    private ArrayAdapter<String> adapterSubjectName;
    private boolean isSlotAlertOpen = false;
    private androidx.appcompat.app.AlertDialog.Builder alertDialogSlot;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_attendance_first_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_attendance_first_date)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_attendance_first_slot)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_attendance_first_subject)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_attendance_first_teacher)).setTextColor(colorIs);
        final Button btn_attendance_first_next = (Button) findViewById(R.id.btn_attendance_first_next);
        btn_attendance_first_next.setBackgroundColor(colorIs);
        btn_attendance_first_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (classSectionNameList.size() > 0) {
                    Intent intent = new Intent(AttendanceFirstActivity.this, FillAttendance.class);
                    intent.putExtra("className", classSectionNameList.get(sp_first_class.getSelectedItemPosition()));
                    intent.putExtra("section", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());
                    intent.putExtra("date", btn_date.getText().toString());
                    Log.e("RITU", rb_period.isSelected() + " RITU");
                    if (rb_full_day.isChecked()) {
                        Log.e("RITU", rb_period.isSelected() + " RITU1");
                        intent.putExtra("slotWise", false);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.nothing);
                    } else {
                        intent.putExtra("slotWise", true);
                        Log.e("RITU", rb_period.isSelected() + " RITU 2");
                        if (slotList.size() > 0) {
                            intent.putExtra("SlotId", slotList.get(sp_slot.getSelectedItemPosition()).getSlotID());

                            String selectedTeacher = "";
                            if (sp_teacher.getSelectedItemPosition() == 0)
                                selectedTeacher = "";
                            else if (staffListData.size() >= sp_teacher.getSelectedItemPosition())
                                selectedTeacher = staffListData.get(sp_teacher.getSelectedItemPosition() - 1).getStaffId();


                            String selectedSubject = "";
                            if (sp_subject.getSelectedItemPosition() == 0)
                                selectedSubject = "";
                            else if (subjectListData.size() >= sp_subject.getSelectedItemPosition())
                                selectedSubject = subjectListData.get(sp_subject.getSelectedItemPosition() - 1).getSubjectId();


                            intent.putExtra("staffID", selectedTeacher);
                            intent.putExtra("subjectId", selectedSubject);

                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                        } else {
                            showAlertSlotsNotAvailable();
                        }

                    }

                } else if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                        ||
                        sp.getString("user_type", "").equalsIgnoreCase("admin"))
                    showSnack(true, getString(R.string.classes_not_available));
                else if (classSectionList.size() == 0)
                    showSnack(false, getString(R.string.classes_not_available));

            }
        });
    }

    private void showAlertSlotsNotAvailable() {

        alertDialogSlot.show();


    }

    private void showSnack(boolean isAdmin, String msg) {


        Snackbar snackbarObj = null;
        if (classSectionList.size() == 0) {
            if (isAdmin)
                snackbarObj = Snackbar.make(
                        snackbar,
                        msg,
                        Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                    Toast.makeText(AttendanceFirstActivity.this,"Permission not available : CREATE CLASS SECTION!",Toast.LENGTH_SHORT).show();

                                }else {
                                    startActivity(new Intent(AttendanceFirstActivity.this, ClassSectionActivity.class));
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                                }
                            }
                        });
            else snackbarObj = Snackbar.make(snackbar,
                    msg,
                    Snackbar.LENGTH_LONG).setAction(R.string.reload_school_data, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.getDataFromServer();
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }
            });

        }
        if (snackbarObj != null) {
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_first);
        sp = Prefs.with(this).getSharedPreferences();


        /*********************************    Demo Screen *******************************/
        final View page_demo = findViewById(R.id.rl_activity_attendance_first_demo);

        if (sp.getBoolean("DemoNewStudentAttendanceActivity", true)) {
            page_demo.setVisibility(View.VISIBLE);
        } else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoNewStudentAttendanceActivity", false).commit();
            }
        });

        /****************************************************************/
        alertDialogSlot = new androidx.appcompat.app.AlertDialog.Builder(AttendanceFirstActivity.this);
        alertDialogSlot.setTitle(getString(R.string.result));
        alertDialogSlot.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info));
        alertDialogSlot.setMessage("Slots not available fot this class !");
        alertDialogSlot.setNegativeButton(getString(R.string.cancel), null);

        if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("admin"))
            alertDialogSlot.setPositiveButton(getString(R.string.create_slots), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isSlotAlertOpen = false;
                    startActivity(new Intent(AttendanceFirstActivity.this, SubActivity.class).putExtra("whichActionData", "timeTable"));
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });
        else
            alertDialogSlot.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isSlotAlertOpen = false;
                }
            });

        snackbar =   findViewById(R.id.activity_attendance_first);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = DbHandler.getInstance(this);
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        cal = Calendar.getInstance();

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        nameEnglishListArray.add("Lecture");
        nameEnglishListArray.add("Lab");
        nameEnglishListArray.add("Games");
        nameEnglishListArray.add("Prayer");
        nameEnglishListArray.add("Yoga");
        nameEnglishListArray.add("Music");
        nameEnglishListArray.add("Breakfast");
        nameEnglishListArray.add("Lunch");
        nameEnglishListArray.add("Dinner");
        nameEnglishListArray.add("Dance");
        nameEnglishListArray.add("Art & Craft");
        nameEnglishListArray.add("Exam");
        nameEnglishListArray.add("Break");
        nameEnglishListArray.add("Activity");
        nameEnglishListArray.add("library");

        lectureTypeAdapter = new LectureAdapter(this, drawableListArray, nameDrawableListArray, nameEnglishListArray, slotList);
        lectureTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        staffNameList.add(getString(R.string.select));
        for (StaffDetail staffObj : staffList)
            staffNameList.add(staffObj.getStaffName());
        adapterStaffName = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, staffNameList);
        adapterStaffName.setDropDownViewResource(R.layout.myspinner_dropdown_item);

        subjectNameList.add(getString(R.string.select));
        for (Subject obj : subjectListData)
            subjectNameList.add(obj.getSubjectName());
        adapterSubjectName = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, subjectNameList);
        adapterSubjectName.setDropDownViewResource(R.layout.myspinner_dropdown_item);

        setColorApp();
        intViews();

        classSectionList = db.getClassSectionList();
        for (StudentData obj : classSectionList)
            classSectionNameList.add(obj.getStudentName());

        try {
            setClassAdapter();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setSlotAdapter();

        final String sessionStartDate = sp.getString("sessionStartDate", "");
        final String sessionEndDate = sp.getString("sessionEndDate", "");
        //final String[] sessionStartDate_arr = sessionStartDate.split("-");
        // final String[] sessionEndDate_arr = sessionEndDate.split("-");
         btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get current date

                // Create the DatePickerDialog instance

                DatePickerDialog datePicker = new DatePickerDialog(AttendanceFirstActivity.this, myDateListener,
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));
                try {
                    cal.setTime(Config.currentDate.parse(sessionEndDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //cal.set(Integer.parseInt(sessionEndDate_arr[2]), Integer.parseInt(sessionEndDate_arr[1]), Integer.parseInt(sessionEndDate_arr[0]));
                datePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());

                try {
                    cal.setTime(Config.currentDate.parse(sessionStartDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
               // cal.set(Integer.parseInt(sessionStartDate_arr[2]), Integer.parseInt(sessionStartDate_arr[1]), Integer.parseInt(sessionStartDate_arr[0]));
                datePicker.getDatePicker().setMinDate(cal.getTimeInMillis());
                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        getStaffDataFromServer();
    }


    public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_date.setText(year + "-" + new_month + "-" + new_day);
            try {
                getSlotList();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void setClassAdapter() throws JSONException {
        clsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classSectionNameList);
        clsAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_class.setAdapter(clsAdapter);
        if (classSectionList.size() == 0)
            Config.responseSnackBarHandler(getString(R.string.classes_not_available), snackbar);
        else getSlotList();
    }

    private void setSlotAdapter() {
        lectureTypeAdapter = new LectureAdapter(this, drawableListArray, nameDrawableListArray, nameEnglishListArray, slotList);
        lectureTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_slot.setAdapter(lectureTypeAdapter);

        if (classSectionList.size() == 0)
            Config.responseSnackBarHandler(getString(R.string.classes_not_available), snackbar);
    }

    private void intViews() {
        sp_first_class = (Spinner) findViewById(R.id.sp_attendance_first_class);

        sp_slot = (Spinner) findViewById(R.id.sp_attendance_first_slot);
        sp_subject = (Spinner) findViewById(R.id.sp_attendance_first_subject);
        sp_teacher = (Spinner) findViewById(R.id.sp_attendance_first_teacher);
        btn_date = (Button) findViewById(R.id.btn_attendance_first_date);
        btn_date.setText((new SimpleDateFormat("yyyy-M-dd", Locale.ENGLISH)).format(new Date()));

        rb_full_day = (RadioButton) findViewById(R.id.rb_attendance_first_full_day);
        rb_period = (RadioButton) findViewById(R.id.rb_attendance_first_period);
        rb_full_day.setChecked(true);


        ll_periodwise_attendance = (LinearLayout) findViewById(R.id.ll_attendance_first_periodwise_attendance);
        ll_periodwise_attendance.setVisibility(View.GONE);
        rb_period.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("RITU", "rb_period " + isChecked);
                if (isChecked) {

                    ll_periodwise_attendance.setVisibility(View.VISIBLE);

                } else {
                    ll_periodwise_attendance.setVisibility(View.GONE);

                }
            }

        });

        sp_slot.setAdapter(lectureTypeAdapter);
        sp_first_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    getSubjectClassWise();
                    getSlotList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_slot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setSubjectSlotWise();
                setStaffSlotWise();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setStaffSlotWise() {
        sp_teacher.setSelection(0);
        if (slotList.size() >= sp_slot.getSelectedItemPosition() && sp_slot.getSelectedItemPosition() != -1) {
            SlotsDetails slotObj = slotList.get(sp_slot.getSelectedItemPosition());
            for (int pos = 0; pos < staffList.size(); pos++) {
                Log.e("RITU", staffList.get(pos).getStaffId() + " ritu");
                if (staffList.get(pos).getStaffId().equalsIgnoreCase(null == slotObj.getTeacher() ? "" : slotObj.getTeacher())) {
                    sp_teacher.setSelection(pos + 1);
                    break;
                }
            }
        }
    }

    private void setSubjectSlotWise() {
        sp_subject.setSelection(0);
        if (slotList.size() >= sp_slot.getSelectedItemPosition() && sp_slot.getSelectedItemPosition() != -1) {
            SlotsDetails slotObj = slotList.get(sp_slot.getSelectedItemPosition());

            Log.e("RITU", slotObj.getSubject() + " ritu");
            for (int pos = 0; pos < subjectListData.size(); pos++) {
                Log.e("RITU", subjectListData.get(pos).getSubjectId() + " ritu");
                if (subjectListData.get(pos).getSubjectId().equalsIgnoreCase(null == slotObj.getSubject() ? "" : slotObj.getSubject())) {
                    sp_subject.setSelection(pos + 1);
                    break;
                }
            }
        }
    }

    private void getSlotList() throws JSONException {

        if (classSectionList.size() >= sp_first_class.getSelectedItemPosition() && sp_first_class.getSelectedItemPosition() != -1) {
            progressbar.show();
            JSONObject param = new JSONObject();
            param.put("SectionID", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());
            param.put("Date", btn_date.getText().toString());

            JSONObject jsonFilter = new JSONObject();
            jsonFilter.put("filter", param);
            jsonFilter.put("action", "Date");

            String getClassUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "TimeTableDayDateWise.php?databaseName=" +
                    dbName +
                    "&data=" + jsonFilter;

            Log.d("getDateSlotUrl", getClassUrl);

            StringRequest request = new StringRequest(Request.Method.GET,
                    getClassUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("geDatetSlotData", s);
                            slotList = new ArrayList<>();
                            if (!isFinishing())
                                progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {
                                    SlotsDetails slotObj = gson.fromJson(s, new TypeToken<SlotsDetails>() {
                                    }.getType());

                                    slotList = slotObj.getResult();

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(AttendanceFirstActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                else
                                    Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Collections.sort(slotList, new Comparator<SlotsDetails>() {
                                @Override
                                public int compare(SlotsDetails o1, SlotsDetails o2) {
                                    Calendar calLeft = Calendar.getInstance();
                                    Calendar calRight = Calendar.getInstance();
                                    try {
                                        calLeft.setTime(CreateSlotsActivity.time.parse(o1.getSlotStartTime()));
                                    } catch (ParseException e) {
                                        calLeft.setTimeInMillis(cal.getTimeInMillis());
                                        e.printStackTrace();
                                    }
                                    try {
                                        calRight.setTime(CreateSlotsActivity.time.parse(o2.getSlotStartTime()));
                                    } catch (ParseException e) {
                                        calRight.setTimeInMillis(cal.getTimeInMillis());
                                        e.printStackTrace();
                                    }

                                    return calLeft.compareTo(calRight);

                                }
                            });

                            if (slotList.size() > 0) {
                                SlotsDetails slotObj = slotList.get(0);
                                if ((slotObj.getHoliday()).equalsIgnoreCase("1")) {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AttendanceFirstActivity.this);
                                    alertDialog.setTitle(getString(R.string.holiday));
                                    alertDialog.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info));
                                    alertDialog.setMessage(null == slotObj.getComment() ? "" : slotObj.getComment());
                                    alertDialog.setPositiveButton(getString(R.string.ok), null);
                                    alertDialog.show();
                                    slotList.clear();
                                }

                            }

                            setSlotAdapter();

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getDateSlotData", volleyError.toString());
                    slotList = new ArrayList<>();
                    setSlotAdapter();
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(AttendanceFirstActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        } else if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("admin"))
            showSnack(true, getString(R.string.classes_not_available));
        else showSnack(false, getString(R.string.classes_not_available));
    }

    public void updateStaffList(ArrayList<StaffDetail> staffListData) {
        this.staffList = staffListData;
        staffNameList = new ArrayList<>();

        staffNameList.add(getString(R.string.select));
        for (StaffDetail staffObj : staffListData)
            staffNameList.add(staffObj.getStaffName());


        adapterStaffName = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, staffNameList);
        adapterStaffName.notifyDataSetChanged();
        sp_teacher.setAdapter(adapterStaffName);
        setStaffSlotWise();
    }

    public void updateSubjectList(ArrayList<Subject> subjectListData) {
        this.subjectListData = subjectListData;
        subjectNameList = new ArrayList<>();
        subjectNameList.add(getString(R.string.select));
        for (Subject obj : subjectListData)
            subjectNameList.add(obj.getSubjectName());

        adapterSubjectName = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, subjectNameList);
        sp_subject.setAdapter(adapterSubjectName);
        adapterSubjectName.notifyDataSetChanged();
        setSubjectSlotWise();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

    }

    public static class LectureAdapter<T> extends ArrayAdapter<Integer> {
        private Integer[] images, imagesName;
        private ArrayList<SlotsDetails> slotList;
        private ArrayList<String> nameEnglishListArray;

        public LectureAdapter(Context context, Integer[] images, Integer[] imagesName, ArrayList<String> nameEnglishListArray, ArrayList<SlotsDetails> slotList) {
            super(context, android.R.layout.simple_spinner_item, images);
            this.images = images;
            this.imagesName = imagesName;
            this.slotList = slotList;
            this.nameEnglishListArray = nameEnglishListArray;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public int getCount() {
            return slotList.size();
        }

        @Nullable
        @Override
        public Integer getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.lecture_type, parent, false);
            int dblPos = nameEnglishListArray.indexOf(slotList.get(position).getType());

            ((ImageView) viewItem.findViewById(R.id.ic_item_lecture_type)).setImageResource(images[dblPos == -1 ? 0 : dblPos]);
            ((TextView) viewItem.findViewById(R.id.tv_item_lecture_type)).setText(imagesName[dblPos == -1 ? 0 : dblPos]);
            return viewItem;
        }


    }

    public void getSubjectClassWise() throws JSONException {
        if (classSectionList.size() >= sp_first_class.getSelectedItemPosition() && sp_first_class.getSelectedItemPosition() != -1) {
            progressbar.show();
            JSONObject param = new JSONObject();
            param.put("SectionID", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());

            JSONObject jsonFilter = new JSONObject();
            jsonFilter.put("filter", param);
            String SubjectGetUrl = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "SubjectApi.php?databaseName=" +
                    dbName +
                    "&action=join&session=" + sp.getString(Config.SESSION, "")
                    + "&data=" + jsonFilter;
            Log.d("SubjectGetUrl", SubjectGetUrl);
            subjectListData.clear();
            StringRequest request = new StringRequest(Request.Method.GET,

                    SubjectGetUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getSubjectData", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 201) {
                                    Subject obj = gson.fromJson(s, new TypeToken<Subject>() {
                                    }.getType());
                                    subjectListData = obj.getResult();

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(AttendanceFirstActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            updateSubjectList(subjectListData);

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getSubjectData", volleyError.toString());
                    progressbar.cancel();
                    subjectListData.clear();
                    updateSubjectList(subjectListData);
                    Config.responseVolleyErrorHandler(AttendanceFirstActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        } else if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sp.getString("user_type", "").equalsIgnoreCase("admin"))
            showSnack(true, getString(R.string.classes_not_available));
        else showSnack(false, getString(R.string.classes_not_available));
    }

    public void getStaffDataFromServer() {

        progressbar.show();

        String getStaffUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffApi.php?databaseName=" +
                dbName + "&action=join";

        Log.d("getStaffData", getStaffUrl);
        StringRequest request = new StringRequest(Request.Method.GET,
                getStaffUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStaffData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                StaffDetail obj = gson.fromJson(s, new TypeToken<StaffDetail>() {
                                }.getType());
                                staffListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(AttendanceFirstActivity.this).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(AttendanceFirstActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                                    /* set Staff List*/
                        updateStaffList(staffListData);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStaffData", volleyError.toString());
                progressbar.cancel();

                Config.responseVolleyErrorHandler(AttendanceFirstActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }


}
