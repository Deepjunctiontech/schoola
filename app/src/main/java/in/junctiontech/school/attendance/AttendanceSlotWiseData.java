package in.junctiontech.school.attendance;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 22-08-2017.
 */

public class AttendanceSlotWiseData implements Serializable {
    String presentStatus,StaffName,SubjectName,SlotID,SlotName;
    ArrayList<AttendanceSlotWiseData> result;

    public String getPresentStatus() {
        return presentStatus;
    }

    public void setPresentStatus(String presentStatus) {
        this.presentStatus = presentStatus;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSlotID() {
        return SlotID;
    }

    public void setSlotID(String slotID) {
        SlotID = slotID;
    }

    public String getSlotName() {
        return SlotName;
    }

    public void setSlotName(String slotName) {
        SlotName = slotName;
    }

    public ArrayList<AttendanceSlotWiseData> getResult() {
        return result;
    }

    public void setResult(ArrayList<AttendanceSlotWiseData> result) {
        this.result = result;
    }
}
