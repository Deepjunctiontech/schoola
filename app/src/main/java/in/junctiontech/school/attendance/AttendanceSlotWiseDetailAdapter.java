package in.junctiontech.school.attendance;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.ReportView.ParentViewAttendance;

/**
 * Created by JAYDEVI BHADE on 22-08-2017.
 */

public class AttendanceSlotWiseDetailAdapter extends RecyclerView.Adapter<AttendanceSlotWiseDetailAdapter.ViewHolder> {
    private ParentViewAttendance parentViewAttendance;
    private ArrayList<AttendanceSlotWiseData> resultAttendanceArray;
    private int colorIs;
    Drawable presentResourceId;
    Drawable absentResourceId;

    public AttendanceSlotWiseDetailAdapter(ParentViewAttendance parentViewAttendance, ArrayList<AttendanceSlotWiseData> resultAttendanceArray, int colorIs) {
        this.parentViewAttendance = parentViewAttendance;
        this.resultAttendanceArray = resultAttendanceArray;
        this.colorIs = colorIs;
        presentResourceId = parentViewAttendance.getResources().getDrawable(R.drawable.right_answer);
        absentResourceId = parentViewAttendance.getResources().getDrawable(R.drawable.gridborder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attendance_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AttendanceSlotWiseData obj = resultAttendanceArray.get(position);
        if (obj.getPresentStatus().equalsIgnoreCase("P")) {
            holder.item_text_status.setText("P");
            holder.item_text_status.setBackground(presentResourceId);
        } else {holder.item_text_status.setBackground(absentResourceId);
            holder.item_text_status.setText("A");}

        holder.tv_item_name.setText(obj.getSlotName()==null?"":obj.getSlotName());
        holder.tv_subitem_name.setText((obj.getSubjectName()==null?"":obj.getSubjectName())+
                "\n"+(obj.getStaffName()==null?"":obj.getStaffName()));



    }

    @Override
    public int getItemCount() {
        return resultAttendanceArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView item_text_status, tv_subitem_name,tv_item_name;


        public ViewHolder(View itemView) {
            super(itemView);

            item_text_status = (TextView) itemView.findViewById(R.id.item_text_status);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_text_status_item_name);
            tv_subitem_name = (TextView) itemView.findViewById(R.id.tv_text_status_subitem_name);
            tv_item_name.setTextColor(colorIs);

        }
    }
}
