package in.junctiontech.school.FCM;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.admin.msgpermission.ChatPermission;
import in.junctiontech.school.models.NotificationEvent;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.DB.SchoolCalenderEntity;
import in.junctiontech.school.schoolnew.chatdb.ChatDatabase;
import in.junctiontech.school.schoolnew.chatdb.ChatListEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.notificationsystem.NotificationHelper;

import static in.junctiontech.school.schoolnew.common.Gc.SCHOOLPREF;

/**
 * Created by LEKHPAL DANGI on 5/12/2016.
 */
public class MyFCMPushReceiver extends FirebaseMessagingService {

    private static final String TAG = MyFCMPushReceiver.class.getSimpleName();

    MainDatabase mDb;
    ChatDatabase chatDb;

    int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

    private SharedPreferences sp;
    // private NotificationUtils notificationUtils;
    SimpleDateFormat sdf1 = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.getDefault());

    @Override
    public void onNewToken(String s) {

        SharedPreferences sharedPreferences = getSharedPreferences(Gc.NOTLOGINVALUES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Gc.FCMTOKEN, s);
        editor.apply();

        if ((Gc.getSharedPreference(Gc.USERID, getApplicationContext())).equalsIgnoreCase(Gc.NOTFOUND)) {
            Log.i("onNewToken", "No login yet:" + s);
        } else {
            try {
                sendTokenToServer(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {

        sp = Prefs.with(this).getSharedPreferences();

        chatDb = ChatDatabase.getDatabase(this);
        mDb = MainDatabase.getDatabase(this);
        DbHandler db = DbHandler.getInstance(this);

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.e("Notification", "From: " + remoteMessage.getData());
            Map map = remoteMessage.getData();
            if (!map.isEmpty()) {

                Log.e("message", map.get("message").toString());
                Log.e("title", (String) map.get("title"));

                String data_msg = (String) map.get("message");
                String title;
                title = (String) map.get("title");

                switch (title) {
                    case "EventNotification":
                        final ChatListEntity eventNotification = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);

                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(eventNotification);
                        }).start();

                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_ATTENDANCE_TEXT, eventNotification.msg, Gc.CHANNELID_OTHER_INFO, R.drawable.ic_attendance);
                        break;

                    case "Attendance":
                        final ChatListEntity Attendance = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);

                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(Attendance);
                        }).start();

                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_ATTENDANCE_TEXT, Attendance.msg, Gc.CHANNELID_ATTENDANCE, R.drawable.ic_attendance);
                        break;
                    case "BirthDayGreeting":
                        final ChatListEntity BirthDayGreeting = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(BirthDayGreeting);
                        }).start();
                        NotificationHelper.showNotificationBitmap(BirthDayGreeting.msg
                                , BitmapFactory.decodeResource(getResources(), R.drawable.happybirthday), Gc.BIRTHDAY_GREETINGS_TEXT, this, Gc.CHANNELID_GREETINGS);
                        break;
                    case "LibraryBooks":
                        final ChatListEntity LibraryBooks = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(LibraryBooks);
                        }).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_LIBRARY_TEXT, LibraryBooks.msg, Gc.CHANNELID_LIBRARY, R.drawable.ic_library);
                        break;
                    case "EarlyDeparture":

                        final ChatListEntity earlyDepartures = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);

                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(earlyDepartures);
                        }).start();

                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_ATTENDANCE_TEXT, earlyDepartures.msg, Gc.CHANNELID_ATTENDANCE, R.drawable.ic_attendance);
                        break;
                    case "NotUsingTransport":
                        final ChatListEntity NotUsingTransport = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(NotUsingTransport);
                        }).start();

                        NotificationHelper.showNotificationSimple(this, Gc.Transport, NotUsingTransport.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);
                        break;
                    case "LateArrival":
                        final ChatListEntity lateArrival = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);

                        new Thread(() -> {
                            chatDb.chatListModel().insertChat(lateArrival);
                        }).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_ATTENDANCE_TEXT, lateArrival.msg, Gc.CHANNELID_ATTENDANCE, R.drawable.ic_attendance);
                        break;
                    case "EventCalendar":
                        try {
                            final JSONObject data = new JSONObject(data_msg);
                            final ArrayList<SchoolCalenderEntity> calender = new Gson().fromJson(data.optString("EventCalendarDetails"),
                                    new TypeToken<List<SchoolCalenderEntity>>() {
                                    }.getType());
                            new Thread(() -> {
                                mDb.schoolCalenderModel().insertSchoolCalender(calender);
                            }).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        final ChatListEntity EventCalendar = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(EventCalendar)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_EVENTS_TEXT, EventCalendar.msg, Gc.CHANNELID_EVENTS, R.mipmap.ic_launcher);
                        break;
                    case "deleteEventCalendar":
                        //Log.e("calendar delete", data_msg);
                        try {
                            final JSONObject data = new JSONObject(data_msg);
                            new Thread(() -> {
                                mDb.schoolCalenderModel().deleteSingleCalendar(data.optString("CalendarId"));
                            }).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "ExamResult":
                        final ChatListEntity ExamResult = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(ExamResult)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_EXAM_TEXT, ExamResult.msg, Gc.CHANNELID_EXAM, R.drawable.ic_exam_result);
                        break;
                    case "ExamSchedule":
                        final ChatListEntity ExamSchedule = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(ExamSchedule)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_EXAM_TEXT, ExamSchedule.msg, Gc.CHANNELID_EXAM, R.drawable.ic_exam_result);
                        break;
                    case "FeeReminder":
                        final ChatListEntity FeeReminder = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(FeeReminder)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_FEE_REMINDER_TEXT, FeeReminder.msg, Gc.CHANNELID_FEE_REMINDER, R.mipmap.ic_launcher);
                        break;
                    case "FeePayment":
                        final ChatListEntity FeePayment = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(FeePayment)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_FEE_RECEIPT_TEXT, FeePayment.msg, Gc.CHANNELID_FEE_RECEIPT, R.mipmap.ic_launcher);
                        break;
                    case "FineReminder":
                        final ChatListEntity FineReminder = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(FineReminder)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_FINE_TEXT, FineReminder.msg, Gc.CHANNELID_FINE, R.drawable.ic_alert);
                        break;
                    case "SchoolGallery":
                        final ChatListEntity SchoolGallery = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(SchoolGallery)).start();
                        NotificationHelper.showNotificationBitmap(SchoolGallery.msg
                                , getBitmapfromUrl(SchoolGallery.imagepath), Gc.CHANNELID_GALLERY_ADD_TEXT, this, Gc.CHANNELID_GALLERY_ADD);
                        break;
                    case "HomeWorkEvaluation":
                        final ChatListEntity HomeWorkEvaluation = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(HomeWorkEvaluation)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_HOMEWORK_EVALUATION_TEXT, HomeWorkEvaluation.msg, Gc.CHANNELID_HOMEWORK, R.mipmap.ic_launcher);
                        break;
                    case "HomeWorkAdd":
                        final ChatListEntity HomeWorkAdd = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(HomeWorkAdd)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_HOMEWORK_TEXT, HomeWorkAdd.msg, Gc.CHANNELID_HOMEWORK, R.mipmap.ic_launcher);
                        break;
                    case "HomeWorkUpdate":
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_HOMEWORK_TEXT, data_msg, Gc.CHANNELID_HOMEWORK, R.mipmap.ic_launcher);
                        break;
                    case "deleteHomework":
                        try {
                            JSONObject data = new JSONObject(data_msg);
                            final String hdate = data.optString("dateofhomework");
                            final String section = data.optString("sectionid");
                            final String subject = data.optString("subjectid");
                            new Thread(() -> mDb.assignmentModel().deleteSingleAssignment(section, hdate, subject)).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "Notification":
                        //  Log.e("data_msg", data_msg);
                        try {
                            final JSONObject data = new JSONObject(data_msg);
                            final NoticeBoardEntity notice = new Gson().fromJson(data.optString("NotificationDetails"), NoticeBoardEntity.class);
                            new Thread(() -> mDb.noticeBoardModel().insertNoticeSingle(notice)).start();
                            if (Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase().equals("paid")) {
                                sendNoticeBoardNotificationResponseToServer(notice);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        final ChatListEntity Notification = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(Notification)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_NOTICEBOARD_TEXT, Notification.msg, Gc.CHANNELID_NOTICEBOARD, R.mipmap.ic_launcher);
                        break;
                    case "OnlineExam":
                        final ChatListEntity onlineExam = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(onlineExam)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_EXAM_TEXT, onlineExam.msg, Gc.CHANNELID_EXAM, R.drawable.ic_exam_result);
                        break;
                    case "permission":
                        Log.i("permission reset", Objects.requireNonNull(data_msg));
                        sp.edit().putString(Gc.RELOADPERMISSIONS, Gc.TRUE).apply();
                        break;
                    case "reloadDemo":
                        Log.i("Demo reset", Objects.requireNonNull(data_msg));
                        sp.edit().putString(Gc.RELOADDEMO, Gc.TRUE).apply();
                        break;
                    case "NewUserJoined":
                        final ChatListEntity NewUserJoined = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(NewUserJoined)).start();
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_OTHER_INFO_TEXT, NewUserJoined.msg, Gc.CHANNELID_OTHER_INFO, R.mipmap.ic_launcher);
                        sp.edit().putString(Gc.RELOADPERMISSIONS, Gc.TRUE).apply();
                        break;
                    case "ImportantMessage":
                        final ChatListEntity ImportantMessage = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(ImportantMessage)).start();

                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_SCHOOL_MESSAGE_TEXT, ImportantMessage.msg, Gc.CHANNELID_SCHOOL_MESSAGE, R.mipmap.ic_launcher);
                        break;
                    case "MySchool Messages":
                        Log.e("senderid", data_msg);
                        final ChatListEntity chat = new Gson().fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(chat)).start();
                        sendChatResponseMessage(chat);
                        SharedPreferences pref = getSharedPreferences(SCHOOLPREF, 0); // 0 - for private mode
                        if (!(pref.getBoolean(Gc.CHATWINDOWACTIVE, false))) {
                            NotificationHelper.showNotificationSimple(this, ("From:" + chat.withName), chat.msg, Gc.CHANNELID_121_MESSAGE, R.drawable.ic_message);
                        }
                        break;
                    case "deleteMessages":
                        try {
                            JSONObject deleteMessages = new JSONObject(data_msg);
                            final JSONArray ids = deleteMessages.getJSONArray("msgId");
                            final ArrayList<String> msgId = new ArrayList<String>();
                            for (int i = 0; i < ids.length(); i++) {
                                msgId.add(ids.get(i).toString());
                            }
                            String msg = "This message was deleted";
                            List<ChatListEntity> chatDetails = chatDb.chatListModel().getChatWithMsgIds(msgId);
                            if (chatDetails.size() > 0) {
                                for (ChatListEntity chatObj : chatDetails) {
                                    if (null != chatObj.localPath) {
                                        File fdelete = new File(chatObj.localPath);
                                        if (fdelete.exists()) {
                                            if (fdelete.delete()) {
                                                System.out.println("file Deleted :");
                                            } else {
                                                System.out.println("file not Deleted :");
                                            }
                                        }
                                    }
                                }
                            }
                            new Thread(() -> chatDb.chatListModel().chatDelete(msg, msgId)).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "deleteNotification":
                        Log.e("notifceboard delete", data_msg);
                        try {
                            final JSONObject data = new JSONObject(data_msg);
                            new Thread(() -> mDb.noticeBoardModel().deleteSingleNotice(data.optString("Notify_id"))).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "ResponseOfMessage":
                        try {
                            JSONObject data = new JSONObject(data_msg);

                            final String id = data.optString("msgId");
                            final String receivedate = data.optString("enteredDate");
                            Log.e("msgId", id);
                            Log.e("enteredDate", data.optString("enteredDate"));
                            Log.e("senderType", data.optString("senderType"));
                            Log.e("senderId", data.optString("senderId"));
                            new Thread(() -> chatDb.chatListModel().updateReceivedStatus(id, receivedate)).start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "studentOfTheMonth":
                        String name = "studentOfTheMonth.jpg";
                        //String deleteImageName = sp.getString(Config.STUDENT_OF_THE_MONTH_PATH, "");
                        sp.edit().putString(Config.STUDENT_OF_THE_MONTH_MESSAGE, remoteMessage.getData().get("message"))
                                .putString(Config.STUDENT_OF_THE_MONTH_IMAGE_URL, remoteMessage.getData().get("image"))
                                .putString(Config.STUDENT_OF_THE_MONTH_PATH, name)
                                .apply();
                        final ChatListEntity studentOfTheMonth = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(studentOfTheMonth)).start();

                        NotificationHelper.showNotificationBitmap(studentOfTheMonth.msg
                                , getBitmapfromUrl(studentOfTheMonth.imagepath), "Spot Light", this, Gc.CHANNELID_GALLERY_ADD);
                        break;
                    case "SurveyNotification":
                        final ChatListEntity SurveyNotification = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(SurveyNotification)).start();

                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_SURVEY_TEXT, SurveyNotification.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);

                        break;
                    case "StaffLeaveApplied":
                        final ChatListEntity StaffLeaveApplied = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(StaffLeaveApplied)).start();
                        NotificationHelper.showNotificationSimple(this, "Staff Leave Applied", StaffLeaveApplied.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);
                        break;
                    case "LeaveStatus":
                        final ChatListEntity LeaveStatus = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(LeaveStatus)).start();
                        NotificationHelper.showNotificationSimple(this, "Leave Status", LeaveStatus.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);
                        break;
                    case "StudentLeaveApplied":
                        final ChatListEntity StudentLeaveApplied = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(StudentLeaveApplied)).start();
                        NotificationHelper.showNotificationSimple(this, "Student Leave Applied", StudentLeaveApplied.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);
                        break;
                    case "HolidayNotification":
                        final ChatListEntity HolidayNotification = new Gson()
                                .fromJson(data_msg, ChatListEntity.class);
                        new Thread(() -> chatDb.chatListModel().insertChat(HolidayNotification)).start();
                        NotificationHelper.showNotificationSimple(this, "Holiday Information", HolidayNotification.msg, Gc.CHANNELID_SURVEY, R.drawable.ic_homework);
                        break;
                    default:
                        NotificationHelper.showNotificationSimple(this, Gc.CHANNELID_OTHER_INFO_TEXT, data_msg, Gc.CHANNELID_OTHER_INFO, R.mipmap.ic_launcher);
                        break;
                }
            }
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);

            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    public static void setMyNotificationList(Context context, String heading) {
        SharedPreferences sp = Prefs.with(context).getSharedPreferences();
        NotificationEvent obj = (new Gson()).fromJson(sp.getString(Config.MY_NOTIFICATION, ""), new TypeToken<NotificationEvent>() {
        }.getType());
        ArrayList<NotificationEvent> notificationList;
        if (obj == null) {
            notificationList = new ArrayList<>();
            obj = new NotificationEvent();
        } else
            notificationList = obj.getResult();
        if (null == notificationList) notificationList = new ArrayList<>();
        notificationList.add(new NotificationEvent(0,
                heading,
                "",
                "Active",
                Config.currentDate.format(new Date()),
                false
        ));
        obj.setResult(notificationList);
        sp.edit().putString(Config.MY_NOTIFICATION, (new Gson()).toJson(obj)).commit();
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(Config.NEW_MY_NOTIFICATION));
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(context.NOTIFICATION_SERVICE);
        //  NotificationManager notificationManager = (NotificationManager) MyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static void sendMsgResponseToServer(Context context, final String app_msg_s_no,
                                               String senderid, String senderType) throws JSONException {
        SharedPreferences sp = Prefs.with(context).getSharedPreferences();

        String db_name = sp.getString("organization_name", "Not Found");

        final JSONObject param = new JSONObject();
        param.put("DB_Name", db_name);
        param.put("app_msg_s_no", app_msg_s_no);
        param.put("status", "sent");
        param.put("senderid", senderid);
        param.put("senderType", senderType);

        //  RequestQueue rqueue = Volley.newRequestQueue(this);
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found") +
                "ChatDetails.php?action=responseMessage";
        Log.d("url", url);
        Log.d("param", param.toString());
        JsonObjectRequest string_req = new JsonObjectRequest(Request.Method.POST,
                url, param
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject s) {
                Log.d("app_msg_s_no_response", s.toString());
                Log.e("app_msg_s_no_response", s.toString());
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("volleyError", volleyError.toString());
                Log.e("volleyError", volleyError.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

        };

        int socketTimeout = 0;
        string_req.setRetryPolicy(new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(context).addToRequestQueue(string_req);


    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    void sendNoticeBoardNotificationResponseToServer(NoticeBoardEntity notice) {

        final JSONObject param = new JSONObject();
        try {
            param.put("Notify_id", notice.Notify_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "noticeBoard/notificationResponse";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), job -> {
            String code = job.optString("code");
            if (Gc.APIRESPONSE200.equals(code)) {
                Log.i("Message Response", "SuccessFul");
            } else {
                Log.e("Message Response", "Failed");
            }
        }, error -> Log.e("API Message response", "Failed")) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void sendChatResponseMessage(ChatListEntity chat) {

        String s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date());


        final JSONObject param1 = new JSONObject();
        try {
            param1.put("msgId", chat.msgId);
            param1.put("enteredDate", s);
            param1.put("withType", chat.senderType);
            param1.put("withId", chat.senderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JSONArray param = new JSONArray();
        param.put(param1);
        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "chat/chatResponce";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:

                        Log.i("Message Response", "SuccessFul");

                        break;


                    default:
                        Log.e("Message Response", "Failed");

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("API Message response", "Failed");

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    void sendTokenToServer(final String s) throws JSONException {
        final JSONObject param = new JSONObject();

        param.put("fcmToken", s);

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "login/updateFCMToken";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                String code = job.optString("code");

                switch (code) {
                    case "200":
                        Log.i("TokenSendSuccess", s);
                        break;

                    default:
                        Log.e("TokenSendFail", s);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TokenSendError", s);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

}