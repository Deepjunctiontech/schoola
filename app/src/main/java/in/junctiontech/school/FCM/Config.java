package in.junctiontech.school.FCM;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Environment;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.FirstScreenActivity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

/**
 * Created by JAYDEVI BHADE on 5/12/2016.
 */
public class Config extends Application {

    public static final int PERMISSION_IMEI_LOCATION_CODE = 9984;
    public static final int CROP_IMAGE_REQUEST_CODE = 546;
    public static final int SCHOOL_INFO_CODE = 345;
    public static final int SCHOOL_IMAGE = 4544;
    public static final int LOCATION = 334;
    public static final int DEMO_SCHOOL_REQUEST_CODE = 177;
    public static final int IMAGE_LOGO_CODE = 456;
    public static final int ADD_PROFILE_IMAGE = 656;
    public static final int AddSessionActive = 898;
    public static final int NewNotificationAddedCode = 567;
    public static final int New_Notification_Click_Code = 987;




    public static final String CREATE_CLASS_SECTION_PERMISSION = "CREATE_CLASS_SECTION_PERMISSION";
    public static final String CREATE_STAFF_PERMISSION = "CREATE_STAFF_PERMISSION";
    public static final String CREATE_STUDENT_PERMISSION = "CREATE_STUDENT_PERMISSION";
    public static final String CREATE_SUBJECT_PERMISSION = "CREATE_SUBJECT_PERMISSION";
    public static final String CREATE_FEE_TYPE_PERMISSION = "CREATE_FEE_TYPE_PERMISSION";
    public static final String CLASS_FEE_PERMISSION = "CLASS_FEE_PERMISSION";
    public static final String CREATE_FEE_ACCOUNT_PERMISSION = "CREATE_FEE_ACCOUNT_PERMISSION";
    public static final String TRANSPORT_FEE_PERMISSION = "TRANSPORT_FEE_PERMISSION";
    public static final String FEE_PAYMENT_PERMISSION = "FEE_PAYMENT_PERMISSION";
    public static final String CREATE_GRADES_PERMISSION = "CREATE_GRADES_PERMISSION";
    public static final String CREATE_EXAM_TYPE_PERMISSION = "CREATE_EXAM_TYPE_PERMISSION";
    public static final String GRADE_MARK_SETUP_PERMISSION = "GRADE_MARK_SETUP_PERMISSION";

    public static final String REMEMBER_ME = "REMEMBER_ME";
    public static final String SESSION_START_DATE = "SESSION_START_DATE";
    public static final String applicationVersionNameValue = "V4/";
    public static final String ADMIN_DATA_FETCHED = "ADMIN_DATA_FETCHED";
    public static final String LAST_USED_SECTION_ID = "LAST_USED_SECTION_ID";
    public static final String LAST_USED_CLASS_ID = "LAST_USED_CLASS_ID";
    public static final String LAST_USED_SUBJECT_ID = "LAST_USED_SUBJECT_ID";
    public static final String FETCH_STATE = "http://testutilities.zeroerp.com/Fetchstate.php?data=";
    public static final String FETCH_CITY = "http://testutilities.zeroerp.com/Fetchcity.php?data=";
    public static final String COUNTRY_CODE = "COUNTRY_CODE";
    public static final String STUDENT_COUNT = "STUDENT_COUNT";
    public static final String APP_COLOR_PREF = "APP_COLOR_PREF";
    public static final String EVALUATION_HINT_COUNTER = "EVALUATION_HINT_COUNTER";
    public static final String MY_NOTIFICATION = "MY_NOTIFICATION";
    public static final String NEW_MY_NOTIFICATION = "NEW_MY_NOTIFICATION";
    public static final String NOTIFICATION_SOUND = "NOTIFICATION_SOUND";
    public static final String CHAT_NOTIFICATION_SOUND = "CHAT_NOTIFICATION_SOUND";
    public static final String SCHOOL_KEY = "SCHOOL_KEY";
    public static final String DATA_LOADING = "DATA_LOADING";
    public static final String DATA_LOADING_COMPLETE = "DATA_LOADING_COMPLETE";
    public static final String PERMISSION_FETCHED = "PERMISSION_FETCHED";
    public static final String DECODED_ORGANIZATION_KEY = "DECODED_ORGANIZATION_KEY";
    public static final String SCHOOL_IMAGE_UPDATED = "SCHOOL_IMAGE_UPDATED";
    public static final String CHECK_CLASS_NAME = "CHECK_CLASS_NAME";
    public static final String CHECK_SECTION_NAME = "CHECK_SECTION_NAME";
    public static final String CHECK_SUBJECT_NAME = "CHECK_SUBJECT_NAME";
    public static final String CREATE_USER_SEND_SMS = "CREATE_USER_SEND_SMS";
    public static final String CREATE_USER_SEND_SMS_NEVER_ASK_AGAIN = "CREATE_USER_SEND_SMS_NEVER_ASK_AGAIN";
    public static final String SMS_ORGANIZATION_NAME = "SMS_ORGANIZATION_NAME";
    public static final String CREATE_STUDENT_SEND_SMS = "CREATE_STUDENT_SEND_SMS";
    public static final String CREATE_STUDENT_SEND_SMS_NEVER_ASK_AGAIN = "CREATE_STUDENT_SEND_SMS_NEVER_ASK_AGAIN";
    public static final String SESSION_DATA = "Session_Data";
    public static final String SESSION_SUBSCRIPTION_TYPE = "sessionSubscriptionType";
    public static final String LOGO_URL = "LogoURL";
    public static final String LOGO_UPDATED = "LOGO_UPDATED";
    public static final String ADDED_NEW_NOTIFICATION = "ADDED_NEW_NOTIFICATION";
    public static final String PROFILE_URL = "PROFILE_URL";

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String New_Message = "newMessage";
    public static final String STATUSOFMSG = "status";
    public static final String SESSION_LIST = "session_list";
    public static final String CHECKMAILID = "checkMailId";
    public static final String GENERAL_SETTING = "general_setting";
    public static final String SESSION = "session";
    public static final String isSessionSelected = "session_selected";
    public static final String mobile_number = "mobile_number";
    public static final String FOLDER_NAME = "MySchool";
    public static final String SCHOOL_IMAGE_FOLDER_NAME = "MySchool Images";
    public static final String INTERNET_AVAILABLE = "internetAvailable";
    public static final String SWAP_UPDATE_STUDENT = "swap_update_student";


    public static final String NewNotification = "NewNotification";
    public static final String chatMessage = "chat";
    public static final String responseChatOrNot = "responseChat";
    public static final String chatDataObj = "chatDataObj";
    //  public static String groupIsAvailable="groupIsAvailable";
    public static final String chatListObj = "chatListObj";
    public static final String ProfileData = "profileData";


    static public final SimpleDateFormat simple = new SimpleDateFormat("MMMM dd yyyy hh:mm a", Locale.ENGLISH);
    static public final SimpleDateFormat feedbackDateFormate = new SimpleDateFormat("MMM dd yyyy hh:mm a", Locale.ENGLISH);
    static public final SimpleDateFormat msgDateFormat = new SimpleDateFormat("MMM dd yyyy hh:mm a M", Locale.ENGLISH);

    public static final SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    static public final SimpleDateFormat currentDate = new SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
    public static final String isFromNotification = "isFromNotification";
    public static final SimpleDateFormat monthYear = new SimpleDateFormat("MMMM", Locale.ENGLISH);
    public static final SimpleDateFormat monthYear_with_dash = new SimpleDateFormat("MMMM-yyyy", Locale.ENGLISH);
    public static final String updateNotification = "updateNotification";
    public static final String applicationVersionName = "applicationVersionName";
    public static final String APP_COLOR = "APP_COLOR";


    public static final String[] weekDayEng = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    public static final int[][] stateOffOn = new int[][]{
            new int[]{-android.R.attr.state_checked}, // unchecked
            new int[]{android.R.attr.state_checked}  // checked
    };
    public static final int[] trackColors = new int[]{
            Color.GRAY,
            Color.LTGRAY,
    };


    public static final String GPS_DATA_URL = "http://apigps.zeroerp.in/V1/GpsData.php";



    /*  --------- 192.168.1.171 -----------------*/
//    public static String hostName = "http://192.168.1.171/cpanel/";
//    public static final String hostNameUrl = Config.hostName + "Login/GetHostName";
//    public static final String adminHostNameUrl = Config.hostName + "Login/GetHostNameApiSchoolerp";
//    public static final String convertPassword = "http://192.168.1.161/apischoolerp/";
//    public static final String dbGetUrl = "http://192.168.1.171/apicpanel/";
//    public static final String sendEmail = Config.hostName + "Login/sendOTP";
//    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
//    public static final String loginCpanel = "http://192.168.1.161/schoolerp/login/login_userCpanel";
//    public static final String tokenSaving = "http://192.168.1.161/schoolerp/android/gcm.php?action=saveToken";
//    public static final String CONTACT_SAVING_URL =  "http://192.168.1.151/orangegps.com/login/contactList";
//    public static final String ORGANIZATION_ID_CHECK =  "http://192.168.1.171/apicpanel/";
//    public static final String OTHER_REGISTRATION_URL = "http://192.168.1.151/contacts/EnquiryApi.php";
//    public static final String ADMISSION_NO_CHECK = "http://192.168.1.151/apischoolerp/AdmissionApi.php?";


/*public static final String NewHostNameUrl = "http://testapicpanel.zeroerp.com/HostApi.php?";
*    public static final String NewHostNameUrl = "http://testapicpanel.zeroerp.com/HostApi.php?";
* 1*/


    /***************** New Test Server  **********************/

/*     public static final String ORGANIZATION_ID_CHECK = "http://192.168.1.181/apicpanel/";
    public static String GET_HOST_NAME = Config.ORGANIZATION_ID_CHECK + "HostApi.php?";
    public static String hostName = "http://192.168.1.181/cpanel/";
    public static final String sendEmail = Config.hostName + "Login/sendOTP";
    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
    public static final String resetPasswordSendOtp = Config.hostName + "ResetPasswordPanel/resetPassword";
    public static final String resetPasswordVerifyOtp = Config.hostName + "ResetPasswordPanel/updatePassword";*/

    /******************************************************************/

    /*****************  Feb 2017 production **********************/

    public static final String ORGANIZATION_ID_CHECK = "http://testapicpanel.zeroerp.com/";
    public static final String GET_HOST_NAME = Config.ORGANIZATION_ID_CHECK + "HostApi.php?";
    public static final String hostName = "http://testcpanel.zeroerp.com/";
    public static final String sendEmail = Config.hostName + "Login/sendOTP";
    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
    public static final String resetPasswordSendOtp = Config.hostName + "ResetPasswordPanel/resetPassword";
    public static final String resetPasswordVerifyOtp = Config.hostName + "ResetPasswordPanel/updatePassword";

    /******************************************************************/


    /* for TEST Feb 2017 */

  /*  public static final String  GET_HOST_NAME = "http://testapicpanel.zeroerp.com/HostApi.php?";
    public static final String ORGANIZATION_ID_CHECK = "http://testapicpanel.zeroerp.com/";
    public static final String hostName = "http://testcpanel.zeroerp.com/";
    public static final String sendEmail = Config.hostName + "Login/sendOTP";
    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
    public static final String resetPasswordSendOtp = Config.hostName + "ResetPasswordPanel/resetPassword";
    public static final String resetPasswordVerifyOtp = Config.hostName + "ResetPasswordPanel/updatePassword";*/


    /* *******    Fetch country code and list         *********/
    public static final String FETCH_COUNTRY = "http://testutilities.zeroerp.com/Fetchcountry.php";
    public static final String FETCH_COUNTRY_CODE = "http://testutilities.zeroerp.com/Fetchcountrycode.php";


    /*    // public static final String loginCpanel = "http://192.168.1.161/apischoolerp/LoginApi.php";
    //http://testapicpanel.zeroerp.com//HostApi.php?data={"applicationName":"School","organizationKey":"DEMOSCHOOL"}
    // * public static final String NewHostNameUrl = "http://192.168.1.171/apicpanel/HostApi.php?";
    // public static final String hostNameUrl = Config.hostName + "Login/GetHostName";
    /*//* public static final String adminHostNameUrl = Config.hostName + "Login/GetHostNameApiSchoolerp";
    // public static final String convertPassword = "http://192.168.1.161/apischoolerp/";
    //  public static final String dbGetUrl = "http://192.168.1.171/apicpanel/";
    //   public static final String sendEmail = Config.hostName + "Login/sendOTP";
    //  public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
    //   public static final String loginCpanel = "http://192.168.1.161/apischoolerp/LoginApi.php";
    //  public static final String loginCpanel = "http://192.168.1.161/schoolerp/login/login_userCpanel";


    // public static final String tokenSaving = "http://192.168.1.161/schoolerp/android/gcm.php?action=saveToken";
    */
       public static final String CONTACT_SAVING_URL = "http://192.168.1.151/orangegps.com/login/contactList";
    // public static final String ORGANIZATION_ID_CHECK =  "http://192.168.1.171/apicpanel/";
      public static final String OTHER_REGISTRATION_URL = "http://192.168.1.151/contacts/EnquiryApi.php";
    /*
    //  public static final String ADMISSION_NO_CHECK = "http://192.168.1.151/apischoolerp/AdmissionApi.php?";


    */
/*  --------- 192.168.1.151 -----------------*//*

    //  public static String hostName = "http://192.168.1.151/cpanel/";
//    public static String hostName = "http://192.168.1.171/cpanel/";
//    public static final String hostNameUrl = Config.hostName + "Login/GetHostName";
//    public static final String adminHostNameUrl = Config.hostName + "Login/GetHostNameApiSchoolerp";
//    public static final String dbGetUrl = "http://192.168.1.151/apischoolerp/";
//    public static final String sendEmail = Config.hostName + "Login/sendOTP";
//    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
//    public static final String loginCpanel = "http://192.168.1.151/schoolerp/login/login_userCpanel";
//    public static final String tokenSaving = "http://192.168.1.151/schoolerp/android/gcm.php?action=saveToken";
//    public static final String CONTACT_SAVING_URL =  "http://192.168.1.151/orangegps.com/login/contactList";
//    public static final String ORGANIZATION_ID_CHECK =  "http://192.168.1.151/apicpanel/";
//    public static final String OTHER_REGISTRATION_URL = "http://192.168.1.151/contacts/EnquiryApi.php";
*/

    /*-----------    Production --------------*/
//    public static final String hostName = "http://cpanel.zeroerp.com/";
//    public static final String hostNameUrl = Config.hostName + "Login/GetHostName";
//    public static final String adminHostNameUrl = Config.hostName + "Login/GetHostNameApiSchoolerp";
//    public static final String convertPassword = "http://apischoolerp.zeroerp.com/";
//    public static final String dbGetUrl = "http://apicpanel.zeroerp.com/";
//    public static final String sendEmail = Config.hostName + "Login/sendOTP";
//    public static final String verifyOtp = Config.hostName + "Login/verifyEmailOTP";
//    public static final String loginCpanel = "http://education.zeroerp.com/login/login_userCpanel";
//    public static final String tokenSaving = "http://education.zeroerp.com/android/gcm.php?action=saveToken";
//    public static final String CONTACT_SAVING_URL =  "http://orangegps.com/login/contactList";
//    public static final String ORGANIZATION_ID_CHECK =  "http://apicpanel.zeroerp.com/";
//    public static final String OTHER_REGISTRATION_URL = "http://contacts.orangegps.com/EnquiryApi.php";
//    public static final String ADMISSION_NO_CHECK = "http://apischoolerp.zeroerp.com/AdmissionApi.php?";
//

    /***************    google short Url *********************/
   /* https://play.google.com/store/apps/details?id=in.junctiontech.school */

    public static final String SCHOOL_PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=";/*play.google.com/store/apps/details*/
    public static final String SCHOOL_PRICING_LINK = "https://goo.gl/i6QQaT";/*www.zeroerp.com/zeroerp-school-management-software-pricing*/
    public static final String SCHOOL_BLOG_LINK = "https://goo.gl/OVLQvM";/*www.zeroerp.com/school-management-software-blog*/
    public static final String SCHOOL_HELP_LINK = "https://goo.gl/3yLp0S";/*www.zeroerp.com/school-assistant*/
    public static final String SCHOOL_HELP_FAQ = "https://goo.gl/ShVa62";/*www.zeroerp.com/zeroerp-school-management-faq*/
    public static final String SCHOOL_PARENT_HELP_LINK = "https://goo.gl/K4JCHi";/*https://www.zeroerp.com/parents-help/*/
    public static final String SCHOOL_STUDENT_HELP_LINK = "https://goo.gl/Lf3KcG";/*https://www.zeroerp.com/students-help/*/
    public static final String SCHOOL_STAFF_HELP_LINK = "https://goo.gl/iGTWJt";/*https://www.zeroerp.com/teachers-help/*/

    public static final String ZEROERP_FACEBOOK_LINK = "https://www.facebook.com/zeroerp";
    public static final String ZEROERP_YOUTUBE_LINK = "https://www.youtube.com/channel/UCTPN4fWdrvpiXh6GoCOL1-g";
    public static final String ZEROERP_LINKDIN_LINK = "https://www.linkedin.com/organization/410760";
    public static final String ZEROERP_Twiter_LINK = "https://twitter.com/bestschoolerp";
    public static final String ZEROERP_G_PLUS_LINK = "https://plus.google.com/116835813394819081537";
    public static final String ZEROERP_BLOGER_LINK = "https://studentteacherapp.blogspot.com";
    public static final String ZEROERP_TUMBLER_LINK = "https://zeroerp-school-software.tumblr.com/";
    public static final String STUDENT_OF_THE_MONTH_MESSAGE = "STUDENT_OF_THE_MONTH_MESSAGE";
    public static final String STUDENT_OF_THE_MONTH_IMAGE_URL = "STUDENT_OF_THE_MONTH_IMAGE_URL";
    public static final String STUDENT_OF_THE_MONTH_PATH = "STUDENT_OF_THE_MONTH_PATH";

    /*************** ********** *********************/


    //  public static String blockCharacterSet = "£,¥~`!@#$%^&*()+=-/;:\\\"\\'.{}[]<>^?,_  ";
    public static String blockCharacterSet = " ";

    public static InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {


            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
         /*   for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || source.toString().matches("[\\x00-\\x7F]+")) {
                    return "";
                }
            }*/
            return null;
        }
    };


    /* set Number Of Message Counter*/
    private int numberOfMessageCounter = 0;

    public static int getAppColor(Activity context, boolean isFromActivity) {
        SharedPreferences spAppColor = context.getSharedPreferences(Config.APP_COLOR_PREF, Context.MODE_PRIVATE);

        int color = spAppColor.getInt(Config.APP_COLOR, Color.rgb(230, 74, 25));
        if (isFromActivity && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setNavigationBarColor(color);
            window.setStatusBarColor(color);
        }

        return color;

    }

    public int getNumberOfMessageCounter() {
        return this.numberOfMessageCounter;
    }

    public void setNumberOfMessageCounter(int d) {
        this.numberOfMessageCounter = d;
        Log.e("SetNumber", this.numberOfMessageCounter + " " + d);
    }

    public void increaseNumberOfMessageCounter() {
        this.numberOfMessageCounter = this.numberOfMessageCounter + 1;
        Log.e("Increment", this.numberOfMessageCounter + " ");
    }


    public void removeAllChatList() {

    }

    public void decreaseNumberOfMessageCounter(int d) {
        this.numberOfMessageCounter = this.numberOfMessageCounter - d;
        Log.e("Decrement", this.numberOfMessageCounter + " " + d);
    }
    /* set Number Of Chat Group List */

    private ArrayList<Integer> numberOfGroupIdsList = new ArrayList<>();

    public ArrayList<Integer> getGroupIdsList() {
        return this.numberOfGroupIdsList;
    }

    public void addGroupIdChatList(int groupId) {
        if (this.numberOfGroupIdsList.contains(groupId)) ;
        else
            this.numberOfGroupIdsList.add(groupId);
    }

    public void removeGroupIdChatList(int groupId) {
        if (this.numberOfGroupIdsList.contains(groupId)) {
            this.numberOfGroupIdsList.remove(this.numberOfGroupIdsList.indexOf(groupId));

            Log.e("grouID", groupId + " Removed");
        }
        Log.e("grouID", groupId + " Not Removed");
    }

    public void decreaseNumberOfMessageCounter() {
        this.numberOfGroupIdsList.clear();
    }


/*   --------------  Registration ---------------*/


    //   public static String hostName= "http://apischoolerp.zeroerp.com/";


    //  String url = hostName+"Login/sendOTP";
    //  String urlOtp = hostName+"Login/verifyEmailOTP";


    //  String searchOtp = hostName + "apischoolerp/Admin_panel/searchNumber";
    //   String verifyControl = hostName+"apischoolerp/Admin_panel/verifyControl";


    public static boolean checkInternet(Context context) {
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        boolean aa = false;

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet

//            Toast.makeText(context, "Internet Connected ", Toast.LENGTH_LONG).show();
            aa = true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            //  Toast.makeText(context, "Internet Not Connected ", Toast.LENGTH_LONG).show();
            aa = false;
        }
        return aa;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void hideKeyboard(Context context) {

        try {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            View view = ((Activity) context).getCurrentFocus();
            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void myToast(Context context, String tv_text, String tv_toast, int gravity) {


        View myView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.my_custom_toast, null);
        TextView tv_drawer_open_close =  myView.findViewById(R.id.tv_of_toast);
        ImageView iv_of_toast =  myView.findViewById(R.id.iv_of_toast);
        iv_of_toast.setImageDrawable(context.getDrawable(R.mipmap.ic_launcher));


        if (tv_text.equalsIgnoreCase("ic_attendance"))
            iv_of_toast.setImageResource(R.drawable.ic_attendance);
        else if (tv_text.equalsIgnoreCase("homework"))
            iv_of_toast.setImageResource(R.drawable.assignment);
        else if (tv_text.equalsIgnoreCase("result"))
            iv_of_toast.setImageResource(R.drawable.ic_homework);
        else iv_of_toast.setImageResource(R.mipmap.ic_launcher);

        tv_drawer_open_close.setText(tv_toast);

        Toast toast = new Toast(context);

        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(myView);
        toast.setGravity(gravity, 0, 0);
        toast.show();

    }


    public static String getTime() {
        Calendar calendar = Calendar.getInstance();
        Log.i("TIME", "Utility Time");
        //return (calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
        return time.format(calendar.getTime());
    }


    public static void showToast(Context context, String err) {
        Toast.makeText(context, err, Toast.LENGTH_SHORT).show();
    }


    public static void getCountryCode(final Context context) {

        String url = Config.FETCH_COUNTRY_CODE;

        Log.e("COUNTRY_CODE", url);

        StringRequest request = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("COUNTRY_CODE", s + "");

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("Code").equalsIgnoreCase("200")) {

                                (context.getApplicationContext().
                                        getSharedPreferences(Config.COUNTRY_CODE, Context.MODE_PRIVATE))
                                        .edit().putString(Config.COUNTRY_CODE, s)
                                        .putBoolean(Config.FETCH_COUNTRY_CODE, false).commit();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());

                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {


                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(context).addToRequestQueue(request);

    }
   /* public static  void setStatusBarColor(Activity context , int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window =context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }*/

    public static void responseVolleyHandlerAlert(final Activity activity, String code, String msg) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity,
                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(activity.getString(R.string.ok), null);

        AlertDialog.Builder alertLogout = new AlertDialog.Builder(activity,
                AlertDialog.THEME_TRADITIONAL);
        alertLogout.setIcon(activity.getResources().getDrawable(R.drawable.ic_alert));
        alertLogout.setTitle(activity.getString(R.string.information));
        alertLogout.setMessage(msg + "\n" +
                activity.getString(R.string.contact_message_to_junction_tech));
        alertLogout.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                                            /* ***** clear SharedPreference*/
                Prefs.with(activity).getSharedPreferences().edit().clear().apply();


                /********  delete MySchool Images Folder  *******/
                File fileOrDirectory = new File(Environment.getExternalStorageDirectory().toString() +
                        "/" + Config.FOLDER_NAME);

                AdminNavigationDrawerNew.deleteRecursive(fileOrDirectory);
                /*****   delete Database   ****/
                DbHandler.getInstance(activity).deleteDatabase();

                Intent intent = new Intent(activity, FirstScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
                activity.overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        alertLogout.setCancelable(false);
        alertLogout.show();


    }

    public static void responseVolleyErrorHandler(Activity activity, VolleyError volleyError, View snackbar) {
        Log.e("VolleyError", volleyError.toString());
        String msg = "Error !";
        if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
            msg = activity.getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.internet_not_available_please_check_your_internet_connectivity);

        } else if (volleyError instanceof AuthFailureError) {
            msg = "AuthFailureError!";
        } else if (volleyError instanceof ServerError) {
            msg = "ServerError!";
            if (volleyError.networkResponse.statusCode == 302)
                msg = "Something wrong with internet connection";
        } else if (volleyError instanceof NetworkError) {
            msg = "NetworkError!";
        } else if (volleyError instanceof ParseError) {
            msg = "ParseError!";
        }

        Snackbar.make(snackbar, msg, Snackbar.LENGTH_LONG).show();
    }


    public static void responseVolleyErrorToast(Activity activity, VolleyError volleyError) {
        Log.e("VolleyError", volleyError.toString());
        String msg = "Error !";
        if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
            msg = activity.getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.internet_not_available_please_check_your_internet_connectivity);

        } else if (volleyError instanceof AuthFailureError) {
            msg = "AuthFailureError!";
        } else if (volleyError instanceof ServerError) {
            msg = "ServerError!";
            if (volleyError.networkResponse.statusCode == 302)
                msg = "Something wrong with internet connection";
        } else if (volleyError instanceof NetworkError) {
            msg = "NetworkError!";
        } else if (volleyError instanceof ParseError) {
            msg = "ParseError!";
        }

        Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }


    public static void responseSnackBarHandler(String msg, View snackbar) {

        Snackbar.make(snackbar, msg, Snackbar.LENGTH_LONG).setAction(R.string.retry, null).show();

    }

    public static void responseSnackBarHandler(String msg, View snackbar, int color) {
        Snackbar snackbarObj = Snackbar.make(snackbar, msg,
                Snackbar.LENGTH_SHORT).setAction("", null);
        snackbarObj.getView().setBackgroundColor(color);
        snackbarObj.show();

    }

    public static void adsInitialize(String bannerId,Context context,View view){
        Log.i("Ads","Disabled");
//        AdView mAdView = new AdView(context);
//        mAdView.setAdSize(AdSize.BANNER);
//        mAdView.setAdUnitId(bannerId);
//       // mAdView.setAdUnitId(Gc.TESTBANNERID);
//        view.setVisibility(View.VISIBLE);
//        ((RelativeLayout)view).addView(mAdView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
    }

    public static void setSwitchCompactColor(SwitchCompat switchButton, int appColor) {
        int[] thumbColors = new int[]{
                Color.LTGRAY,
                appColor,
        };
        DrawableCompat.setTintList(DrawableCompat.wrap(switchButton.getThumbDrawable()), new ColorStateList(Config.stateOffOn, thumbColors));
        DrawableCompat.setTintList(DrawableCompat.wrap(switchButton.getTrackDrawable()), new ColorStateList(Config.stateOffOn, trackColors));
    }

    public static ColorStateList getCheckBoxColor(int appColor) {
        return new ColorStateList(Config.stateOffOn
                ,
                new int[]{
                        Color.LTGRAY,
                        appColor
                }
        );

    }

}
