package in.junctiontech.school;


import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by JAYDEVI BHADE on 9/7/2016.
 */
public class Prefs {


    private static final String PREFS_NAME = "myPreference";
    private static Prefs instance;
    private final SharedPreferences sharedPreferences;
    private static String timer = "";

    public Prefs(Context context) {

        sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static Prefs with(Context context) {
        if (instance == null) {
            instance = new Prefs(context);
        }

        return instance;
    }


    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;

    }

//    public static void setTestTimer(String timer) {
//        Prefs.timer = timer;
//        Context context = StartOnlineTest.getContext();
//        if (context != null) {
//            if (!((StartOnlineTest) context).isFinishing()) {
//                ((StartOnlineTest) context).updateTimer();
//            }
//        }
//    }

    public static String getTimer() {
        return Prefs.timer;
    }


}
