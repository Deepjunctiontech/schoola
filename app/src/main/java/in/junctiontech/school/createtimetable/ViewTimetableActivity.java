package in.junctiontech.school.createtimetable;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;
import in.junctiontech.school.schoolnew.model.StudentInformation;

public class ViewTimetableActivity extends AppCompatActivity {

    MainDatabase mDb;
    ArrayList<Date> dayValueInCells = new ArrayList<>();
    private SharedPreferences sp;
    private int colorIs;
//    private ProgressDialog progressbar;

    ProgressBar progressBar;
    // private CalendarView cv_view_timetable;
    private RecyclerView rv_view_timetable;
    DialogViewItemAdapter adapter;
    ArrayList<TimeTableSlot> slotList = new ArrayList<>();

    private View snackbar;
    private ArrayList<ClassNameSectionName> classListData = new ArrayList<>();
    private boolean logoutAlert;
    private ArrayList<String> sectionNameList = new ArrayList<>();
    private ArrayAdapter<String> classAdapter;
    private Spinner sp_class_list;
    private LinearLayout ll_holiday_view;
    private TextView tv_holiday_comments;
    private TextView tv_calendar_date, tv_calendar_sun, tv_calendar_mon, tv_calendar_tues, tv_calendar_wed,
            tv_calendar_thr, tv_calendar_fri, tv_calendar_sat;
    private Calendar calendar;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private TextView lastViewClicked;
//    private View ll_click_to_view_slot_detail_demo,ll_click_to_view_slot_detail_demo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_timetable);
        sp = Prefs.with(this).getSharedPreferences();

        mDb = MainDatabase.getDatabase(this);

        /* ********************************    Demo Screen *******************************/
//        final View page_demo =   findViewById(R.id.rl_view_timetable_demo);
//        ll_click_to_view_slot_detail_demo = findViewById(R.id.rl_view_timetable_slot_detail_demo);
//        ll_click_to_view_slot_detail_demo1 = findViewById(R.id.ll_view_timetable_slot_detail_demo1);
//
//        if (sp.getBoolean("DemoViewTimetableActivity", true)) {
//            page_demo.setVisibility(View.VISIBLE);
//        } else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoViewTimetableActivity", false).commit();
//            }
//        });

        /* ***************************************************************/



        colorIs = Config.getAppColor(this, true);
        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        snackbar = findViewById(R.id.ll_view_timetable);
        //cv_view_timetable = (CalendarView) findViewById(R.id.cv_view_timetable);
        rv_view_timetable =  findViewById(R.id.rv_view_timetable);

        rv_view_timetable.setLayoutManager(new LinearLayoutManager(this));

        adapter = new DialogViewItemAdapter(this, slotList, false, colorIs);
        rv_view_timetable.setAdapter(adapter);

        sp_class_list =  findViewById(R.id.sp_view_timetable_class_list);

        sp_class_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, classListData.get(position).SectionId)
                        .putString(Config.LAST_USED_CLASS_ID, classListData.get(position).ClassId)
                        .apply();
                getSlotList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this).equalsIgnoreCase(Gc.STUDENTPARENT)){
//            sp_class_list.setVisibility(View.GONE);
            mDb.signedInStudentInformationModel()
                    .getSignedInStudentInformation()
                    .observe(ViewTimetableActivity.this, new Observer<StudentInformation>() {
                @Override
                public void onChanged(@Nullable final StudentInformation studentInformation) {

                    mDb.schoolClassSectionModel()
                            .getClassSectionNameSingle(studentInformation.SectionId)
                            .observe(ViewTimetableActivity.this, new Observer<ClassNameSectionName>() {
                                @Override
                                public void onChanged(@Nullable ClassNameSectionName classNameSectionName) {
                                    if (classNameSectionName == null)
                                        return;

                                    classListData.clear();
                                    sectionNameList.clear();
                                    classListData.add(classNameSectionName);
                                    for(int i = 0; i < classListData.size(); i++){
                                        sectionNameList.add(classListData.get(i).ClassName + " " + classListData.get(i).SectionName );
                                    }
                                    if (classAdapter == null) {
                                        classAdapter = new ArrayAdapter<String>(ViewTimetableActivity.this, android.R.layout.simple_list_item_1, sectionNameList);
                                        classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
                                        sp_class_list.setAdapter(classAdapter);
                                    } else classAdapter.notifyDataSetChanged();

                                    mDb.schoolClassSectionModel()
                                            .getClassSectionNameSingle(studentInformation.SectionId)
                                            .removeObserver(this);

                                }
                            });

                    mDb.signedInStudentInformationModel().getSignedInStudentInformation().removeObserver(this);
                }
            });

        }else   {
            mDb.schoolClassSectionModel()
                    .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this))
                    .observe(this, classNameSectionNames -> {
                        classListData.clear();
                        sectionNameList.clear();
                        classListData.addAll(classNameSectionNames);
                        for(int i = 0; i < classListData.size(); i++){
                            sectionNameList.add(classListData.get(i).ClassName + " " + classListData.get(i).SectionName );
                        }
                        if (classAdapter == null) {
                            classAdapter = new ArrayAdapter<String>(ViewTimetableActivity.this, android.R.layout.simple_list_item_1, sectionNameList);
                            classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
                            sp_class_list.setAdapter(classAdapter);
                        } else classAdapter.notifyDataSetChanged();
                    });
        }

        ((TextView) findViewById(R.id.tv_view_timetable_class_title)).setTextColor(colorIs);
        ( findViewById(R.id.ll_view_timetable_calendar)).setBackgroundColor(colorIs);

        calendar = Calendar.getInstance(Locale.ENGLISH);
        tv_calendar_date =  findViewById(R.id.tv_view_timetable_calendar_date);
        tv_calendar_sun =  findViewById(R.id.tv_view_timetable_calendar_sun);
        tv_calendar_mon =  findViewById(R.id.tv_view_timetable_calendar_mon);
        tv_calendar_tues =  findViewById(R.id.tv_view_timetable_calendar_tues);
        tv_calendar_wed = findViewById(R.id.tv_view_timetable_calendar_wed);
        tv_calendar_thr =  findViewById(R.id.tv_view_timetable_calendar_thr);
        tv_calendar_fri =  findViewById(R.id.tv_view_timetable_calendar_fri);
        tv_calendar_sat =  findViewById(R.id.tv_view_timetable_calendar_sat);
        ( findViewById(R.id.btn_view_timetable_calendar_left)).setOnClickListener(v -> {
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            setCalendarView();
        });
        ( findViewById(R.id.btn_view_timetable_calendar_right)).setOnClickListener(v -> {
            calendar.add(Calendar.DAY_OF_MONTH, +7);
            setCalendarView();
        });


        setCalendarView();

        ll_holiday_view =  findViewById(R.id.ll_holiday_view);
        tv_holiday_comments =  findViewById(R.id.tv_holiday_comments);


//        getClassListFromServer();
        tv_calendar_date.setOnClickListener(v -> {

            DatePickerDialog datePickerDialog = new DatePickerDialog(ViewTimetableActivity.this,
                    (view, year, month, dayOfMonth) -> {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        setCalendarView();

                    },
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });
        tv_calendar_sun.setOnClickListener(v -> {
            performDayClick(0, tv_calendar_sun);
        });
        tv_calendar_mon.setOnClickListener(v -> {
            performDayClick(1, tv_calendar_mon);
        });
        tv_calendar_tues.setOnClickListener(v -> {
                performDayClick(2, tv_calendar_tues);
        });
        tv_calendar_wed.setOnClickListener(v -> {
                performDayClick(3, tv_calendar_wed);
        });
        tv_calendar_thr.setOnClickListener(v -> {
            performDayClick(4, tv_calendar_thr);
        });
        tv_calendar_fri.setOnClickListener(v -> {
            performDayClick(5, tv_calendar_fri);
        });
        tv_calendar_sat.setOnClickListener(v -> {
            performDayClick(6, tv_calendar_sat);
        });


        if(!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")){
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/2781253248",this,adContainer);
        }

    }


    private void performDayClick(int dayPos, TextView tv_calendar) {

      //  if (lastViewClicked != tv_calendar) {
            calendar.setTime(dayValueInCells.get(dayPos));
            getSlotList();
        if (lastViewClicked != tv_calendar) {
            tv_calendar.setBackgroundResource(R.drawable.borders);
            tv_calendar.setTextColor(Color.BLACK);
            if (lastViewClicked != null) {
                lastViewClicked.setBackgroundResource(0);
                lastViewClicked.setTextColor(Color.WHITE);
            }
            lastViewClicked = tv_calendar;
        }
    }

    private void setCalendarView() {
        tv_calendar_date.setText(formatter.format(calendar.getTime()) + "");
        dayValueInCells.clear();

        Calendar mCal = (Calendar) calendar.clone();
        mCal.set(Calendar.HOUR_OF_DAY, 0);
        mCal.set(Calendar.MINUTE, 0);
        mCal.set(Calendar.SECOND, 0);
        mCal.set(Calendar.MILLISECOND, 0);

        mCal.set(Calendar.WEEK_OF_MONTH, calendar == null ? Calendar.WEEK_OF_MONTH + 1 : calendar.get(Calendar.WEEK_OF_MONTH));
        int firstDayOfTheWeek = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheWeek);

        for (int pos = 0; dayValueInCells.size() < 7; pos++) {
            dayValueInCells.add(mCal.getTime());
            switch (pos) {
                case 0:
                    tv_calendar_sun.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(0, tv_calendar_sun);
                    break;
                case 1:
                    tv_calendar_mon.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(1, tv_calendar_mon);
                    break;
                case 2:
                    tv_calendar_tues.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(2, tv_calendar_tues);
                    break;
                case 3:
                    tv_calendar_wed.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(3, tv_calendar_wed);
                    break;
                case 4:
                    tv_calendar_thr.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(4, tv_calendar_thr);
                    break;
                case 5:
                    tv_calendar_fri.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(5, tv_calendar_fri);
                    break;
                case 6:
                    tv_calendar_sat.setText(String.valueOf(mCal.get(Calendar.DAY_OF_MONTH)));
                    if (calendar.get(Calendar.DAY_OF_MONTH) == mCal.get(Calendar.DAY_OF_MONTH))
                        performDayClick(6, tv_calendar_sat);
                    break;
            }

            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }

    }

    private void getSlotList() {
        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {

            progressBar.setVisibility(View.VISIBLE);
            final String date=  calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
            final JSONObject param = new JSONObject();
            try {
                param.put("SectionID", classListData.get(sp_class_list.getSelectedItemPosition()).SectionId);
                param.put("StartDate", date);
                param.put("EndDate", date);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/viewTimeTable/"
                    + param
                    ;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,new JSONObject(),

                    job -> {
                        progressBar.setVisibility(View.GONE);

                        String code = job.optString("code");

                        switch (code) {
                            case Gc.APIRESPONSE200:
                                slotList.clear();

                                try {
                                    JSONObject result = job.getJSONObject("result");
                                    ArrayList<TimeTableSlot> slotReceived = new Gson()
                                            .fromJson(result.optString("SectionTimeDayWise"),
                                                    new TypeToken<List<TimeTableSlot>>() {}.getType());


                                    TimeTableSlot holidayFound = null ;
                                    for (TimeTableSlot s: slotReceived){
                                        if (s.Holiday.equalsIgnoreCase("1")){

                                            holidayFound = s;
                                            break;
                                        }
                                    }

                                    if (holidayFound == null){
                                        ll_holiday_view.setVisibility(View.GONE);
                                        rv_view_timetable.setVisibility(View.VISIBLE);

                                        slotList.addAll(slotReceived);
                                        adapter.notifyDataSetChanged();
                                    }else{
                                        ll_holiday_view.setVisibility(View.VISIBLE);
                                        rv_view_timetable.setVisibility(View.GONE);

                                        tv_holiday_comments.setText(holidayFound.Comment);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;

                            case Gc.APIRESPONSE204:
                                slotList.clear();
                                adapter.notifyDataSetChanged();
                                if (ll_holiday_view.getVisibility() == View.VISIBLE){
                                    ll_holiday_view.setVisibility(View.GONE);
                                }

                                Config.responseSnackBarHandler(getString(R.string.timetable_not_available),
                                        findViewById(R.id.ll_view_timetable),R.color.fragment_first_blue);
                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, ViewTimetableActivity.this);

                                Intent intent1 = new Intent(ViewTimetableActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, ViewTimetableActivity.this);

                                Intent intent2 = new Intent(ViewTimetableActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                            default:
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.ll_view_timetable),R.color.fragment_first_blue);

                        }


//                        ArrayList<SlotsDetails> slotList = new ArrayList<>();
//                        if (!isFinishing())
//                            if (job.optString("code").equalsIgnoreCase("200")) {
//                                try {
//                                    JSONObject result = job.getJSONObject("result");
//                                    slotList = new Gson().fromJson(result.optString("SectionTimeDayWise"), new TypeToken<List<SlotsDetails>>() {
//                                    }.getType());
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            } else if (job.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    job.optString("code").equalsIgnoreCase("511")) {
//                                if (!logoutAlert & !isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(ViewTimetableActivity.this, job.optInt("code") + "", job.optString("message"));
//                                    logoutAlert = true;
//                                }
//                            } else if (job.optString("code").equalsIgnoreCase("502"))
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//                            else
//                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
//
//
//                        if (slotList.size() == 1 && slotList.get(0).getHoliday().equalsIgnoreCase("1")) {
//                            showTimetable(new ArrayList<SlotsDetails>(), date);
//                            ll_holiday_view.setVisibility(View.VISIBLE);
//                            tv_holiday_comments.setText(slotList.get(0).getComment() + "");
//                        } else {
//                            final Calendar cal = Calendar.getInstance();
//                            Collections.sort(slotList, new Comparator<SlotsDetails>() {
//                                @Override
//                                public int compare(SlotsDetails o1, SlotsDetails o2) {
//                                    Calendar calLeft = Calendar.getInstance();
//                                    Calendar calRight = Calendar.getInstance();
//                                    try {
//                                        calLeft.setTime(CreateSlotsActivity.time.parse(o1.getSlotStartTime()));
//                                    } catch (ParseException e) {
//                                        calLeft.setTimeInMillis(cal.getTimeInMillis());
//                                        e.printStackTrace();
//                                    }
//                                    try {
//                                        calRight.setTime(CreateSlotsActivity.time.parse(o2.getSlotStartTime()));
//                                    } catch (ParseException e) {
//                                        calRight.setTimeInMillis(cal.getTimeInMillis());
//                                        e.printStackTrace();
//                                    }
//
//
//                                    return calLeft.compareTo(calRight);
//
//
//                                }
//                            });
//                            ll_holiday_view.setVisibility(View.GONE);
//                            showTimetable(slotList, date);
//
//
//                        }
                    }, volleyError -> {
                progressBar.setVisibility(View.GONE);

                Log.e("getSlotData", volleyError.toString());

                Config.responseVolleyErrorHandler(ViewTimetableActivity.this,volleyError,findViewById(R.id.ll_view_timetable));

                    })
            {
                @Override
                public Map<String, String> getHeaders()  {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }
            }
            ;

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }
    }

    public void showTimetable(ArrayList<TimeTableSlot> slotList, String date) {
        RecyclerView rv_view_timetable_detail =  findViewById(R.id.rv_view_timetable);
        rv_view_timetable_detail.setLayoutManager(new LinearLayoutManager(this));
        DialogViewItemAdapter adapter = new DialogViewItemAdapter(this, slotList, false, colorIs);
        rv_view_timetable_detail.setAdapter(adapter);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

}
