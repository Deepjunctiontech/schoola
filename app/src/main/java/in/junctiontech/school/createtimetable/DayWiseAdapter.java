package in.junctiontech.school.createtimetable;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;

/**
 * Created by JAYDEVI BHADE on 18-07-2017.
 */

public class DayWiseAdapter extends RecyclerView.Adapter<DayWiseAdapter.MyViewHolder> {
    private final DayWiseAdapter.LectureAdapter<String> lectureTypeAdapter;
    private final Integer[] drawableListArray=  CreateSlotsActivity.drawableListArray,
            nameDrawableListArray=CreateSlotsActivity.nameDrawableListArray;
    private final ArrayList<String> nameEnglishListArray= new ArrayList<>();

    private final Calendar cal;
    private ArrayList<String> subjectNameList = new ArrayList<>();

    private ArrayList<String> staffNameList = new ArrayList<>();
    private ArrayAdapter<String> adapterStaffName = null;
    private Context context;
    private ArrayList<TimeTableSlot> slotList;
    private int colorIs;
    private ArrayList<TimeTableSlot> deleteSlotsId= new ArrayList<>();
    private boolean isDelete;
    private ArrayList<SchoolSubjectsEntity> subjectList;
    private ArrayList<SchoolStaffEntity> staffList;
    private ArrayAdapter<String> adapterSubjectName;

    public DayWiseAdapter(Context context, ArrayList<TimeTableSlot> slotList, int colorIs, ArrayList<SchoolStaffEntity> staffList, ArrayList<SchoolSubjectsEntity> subjectList) {
        this.context = context;
        this.slotList = slotList;
        this.colorIs = colorIs;
        this.staffList = staffList;
        this.subjectList = subjectList;
        // Get Current Time
        cal = Calendar.getInstance();

        nameEnglishListArray.add("Lecture");
        nameEnglishListArray.add("Lab");
        nameEnglishListArray.add("Games");
        nameEnglishListArray.add("Prayer");
        nameEnglishListArray.add("Yoga");
        nameEnglishListArray.add("Music");
        nameEnglishListArray.add("Breakfast");
        nameEnglishListArray.add("Lunch");
        nameEnglishListArray.add("Dinner");
        nameEnglishListArray.add("Dance");
        nameEnglishListArray.add("Art & Craft");
        nameEnglishListArray.add("Exam");
        nameEnglishListArray.add("Break");
        nameEnglishListArray.add("Activity");
        nameEnglishListArray.add("library");

        lectureTypeAdapter = new LectureAdapter<>(context, drawableListArray, nameDrawableListArray);

        staffNameList.add(context.getString(R.string.select));
        for (SchoolStaffEntity staffObj : staffList)
            staffNameList.add(staffObj.StaffName);
        adapterStaffName = new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_1, staffNameList);

        subjectNameList.add(context.getString(R.string.select));
        for (SchoolSubjectsEntity obj : subjectList)
            subjectNameList.add(obj.SubjectName);
        adapterSubjectName = new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_1, subjectNameList);

    }

    @Override
    public DayWiseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DayWiseAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_time_table_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(DayWiseAdapter.MyViewHolder holder, int position) {

        final TimeTableSlot obj = slotList.get(position);
        holder.tv_slot_start_time.setText(obj.SlotStartTime);
        holder.tv_slot_end_time.setText(obj.SlotEndTime);
        holder.tv_slot_name.setText(obj.SlotName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.SubjectName))){
            holder.tv_slot_subject.setVisibility(View.GONE);
        }else{
            holder.tv_slot_subject.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_subject.setText(obj.SubjectName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.StaffName))){
            holder.tv_slot_teacher.setVisibility(View.GONE);
        }else{
            holder.tv_slot_teacher.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_teacher.setText(obj.StaffName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.Chapter))){
            holder.tv_slot_chapter.setVisibility(View.GONE);
        }else{
            holder.tv_slot_chapter.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_chapter.setText(obj.Chapter);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.Topic))){
            holder.tv_slot_topic.setVisibility(View.GONE);
        }else{
            holder.tv_slot_topic.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_topic.setText(obj.Topic);
        int dblPos= nameEnglishListArray.indexOf(obj.Type);
        holder.iv_item_view_type.setImageDrawable(context.getResources().getDrawable(drawableListArray[dblPos==-1?0:dblPos ]));

        if (isDelete) holder.item_slot_delete.setVisibility(View.VISIBLE);
        else holder.item_slot_delete.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    public void updateList(ArrayList<TimeTableSlot> slotList) {
        this.slotList = slotList;
        notifyDataSetChanged();
    }

    public void deleteSlot(boolean isDelete) {
        if (this.isDelete) isDelete = false;
        this.isDelete = isDelete;
        notifyDataSetChanged();
    }

    public void updateStaffList(ArrayList<SchoolStaffEntity> staffListData) {
        this.staffList = staffListData;
        staffNameList = new ArrayList<>();

        staffNameList.add(context.getString(R.string.select));
        for (SchoolStaffEntity staffObj : staffList)
            staffNameList.add(staffObj.StaffName);


        adapterStaffName = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, staffNameList);
        adapterStaffName.notifyDataSetChanged();
        notifyDataSetChanged();
    }

    public void updateSubjectList(ArrayList<SchoolSubjectsEntity> subjectListData) {
        this.subjectList = subjectListData;
        subjectNameList = new ArrayList<>();
        subjectNameList.add(context.getString(R.string.select));
        for (SchoolSubjectsEntity obj : subjectList)
            subjectNameList.add(obj.SubjectName);
        adapterSubjectName = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, subjectNameList);
    }

    public ArrayList<TimeTableSlot>  getDeleteSlotsId() {
        return deleteSlotsId;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView iv_item_view_type;
        private final TextView tv_slot_start_time, tv_slot_end_time, tv_slot_name,
                tv_slot_subject,tv_slot_teacher,tv_slot_chapter,tv_slot_topic;
        private final ImageButton item_slot_delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            iv_item_view_type =  itemView.findViewById(R.id.iv_item_view_type);
            tv_slot_start_time =  itemView.findViewById(R.id.tv_slot_start_time);
            tv_slot_end_time =  itemView.findViewById(R.id.tv_slot_end_time);

            tv_slot_name =  itemView.findViewById(R.id.tv_slot_name);
            tv_slot_subject =  itemView.findViewById(R.id.tv_slot_subject);
            tv_slot_teacher =  itemView.findViewById(R.id.tv_slot_teacher);
            tv_slot_chapter =  itemView.findViewById(R.id.tv_slot_chapter);
            tv_slot_topic =  itemView.findViewById(R.id.tv_slot_topic);


            item_slot_delete =  itemView.findViewById(R.id.item_slot_delete);
            tv_slot_start_time.setTextColor(colorIs);
            tv_slot_teacher.setTextColor(colorIs);
            tv_slot_subject.setTextColor(colorIs);


            item_slot_delete.setOnClickListener(v -> {
                if (slotList.size() >= 1) {
                    if (!slotList.get(getLayoutPosition()).SlotID.trim().equalsIgnoreCase(""))
                        deleteSlotsId.add(slotList.get(getLayoutPosition()));
                    slotList.remove(getLayoutPosition());
                    isDelete = false;
                    notifyDataSetChanged();
                }
            });


            itemView.setOnClickListener(v -> {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                View itemTimeMappingView = LayoutInflater.from(context).inflate(R.layout.item_timetable_mapping, null);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_start_time_title)).setTextColor(colorIs);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_end_time_title)).setTextColor(colorIs);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_subject_name)).setTextColor(colorIs);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_teacher_name)).setTextColor(colorIs);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_lecture_type_title)).setTextColor(colorIs);

                final Button btn_slot_start_time =  itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_start_time);
                final Button btn_slot_end_time =  itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_end_time);
                final EditText et_slot_name =  itemTimeMappingView.findViewById(R.id.et_item_timetable_mapping_slot_name);

                final Spinner subject_name =  itemTimeMappingView.findViewById(R.id.sp_item_timetable_mapping_subject_name);
                final Spinner teacher_name =  itemTimeMappingView.findViewById(R.id.sp_item_timetable_mapping_teacher_name);
                final EditText chapter_name = itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_chapter_name);
                final EditText topic_name =  itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_topic_name);

                subject_name.setAdapter(adapterSubjectName);
                teacher_name.setAdapter(adapterStaffName);

                final Spinner sp_lecture_type =  itemTimeMappingView.findViewById(R.id.sp_item_timetable_mapping_lecture_type);
                sp_lecture_type.setAdapter(lectureTypeAdapter);

                alertDialog.setTitle(context.getString(R.string.slot_information));
                et_slot_name.setVisibility(View.VISIBLE);
                final TimeTableSlot slotObj = slotList.get(getLayoutPosition());

                et_slot_name.setText(slotObj.SlotName);


                for (int pos = 0; pos < staffList.size(); pos++) {
                    if (staffList.get(pos).StaffId.equalsIgnoreCase(null==slotObj.Teacher?"":slotObj.Teacher)) {
                        teacher_name.setSelection(pos+1);
                        break;
                    }
                }
                for (int pos = 0; pos < subjectList.size(); pos++) {
                    if (subjectList.get(pos).SubjectId.equalsIgnoreCase(null==slotObj.Subject?"":slotObj.Subject)) {
                        subject_name.setSelection(pos+1);
                        break;
                    }
                }

                chapter_name.setText(null==slotObj.Chapter?"":slotObj.Chapter);
                topic_name.setText(null==slotObj.Topic?"":slotObj.Topic);

                int dblPos= nameEnglishListArray.indexOf(slotObj.Type);
                sp_lecture_type.setSelection( dblPos==-1?0:dblPos );

                btn_slot_start_time.setText(null==slotObj.SlotStartTime?"":slotObj.SlotStartTime);
                btn_slot_end_time.setText(null==slotObj.SlotEndTime?"":slotObj.SlotEndTime);

                btn_slot_start_time.setOnClickListener(v1 -> {
                    Calendar calTemp = Calendar.getInstance();
                    try {
                        calTemp.setTime(CreateSlotsActivity.time.parse(null==slotObj.SlotStartTime?"":slotObj.SlotStartTime));
                    } catch (ParseException e) {
                        calTemp.setTimeInMillis(cal.getTimeInMillis());
                        e.printStackTrace();
                    }

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                            (view, hourOfDay, minute) -> {
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                cal.set(Calendar.MINUTE, minute);
                                cal.set(Calendar.SECOND, 0);
                                btn_slot_start_time.setText(CreateSlotsActivity.time.format(cal.getTime()));
                            },
                            calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                });
                btn_slot_end_time.setOnClickListener(v12 -> {
                    Calendar calTemp = Calendar.getInstance();
                    try {
                        calTemp.setTime(CreateSlotsActivity.time.parse(null==slotObj.SlotEndTime?"":slotObj.SlotEndTime));
                    } catch (ParseException e) {
                        calTemp.setTimeInMillis(cal.getTimeInMillis());
                        e.printStackTrace();
                    }

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                            (view, hourOfDay, minute) -> {
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                cal.set(Calendar.MINUTE, minute);
                                cal.set(Calendar.SECOND, 0);
                                btn_slot_end_time.setText(CreateSlotsActivity.time.format(cal.getTime()));
                            },
                            calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                });
                alertDialog.setView(itemTimeMappingView);

                alertDialog.setPositiveButton(R.string.save, (dialog, which) -> {
                    slotObj.SlotName = et_slot_name.getText().toString().trim();
                    slotObj.SlotEndTime = btn_slot_end_time.getText().toString();
                    slotObj.SlotStartTime =  btn_slot_start_time.getText().toString();
                    slotObj.Chapter = chapter_name.getText().toString();
                    slotObj.Topic = topic_name.getText().toString();

                    if (subject_name.getSelectedItemPosition()==0)
                        slotObj.Subject = "0";
                    else   if (subjectList.size() >= subject_name.getSelectedItemPosition())
                        slotObj.Subject = subjectList.get(subject_name.getSelectedItemPosition()-1).SubjectId;


                    if ( teacher_name.getSelectedItemPosition()==0)slotObj.Teacher = "0";
                    else   if (staffList.size() >= teacher_name.getSelectedItemPosition())
                        slotObj.Teacher = staffList.get(teacher_name.getSelectedItemPosition()-1).StaffId;


                    slotObj.Type = nameEnglishListArray.get(sp_lecture_type.getSelectedItemPosition());
                    notifyDataSetChanged();
                });
                alertDialog.setNegativeButton(R.string.cancel, null);
                alertDialog.show();
            });
        }
    }


    private class LectureAdapter<T> extends ArrayAdapter<Integer> {
        private Integer[] images, imagesName;

        public LectureAdapter(Context context, Integer[] images, Integer[] imagesName) {
            super(context, android.R.layout.simple_spinner_item, images);
            this.images = images;
            this.imagesName = imagesName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.lecture_type, parent, false);
            ((ImageView) viewItem.findViewById(R.id.ic_item_lecture_type)).setImageResource(images[position]);
            ((TextView) viewItem.findViewById(R.id.tv_item_lecture_type)).setText(imagesName[position]);
            return viewItem;
        }

       /* private View getImageForPosition(int position) {
            ImageView imageView = new ImageView(getContext());
            imageView.setBackgroundResource(images[position]);
            imageView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            return imageView;
        }*/
    }
}

