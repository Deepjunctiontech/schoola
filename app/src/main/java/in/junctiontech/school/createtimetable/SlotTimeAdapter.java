package in.junctiontech.school.createtimetable;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 06-07-2017.
 */

public class SlotTimeAdapter extends RecyclerView.Adapter<SlotTimeAdapter.MyViewHolder> {

    private final LectureAdapter<String> lectureTypeAdapter;

    private final ArrayList<String> nameEnglishListArray = new ArrayList<>();
    private Calendar cal;
    private CreateSlotsActivity createSlotsActivity;
    private ArrayList<SlotsDetails> slotList;
    private ArrayList<String> deleteSlotsId;
    private boolean isDelete;
    private int appColor;
    private Integer[] drawableListArray = CreateSlotsActivity.drawableListArray;
    private Integer[] nameDrawableListArray = CreateSlotsActivity.nameDrawableListArray;


    public SlotTimeAdapter(CreateSlotsActivity createSlotsActivity, ArrayList<SlotsDetails> slotList, boolean isDelete, int appColor,
                           ArrayList<String> deleteSlotsId, Calendar cal) {
        this.isDelete = isDelete;
        this.createSlotsActivity = createSlotsActivity;
        this.slotList = slotList;
        this.appColor = appColor;
        this.cal = cal;
        this.deleteSlotsId = new ArrayList<>();
        // Get Current Time
      //  this.cal = Calendar.getInstance();

        nameEnglishListArray.add("Lecture");
        nameEnglishListArray.add("Lab");
        nameEnglishListArray.add("Games");
        nameEnglishListArray.add("Prayer");
        nameEnglishListArray.add("Yoga");
        nameEnglishListArray.add("Music");
        nameEnglishListArray.add("Breakfast");
        nameEnglishListArray.add("Lunch");
        nameEnglishListArray.add("Dinner");
        nameEnglishListArray.add("Dance");
        nameEnglishListArray.add("Art & Craft");
        nameEnglishListArray.add("Exam");
        nameEnglishListArray.add("Break");
        nameEnglishListArray.add("Activity");
        nameEnglishListArray.add("library");
        lectureTypeAdapter = new LectureAdapter<>(createSlotsActivity, drawableListArray, nameDrawableListArray);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SlotTimeAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_value, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SlotsDetails obj = slotList.get(position);
        holder.tv_item_name.setText(obj.getSlotName() + "");
        holder.tv_subitem_name.setText(obj.getSlotStartTime() + " - " + obj.getSlotEndTime());

        int dblPos = nameEnglishListArray.indexOf(obj.getType());
        holder.iv_item_view_type.setImageDrawable(createSlotsActivity.getResources().getDrawable(drawableListArray[dblPos == -1 ? 0 : dblPos]));


        if (isDelete) holder.item_slot_delete.setVisibility(View.VISIBLE);
        else holder.item_slot_delete.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    public void updateList(ArrayList<SlotsDetails> slotList) {
        this.slotList = slotList;
        notifyDataSetChanged();
    }

    public void deleteSlot(boolean isDelete) {
        if (this.isDelete) isDelete = false;
        this.isDelete = isDelete;
        notifyDataSetChanged();
    }

    public ArrayList<String> getDeleteSlotsId() {
        return deleteSlotsId;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView iv_item_view_type;
        private final TextView tv_item_name, tv_subitem_name;
        private final ImageButton item_slot_delete;


        public MyViewHolder(View itemView) {
            super(itemView);
            iv_item_view_type = (ImageView) itemView.findViewById(R.id.iv_item_view_type);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_slot_start_time);
            tv_subitem_name = (TextView) itemView.findViewById(R.id.tv_slot_end_time);
            item_slot_delete = (ImageButton) itemView.findViewById(R.id.item_slot_delete);
            tv_item_name.setTextColor(appColor);

            item_slot_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (slotList.size() >= 1) {
                        String slotId = slotList.get(getLayoutPosition()).getSlotID();
                        if (!slotId.trim().equalsIgnoreCase(""))
                            deleteSlotsId.add(slotId);
                        slotList.remove(getLayoutPosition());
                        isDelete = false;
                        notifyDataSetChanged();
                    }
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(createSlotsActivity);
                    View itemTimeMappingView = LayoutInflater.from(createSlotsActivity).inflate(R.layout.item_timetable_mapping, null);

                    (itemTimeMappingView.findViewById(R.id.ll_item_timetable_mapping_subject_name)).setVisibility(View.GONE);
                    (itemTimeMappingView.findViewById(R.id.ll_item_timetable_mapping_teacher_name)).setVisibility(View.GONE);
                    (itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_chapter_name)).setVisibility(View.GONE);
                    (itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_topic_name)).setVisibility(View.GONE);

                    ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_start_time_title)).setTextColor(appColor);
                    ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_end_time_title)).setTextColor(appColor);
                    ((TextView) itemTimeMappingView.findViewById(R.id.tv_item_timetable_mapping_lecture_type_title)).setTextColor(appColor);

                    final Button btn_slot_start_time = (Button) itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_start_time);
                    final Button btn_slot_end_time = (Button) itemTimeMappingView.findViewById(R.id.btn_item_timetable_mapping_end_time);
                    final EditText et_slot_name = (EditText) itemTimeMappingView.findViewById(R.id.et_item_timetable_mapping_slot_name);
                    final Spinner sp_lecture_type = (Spinner) itemTimeMappingView.findViewById(R.id.sp_item_timetable_mapping_lecture_type);
                    sp_lecture_type.setAdapter(lectureTypeAdapter);

                    alertDialog.setTitle(createSlotsActivity.getString(R.string.slot_information));

                    final SlotsDetails slotObj = slotList.get(getLayoutPosition());

                    int dblPos = nameEnglishListArray.indexOf(slotObj.getType());
                    sp_lecture_type.setSelection(dblPos == -1 ? 0 : dblPos);
                    et_slot_name.setText(slotObj.getSlotName());
                    btn_slot_start_time.setText(slotObj.getSlotStartTime());
                    btn_slot_end_time.setText(slotObj.getSlotEndTime());

                    btn_slot_start_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar calTemp = Calendar.getInstance();
                            try {
                                calTemp.setTime(CreateSlotsActivity.time.parse(slotObj.getSlotStartTime()));
                            } catch (ParseException e) {
                                calTemp.setTimeInMillis(cal.getTimeInMillis());
                                e.printStackTrace();
                            }

                            // Launch Time Picker Dialog
                            TimePickerDialog timePickerDialog = new TimePickerDialog(createSlotsActivity,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay,
                                                              int minute) {
                                            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                            cal.set(Calendar.MINUTE, minute);
//                                            cal.set(Calendar.SECOND, 0);
                                            btn_slot_start_time.setText(CreateSlotsActivity.time.format(cal.getTime()));
                                        }
                                    },
                                    calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
                            timePickerDialog.show();
                        }

                    });
                    btn_slot_end_time.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar calTemp = Calendar.getInstance();
                            try {
                                calTemp.setTime(CreateSlotsActivity.time.parse(slotObj.getSlotEndTime()));
                            } catch (ParseException e) {
                                calTemp.setTimeInMillis(cal.getTimeInMillis());
                                e.printStackTrace();
                            }

                            // Launch Time Picker Dialog
                            TimePickerDialog timePickerDialog = new TimePickerDialog(createSlotsActivity,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay,
                                                              int minute) {
                                            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                            cal.set(Calendar.MINUTE, minute);
//                                            cal.set(Calendar.SECOND, 0);
                                            btn_slot_end_time.setText(CreateSlotsActivity.time.format(cal.getTime()));
                                        }
                                    },
                                    calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
                            timePickerDialog.show();
                        }

                    });
                    alertDialog.setView(itemTimeMappingView);
                    alertDialog.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            slotObj.setSlotName(et_slot_name.getText().toString().trim());
                            (slotObj).setSlotEndTime(btn_slot_end_time.getText().toString());
                            (slotObj).setSlotStartTime(btn_slot_start_time.getText().toString());
                            (slotObj).setType(nameEnglishListArray.get(sp_lecture_type.getSelectedItemPosition()));
                            notifyDataSetChanged();
                        }
                    });
                    alertDialog.setNegativeButton(R.string.cancel, null);
                    alertDialog.show();
                }
            });

        }
    }

    private class LectureAdapter<T> extends ArrayAdapter<Integer> {
        private Integer[] images, imagesName;

        public LectureAdapter(Context context, Integer[] images, Integer[] imagesName) {
            super(context, android.R.layout.simple_spinner_item, images);
            this.images = images;
            this.imagesName = imagesName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.lecture_type, parent, false);
            ((ImageView) viewItem.findViewById(R.id.ic_item_lecture_type)).setImageResource(images[position]);
            ((TextView) viewItem.findViewById(R.id.tv_item_lecture_type)).setText(imagesName[position]);
            return viewItem;
        }

       /* private View getImageForPosition(int position) {
            ImageView imageView = new ImageView(getContext());
            imageView.setBackgroundResource(images[position]);
            imageView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            return imageView;
        }*/
    }
}
