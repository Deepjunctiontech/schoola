package in.junctiontech.school.createtimetable;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 12-07-2017.
 */

public class SlotsDetails implements Serializable {
    public String SlotID, SlotName, SlotStartTime, SlotEndTime, Day,SectionID, Subject, Teacher, Chapter, Topic
            ,Date,Holiday, Comment,Type,SubjectName,StaffName ;
    public int  Flag;

    public ArrayList<SlotsDetails> result;

    public SlotsDetails(String slotId, String slotName, String slotStartTime, String slotEndTime ,String sub, String tea) {
        this.SlotID = slotId;
        this.SlotEndTime = slotEndTime;
        this.SlotName = slotName;
        this.SlotStartTime = slotStartTime;
        this.Subject = sub;
        this.Teacher = tea;

    }


    public SlotsDetails(String slotId, String slotName, String slotStartTime, String slotEndTime, String slotType) {
        this.SlotID = slotId;
        this.SlotEndTime = slotEndTime;
        this.SlotName = slotName;
        this.SlotStartTime = slotStartTime;
        this.Type = slotType;
    }


    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public ArrayList<SlotsDetails> getResult() {
        return result;
    }

    public void setResult(ArrayList<SlotsDetails> result) {
        this.result = result;
    }


    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getHoliday() {
        return Holiday;
    }

    public void setHoliday(String holiday) {
        Holiday = holiday;
    }



    public String getComment() {
        return Strings.nullToEmpty(Comment);
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getTeacher() {
        return Teacher;
    }

    public void setTeacher(String teacher) {
        Teacher = teacher;
    }

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String chapter) {
        Chapter = chapter;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }

    public String getSectionID() {
        return SectionID;
    }

    public void setSectionID(String sectionID) {
        SectionID = sectionID;
    }

    public String getSlotID() {
        return SlotID;
    }

    public void setSlotID(String slotID) {
        SlotID = slotID;
    }

    public String getSlotName() {
        return SlotName;
    }

    public void setSlotName(String slotName) {
        SlotName = slotName;
    }

    public String getSlotStartTime() {
        return SlotStartTime;
    }

    public void setSlotStartTime(String slotStartTime) {
        SlotStartTime = slotStartTime;
    }

    public String getSlotEndTime() {
        return SlotEndTime;
    }

    public void setSlotEndTime(String slotEndTime) {
        SlotEndTime = slotEndTime;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
