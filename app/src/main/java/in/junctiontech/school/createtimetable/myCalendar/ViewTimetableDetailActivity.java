package in.junctiontech.school.createtimetable.myCalendar;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.createtimetable.DialogViewItemAdapter;
import in.junctiontech.school.createtimetable.SlotsDetails;

/**
 * Created by JAYDEVI BHADE on 20-07-2017.
 */

public class ViewTimetableDetailActivity extends AppCompatActivity {
    private ProgressDialog progressbar;
    private SharedPreferences sp;
    private Gson gson;
    private boolean logoutAlert;
    private String dbName;
    private int colorIs;
    private ArrayList<SlotsDetails> slotList = new ArrayList<>();
    private RecyclerView rv_view_timetable_detail;
    private DialogViewItemAdapter adapter;
    private TextView tv_date;
    private ViewPager  viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_view_timetable_pager);
        sp = Prefs.with(this).getSharedPreferences();
        colorIs = getIntent().getIntExtra("color", 0);
        viewPager = (ViewPager) findViewById(R.id.vp_view_timetable_pager);




    }

}
