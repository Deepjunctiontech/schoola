package in.junctiontech.school.createtimetable;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 20-07-2017.
 */

public class DialogViewItemAdapter extends RecyclerView.Adapter<DialogViewItemAdapter.MyViewHolder> {


    private final Integer[] drawableListArray= CreateSlotsActivity.drawableListArray,
            nameDrawableListArray= CreateSlotsActivity.nameDrawableListArray;
    private final ArrayList<String> nameEnglishListArray = new ArrayList<>();
    private Calendar cal;
    private Context createSlotsActivity;
    private ArrayList<TimeTableSlot> slotList;
    private boolean isDelete;
    private int appColor;

    public DialogViewItemAdapter(Context createSlotsActivity, ArrayList<TimeTableSlot> slotList, boolean isDelete, int appColor) {
        this.isDelete = isDelete;
        this.createSlotsActivity = createSlotsActivity;
        this.slotList = slotList;
        this.appColor = appColor;


        // Get Current Time
        cal = Calendar.getInstance();

        nameEnglishListArray.add("Lecture");
        nameEnglishListArray.add("Lab");
        nameEnglishListArray.add("Games");
        nameEnglishListArray.add("Prayer");
        nameEnglishListArray.add("Yoga");
        nameEnglishListArray.add("Music");
        nameEnglishListArray.add("Breakfast");
        nameEnglishListArray.add("Lunch");
        nameEnglishListArray.add("Dinner");
        nameEnglishListArray.add("Dance");
        nameEnglishListArray.add("Art & Craft");
        nameEnglishListArray.add("Exam");
        nameEnglishListArray.add("Break");
        nameEnglishListArray.add("Activity");
        nameEnglishListArray.add("library");


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DialogViewItemAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_time_table_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final TimeTableSlot obj = slotList.get(position);
        holder.tv_slot_start_time.setText(obj.SlotStartTime);
        holder.tv_slot_end_time.setText(obj.SlotEndTime);
        holder.tv_slot_name.setText(obj.SlotName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.SubjectName))){
            holder.tv_slot_subject.setVisibility(View.GONE);
        }else{
            holder.tv_slot_subject.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_subject.setText(obj.SubjectName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.StaffName))){
            holder.tv_slot_teacher.setVisibility(View.GONE);
        }else{
            holder.tv_slot_teacher.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_teacher.setText(obj.StaffName);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.Chapter))){
            holder.tv_slot_chapter.setVisibility(View.GONE);
        }else{
            holder.tv_slot_chapter.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_chapter.setText(obj.Chapter);

        if ("".equalsIgnoreCase(Strings.nullToEmpty(obj.Topic))){
            holder.tv_slot_topic.setVisibility(View.GONE);
        }else{
            holder.tv_slot_topic.setVisibility(View.VISIBLE);
        }
        holder.tv_slot_topic.setText(obj.Topic);

        int dblPos = nameEnglishListArray.indexOf(obj.Type);
        holder.iv_item_view_type.setImageDrawable(createSlotsActivity.getResources().getDrawable(drawableListArray[dblPos == -1 ? 0 : dblPos]));

        if (isDelete) holder.item_slot_delete.setVisibility(View.VISIBLE);
        else holder.item_slot_delete.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    public void updateList(ArrayList<TimeTableSlot> slotList) {
        this.slotList = slotList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView iv_item_view_type;
        private final TextView tv_slot_start_time, tv_slot_end_time, tv_slot_name,
                tv_slot_subject,tv_slot_teacher,tv_slot_chapter,tv_slot_topic;
        private final ImageButton item_slot_delete;


        public MyViewHolder(View itemView) {
            super(itemView);
            iv_item_view_type =  itemView.findViewById(R.id.iv_item_view_type);
            tv_slot_start_time =  itemView.findViewById(R.id.tv_slot_start_time);
            tv_slot_end_time =  itemView.findViewById(R.id.tv_slot_end_time);

            tv_slot_name =  itemView.findViewById(R.id.tv_slot_name);
            tv_slot_subject =  itemView.findViewById(R.id.tv_slot_subject);
            tv_slot_teacher =  itemView.findViewById(R.id.tv_slot_teacher);
            tv_slot_chapter =  itemView.findViewById(R.id.tv_slot_chapter);
            tv_slot_topic =  itemView.findViewById(R.id.tv_slot_topic);


            item_slot_delete =  itemView.findViewById(R.id.item_slot_delete);
            tv_slot_start_time.setTextColor(appColor);
            tv_slot_teacher.setTextColor(appColor);
            tv_slot_subject.setTextColor(appColor);
//            tv_slot_end_time.setTextSize(17f);

            itemView.setOnClickListener(v -> {
                Dialog alertDialog = new Dialog(createSlotsActivity);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(alertDialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                alertDialog.show();
                alertDialog.getWindow().setAttributes(lp);

                View itemTimeMappingView = LayoutInflater.from(createSlotsActivity).inflate(R.layout.layout_view_timetable_detail, null);
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TimeTableSlot obj = slotList.get(getLayoutPosition());
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_topic_name)).setText(obj.Topic);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_slot_chapter_name)).setText(obj.Chapter);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_slot_subject_name)).setText(obj.SubjectName);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_slot_teacher_name)).setText(obj.StaffName);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_slot_name)).setText(obj.SlotName);

                Calendar calDayName = Calendar.getInstance();
                if (null != obj.Date) {
                    try {
                        calDayName.setTime(DateWiseActivity.dateFormat.parse(obj.Date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_day_name)).setText(
                        new SimpleDateFormat("EEEE").format(calDayName.getTime())
                );

                ((TextView) itemTimeMappingView.findViewById(R.id.tv_layout_view_detail_slot_time)).setText(obj.SlotStartTime + " - " + obj.SlotEndTime);
                int dblPos = nameEnglishListArray.indexOf(obj.Type);
                ((CircleImageView) itemTimeMappingView.findViewById(R.id.civ_layout_view_detail_slot_icon)).setImageDrawable(createSlotsActivity.getResources().getDrawable(drawableListArray[dblPos == -1 ? 0 : dblPos]));

                LinearLayout ll_layout_view_detail =  itemTimeMappingView.findViewById(R.id.ll_layout_view_detail);
                ll_layout_view_detail.setBackgroundColor(appColor);

                alertDialog.setContentView(itemTimeMappingView);
                alertDialog.setCancelable(true);
                alertDialog.show();
            });
        }
    }

}
