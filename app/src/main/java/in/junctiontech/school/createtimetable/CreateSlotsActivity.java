package in.junctiontech.school.createtimetable;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import androidx.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class CreateSlotsActivity extends AppCompatActivity implements View.OnClickListener {
    MainDatabase mDb;
    private int colorIs;
    private FloatingActionButton fab, fbtn_add_new, fab_delete;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private RecyclerView rv_owner_school_list;
    private ArrayList<SlotsDetails> slotList = new ArrayList<>();
    private SlotTimeAdapter adapter;
    private boolean isFabOpen;
    private View snackbar;
    private Button btn_timetable_save;
    private Spinner sp_class_list, sp_day_name;
    private Button btn_from_date, btn_to_date;
    private Calendar cal;
    public static SimpleDateFormat time = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    private ProgressDialog progressbar;
    private SharedPreferences sp;
    private Gson gson;
    private ArrayList<ClassNameSectionName> classListData = new ArrayList<>();
    private boolean logoutAlert;
    private String dbName;
    private ArrayList<String> classIdList = new ArrayList<>(), sectionIdList = new ArrayList<>(), sectionNameList = new ArrayList<>();
    private ArrayAdapter<String> classAdapter;

    private ArrayList<String> deleteSlotsId = new ArrayList<>();
    private String[] week_list;
    private CheckBox ck_timetable_holiday;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static final Integer[] drawableListArray = new Integer[]{
            R.drawable.ic_lecture_icon,
            R.drawable.ic_lab,
            R.drawable.ic_games,
            R.drawable.ic_prayer,
            R.drawable.ic_yoga,
            R.drawable.ic_music,
            R.drawable.ic_breakfast,
            R.drawable.ic_lunch,
            R.drawable.ic_dinner,
            R.drawable.ic_dance,
            R.drawable.ic_art_and_craft,
            R.drawable.ic_test,
            R.drawable.ic_break_time,
            R.drawable.ic_activity,
            R.drawable.ic_library,
    };
    public static final Integer[] nameDrawableListArray = new Integer[]{
            R.string.lecture,
            R.string.lab,
            R.string.games,
            R.string.prayer,
            R.string.yoga,
            R.string.music,
            R.string.breakfast,
            R.string.lunch,
            R.string.dinner,
            R.string.dance,
            R.string.art_and_craft,
            R.string.exam,
            R.string.break_time,
            R.string.activity,
            R.string.library,

    };
    private View ll_click_to_view_slot_detail_demo, ll_click_to_view_slot_detail_demo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table_mapping);

        mDb = MainDatabase.getDatabase(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        sp = Prefs.with(this).getSharedPreferences();
        /* ********************************    Demo Screen *******************************/
//        final View page_demo = findViewById(R.id.rl_timetable_mapping_activity_demo);
//        ll_click_to_view_slot_detail_demo = findViewById(R.id.ll_click_to_view_slot_detail_demo);
//        ll_click_to_view_slot_detail_demo1 = findViewById(R.id.ll_click_to_view_slot_detail_demo1);
//
//
//        if (sp.getBoolean("DemoCreateSlot", true)) {
//            page_demo.setVisibility(View.VISIBLE);
//        } else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoCreateSlot", false).commit();
//            }
//        });

        /* ***************************************************************/


        colorIs = Config.getAppColor(this, true);
        gson = new Gson();
//        dbName = Gc.getSharedPreference(Gc.ERPDBNAME,this);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setScreenColor();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        cal = Calendar.getInstance();
        initViews();

        week_list = getResources().getStringArray(R.array.week_list);
        setUpRecycler();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Objects.requireNonNull(intent.getAction()).equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    if (classListData.size() == 0) //getClassListFromServer();
                    if (slotList.size() == 0) getSlotList();

                }
            }
        };
    }

    private void initViews() {
        (findViewById(R.id.et_timetable_mapping_comments)).setVisibility(View.GONE);

        fab = (findViewById(R.id.fbtn_create_time_slot));
        fab.setVisibility(View.VISIBLE);

        fbtn_add_new =  findViewById(R.id.fbtn_create_time_slot_add_new);

        fab_delete =  findViewById(R.id.fbtn_create_time_slot_delete);
        fab.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fbtn_add_new.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fab_delete.setBackgroundTintList(ColorStateList.valueOf(colorIs));

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fbtn_add_new.setOnClickListener(this);
        fab_delete.setOnClickListener(this);

        rv_owner_school_list =  findViewById(R.id.rv_timetable_mapping_list);
        sp_class_list =  findViewById(R.id.sp_timetable_mapping_activity_class_list);
        sp_day_name =  findViewById(R.id.sp_timetable_mapping_activity_day_name);


        btn_from_date =  findViewById(R.id.btn_timetable_mapping_activity_from_date);
        btn_to_date =  findViewById(R.id.btn_timetable_mapping_activity_to_date);
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
        btn_from_date.setText(time.format(cal.getTime()));
        btn_from_date.setOnClickListener(v -> {
            Calendar calTemp = Calendar.getInstance();
            try {
                calTemp.setTime(CreateSlotsActivity.time.parse(btn_from_date.getText().toString()));
            } catch (ParseException e) {
                calTemp.setTimeInMillis(cal.getTimeInMillis());
                e.printStackTrace();
            }

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(CreateSlotsActivity.this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            Log.e("TIME_FROM", hourOfDay + "-" + minute);
                            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            cal.set(Calendar.MINUTE, minute);
//                                cal.set(Calendar.SECOND, 0);
                            btn_from_date.setText(time.format(cal.getTime()));
                        }
                    },
                    calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });
        cal.set(Calendar.HOUR_OF_DAY, 14);
        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
        btn_to_date.setText(time.format(cal.getTime()));
        btn_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calTemp = Calendar.getInstance();
                try {
                    calTemp.setTime(CreateSlotsActivity.time.parse(btn_to_date.getText().toString()));
                } catch (ParseException e) {
                    calTemp.setTimeInMillis(cal.getTimeInMillis());
                    e.printStackTrace();
                }
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(CreateSlotsActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                Log.e("TIME_TO", hourOfDay + "-" + minute);
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                cal.set(Calendar.MINUTE, minute);
//                                cal.set(Calendar.SECOND, 0);
                                btn_to_date.setText(time.format(cal.getTime()));
                            }
                        },
                        calTemp.get(Calendar.HOUR_OF_DAY), calTemp.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
        });


        sp_class_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, classListData.get(position).SectionId)
                        .putString(Config.LAST_USED_CLASS_ID, classListData.get(position).ClassId)
                        .apply();

                getSlotList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_day_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSlotList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        getClassListFromServer();

    }

    private void getSlotList() {

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {

            progressbar.show();
            final JSONObject param = new JSONObject();
            try {
                param.put("SectionID", classListData.get(sp_class_list.getSelectedItemPosition()).SectionId);
                param.put("Day", week_list[sp_day_name.getSelectedItemPosition()]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
//            param1.put("filter", (new JSONObject(param)));
            String getClassUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/sectionTimeSlot/"
                    + param
                    ;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getClassUrl,new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject job) {
                            ck_timetable_holiday.setChecked(false);

                            slotList = new ArrayList<>();
                            if (!isFinishing())
                                progressbar.cancel();
                            try {
                                if (job.optString("code").equalsIgnoreCase("200")) {

                                    JSONObject result = job.getJSONObject("result");
                                    ArrayList<SectionSlotDetail> slotObjMain = gson.fromJson(result.optString("SectionTimeSlot"), new TypeToken<List<SectionSlotDetail>>() {
                                    }.getType());

                                    SectionSlotDetail slotObj = slotObjMain.get(0);
                                    try {
                                        if (slotObj != null)
                                            cal.setTime(time.parse(null == slotObj.getSectionStartTime() ? "" : slotObj.getSectionStartTime()));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    btn_from_date.setText(null == slotObj.getSectionStartTime() ? "00:00" : slotObj.getSectionStartTime());
                                    btn_to_date.setText(null == slotObj.getSectionEndTime() ? "00:00" : slotObj.getSectionEndTime());

                                    slotList = slotObj.getSlots();
                                    if (slotObj.getHoliday()== 1) {
                                        ck_timetable_holiday.setChecked(true);
                                    } else ck_timetable_holiday.setChecked(false);

                                } else if (job.optString("code").equalsIgnoreCase("503")
                                        ||
                                        job.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, job.optInt("code") + "", job.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (job.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                else
                                    Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Collections.sort(slotList, new Comparator<SlotsDetails>() {
                                @Override
                                public int compare(SlotsDetails o1, SlotsDetails o2) {
                                    Calendar calLeft = Calendar.getInstance();
                                    Calendar calRight = Calendar.getInstance();
                                    try {
                                        calLeft.setTime(CreateSlotsActivity.time.parse(o1.getSlotStartTime()));
                                    } catch (ParseException e) {
                                        calLeft.setTimeInMillis(cal.getTimeInMillis());
                                        e.printStackTrace();
                                    }
                                    try {
                                        calRight.setTime(CreateSlotsActivity.time.parse(o2.getSlotStartTime()));
                                    } catch (ParseException e) {
                                        calRight.setTimeInMillis(cal.getTimeInMillis());
                                        e.printStackTrace();
                                    }
                                    return calLeft.compareTo(calRight);
                                }
                            });
                            adapter.updateList(slotList);
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getSlotData", volleyError.toString());
                    slotList = new ArrayList<>();
                    adapter.updateList(slotList);
                    progressbar.cancel();
                    ck_timetable_holiday.setChecked(false);
                    Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
                }
            }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }


            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }
    }

    private void getSlotListForCopy(String classId, String  dayName) {
        progressbar.show();
        JSONObject param = new JSONObject();
        try {
            param.put("SectionID", classId);

            param.put("Day", dayName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
//        param1.put("filter", (new JSONObject(param)));

        String getClassUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "timeTable/sectionTimeSlot/"
                + param
                ;


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getClassUrl,new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        ck_timetable_holiday.setChecked(false);
                        slotList = new ArrayList<>();
                        if (!isFinishing())
                            progressbar.cancel();
                        try {
                            if (job.optString("code").equalsIgnoreCase("200")) {
                                JSONObject result = job.getJSONObject("result");
                                ArrayList<SectionSlotDetail> slotObjMain = gson.fromJson(result.optString("SectionTimeSlot"), new TypeToken<List<SectionSlotDetail>>() {
                                }.getType());

                                SectionSlotDetail slotObj = slotObjMain.get(0);
                                try {
                                    if (slotObj != null)
                                        cal.setTime(time.parse(null == slotObj.getSectionStartTime() ? "" : slotObj.getSectionStartTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                btn_from_date.setText(null == slotObj.getSectionStartTime() ? "00:00" : slotObj.getSectionStartTime());
                                btn_to_date.setText(null == slotObj.getSectionEndTime() ? "00:00" : slotObj.getSectionEndTime());

                                slotList = slotObj.getSlots();
                                if (slotObj.getHoliday()== 1 ) {
                                    ck_timetable_holiday.setChecked(true);
                                } else ck_timetable_holiday.setChecked(false);

                            } else if (job.optString("code").equalsIgnoreCase("503")
                                    ||
                                    job.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, job.optInt("code") + "", job.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (job.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else
                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(slotList, new Comparator<SlotsDetails>() {
                            @Override
                            public int compare(SlotsDetails o1, SlotsDetails o2) {
                                Calendar calLeft = Calendar.getInstance();
                                Calendar calRight = Calendar.getInstance();
                                try {
                                    calLeft.setTime(CreateSlotsActivity.time.parse(o1.getSlotStartTime()));
                                } catch (ParseException e) {
                                    calLeft.setTimeInMillis(cal.getTimeInMillis());
                                    e.printStackTrace();
                                }
                                try {
                                    calRight.setTime(CreateSlotsActivity.time.parse(o2.getSlotStartTime()));
                                } catch (ParseException e) {
                                    calRight.setTimeInMillis(cal.getTimeInMillis());
                                    e.printStackTrace();
                                }
                                return calLeft.compareTo(calRight);
                            }
                        });
                        for (SlotsDetails obj:slotList )  obj.setSlotID("");
                        adapter.updateList(slotList);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getSlotData", volleyError.toString());
                slotList = new ArrayList<>();
                adapter.updateList(slotList);
                progressbar.cancel();
                ck_timetable_holiday.setChecked(false);
                Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }


        };


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }


    public void showSnackClassList() {

        Snackbar snackbarObj = Snackbar.make(
                snackbar,
                getString(R.string.classes_not_available),
                Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (progressbar != null && progressbar.isShowing()) progressbar.dismiss();
                        if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                            Toast.makeText(CreateSlotsActivity.this,"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                        }else { startActivity(new Intent(CreateSlotsActivity.this, ClassSectionActivity.class));
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.nothing);}
                    }
                });
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();

    }


    private void setScreenColor() {
        snackbar = findViewById(R.id.rl_timetable_mapping_activity);
        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_class_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_school_start_time_title)).setTextColor(colorIs);


        TextView tv_timetable_mapping_activity_from_date = (TextView) findViewById(R.id.tv_timetable_mapping_activity_from_date);
        tv_timetable_mapping_activity_from_date.setText(getString(R.string.school_start_time));
        tv_timetable_mapping_activity_from_date.setTextColor(colorIs);
        TextView tv_timetable_mapping_activity_to_date = (TextView) findViewById(R.id.tv_timetable_mapping_activity_to_date);
        tv_timetable_mapping_activity_to_date.setText(getString(R.string.school_end_time));
        tv_timetable_mapping_activity_to_date.setTextColor(colorIs);
        btn_timetable_save = (Button) findViewById(R.id.btn_timetable_save);
        btn_timetable_save.setBackgroundColor(colorIs);
        ck_timetable_holiday = (CheckBox) findViewById(R.id.ck_timetable_holiday);
        ck_timetable_holiday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rv_owner_school_list.setVisibility(isChecked ? View.GONE : View.VISIBLE);
                if (isChecked) {
                    isFabOpen = true;
                    animateFAB();
                    fab.startAnimation(fab_close);
                } else {
                    fab.startAnimation(fab_open);
                }
            }
        });
        btn_timetable_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ck_timetable_holiday.isChecked())
                        postHoliday();
                    else
                        postSlots();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void postHoliday() throws JSONException {

        JSONObject xyz = new JSONObject();

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {
            progressbar.show();
            String sectionId = classListData.get(sp_class_list.getSelectedItemPosition()).SectionId;
            xyz.put("SectionID", new JSONArray().put(sectionId) );
            String day = week_list[sp_day_name.getSelectedItemPosition()];
            xyz.put("Day", new JSONArray().put(day));
            xyz.put("Holiday", 1);

            final JSONArray param = new JSONArray();
            param.put(xyz);

            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/sectionTimeSlot";


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {

                            if (!isFinishing())
                                progressbar.cancel();

                            if (jsonObject.optString("code").equals("200")) {
                                Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar, getResources().getColor(android.R.color.holo_green_dark));


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else
                                Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar);


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("POSTSLotData", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
                }
            }
            )
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() {
                    try {
                        return param == null ? null : param.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                        return null;
                    }
                }
            }

                    ;
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjReq);
        } else showSnackClassList();
    }

    private void postSlots() throws Exception {

        final JSONArray param = new JSONArray();

        JSONObject jsonObj = new JSONObject();

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {
            progressbar.show();
            String sectionId = classListData.get(sp_class_list.getSelectedItemPosition()).SectionId;

            jsonObj.put("SectionID", new JSONArray().put(sectionId));
            String day = week_list[sp_day_name.getSelectedItemPosition()];
            JSONArray dayAr = (new JSONArray().put(day));
            jsonObj.put("Day", dayAr);
            jsonObj.put("SectionStartTime", btn_from_date.getText().toString());
            jsonObj.put("SectionEndTime", btn_to_date.getText().toString());
            jsonObj.put("Holiday", 0);

            JSONArray slotsArray = new JSONArray();
            for (SlotsDetails slotObj : slotList) {
                JSONObject jsonObjSlot = new JSONObject();
                jsonObjSlot.put("SlotName", slotObj.getSlotName());
                jsonObjSlot.put("SlotStartTime", slotObj.getSlotStartTime());
                jsonObjSlot.put("SlotEndTime", slotObj.getSlotEndTime());
                jsonObjSlot.put("Type", slotObj.getType());
                slotsArray.put(jsonObjSlot);
            }
            jsonObj.put("Slots", slotsArray);

            param.put(jsonObj);
//            jsonObject.put("data", (new JSONArray()).put(jsonObj));

            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/sectionTimeSlot";

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("PostSlotData", jsonObject.toString());
                            if (!isFinishing())
                                progressbar.cancel();

                            if (jsonObject.optString("code").equals("200") ) {
                                Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar, getResources().getColor(android.R.color.holo_green_dark));


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else
                                Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar);

//                            try {
//                                deleteSlots();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("POSTSLotData", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
                }
            })
            {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        }
        ;
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjReq);
        } else showSnackClassList();
    }



    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fbtn_add_new.startAnimation(fab_close);
            fab_delete.startAnimation(fab_close);
            fbtn_add_new.setClickable(false);
            fab_delete.setClickable(false);
            isFabOpen = false;
            Log.d("Ritu", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fbtn_add_new.startAnimation(fab_open);

            fab_delete.startAnimation(fab_open);
            fbtn_add_new.setClickable(true);

            fab_delete.setClickable(true);
            isFabOpen = true;
            Log.d("Ritu", "open");

        }
    }

    private void setUpRecycler() {
        rv_owner_school_list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SlotTimeAdapter(this, slotList, false, colorIs, deleteSlotsId, cal);
        rv_owner_school_list.setAdapter(adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fbtn_create_time_slot:
                animateFAB();
                break;
            case R.id.fbtn_create_time_slot_add_new:
                fab.performClick();

                if (slotList.size() != 0) {
                    SlotsDetails obb = slotList.get(slotList.size() - 1);
                    try {
                        cal.setTime(CreateSlotsActivity.time.parse(obb.getSlotEndTime()));
                    } catch (ParseException e) {
                        cal.setTimeInMillis(cal.getTimeInMillis());
                        e.printStackTrace();
                    }
                } else {
                    try {
                        cal.setTime(CreateSlotsActivity.time.parse(btn_from_date.getText().toString()));
                    } catch (ParseException e) {
                        cal.setTimeInMillis(cal.getTimeInMillis());
                        e.printStackTrace();
                    }
                }

                String startTime = CreateSlotsActivity.time.format(cal.getTime());
                cal.add(Calendar.HOUR_OF_DAY, 1);
                slotList.add(new SlotsDetails(
                        "",/* Slot Id */
                        "Lecture",/* Slot Name */
                        startTime,/* Slot Start time */
                        CreateSlotsActivity.time.format(cal.getTime()),/* Slot End time */
                        "Lecture"
                ));
                adapter.updateList(slotList);

//                if (sp.getBoolean("DemoCreateSlotViewSlotDetail", true)) {
//                    ll_click_to_view_slot_detail_demo1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.vibrate_shake));
//                    ll_click_to_view_slot_detail_demo.setVisibility(View.VISIBLE);
//                    ll_click_to_view_slot_detail_demo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            ll_click_to_view_slot_detail_demo1.clearAnimation();
//                            ll_click_to_view_slot_detail_demo.setVisibility(View.INVISIBLE);
//                            sp.edit().putBoolean("DemoCreateSlotViewSlotDetail", false).commit();
//                        }
//                    });
//                } else ll_click_to_view_slot_detail_demo.setVisibility(View.INVISIBLE);

                break;
            case R.id.fbtn_create_time_slot_delete:
                fab.performClick();
                adapter.deleteSlot(true);

                break;

        }
    }


    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();

        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, new Observer<List<ClassNameSectionName>>() {
                    @Override
                    public void onChanged(@Nullable List<ClassNameSectionName> classNameSectionNames) {
                        classListData.clear();
                        sectionNameList.clear();
                        classListData.addAll(classNameSectionNames);
                        for(int i = 0; i < classListData.size(); i++){
                            sectionNameList.add(classListData.get(i).ClassName + " " + classListData.get(i).SectionName );
                        }
        if (classAdapter == null) {
            classAdapter = new ArrayAdapter<>(CreateSlotsActivity.this, android.R.layout.simple_list_item_1, sectionNameList);
            classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_class_list.setAdapter(classAdapter);
        } else classAdapter.notifyDataSetChanged();
                    }
                });

//        if (sectionNameList.size() > 0) {
//            sp_class_list.setSelection(lastUsedSectionIdPos);
//        } else showSnackClassList();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this))
                .removeObservers(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_copy, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_copy_from:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                View itemTimeMappingView = LayoutInflater.from(this).inflate(R.layout.layout_copy_slot_from, null);

                ((TextView) itemTimeMappingView.findViewById(R.id.tv_timetable_mapping_activity_class_list)).setTextColor(colorIs);
                ((TextView) itemTimeMappingView.findViewById(R.id.tv_timetable_copy_from_day_name)).setTextColor(colorIs);
                final Spinner sp_class_list_for_copy = (Spinner) itemTimeMappingView.findViewById(R.id.sp_timetable_copy_from_class_list);
                final Spinner sp_day_name_for_copy = (Spinner) itemTimeMappingView.findViewById(R.id.sp_timetable_copy_from_day_name);

                ArrayAdapter<String> classSectionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sectionNameList);
                classSectionAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
                sp_class_list_for_copy.setAdapter(classSectionAdapter);
                alertDialog.setTitle(getString(R.string.copy_from));


                alertDialog.setView(itemTimeMappingView);
                alertDialog.setPositiveButton(R.string.copy, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (classListData.size() >= sp_class_list_for_copy.getSelectedItemPosition() && sp_class_list_for_copy.getSelectedItemPosition() != -1) {
                            getSlotListForCopy(classListData.get(sp_class_list_for_copy.getSelectedItemPosition()).SectionId,week_list[sp_day_name_for_copy.getSelectedItemPosition()]);
                        }
                    }
                });
                alertDialog.setNegativeButton(R.string.cancel, null);
                alertDialog.show();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    //    private void deleteSlots() throws JSONException {
//        deleteSlotsId = adapter.getDeleteSlotsId();
//
//        JSONArray slotArray = new JSONArray();
//        for (int slotPos = 0; slotPos < deleteSlotsId.size(); slotPos++) {
//
//            progressbar.show();
//            try {
//                slotArray.put(new JSONObject().put("SlotID", deleteSlotsId.get(slotPos)));
//            } catch (JSONException e) {
//                e.printStackTrace();
//                progressbar.dismiss();
//            }
//        }
//
//
//        if (slotArray.length() > 0) {
//
//            final JSONObject param_main = new JSONObject();
//
//            //  param_main.put("data", new JSONObject(param));
//            param_main.put("filter", slotArray);
//            param_main.put("action", "slot");
//
//            Log.e("deleteSlot", param_main.toString());
//
//            String deleteStaffUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
//                    + Config.applicationVersionNameValue
//                    + "SectionTimeSlot.php?databaseName="
//                    + dbName
//                    +"&data="
//                    + new JSONObject().put("data", param_main);
//
//            Log.d("deleteSlotUrl", deleteStaffUrl);
//            StringRequest request = new StringRequest(Request.Method.DELETE,
//                    deleteStaffUrl
//                    ,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String s) {
//                            DbHandler.longInfo(s);
//                            Log.e("deleteSlotRes", s);
//                            progressbar.cancel();
//                            try {
//                                JSONObject jsonObject = new JSONObject(s);
//
//                                if (jsonObject.optInt("code") == 200 && !isFinishing()) {
//                                    Toast.makeText(CreateSlotsActivity.this, getString(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();
//
//                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                        ||
//                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
//                                    if (!logoutAlert & !(CreateSlotsActivity.this).isFinishing()) {
//                                        Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                        logoutAlert = true;
//                                    }
//                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//                                else if (jsonObject.optString("code").equalsIgnoreCase("401"))
//                                    Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                            getSlotList();
//                        }
//
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError volleyError) {
//                    Log.e("deleteResultSlot", volleyError.toString());
//                    progressbar.cancel();
//
//                    Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//
//                    Map<String, String> header = new LinkedHashMap<String, String>();
//                    header.put("Content-Type", "application/x-www-form-urlencoded");
//                    return super.getHeaders();
//                }
//
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new LinkedHashMap<String, String>();
//
//                    return params;
//                }
//            };
//
//            request.setRetryPolicy(new DefaultRetryPolicy(0,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//            AppRequestQueueController.getInstance(CreateSlotsActivity.this).addToRequestQueue(request);
//        } else getSlotList();
//
//    }


    //    public void updateClass(ArrayList<CreateClass> classListData) {
//        this.classListData = classListData;
//        classIdList = new ArrayList<>();
//        sectionIdList = new ArrayList<>();
//        sectionNameList = new ArrayList<>();
//
//        int lastUsedSectionIdPos = 0;
//        int jj = 0;
//
//        for (int cls = 0; cls < classListData.size(); cls++) {
//            CreateClass obj = classListData.get(cls);
//            ArrayList<CreateClass> secList = obj.getSectionList();
//            for (int sec = 0; sec < secList.size(); sec++) {
//                classIdList.add(obj.getClassId());
//                sectionIdList.add(secList.get(sec).getSectionId());
//                sectionNameList.add(obj.getClassName() + " " + secList.get(sec).getSectionName());
//
//                if (sp.getString(Config.LAST_USED_SECTION_ID, "").
//                        equalsIgnoreCase(secList.get(sec).getSectionId())) {
//                    lastUsedSectionIdPos = jj;
//                }
//                jj++;
//            }
//        }
//
//
//        if (classAdapter == null) {
//            classAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sectionNameList);
//            classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
//            sp_class_list.setAdapter(classAdapter);
//        } else this.classAdapter.notifyDataSetChanged();
//
//
//        if (sectionNameList.size() > 0) {
//            sp_class_list.setSelection(lastUsedSectionIdPos);
//        } else showSnackClassList();
//
//    }


    //    private void getClassListFromServer() {
//
//        progressbar.show();
//        String getClassUrl = sp.getString("HostName", "Not Found")
//                + sp.getString(Config.applicationVersionName, "Not Found")
//                + "ClassApi.php?databaseName=" +
//                dbName +
//                "&action=join&session=" + sp.getString(Config.SESSION, "");
//
//        Log.d("getClassUrl", getClassUrl);
//        StringRequest request = new StringRequest(Request.Method.GET,
//                getClassUrl
//                ,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String s) {
//                        DbHandler.longInfo(s);
//                        Log.e("getClassData", s);
//                        if (!isFinishing())
//                            progressbar.cancel();
//                        try {
//                            JSONObject jsonObject = new JSONObject(s);
//                            if (jsonObject.optInt("code") == 200) {
//                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
//                                }.getType());
//                                classListData = obj.getResult();
//
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
//                                if (!logoutAlert & !isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(CreateSlotsActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                    logoutAlert = true;
//                                }
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        updateClass(classListData);
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e("getClassData", volleyError.toString());
//                progressbar.cancel();
//                Config.responseVolleyErrorHandler(CreateSlotsActivity.this, volleyError, snackbar);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> header = new LinkedHashMap<String, String>();
//                header.put("Content-Type", "application/x-www-form-urlencoded");
//                return super.getHeaders();
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new LinkedHashMap<String, String>();
//                return params;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//
//        //}
//    }

}
