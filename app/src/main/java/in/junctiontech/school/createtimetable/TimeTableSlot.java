package in.junctiontech.school.createtimetable;

public class TimeTableSlot {
    public String SectionID;
    public String Date;
    public String SlotID;
    public String Holiday;
    public String SlotName;
    public String SlotStartTime;
    public String SlotEndTime;
    public String Subject;
    public String Teacher;
    public String Chapter;
    public String Topic;
    public String Comment;
    public String CreatedBy;
    public String CreatedDate;
    public String Flag;
    public String Type;
    public String SubjectName;
    public String StaffName;
    public String SectionName;
    public String ClassName;
}
