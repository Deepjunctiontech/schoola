package in.junctiontech.school.createtimetable;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.DB.SchoolSubjectsEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.ClassNameSectionName;

public class DayWiseActivity extends AppCompatActivity implements View.OnClickListener {

    MainDatabase mDb;
    SimpleDateFormat time;


    private RecyclerView rv_timetable_mapping_list;
    private ArrayList<TimeTableSlot> slotList = new ArrayList<>();
    private int colorIs;
    private View snackbar;
    private DayWiseAdapter adapter;
    private boolean isFabOpen;
    private Button btn_timetable_save;
    private Spinner sp_class_list, sp_day_name;
    private Button btn_from_date, btn_to_date;
    private Calendar cal;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    private ProgressDialog progressbar;
    private SharedPreferences sp;
    private Gson gson;
    private ArrayList<ClassNameSectionName> classListData = new ArrayList<>();
    private boolean logoutAlert;
    private String dbName;
    private ArrayList<String>  sectionNameList = new ArrayList<>();
    private ArrayAdapter<String> classAdapter;

    private String[] week_list;
    private CheckBox ck_timetable_holiday;
//    private LinearLayout.LayoutParams param;
    private FloatingActionButton fab, fbtn_add_new, fab_delete;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private ArrayList<SchoolStaffEntity> staffListData = new ArrayList<>();
    private ArrayList<SchoolSubjectsEntity> subjectListData = new ArrayList<>();
    private EditText et_comments;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private View ll_click_to_view_slot_detail_demo, ll_click_to_view_slot_detail_demo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table_mapping);
        sp = Prefs.with(this).getSharedPreferences();

        mDb = MainDatabase.getDatabase(this);

        /* ********************************    Demo Screen *******************************/
//        final RelativeLayout page_demo = (RelativeLayout) findViewById(R.id.rl_timetable_mapping_activity_demo);
//        ll_click_to_view_slot_detail_demo = page_demo;
//        ll_click_to_view_slot_detail_demo1 = findViewById(R.id.ll_click_to_view_slot_detail_demo1);
//
//        if (sp.getBoolean("DemoDayWiseActivity", true)) {
//            page_demo.setVisibility(View.VISIBLE);
//        } else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoDayWiseActivity", false).commit();
//            }
//        });

        /* ***************************************************************/


        colorIs = Config.getAppColor(this, true);
        time = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setScreenColor();
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cal = Calendar.getInstance();
        initViews();
        week_list = getResources().getStringArray(R.array.week_list);
//        getStaffDataFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    if (staffListData.size() == 0)
//                        getStaffDataFromServer();
                    if (classListData.size() == 0)
//                        getClassListFromServer();
                    if (subjectListData.size() == 0)
//                        try {
//                        getSubjectClassWise();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                    if (slotList.size() == 0) try {
                        getSlotList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
    }


    @Override
    protected void onResume() {
        super.onResume();

        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this))
                .observe(this, classNameSectionNames -> {
                    classListData.clear();
                    sectionNameList.clear();

                    classListData.addAll(classNameSectionNames);
                    for(int i = 0; i < classListData.size(); i++){
                        sectionNameList.add(classListData.get(i).ClassName + " " + classListData.get(i).SectionName );
                    }
                    if (classAdapter == null) {
                        classAdapter = new ArrayAdapter<>(DayWiseActivity.this, android.R.layout.simple_list_item_1, sectionNameList);
                        classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
                        sp_class_list.setAdapter(classAdapter);
                    } else classAdapter.notifyDataSetChanged();
                });

        mDb.schoolStaffModel().getAllStaff().observe(this, schoolStaffEntities -> {
            staffListData.clear();
            staffListData.addAll(schoolStaffEntities);
            adapter.updateStaffList(staffListData);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDb.schoolClassSectionModel()
                .getClassNameSectionList(Gc.getSharedPreference(Gc.APPSESSION, this))
                .removeObservers(this);

        mDb.schoolStaffModel().getAllStaff().removeObservers(this);
    }

    private void initViews() {
        et_comments =  findViewById(R.id.et_timetable_mapping_comments);
        fab = findViewById(R.id.fbtn_create_time_slot);
        fab.setVisibility(View.VISIBLE);

        fbtn_add_new =  findViewById(R.id.fbtn_create_time_slot_add_new);

        fab_delete = findViewById(R.id.fbtn_create_time_slot_delete);
        fab.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fbtn_add_new.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        fab_delete.setBackgroundTintList(ColorStateList.valueOf(colorIs));

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fbtn_add_new.setOnClickListener(this);
        fab_delete.setOnClickListener(this);

        rv_timetable_mapping_list =  findViewById(R.id.rv_timetable_mapping_list);
        setUpRecycler();
        sp_day_name = findViewById(R.id.sp_timetable_mapping_activity_day_name);
        sp_class_list =  findViewById(R.id.sp_timetable_mapping_activity_class_list);

        btn_from_date =  findViewById(R.id.btn_timetable_mapping_activity_from_date);
        btn_from_date.setText(dateFormat.format(cal.getTime()));
        btn_from_date.setOnClickListener(v -> {
            Calendar calTemp = Calendar.getInstance();
            try {
                calTemp.setTime(dateFormat.parse(btn_from_date.getText().toString()));
            } catch (ParseException e) {
                calTemp.setTimeInMillis(cal.getTimeInMillis());
                e.printStackTrace();
            }

            // Launch Date Picker Dialog
            DatePickerDialog timePickerDialog = new DatePickerDialog(DayWiseActivity.this,
                    (view, year, month, dayOfMonth) -> {
                        cal.set(Calendar.YEAR, year);
                        cal.set(Calendar.MONTH, month);
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        btn_from_date.setText(dateFormat.format(cal.getTime()));

                    },
                    calTemp.get(Calendar.YEAR), calTemp.get(Calendar.MONTH), calTemp.get(Calendar.DAY_OF_MONTH));
            timePickerDialog.show();
        });
        btn_to_date =  findViewById(R.id.btn_timetable_mapping_activity_to_date);
        btn_to_date.setText(dateFormat.format(cal.getTime()));
        btn_to_date.setText(Gc.getSharedPreference(Gc.APPSESSIONEND, this));
        btn_to_date.setOnClickListener(v -> {
            Calendar calTemp = Calendar.getInstance();
            try {
                calTemp.setTime(dateFormat.parse(btn_to_date.getText().toString()));
            } catch (ParseException e) {
                calTemp.setTimeInMillis(cal.getTimeInMillis());
                e.printStackTrace();
            }

            // Launch Date Picker Dialog
            DatePickerDialog timePickerDialog = new DatePickerDialog(DayWiseActivity.this,
                    (view, year, month, dayOfMonth) -> {
                        cal.set(Calendar.YEAR, year);
                        cal.set(Calendar.MONTH, month);
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        btn_to_date.setText(dateFormat.format(cal.getTime()));

                    },
                    calTemp.get(Calendar.YEAR), calTemp.get(Calendar.MONTH), calTemp.get(Calendar.DAY_OF_MONTH));
            timePickerDialog.show();
        });
        sp_class_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, classListData.get(position).SectionId)
                        .putString(Config.LAST_USED_CLASS_ID, classListData.get(position).ClassId)
                        .apply();

                try {
                    getSubjectForClass();
                    getSlotList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_day_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    getSlotList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        getClassListFromServer();

    }

    private void getSlotList() throws JSONException {

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {
            progressbar.show();
            JSONObject param = new JSONObject();
            param.put("SectionID", classListData.get(sp_class_list.getSelectedItemPosition()).SectionId);
            param.put("Date", btn_from_date.getText().toString());


            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/TimeTableDateWise/"
                    + param
                    ;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,new JSONObject(),
                    job -> {
                        if (!isFinishing())
                            progressbar.cancel();

                        String code = job.optString("code");

                        switch (code) {
                            case Gc.APIRESPONSE200:

                                try {
                                    slotList = new Gson().
                                            fromJson(job.getJSONObject("result").optString("SectionTimeDateWise"), new TypeToken<List<TimeTableSlot>>() {
                                            }.getType());


                                    Collections.sort(slotList, (o1, o2) -> {
                                        Calendar calLeft = Calendar.getInstance();
                                        Calendar calRight = Calendar.getInstance();
                                        try {
                                            calLeft.setTime(CreateSlotsActivity.time.parse(o1.SlotStartTime));
                                        } catch (ParseException e) {
                                            calLeft.setTimeInMillis(cal.getTimeInMillis());
                                            e.printStackTrace();
                                        }
                                        try {
                                            calRight.setTime(CreateSlotsActivity.time.parse(o2.SlotStartTime));
                                        } catch (ParseException e) {
                                            calRight.setTimeInMillis(cal.getTimeInMillis());
                                            e.printStackTrace();
                                        }

                                        return calLeft.compareTo(calRight);

                                    });

                                    if (slotList.size() > 0) {
                                        TimeTableSlot slotObj = slotList.get(0);
                                        if ((slotObj.Holiday).equalsIgnoreCase("1")) {
                                            et_comments.setText(null == slotObj.Comment ? "" : slotObj.Comment);
                                            ck_timetable_holiday.setChecked(true);
                                        } else ck_timetable_holiday.setChecked(false);

                                    } else ck_timetable_holiday.setChecked(false);
                                    adapter.updateList(slotList);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                                break;

                            case Gc.APIRESPONSE204:
                                slotList.clear();
                                adapter.notifyDataSetChanged();
                                Config.responseSnackBarHandler(getString(R.string.timetable_not_available),
                                        findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, DayWiseActivity.this);

                                Intent intent1 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, DayWiseActivity.this);

                                Intent intent2 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                            default:
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                        }

                    }, volleyError -> {
                slotList.clear();
                adapter.updateList(slotList);
                progressbar.cancel();
                Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
            }) {
                @Override
                public Map<String, String> getHeaders()  {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }


            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }else showSnackClassList();
    }


//    public void updateClass(ArrayList<ClassNameSectionName> classListData) {
//        this.classListData = classListData;
//        classIdList = new ArrayList<>();
//        sectionIdList = new ArrayList<>();
//        sectionNameList = new ArrayList<>();
//
//        int lastUsedSectionIdPos = 0;
//        int jj = 0;
//
//        for (int cls = 0; cls < classListData.size(); cls++) {
//            CreateClass obj = classListData.get(cls);
//            ArrayList<CreateClass> secList = obj.getSectionList();
//            for (int sec = 0; sec < secList.size(); sec++) {
//                classIdList.add(obj.getClassId());
//                sectionIdList.add(secList.get(sec).getSectionId());
//                sectionNameList.add(obj.getClassName() + " " + secList.get(sec).getSectionName());
//
//                if (sp.getString(Config.LAST_USED_SECTION_ID, "").
//                        equalsIgnoreCase(secList.get(sec).getSectionId())) {
//                    lastUsedSectionIdPos = jj;
//                }
//                jj++;
//            }
//        }
//
//
//        if (classAdapter == null) {
//            classAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sectionNameList);
//            classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
//            sp_class_list.setAdapter(classAdapter);
//        } else this.classAdapter.notifyDataSetChanged();
//
//
//        if (sectionNameList.size() > 0) {
//            sp_class_list.setSelection(lastUsedSectionIdPos);
//            try {
//                getSubjectClassWise();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        } else showSnackClassList();
//
//
//    }

//    private void getClassListFromServer() {
//
//        progressbar.show();
//        String getClassUrl = sp.getString("HostName", "Not Found")
//                + sp.getString(Config.applicationVersionName, "Not Found")
//                + "ClassApi.php?databaseName=" +
//                dbName +
//                "&action=join&session=" + sp.getString(Config.SESSION, "");
//
//        Log.d("getClassUrl", getClassUrl);
//        StringRequest request = new StringRequest(Request.Method.GET,
//                getClassUrl
//                ,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String s) {
//                        DbHandler.longInfo(s);
//                        Log.e("getClassData", s);
//                        if (!isFinishing())
//                            progressbar.cancel();
//                        try {
//                            JSONObject jsonObject = new JSONObject(s);
//                            if (jsonObject.optInt("code") == 200) {
//                                CreateClass obj = gson.fromJson(s, new TypeToken<CreateClass>() {
//                                }.getType());
//                                classListData = obj.getResult();
//
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
//                                if (!logoutAlert & !isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(DayWiseActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                    logoutAlert = true;
//                                }
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        updateClass(classListData);
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e("getClassData", volleyError.toString());
//                progressbar.cancel();
//                Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> header = new LinkedHashMap<String, String>();
//                header.put("Content-Type", "application/x-www-form-urlencoded");
//                return super.getHeaders();
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new LinkedHashMap<String, String>();
//                return params;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//
//        //}
//    }

    private void setScreenColor() {
        snackbar = findViewById(R.id.rl_timetable_mapping_activity);
        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_class_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_school_start_time_title)).setTextColor(colorIs);

        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_from_date)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_timetable_mapping_activity_to_date)).setTextColor(colorIs);


        btn_timetable_save =  findViewById(R.id.btn_timetable_save);
        btn_timetable_save.setBackgroundColor(colorIs);
        ck_timetable_holiday =  findViewById(R.id.ck_timetable_holiday);
        ck_timetable_holiday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            rv_timetable_mapping_list.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            et_comments.setVisibility(isChecked ? View.VISIBLE : View.GONE);

            fab.setVisibility(isChecked ? View.GONE : View.VISIBLE);
        });
        btn_timetable_save.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(DayWiseActivity.this);
            alertDialog.setTitle(getString(R.string.confirmation) + " !");
            alertDialog.setMessage("Your data will be replaced if it belongs to these dates");
            alertDialog.setPositiveButton(getString(R.string.replace_all), (dialog, which) -> {
                try {
                    if (ck_timetable_holiday.isChecked())
                        postHoliday(true);
                    else
                        postSlotsMapping(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            alertDialog.setNegativeButton(getString(R.string.do_not_replace), (dialog, which) -> {
                try {
                    if (ck_timetable_holiday.isChecked())
                        postHoliday(false);
                    else
                        postSlotsMapping(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            alertDialog.setNeutralButton(getString(R.string.cancel), null);
            alertDialog.setCancelable(false);
            alertDialog.show();

        });
    }

    private void postSlotsMapping(boolean replaceAll) throws JSONException {

        final JSONArray param = new JSONArray();
        JSONObject jsonObj = new JSONObject();

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {

            String sectionId = classListData.get(sp_class_list.getSelectedItemPosition()).SectionId;
            jsonObj.put("SectionID", sectionId);
            jsonObj.put("Day", sp_day_name.getSelectedItem());
            jsonObj.put("FromDate", btn_from_date.getText().toString());
            jsonObj.put("ToDate", btn_to_date.getText().toString());
            jsonObj.put("Holiday", 0);
            jsonObj.put("ReplaceAll", 1);
            jsonObj.put("Comment", et_comments.getText().toString().trim());
            jsonObj.put("CreatedBy", Gc.getSharedPreference(Gc.USERID, this));

            JSONArray slotsArray = new JSONArray();
            for (TimeTableSlot slotObj : slotList
                    ) {
                JSONObject jsonObjSlot = new JSONObject();
                // jsonObjSlot.put("SectionID", sectionId);
                // jsonObjSlot.put("Day", dayAr);
                jsonObjSlot.put("SlotID", slotObj.SlotID);
                jsonObjSlot.put("SlotName", slotObj.SlotName);
                jsonObjSlot.put("SlotStartTime", slotObj.SlotStartTime);
                jsonObjSlot.put("SlotEndTime", slotObj.SlotEndTime);
                jsonObjSlot.put("Subject", slotObj.Subject);
                jsonObjSlot.put("Teacher", slotObj.Teacher);
                jsonObjSlot.put("Type", slotObj.Type);
//                jsonObjSlot.put("FromDate", btn_from_date.getText().toString());
//                jsonObjSlot.put("ToDate", btn_to_date.getText().toString());
                slotsArray.put(jsonObjSlot);
            }
            if (slotsArray.length() > 0) {
                progressbar.show();

                jsonObj.put("Slots", slotsArray);

                param.put(jsonObj);

//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("data", (new JSONArray()).put(jsonObj));

                String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                        + Gc.ERPAPIVERSION
                        + "timeTable/TimeTableDayWise"
                        ;

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                        job -> {

                            if (!isFinishing())
                                progressbar.cancel();

                            String code = job.optString("code");

                            switch (code) {
                                case Gc.APIRESPONSE200:
                                    Config.responseSnackBarHandler(getString(R.string.save_successfully),
                                            findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                                    break;

                                case Gc.APIRESPONSE204:
                                    slotList.clear();
                                    adapter.notifyDataSetChanged();
                                    Config.responseSnackBarHandler(getString(R.string.timetable_not_available),
                                            findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                                    break;

                                case Gc.APIRESPONSE401:

                                    HashMap<String, String> setDefaults1 = new HashMap<>();
                                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                    Gc.setSharedPreference(setDefaults1, DayWiseActivity.this);

                                    Intent intent1 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent1);

                                    finish();

                                    break;

                                case Gc.APIRESPONSE500:

                                    HashMap<String, String> setDefaults = new HashMap<>();
                                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                    Gc.setSharedPreference(setDefaults, DayWiseActivity.this);

                                    Intent intent2 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent2);

                                    finish();

                                    break;

                                default:
                                    Config.responseSnackBarHandler(job.optString("message"),
                                            findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                            }

//                            if (job.optString("code").equalsIgnoreCase("200")) {
//                                Config.responseSnackBarHandler(job.optString("message"), snackbar, getResources().getColor(android.R.color.holo_green_dark));
//
//
//                            } else if (job.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    job.optString("code").equalsIgnoreCase("511")) {
//                                if (!logoutAlert & !isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(DayWiseActivity.this, job.optInt("code") + "", job.optString("message"));
//                                    logoutAlert = true;
//                                }
//                            } else if (job.optString("code").equalsIgnoreCase("502"))
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//                            else
//                                Config.responseSnackBarHandler(job.optString("message"), snackbar);
//
//
////                                try {
////                                    deleteSlots();
////                                } catch (JSONException e) {
////                                    e.printStackTrace();
////                                }
                        }, volleyError -> {
                            Log.e("POSTSLotData", volleyError.toString());
                            progressbar.cancel();
                            Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
                        })
                {
                    @Override
                    public Map<String, String> getHeaders()  {
                        HashMap<String, String> headers = new HashMap<>();

                        headers.put("APPKEY",Gc.APPKEY);
                        headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                        headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                        headers.put("Content-Type", Gc.CONTENT_TYPE);
                        headers.put("DEVICE", Gc.DEVICETYPE);
                        headers.put("DEVICEID",Gc.id(getApplicationContext()));
                        headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                        headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                        headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                        headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                        return headers;
                    }
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    @Override
                    public byte[] getBody() {
                        try {
                            return param == null ? null : param.toString().getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                            return null;
                        }
                    }
                }

                        ;
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjReq);
            }

        }
    }

    private void deleteSlots() throws JSONException {
        ArrayList<TimeTableSlot> deleteSlots = adapter.getDeleteSlotsId();

        JSONArray slotArray = new JSONArray();
        String sectionId = classListData.get(sp_class_list.getSelectedItemPosition()).SectionId;
        for (TimeTableSlot slotObjDelete : deleteSlots
                ) {
            progressbar.show();
            JSONObject deleteJsonObj = new JSONObject();
            deleteJsonObj.put("Date", slotObjDelete.Date);
            deleteJsonObj.put("SectionID", sectionId);
            deleteJsonObj.put("SlotID", slotObjDelete.SlotID);
            slotArray.put(deleteJsonObj);
        }

        if (slotArray.length() > 0) {
            final JSONObject param_main = new JSONObject();

            param_main.put("filter", slotArray);

            Log.e("deleteSlot", param_main.toString());

            String deleteStaffUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Config.applicationVersionNameValue
                    + "TimeTableDayDateWise.php?databaseName="
                    + dbName
                    + "&data="
                    + param_main;

            Log.d("deleteSlotUrl", deleteStaffUrl);
            StringRequest request = new StringRequest(Request.Method.DELETE,
                    deleteStaffUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);

                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optInt("code") == 200 && !isFinishing()) {
                                    Toast.makeText(DayWiseActivity.this, getString(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !(DayWiseActivity.this).isFinishing()) {
                                        Config.responseVolleyHandlerAlert(DayWiseActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                else if (jsonObject.optString("code").equalsIgnoreCase("401"))
                                    Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            try {
//                                getSlotList();
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("deleteResultSlot", volleyError.toString());
                    progressbar.cancel();

                    Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<>();

                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(DayWiseActivity.this).addToRequestQueue(request);

        }
    }

    private void postHoliday(boolean replaceAll) throws JSONException {
         JSONObject param1 = new JSONObject();

        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {

            String sectionId = classListData.get(sp_class_list.getSelectedItemPosition()).SectionId;
            param1.put("SectionID", sectionId);
            param1.put("FromDate", btn_from_date.getText().toString());
            param1.put("ToDate", btn_to_date.getText().toString());
            param1.put("Comment", et_comments.getText().toString().trim());
            param1.put("CreatedBy", Gc.getSharedPreference(Gc.USERID, this));
            param1.put("Holiday", 1);
            param1.put("Day", sp_day_name.getSelectedItem());
            param1.put("ReplaceAll", 1);
            progressbar.show();

            final JSONArray param = new JSONArray();
            param.put(param1);

            String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Gc.ERPAPIVERSION
                    + "timeTable/TimeTableDayWise"
                    ;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(),
                    jsonObject -> {
                        DbHandler.longInfo(jsonObject.toString());

                        if (!isFinishing())
                            progressbar.cancel();

                        String code = jsonObject.optString("code");

                        switch (code) {
                            case Gc.APIRESPONSE200:

                                Config.responseSnackBarHandler(getString(R.string.save_successfully),
                                        findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                                break;

                            case Gc.APIRESPONSE401:

                                HashMap<String, String> setDefaults1 = new HashMap<>();
                                setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults1, DayWiseActivity.this);

                                Intent intent1 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);

                                finish();

                                break;

                            case Gc.APIRESPONSE500:

                                HashMap<String, String> setDefaults = new HashMap<>();
                                setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                                Gc.setSharedPreference(setDefaults, DayWiseActivity.this);

                                Intent intent2 = new Intent(DayWiseActivity.this, AdminNavigationDrawerNew.class);
                                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent2);

                                finish();

                                break;

                            default:
                                Config.responseSnackBarHandler(jsonObject.optString("message"),
                                        findViewById(R.id.rl_timetable_mapping_activity),R.color.fragment_first_blue);
                        }

//                        if (jsonObject.optString("code").equalsIgnoreCase("200")) {
//                            Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar, getResources().getColor(android.R.color.holo_green_dark));
//
//
//                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                ||
//                                jsonObject.optString("code").equalsIgnoreCase("511")) {
//                            if (!logoutAlert & !isFinishing()) {
//                                Config.responseVolleyHandlerAlert(DayWiseActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                logoutAlert = true;
//                            }
//                        } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                            Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//                        else
//                            Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar);


                    }, volleyError -> {
                        progressbar.cancel();
                        Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
                    }
            )
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("APPKEY",Gc.APPKEY);
                    headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                    headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                    headers.put("Content-Type", Gc.CONTENT_TYPE);
                    headers.put("DEVICE", Gc.DEVICETYPE);
                    headers.put("DEVICEID",Gc.id(getApplicationContext()));
                    headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                    headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                    headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                    headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                    return headers;
                }
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() {
                    try {
                        return param == null ? null : param.toString().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                        return null;
                    }
                }
            }

                    ;
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjReq);


        }
    }

    void getSubjectForClass(){
        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {
            mDb.schoolSubjectsClassModel()
                    .getSubjectsForSection(classListData.get(sp_class_list.getSelectedItemPosition()).SectionId)
                    .observe(this, schoolSubjectsEntities -> {
                        subjectListData.clear();
                        subjectListData.addAll(schoolSubjectsEntities);
                        adapter.updateSubjectList(subjectListData);

                        mDb.schoolSubjectsClassModel()
                                .getSubjectsForSection(classListData.get(sp_class_list.getSelectedItemPosition()).SectionId)
                                .removeObservers(DayWiseActivity.this);

                    });
        } else showSnackClassList();

    }

//    public void getSubjectClassWise() throws JSONException {
//        if (classListData.size() >= sp_class_list.getSelectedItemPosition() && sp_class_list.getSelectedItemPosition() != -1) {
//
//            progressbar.show();
//
//            subjectListData.clear();
//
//            JSONObject param = new JSONObject();
//            param.put("SectionID", classListData.get(sp_class_list.getSelectedItemPosition()).SectionId);
//
//            JSONObject jsonFilter = new JSONObject();
//            jsonFilter.put("filter", param);
//            String SubjectGetUrl = sp.getString("HostName", "Not Found")
//                    + sp.getString(Config.applicationVersionName, "Not Found")
//                    + "SubjectApi.php?databaseName=" +
//                    dbName +
//                    "&action=join&session=" + sp.getString(Config.SESSION, "")
//                    + "&data=" + jsonFilter;
//            ;
//            Log.d("SubjectGetUrl", SubjectGetUrl);
//            StringRequest request = new StringRequest(Request.Method.GET,
//
//                    SubjectGetUrl
//                    ,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String s) {
//                            DbHandler.longInfo(s);
//                            Log.e("getSubjectData", s);
//                            progressbar.cancel();
//                            try {
//                                JSONObject jsonObject = new JSONObject(s);
//                                if (jsonObject.optInt("code") == 201) {
//                                    Subject obj = gson.fromJson(s, new TypeToken<Subject>() {
//                                    }.getType());
//                                    subjectListData = obj.getResult();
//
//                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                        ||
//                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
//                                    if (!logoutAlert & !isFinishing()) {
//                                        Config.responseVolleyHandlerAlert(DayWiseActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                        logoutAlert = true;
//                                    }
//                                } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            adapter.updateSubjectList(subjectListData);
//
//                        }
//
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError volleyError) {
//                    Log.e("getSubjectData", volleyError.toString());
//                    progressbar.cancel();
//                    subjectListData.clear();
//                    adapter.updateSubjectList(subjectListData);
//                    Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//
//                    Map<String, String> header = new LinkedHashMap<String, String>();
//                    header.put("Content-Type", "application/x-www-form-urlencoded");
//                    return super.getHeaders();
//                }
//
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new LinkedHashMap<String, String>();
//                    return params;
//                }
//            };
//
//            request.setRetryPolicy(new DefaultRetryPolicy(0,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//        } else showSnackClassList();
//    }

    public void showSnackClassList() {

        Snackbar snackbarObj = Snackbar.make(
                snackbar,
                getString(R.string.classes_not_available),
                Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                v -> {
                    if (progressbar != null && progressbar.isShowing()) progressbar.dismiss();
                    if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION, false)) {
                        Toast.makeText(DayWiseActivity.this, "Permission not available : CREATE CLASS SECTION !", Toast.LENGTH_SHORT).show();

                    } else {
                        startActivity(new Intent(DayWiseActivity.this, ClassSectionActivity.class));
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.nothing);
                    }
                });
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();

    }

//    public void getStaffDataFromServer() {
//
//        progressbar.show();
//
//        String getStaffUrl = sp.getString("HostName", "Not Found")
//                + sp.getString(Config.applicationVersionName, "Not Found")
//                + "StaffApi.php?databaseName=" +
//                dbName + "&action=join";
//
//        Log.d("getStaffData", getStaffUrl);
//        StringRequest request = new StringRequest(Request.Method.GET,
//                getStaffUrl
//                ,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String s) {
//                        DbHandler.longInfo(s);
//                        Log.e("getStaffData", s);
//                        progressbar.cancel();
//                        try {
//                            JSONObject jsonObject = new JSONObject(s);
//                            if (jsonObject.optInt("code") == 200) {
//                                StaffDetail obj = gson.fromJson(s, new TypeToken<StaffDetail>() {
//                                }.getType());
//                                staffListData = obj.getResult();
//
//                                    /* set Staff List*/
//                                adapter.updateStaffList(staffListData);
//
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
//                                    ||
//                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
//                                if (!logoutAlert & !(DayWiseActivity.this).isFinishing()) {
//                                    Config.responseVolleyHandlerAlert(DayWiseActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
//                                    logoutAlert = true;
//                                }
//                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
//                            else if (jsonObject.optString("code").equalsIgnoreCase("401"))
//                                Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e("getStaffData", volleyError.toString());
//                progressbar.cancel();
//
//                Config.responseVolleyErrorHandler(DayWiseActivity.this, volleyError, snackbar);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> header = new LinkedHashMap<String, String>();
//                header.put("Content-Type", "application/x-www-form-urlencoded");
//                return super.getHeaders();
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new LinkedHashMap<String, String>();
//                return params;
//            }
//        };
//
//        request.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
//        //}
//    }

    private void setUpRecycler() {
        rv_timetable_mapping_list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new DayWiseAdapter(this, slotList, colorIs, staffListData, subjectListData);
        rv_timetable_mapping_list.setAdapter(adapter);

    }

    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fbtn_add_new.startAnimation(fab_close);
            fab_delete.startAnimation(fab_close);
            fbtn_add_new.setClickable(false);
            fab_delete.setClickable(false);
            isFabOpen = false;
            Log.d("Ritu", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fbtn_add_new.startAnimation(fab_open);

            fab_delete.startAnimation(fab_open);
            fbtn_add_new.setClickable(true);

            fab_delete.setClickable(true);
            isFabOpen = true;
            Log.d("Ritu", "open");

        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fbtn_create_time_slot:

                animateFAB();
                break;
            case R.id.fbtn_create_time_slot_add_new:
                fab.performClick();

                if (slotList.size()!=0){
                    TimeTableSlot obb = slotList.get(slotList.size()-1);
                    try {
                        cal.setTime(time.parse(obb.SlotEndTime));
                    } catch (ParseException e) {
                        cal.setTimeInMillis(cal.getTimeInMillis());
                        e.printStackTrace();
                    }
                }else {try {
                    cal.setTime(time.parse(btn_from_date.getText().toString()));
                } catch (ParseException e) {
                    cal.setTimeInMillis(cal.getTimeInMillis());
                    e.printStackTrace();
                }}

                String startTime = time.format(cal.getTime());
                cal.add(Calendar.HOUR_OF_DAY,1);


                TimeTableSlot slot = new TimeTableSlot();
                slot.SlotID = "";
                slot.SlotName = getString(R.string.lecture);
                slot.SlotStartTime = startTime;
                slot.SlotEndTime = time.format(cal.getTime());/* Slot End time */

                slotList.add(slot);
                adapter.updateList(slotList);
//                if (sp.getBoolean("DemoDateWiseActivitySlotDetail", true)) {
//                    ll_click_to_view_slot_detail_demo1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.vibrate_shake));
//                    ll_click_to_view_slot_detail_demo.setVisibility(View.VISIBLE);
//                    ll_click_to_view_slot_detail_demo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            ll_click_to_view_slot_detail_demo1.clearAnimation();
//                            ll_click_to_view_slot_detail_demo.setVisibility(View.INVISIBLE);
//                            sp.edit().putBoolean("DemoDateWiseActivitySlotDetail", false).commit();
//                        }
//                    });
//                } else ll_click_to_view_slot_detail_demo.setVisibility(View.INVISIBLE);

                break;
            case R.id.fbtn_create_time_slot_delete:
                fab.performClick();
                adapter.deleteSlot(true);

                break;

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

}
