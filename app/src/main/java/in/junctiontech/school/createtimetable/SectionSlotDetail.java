package in.junctiontech.school.createtimetable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 12-07-2017.
 */

public class SectionSlotDetail implements Serializable  {

    ArrayList<SlotsDetails> Slots;
    ArrayList<SectionSlotDetail> result;
    String ClassName, SectionName, SectionID, Day, SectionStartTime, SectionEndTime;
    int Holiday;


    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public ArrayList<SectionSlotDetail> getResult() {
        return result;
    }

    public void setResult(ArrayList<SectionSlotDetail> result) {
        this.result = result;
    }

    public ArrayList<SlotsDetails> getSlots() {
        return Slots;
    }

    public void setSlots(ArrayList<SlotsDetails> slots) {
        Slots = slots;
    }

    public String getSectionID() {
        return SectionID;
    }

    public void setSectionID(String sectionID) {
        SectionID = sectionID;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getSectionStartTime() {
        return SectionStartTime;
    }

    public void setSectionStartTime(String sectionStartTime) {
        SectionStartTime = sectionStartTime;
    }

    public String getSectionEndTime() {
        return SectionEndTime;
    }

    public void setSectionEndTime(String sectionEndTime) {
        SectionEndTime = sectionEndTime;
    }

    public int getHoliday() {
        return Holiday;
    }

    public void setHoliday(int holiday) {
        Holiday = holiday;
    }
}
