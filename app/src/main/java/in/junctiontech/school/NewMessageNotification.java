package in.junctiontech.school;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.exam.StudentExamResult;
import in.junctiontech.school.notificationbar.NotificationBarActivity;
import in.junctiontech.school.schoolnew.FirstScreenActivity;
import in.junctiontech.school.schoolnew.SchoolLibrary.Library;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.assignments.Assignments;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.messenger.ConversationsActivity;
import in.junctiontech.school.schoolnew.schoolcalender.SchoolCalenderActivity;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT;

/**
 * Helper class for showing and canceling new message
 * notifications.
 * <p>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class NewMessageNotification {
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "NewMessage";

    /**
     * Shows the notification, or updates a previously shown notification of
     * this type, with the given parameters.
     * <p>
     * TODO: Customize this method's arguments to present relevant content in
     * the notification.
     * <p>
     * TODO: Customize the contents of this method to tweak the behavior and
     * presentation of new message notifications. Make
     * sure to follow the
     * <a href="https://developer.android.com/design/patterns/notifications.html">
     * Notification design guidelines</a> when doing so.
     *
     * @see #cancel(Context)
     */
    public static void notify(final Context context,
                              final String message, final String msgTitle, final String  ticker,final int number) {
        final Resources res = context.getResources();

        // This image is used as the notification's large icon (thumbnail).
        // TODO: Remove this if your notification has no relevant thumbnail.
        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.ic_junction);


        //final String ticker = message;
        final String title = res.getString(
                R.string.new_message_notification_title_template, msgTitle);
        final String text = message;


        Intent myNotificationIntent = new Intent(context, ConversationsActivity.class);
        myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(FirstScreenActivity.class);
        taskStackBuilder.addNextIntent(myNotificationIntent);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        SharedPreferences  sharedPreferences = Prefs.with(context).getSharedPreferences();


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context.getApplicationContext(),"channel_01")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setChannelId("channel_01")
                    .setContentTitle(title)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            ;


            NotificationChannel channel = new NotificationChannel("channel_01", "Chat Message", NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Chatting");
            notificationManager.createNotificationChannel(channel);

            mBuilder.setChannelId("channel_01");

            notificationManager.notify(111, mBuilder.build());

        }else {


            final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)

                    // Set appropriate defaults for the notification light, sound,
                    // and vibration.
                    .setDefaults(sharedPreferences.getBoolean(Config.CHAT_NOTIFICATION_SOUND, true) ? Notification.DEFAULT_SOUND : Notification.DEFAULT_VIBRATE)
                    // .setDefaults(Notification.DEFAULT_ALL)

                    // Set required fields, including the small icon, the
                    // notification title, and text.
                    .setSmallIcon(android.R.drawable.stat_notify_chat)
                    .setContentTitle(title)
                    .setContentText(message)

                    // All fields below this line are optional.

                    // Use a default priority (recognized on devices running Android
                    // 4.1 or later)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                    // Provide a large icon, shown with the notification in the
                    // notification drawer on devices running Android 3.0 or later.
                    .setLargeIcon(picture)

                    // Set ticker text (preview) information for this notification.
                    .setTicker(title)

                    // Show a number. This is useful when stacking notifications of
                    // a single type.
                    .setNumber(number)

                    // If this notification relates to a past or upcoming event, you
                    // should set the relevant time information using the setWhen
                    // method below. If this call is omitted, the notification's
                    // timestamp will by set to the time at which it was shown.
                    // TODO: Call setWhen if this notification relates to a past or
                    // upcoming event. The sole argument to this method should be
                    // the notification timestamp in milliseconds.
                    //.setWhen(...)

                    // Set the pending intent to be initiated when the user touches
                    // the notification.
                    .setContentIntent(
                            PendingIntent.getActivity(
                                    context,
                                    0,
                                    new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.zeroerp.com")),
                                    PendingIntent.FLAG_UPDATE_CURRENT))

                    // Show expanded text content on devices running Android 4.1 or
                    // later.

                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(text)
                            .setBigContentTitle(title)
                            .setSummaryText(ticker))

                    // Example additional actions for this notification. These will
                    // only show on devices running Android 4.1 or later, so you
                    // should ensure that the activity in this notification's
                    // content intent provides access to the same actions in
                    // another way.
                    .addAction(
                            R.drawable.ic_action_stat_share,
                            res.getString(R.string.action_share),
                            PendingIntent.getActivity(
                                    context,
                                    0,
                                    Intent.createChooser(new Intent(Intent.ACTION_SEND)
                                            .setType("text/plain")
                                            .putExtra(Intent.EXTRA_TEXT, msgTitle + " : " + message), "MySchool Message"),
                                    PendingIntent.FLAG_UPDATE_CURRENT))
                    .addAction(
                            R.drawable.ic_action_stat_reply,
                            res.getString(R.string.action_reply), pendingIntent
                    )

                    // Automatically dismiss the notification when it is touched.
                    .setAutoCancel(true);
            builder.setContentIntent(pendingIntent);
            notify(context, builder.build());
        }
    }

    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);

            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
    }

    /**
     * Cancels any notifications of this type previously shown using
     * {@link #notify(Context, String,String,String, int)}.
     */
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(NOTIFICATION_SERVICE);

            nm.cancel(NOTIFICATION_TAG.hashCode());

    }

    public static void notify(Context context, String message   ,
                              String requestType) {
        String reminderTitle = "";
        if (requestType.equalsIgnoreCase("Reminder")){
            try {
                JSONObject data = new JSONObject(message);
                message = data.optString("message");
                reminderTitle = data.optString("title");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final Resources res = context.getResources();
        SharedPreferences  sharedPreferences = Prefs.with(context).getSharedPreferences();

        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.ic_junction);
        final String text = message;

        Intent myNotificationIntent = new Intent();
        String noteSummery=context.getString(R.string.zeroerp_school);
        String noteTitle=context.getString(R.string.zeroerp_school);
        int shortIcon=R.drawable.ic_attendance;
        switch (requestType){
            case "AttendanceData":
                myNotificationIntent = new Intent(context, ConversationsActivity.class);
//                noteSummery=context.getString(R.string.absent_status);
                noteTitle=context.getString(R.string.attendance);
                shortIcon=R.drawable.ic_attendance;
                break;
            case "ResultData":
                myNotificationIntent = new Intent(context, StudentExamResult.class);
                noteSummery=context.getString(R.string.exam_result_uploaded);
                noteTitle=context.getString(R.string.exam_results);
                shortIcon=R.drawable.ic_exam_result;
                break;
            case "issuedBookData":
                myNotificationIntent = new Intent(context, Library.class);
                noteSummery=context.getString(R.string.book_issued);
                noteTitle=context.getString(R.string.library);
                shortIcon=R.drawable.library;
                break;
            case "returnedBookData":
                myNotificationIntent = new Intent(context, Library.class);
                noteSummery=context.getString(R.string.book_returned);
                noteTitle=context.getString(R.string.library);
                shortIcon=R.drawable.library;
                break;

            case "NoticeBoard":
                myNotificationIntent = new Intent(context, NotificationBarActivity.class);
                noteSummery=message;
                noteTitle=context.getString(R.string.noticeboard);
                shortIcon=R.drawable.ic_notification;
                break;

            case "eventCalendar":
                myNotificationIntent = new Intent(context, SchoolCalenderActivity.class);
                noteSummery=message;
                noteTitle=context.getString(R.string.event_calendar);
                shortIcon=R.drawable.ic_notification;
                break;

            case "sendMessage":
                myNotificationIntent = new Intent(context, ConversationsActivity.class);
                noteSummery=message;
                noteTitle=context.getString(R.string.message);
                shortIcon=R.drawable.ic_notification;
                break;
            case "NewHomework":
                myNotificationIntent = new Intent(context, AdminNavigationDrawerNew.class)
                        .setFlags(FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                noteSummery=context.getString(R.string.new_assignment);
                noteTitle=context.getString(R.string.assignment);
                shortIcon=R.drawable.ic_homework;
                break;
            case "HomeworkEvaluationData":
                myNotificationIntent = new Intent(context, Assignments.class);
                noteSummery=context.getString(R.string.assignment_evaluation);
                noteTitle=context.getString(R.string.assignment_evaluation);
                shortIcon=R.drawable.ic_homework;
                break;

            case "FeeReminder":
                myNotificationIntent = new Intent(context, AdminNavigationDrawerNew.class);
                noteSummery=context.getString(R.string.due_fee);
                noteTitle=context.getString(R.string.fee_reminder);
                shortIcon=R.drawable.ic_coins;
                break;


            case "Reminder":
                myNotificationIntent = new Intent(context, AdminNavigationDrawerNew.class);
                noteSummery=message;
                noteTitle=reminderTitle;
                shortIcon=R.drawable.ic_notification;
                break;

            case "SessionExpireReminder":
                myNotificationIntent = new Intent(context, AdminNavigationDrawerNew.class);
                noteSummery=context.getString(R.string.session_expire);
                noteTitle=context.getString(R.string.session_expire);
                shortIcon=android.R.drawable.ic_dialog_alert;
                break;
        }


        myNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(FirstScreenActivity.class);
        taskStackBuilder.addNextIntent(myNotificationIntent);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "Important")

                // Set appropriate defaults for the notification light, sound,
                // and vibration.
                .setDefaults(sharedPreferences.getBoolean(Config.NOTIFICATION_SOUND,true)?Notification.DEFAULT_SOUND:Notification.DEFAULT_VIBRATE)

                // Set required fields, including the small icon, the
                // notification title, and text.
                .setSmallIcon(shortIcon)
                .setContentTitle(noteTitle +" : " + Gc.getSharedPreference(Gc.SCHOOLNAME, context))
                .setContentText(noteSummery)

                // All fields below this line are optional.

                // Use a default priority (recognized on devices running Android
                // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setLargeIcon(picture)
                .setTicker(noteTitle)
                .setNumber(0)
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.zeroerp.com")),
                                PendingIntent.FLAG_UPDATE_CURRENT))


                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text)
                        .setBigContentTitle(Gc.getSharedPreference(Gc.SCHOOLNAME, context)+" : "+noteTitle)
                        .setSummaryText(noteSummery))



                // Automatically dismiss the notification when it is touched.
                .setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        notify(context, builder.build());
    }
}
