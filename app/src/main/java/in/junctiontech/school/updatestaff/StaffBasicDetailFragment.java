package in.junctiontech.school.updatestaff;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.models.StaffDetail;
import in.junctiontech.school.models.country.CountryCode;

/**
 * Created by Administrator on 11/5/2016.
 */

public class StaffBasicDetailFragment extends
        Fragment {
    private AppCompatSpinner spinner_update_teacher_staff_position;
    private ArrayAdapter<String> positionAdapter;
    private ArrayList<MasterEntry> masterEntryPosList;

    private EditText et_update_teacher_staff_name, et_update_teacher_staff_email, et_update_teacher_staff_mobile,
            et_update_teacher_present_address, et_update_teacher_permanent_address,
            et_update_teacher_staff_mother_name, et_update_teacher_staff_father_name;
    private Button btn_update_teacher_dob, btn_update_teacher;
    private StaffDetail staffObj;
    private int staffPos = 0;
    private ArrayList<String> staffPosition = new ArrayList<>();
    private TextView tv_update_teacher_date_of_joining;
    private String mandatory_fields = "";
    private AutoCompleteTextView et_update_teacher_country_code;
    private ArrayList<String> countryCodeList = new ArrayList<>();
    private int appColor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_teacher_basic_detail, container, false);
        tv_update_teacher_date_of_joining = (TextView) convertView.findViewById(R.id.tv_update_teacher_date_of_joining);

        ((TextView) convertView.findViewById(R.id.tv_spinner_update_teacher_staff_position)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_staff_name)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_staff_father_name)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_staff_mother_name)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_staff_email)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_staff_mobile)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_present_address)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_update_teacher_permanent_address)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.tv_btn_update_teacher_dob)).setTextColor(appColor);

        spinner_update_teacher_staff_position = (AppCompatSpinner) convertView.findViewById(R.id.spinner_update_teacher_staff_position);
        et_update_teacher_staff_name = (EditText) convertView.findViewById(R.id.et_update_teacher_staff_name);
        et_update_teacher_staff_father_name = (EditText) convertView.findViewById(R.id.et_update_teacher_staff_father_name);
        et_update_teacher_staff_mother_name = (EditText) convertView.findViewById(R.id.et_update_teacher_staff_mother_name);
        et_update_teacher_staff_email = (EditText) convertView.findViewById(R.id.et_update_teacher_staff_email);

        et_update_teacher_country_code = (AutoCompleteTextView) convertView.findViewById(R.id.et_update_teacher_country_code);
        et_update_teacher_staff_mobile = (EditText) convertView.findViewById(R.id.et_update_teacher_staff_mobile);
        et_update_teacher_present_address = (EditText) convertView.findViewById(R.id.et_update_teacher_present_address);
        et_update_teacher_permanent_address = (EditText) convertView.findViewById(R.id.et_update_teacher_permanent_address);
        btn_update_teacher_dob = (Button) convertView.findViewById(R.id.btn_update_teacher_dob);
        btn_update_teacher= (Button) convertView.findViewById(R.id.btn_update_teacher);
        btn_update_teacher.setBackgroundColor(appColor);
        btn_update_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_update_teacher_staff_name.getText().toString().trim().equalsIgnoreCase("") ||
                        (  et_update_teacher_country_code.getText().toString().trim() +
                                et_update_teacher_staff_mobile.getText().toString().trim()).length() <1) {

                    if (et_update_teacher_staff_name.getText().toString().trim().equalsIgnoreCase(""))
                        mandatory_fields = mandatory_fields + " " + getString(R.string.name) + " , ";

                    if ((  et_update_teacher_country_code.getText().toString().trim() +
                            et_update_teacher_staff_mobile.getText().toString().trim()).length() < 1)
                        mandatory_fields = mandatory_fields + " " + getString(R.string.mobile_number) + " , ";

                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext(), AlertDialog.THEME_HOLO_LIGHT);
                    alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {


                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mandatory_fields = "";
                        }
                    });
                    alert.setCancelable(false);

                    alert.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + " : " +
                            mandatory_fields);
                    alert.show();

                } else
                    try {
                        ((UpdateStaffActivity) getActivity()).updateTeacherData(staffObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
            }
        });

        btn_update_teacher_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenersession_date_of_of_birth, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        et_update_teacher_staff_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffName(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_staff_father_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffFName(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_staff_mother_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffMName(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_staff_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffEmail(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_country_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffMobile(s.toString().trim() + "-" +
                        et_update_teacher_staff_mobile.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_staff_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffMobile(et_update_teacher_country_code.getText().toString().trim() + "-" +
                        s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_present_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffPresentAddress(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_update_teacher_permanent_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                staffObj.setStaffPermanentAddress(s.toString().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        spinner_update_teacher_staff_position.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                staffObj.setStaffPosition(masterEntryPosList.get(position).getMasterEntryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (staffObj.getStaffDOB() != null && !staffObj.getStaffDOB().equalsIgnoreCase("") )
            ((UpdateStaffActivity) getActivity()).convertStringToDateDOB("dob");

        ((UpdateStaffActivity) getActivity()).convertStringToDateDOB("doj");

        return convertView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        staffObj = ((UpdateStaffActivity) getActivity()).getStaffData();
        masterEntryPosList = ((UpdateStaffActivity) getActivity()).getMasterEntryPosition();
        appColor = ((UpdateStaffActivity) getActivity()).getAppColor();
//        if (masterEntryPosList.size() != 0) {
//            staffPosition.add(masterEntryPosList.get(0).getMasterEntryValue());
//            staffPosition.add(masterEntryPosList.get(1).getMasterEntryValue());
//            if (staffObj.getStaffPosition().equalsIgnoreCase(masterEntryPosList.get(0).getMasterEntryId()))
//                staffPos = 0;
//            if (staffObj.getStaffPosition().equalsIgnoreCase(masterEntryPosList.get(1).getMasterEntryId()))
//                staffPos = 1;
//        }

        for (int m = 0; m < masterEntryPosList.size(); m++) {
            staffPosition.add(masterEntryPosList.get(m).getMasterEntryValue());
            if (staffObj.getStaffPosition().equalsIgnoreCase(masterEntryPosList.get(m).getMasterEntryId()))
                staffPos = m;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        positionAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, staffPosition);
        positionAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_update_teacher_staff_position.setAdapter(positionAdapter);
        spinner_update_teacher_staff_position.setSelection(staffPos);

        if (staffObj.getStaffName() != null)
            et_update_teacher_staff_name.setText(staffObj.getStaffName().replaceAll("_", " "));
        if (staffObj.getStaffFName() != null)
            et_update_teacher_staff_father_name.setText(staffObj.getStaffFName().replaceAll("_", " "));
        if (staffObj.getStaffMName() != null)
            et_update_teacher_staff_mother_name.setText(staffObj.getStaffMName().replaceAll("_", " "));
        if (staffObj.getStaffEmail() != null)
            et_update_teacher_staff_email.setText(staffObj.getStaffEmail().replaceAll("_", " "));
        if (staffObj.getStaffMobile() != null) {
            if (staffObj.getStaffMobile().contains("-")) {
                String[] arr = staffObj.getStaffMobile().split("-");
                et_update_teacher_country_code.setText(arr.length >= 1 ? (arr[0].contains("+") ? arr[0].replace("+", "") : "" + arr[0]) : "");
                et_update_teacher_staff_mobile.setText(arr.length >= 2 ? arr[1] : "");
            } else
                et_update_teacher_staff_mobile.setText(staffObj.getStaffMobile().replaceAll("_", " "));

        }
        if (staffObj.getStaffPresentAddress() != null)
            et_update_teacher_present_address.setText(staffObj.getStaffPresentAddress().replaceAll("_", " "));
        if (staffObj.getStaffPermanentAddress() != null)
            et_update_teacher_permanent_address.setText(staffObj.getStaffPermanentAddress().replaceAll("_", " "));


        String countryCodeString = getActivity().getSharedPreferences(Config.COUNTRY_CODE, Context.MODE_PRIVATE).getString(Config.COUNTRY_CODE, "");
        if (!countryCodeString.equalsIgnoreCase("")) {
            CountryCode obj = (new Gson()).fromJson(countryCodeString, new TypeToken<CountryCode>() {
            }.getType());
            ArrayList<CountryCode> countryCodeObjList = obj.getResult();

            for (CountryCode countryObj : countryCodeObjList)
                countryCodeList.add("+" + countryObj.getCode());
            ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, countryCodeList);
            //adapterSpinner.set
           adapterCountry.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            et_update_teacher_country_code.setAdapter(adapterCountry);

        } else Config.getCountryCode(getActivity());
//        et_student_data_student_name.setText(studentObj.getStudentName().replaceAll("_"," "));
//        ((UpdateStudentActivity) getActivity()).convertStringToDate("R", studentObj.getDOR());
//        if (studentObj.getDOB() != null)
//            ((UpdateStudentActivity) getActivity()).convertStringToDate("S", studentObj.getDOB());
//        /* class spinner */
//        int studentClassPos = 0;
//        for (int cls = 0; cls < classListData.size(); cls++) {
//            CreateClass obj = classListData.get(cls);
//            ArrayList<CreateClass> secList = obj.getSectionList();
//            for (int sec = 0; sec < secList.size(); sec++) {
//                sectionIdList.add(secList.get(sec).getSectionId());
//                sectionNameList.add(obj.getClassName() + " " +
//                        secList.get(sec).getSectionName());
//                if (studentObj.getSectionId().equalsIgnoreCase(secList.get(sec).getSectionId()))
//                    studentClassPos = sectionIdList.size() - 1;
//            }
//        }
//
//        classAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1, sectionNameList);
//        spinner_student_data_class.setAdapter(classAdapter);
//        spinner_student_data_class.setSelection(studentClassPos);
//
//      /* gender type spinner */
//        int studentGenderPos = 0;
//        for (int i = 0; i < genderTypeListData.size(); i++) {
//            genderList.add(genderTypeListData.get(i).getMasterEntryValue());
//            if (studentObj.getGender().equalsIgnoreCase(genderTypeListData.get(i).getMasterEntryId()))
//                studentGenderPos = i;
//        }
//
//        genderTypeAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1, genderList);
//        spinner_student_data_gender.setAdapter(genderTypeAdapter);
//        spinner_student_data_gender.setSelection(studentGenderPos);
//
//        /* caste type spinner */
//        int studentCastePos = 0;
//        for (int i = 0; i < casteListData.size(); i++) {
//            casteList.add(casteListData.get(i).getMasterEntryValue());
//            if (studentObj.getCaste() != null)
//                if (studentObj.getCaste().equalsIgnoreCase(casteListData.get(i).getMasterEntryId()))
//                    studentCastePos = i;
//        }
//
//        casteTypeAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1, casteList);
//        spinner_student_data_caste.setAdapter(casteTypeAdapter);
//        spinner_student_data_caste.setSelection(studentCastePos);
//
//        /* category type spinner */
//        int studentCategoryPos = 0;
//        for (int i = 0; i < categoryListData.size(); i++) {
//            categoryList.add(categoryListData.get(i).getMasterEntryValue());
//            if (studentObj.getCategory() != null)
//                if (studentObj.getCategory().equalsIgnoreCase(categoryListData.get(i).getMasterEntryId()))
//                    studentCategoryPos = i;
//        }
//
//        categoryTypeAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1, categoryList);
//        spinner_student_data_category.setAdapter(categoryTypeAdapter);
//        spinner_student_data_category.setSelection(studentCategoryPos);
//
//         /* blood group type spinner */
//        int studentBloodGroupPos = 0;
//        for (int i = 0; i < bloodGroupListData.size(); i++) {
//            bloodGroupList.add(bloodGroupListData.get(i).getMasterEntryValue());
//            if (studentObj.getBloodGroup() != null)
//                if (studentObj.getBloodGroup().equalsIgnoreCase(bloodGroupListData.get(i).getMasterEntryId()))
//                    studentBloodGroupPos = i;
//        }
//
//        bloodGroupTypeAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1, bloodGroupList);
//        spinner_student_data_blood_group.setAdapter(bloodGroupTypeAdapter);
//        spinner_student_data_blood_group.setSelection(studentBloodGroupPos);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public DatePickerDialog.OnDateSetListener myDateListenersession_date_of_of_birth = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_update_teacher_dob.setText(year + "-" + new_month + "-" + new_day);
            ((UpdateStaffActivity) getActivity()).convertDateOfBirthToString(year + "-" + new_month + "-" + new_day);
        }
    };


    public void setDateOfBirth(String dateOfBirth, String keyString) {
        if (keyString.equalsIgnoreCase("dob"))
            btn_update_teacher_dob.setText(dateOfBirth);
        else if (keyString.equalsIgnoreCase("doj"))
            tv_update_teacher_date_of_joining.setText(getString(R.string.date_of_joining) + " : " +
                    dateOfBirth);
    }


}
