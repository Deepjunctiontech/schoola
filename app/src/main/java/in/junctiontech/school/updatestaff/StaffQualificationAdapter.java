package in.junctiontech.school.updatestaff;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.Qualification;

/**
 * Created by JAYDEVI BHADE on 11/5/2016.
 */

public class StaffQualificationAdapter extends RecyclerView.Adapter<StaffQualificationAdapter.MyViewHolder> {
    private ArrayList<Qualification> qualificationList;
    private Context context;
    private int appColor;

    public StaffQualificationAdapter(Context context, ArrayList<Qualification> qualificationList, int appColor) {
        this.qualificationList = qualificationList;
        this.context = context;
        this.appColor = appColor;

    }

    @Override
    public StaffQualificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_qualification, parent, false);

        return new StaffQualificationAdapter.MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final StaffQualificationAdapter.MyViewHolder holder, int position) {
        final Qualification qualificationObj = qualificationList.get(position);
        holder.tv_item_qualification_subtext.setText(
                context.getString(R.string.board_university) + " : " +
                        qualificationObj.getBoardUniversity() + "\n"
                        + context.getString(R.string.passing_year) + " : " +
                        qualificationObj.getYear() + "\n" +
                        context.getString(R.string.marks) + " : " + qualificationObj.getMarks());
        holder.tv_item_qualification_degree_name.setText(qualificationObj.getDegreeName());
        holder.tv_item_qualification_degree_name.setVisibility(View.VISIBLE);

        holder.tv_item_qualification_subtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UpdateStaffActivity) context).updateQualification(
                        qualificationObj, qualificationList.indexOf(qualificationObj));

            }


        });
     //animate(holder);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return qualificationList.size();
    }

    public void updateList(ArrayList<Qualification> qualificationList) {
        this.qualificationList = qualificationList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_qualification_degree_name, tv_item_qualification_subtext;
        Button btn_item_qualification_delete;


        public MyViewHolder(View view) {
            super(view);
            (view.findViewById(R.id.ll_item_view_full_qualification_detail)).setVisibility(View.GONE);

            tv_item_qualification_degree_name = (TextView) view.findViewById(R.id.tv_item_qualification_degree_name);
            tv_item_qualification_degree_name.setTextColor(appColor);
            tv_item_qualification_subtext = (TextView) view.findViewById(R.id.tv_item_qualification_subtext);
            tv_item_qualification_subtext.setVisibility(View.VISIBLE);
            btn_item_qualification_delete = (Button) view.findViewById(R.id.btn_item_qualification_delete);
            btn_item_qualification_delete.setVisibility(View.VISIBLE);

            btn_item_qualification_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((UpdateStaffActivity)context).deleteQualification(
                            qualificationList.get(getLayoutPosition()),getLayoutPosition());
                }
            });
            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }
}
