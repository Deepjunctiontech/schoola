package in.junctiontech.school.updatestaff;

import android.content.res.ColorStateList;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.Qualification;

/**
 * Created by JAYDEVI BHADE on 11/5/2016.
 */

public class StaffQualificationFragment extends
        Fragment {


    private RecyclerView recycler_view_qualification_list;
    private ArrayList<Qualification> qualificationListData;
    private StaffQualificationAdapter adapter;
    private int  appColor;
    private FloatingActionButton  fab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_student_qualification, container, false);
        recycler_view_qualification_list = (RecyclerView) convertView.findViewById(R.id.recycler_view_student_qualification_qualification_list);
        fab=(FloatingActionButton)  convertView.findViewById(R.id.fab_student_qualification_new_qualification);
        fab.setBackgroundTintList(ColorStateList.valueOf(appColor));
       fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UpdateStaffActivity)getActivity()).addQualification();
            }
        });
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecycler();
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_qualification_list.setLayoutManager(layoutManager);
        adapter = new StaffQualificationAdapter(getContext(), qualificationListData,appColor);
        recycler_view_qualification_list.setAdapter(adapter);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appColor = ((UpdateStaffActivity) getActivity()).getAppColor();
        qualificationListData = ((UpdateStaffActivity) getActivity()).getQualification();

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void updateList(ArrayList<Qualification> qualificationListData) {
        this.qualificationListData = qualificationListData;

        this.adapter.updateList(qualificationListData);


    }

}