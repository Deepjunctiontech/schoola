package in.junctiontech.school.updatestaff;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.models.Qualification;
import in.junctiontech.school.models.StaffDetail;

public class UpdateStaffActivity extends AppCompatActivity {

    private ArrayList<MasterEntry> masterEntryPosList;
    private StaffDetail staffData;
    private AppCompatSpinner spinner_update_teacher_staff_position;
    private EditText et_update_teacher_staff_name, et_update_teacher_staff_email, et_update_teacher_staff_mobile,
            et_update_teacher_present_address, et_update_teacher_permanent_address;
    private Button btn_update_teacher_dob;
    private LinearLayout ll_update_teacher_qualification_list;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private SharedPreferences sp;
    private String dbName;
    private Type typeQuali;
    private ArrayList<Qualification> qualificationArrayList = new ArrayList<>();
    private ArrayList<String> staffPosition = new ArrayList<>();
    private ArrayAdapter<String> adapterSpinner;
    private Gson gson;
    private Type type, typeSibling;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private StaffBasicDetailFragment fragmentBasicDetail;
    private StaffQualificationFragment fragmentQualificationDetail;
    private boolean isDialogShowing = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean updation = false;

    private boolean logoutAlert = false;
    private FrameLayout snackbar;


    private   int colorIs;
    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
     //   getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        tabLayout.setBackgroundColor(colorIs);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //   LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_create_student2);
        snackbar = (FrameLayout) findViewById(R.id.snackbar_view_pager);

        dbName = sp.getString("organization_name", "");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gson = new Gson();
        type = new TypeToken<Qualification>() {
        }.getType();
        masterEntryPosList = (ArrayList<MasterEntry>) getIntent().getSerializableExtra("masterEntryData");
        staffData = (StaffDetail) getIntent().getSerializableExtra("data");
        //  initViews();


        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setColorApp();
        tabLayout.setupWithViewPager(viewPager);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        getQualificationList();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (qualificationArrayList.size() == 0)
                        getQualificationList();

                }
            }
        };
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.login);
        tabLayout.getTabAt(1).setIcon(android.R.drawable.ic_menu_my_calendar);

    }

    private void setupViewPager(ViewPager viewPager) {
        UpdateStaffActivity.ViewPagerAdapter adapter = new UpdateStaffActivity.ViewPagerAdapter(getSupportFragmentManager());
        fragmentBasicDetail = new StaffBasicDetailFragment();
        fragmentQualificationDetail = new StaffQualificationFragment();

        adapter.addFragment(fragmentBasicDetail, getString(R.string.basic_details));
        adapter.addFragment(fragmentQualificationDetail, getString(R.string.qualification));


        viewPager.setAdapter(adapter);

    }

    public void deleteQualification(final Qualification qualification, int position) {
        AlertDialog.Builder alertDelete = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertDelete.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


              /*  if (!Config.checkInternet(UpdateStaffActivity.this)) {
                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }

                } else {*/

                    progressbar.show();


                    JSONObject job_filter = new JSONObject();
                    try {
                        job_filter.put("QualificationId", qualification.getQualificationId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                    //  param_main.put("data", new JSONObject(param));
                    param_main.put("filter", job_filter);


                    Log.e("deleteResultClass", new JSONObject(param_main).toString());


                    String deleteClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "QualificationApi.php?databaseName=" + dbName +
                            "&data=" + (new JSONObject(param_main));

                    Log.d("deleteClassUrl", deleteClassUrl);
                    StringRequest request = new StringRequest(Request.Method.DELETE,
                            //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                            deleteClassUrl

                            //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                            ,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String s) {
                                    DbHandler.longInfo(s);
                                    Log.e("deleteQualification", s);
                                    progressbar.cancel();
                                    try {
                                        JSONObject jsonObject = new JSONObject(s);

                                        if (jsonObject.optString("code") .equalsIgnoreCase("200") && !isFinishing()) {

                                            qualificationArrayList.remove(qualification);
                                            fragmentQualificationDetail.updateList(qualificationArrayList);
                                            Toast.makeText(UpdateStaffActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !isFinishing()) {
                                                Config.responseVolleyHandlerAlert(UpdateStaffActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                            Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                        }else {

                                            alert.setMessage(jsonObject.optString("message"));
                                            if (!isFinishing())
                                                alert.show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("deleteQualification", volleyError.toString());
                            progressbar.cancel();
                            Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);

                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {

                            Map<String, String> header = new LinkedHashMap<String, String>();
                            header.put("Content-Type", "application/x-www-form-urlencoded");
                            return super.getHeaders();
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new LinkedHashMap<String, String>();
                            return params;
                        }
                    };

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    AppRequestQueueController.getInstance(UpdateStaffActivity.this).addToRequestQueue(request);
               // }


            }
        });

        alertDelete.setMessage(R.string.are_you_sure_you_want_to_delete);
        alertDelete.show();
    }

    public void addQualification() {
        final View item_add_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
        final TextView tv_item_qualification_degree_name = (TextView) item_add_qualification.findViewById(R.id.tv_item_qualification_degree_name);
        final EditText et_item_qualification_board_university = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_board_university);
        final EditText et_item_qualification_degree_name = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_degree_name);
        final TextView et_item_qualification_year = (TextView) item_add_qualification.findViewById(R.id.et_item_qualification_year);
        final EditText et_item_qualification_marks = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_marks);
        final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_add_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);

        ((TextView)item_add_qualification.findViewById(R.id.tv_item_qualification_degree_name_title)).setTextColor(colorIs);
        ((TextView)item_add_qualification.findViewById(R.id.et_item_qualification_board_university_title)).setTextColor(colorIs);
        ((TextView)item_add_qualification.findViewById(R.id.et_item_qualification_year_title)).setTextColor(colorIs);
        ((TextView)item_add_qualification.findViewById(R.id.et_item_qualification_marks_title)).setTextColor(colorIs);

        ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
        tv_item_qualification_degree_name.setVisibility(View.GONE);

        AlertDialog.Builder alert_add_qua = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alert_add_qua.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_item_qualification_board_university.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_degree_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_year.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_marks.getText().toString().equalsIgnoreCase("")
                        ) {
                    Toast.makeText(UpdateStaffActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();
                } else
                    try {
                        addQualificationToServer(et_item_qualification_board_university.getText().toString(),
                                et_item_qualification_degree_name.getText().toString(),
                                et_item_qualification_year.getText().toString(),
                                et_item_qualification_marks.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


            }
        });
        alert_add_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        et_item_qualification_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * Hides the soft keyboard
                 */

                hideSoftKeyboard(item_add_qualification);
                /*** * */
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(
                        UpdateStaffActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                et_item_qualification_year.setText(year + "");

                            }
                        },
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        alert_add_qua.setTitle(getString(R.string.add_qualification));
        alert_add_qua.setView(item_add_qualification);
        alert_add_qua.setCancelable(false);
        alert_add_qua.show();
    }
    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard(View view) {
        /**
         * Hides the soft keyboard
         */

        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        /*** * */


    }
    private void addQualificationToServer(final String board_university, final String degree_name, final String year,
                                          final String marks) throws JSONException {

            progressbar.show();
            final JSONObject param = new JSONObject();

            param.put("Type", "Staff");
            param.put("UniqueId", staffData.getStaffId());
            param.put("BoardUniversity", board_university);
            param.put("Class", degree_name);
            param.put("Year", year);
            param.put("Marks", marks);
            param.put("Remarks", "");




            String QualiUrl = sp.getString("HostName", "Not Found")

                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "QualificationApi.php?databaseName=" + dbName;
            Log.d("QualiUrl", QualiUrl);
            Log.d("param", param.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    QualiUrl,param
                    //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("ResultQualification",jsonObject.toString());
                            progressbar.cancel();


                                if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                    int id = jsonObject.optInt("result");
                                    qualificationArrayList.add(new Qualification(id + "", "Staff",
                                            staffData.getStaffId(),
                                            board_university,
                                            degree_name,
                                            year,
                                            marks
                                    ));
                                    if (!isFinishing())
                                        fragmentQualificationDetail.updateList(qualificationArrayList);
                                    Toast.makeText(UpdateStaffActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                                }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStaffActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }else {

                                    alert.setMessage(jsonObject.optString("message"));
                                    if (!isFinishing()) alert.show();
                                }



                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("ResultClass", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            })  ;

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);


       // }

    }

    public ArrayList<Qualification> getQualification() {
        return qualificationArrayList;
    }

    public StaffDetail getStaffData() {
        return staffData;
    }

    public ArrayList<String> getPositionListData() {
        return staffPosition;
    }

    public ArrayList<MasterEntry> getMasterEntryPosition() {
        return masterEntryPosList;
    }

    public void convertDateOfBirthToString(String dob) {

      if (!dob.equalsIgnoreCase("")){
            progressbar.show();
            String  convertDobUrl=   sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "Conversion.php?type=dateToString&date=" +
                    dob;
            Log.d("convertDobUrl",convertDobUrl);
            StringRequest request = new StringRequest(Request.Method.GET,
                    convertDobUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("dateStrResult", s);
                            progressbar.dismiss();
                            staffData.setStaffDOB(s);


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateStrResult", volleyError.toString());
                    progressbar.dismiss();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

       }
    }

    public void convertStringToDateDOB(final String keyString) {

            progressbar.show();
            String date = "";
            if (keyString.equalsIgnoreCase("dob"))
                date = staffData.getStaffDOB();
            else if (keyString.equalsIgnoreCase("doj"))
                date = staffData.getStaffDOJ();


            String convStrToDate=sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "Conversion.php?type=stringToDate&date=" +
                    date;
            Log.d("convStrToDate",convStrToDate);

            StringRequest request = new StringRequest(Request.Method.GET,
                    convStrToDate
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("dateResult", s);
                            progressbar.dismiss();
                          if (!isFinishing()) {
                              try {
                                  s = new JSONObject(s).optString("result");
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }
                              fragmentBasicDetail.setDateOfBirth(s, keyString);
                          }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateResult", volleyError.toString());
                    progressbar.dismiss();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

       // }
    }

    public void updateTeacherData(final StaffDetail staffObj) throws JSONException, UnsupportedEncodingException {


            progressbar.show();
            JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("StaffId", staffObj.getStaffId());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();



             /*   if (staffData.getStaffProfileImage()!=null) {
                    if (staffData.getStaffProfileImage().contains("/")) {
                        String[] logoUrlArray = staffData.getStaffProfileImage().split("/");
                        staffData.setStaffProfileImage(logoUrlArray[logoUrlArray.length - 1]);
                    }
                }else staffData.setStaffProfileImage("");*/

                JSONObject jsonObject = (new JSONObject( ));
                jsonObject.put("StaffName", URLEncoder.encode((null==staffData.getStaffName()?"":staffData.getStaffName()) ,"utf-8"));
                jsonObject.put("StaffMobile",  staffData.getStaffMobile()  );
                jsonObject.put("StaffEmail", URLEncoder.encode(null==staffData.getStaffEmail()?"":staffData.getStaffEmail() ,"utf-8"));
                jsonObject.put("StaffFName", URLEncoder.encode(null==staffData.getStaffFName()?"":staffData.getStaffFName() ,"utf-8"));

                jsonObject.put("StaffMName", URLEncoder.encode(null==staffData.getStaffMName()?"":staffData.getStaffMName() ,"utf-8"));
                jsonObject.put("StaffDOB",  staffData.getStaffDOB() );
                jsonObject.put("StaffPresentAddress", URLEncoder.encode(null==staffData.getStaffPresentAddress()?"":staffData.getStaffPresentAddress() ,"utf-8"));
                jsonObject.put("StaffPermanentAddress", URLEncoder.encode(null==staffData.getStaffPermanentAddress()?"":staffData.getStaffPermanentAddress() ,"utf-8"));

                param_main.put("data", jsonObject);


            param_main.put("filter", job_filter);


            Log.e("staff", new JSONObject(param_main).toString());

            String staffUrl= sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "StaffApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param_main))
;

            Log.d("staffUrl",staffUrl);
            StringRequest request = new StringRequest(Request.Method.PUT,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    staffUrl
                    //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("UpdatestaffRes", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    staffData = staffObj;
                                    updation = true;
                                    Toast.makeText(UpdateStaffActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStaffActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }else {
                                    updation = false;

                                    alert.setMessage(jsonObject.optString("message"));

                                    if (!isFinishing()) alert.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("UpdatestaffRes", volleyError.toString());
                    updation = false;
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void getDateToString(String datestr) {

     /*   if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
     if(!datestr.equalsIgnoreCase("")){
            progressbar.show();
            String convStrToTime=sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "Conversion.php?type=dateToString&date=" +
                    datestr;
            Log.d("convStrToTime",convStrToTime);
            StringRequest request = new StringRequest(Request.Method.GET,
                    convStrToTime
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("dateStrResult", s);
                            progressbar.dismiss();


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateStrResult", volleyError.toString());
                    progressbar.dismiss();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        }

    }

    private void getQualificationList() {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
            progressbar.show();
            Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("Type", "Staff");
            param.put("UniqueId", staffData.getStaffId());
            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));

            Log.e("filter", (new JSONObject(param1).toString()));

            String qualGetUrl= sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "QualificationApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param1));
            Log.d("qualGetUrl",qualGetUrl);

            StringRequest request = new StringRequest(Request.Method.GET,
                    qualGetUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("Qualification", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {
                                    Qualification obj = gson.fromJson(s, type);
                                    qualificationArrayList = obj.getResult();
                                    fragmentQualificationDetail.updateList(qualificationArrayList);
                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStaffActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("Qualification", volleyError.toString());
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    @Override
    public void onBackPressed() {

//        Intent intent = new Intent(this, CreateStudentActivity.class);
//        intent.putExtra("data", staffData);
//        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));
//        if (updation) {
//            setResult(RESULT_OK, intent);
//
//        } else {
//            setResult(RESULT_CANCELED, intent);
//
//        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
//        Intent intent = new Intent(this, CreateStudentActivity.class);
//        intent.putExtra("data", staffData);
//        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));
//        if (updation) {
//            setResult(RESULT_OK, intent);
//
//        } else {
//            setResult(RESULT_CANCELED, intent);
//
//        }

        finish();

        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

//    public void addQualificationToLayout() {
//        ll_update_teacher_qualification_list.removeAllViews();
//        for (final Qualification qualificationObj : qualificationArrayList) {
//            final View item_add_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
//            (item_add_qualification.findViewById(R.id.ll_item_view_full_qualification_detail)).setVisibility(View.GONE);
//
//            TextView tv_item_qualification_degree_name = (TextView) item_add_qualification.findViewById(R.id.tv_item_qualification_degree_name);
//            TextView tv_item_qualification_subtext = (TextView) item_add_qualification.findViewById(R.id.tv_item_qualification_subtext);
//            tv_item_qualification_subtext.setVisibility(View.VISIBLE);
//            Button btn_item_qualification_delete = (Button) item_add_qualification.findViewById(R.id.btn_item_qualification_delete);
//            btn_item_qualification_delete.setVisibility(View.VISIBLE);
//            btn_item_qualification_delete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    qualificationArrayList.remove(qualificationObj);
//                    ll_update_teacher_qualification_list.removeViewAt(
//                            ll_update_teacher_qualification_list.indexOfChild(item_add_qualification)
//                    );
//                }
//            });
//            tv_item_qualification_subtext.setText(
//                    getString(R.string.board_university) + " : " +
//                            qualificationObj.getBoardUniversity() + "\n"
//                            + getString(R.string.passing_year) + " : " +
//                            qualificationObj.getYear() + "\n" +
//                            getString(R.string.marks) + " : " + qualificationObj.getMarks());
//            tv_item_qualification_degree_name.setText(qualificationObj.getDegreeName());
//            tv_item_qualification_degree_name.setVisibility(View.VISIBLE);
//
//            ll_update_teacher_qualification_list.addView(item_add_qualification);
//
//            tv_item_qualification_subtext.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    updateQualification(qualificationObj);
////                    android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(UpdateTeacherActivity.this);
////                    final String[] aa = new String[]{getString(R.string.update),
////                            getString(R.string.delete)
////                    };
////                    alert.setItems(aa, new DialogInterface.OnClickListener() {
////                        @Override
////                        public void onClick(DialogInterface dialog, int which) {
////
////                            if (which == 0) {
////
////
////
////                            } else if (which == 1) {
////                                qualificationArrayList.remove(qualificationObj);
////                                ll_update_teacher_qualification_list.removeViewAt(
////                                        ll_update_teacher_qualification_list.indexOfChild(item_add_qualification)
////                                );
////                            }
////
////
////                        }
////
////
////                    });
////
////                    alert.show();
//                                   }
//
//
//            });
//
//            item_add_qualification.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    updateQualification(qualificationObj);
////                    android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(UpdateTeacherActivity.this);
////                    final String[] aa = new String[]{getString(R.string.update),
////                            getString(R.string.delete)
////                    };
////                    alert.setItems(aa, new DialogInterface.OnClickListener() {
////                        @Override
////                        public void onClick(DialogInterface dialog, int which) {
////
////                            if (which == 0) {
////                                updateQualification(qualificationObj, ll_update_teacher_qualification_list.indexOfChild(item_add_qualification));
////
////
////                            } else if (which == 1) {
////                                qualificationArrayList.remove(qualificationObj);
////                                ll_update_teacher_qualification_list.removeViewAt(
////                                        ll_update_teacher_qualification_list.indexOfChild(item_add_qualification)
////                                );
////                            }
////
////
////                        }
////
////                        private void updateQualification(final Qualification qualification, int pos) {
////                            final View item_update_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
////                            final TextView tv_item_qualification_degree_name = (TextView) item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name);
////                            final EditText et_item_qualification_board_university = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_board_university);
////                            final EditText et_item_qualification_degree_name = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_degree_name);
////                            final EditText et_item_qualification_year = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_year);
////                            final EditText et_item_qualification_marks = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_marks);
////                            final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_update_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);
////
////                            ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
////                            tv_item_qualification_degree_name.setVisibility(View.GONE);
////
////                            AlertDialog.Builder alert_update_qua = new AlertDialog.Builder(UpdateTeacherActivity.this, AlertDialog.THEME_HOLO_LIGHT);
////                            alert_update_qua.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
////                                @Override
////                                public void onClick(DialogInterface dialog, int which) {
////                                    qualification.setBoardUniversity(et_item_qualification_board_university.getText().toString());
////                                    qualification.setDegreeName(et_item_qualification_degree_name.getText().toString());
////                                    qualification.setYear(et_item_qualification_year.getText().toString());
////                                    qualification.setMarks(et_item_qualification_marks.getText().toString());
////
////                                    addQualificationToLayout();
////
////
////                                }
////                            });
////                            alert_update_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
////                                @Override
////                                public void onClick(DialogInterface dialog, int which) {
////                                }
////                            });
////
////                            alert_update_qua.setTitle(getString(R.string.update_qualification));
////                            alert_update_qua.setView(item_update_qualification);
////                            alert_update_qua.setCancelable(false);
////                            alert_update_qua.show();
////
////
////                        }
////                    });
////                    alert.show();
////
//
//
//                }
//            });
//
//
//        }
//    }

    public final void updateQualification(final Qualification qualification, final int pos) {
//        final View item_update_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
//        final TextView tv_item_qualification_degree_name = (TextView) item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name);
//        final EditText et_item_qualification_board_university = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_board_university);
//        final EditText et_item_qualification_degree_name = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_degree_name);
//        final EditText et_item_qualification_year = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_year);
//        final EditText et_item_qualification_marks = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_marks);
//        final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_update_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);
//
//        ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
//        tv_item_qualification_degree_name.setVisibility(View.GONE);
//
//        et_item_qualification_board_university.setText(qualification.getBoardUniversity());
//        et_item_qualification_degree_name.setText(qualification.getDegreeName());
//        et_item_qualification_year.setText(qualification.getYear());
//        et_item_qualification_marks.setText(qualification.getMarks());
//
//        AlertDialog.Builder alert_update_qua = new AlertDialog.Builder(UpdateTeacherActivity.this, AlertDialog.THEME_HOLO_LIGHT);
//        alert_update_qua.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                qualification.setBoardUniversity(et_item_qualification_board_university.getText().toString());
//                qualification.setDegreeName(et_item_qualification_degree_name.getText().toString());
//                qualification.setYear(et_item_qualification_year.getText().toString());
//                qualification.setMarks(et_item_qualification_marks.getText().toString());
//                addQualificationToLayout();
//            }
//        });
//        alert_update_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//
//        alert_update_qua.setTitle(getString(R.string.update_qualification));
//        alert_update_qua.setView(item_update_qualification);
//        alert_update_qua.setCancelable(false);
//        alert_update_qua.show();
//
        final View item_update_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
        final TextView tv_item_qualification_degree_name = (TextView) item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name);
        final EditText et_item_qualification_board_university = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_board_university);
        final EditText et_item_qualification_degree_name = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_degree_name);
        final TextView et_item_qualification_year = (TextView) item_update_qualification.findViewById(R.id.et_item_qualification_year);
        final EditText et_item_qualification_marks = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_marks);
        final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_update_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);

        ((TextView)item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name_title)).setTextColor(colorIs);
        ((TextView)item_update_qualification.findViewById(R.id.et_item_qualification_board_university_title)).setTextColor(colorIs);
        ((TextView)item_update_qualification.findViewById(R.id.et_item_qualification_year_title)).setTextColor(colorIs);
        ((TextView)item_update_qualification.findViewById(R.id.et_item_qualification_marks_title)).setTextColor(colorIs);



        ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
        tv_item_qualification_degree_name.setVisibility(View.GONE);

        et_item_qualification_board_university.setText(qualification.getBoardUniversity());
        et_item_qualification_degree_name.setText(qualification.getDegreeName());
        et_item_qualification_year.setText(qualification.getYear());
        et_item_qualification_marks.setText(qualification.getMarks());

        AlertDialog.Builder alert_update_qua = new AlertDialog.Builder(UpdateStaffActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        alert_update_qua.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (et_item_qualification_board_university.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_degree_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_year.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_marks.getText().toString().equalsIgnoreCase("")
                        ) {
                    Toast.makeText(UpdateStaffActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();
                } else
                    try {
                        updateQualificationToServer(qualification, pos,
                                et_item_qualification_board_university.getText().toString(),
                                et_item_qualification_degree_name.getText().toString(),
                                et_item_qualification_year.getText().toString(),
                                et_item_qualification_marks.getText().toString()
                        );
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
            }
        });
        alert_update_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        et_item_qualification_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * Hides the soft keyboard
                 */

                hideSoftKeyboard(item_update_qualification);
                /*** * */
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(
                        UpdateStaffActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                et_item_qualification_year.setText(year + "");

                            }
                        },
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });


        alert_update_qua.setTitle(getString(R.string.update_qualification));
        alert_update_qua.setView(item_update_qualification);
        alert_update_qua.setCancelable(false);
        alert_update_qua.show();
    }

    private void updateQualificationToServer(final Qualification qualification, final int pos, final String board_university, final String degree_name,
                                             final String year, final String marks) throws UnsupportedEncodingException {


            final Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("Type", qualification.getType());
            param.put("UniqueId", qualification.getUniqueId());
            param.put("BoardUniversity", URLEncoder.encode(board_university.replaceAll(" ", "_"),"utf-8"));
            param.put("Class", URLEncoder.encode(degree_name.replaceAll(" ", "_"),"utf-8"));
            param.put("Year", year);
            param.put("Marks", marks);
            param.put("Remarks", "");

            JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("QualificationId", qualification.getQualificationId());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
            param_main.put("data", new JSONObject(param));
            param_main.put("filter", job_filter);


            Log.e("UpdateQualification", sp.getString("HostName", "Not Found") + "QualificationApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param_main)));

            String qualUpdateUrl=  sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "QualificationApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param_main))
;
            Log.d("qualUpdateUrl",qualUpdateUrl);
            StringRequest request = new StringRequest(Request.Method.PUT,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    qualUpdateUrl
                    //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("UpdateQualiResponse", s + " ritu");
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {
                                    qualification.setBoardUniversity(board_university);
                                    qualification.setDegreeName(degree_name);
                                    qualification.setYear(year);
                                    qualification.setMarks(marks);
                                    qualificationArrayList.set(pos, qualification);
                                    fragmentQualificationDetail.updateList(qualificationArrayList);
                                    Toast.makeText(UpdateStaffActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                                }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStaffActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }else {

                                    alert.setMessage(jsonObject.optString("message"));
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("UpdateQualification", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(UpdateStaffActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    //   params.put("data",( new JSONObject(param)).toString());

                    //  params.put("className", et_create_class_new_class_name.getText().toString());
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
       // }
    }
}
