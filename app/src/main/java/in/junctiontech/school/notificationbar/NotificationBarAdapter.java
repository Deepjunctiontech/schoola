package in.junctiontech.school.notificationbar;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;

/**
 * Created by JAYDEVI BHADE on 11/11/2016.
 */

public class NotificationBarAdapter extends RecyclerView.Adapter<NotificationBarAdapter.MyViewHolder> {
    private ArrayList<NoticeBoardEntity> notification_list;
    private Context context;

    public NotificationBarAdapter(Context context, ArrayList<NoticeBoardEntity> notification_list) {
        this.notification_list = notification_list;
        this.context = context;
    }
    public void updateList(ArrayList<NoticeBoardEntity> notification_list){
        this.notification_list = notification_list;
        this.notifyDataSetChanged();
        Log.e("change","yes");
    }

    @Override
    public NotificationBarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_view_layout, parent, false);

        return new NotificationBarAdapter.MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final NotificationBarAdapter.MyViewHolder holder, final int position) {
        NoticeBoardEntity obj = notification_list.get(position);

            holder.tv_name.setText(obj.Description);
            holder.notification_date.setText(obj.Date);

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return notification_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView notification_date;
        TextView tv_name; ImageView iv_notification;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_notification);
            notification_date = (TextView) itemView.findViewById(R.id.notification_date);
            iv_notification = (ImageView)itemView.findViewById(R.id.iv_notification);
            iv_notification.startAnimation(AnimationUtils.
                    loadAnimation(context,R.anim.blink));
        //    itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }

}
