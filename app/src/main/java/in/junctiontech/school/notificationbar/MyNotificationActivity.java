package in.junctiontech.school.notificationbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;

public class MyNotificationActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<NoticeBoardEntity> notificationList= new ArrayList<>();
    private RecyclerView rv_latest_notification_list;
    private NotificationBarAdapter madapter;
    private TextView tv_notification_board_title, tv_no_notifications_available_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // add for remove the title bar from activity


        //TODO uncomment and make it work

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latest_notification);
        sp = Prefs.with(this).getSharedPreferences();
        notificationList = (ArrayList<NoticeBoardEntity>) getIntent().getSerializableExtra("data");
        rv_latest_notification_list = (RecyclerView) findViewById(R.id.rv_latest_notification_list);
        tv_notification_board_title = ((TextView) findViewById(R.id.tv_notification_board_title));
        tv_no_notifications_available_title = ((TextView) findViewById(R.id.tv_no_notifications_available_title));
        int colorIs = Config.getAppColor(this, false);
        tv_notification_board_title.setTextColor(colorIs);
        tv_no_notifications_available_title.setTextColor(colorIs);

        tv_notification_board_title.setText(getString(R.string.my_notification));

        NotificationEvent obj = (new Gson()).fromJson(sp.getString(Config.MY_NOTIFICATION, ""), new TypeToken<NotificationEvent>() {
        }.getType());
        if (obj!=null) {
//            notificationList = obj.getResult();
            Collections.reverse(notificationList);
        }
        if (notificationList==null)notificationList= new ArrayList<>();
        setUpRecyclerView();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.NEW_MY_NOTIFICATION)) {
                    NotificationEvent obj = (new Gson()).fromJson(sp.getString(Config.MY_NOTIFICATION, ""), new TypeToken<NotificationEvent>() {
                    }.getType());
//                    notificationList = obj.getResult();
                    if (notificationList!=null ) {
                        Collections.reverse(notificationList);
                        madapter.updateList(notificationList);
                    }
                    if (notificationList.size()==0){
                        tv_no_notifications_available_title.setVisibility(View.VISIBLE);
                    }else tv_no_notifications_available_title.setVisibility(View.GONE);
                }else if (intent.getAction().equalsIgnoreCase(Config.updateNotification)) {
//                    NotificationEvent obj = (new Gson()).fromJson(sp.getString(Config.updateNotification, ""), new TypeToken<NotificationEvent>() {
//                    }.getType());
//                    notificationList = obj.getResult();
                    if (notificationList!=null ) {
                        Collections.reverse(notificationList);
                        madapter.updateList(notificationList);
                    }
                    if (notificationList.size()==0){
                        tv_no_notifications_available_title.setVisibility(View.VISIBLE);
                    }else tv_no_notifications_available_title.setVisibility(View.GONE);
                }
            }
        };
        if (notificationList.size()==0){
            tv_no_notifications_available_title.setVisibility(View.VISIBLE);
        }else tv_no_notifications_available_title.setVisibility(View.GONE);
    }

    public void setUpRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_latest_notification_list.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        rv_latest_notification_list.setItemAnimator(itemAnimator);
        madapter = new NotificationBarAdapter(getBaseContext(), notificationList);
        rv_latest_notification_list.setAdapter(madapter);
    }

    public void cancel(View view) {

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.NEW_MY_NOTIFICATION)); LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.updateNotification));

        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        super.onPause();
    }
}
