package in.junctiontech.school.notificationbar;

import androidx.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.NotificationEvent;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.NoticeBoardEntity;
import in.junctiontech.school.schoolnew.common.Gc;

public class NotificationBarActivity extends AppCompatActivity {
    private ArrayList<NoticeBoardEntity> notificationList = new ArrayList<NoticeBoardEntity>();
    private RecyclerView rv_latest_notification_list;
    private NotificationBarAdapter madapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private SharedPreferences sp;
    private MainDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // add for remove the title bar from activity
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        mDb = MainDatabase.getDatabase(getApplicationContext());


        setContentView(R.layout.activity_latest_notification);

        rv_latest_notification_list =  findViewById(R.id.rv_latest_notification_list);
        String Dates = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());
        mDb.noticeBoardModel().getNoticesActive(Gc.ACTIVE,Dates).observe(this, new Observer<List<NoticeBoardEntity>>() {
            @Override
            public void onChanged(@Nullable List<NoticeBoardEntity> noticeBoardEntities) {
                notificationList.clear();
                notificationList.addAll(noticeBoardEntities);
                setUpRecyclerView();

            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.updateNotification)) {
                    NotificationEvent obj = (new Gson()).fromJson(sp.getString(Config.updateNotification, ""), new TypeToken<NoticeBoardEntity>() {
                    }.getType());
//                    notificationList = obj.getResult();
//                    madapter.updateList(notificationList);
                }
            }
        };

    }


    public void setUpRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_latest_notification_list.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        rv_latest_notification_list.setItemAnimator(itemAnimator);
        madapter = new NotificationBarAdapter(getBaseContext() , notificationList);
//        madapter = new NotificationBarAdapter(getBaseContext(), notificationList);
        rv_latest_notification_list.setAdapter(madapter);
    }

    public void cancel(View view) {
        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.updateNotification));

        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        super.onPause();
    }


}
