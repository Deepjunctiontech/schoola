package in.junctiontech.school;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 8/26/2016.
 */
public class AppRequestQueueController  {

    private RequestQueue mRequestQueue;
    private ArrayList<AsyncTask> asyncTask = new ArrayList<>();
    private Context context;
public static AppRequestQueueController mInstance;

    private AppRequestQueueController(Context context) {
        this.context = context;
    }


    public static synchronized AppRequestQueueController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AppRequestQueueController(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
    public void cancelRequestByTag(String  tag){
        getRequestQueue().cancelAll(tag);
    }

    public void addToRequestQueue(StringRequest strReq, String tag) {
        getRequestQueue().add(strReq).setTag(tag);
    }
    public void addToRequestQueue(JsonObjectRequest strReq, String tag) {
        getRequestQueue().cancelAll(tag);
        getRequestQueue().add(strReq).setTag(tag);
    }

    public void addToAsyncTask(AsyncTask asyncTask1){
        asyncTask.add(asyncTask1);
    }

    public ArrayList<AsyncTask> getAsyncTask() {
        return asyncTask;
    }
}
