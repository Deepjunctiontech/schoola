package in.junctiontech.school.schooldetails;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.Communicator.Communicator;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.country.CityCode;
import in.junctiontech.school.models.country.CountryCode;
import in.junctiontech.school.models.country.StateCode;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;

/**
 * Created by JAYDEVI BHADE on 9/26/2016.
 * updated by Deep on 12-April-2018
 */

public class SchoolAddressFragment extends Fragment {
    private SharedPreferences sp;
    private MainDatabase mDb;
    private ProgressDialog progressbar;
    private EditText et_general_setting_address, et_school_detail_pin_code,
            et_school_detail_phone_number, et_school_detail_alternative_phone_number,
            et_school_detail_landline_number, et_school_detail_school_email, et_school_detail_fax_number,
            et_school_detail_country_code,et_landline_country_code,et_alternative_country_code;

    private Button btn_general_setting_next, btn_general_setting_previous;
    private SchoolDetailsEntity generalSettingDataObj;
    private Communicator comm;
    private ArrayList<CountryCode> countryCodeList = new ArrayList<>();
    private AutoCompleteTextView actv_school_detail_country, actv_school_detail_state, actv_school_detail_city,
            actv_school_detail_district;
    private ArrayAdapter<String> adapterCountry;
    private ArrayList<String> countryNameCodeList = new ArrayList<>();
    private ArrayList<String> stateNameCodeList = new ArrayList<>();
    private ArrayList<String> cityNameCodeList = new ArrayList<>();

    private ArrayList<StateCode> stateCodeList = new ArrayList<>();
    private ArrayList<CityCode> cityCodeList = new ArrayList<>();
    private ArrayAdapter<String> adapterState;
    private ArrayAdapter<String> adapterCity;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int  appColor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = MainDatabase.getDatabase(getContext());
//        generalSettingDataObj = mDb.schoolDetailsModel().getSchoolDetails();
        appColor =((SchoolDetailsActivity) getActivity()).getAppColor();
        sp = Prefs.with(getActivity()).getSharedPreferences();
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        getCountryCode();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (countryCodeList.size() == 0)
                       getCountryCode();
                    if (stateCodeList.size()==0)
                      fetchStateListFromServer();
                    if (cityCodeList.size()==0)
                        fetchCityListFromServer();
                }
            }
        };
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onDetach();
    }

    private void getCountryCode() {
        //progressbar.show();

        String url = Config.FETCH_COUNTRY;

        Log.e("COUNTRY_CODE", url);

        StringRequest request = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("COUNTRY_CODE", s + "");
                        //   progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("Code").equalsIgnoreCase("200")) {
                                CountryCode obj = (new Gson()).fromJson(s, new TypeToken<CountryCode>() {
                                }.getType());
                                countryCodeList = obj.getResult();
                                setCountryAdapter();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }

    private void setCountryAdapter() {
        if (getContext() != null) {
            for (CountryCode countryObj : countryCodeList)
                countryNameCodeList.add(countryObj.getCountryName());
            adapterCountry = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, countryNameCodeList);
            //adapterSpinner.set
            actv_school_detail_country.setAdapter(adapterCountry);
        }

        fetchStateListFromServer();
       /* actv_school_detail_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fetchStateListFromServer();
            }
        });*/

        actv_school_detail_country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (countryCodeList.size() == 0)
                    getCountryCode();
                fetchStateListFromServer();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void fetchStateListFromServer() {
        //  progressbar.show();
        stateCodeList = new ArrayList<>();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("countryId", filterCountry(countryCodeList, actv_school_detail_country.getText().toString().trim()));
        String url = Config.FETCH_STATE + new JSONObject(param);

        Log.e("STATE_CODE", url);

        StringRequest request = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("STATE_CODE", s + "");
                        //  progressbar.cancel();
                        try {

                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("Code").equalsIgnoreCase("200")) {
                                StateCode obj = (new Gson()).fromJson(s, new TypeToken<StateCode>() {
                                }.getType());
                                stateCodeList = new ArrayList<>();
                                stateCodeList = obj.getResult();
                            }
                            setStateAdapter();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                setStateAdapter();


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("STATE_CODE");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "STATE_CODE");

    }

    private void setStateAdapter() {
        stateNameCodeList.clear();
        if (getContext() != null) {
            for (StateCode stateObj : stateCodeList)
                stateNameCodeList.add(stateObj.getName());
            adapterState = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, stateNameCodeList);
            //adapterSpinner.set
            actv_school_detail_state.setAdapter(adapterState);
        }
        fetchCityListFromServer();
        /*  actv_school_detail_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fetchCityListFromServer();

            }
        });*/
        actv_school_detail_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                fetchCityListFromServer();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void fetchCityListFromServer() {
        //  progressbar.show();
        cityCodeList = new ArrayList<>();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("citynameId", filterState(stateCodeList, actv_school_detail_state.getText().toString().trim()));
        String url = Config.FETCH_CITY + new JSONObject(param);

        Log.e("CITY_CODE", url);

        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("STATE_CODE", s + "");
                        //    progressbar.cancel();
                        try {

                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("Code").equalsIgnoreCase("200")) {
                                CityCode obj = (new Gson()).fromJson(s, new TypeToken<CityCode>() {
                                }.getType());
                                cityCodeList = new ArrayList<>();
                                cityCodeList = obj.getResult();
                            }
                            setCityAdapter();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                // progressbar.cancel();
                setCityAdapter();


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("CITY_CODE");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "CITY_CODE");

    }

    private void setCityAdapter() {
        if (getContext() != null) {
            cityNameCodeList.clear();
            for (CityCode cityObj : cityCodeList)
                cityNameCodeList.add(cityObj.getName());
            adapterCity = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, cityNameCodeList);
            //adapterSpinner.set
            actv_school_detail_city.setAdapter(adapterCity);
//            actv_school_detail_district.setAdapter(adapterCity);
        }
    }

    private String filterState(ArrayList<StateCode> stateCodeList, String text) {
        for (StateCode stateCode : stateCodeList) {
            if (stateCode.getName().equalsIgnoreCase(text)) {
                return stateCode.getStategeonameId();
            }
        }
        return "";
    }

    private String filterCountry(ArrayList<CountryCode> countryCodeList, String text) {
        //    et_school_detail_country_code.setText("+");

        for (CountryCode countryObj : countryCodeList) {
            if (countryObj.getCountryName().equalsIgnoreCase(text)) {
                et_school_detail_country_code.setText(countryObj.getCode());
                et_landline_country_code.setText(countryObj.getCode());
                et_alternative_country_code.setText(countryObj.getCode());
                sp.edit().putString("SchoolCountryCode", et_school_detail_country_code.getText().toString().trim())
                        .commit();
                return countryObj.getGeonameId();
            }
        }
        return "";
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_school_detail_address, container, false);

//        ((TextView) convertView.findViewById(R.id.tv_school_detail_title)).setBackgroundColor(appColor);

        actv_school_detail_country = (AutoCompleteTextView) convertView.findViewById(R.id.actv_school_detail_country);
        actv_school_detail_country.setThreshold(0);

        actv_school_detail_state = (AutoCompleteTextView) convertView.findViewById(R.id.actv_school_detail_state);
        actv_school_detail_state.setThreshold(0);

        actv_school_detail_city = (AutoCompleteTextView) convertView.findViewById(R.id.actv_school_detail_city);
        actv_school_detail_city.setThreshold(0);

        et_general_setting_address = (EditText) convertView.findViewById(R.id.et_general_setting_address);
        // et_school_detail_city = (EditText) convertView.findViewById(R.id.et_school_detail_city);
        //    et_school_detail_state = (EditText) convertView.findViewById(R.id.et_school_detail_state);

        //et_school_detail_country = (EditText) convertView.findViewById(R.id.et_school_detail_country);
        et_school_detail_pin_code = (EditText) convertView.findViewById(R.id.et_school_detail_pin_code);
        et_school_detail_phone_number = (EditText) convertView.findViewById(R.id.et_school_detail_phone_number);
        et_school_detail_country_code = (EditText) convertView.findViewById(R.id.et_school_detail_country_code);
        et_landline_country_code = (EditText) convertView.findViewById(R.id.et_school_detail_landline_country_code);
        et_alternative_country_code = (EditText) convertView.findViewById(R.id.et_school_detail_alternative_country_code);



        et_school_detail_alternative_phone_number = (EditText) convertView.findViewById(R.id.et_school_detail_alternative_phone_number);
        et_school_detail_landline_number = (EditText) convertView.findViewById(R.id.et_school_detail_landline_number);
        et_school_detail_school_email = (EditText) convertView.findViewById(R.id.et_school_detail_school_email);

        btn_general_setting_next = (Button) convertView.findViewById(R.id.btn_general_setting_next_school_address);
        btn_general_setting_previous = (Button) convertView.findViewById(R.id.btn_general_setting_previous_school_address);

        btn_general_setting_next.setTextColor(appColor);
        btn_general_setting_previous.setTextColor(appColor);
        return convertView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        comm = (Communicator) getActivity();

        if (generalSettingDataObj != null) {

            if (generalSettingDataObj.SchoolAddress != null)
                et_general_setting_address.setText(generalSettingDataObj.SchoolAddress.replaceAll("_", " "));

            if (generalSettingDataObj.Country != null) {
                actv_school_detail_country.setText(generalSettingDataObj.Country.replaceAll("_", " "));
                fetchStateListFromServer();
            }
            if (generalSettingDataObj.State != null) {
                actv_school_detail_state.setText(generalSettingDataObj.State.replaceAll("_", " "));
                fetchCityListFromServer();
            }

            if (generalSettingDataObj.City != null) {
                actv_school_detail_city.setText(generalSettingDataObj.City.replaceAll("_", " "));

            }
            if (generalSettingDataObj.District != null) {
//                actv_school_detail_district.setText(generalSettingDataObj.District.replaceAll("_", " "));
            }


            if (generalSettingDataObj.PIN != null)
                et_school_detail_pin_code.setText(generalSettingDataObj.PIN.replaceAll("_", " "));

            setMobileNumberAndCountryCode(generalSettingDataObj.Mobile,et_school_detail_country_code,et_school_detail_phone_number);
            setMobileNumberAndCountryCode(generalSettingDataObj.AlternateMobile,et_alternative_country_code,et_school_detail_alternative_phone_number);
            setMobileNumberAndCountryCode(generalSettingDataObj.Landline,et_landline_country_code,et_school_detail_landline_number);


           /* if (generalSettingDataObj.getAlternateMobile() != null)
                et_school_detail_alternative_phone_number.setText(generalSettingDataObj.getAlternateMobile().replaceAll("_", " "));

            if (generalSettingDataObj.getLandline() != null)
                et_school_detail_landline_number.setText(generalSettingDataObj.getLandline().replaceAll("_", " "));
*/
            if (generalSettingDataObj.Email != null)
                et_school_detail_school_email.setText(generalSettingDataObj.Email.replaceAll("_", " "));

            if (generalSettingDataObj.Fax != null)
//                et_school_detail_fax_number.setText(generalSettingDataObj.Fax.replaceAll("_", " "));

            et_general_setting_address.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.SchoolAddress = s.toString().trim() ;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
//            actv_school_detail_district.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    generalSettingDataObj.District = s.toString().trim() ;
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            });

            actv_school_detail_city.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.City = s.toString().trim() ;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            actv_school_detail_state.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.State = s.toString().trim() ;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            actv_school_detail_country.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.Country = s.toString().trim() ;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            et_school_detail_pin_code.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.PIN = s.toString().trim() ;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


           /* et_school_detail_country_code.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.setMobile(s.toString().trim()+"-"+et_school_detail_phone_number.getText().toString()
                    );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });*/

            et_school_detail_phone_number.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.Mobile = (et_school_detail_country_code.getText().toString().trim() + "-" +
                            s.toString().trim() );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            et_school_detail_alternative_phone_number.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.AlternateMobile = (s.toString().trim() );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            et_school_detail_landline_number.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.Landline = (s.toString().trim() );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            et_school_detail_school_email.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    generalSettingDataObj.Email = (s.toString().trim() );
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
//            et_school_detail_fax_number.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    generalSettingDataObj.Fax = (s.toString().trim() );
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            });

        }
        btn_general_setting_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generalSettingDataObj.Mobile = (et_school_detail_country_code.getText().toString().trim() + "-" +
                        et_school_detail_phone_number.getText().toString().trim()
                );
                generalSettingDataObj.AlternateMobile = (et_alternative_country_code.getText().toString().trim() + "-" +
                        et_school_detail_alternative_phone_number.getText().toString().trim()
                );
                generalSettingDataObj.Landline = (et_landline_country_code.getText().toString().trim() + "-" +
                        et_school_detail_landline_number.getText().toString().trim()
                );
                comm.setSchoolAddress(generalSettingDataObj);
                comm.callNext();
            }
        });
        btn_general_setting_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comm.setSchoolAddress(generalSettingDataObj);
                comm.callPrevious();
            }
        });
    }

    private void setMobileNumberAndCountryCode(String mobile, EditText et_country_code, EditText et_phone_number) {
        if (mobile != null) {
            if (mobile.contains("-")) {
                String[] mobilArr = mobile.trim().split("-");
                Log.e("RITU", mobilArr.length + "  " + mobile+ " RITU");
                switch (mobilArr.length) {
                    case 1:
                        et_country_code.setText(mobilArr[0]);
                        et_phone_number.setText("");
                        break;
                    case 2:
                        et_country_code.setText(mobilArr[0]);
                        et_phone_number.setText(mobilArr[1]);
                        break;
                    default:
                        et_country_code.setText("");
                        et_phone_number.setText(mobile.replaceAll("_", " "));
                        break;
                }

            } else
                et_phone_number.setText(mobile.replaceAll("_", " "));
        }
    }

}
