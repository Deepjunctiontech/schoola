package in.junctiontech.school.schooldetails;


import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.Communicator.Communicator;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;

/**
 * Created by JAYDEVI BHADE on 9/26/2016.
 */

public class SchoolBasicDetailFragment
        extends
        Fragment {



    private Button btn_session_start_date, btn_date_of_stablishment,
            btn_general_setting_next, btn_general_setting_previous;
    private EditText et_school_name, et_school_moto, et_school_board, et_affiliated_by,
            et_affiliated_number, et_registration_number;


    private SchoolDetailsEntity generalSettingDataObj;

    private MainDatabase mDb;
    private Communicator comm;
    int tvNumber =1;
    private int  appColor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDb = MainDatabase.getDatabase(getContext());
//        generalSettingDataObj = mDb.schoolDetailsModel().getSchoolDetails();
        appColor =((SchoolDetailsActivity) getActivity()).getAppColor();

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_school_detal_basic_details, container, false);



        btn_session_start_date =  convertView.findViewById(R.id.btn_general_setting_session_start_date);
        et_school_name = convertView.findViewById(R.id.et_general_setting_school_name);
        et_school_moto =  convertView.findViewById(R.id.et_general_setting_school_moto);
        et_school_board =  convertView.findViewById(R.id.et_general_setting_school_board);
        et_affiliated_by =  convertView.findViewById(R.id.et_general_setting_affiliated_by);
        et_affiliated_number =  convertView.findViewById(R.id.et_general_setting_affiliated_number);
        et_registration_number =  convertView.findViewById(R.id.et_general_setting_school_registration_number);

        btn_date_of_stablishment =  convertView.findViewById(R.id.btn_general_setting_date_of_stablishment);
        btn_general_setting_next= convertView. findViewById(R.id.btn_general_setting_next);
        btn_general_setting_previous=  convertView.  findViewById(R.id.btn_general_setting_previous);
        btn_general_setting_next.setTextColor(appColor);
        btn_general_setting_previous.setTextColor(appColor);
        return convertView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        comm = (Communicator) getActivity();

        if (generalSettingDataObj!=null){

        if (generalSettingDataObj.sessionStartDate == null ||
                generalSettingDataObj.sessionStartDate.equalsIgnoreCase("")) ;
        else {
            ((SchoolDetailsActivity)getActivity()).
                    getStringToDate(generalSettingDataObj.sessionStartDate,
                            "sessionStartDate"
                    );
//            btn_session_start_date.setText(getString(R.string.select_date)
//              );
            btn_session_start_date.setEnabled(false);
        }

        if (generalSettingDataObj.SchoolName != null)
            et_school_name.setText(generalSettingDataObj.SchoolName.replaceAll("_", " "));

        if (generalSettingDataObj.SchoolMoto != null)
            et_school_moto.setText(generalSettingDataObj.SchoolMoto.replaceAll("_", " "));

        if (generalSettingDataObj.Board != null)
            et_school_board.setText(generalSettingDataObj.Board.replaceAll("_", " "));

        if (generalSettingDataObj.AffiliatedBy != null)
            et_affiliated_by.setText(generalSettingDataObj.AffiliatedBy.replaceAll("_", " "));

        if (generalSettingDataObj.AffiliationNo != null)
            et_affiliated_number.setText(generalSettingDataObj.AffiliationNo.replaceAll("_", " "));

        if (generalSettingDataObj.RegistrationNo != null)
            et_registration_number.setText(generalSettingDataObj.RegistrationNo.replaceAll("_", " "));

        if (generalSettingDataObj.DateOfEstablishment != null) {
            ((SchoolDetailsActivity)getActivity()).
                    getStringToDate(generalSettingDataObj.DateOfEstablishment,
                            "dateOfStabilised"
                    );
            btn_date_of_stablishment.setText(getString(R.string.select_date)
            );
        }

        et_school_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.SchoolName = s.toString().trim() ;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_school_moto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.SchoolMoto = s.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_school_board.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.Board = s.toString().trim() ;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_affiliated_by.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.AffiliatedBy = s.toString().trim() ;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_affiliated_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.AffiliationNo = s.toString().trim() ;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_registration_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                generalSettingDataObj.RegistrationNo = s.toString().trim() ;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btn_session_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenersession_start_date, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        btn_date_of_stablishment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenerdate_of_stablishment, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(R.string.select_date);
                datePicker.show();
            }
        });
        }


        btn_general_setting_next  .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                comm.setBasicDetail(generalSettingDataObj);
                comm.callNext();
            }
        });
        btn_general_setting_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comm.setBasicDetail(generalSettingDataObj);
                comm.callPrevious();
            }
        });
    }


    public DatePickerDialog.OnDateSetListener myDateListenersession_start_date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            generalSettingDataObj.sessionStartDate = (year + "-" + new_month + "-" + new_day);
            btn_session_start_date.setText(year + "-" + new_month + "-" + new_day);

            ((SchoolDetailsActivity)getActivity()).
                    getDateToString("sessionStartDate",year + "-" + new_month + "-" + new_day);
            // Toast.makeText(getContext(), year + "-" + new_month + "-" + new_day, Toast.LENGTH_LONG).show();
        }
    };

    public DatePickerDialog.OnDateSetListener myDateListenerdate_of_stablishment = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_date_of_stablishment.setText(year + "-" + new_month + "-" + new_day);
            generalSettingDataObj.DateOfEstablishment = (year + "-" + new_month + "-" + new_day);

            ((SchoolDetailsActivity)getActivity()).
                    getDateToString("dateOfStablishment",year + "-" + new_month + "-" + new_day);

            //   Toast.makeText(getContext(), year + "-" + new_month + "-" + new_day, Toast.LENGTH_LONG).show();
        }
    };


    public void setSessionStartDate(String sessionStartDate) {
       if (btn_session_start_date!=null)
        btn_session_start_date.setText(sessionStartDate);
    }

    public void setDateOfStablished(String dateOfStablished) {
        if (btn_date_of_stablishment!=null)
    btn_date_of_stablishment.setText(dateOfStablished);
    }
}
