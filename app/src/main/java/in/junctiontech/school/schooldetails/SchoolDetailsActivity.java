package in.junctiontech.school.schooldetails;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.Communicator.Communicator;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.schoolsession.SessionActivity;
import in.junctiontech.school.models.SchoolSession;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.setup.SetupActivity;

public class    SchoolDetailsActivity extends AppCompatActivity implements Communicator {

    private MainDatabase mDb;

    private ViewPager viewPager;
    //    private ProgressBar progressbar_status;
    private int[] layouts;
    private TextView[] dots;
    private LinearLayout dotsLayout;
    private Gson gson;
    private SchoolDetailsEntity schoolDetailsEntity;
    String mandatory_fields = "";
    private SharedPreferences sp;

    private ProgressDialog progressbar;
    private android.app.AlertDialog.Builder alert;
    private SchoolBasicDetailFragment fragBasicDetail;
    private SchoolAddressFragment fragSchoolAddressDetails;
    private boolean isDialogShowing = false, isDataUpdated = false,isDataInserted = false;

    private   int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();

        mDb = MainDatabase.getDatabase(this);
        //LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_school_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        gson = new Gson();
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.updating_data));
        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {  isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

//        schoolDetailsEntity = mDb.schoolDetailsModel().getSchoolDetails();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        layouts = new int[]{
                R.layout.fragment_school_detal_basic_details,
                R.layout.fragment_school_detail_address/*,
                R.layout.fragment_school_details_add_school_logo*/
        };
        // adding bottom dots

        addBottomDots(0);

        // making notification bar transparent
      //  changeStatusBarColor();


    }

    @Override
    protected void onResume() {
        if (schoolDetailsEntity.SchoolName.isEmpty() ||
                schoolDetailsEntity.DateOfEstablishment.isEmpty() ||
                schoolDetailsEntity.Country.isEmpty() ||
                schoolDetailsEntity.Mobile.isEmpty() ||
                schoolDetailsEntity.Email.isEmpty()){
            Snackbar snackbarObj = Snackbar.make(findViewById(R.id.activity_school_details), R.string.update_basic_details,
                    Snackbar.LENGTH_LONG).setAction("",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.fragment_first_green));
            snackbarObj.show();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (schoolDetailsEntity==null || schoolDetailsEntity.Id == null || "".equalsIgnoreCase(schoolDetailsEntity.Id)) {
            Toast.makeText(this, "Please Fill School Details !!", Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent( );
            intent.putExtra("isDataUpdated",isDataUpdated);
            intent.putExtra("isDataInserted",isDataInserted);
            if (schoolDetailsEntity == null)
                setResult(RESULT_CANCELED, intent);
            else if (schoolDetailsEntity.Id.equalsIgnoreCase(""))
                setResult(RESULT_CANCELED, intent);
            else
                setResult(RESULT_OK, intent);

            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {

        if (schoolDetailsEntity==null || schoolDetailsEntity.Id == null || "".equalsIgnoreCase(schoolDetailsEntity.Id)) {
            Toast.makeText(this, "Please Fill School Details !!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            Intent intent = new Intent( );
            intent.putExtra("isDataUpdated",isDataUpdated);
            intent.putExtra("isDataInserted",isDataInserted);
            if (schoolDetailsEntity == null)
                setResult(RESULT_CANCELED, intent);
            else if ("".equalsIgnoreCase(schoolDetailsEntity.Id))
                setResult(RESULT_CANCELED, intent);
            else
                setResult(RESULT_OK, intent);

            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            return false;
        }
    }

    public void btnPrevious() {

        int currentPage = viewPager.getCurrentItem();
        int current = getItem(-1);
        switch (currentPage) {
            case 0:
                // startActivity(new Intent(this, AdminNavigationDrawer.class));

                if (schoolDetailsEntity.Id == null || "".equalsIgnoreCase(schoolDetailsEntity.Id)) {
                    Toast.makeText(this, "Please Fill School Details !!", Toast.LENGTH_SHORT).show();

                } else {
                    Intent intent = new Intent( );
                    intent.putExtra("isDataUpdated",isDataUpdated);
                    intent.putExtra("isDataInserted",isDataInserted);
                    if (schoolDetailsEntity == null)
                        setResult(RESULT_CANCELED, intent);
                    else if ("".equalsIgnoreCase(schoolDetailsEntity.Id))
                        setResult(RESULT_CANCELED, intent);
                    else
                        setResult(RESULT_OK, intent);

                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }


                //  overridePendingTransition(R.anim.enter, R.anim.nothing);
                break;
            case 1:


                // move to previous screen
                viewPager.setCurrentItem(current);
                break;

        }
    }

    public void btnNext() throws JSONException {

        int currentPage = viewPager.getCurrentItem();
        switch (currentPage) {

            case 0:
                if (checkSchoolBasicDetailsMandatoryFiels()) {
                    int current = getItem(+1);
                    viewPager.setCurrentItem(current);
                    mandatory_fields = "";

                } else {
                    android.app.AlertDialog.Builder alert_save_msg =
                            new android.app.AlertDialog.Builder(SchoolDetailsActivity.this,
                                    android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alert_save_msg.setPositiveButton(getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mandatory_fields = "";
                                }
                            });
                    alert_save_msg.setMessage(getString(R.string.kindally_fill_the_mandatory_fields)
                            + "\n" + mandatory_fields);
                    alert_save_msg.show();
                }
                break;

            case 1:
                if (checkSchoolAddresssMandatoryFiels()) {
                    try {
                        sendSchoolData();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                   /* int current = getItem(+1);
                    viewPager.setCurrentItem(current);
                    mandatory_fields = "";*/

                } else {
                    android.app.AlertDialog.Builder alert_save_msg =
                            new android.app.AlertDialog.Builder(SchoolDetailsActivity.this,
                                    android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alert_save_msg.setPositiveButton(getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mandatory_fields = "";
                                }
                            });
                    alert_save_msg.setMessage(getString(R.string.kindally_fill_the_mandatory_fields)
                            + "\n" + mandatory_fields);
                    alert_save_msg.show();
                }
                break;

        }


    }

    private boolean checkSchoolAddresssMandatoryFiels() {
        if (/*generalSettingDataObj.getSchoolAddress().equalsIgnoreCase("") ||
                generalSettingDataObj.getCity().equalsIgnoreCase("") ||
                generalSettingDataObj.getDistrict().equalsIgnoreCase("") ||
                generalSettingDataObj.getState().equalsIgnoreCase("") ||
                generalSettingDataObj.getCountry().equalsIgnoreCase("") ||
                generalSettingDataObj.getPIN().equalsIgnoreCase("") ||*/
                schoolDetailsEntity.Mobile.equalsIgnoreCase("-") ||
                        schoolDetailsEntity.Mobile.length() < 8 ||
                        schoolDetailsEntity.Country.equalsIgnoreCase("")||

                        (    schoolDetailsEntity.Email.equalsIgnoreCase("")?true:
                        (!schoolDetailsEntity.Email.contains("@")
                        ||
                        !schoolDetailsEntity.Email.contains(".")))
                ) {


            /*if (generalSettingDataObj.getSchoolAddress().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.address);

            if (generalSettingDataObj.getCity().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.city);

            if (generalSettingDataObj.getDistrict().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.district);

            if (generalSettingDataObj.getState().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.state);

            if (generalSettingDataObj.getCountry().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.country);

            if (generalSettingDataObj.getPIN().equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.pin_code);*/

            if (schoolDetailsEntity.Country.equalsIgnoreCase("") ){
              /* String[] arrayStr  = generalSettingDataObj.getMobile().split("-");
                if (arrayStr.length>=2 && arrayStr[0].equalsIgnoreCase("+")){
                     mandatory_fields = mandatory_fields + " " + getString(R.string.country )+ " ,";
                }else */ mandatory_fields = mandatory_fields + " " + getString(R.string.country_code )+ " ,";
            }

            if (schoolDetailsEntity.Mobile.equalsIgnoreCase("-")
                    ||
                    schoolDetailsEntity.Mobile.length() < 8)
                mandatory_fields = mandatory_fields + " " + getString(R.string.phone_number )+ " ,";

            if (!schoolDetailsEntity.Email.contains("@")
                    ||
                    !schoolDetailsEntity.Email.contains("."))
                mandatory_fields = mandatory_fields + " " + getString(R.string.email_id);


            return false;
        } else
            return true;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragBasicDetail = new SchoolBasicDetailFragment();
        fragSchoolAddressDetails = new SchoolAddressFragment();


        adapter.addFragment(fragBasicDetail, "Basic Details");
        adapter.addFragment(fragSchoolAddressDetails, "Address Details");


        viewPager.setAdapter(adapter);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);


            switch (position) {
                case 1:

                    if (checkSchoolBasicDetailsMandatoryFiels()) {

                    } else {
                        AlertDialog.Builder alert_save_msg = new AlertDialog.Builder(SchoolDetailsActivity.this);
                        alert_save_msg.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int current = getItem(-1);

                                // move to Same screen
                                viewPager.setCurrentItem(current);
                                mandatory_fields = "";
                            }
                        });
                        alert_save_msg.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + "\n" + mandatory_fields);
                        alert_save_msg.show();
                    }

                    break;

                case 2:
                    if (checkSchoolAddresssMandatoryFiels()) {

                    } else {
                        AlertDialog.Builder alert_save_msg = new AlertDialog.Builder(SchoolDetailsActivity.this
                        );
                        alert_save_msg.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int current = getItem(-1);

                                // move to Same screen
                                viewPager.setCurrentItem(current);
                                mandatory_fields = "";
                            }
                        });
                        alert_save_msg.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + "\n" + mandatory_fields);
                        alert_save_msg.show();
                    }
                    break;

                case 3:

                    break;


            }


        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private boolean checkSchoolBasicDetailsMandatoryFiels() {


        if ( schoolDetailsEntity.SchoolName.equalsIgnoreCase("")
                ||
                // generalSettingDataObj.getSessionStartDate().equalsIgnoreCase("") ||
                // generalSettingDataObj.getSchoolMoto().equalsIgnoreCase("") ||
                // generalSettingDataObj.getBoard().equalsIgnoreCase("") ||
                // generalSettingDataObj.getAffiliatedBy().equalsIgnoreCase("") ||
                // generalSettingDataObj.getAffiliationNo().equalsIgnoreCase("") ||
                // generalSettingDataObj.getRegistrationNo().equalsIgnoreCase("") ||
                schoolDetailsEntity.DateOfEstablishment.equalsIgnoreCase("")
        ) {


//            if (generalSettingDataObj.getSessionStartDate().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.session_start_date) + " ,";

            if (schoolDetailsEntity.SchoolName.equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.school_name) + " ,";

//            if (generalSettingDataObj.getSchoolMoto().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.school_moto);
//
//            if (generalSettingDataObj.getBoard().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.school_board);
//
//            if (generalSettingDataObj.getAffiliatedBy().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.affiliated_by);
//
//            if (generalSettingDataObj.getAffiliationNo().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.affiliated_number);
//
//            if (generalSettingDataObj.getRegistrationNo().equalsIgnoreCase(""))
//                mandatory_fields = mandatory_fields + " " + getString(R.string.registration_number);

            if (schoolDetailsEntity.DateOfEstablishment.equalsIgnoreCase(""))
                mandatory_fields = mandatory_fields + " " + getString(R.string.date_of_stablishment) + " ,";


            return false;
        } else return true;

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void setBasicDetail(SchoolDetailsEntity obj) {


        schoolDetailsEntity.sessionStartDate= obj.sessionStartDate;
        schoolDetailsEntity.SchoolName = obj.SchoolName;
        schoolDetailsEntity.SchoolMoto = obj.SchoolMoto;
        schoolDetailsEntity.Board = obj.Board;
        schoolDetailsEntity.AffiliatedBy = obj.AffiliatedBy;
        schoolDetailsEntity.AffiliationNo = obj.AffiliationNo;
        schoolDetailsEntity.RegistrationNo = obj.RegistrationNo;
        schoolDetailsEntity.DateOfEstablishment = obj.DateOfEstablishment;

    }

    @Override
    public void setSchoolAddress(SchoolDetailsEntity obj) {

        schoolDetailsEntity.SchoolAddress = obj.SchoolAddress;
        schoolDetailsEntity.City = obj.City;
        schoolDetailsEntity.District = obj.District;
        schoolDetailsEntity.State = obj.State;
        schoolDetailsEntity.Country = obj.Country;
        schoolDetailsEntity.PIN = obj.PIN;
        schoolDetailsEntity.Mobile = obj.Mobile;
        schoolDetailsEntity.AlternateMobile = obj.AlternateMobile;
        schoolDetailsEntity.Landline = obj.Landline;
        schoolDetailsEntity.Email = obj.Email;
        schoolDetailsEntity.Fax = obj.Fax;

    }

    @Override
    public void setSchoolLogo(SchoolDetailsEntity obj) {

    }

    @Override
    public void callPrevious() {
        btnPrevious();
    }

    @Override
    public void callNext() {
        try {
            btnNext();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public int  getAppColor() {
        return colorIs;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);

            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private void sendSchoolData() throws JSONException, UnsupportedEncodingException {
        // progressbar.show();

        final JSONObject param = new JSONObject();

        if (schoolDetailsEntity.Mobile.trim().contains("-"))
        {
           String ar[]  = schoolDetailsEntity.Mobile.trim().split("-");
            sp.edit().putString("SchoolCountryCode",ar.length>=2?ar[0]:"91" )
                    .commit();
        }

        if (schoolDetailsEntity.Id.equalsIgnoreCase("")) {
            param.put("sessionStartDate", schoolDetailsEntity.sessionStartDate);
            // param.put("sessionStartDate", admin_registration_organization_key.getText().toString());
            param.put("SchoolName", schoolDetailsEntity.SchoolName.trim());
            param.put("SchoolMoto", schoolDetailsEntity.SchoolMoto.trim());
            //   param.put("Logo", "");
            param.put("SchoolAddress", schoolDetailsEntity.SchoolAddress.trim());

            param.put("City", schoolDetailsEntity.City.trim());
            param.put("District", schoolDetailsEntity.District.trim());
            param.put("PIN", schoolDetailsEntity.PIN.trim());
            param.put("State", schoolDetailsEntity.State.trim());
            param.put("Country", schoolDetailsEntity.Country.trim());
            param.put("Mobile", schoolDetailsEntity.Mobile.trim());
            param.put("AlternateMobile", schoolDetailsEntity.AlternateMobile.trim());
            param.put("Email", schoolDetailsEntity.Email.trim());

            param.put("Landline", schoolDetailsEntity.Landline.trim());
            param.put("Fax", schoolDetailsEntity.Fax.trim());
            param.put("DateOfEstablishment", schoolDetailsEntity.DateOfEstablishment.trim());
            param.put("Board", schoolDetailsEntity.Board.trim());
            param.put("AffiliatedBy", schoolDetailsEntity.AffiliatedBy.trim());
            param.put("RegistrationNo", schoolDetailsEntity.RegistrationNo.trim());
            param.put("AffiliationNo", schoolDetailsEntity.AffiliationNo.trim());

            param.put("facebookId", schoolDetailsEntity.facebookId==null?"": schoolDetailsEntity.facebookId);
            param.put("YouTubeId",  schoolDetailsEntity.YouTubeId==null?"": schoolDetailsEntity.YouTubeId);
            param.put("LinkdinId",  schoolDetailsEntity.LinkdinId==null?"": schoolDetailsEntity.LinkdinId);
//            param.put("gPlusId",  schoolDetailsEntity.gPlusId==null?"": schoolDetailsEntity.gPlusId);
            param.put("schoolDescription",
                    schoolDetailsEntity.schoolDescription==null?"": schoolDetailsEntity.schoolDescription.replaceAll(" ", "+"));
            param.put("schoolMapLocation",schoolDetailsEntity.schoolMapLocation==null?"": schoolDetailsEntity.schoolMapLocation );
            param.put("Terms","Accepted");
            sendGeneralSettingDataPost(param);
        } else {
            final Map<String, Object> paramMap = new LinkedHashMap<>();
            paramMap.put("sessionStartDate", schoolDetailsEntity.sessionStartDate);
            // paramMap.put("sessionStartDate", admin_registration_organization_key.getText().toString());
            paramMap.put("SchoolName", URLEncoder.encode(schoolDetailsEntity.SchoolName ,"utf-8"));
            paramMap.put("SchoolMoto", URLEncoder.encode(schoolDetailsEntity.SchoolMoto ,"utf-8"));

           /* if (generalSettingDataObj.getLogo().contains("/")) {
                String[] logoUrlArray = generalSettingDataObj.getLogo().split("/");
                paramMap.put("Logo", logoUrlArray[logoUrlArray.length - 1]);
            }
            if (generalSettingDataObj.getSchoolImage().contains("/")) {
                String[] logoUrlArray = generalSettingDataObj.getSchoolImage().split("/");
                paramMap.put("schoolImage", logoUrlArray[logoUrlArray.length - 1]);
            }*/
            //
            paramMap.put("SchoolAddress",URLEncoder.encode(schoolDetailsEntity.SchoolAddress ,"utf-8"));
            paramMap.put("City", URLEncoder.encode(schoolDetailsEntity.City ,"utf-8"));
            paramMap.put("District", URLEncoder.encode(schoolDetailsEntity.District ,"utf-8"));
            paramMap.put("PIN", schoolDetailsEntity.PIN );
            paramMap.put("State", URLEncoder.encode(schoolDetailsEntity.State ,"utf-8"));
            paramMap.put("Country",URLEncoder.encode(schoolDetailsEntity.Country ,"utf-8"));
            paramMap.put("Mobile", schoolDetailsEntity.Mobile.trim());
            paramMap.put("AlternateMobile", schoolDetailsEntity.AlternateMobile.trim());
            paramMap.put("Email", URLEncoder.encode(schoolDetailsEntity.Email ,"utf-8"));
            paramMap.put("Landline", schoolDetailsEntity.Landline.replaceAll(" ", "+"));
            paramMap.put("Fax",  schoolDetailsEntity.Fax!=null?schoolDetailsEntity.Fax:""  );
            paramMap.put("DateOfEstablishment", schoolDetailsEntity.DateOfEstablishment);
            paramMap.put("Board",URLEncoder.encode( schoolDetailsEntity.Board ,"utf-8"));
            paramMap.put("AffiliatedBy", URLEncoder.encode(schoolDetailsEntity.AffiliatedBy ,"utf-8"));
            paramMap.put("RegistrationNo", URLEncoder.encode(schoolDetailsEntity.RegistrationNo  ,"utf-8"));
            paramMap.put("AffiliationNo", URLEncoder.encode(schoolDetailsEntity.AffiliationNo ,"utf-8"));
          /*  paramMap.put("facebookId", generalSettingDataObj.getFacebookId()==null?"": generalSettingDataObj.getFacebookId());
            paramMap.put("YouTubeId",  generalSettingDataObj.getYouTubeId()==null?"": generalSettingDataObj.getYouTubeId());
            paramMap.put("LinkdinId",  generalSettingDataObj.getLinkdinId()==null?"": generalSettingDataObj.getLinkdinId());
            paramMap.put("gPlusId",  generalSettingDataObj.getgPlusId()==null?"": generalSettingDataObj.getgPlusId());
            paramMap.put("schoolDescription",
                    generalSettingDataObj.getSchoolDescription()==null?"": generalSettingDataObj.getSchoolDescription().replaceAll(" ", "+"));
            paramMap.put("schoolMapLocation",generalSettingDataObj.getSchoolMapLocation()==null?"": generalSettingDataObj.getSchoolMapLocation() );
           */ sendGeneralSettingDataUpdate(paramMap);
        }

    }

    private void sendGeneralSettingDataUpdate(final Map<String, Object> param) {
        progressbar.show();
        final JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Id",schoolDetailsEntity.Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        param.remove("logoUrl");


        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);

        Log.e("BasicDetailResult1", new JSONObject(param_main).toString());
       /* DbHandler.longInfo(sp.getString("HostName", "Not Found") + "GeneralSettingApi.php?" + "databaseName=" +
                sp.getString("organization_name", "") + "&data=" + (new JSONObject(param_main)));
*/

        String basicDetailUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "GeneralSettingApi.php?" + "databaseName=" +
                sp.getString("organization_name", "") + "&data=" + (new JSONObject(param_main));

        Log.d("basicDetailUrl", basicDetailUrl);
        StringRequest request1 = new StringRequest(Request.Method.PUT,
                basicDetailUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("BasicDetailResult1", s);
                        progressbar.cancel();
                        isDataUpdated = false;
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {

                                isDataUpdated =true;
                                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog
                                        .Builder(SchoolDetailsActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

                                alert1.setCancelable(false);

                                alert1.setMessage(jsonObject.optString("message"));
                                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent( );
                                        intent.putExtra("isDataUpdated",isDataUpdated);
                                        intent.putExtra("isDataInserted",isDataInserted);
                                        if (schoolDetailsEntity == null)
                                            setResult(RESULT_CANCELED, intent);
                                        else if (schoolDetailsEntity.Id.equalsIgnoreCase(""))
                                            setResult(RESULT_CANCELED, intent);
                                        else
                                            setResult(RESULT_OK, intent);

                                        finish();
                                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                    }
                                });
                                if (!isFinishing())
                                    alert1.show();

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);

                                    }
                                }).start();
                                HashMap<String,String> setDefaults = new HashMap<String, String>();
                                setDefaults.put(Gc.SCHOOLDETAILEXISTS, Gc.EXISTS);
                                Gc.setSharedPreference(setDefaults,SchoolDetailsActivity.this);

                                Intent intent = new Intent(SchoolDetailsActivity.this, SetupActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                                finish();
                                return;


                            }else if (jsonObject.optString("code").equalsIgnoreCase("502")){
                                alert.setMessage(R.string.maintanance_work_in_progress_please_visit_after_ten_min);
                                if (!isFinishing())
                                    alert.show();

                            } else {
                                alert.setMessage(jsonObject.optString("message"));
                                if (!isFinishing())
                                    alert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("BasicDetailResult", volleyError.toString());
                progressbar.cancel();
                isDataUpdated =false;
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //  params.put("data", new JSONObject(param).toString());
                //    Log.e("BasicDetailResult", new JSONObject(param).toString());
                return params;
            }
        };

        request1.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request1);
        //}
    }


    private void sendGeneralSettingDataPost(final JSONObject param) {

        progressbar.show();
        param.remove("logoUrl");

        String logoUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "GeneralSettingApi.php?" + "databaseName=" +
                sp.getString("organization_name", "");
        Log.d("LogoIrl", logoUrl);
        Log.d("param", param.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                logoUrl,param
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("BasicDetailResult", jsonObject.toString());
                        progressbar.cancel();
                        isDataInserted = false ;

                            if (jsonObject.optInt("code") == 201) {
                                isDataInserted =true;

                              /*  if (generalSettingDataObj.getLogoUrl() != null)
                                    if (!generalSettingDataObj.getLogoUrl().equalsIgnoreCase(""))
                                        uploadLogo(generalSettingDataObj.getLogoUrl());*/

                                android.app.AlertDialog.Builder alert1 = new android.app.AlertDialog
                                        .Builder(SchoolDetailsActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

                                alert1.setCancelable(false);

                                alert1.setMessage(jsonObject.optString("message"));
                                alert1.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        Intent intent = new Intent(SchoolDetailsActivity.this,
                                                SessionActivity.class);
                                        schoolDetailsEntity.Id = jsonObject.optInt("result") + "";

                                        intent.putExtra("generalSettingDataObj", (Serializable) schoolDetailsEntity);
                                        intent.putExtra("Id", jsonObject.optInt("result"));
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.enter, R.anim.nothing);
                                    }
                                });
                                if (!isFinishing())
                                    alert1.show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")){
                                alert.setMessage(R.string.maintanance_work_in_progress_please_visit_after_ten_min);
                                if (!isFinishing())
                                    alert.show();

                            }else {
                                alert.setMessage(jsonObject.optString("message"));
                                if (!isFinishing())
                                    alert.show();
                            }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("BasicDetailResult", volleyError.toString());
                progressbar.cancel();
                isDataInserted = false;
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) ;

        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }


    public void getStringToDate(String strdate, final String keyString) {

        if (!strdate.equalsIgnoreCase("")) {
            progressbar.show();
            String convertDate = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "Conversion.php?type=stringToDate&date=" +
                    strdate;

            Log.d("convertDate", convertDate);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, convertDate, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {
                    DbHandler.longInfo(s.toString());
                    Log.e("dateResult", s.toString());
                    progressbar.dismiss();
                    if (!isFinishing() && fragBasicDetail != null) {
                        if (keyString.equalsIgnoreCase("sessionStartDate")) {
                            fragBasicDetail.setSessionStartDate(s.optString("result"));
                        } else if (keyString.equalsIgnoreCase("dateOfStabilised")) {
                            fragBasicDetail.setDateOfStablished(s.optString("result") );
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateResult", volleyError.toString());
                    progressbar.dismiss();
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                        if (!isDialogShowing) {
                            alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                            alert.show();
                            isDialogShowing = true;
                        }
                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }
                }
            });
           /* StringRequest request = new StringRequest(Request.Method.GET,
                    convertDate
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {




                            DbHandler.longInfo(s);
                            Log.e("dateResult", s);
                            progressbar.dismiss();
                            if (!isFinishing() && fragBasicDetail != null) {
                                if (keyString.equalsIgnoreCase("sessionStartDate")) {
                                    fragBasicDetail.setSessionStartDate(s);
                                } else if (keyString.equalsIgnoreCase("dateOfStabilised")) {
                                    fragBasicDetail.setDateOfStablished(s);
                                }
                            }

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("dateResult", volleyError.toString());
                    progressbar.dismiss();
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                        if (!isDialogShowing) {
                            alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                            alert.show();
                            isDialogShowing = true;
                        }
                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };
*/

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        }

    }

    public void getDateToString(final String keyString, final String datestr) {

 /*       if (!Config.checkInternet(this)) {

            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();

        String dateToStr = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                datestr;
        Log.d("dateToStr", dateToStr);
        StringRequest request = new StringRequest(Request.Method.GET,
                dateToStr
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
                        progressbar.dismiss();
                        if (!isFinishing()) {
                            if (keyString.equalsIgnoreCase("dateOfStablishment")) {
                                schoolDetailsEntity.DateOfEstablishment = s;
                            } else if (keyString.equalsIgnoreCase("sessionStartDate")) {
                                schoolDetailsEntity.sessionStartDate = s;
                                if (keyString.equalsIgnoreCase("sessionStartDate"))
                                    fetchSession(s);
                            }
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }


    }

    private void fetchSession(String datestr) {
     /*   if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

        final Map<String, String> paramfilter = new LinkedHashMap<>();

        paramfilter.put("SessionStartDate", datestr);


        final Map<String, JSONObject> param = new LinkedHashMap<>();
        param.put("filter", new JSONObject(paramfilter));

//            Log.e("SessionDataUrlTEST", sp.getString("HostName", "Not Found") +
//                    "SchoolSessionApi.php?data=" +
//                    new JSONObject(param) +
//                    "&databaseName=" + sp.getString("organization_name", "") + "&action=getSession");


        String schoolSessionUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "SchoolSessionApi.php?data=" + new JSONObject(param) +
                "&databaseName=" + sp.getString("organization_name", "") + "&action=getSession";

        Log.d("schoolSessionUrl", schoolSessionUrl);
        StringRequest request = new StringRequest(Request.Method.GET,
                schoolSessionUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("SessionDataResTEST", s);

                        if (s != "") {

                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                        /*--  session List Saved in SharedPrefrence ---*/
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(Config.SESSION_DATA, s);
                                    editor.commit();
                                    SchoolSession schoolSessionObj = gson.fromJson(s,
                                            new TypeToken<SchoolSession>() {
                                            }.getType());
                                    if (schoolSessionObj != null) {

                                        ArrayList<SchoolSession> schoolSessionArrayList = schoolSessionObj.getSessionList();

                                        if (!isDialogShowing) {
                                            String currentSessionStartDate = "", currentSessionEndDate = "";

                                            for (SchoolSession schoolSession : schoolSessionArrayList) {
                                                if (schoolSessionObj.getCurrentSession().equalsIgnoreCase(schoolSession
                                                        .getSession().replaceAll("_", " "))) {
                                                    currentSessionStartDate = schoolSession.getSessionStartDate();
                                                    currentSessionEndDate = schoolSession.getSessionEndDate();
                                                    break;
                                                }
                                            }
                                            String currentSessionText = "<font color='#ce001f'>"
                                                    + schoolSessionObj.getCurrentSession().replaceAll("_", " ")
                                                    + "</font>";
                                            String sessionStartDateText = "<font color='#ce001f'>" +
                                                    currentSessionStartDate + "</font>";

                                            String sessionEndDateText = "<font color='#ce001f'>"
                                                    + currentSessionEndDate + "</font>";


                                            alert.setMessage(Html.fromHtml(getString(R.string.you_sesion_is) + " " +
                                                    currentSessionText + " " +
                                                    getString(R.string.and_its_starts_from) + " " + sessionStartDateText
                                                    + " " + getString(R.string.and_ends_on) + " " +
                                                    sessionEndDateText
                                            ));
                                            alert.show();
                                            isDialogShowing = true;
                                        }

                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("SessionDataErrTEST", volleyError.toString());
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }
}
