package in.junctiontech.school.exam.grades;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by JAYDEVI BHADE on 29-03-2017.
 */

public class CreateGradesFragment extends
        Fragment implements SearchView.OnQueryTextListener {
    private EditText et_class_name;
    private Button btn_add_class;
    private RecyclerView recycler_view_school_class_list;
    private GradesAdapter adapter;

    private boolean classNameAlreadyRegister = false;
    private LinearLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private ArrayList<MasterEntry> gradeListData = new ArrayList<>();
    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private RelativeLayout  page_demo;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_create_school_class, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));


                /*********************************    Demo Screen *******************************/
////                page_demo =(RelativeLayout)convertView.findViewById(R.id.rl_create_grades_demo);
//        if (sp.getBoolean("DemoCreateGrades",true)){
//            page_demo.setVisibility(View.VISIBLE);
//        }else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoCreateGrades",false).commit();
//                return false;
//            }
//        });
        /****************************************************************/
//        snackbar = (LinearLayout) convertView.findViewById(R.id.activity_create_class);
//        et_class_name = (EditText) convertView.findViewById(R.id.et_create_school_class_class_name);
//        btn_add_class = (Button) convertView.findViewById(R.id.btn_add_school_class);
        btn_add_class.setBackgroundColor(appColor);
        recycler_view_school_class_list = (RecyclerView) convertView.findViewById(R.id.recycler_view_school_class_list);
        et_class_name.setHint("");
//        ((TextInputLayout) convertView.findViewById(R.id.text_input_layout_write_name)).setHint(getString(R.string.write_grade_name));
//        ((TextView) convertView.findViewById(R.id.tv_header_list_name)).setText(getString(R.string.grade_list));

        et_class_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().equalsIgnoreCase(""))
                    checkGradeToServer(new MasterEntry("",""),
                            et_class_name.getText().toString().trim(), false/*not for save just check class name*/,
                            false/* not from adapter*/);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btn_add_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_class_name.getText().toString().trim().equalsIgnoreCase("")) {
                    et_class_name.setError(getString(R.string.name_can_not_be_blank));
                } else {
                    Config.hideKeyboard(getActivity());
                    if (!classNameAlreadyRegister) {

                        try {
                            addGradeToServer(
                                    et_class_name.getText().toString().trim(), false/* not first save firsly check class name*/);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // btn_add_class.setText(R.string.add);

                    } else {
//                       showDialougeWithMsg(
//                                getString(R.string.class_name_already_exist)
//                        );
                    }
                }
            }
        });
        setHasOptionsMenu(true);
        return convertView;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view_school_class_list.setLayoutManager(layoutManager);
        adapter = new GradesAdapter(getActivity(), gradeListData, CreateGradesFragment.this,appColor);
        recycler_view_school_class_list.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
        appColor=  Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setupRecycler();
        getGradeListFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");


                    if (gradeListData.size() == 0)
                        getGradeListFromServer();


                }
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public void updateList(ArrayList<MasterEntry> classListData) {

        this.gradeListData = classListData;
        this.adapter.updateList(gradeListData);


    }

    public void performClick() {
        btn_add_class.performClick();
    }

    public void setClassNameAlreadyRegister(boolean classNameAlreadyRegister) {
        this.classNameAlreadyRegister = classNameAlreadyRegister;
        if (classNameAlreadyRegister) {
            et_class_name.setError(getString(R.string.class_name_already_exist));
        } else {
            et_class_name.setError(null);

        }
    }

    public void clearClassText() {
        et_class_name.setText("");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter != null)
                            adapter.updateList(gradeListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<MasterEntry> filteredModelList = filter(gradeListData, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<MasterEntry> filter(ArrayList<MasterEntry> models, String query) {
        query = query.toLowerCase();
        final ArrayList<MasterEntry> filteredModelList = new ArrayList<>();

        for (MasterEntry model : models) {

            if (model.getMasterEntryValue().toLowerCase().contains(query)

                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void getGradeListFromServer() {
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("MasterEntryName", "Grade");

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param);

        Log.d("fetchGradeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getGradeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                gradeListData = new ArrayList<>();
                                gradeListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        getString(R.string.data_not_available),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                            adapter.updateList(gradeListData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    public void checkGradeToServer(final MasterEntry updateGradeObj ,final String gradeName, final boolean saveOrNot, final boolean isFromAdapter) {
        if (isFromAdapter)
            progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        try {
            param.put("MasterEntryValue",URLEncoder.encode(  gradeName/*gradeName.replaceAll("\\+", "%2B")*/   ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        param.put("MasterEntryName", "Grade");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("gradeCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("gradeCheck", s);
                        checkingKey = false;
                        if (progressbar.isShowing())
                            progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            et_class_name.setError(null);
                            if (jsonObject.optInt("code") == 200) {
                                isNameAlreadyExist = true;
                                if (progressbar.isShowing()) {
                                    progressbar.cancel();
                                }
                                if (isFromAdapter) {
                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            getString(R.string.grade_alresdy_exist),
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                } else
                                    et_class_name.setError(getString(R.string.grade_alresdy_exist));



                                /*if (updateClass) {
                                    *//* update class*//*
                                    if (saveOrNot)
                                        Toast.makeText(ClassSectionActivity.this,
                                                getString(R.string.class_name_already_exist), Toast.LENGTH_SHORT).show();
                                } else {
                                    *//* add class *//*
                                    fragmentCreateClass.setClassNameAlreadyRegister(true);
                                }


*/

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                isNameAlreadyExist = false;


                                if (isFromAdapter)
                                    updateGrade( updateGradeObj, gradeName);
                                else if (saveOrNot)
                                    addGradeToServer(gradeName, true);


/*
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GradeKey", volleyError.toString());
                if (progressbar.isShowing())
                    progressbar.cancel();
                checkingKey = false;
                isNameAlreadyExist = false;
                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");

    }

    public void updateGrade(MasterEntry updateGradeObj , final String gradeName){
        progressbar.show();

        final Map<String, String> param = new LinkedHashMap<String, String>();

        try {
            param.put("MasterEntryValue", URLEncoder.encode( gradeName/*.replaceAll("\\+", "%2B")*/,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("MasterEntryId", updateGradeObj.getMasterEntryId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String updateClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("MasterEntryApi", updateClassUrl);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateClassUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResultMaster", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                        getGradeListFromServer();

                                Toast.makeText(getActivity(),
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                               /* alert.setMessage(jsonObject.optString("message"));
                                alert.show();*/

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }

    public void addGradeToServer(String feeName, boolean saveOrNot) throws JSONException {
        progressbar.show();
        if (saveOrNot) {

            progressbar.show();
            if (!checkingKey) {
                if (isNameAlreadyExist) {
                    Snackbar snackbarObj = Snackbar.make(snackbar,
                            getString(R.string.grade_alresdy_exist),
                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                    snackbarObj.show();

                } else {
                    final JSONObject param = new JSONObject();

                    param.put("MasterEntryValue", feeName.trim().toUpperCase());
                    param.put("MasterEntryStatus", "Active");
                    param.put("MasterEntryName", "Grade");
                    param.put("Grade", "");
                    param.put("GradeRange", "");


                    String addClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "MasterEntryApi.php?databaseName=" + dbName;
                    Log.d("addGradeUrl", addClassUrl);
                    Log.d("param", param.toString());

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, addClassUrl,param   ,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    DbHandler.longInfo(jsonObject.toString());
                                    Log.e("ResultGradeFee", jsonObject.toString());
                                    progressbar.cancel();

                                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                            getGradeListFromServer();
                                            clearText();

                                            Toast.makeText(getActivity(),
                                                    jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !(getActivity()).isFinishing()) {
                                                Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                            Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                        else {
                                           /* alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();*/
                                            Snackbar snackbarObj = Snackbar.make(snackbar,
                                                    jsonObject.optString("message"),
                                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                }
                                            });
                                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                            snackbarObj.show();

                                        }


                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("ResultGrade", volleyError.toString());
                            Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

                            progressbar.cancel();
                        }
                    })  ;

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request.setTag(getTag());
                    AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

                }


            }

        } else {
            checkGradeToServer(new MasterEntry("",""),feeName, true, false);

        }

    }



    private void clearText() {
        et_class_name.setText("");
    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
