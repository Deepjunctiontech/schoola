package in.junctiontech.school.exam;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.models.StudentData;

public class FillMarksActivity extends AppCompatActivity {

    //   private String[] gradesarray, result, resultID;
    private Spinner spinner_studentname/*, spinner_grades, spinner_result*/;
    private String[] studentID, studentname;
    private int selectedStudentPosition;
    private ArrayList<String> arraylist_studentData/*, arraylist_result*/;
    private String selected_class_id, max_marks, examDate;
    private String selected_section, selected_examtype, selected_subject;
    private EditText et_selected_marks;
    //  private String selected_grade, selected_result, selected_marks;
    private List<StudentData> studentDataList;
    StudentData obj = null;
    Bundle b;
    private boolean inputNumberIsGreaterToMaxNumber = false;
    private DbHandler db;
    private SharedPreferences sharedPsreferences;
    private ArrayList<Grade> maxMarkListData = new ArrayList<>();
    private TextView tv_result, tv_grades;
    private int colorIs = 0;
    private TextView viewFilledResults;
    private CheckBox ck_student_absent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));


        setContentView(R.layout.activity_fill_marks);
        viewFilledResults = (TextView) findViewById(R.id.viewFilledResults);
        setColorApp();
        db = DbHandler.getInstance(this);
        getSupportActionBar().setTitle(R.string.exam_result_entry);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setSubtitle(/*getString(R.string.class_text) + " : "
                +*/ getIntent().getStringExtra("selected_class_idNAME") + " "
                + getIntent().getStringExtra("selected_sectionNAME") + " : "
                + getIntent().getStringExtra("selected_examtype"));

/*        AdView mAdView = (AdView) findViewById(R.id.adview_fillexam_entry);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (sharedPsreferences.getBoolean("addStatusGone",false))
            mAdView.setVisibility(View.GONE);*/

        //     gradesarray = getResources().getStringArray(R.array.grades);
        b = savedInstanceState;

        spinner_studentname = (Spinner) findViewById(R.id.spinner_studentname);
        et_selected_marks = (EditText) findViewById(R.id.et_selected_marks);
        ck_student_absent = (CheckBox) findViewById(R.id.ck_student_absent);
        ck_student_absent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                et_selected_marks.setEnabled(isChecked ? false : true);
            }
        });

        /*spinner_grades = (Spinner) findViewById(R.id.spinner_grades);
        spinner_result = (Spinner) findViewById(R.id.spinner_result);*/
        tv_result = (TextView) findViewById(R.id.tv_result);
        tv_grades = (TextView) findViewById(R.id.tv_grades);
//        if (b != null) {
//            selected_class_id = b.getString("selected_class_id");
//            selected_section = b.getString("selected_section");
//            selected_examtype = b.getString("selected_examtype");
//            selected_subject = b.getString("selected_subject");
//            max_marks = b.getString("max_marks");
//            examDate = b.getString("examDate");
//
//            resultID = b.getStringArray("resultID");
//            gradesarray = b.getStringArray("gradesarray");
//            result = b.getStringArray("result");
//            studentID = b.getStringArray("studentID");
//            studentname = b.getStringArray("studentname");
//            selectedStudentPosition = b.getInt("selectedStudentPosition");
//
//            ArrayAdapter arr1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, result);
//            spinner_result.setAdapter(arr1);
//
//            arraylist_studentData = b.getStringArrayList("arraylist_studentData");
//            ArrayAdapter arr = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arraylist_studentData);
//            spinner_studentname.setAdapter(arr);
//            spinner_studentname.setSelection(selectedStudentPosition);
//        } else {
        selected_class_id = getIntent().getStringExtra("selected_class_id");
        selected_section = getIntent().getStringExtra("selected_section");
        selected_examtype = getIntent().getStringExtra("selected_examtype");
        selected_subject = getIntent().getStringExtra("selected_subject");
        max_marks = getIntent().getStringExtra("et_maximum_marks");
        examDate = getIntent().getStringExtra("btn_currentdateExamResult");

       /* String[][] data_res = db.getResultList();
        resultID = data_res[0];
        result = data_res[1];

        ArrayAdapter arr1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, result);
        spinner_result.setAdapter(arr1);*/

        String[][] data = db.getStudentData(selected_class_id, selected_section, "");
        studentID = data[1];
        studentname = data[0];

        arraylist_studentData = new ArrayList<String>(studentID.length);
        for (int i = 0; i < studentID.length; i++)
            arraylist_studentData.add(i, studentname[i] + "\n" + getString(R.string.roll_no) + "( " + studentID[i] + ")");

        ArrayAdapter arr = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arraylist_studentData);
        spinner_studentname.setAdapter(arr);

        maxMarkListData = (ArrayList<Grade>) getIntent().getSerializableExtra("markObjList");
        if (getIntent().getBooleanExtra("back", false)) {

            spinner_studentname.setSelection(getIntent().getIntExtra("position", 0));
        } else {
            spinner_studentname.setSelection(0);
            if (!et_selected_marks.getText().toString().trim().equalsIgnoreCase(""))
                setResultAndGrade();

        }

        studentDataList = db.getExamResult(selected_examtype, selected_subject, studentID, examDate);
        spinner_studentname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ck_student_absent.setChecked(false);
                selectedStudentPosition = position;

                obj = studentDataList.get(selectedStudentPosition);

                obj.setStudentID(studentID[position]);
                obj.setStudentName(studentname[position]);

                et_selected_marks.setText(obj.getStudentMarks());
                String s = et_selected_marks.getText().toString().trim();
                if ((s + "").equalsIgnoreCase(""))
                    s = "0";

                if (max_marks.equalsIgnoreCase(""))
                    max_marks = "0";
                if (Double.parseDouble(max_marks) < Double.parseDouble(s + "")) {
                    et_selected_marks.setError(getString(R.string.should_not_be_greater_than_maximum_marks) + " i.e. " + max_marks);
                    inputNumberIsGreaterToMaxNumber = true;
                }
                if (!et_selected_marks.getText().toString().trim().equalsIgnoreCase(""))
                    setResultAndGrade();
             /*   if (obj.getStudentGrade().equalsIgnoreCase("")) {
                    obj.setStudentGrade(gradesarray[0]);
                    spinner_grades.setSelection(0);
                } else {
                    for (int i = 0; i < gradesarray.length; i++) {
                        if (gradesarray[i].equalsIgnoreCase(obj.getStudentGrade())) {
                            spinner_grades.setSelection(i);
                            obj.setStudentGrade(gradesarray[i]);
                            break;
                        }
                    }
                }


                if (obj.getStudentResult().equalsIgnoreCase("")) {
                    obj.setStudentResult(resultID[0]);
                    spinner_result.setSelection(0);
                } else {
                    for (int j = 0; j < result.length; j++) {
                        if (resultID[j].equalsIgnoreCase(obj.getStudentResult())) {
                            spinner_result.setSelection(j);
                            obj.setStudentResult(resultID[j]);
                            break;
                        }
                    }

                    // db.saveExamResult(selected_examtype, selected_class_id, selected_section, selected_subject, max_marks, examDate, obj);

                }*/

                et_selected_marks.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if ((s + "").equalsIgnoreCase(""))
                            s = "0";

                        if (max_marks.equalsIgnoreCase(""))
                            max_marks = "0";
                        if (Double.parseDouble(max_marks) < Double.parseDouble(s + "")) {
                            et_selected_marks.setError(getString(R.string.should_not_be_greater_than_maximum_marks) + " i.e. " + max_marks);
                            inputNumberIsGreaterToMaxNumber = true;
                        } else {
                            obj.setStudentMarks(s.toString());
                            inputNumberIsGreaterToMaxNumber = false;
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().trim().equalsIgnoreCase(""))
                            setResultAndGrade();
                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
      /*  spinner_grades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selected_grade = gradesarray[position];
                obj.setStudentGrade(gradesarray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_result.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selected_result = gradesarray[position];
                obj.setStudentResult(resultID[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
*/

        viewFilledResults.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        viewFilledResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFilledResults();
            }
        });
    }


    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_spinner_studentname)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_selected_marks)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_grades_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_result_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.viewFilledResults)).setTextColor(colorIs);


    }

    private void setResultAndGrade() {
        for (Grade gradeObj : maxMarkListData) {

            if ((Double.parseDouble(et_selected_marks.getText().toString().trim())
                    <=
                    Double.parseDouble(gradeObj.getRangeStart())
                    &&
                    Double.parseDouble(et_selected_marks.getText().toString().trim())
                            >=
                            Double.parseDouble(gradeObj.getRangeEnd()))
                    ||
                    (Double.parseDouble(et_selected_marks.getText().toString().trim())
                            >=
                            Double.parseDouble(gradeObj.getRangeStart())
                            &&
                            Double.parseDouble(et_selected_marks.getText().toString().trim())
                                    <=
                                    Double.parseDouble(gradeObj.getRangeEnd()))

                    ) {

                obj.setStudentGrade(gradeObj.getGradeValue());
                obj.setStudentResult(gradeObj.getGradeResult());
                tv_grades.setText(gradeObj.getGradeValue());
                tv_result.setText(gradeObj.getResultValue());

              /*  if (gradeObj.getGrade()*//*obj.getStudentGrade()*//*.equalsIgnoreCase("")) {
                    obj.setStudentGrade(gradesarray[0]);
                    spinner_grades.setSelection(0);
                } else {

                    for (int i = 0; i < gradesarray.length; i++) {
                        Log.e("gradesarray[i]", gradesarray[i]);
                        Log.e("gradeObj.getGrade()", gradeObj.getGradeValue());

                        if (gradesarray[i].equalsIgnoreCase(gradeObj.getGradeValue())) {
                            spinner_grades.setSelection(i);
                            obj.setStudentGrade(gradesarray[i]);
                            break;
                        }
                    }

                }*/
                 /*   if (gradeObj.getGradeResult()*//*obj.getStudentResult()*//*.equalsIgnoreCase("")) {
                        obj.setStudentResult(resultID[0]);
                        spinner_result.setSelection(0);
                    } else {
                        for (int j = 0; j < result.length; j++) {

                            Log.e("resultID[i]",resultID[j]);
                            Log.e("getGradeResult",gradeObj.getGradeResult());

                            if (resultID[j].equalsIgnoreCase(gradeObj.getGradeResult())) {
                                spinner_result.setSelection(j);
                                obj.setStudentResult(resultID[j]);
                                break;
                            }
                        }


                    }*/


                break;
            } else {
                Log.e("RITU", "FALSE");
                tv_grades.setText("");
                tv_result.setText("");
            }
        }
    }

    public void previousStudent(View v) {
        if (ck_student_absent.isChecked()) {
            if (selectedStudentPosition > 0)
                selectedStudentPosition = selectedStudentPosition - 1;
            else selectedStudentPosition = arraylist_studentData.size() - 1;


            for (int i = 0; i < arraylist_studentData.size(); i++) {
                if (arraylist_studentData.get(i).equalsIgnoreCase(arraylist_studentData.get(selectedStudentPosition))) {
                    spinner_studentname.setSelection(i);
                    break;
                }
            }
        } else if (obj.getStudentGrade().equalsIgnoreCase("") || obj.getStudentResult().equalsIgnoreCase("")) {
            showDialogForGradeMarkNotAvailable();
        } else if (!inputNumberIsGreaterToMaxNumber) {
            db.saveExamResult(selected_examtype, selected_class_id, selected_section, selected_subject, max_marks, examDate, obj);
            tv_grades.setText("");
            tv_result.setText("");
            ck_student_absent.setChecked(false);
            if (selectedStudentPosition > 0)
                selectedStudentPosition = selectedStudentPosition - 1;
            else selectedStudentPosition = arraylist_studentData.size() - 1;


            for (int i = 0; i < arraylist_studentData.size(); i++) {
                if (arraylist_studentData.get(i).equalsIgnoreCase(arraylist_studentData.get(selectedStudentPosition))) {
                    spinner_studentname.setSelection(i);
                    break;
                }
            }
        } else {
            // et_selected_marks.startAnimation(AnimationUtils.loadAnimation(this,R.anim.vibrate_shake));
        }
    }

    private void showDialogForGradeMarkNotAvailable() {
        AlertDialog.Builder alertAdmin = new AlertDialog.Builder(this);
        alertAdmin.setIcon(getResources().getDrawable(R.drawable.ic_hand_arrow));
        alertAdmin.setTitle(getString(R.string.confirmation));
        alertAdmin.setMessage("Grade and result are not available. First setup Grade & Marks or mark student as absent !");
        alertAdmin.setCancelable(false);
        alertAdmin.setPositiveButton(getString(R.string.absent), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ck_student_absent.setChecked(true);
            }
        });
        alertAdmin.setNegativeButton(getString(R.string.scholastic_grade_marks_setup), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(FillMarksActivity.this, AnotherActivity.class)
                        .putExtra("id", "GradesRange"));
                finish();
                overridePendingTransition(R.anim.enter, R.anim.nothing);
            }
        });
        alertAdmin.show();


    }

    public void nextStudent(View v) {
        if (ck_student_absent.isChecked()) {
            if (selectedStudentPosition + 1 == arraylist_studentData.size())
                viewFilledResults();
            else {
                if (selectedStudentPosition + 1 < arraylist_studentData.size())
                    selectedStudentPosition = selectedStudentPosition + 1;
                else selectedStudentPosition = 0;


                for (int i = 0; i < arraylist_studentData.size(); i++) {
                    if (arraylist_studentData.get(i).equalsIgnoreCase(arraylist_studentData.get(selectedStudentPosition))) {
                        spinner_studentname.setSelection(i);
                        break;
                    }
                }
            }
        } else if (obj.getStudentGrade().equalsIgnoreCase("") || obj.getStudentResult().equalsIgnoreCase("")) {
            showDialogForGradeMarkNotAvailable();
        } else if (!inputNumberIsGreaterToMaxNumber) {
            db.saveExamResult(selected_examtype, selected_class_id, selected_section, selected_subject, max_marks, examDate, obj);
            tv_grades.setText("");
            tv_result.setText("");
            ck_student_absent.setChecked(false);
            if (selectedStudentPosition + 1 == arraylist_studentData.size())
                viewFilledResults();
            else {
                if (selectedStudentPosition + 1 < arraylist_studentData.size())
                    selectedStudentPosition = selectedStudentPosition + 1;
                else selectedStudentPosition = 0;


                for (int i = 0; i < arraylist_studentData.size(); i++) {
                    if (arraylist_studentData.get(i).equalsIgnoreCase(arraylist_studentData.get(selectedStudentPosition))) {
                        spinner_studentname.setSelection(i);
                        break;
                    }
                }
            }
        }
        {
            // et_selected_marks.startAnimation(AnimationUtils.loadAnimation(this,R.anim.vibrate_shake));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {


        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }


    public void viewFilledResults() {

//        db.saveExamResult(selected_examtype, selected_class_id, selected_section, selected_subject, max_marks, examDate, obj);

        Intent intent = new Intent(this, MarksListActivity.class);
        intent.putExtra("report", false);
        intent.putExtra("subtitle", /*getString(R.string.class_text) + " : " + */getIntent().getStringExtra("selected_class_idNAME") + " "
                + getIntent().getStringExtra("selected_sectionNAME") + " : " + selected_examtype);

        intent.putExtra("selected_class_id", selected_class_id);
        intent.putExtra("selected_section", selected_section);
        intent.putExtra("selected_examtype", selected_examtype);
        intent.putExtra("selected_subject", selected_subject);
        intent.putExtra("examDate", examDate);
        intent.putExtra("et_maximum_marks", max_marks);
     /*   intent.putExtra("resultID", resultID);
        intent.putExtra("result", result);
*/
        intent.putExtra("selected_examtype", getIntent().getStringExtra("selected_examtype"));
        intent.putExtra("selected_class_id", getIntent().getStringExtra("selected_class_id"));
        intent.putExtra("selected_section", getIntent().getStringExtra("selected_section"));
        intent.putExtra("selected_class_idNAME", getIntent().getStringExtra("selected_class_idNAME"));
        intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
        intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
        intent.putExtra("selected_subject", getIntent().getStringExtra("selected_subject"));
        intent.putExtra("et_maximum_marks", getIntent().getStringExtra("et_maximum_marks"));
        intent.putExtra("btn_currentdateExamResult", getIntent().getStringExtra("btn_currentdateExamResult"));
        intent.putExtra("markObjList", maxMarkListData);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.nothing);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        db.saveExamResult(selected_examtype, selected_class_id, selected_section, selected_subject, max_marks, examDate, obj);
//
//        outState.putStringArray("resultID", resultID);
//        outState.putStringArray("result", result);
//        outState.putStringArray("gradesarray", gradesarray);
//        outState.putStringArray("studentID", studentID);
//        outState.putStringArray("studentname", studentname);
//        outState.putStringArrayList("arraylist_studentData", arraylist_studentData);
//        outState.putInt("selectedStudentPosition", selectedStudentPosition);
//
//        outState.putString("selected_class_id", selected_class_id);
//        outState.putString("selected_section", selected_section);
//        outState.putString("selected_examtype", selected_examtype);
//        outState.putString("selected_subject", selected_subject);
//        outState.putString("max_marks", max_marks);
//        outState.putString("examDate", examDate);
//
//
//    }
}
