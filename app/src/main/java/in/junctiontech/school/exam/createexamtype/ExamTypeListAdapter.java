package in.junctiontech.school.exam.createexamtype;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.ExamType;

/**
 * Created by JAYDEVI BHADE on 15-04-2017.
 */

public class ExamTypeListAdapter extends RecyclerView.Adapter<ExamTypeListAdapter.MyViewHolder> {
    private final CreateExamTypeFragment   fragment;
    private ArrayList<ExamType> examTypeList;
    private Context context;
    private int appColor;

    public ExamTypeListAdapter(Context context, ArrayList<ExamType> gradeList, CreateExamTypeFragment createGradesFragment, int appColor) {
        this.examTypeList = gradeList;
        this.context = context;
        this.fragment =createGradesFragment;
        this.appColor= appColor;
    }


    public void updateList(ArrayList<ExamType> gradeList){
        this.examTypeList = gradeList;
        notifyDataSetChanged();
    }

    @Override
    public ExamTypeListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final ExamTypeListAdapter.MyViewHolder holder, final int position) {
        final ExamType gradeObj = examTypeList.get(position);
        holder.tv_item_name.setText(gradeObj.getExam_Type().toUpperCase().replaceAll("_"," "));
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return examTypeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(appColor);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateExamType(examTypeList.get(getLayoutPosition()));
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    final String[] aa = new String[]{
                            context.getString(R.string.update),
                            context.getString(R.string.delete)
                    };
                    alert.setItems(aa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (which == 0) {
                                updateExamType(examTypeList.get(getLayoutPosition()));

                            } else  if (which == 1) {
                               fragment.deleteExamType(examTypeList.get(getLayoutPosition()));
                            }


                        }
                    });

                    alert.show();

                    return false;
                }
            });

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
        }
    }
    public void setFilter(ArrayList<ExamType> gradeList) {
        this.examTypeList = new ArrayList<>();
        this.examTypeList.addAll(gradeList);
        notifyDataSetChanged();
    }

    public void updateExamType(final ExamType examTypeObj){
        android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(context, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setIcon(context.getResources().getDrawable(R.drawable.ic_edit));
        alertUpdate.setTitle(context.getString(R.string.update) + " " +
                context.getString(R.string.exam_type));

        final EditText et_name = new EditText(context);
        et_name.setText(examTypeObj.getExam_Type().replaceAll("_"," "));

        LinearLayout linearLayout= new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        linearLayout.addView(et_name);

        linearLayout.setPadding(15,10,15,10);

        alertUpdate.setView(linearLayout);
        alertUpdate.setCancelable(false);

        alertUpdate.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_name.getText().toString().trim().length() == 0) {
                    Toast.makeText(context, context.getString(R.string.name_can_not_be_blank), Toast.LENGTH_SHORT).show();
                } else {
                    fragment.checkExamTypeToServer(examTypeObj ,et_name.getText().toString().trim(),true, true/* its comes from adapter */);

                }

            }
        });
        alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertUpdate.show();
    }

}
