package in.junctiontech.school.exam.examlist;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE 05-04-2017.
 */

public class ExamData implements Serializable {
String examName , sectionId,subjectId, classSectionName , subjectName , examDate, maxMarks ,classId;
    public ExamData(String   examName, String section_id, String classSectionName, String subjectId,
                    String subjectName, String examDate ,String  maxMarks,String  classId) {
        this.examName =examName;
        this.sectionId =section_id;
        this.classSectionName =classSectionName;
        this.subjectId =subjectId;
        this.subjectName =subjectName;
        this.examDate =examDate;
        this.maxMarks =maxMarks;
        this.classId =classId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(String maxMarks) {
        this.maxMarks = maxMarks;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getClassSectionName() {
        return classSectionName;
    }

    public void setClassSectionName(String classSectionName) {
        this.classSectionName = classSectionName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getExamDate() {
        return examDate;
    }

    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }
}
