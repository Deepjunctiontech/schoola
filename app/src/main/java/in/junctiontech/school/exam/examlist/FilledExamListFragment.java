package in.junctiontech.school.exam.examlist;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.BuildConfig;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.exam.ExamResultEntry;
import in.junctiontech.school.exam.FillMarksActivity;
import in.junctiontech.school.exam.Grade;

/**
 * Created by JAYDEVI BHADE on 03-04-2017.
 */

public class FilledExamListFragment extends
        Fragment implements SearchView.OnQueryTextListener {

    private FloatingActionButton btn_add_class;
    private RecyclerView recycler_view_school_class_list;
    private ExamListAdapter adapter;

    private boolean classNameAlreadyRegister = false;
    private RelativeLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private ArrayList<ExamData> examResultListData = new ArrayList<>();
    //private ArrayList<Grade> maxMarkListData = new ArrayList<>();
    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private DbHandler db;

    private ArrayList<Grade> gradeListData = new ArrayList<>();
    private int  appColor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_list_and_one_button, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        snackbar = (RelativeLayout) convertView.findViewById(R.id.ll_fragment_list_and_one_button);
        //  et_class_name = (EditText) convertView.findViewById(R.id.et_create_school_class_class_name);
        FloatingActionButton fab_fragment_add   =(FloatingActionButton) convertView.findViewById(R.id.fab_fragment_add);
        fab_fragment_add.setBackgroundTintList( ColorStateList.valueOf(appColor));
        fab_fragment_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), ExamResultEntry.class), 111);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
                getActivity().finish();
            }
        });
        recycler_view_school_class_list = (RecyclerView) convertView.findViewById(R.id.rv_fragment_list);

        setHasOptionsMenu(true);
        return convertView;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view_school_class_list.setLayoutManager(layoutManager);
        adapter = new ExamListAdapter(getActivity(), examResultListData, FilledExamListFragment.this,appColor);
        recycler_view_school_class_list.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
        super.onCreate(savedInstanceState);
        db = DbHandler.getInstance(getActivity());
        sp = Prefs.with(getActivity()).getSharedPreferences();
        progressbar = new ProgressDialog(getActivity());
        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        appColor =  Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupRecycler();
        getGradeMarksListFromServer();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (gradeListData.size() == 0)
                        getGradeMarksListFromServer();

                }
            }
        };

        fetchExamAlreadyFilledDataFromServer();
    }

    private void fetchExamAlreadyFilledDataFromServer() {
        progressbar.show();

        String db_name = sp.getString("organization_name", "Not Found");
        final Map<String, Object> param = new LinkedHashMap<>();
        param.put("session", sp.getString("session", ""));
        param.put("Exam_Detail_Status", "Active");


        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found") +
                "ExamApi.php?databaseName=" + db_name;

        Log.e("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
               /* sharedPreferences.getString("HostName", "Not Found") +
                        "reportview.php"*/
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("ResultResponse", s);

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.optString("code").equalsIgnoreCase("200")||
                            jsonObject.optString("code").equalsIgnoreCase("201")) {
                        final PreviousResultData obj = gson.fromJson(s, new TypeToken<PreviousResultData>() {
                        }.getType());
                        ArrayList<PreviousResultData> resultData = obj.getResultData() ;

                      //  ArrayList<ArrayList<PreviousResultData>> parts = chopped(resultData, 1000);

                        db.savePreviousExamResultDataFromServer(resultData);

                        Log.e("RRRR",resultData.size()+ " ttt");

                      /*  for (final ArrayList<PreviousResultData> partList : parts){

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("RRRR",partList.size()+ " ttt");

                                    db.savePreviousExamResultDataFromServer(partList);

                                }
                            }, 200);


                        }
*/





                       // db.savePreviousExamResultDataFromServer(obj.getResultData());

                    }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                            ||
                            jsonObject.optString("code").equalsIgnoreCase("511")) {
                        if (!logoutAlert & !(getActivity()).isFinishing()) {
                            Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                            logoutAlert = true;
                        }
                    }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                        Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                    else {
                        showSnack(jsonObject.optString("message"));

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
               refreshList();
                progressbar.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();


                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    // chops a list into non-view sublists of length L
    static <T> ArrayList<ArrayList<T>> chopped(ArrayList<T> list, final int L) {
        ArrayList<ArrayList<T>> parts = new ArrayList<ArrayList<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }


    private void refreshList() {
        examResultListData = db.getFilledExamResultListFromDatabase();
        adapter.updateList(examResultListData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter != null)
                            adapter.updateList(examResultListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<ExamData> filteredModelList = filter(examResultListData, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<ExamData> filter(ArrayList<ExamData> models, String query) {
        query = query.toLowerCase();
        final ArrayList<ExamData> filteredModelList = new ArrayList<>();

        for (ExamData model : models) {

            if (model.getClassSectionName().toLowerCase().contains(query)
                    ||
                    model.getExamName().toLowerCase().contains(query)
                    ||
                    model.getExamDate().toLowerCase().contains(query)
                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void openResultList(ExamData examData) {
        ArrayList<Grade> maxMarkListData = new ArrayList<>();
        for (Grade gradeObj : gradeListData) {
            if (gradeObj.getMaxMarks().equalsIgnoreCase(examData.getMaxMarks())) {
                maxMarkListData.add(gradeObj);
            }
        }


        if (maxMarkListData.size() > 0) {
            Intent intent = new Intent(getActivity(), FillMarksActivity.class);

            intent.putExtra("selected_class_id", examData.getClassId());
            intent.putExtra("selected_section", examData.getSectionId());
            intent.putExtra("selected_class_idNAME", examData.getClassSectionName());
            intent.putExtra("selected_sectionNAME", "");
            intent.putExtra("selected_examtype", examData.getExamName());
            intent.putExtra("selected_subject", examData.getSubjectId());
            intent.putExtra("et_maximum_marks",/* et_maximum_marks.getText().toString()*/
                    examData.getMaxMarks());
            intent.putExtra("btn_currentdateExamResult", examData.getExamDate());
            intent.putExtra("back", false);
            intent.putExtra("markObjList", maxMarkListData);

            String[][] data = db.getStudentData(examData.getClassId(), examData.getSectionId(), "");
            String[] studentID = data[1];


            if (studentID.length == 0) {
                showSnack(getString(R.string.students_not_available));
            } else {
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);

            }


            /*}*/
        } else {
            showSnack(getString(R.string.maximum_marks_list_not_available));

        }
    }

    public void showSnack(String msg) {
        Snackbar snackbarObj = Snackbar.make(snackbar,
                msg,
                Snackbar.LENGTH_LONG).setAction(getString(R.string.retry), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();
    }

    public void getGradeMarksListFromServer() {
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();


        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?databaseName=" + sp.getString("organization_name", "");
        ;

        Log.d("fetchGradeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getGradeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                Grade obj = (new Gson()).fromJson(s, new TypeToken<Grade>() {
                                }.getType());
                                gradeListData = new ArrayList<>();
                                gradeListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                showSnack(getString(R.string.maximum_marks_list_not_available));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag("exam");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
