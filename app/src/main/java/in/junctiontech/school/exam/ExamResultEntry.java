package in.junctiontech.school.exam;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.exam.examlist.PreviousResultData;
import in.junctiontech.school.managefee.AnotherActivity;

public class ExamResultEntry extends AppCompatActivity {
    private ArrayList<Grade> gradeListData = new ArrayList<>();
    public LayoutInflater inflater;
    private Spinner spinner_examresult_class, spinner_examresult_section, spinner_examresult_examtype, spinner_examresult_subject;

    private String[] class_id, className, section_id, section_name, exam_name, exam_subject_id, exam_subject_name;

    private int selected_class_id = 0, selected_section = 0, selected_examtype = 0, selected_subject = 0;
    //   private String selected_examtype = "", selected_subject = "";
    // private String maxMarks;
    // private EditText et_maximum_marks;
    private Calendar calendar;
    private int year, month, day;
    private Button btn_currentdateExamResult;
    private SharedPreferences sharedPreferences;
    // private AlertDialog.Builder alert;
    private String msg = "", maxMark = "";
    private DbHandler db;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private LinearLayout snackbar;
    private ArrayList<Grade> maxMarkListData = new ArrayList<>();
    private Spinner sp_exam_result_max_marks_list;
    private ArrayList<String> maxArray = new ArrayList<>();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int  colorIs=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
      //  LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));
        setContentView(R.layout.activity_exam_result_entry);
        getSupportActionBar().setTitle(R.string.exam_results);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        db = DbHandler.getInstance(this);
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));
        snackbar = (LinearLayout) findViewById(R.id.ll_exam_result_entry);


       /* AdView mAdView = (AdView) findViewById(R.id.adview_exam_entry);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        if (sharedPreferences.getBoolean("addStatusGone", false))
            mAdView.setVisibility(View.GONE);*/
/*

        alert = new AlertDialog.Builder(this);
        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

       */
/* alert.setNegativeButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.getDataFromServer();
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });*//*

        alert.setCancelable(false);
*/

        calendar = Calendar.getInstance();
       /* et_maximum_marks = (EditText) findViewById(R.id.et_maximum_marks);*/
        btn_currentdateExamResult = (Button) findViewById(R.id.btn_currentdateExamResult);
        spinner_examresult_class = (Spinner) findViewById(R.id.examresult_class);
        spinner_examresult_section = (Spinner) findViewById(R.id.examresult_section);
        spinner_examresult_examtype = (Spinner) findViewById(R.id.examresult_examtype);
        spinner_examresult_subject = (Spinner) findViewById(R.id.examresult_subject);
        sp_exam_result_max_marks_list = (Spinner) findViewById(R.id.sp_exam_result_max_marks_list);

        getGradeMarksListFromServer();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (maxArray.size() == 0)
                        getGradeMarksListFromServer();

                }
            }
        };
        String[][] data = db.getClassName();
        class_id = data[0];
        className = data[1];
        selected_class_id = 0;

        exam_name = db.getExamType();

        if (class_id.length == 0)
            showSnack(getString(R.string.classes_not_available));
        else if (exam_name.length == 0)
            showSnack(getString(R.string.exams_not_available));

        ArrayAdapter<String> arr = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, className);
      arr .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_examresult_class.setAdapter(arr);

        int lastUsedClassIdPos = 0;
        int jj = 0;
        for (String classID : class_id) {
            if (sharedPreferences.getString(Config.LAST_USED_CLASS_ID, "").
                    equalsIgnoreCase(classID)) {
                lastUsedClassIdPos = jj;

            }
            jj++;
        }
        if (class_id.length > 0)
            spinner_examresult_class.setSelection(lastUsedClassIdPos);


//        selected_class_id = sharedPreferences.getInt("defaultClass", 0);
//        spinner_examresult_class.setSelection(selected_class_id);

        ArrayAdapter<String> arr2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, exam_name);
       arr2 .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_examresult_examtype.setAdapter(arr2);
//        }
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        String new_day = day + "";
        if (day < 10)
            new_day = "0" + day;

        String new_month = month + 1 + "";
        if ((month + 1) < 10)
            new_month = "0" + (month + 1);

        btn_currentdateExamResult.setText(year + "-" + new_month + "-" + new_day);

        spinner_examresult_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_class_id = position;

                String[][] data = db.getSectionName(class_id[position]);
                section_id = data[0];
                section_name = data[1];

                if (section_id.length == 0) showSnack(getString(R.string.sections_not_available));

                ArrayAdapter<String> arr_sec = new ArrayAdapter<String>(ExamResultEntry.this,
                        android.R.layout.simple_list_item_1, section_name);
                arr_sec .setDropDownViewResource(R.layout.myspinner_dropdown_item);
                spinner_examresult_section.setAdapter(arr_sec);

                int lastUsedSecIdPos = 0;
                int jj = 0;
                for (String secID : section_id) {
                    if (sharedPreferences.getString(Config.LAST_USED_SECTION_ID, "").
                            equalsIgnoreCase(secID)) {
                        lastUsedSecIdPos = jj;
                    }
                    jj++;
                }
                if ( class_id.length > 0 && class_id.length>=spinner_examresult_class.getSelectedItemPosition()
                        && section_id.length > 0 && section_id.length>=spinner_examresult_section.getSelectedItemPosition()) {
                    spinner_examresult_section.setSelection(lastUsedSecIdPos);

                    sharedPreferences.edit().
                            putString(Config.LAST_USED_SECTION_ID, section_id[spinner_examresult_section.getSelectedItemPosition()])
                            .putString(Config.LAST_USED_CLASS_ID, class_id[spinner_examresult_class.getSelectedItemPosition()])
                            .commit();
                }
                if (exam_name!=null && exam_name.length > 0 &&
                        exam_subject_id!=null && exam_subject_id.length > 0
                        && class_id!=null && class_id.length > 0 &&
                        section_id!=null && section_id.length > 0){
                   /* et_maximum_marks.setText(db.getMaximumMarks(exam_name[selected_examtype],
                            exam_subject_id[selected_subject],
                            class_id[selected_class_id], section_id[selected_section]));
*/
                    setMaxMarkSpinner(db.getMaximumMarks(exam_name[spinner_examresult_examtype.getSelectedItemPosition()],
                            exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()],
                            class_id[spinner_examresult_class.getSelectedItemPosition()],
                            section_id[spinner_examresult_section.getSelectedItemPosition()],
                            btn_currentdateExamResult.getText().toString()));

                }
                //   }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_examresult_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_section = position;
                String[][] data = db.getSubjectsClasswise(class_id[selected_class_id], section_id[position]);
                exam_subject_id = data[0];
                exam_subject_name = data[1];
                if ( class_id.length > 0 && class_id.length>=spinner_examresult_class.getSelectedItemPosition()
                && section_id.length > 0 && section_id.length>=spinner_examresult_section.getSelectedItemPosition()) {
                    sharedPreferences.edit().
                            putString(Config.LAST_USED_SECTION_ID, section_id[spinner_examresult_section.getSelectedItemPosition()])
                            .putString(Config.LAST_USED_CLASS_ID, class_id[spinner_examresult_class.getSelectedItemPosition()])
                            .commit();
                }
                ArrayAdapter<String> arr_sub = new ArrayAdapter<String>(ExamResultEntry.this,
                        android.R.layout.simple_list_item_1, exam_subject_name);
               arr_sub .setDropDownViewResource(R.layout.myspinner_dropdown_item);
                spinner_examresult_subject.setAdapter(arr_sub);
                if (exam_subject_id.length == 0) {
                    showSnack(getString(R.string.subjects_not_available));

                } else {



                    int lastUsedSubIdPos = 0;
                    int jj = 0;
                    for (String subID : exam_subject_id) {
                        if (sharedPreferences.getString(Config.LAST_USED_SUBJECT_ID, "").
                                equalsIgnoreCase(subID)) {
                            lastUsedSubIdPos = jj;
                        }
                        jj++;
                    }
                    if (exam_subject_id.length>0)
                        spinner_examresult_subject.setSelection(lastUsedSubIdPos);
                    if (exam_name!=null && exam_name.length > 0 &&
                            exam_subject_id!=null && exam_subject_id.length > 0
                            && class_id!=null && class_id.length > 0 &&
                            section_id!=null && section_id.length > 0){
                       /* et_maximum_marks.setText(db.getMaximumMarks(exam_name[selected_examtype],
                                exam_subject_id[selected_subject],
                                class_id[selected_class_id], section_id[selected_section]));*/

                        setMaxMarkSpinner(db.getMaximumMarks(exam_name[spinner_examresult_examtype.getSelectedItemPosition()],
                                exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()],
                                class_id[spinner_examresult_class.getSelectedItemPosition()],
                                section_id[spinner_examresult_section.getSelectedItemPosition()],
                                btn_currentdateExamResult.getText().toString()));

                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_examresult_subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_subject = position;


                if (exam_subject_id.length>0 && exam_subject_id.length>=spinner_examresult_subject.getSelectedItemPosition())
                    sharedPreferences.edit().
                            putString(Config.LAST_USED_SUBJECT_ID, exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()])
                            .commit();
                if (exam_name!=null && exam_name.length > 0 &&
                        exam_subject_id!=null && exam_subject_id.length > 0
                        && class_id!=null && class_id.length > 0 &&
                        section_id!=null && section_id.length > 0)
               /* et_maximum_marks.setText(db.getMaximumMarks(exam_name[selected_examtype],
                        exam_subject_id[selected_subject],
                        class_id[selected_class_id], section_id[selected_section]));*/



                    setMaxMarkSpinner(db.getMaximumMarks(exam_name[spinner_examresult_examtype.getSelectedItemPosition()],
                            exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()],
                            class_id[spinner_examresult_class.getSelectedItemPosition()],
                            section_id[spinner_examresult_section.getSelectedItemPosition()],
                            btn_currentdateExamResult.getText().toString()));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_examresult_examtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_examtype = position;

                if (class_id.length != 0) {

                    if (exam_name!=null && exam_name.length > 0 &&
                            exam_subject_id!=null && exam_subject_id.length > 0
                            && class_id!=null && class_id.length > 0 &&
                            section_id!=null && section_id.length > 0){
                       /* et_maximum_marks.setText(db.getMaximumMarks(exam_name[selected_examtype],
                                exam_subject_id[selected_subject],
                                class_id[selected_class_id], section_id[selected_section]));*/
                        setMaxMarkSpinner(db.getMaximumMarks(exam_name[spinner_examresult_examtype.getSelectedItemPosition()],
                                exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()],
                                class_id[spinner_examresult_class.getSelectedItemPosition()],
                                section_id[spinner_examresult_section.getSelectedItemPosition()],
                                btn_currentdateExamResult.getText().toString()));
                    }

                } else showSnack(getString(R.string.classes_not_available));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_exam_result_max_marks_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!maxMark.equalsIgnoreCase("") && !maxMark.equalsIgnoreCase(sp_exam_result_max_marks_list.getSelectedItem().toString())) {
                    AlertDialog.Builder alertInfo = new AlertDialog.Builder(ExamResultEntry.this);
                    alertInfo.setMessage(getString(R.string.you_already_filled_result_of_some_student_if_you_change_maximum_marks_then_result_of_those_students_will_be_updated));
                    alertInfo.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    alertInfo.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setMaxMarkSpinner(maxMark);
                        }
                    });
                    alertInfo.setCancelable(false);
                    alertInfo.show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setMaxMarkSpinner(String maximumMarks) {
        maxMark = maximumMarks;
        Log.e("MaxMark", maximumMarks + " ritu");
        int i = 0;
        if (maximumMarks != null && !maximumMarks.equalsIgnoreCase(""))
            for (String maxMark : maxArray) {
                if (maximumMarks.equalsIgnoreCase(maxMark)) {
                    sp_exam_result_max_marks_list.setSelection(i);
                    break;
                }
                i++;
            }

        if (maximumMarks.equalsIgnoreCase("") && maxArray.size() > 0)
            sp_exam_result_max_marks_list.setSelection(0);
    }

    public void selectExamDate(View v) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            String sessionStartDate = sharedPreferences.getString("sessionStartDate", "");
            String sessionEndDate = sharedPreferences.getString("sessionEndDate", "");
            String[] sessionStartDate_arr = sessionStartDate.split("-");
            String[] sessionEndDate_arr = sessionEndDate.split("-");

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);

            if (sessionEndDate_arr.length == 3) {
                if (sessionEndDate_arr[0].length() == 4) {
                    calendar.set(Integer.parseInt(sessionEndDate_arr[0]),
                            Integer.parseInt(sessionEndDate_arr[1]) - 1, Integer.parseInt(sessionEndDate_arr[2]));
                } else if (sessionEndDate_arr[2].length() == 4) {
                    calendar.set(Integer.parseInt(sessionEndDate_arr[2]),
                            Integer.parseInt(sessionEndDate_arr[1]) - 1, Integer.parseInt(sessionEndDate_arr[0]));
                }
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            }
            if (sessionStartDate_arr.length == 3) {
                if (sessionStartDate_arr[0].length() == 4) {
                    calendar.set(Integer.parseInt(sessionStartDate_arr[0]), Integer.parseInt(sessionStartDate_arr[1]) - 1, Integer.parseInt(sessionStartDate_arr[2]));
                } else if (sessionStartDate_arr[2].length() == 4) {
                    calendar.set(Integer.parseInt(sessionStartDate_arr[2]), Integer.parseInt(sessionStartDate_arr[1]) - 1, Integer.parseInt(sessionStartDate_arr[0]));
                }
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

            }

            return datePickerDialog;
//            return newtext DatePickerDialog(this,R.style.DialogTheme, myDateListener, year, month, day);
        }
        return null;
    }

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView)findViewById(R.id.tv_examresult_class)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_examresult_section)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_examresult_examtype)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_examresult_subject)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_currentdateExamResult)).setTextColor(colorIs);
         ((TextView)findViewById(R.id.tv_exam_result_max_marks_list)).setTextColor(colorIs);
         ((Button)findViewById(R.id.btn_next_exam_result_entry)).setBackgroundColor(colorIs);

    }

    public void showSnack(String msg) {
        Snackbar snackbarObj = Snackbar.make(snackbar,
                msg,
                Snackbar.LENGTH_LONG).setAction(R.string.reload_school_data, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db.getDataFromServer();
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

            }
        });
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();
    }

    public DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_currentdateExamResult.setText(year + "-" + new_month + "-" + new_day);
            if (exam_name!=null && exam_name.length > 0 &&
                    exam_subject_id!=null && exam_subject_id.length > 0
                    && class_id!=null && class_id.length > 0 &&
                    section_id!=null && section_id.length > 0)
                setMaxMarkSpinner(db.getMaximumMarks(exam_name[spinner_examresult_examtype.getSelectedItemPosition()],
                        exam_subject_id[spinner_examresult_subject.getSelectedItemPosition()],
                        class_id[spinner_examresult_class.getSelectedItemPosition()],
                        section_id[spinner_examresult_section.getSelectedItemPosition()],
                        btn_currentdateExamResult.getText().toString()));
        }
    };


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    public void fillExamResult(View v) {
        maxMarkListData = new ArrayList<>();
        for (Grade gradeObj : gradeListData) {
            if (gradeObj.maxMarks.equalsIgnoreCase(sp_exam_result_max_marks_list.getSelectedItem().toString())) {
                maxMarkListData.add(gradeObj);
            }
        }

        if (class_id.length > 0 && section_id.length > 0 && exam_name.length > 0 && exam_subject_id.length > 0
                && maxMarkListData.size() > 0) {
            Intent intent = new Intent(this, FillMarksActivity.class);
            intent.putExtra("selected_class_id", class_id[selected_class_id]);
            intent.putExtra("selected_section", section_id[selected_section]);
            intent.putExtra("selected_class_idNAME", className[selected_class_id]);
            intent.putExtra("selected_sectionNAME", section_name[selected_section]);
            intent.putExtra("selected_examtype", exam_name[selected_examtype]);
            intent.putExtra("selected_subject", exam_subject_id[selected_subject]);
            intent.putExtra("et_maximum_marks",/* et_maximum_marks.getText().toString()*/
                    sp_exam_result_max_marks_list.getSelectedItem().toString());
            intent.putExtra("btn_currentdateExamResult", btn_currentdateExamResult.getText().toString());
            intent.putExtra("back", false);
            intent.putExtra("markObjList", maxMarkListData);

           /* if (et_maximum_marks.getText().toString().equalsIgnoreCase(""))
                et_maximum_marks.setError(getString(R.string.enter_maximum_marks));
            else {*/
            String[][] data = db.getStudentData(class_id[selected_class_id], section_id[selected_section], "");
            String[] studentID = data[1];


            if (studentID.length == 0) {
                showSnack(getString(R.string.students_not_available));
            } else {
                fetchExamAlreadyFilledDataFromServer(intent);

            }


            /*}*/
        } else if (sharedPreferences.getString("user_type", "Not Found").equalsIgnoreCase("Administrator")
                ||
                sharedPreferences.getString("user_type", "").equalsIgnoreCase("admin"))showSnack();
        else{
            if (class_id.length == 0)
                showSnack(getString(R.string.classes_not_available));
            else if (section_id.length == 0)
                showSnack(getString(R.string.sections_not_available));
            else if (exam_name.length == 0)
                showSnack(getString(R.string.exams_not_available));
            else if (exam_subject_id.length == 0)
                showSnack(getString(R.string.subjects_not_available));
            else if (maxMarkListData.size() == 0) {

                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.maximum_marks_list_not_available),
                        Snackbar.LENGTH_LONG).setAction(getString(R.string.scholastic_grade_marks_setup),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!sharedPreferences.getBoolean(Config.GRADE_MARK_SETUP_PERMISSION, false)) {
                                    Toast.makeText(ExamResultEntry.this, "Permission not available : GRADE MARK SETUP !", Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(ExamResultEntry.this, AnotherActivity.class).putExtra("id", "GradesRange"));
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.nothing);}
                            }
                        });
                snackbarObj.setDuration(5000);
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                snackbarObj.show();
            }
        }
    }

    private void  showSnack() {
        Snackbar snackbarObj = null;
        if (class_id.length == 0 || section_id.length== 0 ) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    class_id.length == 0? getString(R.string.classes_not_available):getString(R.string.sections_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sharedPreferences.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                Toast.makeText(ExamResultEntry.this,"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                            }else {
                            startActivity(new Intent(ExamResultEntry.this, ClassSectionActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });

        } else if (exam_name.length == 0) snackbarObj = Snackbar.make(
                snackbar,
                getString(R.string.students_not_available),
                Snackbar.LENGTH_LONG).setAction(getString(R.string.exam_type),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!sharedPreferences.getBoolean(Config.CREATE_EXAM_TYPE_PERMISSION, false)) {
                            Toast.makeText(ExamResultEntry.this, "Permission not available : CREATE EXAM TYPE !", Toast.LENGTH_SHORT).show();
                        } else {   startActivity(new Intent(ExamResultEntry.this, AnotherActivity.class).putExtra("id", "examType"));
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.nothing);}
                    }
                });
        else if (exam_subject_id.length == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.subjects_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_subject),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sharedPreferences.getBoolean(Config.CREATE_SUBJECT_PERMISSION, false)) {
                                Toast.makeText(ExamResultEntry.this, "Permission not available : CREATE SUBJECT !", Toast.LENGTH_SHORT).show();
                            } else {
//                                startActivity(new Intent(ExamResultEntry.this, CreateSubjectActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
        }else if (maxMarkListData.size() == 0) {

           snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.maximum_marks_list_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.scholastic_grade_marks_setup),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sharedPreferences.getBoolean(Config.GRADE_MARK_SETUP_PERMISSION, false)) {
                                Toast.makeText(ExamResultEntry.this, "Permission not available : GRADE MARK SETUP !", Toast.LENGTH_SHORT).show();
                            } else { startActivity(new Intent(ExamResultEntry.this, AnotherActivity.class).putExtra("id", "GradesRange"));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });

        }
        if (snackbarObj != null) {
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }
    }

    public void getGradeMarksListFromServer() {
        progressbar.show();
              String fetchClassList = sharedPreferences.getString("HostName", "Not Found")
                + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?databaseName=" + sharedPreferences.getString("organization_name", "");

        Log.d("fetchGradeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getGradeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                Grade obj = (new Gson()).fromJson(s, new TypeToken<Grade>() {
                                }.getType());
                                gradeListData = new ArrayList<>();
                                gradeListData = obj.getResult();
                                Collections.sort(gradeListData, new Comparator<Grade>() {
                                    @Override
                                    public int compare(Grade o1, Grade o2) {
                                        String lid = o1.getMaxMarks();
                                        String rid = o2.getMaxMarks();

                                        try {
                                            double ld = Double.parseDouble(lid);
                                            double rd = Double.parseDouble(rid);
                                            //  Log.e("ArrayListSort","INTEGER");
                                            if (ld > rd)
                                                return -1;
                                            else if (ld < rd)
                                                return 1;
                                            else
                                                return 0;

                                        } catch (NumberFormatException e) {
                                            // Log.e("ArrayListSort","String");
                                            return lid.compareTo(rid);
                                        }

                                        //  return lid.compareToIgnoreCase(rid);

                                    }
                                });
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(ExamResultEntry.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {

                                Snackbar snackbarObj = Snackbar.make(
                                        snackbar,
                                        getString(R.string.maximum_marks_list_not_available),
                                        Snackbar.LENGTH_LONG).setAction(getString(R.string.scholastic_grade_marks_setup),
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {  if (!sharedPreferences.getBoolean(Config.GRADE_MARK_SETUP_PERMISSION, false)) {
                                                Toast.makeText(ExamResultEntry.this, "Permission not available : GRADE MARK SETUP !", Toast.LENGTH_SHORT).show();
                                            } else {
                                                startActivity(new Intent(ExamResultEntry.this, AnotherActivity.class).putExtra("id", "GradesRange"));
                                                finish();
                                                overridePendingTransition(R.anim.enter, R.anim.nothing);}
                                            }
                                        });
                                snackbarObj.setDuration(5000);
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                            setMaxMarkList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(ExamResultEntry.this,volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag("exam");
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }

    public void setMaxMarkList() {

        maxArray = new ArrayList<>();
        for (Grade gradeObj : gradeListData) {
            if (!maxArray.contains(gradeObj.getMaxMarks())) {

                maxArray.add(gradeObj.getMaxMarks());
            }
        }
        ArrayAdapter<String> adapterMaxMarks = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, maxArray);
       adapterMaxMarks .setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_exam_result_max_marks_list.setAdapter(adapterMaxMarks);

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void fetchExamAlreadyFilledDataFromServer(final Intent intent) {
        progressbar.show();

        String db_name = sharedPreferences.getString("organization_name", "Not Found");
        final Map<String, Object> param = new LinkedHashMap<>();
        param.put("Session", sharedPreferences.getString("session", ""));
        param.put("Exam_Detail_Status", "Active");
        param.put("Exam_Type", intent.getStringExtra("selected_examtype"));
        param.put("Section_Id",intent.getStringExtra("selected_section"));
        param.put("Subject_Id", intent.getStringExtra("selected_subject"));
        param.put("DateOfExam", intent.getStringExtra("btn_currentdateExamResult"));

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        String url = sharedPreferences.getString("HostName", "Not Found")
                + sharedPreferences.getString(Config.applicationVersionName, "Not Found") +
                "ExamApi.php?databaseName=" + db_name+
                "&data=" + (new JSONObject(param1));

        Log.e("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
               /* sharedPreferences.getString("HostName", "Not Found") +
                        "reportview.php"*/
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("ResultResponse", s);

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.optString("code").equalsIgnoreCase("200")||
                            jsonObject.optString("code").equalsIgnoreCase("201")) {
                        final PreviousResultData obj = (new Gson()).fromJson(s, new TypeToken<PreviousResultData>() {
                        }.getType());
                        ArrayList<PreviousResultData> resultData = obj.getResultData() ;

                        //  ArrayList<ArrayList<PreviousResultData>> parts = chopped(resultData, 1000);

                        db.savePreviousExamResultDataFromServer(resultData);

                        Log.e("RRRR",resultData.size()+ " ttt");

                      /*  for (final ArrayList<PreviousResultData> partList : parts){

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("RRRR",partList.size()+ " ttt");

                                    db.savePreviousExamResultDataFromServer(partList);

                                }
                            }, 200);


                        }
*/





                        // db.savePreviousExamResultDataFromServer(obj.getResultData());

                    }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                            ||
                            jsonObject.optString("code").equalsIgnoreCase("511")) {
                        if (!logoutAlert & !ExamResultEntry.this.isFinishing()) {
                            Config.responseVolleyHandlerAlert(ExamResultEntry.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                            logoutAlert = true;
                        }
                    }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                        Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                    else {
                        showSnack(jsonObject.optString("message"));

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

                progressbar.dismiss();
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
              //  finish();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(ExamResultEntry.this,volleyError,snackbar);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.nothing);
               // finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(ExamResultEntry.this).addToRequestQueue(request);
    }


}
