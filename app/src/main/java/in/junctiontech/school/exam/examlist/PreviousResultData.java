package in.junctiontech.school.exam.examlist;

import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 07-06-2017.
 */

public class PreviousResultData {
    String Exam_Type,Section_Id, Student_Id,Subject_Id,Max_Marks, Marks_Obtain, Result,Grade,DateOfExam;

ArrayList<PreviousResultData> result;
    public String getExam_Type() {
        return Exam_Type;
    }

    public String getMax_Marks() {
        return Max_Marks;
    }

    public void setMax_Marks(String max_Marks) {
        Max_Marks = max_Marks;
    }

    public void setExam_Type(String exam_Type) {
        Exam_Type = exam_Type;
    }

    public String getSection_Id() {
        return Section_Id;
    }

    public void setSection_Id(String section_Id) {
        Section_Id = section_Id;
    }

    public String getStudent_Id() {
        return Student_Id;
    }

    public void setStudent_Id(String student_Id) {
        Student_Id = student_Id;
    }

    public String getSubject_Id() {
        return Subject_Id;
    }

    public void setSubject_Id(String subject_Id) {
        Subject_Id = subject_Id;
    }

    public String getMarks_Obtain() {
        return Marks_Obtain;
    }

    public void setMarks_Obtain(String marks_Obtain) {
        Marks_Obtain = marks_Obtain;
    }

    public String getResult() {
        return Result;
    }
    public ArrayList<PreviousResultData> getResultData() {
        return   result;
    }
    public void setResult(ArrayList<PreviousResultData> result) {
        this.result = result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getDateOfExam() {
        return DateOfExam;
    }

    public void setDateOfExam(String dateOfExam) {
        DateOfExam = dateOfExam;
    }
}
