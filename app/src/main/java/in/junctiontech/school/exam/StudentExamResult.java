package in.junctiontech.school.exam;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.models.StudentResult;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;

public class StudentExamResult extends AppCompatActivity {
    private SharedPreferences sp;
    private Spinner sp_parent_result_examtype;
    private String[] examType;

    private ProgressDialog progressDialog;
    private AlertDialog.Builder alert;

    private DbHandler db;
    private RecyclerView my_recycler_view;
    private JSONArray arr_subjectID, arr_subjectName;
    private ArrayList<StudentResult> listData = new ArrayList<>();
    private AlertDialog.Builder alert_normal;
    private boolean logoutAlert = false;
    //  private Button  btn_parent_result_exam_result;
    private Calendar calendar_result;
    private int yearResult, monthResult, dayResult;
    private LinearLayout snackbar;
    private int[] colorsList;
    private ResultAdapter mAdapter;
    private int colorIs = 0;
    private ArrayList<StudentData> classSectionList = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>();
    private ArrayAdapter<String> clsAdapter;
    private Spinner sp_first_class, sp_first_student;
    private ArrayList<String> studentArrayList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private ArrayList<StudentData> studentObjArrayList = new ArrayList<>();
    private String db_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sp.getString("app_language", ""));

        setContentView(R.layout.activity_exam_result);
        colorsList = getResources().getIntArray(R.array.color_array_notification_list);
        getSupportActionBar().setTitle(R.string.exam_results);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = DbHandler.getInstance(this);
        setColorApp();

        if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Not Found")) {
//            startActivity(new Intent(this, FirstScreen.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        } else {
            if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                    sp.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
                ;
            else
                findViewById(R.id.ll_examresult_class_selection).setVisibility(View.VISIBLE);

            sp_first_class = (Spinner) findViewById(R.id.sp_examresult_class);
            sp_first_student = (Spinner) findViewById(R.id.sp_examresult_student);

            progressDialog = new ProgressDialog(StudentExamResult.this);
            progressDialog.setMessage(getString(R.string.fetching_result));
            alert_normal = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alert_normal.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });


//        layout_viewReport = (LinearLayout) findViewById(R.id.layout_viewReport);
//        layout_viewReport.setVisibility(View.INVISIBLE);

            alert = new AlertDialog.Builder(StudentExamResult.this);
            alert.setMessage(getString(R.string.some_error_occurred_try_again_later));
            alert.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // if (Config.checkInternet(StudentExamResult.this))
                    try {
                        parentView();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                      /*  else {
                            alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                            alert.show();
                        }*/

                }
            });
            alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            snackbar = (LinearLayout) findViewById(R.id.ll_student_exam_result_Activity);
            sp_parent_result_examtype = (Spinner) findViewById(R.id.sp_parent_result_examtype);
            // sp_parent_result_subject = (Spinner) findViewById(R.id.sp_parent_result_subject);

            my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view_result);
            setupRecycler();

            examType = db.getExamType();
            ArrayAdapter<String> examArray = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, examType);
            examArray.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            sp_parent_result_examtype.setAdapter(examArray);
            classSectionList = db.getClassSectionList();
            for (StudentData obj : classSectionList)
                classSectionNameList.add(obj.getStudentName());

            try {
                setClassAdapter();
            } catch (JSONException e) {
                e.printStackTrace();
            }



            sp_parent_result_examtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        parentView();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
      /*      } else {
                android.support.v7.app.AlertDialog.Builder alertMsg = new android.support.v7.app.AlertDialog.Builder(this);
                alertMsg.setTitle(getString(R.string.sorry));
                alertMsg.setMessage(getString(R.string.exam_result_report)+" "+getString(R.string.feature_is_only_for_student_and_parents));
                alertMsg.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
                            startActivity(new Intent(StudentExamResult.this, AdminNavigationDrawer.class));
                            finish();

                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        } else {
                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        }
                    }
                });
                alertMsg.setCancelable(false);
                alertMsg.show();
            }
*/

            ((CheckBox) findViewById(R.id.tv_exam_result_sort_by_date)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

                    Collections.sort(listData, new Comparator<StudentResult>() {
                        @Override
                        public int compare(StudentResult o1, StudentResult o2) {
                            Calendar calLeft = Calendar.getInstance();
                            Calendar calRight = Calendar.getInstance();
                            String[] lname = o1.getExamDate().split("/");
                            String[] rname = o2.getExamDate().split("/");
                            if (lname.length >= 3) {
                                if (lname[2].length() == 4)
                                    calLeft.set(Integer.parseInt(lname[2]), (Integer.parseInt(lname[1]) - 1), Integer.parseInt(lname[0]));
                                else if (lname[0].length() == 4)
                                    calLeft.set(Integer.parseInt(lname[0]), (Integer.parseInt(lname[1]) - 1), Integer.parseInt(lname[2]));
                            }

                            if (rname.length >= 3) {
                                if (rname[2].length() == 4)
                                    calRight.set(Integer.parseInt(rname[2]), (Integer.parseInt(rname[1]) - 1), Integer.parseInt(rname[0]));
                                else if (rname[0].length() == 4)
                                    calRight.set(Integer.parseInt(rname[0]), (Integer.parseInt(rname[1]) - 1), Integer.parseInt(rname[2]));
                            }
                            if (isChecked) {
                                if (isChecked) {
                                    int res = calLeft.compareTo(calRight);
                                    if (res == 0) return 0;
                                    else if (res > 0) return -1;
                                    else if (res < 0) return 1;


                                }
                            }
                            return calLeft.compareTo(calRight);

                        }
                    });
                    mAdapter.notifyDataSetChanged();

                }
            });


            ((CheckBox) findViewById(R.id.tv_exam_result_sort_by_subject)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {

                    Collections.sort(listData, new Comparator<StudentResult>() {
                        @Override
                        public int compare(StudentResult o1, StudentResult o2) {
                            String lname = o1.getSubjectName().toLowerCase();
                            String rname = o2.getSubjectName().toLowerCase();

                            if (isChecked) {
                                int res = lname.compareTo(rname);
                                if (res == 0) return 0;
                                else if (res > 0) return -1;
                                else if (res < 0) return 1;

                            }
                            return lname.compareTo(rname);

                        }
                    });
                    mAdapter.notifyDataSetChanged();

                }
            });
        }


    }

    private void setClassAdapter() throws JSONException {
        clsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classSectionNameList);
        clsAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_class.setAdapter(clsAdapter);
        if (classSectionList.size() == 0)
            Config.responseSnackBarHandler(getString(R.string.classes_not_available), snackbar);
        else setStudentList();

        sp_first_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setStudentList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_first_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    parentView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setStudentList() {
        studentObjArrayList = (ArrayList<StudentData>) db.getStudents("", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());
        studentArrayList.clear();
        for (StudentData obj : studentObjArrayList)
            studentArrayList.add(obj.getStudentName());

        studentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, studentArrayList);
        studentAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_student.setAdapter(studentAdapter);
        if (studentObjArrayList.size() == 0) {
            mAdapter.updateList(new ArrayList<StudentResult>());
            Config.responseSnackBarHandler(getString(R.string.students_not_available), snackbar);
        } else try {
            parentView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_parent_result_examtype)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_examresult_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_examresult_student)).setTextColor(colorIs);
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(StudentExamResult.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(StudentExamResult.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return true;
    }


    public void parentView() throws JSONException {

        String studentID="";
        if (sp.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                sp.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
            studentID = sp.getString("loggedUserID", "Not Found");
        else if (studentObjArrayList.size() > 0 && sp_first_student.getSelectedItemPosition() != -1)
            studentID = /*sharedPreferences.getString("loggedUserID", "Not Found")*/ studentObjArrayList.get(sp_first_student.getSelectedItemPosition()).getStudentID();

        if (!studentID.equalsIgnoreCase("")) {

            progressDialog.show();
            progressDialog.setCancelable(false);

            String db_name = sp.getString("organization_name", "Not Found");

            final JSONObject param = new JSONObject();

            param.put("studentID", studentID);
            param.put("Exam_Type", examType[sp_parent_result_examtype.getSelectedItemPosition()]);

            param.put("Session", sp.getString("session", ""));

            String url = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found") +
                    "ExamApi.php?action=parentStudent&databaseName=" + db_name;

            Log.e("ParentExamFetchUrl", url);
            Log.e("param", param.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.e("result", jsonObject.toString());
                            progressDialog.dismiss();

                            listData = new ArrayList<>();
                            if (jsonObject.optString("code").equalsIgnoreCase("201")) {
                                if (jsonObject.optString("result") != null) {
                                    StudentResult obj = (new Gson()).fromJson(jsonObject.toString(), new TypeToken<StudentResult>() {
                                    }.getType());
                                    listData = obj.getResult();
                                    if (listData.size() == 0) {
                                        Snackbar snackbarObj = Snackbar.make(
                                                snackbar,
                                                getString(R.string.result_not_available),
                                                Snackbar.LENGTH_LONG).setAction(getString(R.string.ok),
                                                new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                    }
                                                });
                                        snackbarObj.setDuration(3000);
                                        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                                        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                                        snackbarObj.show();
                                    }
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(StudentExamResult.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            else {

                                Snackbar snackbarObj = Snackbar.make(
                                        snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction(getString(R.string.ok),
                                        new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                            }
                                        });
                                snackbarObj.setDuration(3000);
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
                                snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                            mAdapter.updateList(listData);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    Config.responseVolleyErrorHandler(StudentExamResult.this, volleyError, snackbar);

                }
            });


            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        }
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        my_recycler_view.setLayoutManager(layoutManager);
        mAdapter = new ResultAdapter(this, listData);
        my_recycler_view.setAdapter(mAdapter);
    }

    public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {
        Context context;
        ArrayList<StudentResult> resultData;

        public ResultAdapter(Context context, ArrayList<StudentResult> resultData) {
            this.context = context;
            this.resultData = resultData;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_student_result, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            StudentResult data = resultData.get(position);


            holder.tv_subject_name.setText(data.getSubjectName());
            holder.item_student_exam_date.setText(data.getExamDate());
            holder.tv_obtained_marks.setText((data.getObtainedMarks() == null ? "" : data.getObtainedMarks())
                    + "/" + (data.getMaximumMarks() == null ? "" : data.getMaximumMarks()));
            if (data.getResultStudent() != null && data.getResultStudent().equalsIgnoreCase("pass"))
                holder.tv_result.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            else if (data.getResultStudent() != null && data.getResultStudent().equalsIgnoreCase("fail"))
                holder.tv_result.setTextColor(getResources().getColor(android.R.color.holo_red_dark));

            holder.tv_result.setText(data.getResultStudent() == null ? "" : data.getResultStudent());
            holder.tv_grades.setText(data.getGrade() == null ? "" : data.getGrade());
            //  holder.item.setBackgroundColor(colorsList[position<=colorsList.length?position:(position%colorsList.length)]);
        }

        @Override
        public int getItemCount() {
            return resultData.size();
        }

        public void updateList(ArrayList<StudentResult> listData) {
            this.resultData = listData;
            notifyDataSetChanged();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_obtained_marks, tv_grades, tv_result, tv_subject_name, item_student_exam_date;
            LinearLayout item_student_exam_layout;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_obtained_marks = (TextView) itemView.findViewById(R.id.item_student_exam_marks);
                tv_subject_name = (TextView) itemView.findViewById(R.id.item_student_exam_subject);
                tv_grades = (TextView) itemView.findViewById(R.id.item_student_exam_grade);
                tv_result = (TextView) itemView.findViewById(R.id.item_student_exam_result_status);
                itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
                item_student_exam_layout = (LinearLayout) itemView.findViewById(R.id.item_student_exam_layout);
                item_student_exam_date = (TextView) itemView.findViewById(R.id.item_student_exam_date);
            }
        }
    }
}
