package in.junctiontech.school.exam.examlist;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;

/**
 * Created by JAYDEVI BHADE on 03-04-2017.
 */

public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.MyViewHolder> {
    private final FilledExamListFragment fragment;
    private ArrayList<ExamData> gradeList;
    private Context context;
    private int appColor;

    public ExamListAdapter(Context context, ArrayList<ExamData> gradeList, FilledExamListFragment fragment, int appColor) {
        this.gradeList = gradeList;
        this.context = context;
        this.fragment = fragment;
        this.appColor = appColor;
    }


    public void updateList(ArrayList<ExamData> gradeList) {
        this.gradeList = gradeList;
        notifyDataSetChanged();
    }

    @Override
    public ExamListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final ExamListAdapter.MyViewHolder holder, final int position) {
        final ExamData gradeObj = gradeList.get(position);
        String next = "<font color='#727272'>" + "( " +
                gradeObj.getClassSectionName() + " )" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(gradeObj.getExamName().toUpperCase() +
                " " + next));

        holder.tv_designation.setText(context.getString(R.string.subject)+ " : " +gradeObj.getSubjectName()+"\n"
                +
                context.getString(R.string.exam_date)+ " : " +gradeObj.getExamDate());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return gradeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name ,tv_designation;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(appColor);
            tv_designation = (TextView) itemView.findViewById(R.id.tv_designation);
            tv_designation.setAllCaps(true);
            tv_designation.setVisibility(View.VISIBLE);

            ImageView iv_item_icon = (ImageView) itemView.findViewById(R.id.iv_item_icon);
            iv_item_icon.setVisibility(View.VISIBLE);
            itemView.setClickable(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   fragment.openResultList(gradeList.get(getLayoutPosition()));
                }
            });


            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
        }
    }

    public void setFilter(ArrayList<ExamData> gradeList) {
        this.gradeList = new ArrayList<>();
        this.gradeList.addAll(gradeList);
        notifyDataSetChanged();
    }
}
