package in.junctiontech.school.exam;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.StudentData;

public class MarksListActivity extends AppCompatActivity {
    RecyclerView lv_fill_exammarks;
    private String selected_class_id, selected_section, selected_examtype, selected_subject;
    private LayoutInflater inflater;
    private String max_marks = "0";

    private String examDate;
    ArrayList<StudentData> studentDreport = new ArrayList<>();

    private String[] result, resultID;
    private ProgressDialog progressDialog;
    private String[] sid;
    private String[] stName;
    private DbHandler db;
    private SharedPreferences sharedPsreferences;
    private AlertDialog.Builder alert_normal;
    private MarksListActivity.ExamListAdapter adapter;
    private int colorIs = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));

        setContentView(R.layout.activity_fill_exam_marks);
        getSupportActionBar().setTitle(R.string.fill_marks);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(getIntent().getStringExtra("subtitle"));
        setColorApp();

       /* AdView mAdView = (AdView) findViewById(R.id.adview_view_exam_marks);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        if (sharedPsreferences.getBoolean("addStatusGone",false))
            mAdView.setVisibility(View.GONE);*/


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.fetching_data));

        alert_normal = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert_normal.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        db = DbHandler.getInstance(this);
        lv_fill_exammarks = (RecyclerView) findViewById(R.id.lv_fill_exammarks);
        setupRecycler();
        String[][] data_res = db.getResultList();
        resultID = data_res[0];
        result = data_res[1];
       /* // for Teacher Report View Result From Server*/
        if (getIntent().getBooleanExtra("report", false)) {

            selected_class_id = getIntent().getStringExtra("selected_class_id");
            selected_section = getIntent().getStringExtra("selected_section");
            selected_examtype = getIntent().getStringExtra("selected_examtype");
            selected_subject = getIntent().getStringExtra("selected_subject");
            String[][] aab = db.getStudentData(selected_class_id, selected_section, "");
            sid = aab[1];
            stName = aab[0];

            try {
                fetchReport(sid);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
//          /*   for Teacher  filled Result View*/
            ShowFilledResult();
        }


    }

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
     //   getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        lv_fill_exammarks.setLayoutManager(layoutManager);
        adapter = new ExamListAdapter(MarksListActivity.this, studentDreport, colorIs);
        lv_fill_exammarks.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (getIntent().getBooleanExtra("report", false))
            getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int ids = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (ids == R.id.refresh) {
            // if (Config.checkInternet(MarksListActivity.this))
            try {
                fetchReport(sid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /*else {
                AlertDialog.Builder alert = new AlertDialog.Builder(MarksListActivity.this);
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();overridePendingTransition(R.anim.nothing,R.anim.slide_out);
                    }
                });
                alert.show();
            }*/

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*synchronized public void fetchReport(final String[] sid) {
        studentDreport = new ArrayList<>();
        progressDialog.show();
        progressDialog.setCancelable(false);

        JSONArray rr = new JSONArray();

        for (int i = 0; i < sid.length; i++)
            rr.put(sid[i]);

        SharedPreferences sharedPreferences = Prefs.with(this).getSharedPreferences();
        String db_name = sharedPreferences.getString("organization_name", "Not Found");

//        String session;
//        Calendar c = Calendar.getInstance();
//        if ((c.get(Calendar.MONTH) + 1 >= 6))
//            session = c.get(Calendar.YEAR) + "-" + (c.get(Calendar.YEAR) + 1);
//        else session = (c.get(Calendar.YEAR) - 1) + "-" + (c.get(Calendar.YEAR));

        final Map<String, Object> param = new LinkedHashMap<>();
     //   param.put("DB_Name", db_name);
        param.put("userType", "Teacher");
        param.put("viewRequest", "Result");
        param.put("Exam_Type", selected_examtype);
        param.put("subjectID", selected_subject);
        param.put("sectionID", selected_section);
        param.put("session", sharedPreferences.getString("session", ""));
        param.put("studentID", rr);

        //  String login_url = "http://junctiondev.cloudapp.net/sms/reportview.php";
      //  RequestQueue queue = Volley.newRequestQueue(this);

        String examUrl = sharedPreferences.getString("HostName", "Not Found")
                + "version2.0/ExamApi.php?action=teacher&databaseName=" + db_name;
        Log.e("examUrl", examUrl);

        StringRequest request = new StringRequest(Request.Method.POST,
                examUrl
//                sharedPreferences.getString("HostName", "Not Found") +
//                "reportview.php"
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                    Log.d("ResultResponse", s+" ritu");
                progressDialog.dismiss();
                try {
                    JSONObject resJson= new JSONObject(s);
                    if (resJson.optString("code").equalsIgnoreCase("201")) {
                        JSONArray job = new JSONArray(resJson.optString("result"));
                        if (job.length() != 0) {

                            for (int i = 0; i < job.length(); i++) {
                                JSONObject jj = job.getJSONObject(i);

                                for (int a = 0; a < stName.length; a++) {
                                    if (jj.getString("studentID").equalsIgnoreCase(sid[a])) {
                                        String grad = jj.getString("Grade");
                                        if (grad.equalsIgnoreCase("null") || grad == null)
                                            grad = "";
                                        String obt = jj.getString("Marks_Obtain");
                                        if (obt.equalsIgnoreCase("null") || obt == null)
                                            obt = "0";

                                        String res = jj.getString("ResultID");
                                        if (res.equalsIgnoreCase("null") || res == null)
                                            res = "";


                                        if (jj.getString("Max_Marks") == null || jj.getString("Max_Marks").equalsIgnoreCase("null"))
                                            max_marks = "0";
                                        else max_marks = jj.getString("Max_Marks");

                                        studentDreport.add(i, new StudentData(jj.getString("studentID"), stName[a], grad,
                                                obt, res, max_marks));
                                        break;
                                    }
                                }
//                            if (jj.getString("Max_Marks") == null || jj.getString("Max_Marks").equalsIgnoreCase("null"))
//                                ;
//                            else max_marks = jj.getString("Max_Marks");


                            }

                            lv_fill_exammarks.setAdapter(new MyListAdapter(ExamMarksFilledListActivity.this, studentDreport));
                            progressDialog.dismiss();

                        } else {

                            AlertDialog.Builder alert = new AlertDialog.Builder(ExamMarksFilledListActivity.this);
                            alert.setMessage(getString(R.string.result_not_available));
                            alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                }
                            });
                            alert.show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                progressDialog.dismiss();
                AlertDialog.Builder alert = new AlertDialog.Builder(ExamMarksFilledListActivity.this);
                alert.setMessage(getString(R.string.some_error_occurred_try_again_later));
                alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();overridePendingTransition(R.anim.nothing,R.anim.slide_out);
                    }
                });
                alert.show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());
                    Log.d("TeacherExamResult",new  JSONObject(param).toString());

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }*/
    synchronized public void fetchReport(final String[] sid) throws JSONException {
        studentDreport = new ArrayList<>();
        progressDialog.show();
        progressDialog.setCancelable(false);

        JSONArray rr = new JSONArray();

        for (int i = 0; i < sid.length; i++)
            rr.put(sid[i]);

        SharedPreferences sharedPreferences = Prefs.with(this).getSharedPreferences();
        String db_name = sharedPreferences.getString("organization_name", "Not Found");


        final JSONObject param = new JSONObject();
        //  param.put("DB_Name", db_name);
        //  param.put("userType", "Teacher");
        //  param.put("viewRequest", "Result");
        param.put("Exam_Type", selected_examtype);
        param.put("subjectID", selected_subject);
        param.put("sectionID", selected_section);
        param.put("examDate", getIntent().getStringExtra("examDate"));
        param.put("session", sharedPreferences.getString("session", ""));
        param.put("studentID", rr);

        //  String login_url = "http://junctiondev.cloudapp.net/sms/reportview.php";
        //  RequestQueue queue = Volley.newRequestQueue(this);


        String url = sharedPreferences.getString("HostName", "Not Found")
                + sharedPreferences.getString(Config.applicationVersionName, "Not Found") +
                "ExamApi.php?action=teacher&databaseName=" + db_name;

        Log.e("url", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d("ResultResponse", jsonObject.toString());
                        progressDialog.dismiss();


                            if (jsonObject.optString("code").equalsIgnoreCase("201")) {
                                if (jsonObject.optString("result") != null) {

                                    JSONArray job = null;
                                    try {
                                        job = new JSONArray(jsonObject.optString("result"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (job.length() != 0) {
                                        for (int i = 0; i < job.length(); i++) {
                                            JSONObject jj = job.optJSONObject(i);

                                            for (int a = 0; a < stName.length; a++) {
                                                if (jj.optString("studentID").equalsIgnoreCase(sid[a])) {
                                                    String grad = jj.optString("Grade");
                                                    if (grad.equalsIgnoreCase("null") || grad == null)
                                                        grad = "";
                                                    String obt = jj.optString("Marks_Obtain");
                                                    if (obt.equalsIgnoreCase("null") || obt == null)
                                                        obt = "0";

                                                    String res = jj.optString("ResultID");
                                                    if (res.equalsIgnoreCase("null") || res == null)
                                                        res = "";


                                                    if (jj.optString("Max_Marks") == null || jj.optString("Max_Marks").equalsIgnoreCase("null"))
                                                        max_marks = "0";
                                                    else max_marks = jj.optString("Max_Marks");

                                                    studentDreport.add(i, new StudentData(jj.optString("studentID"), stName[a], grad,
                                                            obt, res, max_marks));
                                                    break;
                                                }
                                            }
//                            if (jj.getString("Max_Marks") == null || jj.getString("Max_Marks").equalsIgnoreCase("null"))
//                                ;
//                            else max_marks = jj.getString("Max_Marks");


                                        }
                                        adapter.updateList(studentDreport);


                                    } else {
                                        progressDialog.dismiss();
                                        showDialog();

                                    }
                                } else {
                                    progressDialog.dismiss();
                                    showDialog();

                                }


                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                alert_normal.setMessage(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min));
                                alert_normal.show();
                            } else {
                                alert_normal.setMessage(jsonObject.optString("message"));
                                alert_normal.show();
                            }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                progressDialog.dismiss();
                String msg = "Error !";
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    msg = getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.please_retry_error_occurred);

                } else if (volleyError instanceof AuthFailureError) {
                    msg = "AuthFailureError!";
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    msg = "ServerError!";
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    msg = "NetworkError!";
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    msg = "ParseError!";
                    //TODO
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(MarksListActivity.this);
                alert.setMessage(msg);
                alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                    }
                });
                alert.show();

            }
        });

        /*StringRequest request = new StringRequest(Request.Method.POST,
                url
               *//* sharedPreferences.getString("HostName", "Not Found") +
                        "reportview.php"*//*
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("ResultResponse", s);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.optString("code").equalsIgnoreCase("201")) {
                        if (jsonObject.optString("result") != null) {

                            JSONArray job = new JSONArray(jsonObject.optString("result"));

                            if (job.length() != 0) {
                                for (int i = 0; i < job.length(); i++) {
                                    JSONObject jj = job.getJSONObject(i);

                                    for (int a = 0; a < stName.length; a++) {
                                        if (jj.getString("studentID").equalsIgnoreCase(sid[a])) {
                                            String grad = jj.getString("Grade");
                                            if (grad.equalsIgnoreCase("null") || grad == null)
                                                grad = "";
                                            String obt = jj.getString("Marks_Obtain");
                                            if (obt.equalsIgnoreCase("null") || obt == null)
                                                obt = "0";

                                            String res = jj.getString("ResultID");
                                            if (res.equalsIgnoreCase("null") || res == null)
                                                res = "";


                                            if (jj.getString("Max_Marks") == null || jj.getString("Max_Marks").equalsIgnoreCase("null"))
                                                max_marks = "0";
                                            else max_marks = jj.getString("Max_Marks");

                                            studentDreport.add(i, new StudentData(jj.getString("studentID"), stName[a], grad,
                                                    obt, res, max_marks));
                                            break;
                                        }
                                    }
//                            if (jj.getString("Max_Marks") == null || jj.getString("Max_Marks").equalsIgnoreCase("null"))
//                                ;
//                            else max_marks = jj.getString("Max_Marks");


                                }
                                adapter.updateList(studentDreport);


                            } else {
                                progressDialog.dismiss();
                                showDialog();

                            }
                        } else {
                            progressDialog.dismiss();
                            showDialog();

                        }


                    } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                        alert_normal.setMessage(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min));
                        alert_normal.show();
                    } else {
                        alert_normal.setMessage(jsonObject.optString("message"));
                        alert_normal.show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    showDialog();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("error volley", volleyError.toString());
                progressDialog.dismiss();
                String msg = "Error !";
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    msg = getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.please_retry_error_occurred);

                } else if (volleyError instanceof AuthFailureError) {
                    msg = "AuthFailureError!";
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    msg = "ServerError!";
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    msg = "NetworkError!";
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    msg = "ParseError!";
                    //TODO
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(MarksListActivity.this);
                alert.setMessage(msg);
                alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                    }
                });
                alert.show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());
                Log.d("ritu", new JSONObject(param).toString());

                return params;
            }
        };
*/

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private void showDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MarksListActivity.this,
                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setMessage(getString(R.string.result_not_available));
        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        alert.show();
    }

    @Override
    public void onBackPressed() {
     /*   if (getIntent().getBooleanExtra("report", false)) {
            startActivity(new Intent(this, TeacherReportView.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);

        } else {*/
            /*Intent intent = new Intent(MarksListActivity.this,
                    FillMarksActivity.class);
            intent.putExtra("selected_examtype", getIntent().getStringExtra("selected_examtype"));
            intent.putExtra("selected_class_id", getIntent().getStringExtra("selected_class_id"));
            intent.putExtra("selected_section", getIntent().getStringExtra("selected_section"));
            intent.putExtra("selected_class_idNAME", getIntent().getStringExtra("selected_class_idNAME"));
            intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
            intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
            intent.putExtra("selected_subject", getIntent().getStringExtra("selected_subject"));
            intent.putExtra("et_maximum_marks",getIntent().getStringExtra("et_maximum_marks"));
            intent.putExtra("btn_currentdateExamResult", getIntent().getStringExtra("btn_currentdateExamResult"));

            intent.putExtra("back", false);
            startActivity(intent);*/
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);

       // }
        super.onBackPressed();
    }

    public void ShowFilledResult() {
        selected_class_id = getIntent().getStringExtra("selected_class_id");
        selected_section = getIntent().getStringExtra("selected_section");
        selected_examtype = getIntent().getStringExtra("selected_examtype");
        selected_subject = getIntent().getStringExtra("selected_subject");
        max_marks = getIntent().getStringExtra("et_maximum_marks");
        examDate = getIntent().getStringExtra("examDate");
       /* result = *//*getIntent().getStringArrayExtra("result")*//* result;
        resultID = getIntent().getStringArrayExtra("resultID");
*/
        String[][] data = db.getStudentData(selected_class_id, selected_section, "");

        ArrayList<StudentData> studentDataList = db.getExamResult(selected_examtype, selected_subject, data[1], examDate);
        adapter.updateList(studentDataList);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


        if (getIntent().getBooleanExtra("report", false))
            progressDialog.dismiss();

    }

    @Override
    public boolean onSupportNavigateUp() {
        /*if (getIntent().getBooleanExtra("report", false)) {
            startActivity(new Intent(this, TeacherReportView.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);

        } else {*/
          /*  Intent intent = new Intent(MarksListActivity.this,
                    FillMarksActivity.class);
            intent.putExtra("selected_examtype", getIntent().getStringExtra("selected_examtype"));
            intent.putExtra("selected_class_id", getIntent().getStringExtra("selected_class_id"));
            intent.putExtra("selected_section", getIntent().getStringExtra("selected_section"));
            intent.putExtra("selected_class_idNAME", getIntent().getStringExtra("selected_class_idNAME"));
            intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
            intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
            intent.putExtra("selected_subject", getIntent().getStringExtra("selected_subject"));
            intent.putExtra("et_maximum_marks",getIntent().getStringExtra("et_maximum_marks"));
            intent.putExtra("btn_currentdateExamResult", getIntent().getStringExtra("btn_currentdateExamResult"));

            intent.putExtra("back", false);
            startActivity(intent);*/
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);

       // }
        return true;
    }


    public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.MyViewHolder> {

        private ArrayList<StudentData> gradeList;
        private Context context;
        private int appColor;

        public ExamListAdapter(Context context, ArrayList<StudentData> gradeList, int appColor) {
            this.gradeList = gradeList;
            this.context = context;
            this.appColor = appColor;

        }


        public void updateList(ArrayList<StudentData> gradeList) {
            this.gradeList = gradeList;
            notifyDataSetChanged();
        }

        @Override
        public ExamListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_fill_exam_marks, parent, false);

            return new MyViewHolder(view);
        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public void onBindViewHolder(final ExamListAdapter.MyViewHolder holder, final int position) {

            StudentData studentData = gradeList.get(position);
            if (studentData.getStudentMarks().equalsIgnoreCase("") || studentData.getStudentMarks() == null)
                holder.tv_fill_exammarks_marks.setText("0" + "/" + studentData.getMax_marks());
            else
                holder.tv_fill_exammarks_marks.setText(studentData.getStudentMarks() + "/" + studentData.getMax_marks());

            holder.tv_fill_exammarks_studentName.setText(studentData.getStudentName());
            holder.tv_for_rollNO_fill_exammarks.setText(getString(R.string.roll_no) + " " + studentData.getStudentID());
            holder.sp_fill_exammarks_grade.setText(studentData.getStudentGrade());

            if (studentData.getStudentResult().equalsIgnoreCase(""))
                holder.tv_for_result_fill_exammarks.setText("");
            else {
                for (int i = 0; i < result.length; i++) {
                    if (studentData.getStudentResult().equalsIgnoreCase(resultID[i])) {
                        holder.tv_for_result_fill_exammarks.setText(result[i]);
                        break;
                    }

                }
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return gradeList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_fill_exammarks_studentName, tv_for_rollNO_fill_exammarks, sp_fill_exammarks_grade,
                    tv_fill_exammarks_marks, tv_for_result_fill_exammarks;

            public MyViewHolder(View itemView) {
                super(itemView);
                tv_fill_exammarks_studentName = (TextView) itemView.findViewById(R.id.tv_fill_exammarks_studentName);
                tv_fill_exammarks_studentName.setTextColor(appColor);

                tv_for_rollNO_fill_exammarks = (TextView) itemView.findViewById(R.id.tv_for_rollNO_fill_exammarks);
                sp_fill_exammarks_grade = (TextView) itemView.findViewById(R.id.tv_fill_exammarks_grade);
                tv_fill_exammarks_marks = (TextView) itemView.findViewById(R.id.tv_fill_exammarks_marks);
                tv_for_result_fill_exammarks = (TextView) itemView.findViewById(R.id.tv_for_result_fill_exammarks);
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (!getIntent().getBooleanExtra("report", false)) {
                            androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(context);
                            final String[] aa = new String[]{"Edit"};
                            alert.setItems(aa, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(MarksListActivity.this,
                                            FillMarksActivity.class);

                                    intent.putExtra("selected_examtype", getIntent().getStringExtra("selected_examtype"));
                                    intent.putExtra("selected_class_id", getIntent().getStringExtra("selected_class_id"));
                                    intent.putExtra("selected_section", getIntent().getStringExtra("selected_section"));
                                    intent.putExtra("selected_class_idNAME", getIntent().getStringExtra("selected_class_idNAME"));
                                    intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
                                    intent.putExtra("selected_sectionNAME", getIntent().getStringExtra("selected_sectionNAME"));
                                    intent.putExtra("selected_subject", getIntent().getStringExtra("selected_subject"));
                                    intent.putExtra("et_maximum_marks", getIntent().getStringExtra("et_maximum_marks"));
                                    intent.putExtra("btn_currentdateExamResult", getIntent().getStringExtra("btn_currentdateExamResult"));
                                    intent.putExtra("markObjList", getIntent().getSerializableExtra("markObjList"));
                                    intent.putExtra("back", true);
                                    intent.putExtra("position", getLayoutPosition());
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.nothing);

                                }
                            });

                            alert.show();
                        }
                        return false;
                    }
                });

                itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
            }
        }


    }
}
