package in.junctiontech.school.exam.graderange;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.exam.Grade;

/**
 * Created by JAYDEVI BHADE on 30-03-2017.
 */

public class MaximumMarksListAdapter extends RecyclerView.Adapter<MaximumMarksListAdapter.MyViewHolder> {
    private final GradeRangeFragment createGradesFragment;
    private ArrayList<Grade> gradeList;
    private Context context;
    private int appColor;

    public MaximumMarksListAdapter(Context context, ArrayList<Grade> gradeList, GradeRangeFragment createGradesFragment, int appColor) {
        this.gradeList = gradeList;
        this.context = context;
        this.createGradesFragment = createGradesFragment;
        this.appColor= appColor;
    }


    public void updateList(ArrayList<Grade> gradeList) {
        this.gradeList = gradeList;
        notifyDataSetChanged();
    }

    @Override
    public MaximumMarksListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final MaximumMarksListAdapter.MyViewHolder holder, final int position) {
        final Grade gradeObj = gradeList.get(position);
        holder.tv_item_name.setText(gradeObj.getMaxMarks().toUpperCase());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return gradeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);

            tv_item_name.setTextColor(appColor);
            ImageView  iv_item_icon = (ImageView) itemView.findViewById(R.id.iv_item_icon);
            iv_item_icon.setVisibility(View.VISIBLE);
            itemView.setClickable(true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    createGradesFragment.openGradList(gradeList.get(getLayoutPosition()));
                }
            });


            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
        }
    }


}