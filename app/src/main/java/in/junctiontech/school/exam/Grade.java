package in.junctiontech.school.exam;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 30-03-2017.
 */

public class Grade implements Serializable {
    String gradeID,maxMarks, rangeStart,rangeEnd,grade,gradeResult,gradeStatus ,gradeValue,
            resultValue;

    ArrayList<Grade> result;

    public Grade(String gradeId,String grade,String gradeValue, String resultValueId, String resultValue,String rangeStart,String rangeEnd, String maxMarks) {
        this.gradeID  = gradeId;
        this.grade  = grade;
        this.gradeValue  = gradeValue;

        this.gradeResult = resultValueId;
        this.resultValue = resultValue;

        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.maxMarks = maxMarks;
    }

    public Grade(String gradeID, String maxMarks) {
        this.gradeID = gradeID;
        this.maxMarks = maxMarks;
    }

    public String getGradeID() {
        return gradeID;
    }

    public void setGradeID(String gradeID) {
        this.gradeID = gradeID;
    }

    public String getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(String maxMarks) {
        this.maxMarks = maxMarks;
    }

    public String getRangeStart() {
        return rangeStart;
    }

    public void setRangeStart(String rangeStart) {
        this.rangeStart = rangeStart;
    }

    public String getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(String rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getGradeResult() {
        return gradeResult;
    }

    public void setGradeResult(String gradeResult) {
        this.gradeResult = gradeResult;
    }

    public String getGradeStatus() {
        return gradeStatus;
    }

    public void setGradeStatus(String gradeStatus) {
        this.gradeStatus = gradeStatus;
    }

    public String getGradeValue() {
        return gradeValue;
    }

    public void setGradeValue(String gradeValue) {
        this.gradeValue = gradeValue;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    public ArrayList<Grade> getResult() {
        return result;
    }

    public void setResult(ArrayList<Grade> result) {
        this.result = result;
    }
}
