package in.junctiontech.school.exam.graderange;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.exam.Grade;
import in.junctiontech.school.managefee.AnotherActivity;

/**
 * Created by JAYDEVI BHADE on 30-03-2017.
 */

public class GradeRangeFragment extends
        Fragment implements SearchView.OnQueryTextListener {

    private FloatingActionButton btn_add_class;
    private RecyclerView recycler_view_school_class_list;
    private MaximumMarksListAdapter adapter;

    private boolean classNameAlreadyRegister = false;
    private RelativeLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private ArrayList<Grade> gradeListData = new ArrayList<>();
    private ArrayList<Grade> maxMarkListData = new ArrayList<>();
    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_list_and_one_button, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        snackbar = (RelativeLayout) convertView.findViewById(R.id.ll_fragment_list_and_one_button);
        //  et_class_name = (EditText) convertView.findViewById(R.id.et_create_school_class_class_name);
        FloatingActionButton  fab_fragment_add=  (FloatingActionButton) convertView.findViewById(R.id.fab_fragment_add);

        fab_fragment_add.setBackgroundTintList( ColorStateList.valueOf(appColor));
       fab_fragment_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMaximumMarks();


            }
        });
        recycler_view_school_class_list = (RecyclerView) convertView.findViewById(R.id.rv_fragment_list);
      /*   et_class_name.setHint("");
        ((TextInputLayout)convertView.findViewById(R.id.text_input_layout_write_name)).setHint(getString(R.string.write_grade_name));
        ((TextView)convertView.findViewById(R.id.tv_header_list_name)).setText(getString(R.string.grade_list));

        et_class_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equalsIgnoreCase(""))
                    checkGradeToServer(
                            et_class_name.getText().toString().trim(),false*//*not for save just check class name*//*,
                            false*//* not from adapter*//*);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btn_add_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_class_name.getText().toString().trim().equalsIgnoreCase("")) {
                    et_class_name.setError(getString(R.string.please_write_class_name));
                } else {
                    Config.hideKeyboard(getActivity());
                    if (!classNameAlreadyRegister) {

                        addGradeToServer(
                                et_class_name.getText().toString().trim(),false*//* not first save firsly check class name*//*);
                        // btn_add_class.setText(R.string.add);

                    } else {
//                       showDialougeWithMsg(
//                                getString(R.string.class_name_already_exist)
//                        );
                    }
                }
            }
        });*/
        ((Button)convertView.findViewById(R.id.btn_fragment_save)).setBackgroundColor(appColor);
        setHasOptionsMenu(true);
        return convertView;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view_school_class_list.setLayoutManager(layoutManager);
        adapter = new MaximumMarksListAdapter(getActivity(), maxMarkListData, GradeRangeFragment.this,appColor);
        recycler_view_school_class_list.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appColor=  Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();

        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setupRecycler();
        getGradeMarksListFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");


                    if (maxMarkListData.size() == 0)
                        getGradeMarksListFromServer();


                }
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void openGradList(Grade grade) {
        ArrayList<Grade> marksGradeListData = new ArrayList<>();
        for (Grade gradeObj : filterMark(gradeListData, grade.getMaxMarks())) {
            marksGradeListData.add(new Grade(
                            gradeObj.getGradeID(),
                            gradeObj.getGrade(),
                            gradeObj.getGradeValue(),
                            gradeObj.getGradeResult(),
                            gradeObj.getResultValue(),
                            gradeObj.getRangeStart(),
                            gradeObj.getRangeEnd(),
                            gradeObj.getMaxMarks()
                    )
            );
        }

        startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "GradeResultMapping")
                .putExtra("maxMarks", grade.getMaxMarks())
                .putExtra("dataList", marksGradeListData)
                .putExtra("isNewEntry", false)
        );
        getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
        getActivity().onBackPressed();
    }

    public void setMaxMarkList() {

        ArrayList<String> maxArray = new ArrayList<>();
        for (Grade gradeObj : gradeListData) {

            if (!maxArray.contains(gradeObj.getMaxMarks())) {
                maxMarkListData.add(new Grade(gradeObj.getGradeID(), gradeObj.getMaxMarks()));
                maxArray.add(gradeObj.getMaxMarks());
            }
        }



        this.adapter.updateList(maxMarkListData);

    }

    public void performClick() {
        btn_add_class.performClick();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter != null)
                            adapter.updateList(maxMarkListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<Grade> filteredModelList = filter(maxMarkListData, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<Grade> filter(ArrayList<Grade> models, String query) {
        query = query.toLowerCase();
        final ArrayList<Grade> filteredModelList = new ArrayList<>();

        for (Grade model : models) {

            if (model.getMaxMarks().toLowerCase().contains(query)

                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private ArrayList<Grade> filterMark(ArrayList<Grade> models, String query) {
        query = query.toLowerCase();
        final ArrayList<Grade> filteredModelList = new ArrayList<>();

        for (Grade model : models) {

            if (model.getMaxMarks().toLowerCase().equalsIgnoreCase(query)

                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    public void getGradeMarksListFromServer() {
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();


        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?databaseName=" + dbName;

        Log.d("fetchGradeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getGradeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                Grade obj = gson.fromJson(s, new TypeToken<Grade>() {
                                }.getType());
                                gradeListData = new ArrayList<>();
                                gradeListData = obj.getResult();

                                Collections.sort(gradeListData, new Comparator<Grade>() {
                                    @Override
                                    public int compare(Grade o1, Grade o2) {
                                        String lid = o1.getMaxMarks();
                                        String rid = o2.getMaxMarks();

                                        try {
                                            int ld = Integer.parseInt(lid);
                                            int rd = Integer.parseInt(rid);
                                            //  Log.e("ArrayListSort","INTEGER");
                                            if (ld > rd)
                                                return -1;
                                            else if (ld < rd)
                                                return 1;
                                            else
                                                return 0;

                                        } catch (NumberFormatException e) {
                                            // Log.e("ArrayListSort","String");
                                            return lid.compareTo(rid);
                                        }

                                        //  return lid.compareToIgnoreCase(rid);

                                    }
                                });

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        getString(R.string.data_not_available),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                            setMaxMarkList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    public void checkMarksToServer(final String gradeName, final boolean saveOrNot) {


        Map<String, String> param = new LinkedHashMap<String, String>();

        param.put("maxMarks", gradeName);

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("markCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("markCheck", s);
                        checkingKey = false;
                        if (progressbar.isShowing())
                            progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                isNameAlreadyExist = true;
                                if (progressbar.isShowing()) {
                                    progressbar.cancel();
                                }

                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        getString(R.string.maximum_marks) + " " + getString(R.string.already_exist),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();




                                /*if (updateClass) {
                                    *//* update class*//*
                                    if (saveOrNot)
                                        Toast.makeText(ClassSectionActivity.this,
                                                getString(R.string.class_name_already_exist), Toast.LENGTH_SHORT).show();
                                } else {
                                    *//* add class *//*
                                    fragmentCreateClass.setClassNameAlreadyRegister(true);
                                }


*/

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "GradeResultMapping")
                                        .putExtra("maxMarks", gradeName)
                                        .putExtra("isNewEntry", true));
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);
                                getActivity().onBackPressed();
/*
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("markKey", volleyError.toString());
                if (progressbar.isShowing())
                    progressbar.cancel();
                checkingKey = false;
                isNameAlreadyExist = false;
                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");

    }

    public void addMarksToServer(String feeName, boolean saveOrNot) throws JSONException {
        progressbar.show();
        if (saveOrNot) {

            progressbar.show();
            if (!checkingKey) {
                if (isNameAlreadyExist) {
                    Snackbar snackbarObj = Snackbar.make(snackbar,
                            getString(R.string.grade_alresdy_exist),
                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                    snackbarObj.show();

                } else {
                    final JSONObject param = new JSONObject();

                    param.put("MasterEntryValue", feeName.trim().toUpperCase());
                    param.put("MasterEntryStatus", "Active");
                    param.put("MasterEntryName", "Grade");
                    param.put("Grade", "");
                    param.put("GradeRange", "");



                    String addClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "Grade.php?databaseName=" + dbName;
                    Log.d("addGradeUrl", addClassUrl);
                    Log.d("param", param.toString());

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,  addClassUrl,param   ,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    DbHandler.longInfo(jsonObject.toString());
                                    Log.e("ResultGradeFee", jsonObject.toString());
                                    progressbar.cancel();

                                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                            getGradeMarksListFromServer();


                                            Toast.makeText(getActivity(),
                                                    jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !(getActivity()).isFinishing()) {
                                                Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                            Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                        else {
                                           /* alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();*/
                                            Snackbar snackbarObj = Snackbar.make(snackbar,
                                                    jsonObject.optString("message"),
                                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                }
                                            });
                                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                            snackbarObj.show();

                                        }



                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("ResultGrade", volleyError.toString());
                            Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

                            progressbar.cancel();
                        }
                    })  ;

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request.setTag(getTag());
                    AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

                }


            }

        } else {
            checkMarksToServer(feeName, true);

        }

    }


    public void addMaximumMarks() {
        android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(getActivity(), android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_edit));
        alertUpdate.setTitle(getString(R.string.enter_maximum_marks));

        final EditText et_name = new EditText(getActivity());
        et_name.setHint(getString(R.string.enter_maximum_marks));
        et_name.setInputType(InputType.TYPE_CLASS_NUMBER);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        et_name.setFilters(new InputFilter[]{new InputFilter.LengthFilter(getActivity().getResources().getInteger(R.integer.fee_amount))});
        linearLayout.addView(et_name);
        linearLayout.setPadding(15, 10, 15, 10);
        alertUpdate.setView(linearLayout);
        alertUpdate.setCancelable(false);
        alertUpdate.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (et_name.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();
                } else {
                    checkMarksToServer(et_name.getText().toString().trim(), true); } }
        });
        alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        alertUpdate.show();
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


}
