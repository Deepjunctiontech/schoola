package in.junctiontech.school.exam.createexamtype;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.ExamType;

/**
 * Created by JAYDEVI BHADE on 15-04-2017.
 */

public class CreateExamTypeFragment extends
        Fragment implements SearchView.OnQueryTextListener {

    private Button btn_add_class;
    private RecyclerView recycler_view_school_class_list;
    private ExamTypeListAdapter adapter;

    private boolean classNameAlreadyRegister = false;
    private LinearLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private ArrayList<ExamType> examTypeListData = new ArrayList<>();
    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private EditText  et_class_name;
    private RelativeLayout  page_demo;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
     if (container==null)return null;
        View convertView = inflater.inflate(R.layout.fragment_create_school_class, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

//        snackbar = (LinearLayout) convertView.findViewById(R.id.activity_create_class);
//        et_class_name = (EditText) convertView.findViewById(R.id.et_create_school_class_class_name);
//        btn_add_class = (Button) convertView.findViewById(R.id.btn_add_school_class);
        recycler_view_school_class_list = (RecyclerView) convertView.findViewById(R.id.recycler_view_school_class_list);
        et_class_name.setHint("");
        et_class_name.setFilters(new InputFilter[] {   new InputFilter.LengthFilter(getResources().getInteger(R.integer.exam_type)) });

//        ((TextInputLayout) convertView.findViewById(R.id.text_input_layout_write_name)).setHint(getString(R.string.write_exam_type_name));
//        ((TextView) convertView.findViewById(R.id.tv_header_list_name)).setText(getString(R.string.exam_type_list));

        et_class_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().equalsIgnoreCase(""))
                    checkExamTypeToServer(new ExamType(),
                            et_class_name.getText().toString().trim(), false/*not for save just check  ExamType name*/,
                            false/* not from adapter*/);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btn_add_class.setBackgroundColor(appColor);
        btn_add_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_class_name.getText().toString().trim().equalsIgnoreCase("")) {
                    et_class_name.setError(getString(R.string.name_can_not_be_blank));
                } else {
                    Config.hideKeyboard(getActivity());
                    if (!classNameAlreadyRegister) {

                        try {
                            addExamTypeToServer(
                                    et_class_name.getText().toString().trim(), false/* not first save firstly check ExamType name*/);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }
            }
        });
        /*********************************    Demo Screen *******************************/
//        page_demo =(RelativeLayout)convertView.findViewById(R.id.rl_create_grades_demo);
//        if (sp.getBoolean("DemoCreateExamType",true)){
//            page_demo.setVisibility(View.VISIBLE);
//        }else page_demo.setVisibility(View.GONE);
//
//        page_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                page_demo.setVisibility(View.GONE);
//                sp.edit().putBoolean("DemoCreateExamType",false).commit();
//            }
//        });
        /****************************************************************/

        setHasOptionsMenu(true);
        return convertView;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler_view_school_class_list.setLayoutManager(layoutManager);
        adapter = new ExamTypeListAdapter(getActivity(), examTypeListData, CreateExamTypeFragment.this,appColor);
        recycler_view_school_class_list.setAdapter(adapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();
       appColor=  Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setupRecycler();
        getExamTypeListFromServer();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");


                    if (examTypeListData.size() == 0)
                        getExamTypeListFromServer();


                }
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if (adapter != null)
                            adapter.updateList(examTypeListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<ExamType> filteredModelList = filter(examTypeListData, newText);
        adapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<ExamType> filter(ArrayList<ExamType> models, String query) {
        query = query.toLowerCase();
        final ArrayList<ExamType> filteredModelList = new ArrayList<>();

        for (ExamType model : models) {

            if (model.getExam_Type().toLowerCase().contains(query)

                    ) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    public void getExamTypeListFromServer() {
        progressbar.show();
        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ExamType.php?databaseName=" + dbName;

        Log.d("fetchExamTypeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getExamTypeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                ExamType obj = gson.fromJson(s, new TypeToken<ExamType>() {
                                }.getType());
                                examTypeListData = new ArrayList<>();
                                examTypeListData = obj.getResult();


                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        getString(R.string.data_not_available),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                            adapter.updateList(examTypeListData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getExamTypeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    public void checkExamTypeToServer(final ExamType updateObj, final String name, final boolean saveOrNot, final boolean isFromAdapter) {
        if (isFromAdapter)
            progressbar.show();

        Map<String, String> param = new LinkedHashMap<String, String>();
        try {
            param.put("Exam_Type", URLEncoder.encode(  name.replaceAll(" ","_")   ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String feeCheck = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ExamType.php?" +
                "data=" + (new JSONObject(param1)) + "&databaseName=" + dbName;
        Log.e("ExamTypeCheck", feeCheck);

        StringRequest request = new StringRequest(Request.Method.GET,
                feeCheck
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("ExamTypeCheck", s);
                        checkingKey = false;
                        if (progressbar.isShowing())
                            progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            et_class_name.setError(null);
                            if (jsonObject.optInt("code") == 200) {
                                isNameAlreadyExist = true;
                                if (progressbar.isShowing()) {
                                    progressbar.cancel();
                                }
                                if (isFromAdapter) {
                                    Snackbar snackbarObj = Snackbar.make(snackbar,
                                            getString(R.string.exam_type_already_exist),
                                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    });
                                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                    snackbarObj.show();
                                } else
                                    et_class_name.setError(getString(R.string.exam_type_already_exist));



                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                isNameAlreadyExist = false;


                                if (isFromAdapter)
                                    updateExamType( updateObj, name);
                                else if (saveOrNot)
                                    addExamTypeToServer(name, true);


/*
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("GradeKey", volleyError.toString());
                if (progressbar.isShowing())
                    progressbar.cancel();
                checkingKey = false;
                isNameAlreadyExist = false;
                if (progressbar.isShowing())
                    progressbar.cancel();

                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        request.setTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).cancelRequestByTag("FEE_NAME");
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request, "FEE_NAME");
   }

    private void  updateExamType(ExamType updateObj, String name) {
        progressbar.show();

        final Map<String, String> param = new LinkedHashMap<String, String>();

        try {
            param.put("Exam_Type", URLEncoder.encode( name.replaceAll(" ","_") ,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Exam_Type",updateObj.getExam_Type().replaceAll(" ","_"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String updateClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ExamType.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("Exam_Type", updateClassUrl);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateClassUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResultMaster", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                getExamTypeListFromServer();

                                Toast.makeText(getActivity(),
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                               /* alert.setMessage(jsonObject.optString("message"));
                                alert.show();*/

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultExam_Type", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }


    public void addExamTypeToServer(String feeName, boolean saveOrNot) throws JSONException {
        progressbar.show();
        if (saveOrNot) {

            progressbar.show();
            if (!checkingKey) {
                if (isNameAlreadyExist) {
                    Snackbar snackbarObj = Snackbar.make(snackbar,
                            getString(R.string.exam_type_already_exist),
                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                    snackbarObj.show();

                } else {
                    final JSONObject param = new JSONObject();

                    param.put("Exam_Type", feeName.trim().toUpperCase());
                    param.put("Exam_Status", "Active");
                    param.put("Remarks", "");
                    param.put("Duration", "");


                    String addClassUrl = sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            + "ExamType.php?databaseName=" + dbName;
                    Log.d("addExamTypeUrl", addClassUrl);
                    Log.d("param", param.toString());

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,  addClassUrl,param  ,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {
                                    DbHandler.longInfo(jsonObject.toString());
                                    Log.e("ResultExamType", jsonObject.toString());
                                    progressbar.cancel();

                                        if (jsonObject.optString("code").equalsIgnoreCase("201") ||
                                                jsonObject.optString("code").equalsIgnoreCase("200")) {

                                            getExamTypeListFromServer();


                                            Toast.makeText(getActivity(),
                                                    jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                                ||
                                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                                            if (!logoutAlert & !(getActivity()).isFinishing()) {
                                                Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                                logoutAlert = true;
                                            }
                                        }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                            Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                                        else {
                                           /* alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();*/
                                            Snackbar snackbarObj = Snackbar.make(snackbar,
                                                    jsonObject.optString("message"),
                                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                }
                                            });
                                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                            snackbarObj.show();

                                        }




                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("ResultGrade", volleyError.toString());
                            Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

                            progressbar.cancel();
                        }
                    }) ;

                    request.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    request.setTag(getTag());
                    AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

                }


            }

        } else {
            checkExamTypeToServer(new ExamType(),
                    et_class_name.getText().toString().trim(), true/* for save ExamType name*/,
                    false/* not from adapter*/);

        }

    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void  deleteExamType(ExamType examType) {
        progressbar.show();

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Exam_Type", URLEncoder.encode(examType.getExam_Type() ,"utf-8"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        //  param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        String deleteNotiUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ExamType.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));

        Log.d("deleteExamTypeUrl", deleteNotiUrl);

        Log.e("deleteExamType", new JSONObject(param_main).toString());

        StringRequest request = new StringRequest(Request.Method.DELETE,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                deleteNotiUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("deleteExamType", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                             getExamTypeListFromServer();
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {

                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("deleteResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }
}
