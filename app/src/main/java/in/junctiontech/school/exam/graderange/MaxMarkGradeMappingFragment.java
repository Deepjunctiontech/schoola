package in.junctiontech.school.exam.graderange;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.exam.Grade;
import in.junctiontech.school.managefee.AnotherActivity;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by JAYDEVI BHADE on 30-03-2017.
 */

public class MaxMarkGradeMappingFragment extends
        Fragment {


    private FloatingActionButton btn_add_class;
    //  private RecyclerView recycler_view_school_class_list;
    //  private boolean isAnyGreaterNumber = false;

    private boolean classNameAlreadyRegister = false;
    private RelativeLayout snackbar;
    private SharedPreferences sp;
    private Gson gson;
    private String dbName;
    private ProgressDialog progressbar;
    private boolean logoutAlert;
    private ArrayList<MasterEntry> gradeListData = new ArrayList<>();
    private ArrayList<Grade> marksGradeListData = new ArrayList<>();
    private boolean checkingKey;
    private boolean isNameAlreadyExist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<MasterEntry> resultListData = new ArrayList<>();
    private ArrayList<String> gradeStringListData = new ArrayList<>();
    private ArrayList<String> resultStringListData = new ArrayList<>();
    private String maxMarks = "";
    private LinearLayout ll_fragment_item_list, ll_fragment_range_title;
    private Button btn_fragment_save;
    private boolean isNewEntry = true;
    // private ArrayList<String> alreadyExistGrade = new ArrayList<>();
    private String lastEnterMarksString = "0";
    private boolean isCurrentlyFetchingData =false;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_list_and_one_button, container, false);
        convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

        snackbar = (RelativeLayout) convertView.findViewById(R.id.ll_fragment_list_and_one_button);
        btn_fragment_save = (Button) convertView.findViewById(R.id.btn_fragment_save);
        btn_fragment_save.setVisibility(View.VISIBLE);
        btn_fragment_save.setBackgroundColor(appColor);
        ((TextView)convertView.findViewById(R.id.tv_fragment_list_btn_result_title)).setTextColor(appColor);
        ((TextView)convertView.findViewById(R.id.tv_fragment_list_btn_rang_end_title)).setTextColor(appColor);
        ((TextView)convertView.findViewById(R.id.tv_fragment_list_btn_rang_start_title)).setTextColor(appColor);
        ((TextView)convertView.findViewById(R.id.tv_fragment_list_btn_grade_title)).setTextColor(appColor);

       /* (
                (FloatingActionButton) convertView.findViewById(R.id.fab_fragment_add)
        ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewRow();
            }
        });*/
        (  convertView.findViewById(R.id.fab_fragment_add)
        ).setVisibility(View.INVISIBLE);

        ll_fragment_item_list = (LinearLayout) convertView.findViewById(R.id.ll_fragment_item_list);
        ll_fragment_range_title = (LinearLayout) convertView.findViewById(R.id.ll_fragment_range_title);
        ll_fragment_range_title.setVisibility(View.VISIBLE);
        btn_fragment_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkFields() && gradeListData.size() > 0 && resultListData.size() > 0) {
                    final JSONArray jsonArrayInsert = new JSONArray();
                    final JSONArray jsonArrayUpdate = new JSONArray();
                    final JSONArray jsonArrayDelete = new JSONArray();
                    for (Grade gradeObj : marksGradeListData) {
                        if (gradeObj.getGradeID().equalsIgnoreCase("")) {
                            if (!(gradeObj.getRangeEnd() == null || gradeObj.getRangeStart() == null) &&

                                    !(gradeObj.getRangeEnd().equalsIgnoreCase("") || gradeObj.getRangeStart().equalsIgnoreCase(""))) {
                                final Map<String, String> param = new LinkedHashMap<String, String>();
                                param.put("maxMarks", gradeObj.getMaxMarks());
                                param.put("gradeStatus", "Active");
                                param.put("rangeStart", gradeObj.getRangeStart());
                                param.put("rangeEnd", gradeObj.getRangeEnd());
                                param.put("grade", gradeObj.getGrade());
                                param.put("result", gradeObj.getGradeResult());
                                param.put("gradeCreatedBy", "0");
                                jsonArrayInsert.put(new JSONObject(param));
                            }
                        } else {
                            if (gradeObj.getRangeEnd().equalsIgnoreCase("") ||
                                    gradeObj.getRangeStart().equalsIgnoreCase("")
                                    ||(gradeObj.getRangeEnd().equalsIgnoreCase("0")
                                    && gradeObj.getRangeStart().equalsIgnoreCase("0"))) {
                                JSONObject job_filter = new JSONObject();
                                try {
                                    job_filter.put("gradeID", gradeObj.getGradeID());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                                param_main.put("filter", job_filter);
                                jsonArrayDelete.put((new JSONObject(param_main)));
                            } else {

                                final Map<String, String> param = new LinkedHashMap<String, String>();
                                param.put("rangeStart", gradeObj.getRangeStart());
                                param.put("rangeEnd", gradeObj.getRangeEnd());
                                param.put("result", gradeObj.getGradeResult());
                                param.put("grade", gradeObj.getGrade());
                                JSONObject job_filter = new JSONObject();
                                try {
                                    job_filter.put("gradeID", gradeObj.getGradeID());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                                param_main.put("data", new JSONObject(param));
                                param_main.put("filter", job_filter);
                                jsonArrayUpdate.put((new JSONObject(param_main)));
                            }
                        }

                    }
                    if (jsonArrayInsert.length() > 0)
                        addGradeMarksToServer(jsonArrayInsert);
                    if (jsonArrayUpdate.length() > 0)
                        updateGradeMarkToServer(jsonArrayUpdate);

                    if (jsonArrayDelete.length() > 0)
                        deleteGradeMarkToServer(jsonArrayDelete);
                } else showSnack();
            }
        });


        if (!this.getArguments().getBoolean("isNewEntry")) {
            marksGradeListData = (ArrayList<Grade>) this.getArguments().getSerializable("dataList");
            Collections.sort(marksGradeListData, new Comparator<Grade>() {
                @Override
                public int compare(Grade o1, Grade o2) {
                    String lid = o1.getRangeStart();
                    String rid = o2.getRangeStart();

                    try {
                        int ld = Integer.parseInt(lid);
                        int rd = Integer.parseInt(rid);
                        //  Log.e("ArrayListSort","INTEGER");
                        if (ld > rd)
                            return -1;
                        else if (ld < rd)
                            return 1;
                        else
                            return 0;

                    } catch (NumberFormatException e) {
                        // Log.e("ArrayListSort","String");
                        return lid.compareTo(rid);
                    }

                    //  return lid.compareToIgnoreCase(rid);

                }
            });


            isNewEntry = false;
        }

        setHasOptionsMenu(true);
        return convertView;
    }

    private boolean checkFields() {

        for (Grade gradeObj : marksGradeListData) {
            if (filterGradeExistence(marksGradeListData, gradeObj.getGrade()).size() != 1) {
                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.you_entered_some_duplicate_grades_value)
                        ,
                        Snackbar.LENGTH_LONG).setAction("",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.spinner_marks));
                snackbarObj.show();
                return false;
            } else if (checkMaximumMarksExistence(marksGradeListData)) {

                return false;
            }
        }


        return true;
    }

    private boolean checkMaximumMarksExistence(ArrayList<Grade> models) {

        for (Grade model : models) {

            if (Integer.parseInt(maxMarks) < Integer.parseInt(model.getRangeStart().equalsIgnoreCase("") ? "0" : model.getRangeStart())
                    ||
                    Integer.parseInt(maxMarks) < Integer.parseInt(model.getRangeEnd().equalsIgnoreCase("") ? "0" : model.getRangeEnd())
                    ) {
                Snackbar snackbarObj = Snackbar.make(
                        snackbar,
                        getString(R.string.marks) + " " +
                                getString(R.string.should_not_be_greater_than_maximum_marks) + " i.e. " + maxMarks
                        ,
                        Snackbar.LENGTH_LONG).setAction("",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.spinner_marks));
                snackbarObj.show();
                return true;
            }
        }
        return false;
    }

    private ArrayList<Grade> filterGradeExistence(ArrayList<Grade> models, String query) {
        query = query.toLowerCase();
        final ArrayList<Grade> filteredModelList = new ArrayList<>();

        for (Grade model : models) {

            if (model.getGrade().toLowerCase().equalsIgnoreCase(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


    private void addNewRow() {
        Log.e("lastEnterMarksString", lastEnterMarksString + "  ritu");
        for (MasterEntry masterEntry : gradeListData) {
            if (filterGradeExistence(marksGradeListData, masterEntry.getMasterEntryId()).size() != 1) {

                marksGradeListData.add(new Grade("",
                                masterEntry.getMasterEntryId(),
                                masterEntry.getMasterEntryValue(),
                                resultListData.get(0).getMasterEntryId(),
                                resultListData.get(0).getMasterEntryValue(),
                                (lastEnterMarksString.equalsIgnoreCase("") ||
                                        lastEnterMarksString.equalsIgnoreCase("0")) ? "" :
                                        (Integer.parseInt(lastEnterMarksString) - 1) + ""

                                ,
                                "0",
                                maxMarks
                        )
                );

                break;
            }
        }
        setupList();
    }

    private void deleteGradeMarkToServer(JSONArray jsonArrayDelete) {

        progressbar.show();

        String deleteData = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "Grade.php?databaseName=" + dbName +
                "&data=" + jsonArrayDelete.toString();

        Log.e("deleteGradeMark", deleteData);

        StringRequest request = new StringRequest(Request.Method.DELETE,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                deleteData
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("deleteResultGrade", s);
                        progressbar.cancel();
                        getActivity().onBackPressed();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("deleteResultGrade", volleyError.toString());
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
                progressbar.cancel();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }

    private void updateGradeMarkToServer(JSONArray jsonArray) {

        progressbar.show();


        String updateClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?databaseName=" + dbName +
                "&data=" + jsonArray;

        Log.d("GradePut", updateClassUrl);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                updateClassUrl

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateResultGrade", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
                                //   getGradeListFromServer();

                                Toast.makeText(getActivity(),
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                              /*  Snackbar snackbarObj= Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                                snackbarObj.show();*/
                            }else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                               /* alert.setMessage(jsonObject.optString("message"));
                                alert.show();*/

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);

    }

    private void setupList() {

        int position = 0;
        if (ll_fragment_item_list != null) {
            ll_fragment_item_list.removeAllViews();

            for (int i=0;i<marksGradeListData.size();i++) {
             final    Grade obj = marksGradeListData.get(i);
                final View itemView = getActivity().getLayoutInflater().inflate(R.layout.item_max_mark_grade_mapping, null);

                Spinner spinner_item_grades_list = (Spinner) itemView.findViewById(R.id.spinner_item_grades_list);
                Spinner spinner_item_result_list = (Spinner) itemView.findViewById(R.id.spinner_item_result_list);

                //spinner_item_grades_list.setEnabled(false);

                final EditText et_item_range_start_list = (EditText) itemView.findViewById(R.id.et_item_range_start_list);
                final EditText et_item_range_end_list = (EditText) itemView.findViewById(R.id.et_item_range_end_list);

                et_item_range_start_list.setText(obj.getRangeStart());
                et_item_range_end_list.setText(obj.getRangeEnd());

                lastEnterMarksString = obj.getRangeEnd();

                obj.setRangeStart(obj.getRangeStart());
                obj.setRangeEnd(obj.getRangeEnd());

                ArrayAdapter<String> gradeListAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, gradeStringListData);
                gradeListAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
                spinner_item_grades_list.setAdapter(gradeListAdapter);
                spinner_item_grades_list.setBackgroundColor(appColor);
               // spinner_item_grades_list.setPopupBackgroundDrawable(new ColorDrawable((appColor & appColor) << 1));



                int gradPos = 0;
                for (String gradeValue : gradeStringListData) {
                    Log.e("RITU_gradeValue",gradeValue);
                    Log.e("RITU_obj_gradeGrade",obj.getGradeValue());
                  //  Log.e("RITU_obj_gradeValue",obj.getGrade());
                  //  Log.e("RITU_obj_gradeResult",obj.getGradeResult()    );
//                    Log.e("RITU_obj_gradeStatus",obj.getGradeStatus()    );

                    if (gradeValue.equalsIgnoreCase(obj.getGradeValue())) {
                        Log.e("RITU_Match","yes"+position);
                        spinner_item_grades_list.setSelection(gradPos);
                        break;
                    }
                    gradPos++;
                }
                // (  (TextView)spinner_item_grades_list.getSelectedView()).setTextColor(getActivity().getResources().getColor(R.color.black));

                ArrayAdapter<String> resultListAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, resultStringListData);
                resultListAdapter .setDropDownViewResource(R.layout.myspinner_dropdown_item);
                spinner_item_result_list.setAdapter(gradeListAdapter);
                spinner_item_result_list.setBackgroundColor(appColor);
               // spinner_item_result_list.setPopupBackgroundDrawable(new ColorDrawable((appColor & 0x7f7f7f) << 2));

                spinner_item_result_list.setAdapter(resultListAdapter);

                gradPos = 0;
                for (String resultValue : resultStringListData) {
                    if (resultValue.equalsIgnoreCase(obj.getResultValue())) {
                        spinner_item_result_list.setSelection(gradPos);
                        break;
                    }
                    gradPos++;
                }

                et_item_range_start_list.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        if ((s + "").equalsIgnoreCase(""))
                            s = "0";
                        obj.setRangeStart(s.toString().trim());

                        if (Integer.parseInt(obj.getMaxMarks()) < Integer.parseInt(s + "")) {
                            et_item_range_start_list.setError(getString(R.string.should_not_be_greater_than_maximum_marks)
                                    + " i.e. " + obj.getMaxMarks());

                        }


                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                final int finalPosition = position;
                et_item_range_end_list.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if ((s + "").equalsIgnoreCase(""))
                            s = "0";
                        obj.setRangeEnd(s.toString().trim());

                        if (Integer.parseInt(obj.getMaxMarks()) < Integer.parseInt(s + "")) {
                            et_item_range_end_list.setError(getString(R.string.should_not_be_greater_than_maximum_marks)
                                    + " i.e. " + obj.getMaxMarks());
                        } else {
                            if (finalPosition + 1 == marksGradeListData.size())
                                lastEnterMarksString = s.toString().trim();
                        }


                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                spinner_item_grades_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obj.setGrade(gradeListData.get(position).getMasterEntryId());
                        obj.setGradeValue(gradeListData.get(position).getMasterEntryValue());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinner_item_result_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        obj.setGradeResult(resultListData.get(position).getMasterEntryId());
                        obj.setResultValue(resultListData.get(position).getMasterEntryValue());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                ll_fragment_item_list.addView(itemView);
                position++;
            }
            //showSnack();
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        maxMarks = getTag();
        appColor=  Config.getAppColor(getActivity(),false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sp = Prefs.with(getActivity()).getSharedPreferences();

        gson = new Gson();
        dbName = sp.getString("organization_name", "");
        progressbar = new ProgressDialog(getActivity());
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        setupList();
        getGradeListFromServer();
        getResultListFromServer();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");


                    if (gradeListData.size() == 0)
                        getGradeListFromServer();


                }
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void updateList() {
        isCurrentlyFetchingData = false;
        gradeStringListData = new ArrayList<>();
        resultStringListData = new ArrayList<>();
        for (MasterEntry mas : gradeListData) {
            gradeStringListData.add(mas.getMasterEntryValue());
        }
        for (MasterEntry mas : resultListData) {
            resultStringListData.add(mas.getMasterEntryValue());
        }

        if (isNewEntry) {
            marksGradeListData = new ArrayList<>();
            int i = 0;
            if (resultListData.size() > 0)
                for (MasterEntry masterEntry : gradeListData) {
                    marksGradeListData.add(new Grade("", masterEntry.getMasterEntryId(),
                                    masterEntry.getMasterEntryValue(),
                                    resultListData.get(0).getMasterEntryId(),
                                    resultListData.get(0).getMasterEntryValue(),
                                    i == 0 ? maxMarks : "",
                                    "0",
                                    maxMarks
                            )
                    );
                    i++;
                    break;
                }
        } /*else {
            if (resultListData.size() > 0)

                // for (MasterEntry masterEntry : gradeListData) {

                // int pos = 1;
                for (Grade oldGradeDataObj : marksGradeListData) {
                    alreadyExistGrade.add(oldGradeDataObj.getGradeValue());
                        *//*   if (oldGradeDataObj.getGrade().equalsIgnoreCase(masterEntry.getMasterEntryId()))

                        break;
                        else if (pos == marksGradeListData.size()) {
                            alreadyExistGrade.add(masterEntry.getMasterEntryId());
                            marksGradeListData.add(new Grade("", masterEntry.getMasterEntryId(),
                                            masterEntry.getMasterEntryValue(),
                                            resultListData.get(0).getMasterEntryId(),
                                            resultListData.get(0).getMasterEntryValue(),
                                            "",
                                            "",
                                            maxMarks
                                    )
                            );
                        }*//*
                    //   pos++;
                }
            // }

        }*/

        Log.e("UpdateList", " ritu");


        setupList();

    }

    public void performClick() {
        btn_add_class.performClick();
    }



    public void getGradeListFromServer() {
        isCurrentlyFetchingData =true;
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("MasterEntryName", "Grade");

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param);

        Log.d("fetchGradeList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        isCurrentlyFetchingData = false;
                        DbHandler.longInfo(s);
                        Log.e("getGradeData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                gradeListData = new ArrayList<>();
                                gradeListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                showSnack();
                            }


                            updateList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isCurrentlyFetchingData = false;
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void showSnack() {
        if (gradeListData.size() == 0 && !isCurrentlyFetchingData) {
            Snackbar snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.grade) + " " + getString(R.string.not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_scholastic_grades),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_GRADES_PERMISSION, false)) {
                                Toast.makeText(getActivity(), "Permission not available : CREATE GRADES !", Toast.LENGTH_SHORT).show();
                            } else {
                                startActivity(new Intent(getActivity(), AnotherActivity.class).putExtra("id", "createGrades"));
                            getActivity().finish();
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });

            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }
    }

    public void getResultListFromServer() {
        isCurrentlyFetchingData = true;
        progressbar.show();
        final Map<String, Object> param_main = new LinkedHashMap<String, Object>();
        param_main.put("MasterEntryName", "Result");

        final Map<String, JSONObject> param = new LinkedHashMap<String, JSONObject>();
        param.put("filter", new JSONObject(param_main));

        String fetchClassList = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + new JSONObject(param);

        Log.d("fetchResultList", fetchClassList);
        StringRequest request = new StringRequest(Request.Method.GET,
                fetchClassList

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        DbHandler.longInfo(s);
                        Log.e("getResultData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = gson.fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                resultListData = new ArrayList<>();
                                resultListData = obj.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        getString(R.string.data_not_available),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();
                            }

                            updateList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isCurrentlyFetchingData = false;
                Log.e("getGradeData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(getTag());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);
    }

    public void addGradeMarksToServer(final JSONArray jsonArray) {
        progressbar.show();
        String addClassUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Grade.php?databaseName=" + dbName;
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("data",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("addGradeUrl", addClassUrl);
        Log.d("param", jsonObject1.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,  addClassUrl, jsonObject1,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("ResultGradeFee",jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optString("code").equalsIgnoreCase("201") ||
                                    jsonObject.optString("code").equalsIgnoreCase("200")) {

                                getActivity().onBackPressed();


                                Toast.makeText(getActivity(),
                                        jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(getActivity()).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(getActivity(), jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler( getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min),snackbar);
                            else {
                                           /* alert.setMessage(jsonObject.optString("message"));
                                            if (!getActivity().isFinishing())
                                                alert.show();*/
                                Snackbar snackbarObj = Snackbar.make(snackbar,
                                        jsonObject.optString("message"),
                                        Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                                snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                                snackbarObj.show();

                            }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ResultGrade", volleyError.toString());
                Config.responseVolleyErrorHandler(getActivity(),volleyError,snackbar);

                progressbar.cancel();
            }
        })  ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(getTag());

        Log.e("ResultGrade", jsonArray.toString());
        AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(request);


    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id== R.id.menu_add_new_data) {

addNewRow();
        }

        return super.onOptionsItemSelected(item);
    }

}
