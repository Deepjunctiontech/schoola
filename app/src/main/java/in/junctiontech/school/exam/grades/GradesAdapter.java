package in.junctiontech.school.exam.grades;

import android.content.Context;
import android.content.DialogInterface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by JAYDEVI BHADE on 29-03-2017.
 */

public class GradesAdapter extends RecyclerView.Adapter<GradesAdapter.MyViewHolder> {
    private final CreateGradesFragment   createGradesFragment;
    private final int  appColor;
    private ArrayList<MasterEntry> gradeList;
    private Context context;

    public GradesAdapter(Context context, ArrayList<MasterEntry> gradeList, CreateGradesFragment createGradesFragment, int appColor) {
        this.gradeList = gradeList;
        this.context = context;
        this.createGradesFragment =createGradesFragment;
        this.appColor = appColor;
    }


    public void updateList(ArrayList<MasterEntry> gradeList){
        this.gradeList = gradeList;
        notifyDataSetChanged();
    }

    @Override
    public GradesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_view, parent, false);

        return new MyViewHolder(view);
    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public void onBindViewHolder(final GradesAdapter.MyViewHolder holder, final int position) {
        final MasterEntry gradeObj = gradeList.get(position);
        holder.tv_item_name.setText(gradeObj.getMasterEntryValue().toUpperCase());


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return gradeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_class_name);
            tv_item_name.setTextColor(appColor);
           itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateGrade(gradeList.get(getLayoutPosition()));
                }
            });



            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
        }
    }
    public void setFilter(ArrayList<MasterEntry> gradeList) {
        this.gradeList = new ArrayList<>();
        this.gradeList.addAll(gradeList);
        notifyDataSetChanged();
    }

    public void updateGrade(final MasterEntry gradeObj){
        android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(context, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setIcon(context.getResources().getDrawable(R.drawable.ic_edit));
        alertUpdate.setTitle(context.getString(R.string.update) + " " +
                context.getString(R.string.grade));

        final EditText et_name = new EditText(context);
        et_name.setText(gradeObj.getMasterEntryValue());

        LinearLayout linearLayout= new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        linearLayout.addView(et_name);

        linearLayout.setPadding(15,10,15,10);

        alertUpdate.setView(linearLayout);
        alertUpdate.setCancelable(false);


        alertUpdate.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_name.getText().toString().trim().length() == 0) {
                    Toast.makeText(context, context.getString(R.string.name_can_not_be_blank), Toast.LENGTH_SHORT).show();
                } else {
                    createGradesFragment.checkGradeToServer(gradeObj ,et_name.getText().toString().trim(),true, true/* its comes from adapter */);

                }

            }
        });
        alertUpdate.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertUpdate.show();
    }

}
