package in.junctiontech.school.updatestudent;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.R;
import in.junctiontech.school.models.RegisteredStudent;

/**
 * Created by Administrator on 11/3/2016.
 */

public class ParentDetailFragment extends
        Fragment {


    private RegisteredStudent studentObj;
    private EditText et_student_data_father_name, et_student_data_mother_name,et_student_data_father_designation,
    et_student_data_mother_designation, et_student_data_father_qualification, et_student_data_mother_qualification,
    et_student_data_father_occupation, et_student_data_mother_occupation, et_student_data_father_organization,
    et_student_data_mother_organization;
    private Button btn__student_data_father_dob, btn_student_data_mother_dob;
    private int appColor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_student_parent_detail, container, false);
        ((TextView) convertView.findViewById(R.id.btn__student_data_father_dob_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.btn_student_data_mother_dob_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_name_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_designation_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_qualification_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_occupation_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_organization_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_name_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_designation_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_qualification_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_occupation_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_organization_title)).setTextColor(appColor);

        Button  btn_student_data_parent_update=(Button)  (convertView.findViewById(R.id.btn_student_data_parent_update));
        btn_student_data_parent_update.setBackgroundColor(appColor);
        btn_student_data_parent_update .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((UpdateStudentActivity) getActivity()).updateStudentBasicDetailData(2/*for Parent detail*/);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        et_student_data_father_name = (EditText) convertView.findViewById(R.id.et_student_data_father_name);
        et_student_data_mother_name = (EditText) convertView.findViewById(R.id.et_student_data_mother_name);
        et_student_data_father_designation = (EditText) convertView.findViewById(R.id.et_student_data_father_designation);
        et_student_data_mother_designation = (EditText) convertView.findViewById(R.id.et_student_data_mother_designation);
        et_student_data_father_qualification = (EditText) convertView.findViewById(R.id.et_student_data_father_qualification);
        et_student_data_mother_qualification = (EditText) convertView.findViewById(R.id.et_student_data_mother_qualification);
        et_student_data_father_occupation = (EditText) convertView.findViewById(R.id.et_student_data_father_occupation);
        et_student_data_mother_occupation = (EditText) convertView.findViewById(R.id.et_student_data_mother_occupation);
        et_student_data_father_organization = (EditText) convertView.findViewById(R.id.et_student_data_father_organization);
        et_student_data_mother_organization = (EditText) convertView.findViewById(R.id.et_student_data_mother_organization);

        btn__student_data_father_dob = (Button) convertView.findViewById(R.id.btn__student_data_father_dob);
        btn_student_data_mother_dob = (Button) convertView.findViewById(R.id.btn_student_data_mother_dob);

        btn__student_data_father_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenersession_fDob, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
        btn_student_data_mother_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenersession_mDob, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });


        et_student_data_father_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherName(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherName(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_designation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherDesignation(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_designation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherDesignation(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_qualification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherQualification(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_qualification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherQualification(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_occupation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherOccupation(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_occupation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherOccupation(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_organization.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherOrganization(s.toString().trim().replaceAll(" ","_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_organization.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherOrganization(s.toString().trim().replaceAll(" ","_"));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return convertView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
               studentObj = ((UpdateStudentActivity) getActivity()).getRegisterStudentData();
        appColor = ((UpdateStudentActivity) getActivity()).getAppColor();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        et_student_data_father_name.setText(studentObj.getFatherName().replaceAll("_"," "));
        et_student_data_mother_name.setText(studentObj.getMotherName().replaceAll("_"," "));
        et_student_data_father_designation.setText(studentObj.getFatherDesignation().replaceAll("_"," "));
        et_student_data_mother_designation.setText(studentObj.getMotherDesignation().replaceAll("_"," "));
        et_student_data_father_qualification.setText(studentObj.getFatherQualification().replaceAll("_"," "));
        et_student_data_mother_qualification.setText(studentObj.getMotherQualification().replaceAll("_"," "));
        et_student_data_father_occupation.setText(studentObj.getFatherOccupation().replaceAll("_"," "));
        et_student_data_mother_occupation.setText(studentObj.getMotherOccupation().replaceAll("_"," "));
        et_student_data_father_organization.setText(studentObj.getFatherOrganization().replaceAll("_"," "));
        et_student_data_mother_organization.setText(studentObj.getMotherOrganization().replaceAll("_"," "));

        if (studentObj.getFatherDateOfBirth() != null)
            ((UpdateStudentActivity) getActivity()).convertStringToDate("F", studentObj.getFatherDateOfBirth());

        if (studentObj.getMotherDateOfBirth() != null)
            ((UpdateStudentActivity) getActivity()).convertStringToDate("M", studentObj.getMotherDateOfBirth());

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public DatePickerDialog.OnDateSetListener myDateListenersession_fDob = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn__student_data_father_dob.setText(year + "-" + new_month + "-" + new_day);
            ((UpdateStudentActivity) getActivity()).convertDateOfBirthToString("F",year + "-" + new_month + "-" + new_day,0);
        }
    };
    public DatePickerDialog.OnDateSetListener myDateListenersession_mDob = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_student_data_mother_dob.setText(year + "-" + new_month + "-" + new_day);
            ((UpdateStudentActivity) getActivity()).convertDateOfBirthToString("M",year + "-" + new_month + "-" + new_day,0);
        }
    };

    public void setFatherDateOfBirth(String fatherDateOfBirth) {
       btn__student_data_father_dob.setText(fatherDateOfBirth);
    }

    public void setMotherDateOfBirth(String motherDateOfBirth) {
      btn_student_data_mother_dob.setText(motherDateOfBirth);
    }
}
