package in.junctiontech.school.updatestudent;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.models.Qualification;
import in.junctiontech.school.models.RegisteredStudent;
import in.junctiontech.school.models.Sibling;


/**
 * Created by JAYDEVI BHADE on 11/3/2016.
 */

public class UpdateStudentActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private String dbName;
    private Gson gson;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private BasicDetailFragment fragmentBasicDetail;
    private ContactDetailFragment fragmentContactDetail;
    private ParentDetailFragment fragmentParentDetail;
    private QualificationFragment fragmentQualification;
    private SiblingInfoFragment fragmentSiblingInfo;
    private RegisteredStudent studentObj;
    private ArrayList<CreateClass> classListData;
    private ArrayList<MasterEntry> categoryListData, casteListData, genderTypeData, bloodGroupListData;
    private boolean updation = false;
    private ArrayList<Qualification> qualificationList = new ArrayList<>();
    private Type type, typeSibling;
    private ArrayList<Sibling> siblingList = new ArrayList<>();
    private ImageView iv_click_here, iv_swape_pager;
    private Runnable r;
    private Handler handler;
    private boolean isDialogShowing = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean logoutAlert = false;
    private FrameLayout snackbar;


    private int colorIs;

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        //  getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        tabLayout.setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //  LanguageSetup.changeLang(this, sp.getString("app_language", ""));

        setContentView(R.layout.activity_create_student2);
        snackbar = (FrameLayout) findViewById(R.id.snackbar_view_pager);

        dbName = sp.getString("organization_name", "");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gson = new Gson();
        type = new TypeToken<Qualification>() {
        }.getType();
        typeSibling = new TypeToken<Sibling>() {
        }.getType();

        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        setColorApp();
        setupTabIcons();

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);

        studentObj = (RegisteredStudent) getIntent().getSerializableExtra("data");
        classListData = (ArrayList<CreateClass>) getIntent().getSerializableExtra("classList");
        genderTypeData = (ArrayList<MasterEntry>) getIntent().getSerializableExtra("genderList");
        casteListData = (ArrayList<MasterEntry>) getIntent().getSerializableExtra("casteList");
        categoryListData = (ArrayList<MasterEntry>) getIntent().getSerializableExtra("categoryList");
        bloodGroupListData = (ArrayList<MasterEntry>) getIntent().getSerializableExtra("bloodGroupList");

        getQualificationListFromServer();
        getSiblingListFromServer();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1 || position == 2 || position == 3 || position == 4) {
                   /* iv_swape_pager.setAnimation(null);
                    iv_swape_pager.setVisibility(View.GONE);*/
                    sp.edit().putBoolean(Config.SWAP_UPDATE_STUDENT, false).commit();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (qualificationList.size() == 0)
                        getQualificationListFromServer();

                    if (siblingList.size() == 0)
                        getSiblingListFromServer();


                }
            }
        };
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    public void getQualificationListFromServer() {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Type", "Student");
        param.put("UniqueId", studentObj.getRegistrationId());

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        Log.e("filter", (new JSONObject(param1).toString()));

        String qualGet = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "QualificationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param1));

        Log.d("qualGet", qualGet);
        StringRequest request = new StringRequest(Request.Method.GET,
                qualGet
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("Qualification", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                Qualification obj = gson.fromJson(s, type);
                                qualificationList = obj.getResult();
                                // fragmentQualification.updateList(qualificationList);
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Qualification", volleyError.toString());
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    public ArrayList<MasterEntry> getCasteListData() {
        return casteListData;
    }

    public ArrayList<MasterEntry> getCategoryListData() {
        return categoryListData;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.login);
        tabLayout.getTabAt(1).setIcon(android.R.drawable.ic_menu_call);
        tabLayout.getTabAt(2).setIcon(R.drawable.aboutusnew);
        tabLayout.getTabAt(3).setIcon(android.R.drawable.ic_menu_my_calendar);
        tabLayout.getTabAt(4).setIcon(android.R.drawable.ic_menu_my_calendar);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragmentBasicDetail = new BasicDetailFragment();
        fragmentContactDetail = new ContactDetailFragment();
        fragmentParentDetail = new ParentDetailFragment();
        fragmentQualification = new QualificationFragment();
        fragmentSiblingInfo = new SiblingInfoFragment();
        adapter.addFragment(fragmentBasicDetail, getString(R.string.basic_details));
        adapter.addFragment(fragmentContactDetail, getString(R.string.contact_detail));
        adapter.addFragment(fragmentParentDetail, getString(R.string.parent_detail));
        adapter.addFragment(fragmentQualification, getString(R.string.qualification));
        adapter.addFragment(fragmentSiblingInfo, getString(R.string.sibling_info));

        viewPager.setAdapter(adapter);

    }

    public ArrayList<CreateClass> getClassData() {
        return classListData;
    }

    public ArrayList<MasterEntry> getGenderTypeData() {
        Log.e("getGenderTypeData", "getGenderTypeData");
        return genderTypeData;
    }

    public RegisteredStudent getRegisterStudentData() {
        return studentObj;
    }

    public ArrayList<MasterEntry> getBloodGroupListData() {
        return bloodGroupListData;
    }

    public void convertStringToDate(final String keyString, String dor) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
       if (!dor.equalsIgnoreCase("")){
        progressbar.show();
        String convStrToTime = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=stringToDate&date=" +
                dor;
        Log.d("convStrToTime", convStrToTime);
        StringRequest request = new StringRequest(Request.Method.GET,
                convStrToTime
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateResult", s);
                        progressbar.dismiss();
                        try {
                            s=    new JSONObject(s).optString("result");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (!isFinishing()) {
                            switch (keyString) {
                                case "S":
                                    fragmentBasicDetail.setDateOfBirth(s);
                                    break;

                                case "F":
                                    fragmentParentDetail.setFatherDateOfBirth(s);
                                    break;
                                case "M":
                                    fragmentParentDetail.setMotherDateOfBirth(s);
                                    break;
                                case "R":
                                    fragmentBasicDetail.setDateOfRegistration(s);
                                    break;
                            }
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

     }

    }

    public void convertDateOfBirthToString(final String keyString, String dob, final int pos) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        String convStrToDate = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                dob;
        Log.d("convStrToDate", convStrToDate);
        StringRequest request = new StringRequest(Request.Method.GET,
                convStrToDate
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
                        progressbar.dismiss();

                        if (!isFinishing()) {
                            switch (keyString) {
                                case "S":
                                    studentObj.setDOB(s);
                                    break;
                                case "F":
                                    studentObj.setFatherDateOfBirth(s);
                                    break;
                                case "M":
                                    studentObj.setMotherDateOfBirth(s);
                                    break;
                                case "C":
                                    siblingList.get(pos).setSDOB(s);
                                    fragmentSiblingInfo.updateList(siblingList);
                                    break;
                            }

                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }

    }

    public void updateStudentData() {

        if (studentObj.getStudentName().equalsIgnoreCase("")) {

            alert.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + " : " + getString(R.string.student_name));
            alert.show();
            isDialogShowing = true;
        } else {
            progressbar.show();


            JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("RegistrationId", studentObj.getRegistrationId());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
            try {
                if (studentObj.getFatherEmail() != null)
                    studentObj.setFatherEmail(studentObj.getFatherEmail().replaceAll(" ", "_"));

                if (studentObj.getMotherEmail() != null)
                    studentObj.setMotherEmail(studentObj.getMotherEmail().replaceAll(" ", "_"));

                if (studentObj.getPresentAddress() != null)
                    studentObj.setPresentAddress(studentObj.getPresentAddress().replaceAll(" ", "_"));

                if (studentObj.getPermanentAddress() != null)
                    studentObj.setPermanentAddress(studentObj.getPermanentAddress().replaceAll(" ", "_"));

                if (studentObj.getFatherName() != null)
                    studentObj.setFatherName(studentObj.getFatherName().replaceAll(" ", "_"));

                if (studentObj.getFatherDesignation() != null)
                    studentObj.setFatherDesignation(studentObj.getFatherDesignation().replaceAll(" ", "_"));

                if (studentObj.getFatherQualification() != null)
                    studentObj.setFatherQualification(studentObj.getFatherQualification().replaceAll(" ", "_"));

                if (studentObj.getFatherOccupation() != null)
                    studentObj.setFatherOccupation(studentObj.getFatherOccupation().replaceAll(" ", "_"));

                if (studentObj.getFatherOrganization() != null)
                    studentObj.setFatherOrganization(studentObj.getFatherOrganization().replaceAll(" ", "_"));

                if (studentObj.getMotherName() != null)
                    studentObj.setMotherName(studentObj.getMotherName().replaceAll(" ", "_"));

                if (studentObj.getFatherDesignation() != null)
                    studentObj.setMotherDesignation(studentObj.getFatherDesignation().replaceAll(" ", "_"));

                if (studentObj.getFatherQualification() != null)
                    studentObj.setMotherQualification(studentObj.getFatherQualification().replaceAll(" ", "_"));

                if (studentObj.getMotherOccupation() != null)
                    studentObj.setMotherOccupation(studentObj.getMotherOccupation().replaceAll(" ", "_"));

                if (studentObj.getMotherOrganization() != null)
                    studentObj.setMotherOrganization(studentObj.getMotherOrganization().replaceAll(" ", "_"));

              /*  if (studentObj.getStudentProfileImage() != null)
                    if (studentObj.getStudentProfileImage().contains("/")) {
                        String[] logoUrlArray = studentObj.getStudentProfileImage().split("/");
                        studentObj.setStudentProfileImage(logoUrlArray[logoUrlArray.length - 1]);
                    }*/

               /* if (studentObj.getParentProfileImage() != null)
                    if (studentObj.getParentProfileImage().contains("/")) {
                        String[] logoUrlArray = studentObj.getParentProfileImage().split("/");
                        studentObj.setParentProfileImage(logoUrlArray[logoUrlArray.length - 1]);
                    }
*/

                JSONObject jsonObject = (new JSONObject(gson.toJson(studentObj)));
                jsonObject.remove("AdmissionId");
                jsonObject.remove("AdmissionNo");
                jsonObject.remove("ClassId");
                jsonObject.remove("SectionName");
                jsonObject.remove("SectionStatus");
                jsonObject.remove("ClassName");
                jsonObject.remove("ClassStatus");
                jsonObject.remove("result");
                jsonObject.remove("code");
                jsonObject.remove("onClick");
                jsonObject.remove("positionName");

                param_main.put("data", jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            param_main.put("filter", job_filter);


            Log.e("UpdateStudent", new JSONObject(param_main).toString());

            String stdRegiUpdate = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "StudentRegistrationApi.php?databaseName=" + dbName +
                    "&data=" + (new JSONObject(param_main));
            Log.d("stdRegiUpdate", stdRegiUpdate);
            StringRequest request = new StringRequest(Request.Method.PUT,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    stdRegiUpdate

                    //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("UpdateStudent", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    updation = true;
                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }

                                alert.setMessage(jsonObject.optString("message"));
                                if (!isFinishing())
                                    alert.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("UpdateStudent", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        }

        // }
    }

    public ArrayList<Qualification> getQualificationList() {
        return qualificationList;
    }

    public void addQualification() {
        final View item_add_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
        final TextView tv_item_qualification_degree_name = (TextView) item_add_qualification.findViewById(R.id.tv_item_qualification_degree_name);
        final EditText et_item_qualification_board_university = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_board_university);
        final EditText et_item_qualification_degree_name = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_degree_name);
        final TextView et_item_qualification_year = (TextView) item_add_qualification.findViewById(R.id.et_item_qualification_year);
        final EditText et_item_qualification_marks = (EditText) item_add_qualification.findViewById(R.id.et_item_qualification_marks);
        final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_add_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);

        ((TextView) item_add_qualification.findViewById(R.id.tv_item_qualification_degree_name_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_qualification_board_university_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_qualification_year_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_qualification_marks_title)).setTextColor(colorIs);


        ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
        tv_item_qualification_degree_name.setVisibility(View.GONE);

        AlertDialog.Builder alert_add_qua = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alert_add_qua.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_item_qualification_board_university.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_degree_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_year.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_marks.getText().toString().equalsIgnoreCase("")
                        ) {

                    Toast.makeText(UpdateStudentActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();


                } else
                    try {
                        addQualificationToServer(et_item_qualification_board_university.getText().toString(),
                                et_item_qualification_degree_name.getText().toString(),
                                et_item_qualification_year.getText().toString(),
                                et_item_qualification_marks.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        });
        alert_add_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });


        et_item_qualification_year.
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /**
                         * Hides the soft keyboard
                         */

                        hideSoftKeyboard(item_add_qualification);
                        /*** * */


                        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                        // Create the DatePickerDialog instance
                        DatePickerDialog datePicker = new DatePickerDialog(
                                UpdateStudentActivity.this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                        et_item_qualification_year.setText(year + "");

                                    }
                                },
                                cal.get(Calendar.YEAR),
                                cal.get(Calendar.MONTH),
                                cal.get(Calendar.DAY_OF_MONTH));

                        datePicker.setCancelable(false);
                        datePicker.setTitle(getString(R.string.select_date));
                        datePicker.show();
                    }
                });


        alert_add_qua.setTitle(getString(R.string.add_qualification));
        alert_add_qua.setView(item_add_qualification);
        alert_add_qua.setCancelable(false);
        alert_add_qua.show();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard(View view) {
        /**
         * Hides the soft keyboard
         */

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        /*** * */


    }

    private void addQualificationToServer(final String board_university, final String degree_name, final String year,
                                          final String marks) throws JSONException {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        final JSONObject param = new JSONObject();

        param.put("Type", "Student");
        param.put("UniqueId", studentObj.getRegistrationId());
        param.put("BoardUniversity", board_university);
        param.put("Class", degree_name);
        param.put("Year", year);
        param.put("Marks", marks);
        param.put("Remarks", "");




        String qualPost = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "QualificationApi.php?databaseName=" + dbName;
        Log.d("qualPost", qualPost);
        Log.d("param", param.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                qualPost,param
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("ResultQualification", jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                int id = jsonObject.optInt("result");
                                qualificationList.add(new Qualification(id + "", "Student",
                                        studentObj.getRegistrationId(),
                                        board_university,
                                        degree_name,
                                        year,
                                        marks
                                ));
                                if (!isFinishing())
                                    fragmentQualification.updateList(qualificationList);
                                Toast.makeText(UpdateStudentActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            } else {
                                alert.setMessage(jsonObject.optString("message"));
                                alert.show();
                            }




                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ResultClass", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);


        // }

    }


    public final void updateQualification(final Qualification qualification, final int pos) {
        final View item_update_qualification = getLayoutInflater().inflate(R.layout.item_add_qualification, null);
        final TextView tv_item_qualification_degree_name = (TextView) item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name);
        final EditText et_item_qualification_board_university = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_board_university);
        final EditText et_item_qualification_degree_name = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_degree_name);
        final TextView et_item_qualification_year = (TextView) item_update_qualification.findViewById(R.id.et_item_qualification_year);
        final EditText et_item_qualification_marks = (EditText) item_update_qualification.findViewById(R.id.et_item_qualification_marks);
        final LinearLayout ll_item_view_full_qualification_detail = (LinearLayout) item_update_qualification.findViewById(R.id.ll_item_view_full_qualification_detail);

        ((TextView) item_update_qualification.findViewById(R.id.tv_item_qualification_degree_name_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.et_item_qualification_board_university_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.et_item_qualification_year_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.et_item_qualification_marks_title)).setTextColor(colorIs);


        ll_item_view_full_qualification_detail.setVisibility(View.VISIBLE);
        tv_item_qualification_degree_name.setVisibility(View.GONE);

        et_item_qualification_board_university.setText(qualification.getBoardUniversity());
        et_item_qualification_degree_name.setText(qualification.getDegreeName());
        et_item_qualification_year.setText(qualification.getYear());
        et_item_qualification_marks.setText(qualification.getMarks());

        AlertDialog.Builder alert_update_qua = new AlertDialog.Builder(UpdateStudentActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        alert_update_qua.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_item_qualification_board_university.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_degree_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_year.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_qualification_marks.getText().toString().equalsIgnoreCase("")
                        ) {
                    Toast.makeText(UpdateStudentActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();

                } else try {
                    updateQualificationToServer(qualification, pos,
                            et_item_qualification_board_university.getText().toString(),
                            et_item_qualification_degree_name.getText().toString(),
                            et_item_qualification_year.getText().toString(),
                            et_item_qualification_marks.getText().toString()
                    );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
        alert_update_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alert_update_qua.setTitle(getString(R.string.update_qualification));
        alert_update_qua.setView(item_update_qualification);
        alert_update_qua.setCancelable(false);
        alert_update_qua.show();

        et_item_qualification_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                /**
                 * Hides the soft keyboard
                 */


                /**
                 * Hides the soft keyboard
                 */

                hideSoftKeyboard(item_update_qualification);
                /*** * */
                /*** * */

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(
                        UpdateStudentActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                et_item_qualification_year.setText(year + "");

                            }
                        },
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });


    }


    private void updateQualificationToServer(final Qualification qualification, final int pos, final String board_university, final String degree_name,
                                             final String year, final String marks) throws UnsupportedEncodingException {


        final Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Type", qualification.getType());
        param.put("UniqueId", qualification.getUniqueId());
        param.put("BoardUniversity",URLEncoder.encode( board_university.replaceAll(" ", "_"),"utf-8"));
        param.put("Class", URLEncoder.encode(degree_name.replaceAll(" ", "_"),"utf-8"));
        param.put("Year", year);
        param.put("Marks", marks);
        param.put("Remarks", "");

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("QualificationId", qualification.getQualificationId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        Log.e("UpdateQualification", sp.getString("HostName", "Not Found") + "QualificationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main)));

        String qualUpdate = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "QualificationApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));
        Log.d("qualUpdate", qualUpdate);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                qualUpdate
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateQualification", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {
                                qualification.setBoardUniversity(board_university);
                                qualification.setDegreeName(degree_name);
                                qualification.setYear(year);
                                qualification.setMarks(marks);
                                qualificationList.set(pos, qualification);
                                fragmentQualification.updateList(qualificationList);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }

                            alert.setMessage(jsonObject.optString("message"));
                            if (!isFinishing())
                                alert.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateQualification", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    public void deleteQualification(final Qualification qualification, int position) {
        AlertDialog.Builder alertDelete = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertDelete.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

/*
                if (!Config.checkInternet(UpdateStudentActivity.this)) {
                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }

                } else {*/

                progressbar.show();


                JSONObject job_filter = new JSONObject();
                try {
                    job_filter.put("QualificationId", qualification.getQualificationId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                //  param_main.put("data", new JSONObject(param));
                param_main.put("filter", job_filter);


                Log.e("deleteResultClass", new JSONObject(param_main).toString());

                String qualDelete = sp.getString("HostName", "Not Found")
                        + sp.getString(Config.applicationVersionName, "Not Found")
                        + "QualificationApi.php?databaseName=" + dbName +
                        "&data=" + (new JSONObject(param_main));
                Log.d("qualDelete", qualDelete);
                StringRequest request = new StringRequest(Request.Method.DELETE,
                        //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",

                        qualDelete
                        //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                        ,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                DbHandler.longInfo(s);
                                Log.e("deleteQualification", s);
                                progressbar.cancel();
                                try {
                                    JSONObject jsonObject = new JSONObject(s);

                                    if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {

                                        qualificationList.remove(qualification);
                                        fragmentQualification.updateList(qualificationList);
                                    } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                            ||
                                            jsonObject.optString("code").equalsIgnoreCase("511")) {
                                        if (!logoutAlert & !isFinishing()) {
                                            Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                            logoutAlert = true;
                                        }
                                    } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                        Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                    }

                                    alert.setMessage(jsonObject.optString("message"));
                                    if (!isFinishing())
                                        alert.show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("deleteQualification", volleyError.toString());
                        progressbar.cancel();
                        Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        Map<String, String> header = new LinkedHashMap<String, String>();
                        header.put("Content-Type", "application/x-www-form-urlencoded");
                        return super.getHeaders();
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new LinkedHashMap<String, String>();
                        return params;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                AppRequestQueueController.getInstance(UpdateStudentActivity.this).addToRequestQueue(request);
                // }


            }
        });

        alertDelete.setMessage(R.string.are_you_sure_you_want_to_delete);
        alertDelete.show();
    }

    public void deleteSibling(final Sibling sibling, int position) {
        AlertDialog.Builder alertDelete = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alertDelete.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


               /* if (!Config.checkInternet(UpdateStudentActivity.this)) {
                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }

                } else {*/

                progressbar.show();


                JSONObject job_filter = new JSONObject();
                try {
                    job_filter.put("SiblingId", sibling.getSiblingId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
                //  param_main.put("data", new JSONObject(param));
                param_main.put("filter", job_filter);


                Log.e("deleteSibling", new JSONObject(param_main).toString());

                String siblingDelete = sp.getString("HostName", "Not Found")
                        + sp.getString(Config.applicationVersionName, "Not Found")
                        + "SiblingApi.php?databaseName=" + dbName +
                        "&data=" + (new JSONObject(param_main));
                Log.d("siblingDelete", siblingDelete);

                StringRequest request = new StringRequest(Request.Method.DELETE,
                        //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                        siblingDelete
                        //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                        ,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                DbHandler.longInfo(s);
                                Log.e("deleteSibling", s);
                                progressbar.cancel();
                                try {
                                    JSONObject jsonObject = new JSONObject(s);

                                    if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {

                                        siblingList.remove(sibling);
                                        fragmentSiblingInfo.updateList(siblingList);
                                    } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                            ||
                                            jsonObject.optString("code").equalsIgnoreCase("511")) {
                                        if (!logoutAlert & !isFinishing()) {
                                            Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                            logoutAlert = true;
                                        }
                                    } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                        Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                    }

                                    alert.setMessage(jsonObject.optString("message"));
                                    if (!isFinishing())
                                        alert.show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e("deleteSibling", volleyError.toString());
                        progressbar.cancel();
                        Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        Map<String, String> header = new LinkedHashMap<String, String>();
                        header.put("Content-Type", "application/x-www-form-urlencoded");
                        return super.getHeaders();
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new LinkedHashMap<String, String>();
                        return params;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                AppRequestQueueController.getInstance(UpdateStudentActivity.this).addToRequestQueue(request);
                //  }


            }
        });

        alertDelete.setMessage(R.string.are_you_sure_you_want_to_delete);
        alertDelete.show();
    }

    public ArrayList<Sibling> getSiblingList() {
        return siblingList;
    }

    public void addSibling() {
        final View item_add_qualification = getLayoutInflater().inflate(R.layout.item_add_sibling, null);
        final TextView tv_item_sibling_name = (TextView) item_add_qualification.findViewById(R.id.tv_item_sibling_name);
        final EditText et_item_sibling_name = (EditText) item_add_qualification.findViewById(R.id.et_item_sibling_name);
        final EditText et_item_sibling_class = (EditText) item_add_qualification.findViewById(R.id.et_item_sibling_class);
        final EditText et_item_sibling_school_name = (EditText) item_add_qualification.findViewById(R.id.et_item_sibling_school_name);
        final Button btn_item_sibling_date_of_of_birth = (Button) item_add_qualification.findViewById(R.id.btn_item_sibling_date_of_of_birth);
        //  final EditText btn_item_sibling_date_of_of_birth = (EditText) item_add_qualification.findViewById(R.id.btn_item_sibling_date_of_of_birth);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_sibling_name_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_sibling_class_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.et_item_sibling_school_name_title)).setTextColor(colorIs);
        ((TextView) item_add_qualification.findViewById(R.id.btn_item_sibling_date_of_of_birth_title)).setTextColor(colorIs);

        btn_item_sibling_date_of_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * Hides the soft keyboard
                 */

                hideSoftKeyboard(item_add_qualification);
                /*** * */

                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(UpdateStudentActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String new_day = dayOfMonth + "";
                                if (dayOfMonth < 10)
                                    new_day = "0" + dayOfMonth;

                                String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
                                if ((monthOfYear + 1) < 10)
                                    new_month = "0" + (monthOfYear + 1);

                                btn_item_sibling_date_of_of_birth.setText(year + "-" + new_month + "-" + new_day);
                            }
                        },

                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        final LinearLayout ll_item_view_full_sibling_detail = (LinearLayout) item_add_qualification.findViewById(R.id.ll_item_view_full_sibling_detail);

        ll_item_view_full_sibling_detail.setVisibility(View.VISIBLE);
        tv_item_sibling_name.setVisibility(View.GONE);

        AlertDialog.Builder alert_add_qua = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        alert_add_qua.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (et_item_sibling_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_sibling_class.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_sibling_school_name.getText().toString().equalsIgnoreCase("")
                        ) {
                    Toast.makeText(UpdateStudentActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();
                } else

                    convertDateThenAddSiblingToServer(et_item_sibling_name.getText().toString(),
                            et_item_sibling_class.getText().toString(),
                            et_item_sibling_school_name.getText().toString(),
                            btn_item_sibling_date_of_of_birth.getText().toString());


            }
        });
        alert_add_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alert_add_qua.setTitle(getString(R.string.add_sibling));
        alert_add_qua.setView(item_add_qualification);
        alert_add_qua.setCancelable(false);
        alert_add_qua.show();
    }

    private void convertDateThenAddSiblingToServer(final String childName, final String childClass,
                                                   final String schoolName, final String childDob) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        String convDtae = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                childDob;
        Log.d("convDtae", convDtae);
        StringRequest request = new StringRequest(Request.Method.GET,
                convDtae
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
                        progressbar.dismiss();

                        try {
                            addSiblingToServer(childName, childClass, schoolName, s);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }


    }

    private void addSiblingToServer(final String childName, final String childClass,
                                    final String schoolName, final String childDob) throws JSONException {

/*
        if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        final JSONObject param = new JSONObject();

        param.put("RegistrationId", studentObj.getRegistrationId());
        param.put("SName", childName);
        param.put("SDOB", childDob);
        param.put("SClass", childClass);
        param.put("SSchool", schoolName);
        param.put("SRemarks", "");

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "SiblingApi.php?databaseName=" + dbName;
        Log.d("url", url);
        Log.d("param", param.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                url,param

                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("ResultSibling", jsonObject.toString());
                        progressbar.cancel();

                            if (jsonObject.optString("code").equalsIgnoreCase("201")) {

                                int id = jsonObject.optInt("result");
                                siblingList.add(new Sibling(id + "",
                                        studentObj.getRegistrationId(),
                                        childName,
                                        childDob,
                                        childClass,
                                        schoolName,
                                        ""
                                ));
                                if (!isFinishing())
                                    fragmentSiblingInfo.updateList(siblingList);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }
                            alert.setMessage(jsonObject.optString("message"));
                            alert.show();

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("ResultSibling", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        })  ;

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);


        //}
    }

    public void updateSibling(final Sibling siblingObj, final int pos) {
        if (siblingObj.getSDOB() != null)
            convertDateOfBirthToStringSibling(
                    siblingObj, pos
            );
        else {
            updateSiblingData(siblingObj, pos, siblingObj.getSDOB());
        }
    }

    private void convertDateOfBirthToStringSibling(final Sibling siblingObj, final int pos) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
       if (!siblingObj.getSDOB().equalsIgnoreCase("")){
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=stringToDate&date=" +
                siblingObj.getSDOB();
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateResult", s);
                        progressbar.dismiss();
                        try {
                            s=    new JSONObject(s).optString("result");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        updateSiblingData(siblingObj, pos, s);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        }
    }

    private void updateSiblingData(final Sibling siblingObj, final int pos, String dateOfBirth) {


        final View item_update_qualification = getLayoutInflater().inflate(R.layout.item_add_sibling, null);

        final TextView tv_item_sibling_name = (TextView) item_update_qualification.findViewById(R.id.tv_item_sibling_name);
        final EditText et_item_sibling_name = (EditText) item_update_qualification.findViewById(R.id.et_item_sibling_name);
        final EditText et_item_sibling_class = (EditText) item_update_qualification.findViewById(R.id.et_item_sibling_class);
        final EditText et_item_sibling_school_name = (EditText) item_update_qualification.findViewById(R.id.et_item_sibling_school_name);
        final Button btn_item_sibling_date_of_of_birth = (Button) item_update_qualification.findViewById(R.id.btn_item_sibling_date_of_of_birth);
        final LinearLayout ll_item_view_full_sibling_detail = (LinearLayout) item_update_qualification.findViewById(R.id.ll_item_view_full_sibling_detail);


        ((TextView) item_update_qualification.findViewById(R.id.et_item_sibling_name_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.et_item_sibling_class_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.et_item_sibling_school_name_title)).setTextColor(colorIs);
        ((TextView) item_update_qualification.findViewById(R.id.btn_item_sibling_date_of_of_birth_title)).setTextColor(colorIs);

        ll_item_view_full_sibling_detail.setVisibility(View.VISIBLE);
        tv_item_sibling_name.setVisibility(View.GONE);

        et_item_sibling_class.setText(siblingObj.getSClass());
        et_item_sibling_name.setText(siblingObj.getSName());
        et_item_sibling_school_name.setText(siblingObj.getSSchool());
        btn_item_sibling_date_of_of_birth.setText(dateOfBirth);
        btn_item_sibling_date_of_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Hides the soft keyboard
                 */

                hideSoftKeyboard(item_update_qualification);
                /*** * */
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(UpdateStudentActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String new_day = dayOfMonth + "";
                                if (dayOfMonth < 10)
                                    new_day = "0" + dayOfMonth;

                                String new_month = monthOfYear + 1 + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
                                if ((monthOfYear + 1) < 10)
                                    new_month = "0" + (monthOfYear + 1);

                                btn_item_sibling_date_of_of_birth.setText(year + "-" + new_month + "-" + new_day);
                            }
                        },

                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        AlertDialog.Builder alert_update_qua = new AlertDialog.Builder(UpdateStudentActivity.this, AlertDialog.THEME_HOLO_LIGHT);
        alert_update_qua.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (et_item_sibling_class.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_sibling_name.getText().toString().equalsIgnoreCase("")
                        &&
                        et_item_sibling_school_name.getText().toString().equalsIgnoreCase("")
                        &&
                        btn_item_sibling_date_of_of_birth.getText().toString().equalsIgnoreCase(getString(R.string.select_date))
                        ) {
                    Toast.makeText(UpdateStudentActivity.this, getString(R.string.blank_entry_not_allowed), Toast.LENGTH_SHORT).show();
                } else
                    updateSiblingConvertDate(siblingObj, pos, et_item_sibling_class.getText().toString(),
                            et_item_sibling_name.getText().toString(),
                            et_item_sibling_school_name.getText().toString(),
                            btn_item_sibling_date_of_of_birth.getText().toString()
                    );
            }
        });
        alert_update_qua.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alert_update_qua.setTitle(getString(R.string.update_sibling));
        alert_update_qua.setView(item_update_qualification);
        alert_update_qua.setCancelable(false);
        alert_update_qua.show();
    }

    private void updateSiblingConvertDate(final Sibling siblingObj, final int pos, final String sibling_class, final String sibling_name, final String school_name,
                                          final String sibling_dob) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        Log.e("date", sibling_dob);

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +
                "Conversion.php?type=dateToString&date=" +
                sibling_dob;
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String sibling_dob_converted) {
                        DbHandler.longInfo(sibling_dob_converted);
                        Log.e("dateStrResult", sibling_dob_converted);
                        //   progressbar.dismiss();
                        try {
                            updateSiblingToServer(siblingObj, pos, sibling_class,
                                    sibling_name,
                                    school_name,
                                    sibling_dob,
                                    sibling_dob_converted
                            );
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }
    }

    private void updateSiblingToServer(final Sibling siblingObj, final int pos, final String sibling_class, final String sibling_name, final String school_name, final String sibling_dob, final String sibling_dob_converted) throws UnsupportedEncodingException {

        final Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("RegistrationId", siblingObj.getRegistrationId());
        param.put("SName",URLEncoder.encode(  sibling_name.replaceAll(" ", "_"),"utf-8"));
        param.put("SDOB", sibling_dob_converted);
        param.put("SClass", URLEncoder.encode( sibling_class.replaceAll(" ", "_"),"utf-8"));
        param.put("SSchool", URLEncoder.encode( school_name.replaceAll(" ", "_"),"utf-8"));
        param.put("SRemarks", "");

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("SiblingId", siblingObj.getSiblingId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("data", new JSONObject(param));
        param_main.put("filter", job_filter);


        Log.e("UpdateSibling", sp.getString("HostName", "Not Found") + "SiblingApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main)));

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found") + "SiblingApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.PUT,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                url
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("UpdateSibling", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optString("code").equalsIgnoreCase("200") && !isFinishing()) {
                                siblingObj.setSClass(sibling_class);
                                siblingObj.setSName(sibling_name);
                                siblingObj.setSSchool(school_name);
                                siblingObj.setSDOB(sibling_dob_converted);
                                siblingList.set(pos, siblingObj);
                                fragmentSiblingInfo.updateList(siblingList);
                                Toast.makeText(UpdateStudentActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            } else {

                                alert.setMessage(jsonObject.optString("message"));
                                alert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("UpdateSibling", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                //   params.put("data",( new JSONObject(param)).toString());

                //  params.put("className", et_create_class_new_class_name.getText().toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //  }
    }

    public void getSiblingListFromServer() {
        /*if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        Map<String, String> param = new LinkedHashMap<String, String>();
        //   param.put("Type", "Student");
        param.put("RegistrationId", studentObj.getRegistrationId());


        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        Log.e("SiblingApifilter", (new JSONObject(param1).toString()));
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "SiblingApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param1));
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("Sibling", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                Sibling obj = gson.fromJson(s, typeSibling);
                                siblingList = obj.getResult();
                                //  fragmentSiblingInfo.updateList(siblingList);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Sibling", volleyError.toString());
                Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    public void  updateStudentBasicDetailData(int whichDataUpdate) throws JSONException, UnsupportedEncodingException {
        if (studentObj.getStudentName().equalsIgnoreCase("")) {

            alert.setMessage(getString(R.string.kindally_fill_the_mandatory_fields) + " : " + getString(R.string.student_name));
            alert.show();
            isDialogShowing = true;
        } else {
            progressbar.show();


            JSONObject job_filter = new JSONObject();
            try {
                job_filter.put("RegistrationId", studentObj.getRegistrationId());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject =  new JSONObject();
           switch (whichDataUpdate){
               case 0:/*for basic detail*/
                   jsonObject.put("StudentName", URLEncoder.encode(  null==studentObj.getStudentName() ?"":studentObj.getStudentName() ,"utf-8"));
                   jsonObject.put("DOB",URLEncoder.encode(  null==studentObj.getDOB() ?"":studentObj.getDOB() ,"utf-8"));
                   jsonObject.put("Gender",URLEncoder.encode( null==studentObj.getGender() ?"": studentObj.getGender() ,"utf-8"));
                   jsonObject.put("Caste",URLEncoder.encode( null==studentObj.getCaste() ?"": studentObj.getCaste() ,"utf-8"));
                   jsonObject.put("Category",URLEncoder.encode(  null==studentObj.getCategory() ?"":studentObj.getCategory() ,"utf-8"));
                   jsonObject.put("BloodGroup",URLEncoder.encode( null==studentObj.getBloodGroup() ?"": studentObj.getBloodGroup() ,"utf-8"));

                   break;
               case 1:/*for Contact detail*/

                   jsonObject.put("Mobile",URLEncoder.encode(  null==studentObj.getMobile() ?"":studentObj.getMobile() ,"utf-8"));
                   jsonObject.put("FatherMobile", null==studentObj.getFatherMobile() ?"": studentObj.getFatherMobile()  );
                   jsonObject.put("MotherMobile",   null==studentObj.getMotherMobile() ?"":studentObj.getMotherMobile()  );
                   jsonObject.put("Landline",  null==studentObj.getLandline() ?"":studentObj.getLandline()  );
                   jsonObject.put("FatherEmail",URLEncoder.encode( null==studentObj.getFatherEmail() ?"":studentObj.getFatherEmail() ,"utf-8"));
                   jsonObject.put("MotherEmail",URLEncoder.encode( null==studentObj.getMotherEmail() ?"": studentObj.getMotherEmail() ,"utf-8"));
                   jsonObject.put("PresentAddress",URLEncoder.encode( null==studentObj.getPresentAddress() ?"": studentObj.getPresentAddress() ,"utf-8"));
                   jsonObject.put("PermanentAddress",URLEncoder.encode( null==studentObj.getPermanentAddress() ?"": studentObj.getPermanentAddress() ,"utf-8"));
                   break;
               case 2:

                   jsonObject.put("FatherDateOfBirth",URLEncoder.encode( null==studentObj.getFatherDateOfBirth() ?"":studentObj.getFatherDateOfBirth() ,"utf-8"));
                   jsonObject.put("MotherDateOfBirth",URLEncoder.encode( null==studentObj.getMotherDateOfBirth() ?"":studentObj.getMotherDateOfBirth() ,"utf-8"));
                   jsonObject.put("FatherName",URLEncoder.encode(null==studentObj.getFatherName() ?"": studentObj.getFatherName() ,"utf-8"));
                   jsonObject.put("MotherName",URLEncoder.encode( null==studentObj.getMotherName() ?"":studentObj.getMotherName() ,"utf-8"));
                   jsonObject.put("FatherQualification",URLEncoder.encode( null==studentObj.getFatherQualification() ?"":studentObj.getFatherQualification() ,"utf-8"));
                   jsonObject.put("FatherOccupation",URLEncoder.encode( null==studentObj.getFatherOccupation() ?"":studentObj.getFatherOccupation() ,"utf-8"));
                   jsonObject.put("FatherDesignation",URLEncoder.encode( null==studentObj.getFatherDesignation() ?"":studentObj.getFatherDesignation() ,"utf-8"));
                   jsonObject.put("FatherOrganization",URLEncoder.encode(null==studentObj.getFatherOrganization() ?"": studentObj.getFatherOrganization() ,"utf-8"));
                   jsonObject.put("MotherQualification",URLEncoder.encode(null==studentObj.getMotherQualification() ?"": studentObj.getMotherQualification() ,"utf-8"));
                   jsonObject.put("MotherOccupation",URLEncoder.encode( null==studentObj.getMotherOccupation() ?"":studentObj.getMotherOccupation() ,"utf-8"));
                   jsonObject.put("MotherDesignation",URLEncoder.encode(null==studentObj.getMotherDesignation() ?"": studentObj.getMotherDesignation() ,"utf-8"));
                   jsonObject.put("MotherOrganization",URLEncoder.encode(null==studentObj.getMotherOrganization() ?"": studentObj.getMotherOrganization() ,"utf-8"));
                   break;
           }


            final JSONObject param_main = new JSONObject();
            param_main.put("data", jsonObject);
            param_main.put("filter", job_filter);

            Log.e("UpdateStudent",  param_main .toString());

            String stdRegiUpdate = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "StudentRegistrationApi.php?databaseName=" + dbName +
                    "&data=" +  param_main ;
            Log.d("stdRegiUpdate", stdRegiUpdate);
            StringRequest request = new StringRequest(Request.Method.PUT,
                    //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                    stdRegiUpdate

                    //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("UpdateStudent", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    updation = true;
                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(UpdateStudentActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                    Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                                }

                                alert.setMessage(jsonObject.optString("message"));
                                if (!isFinishing())
                                    alert.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("UpdateStudent", volleyError.toString());
                    progressbar.cancel();
                    Config.responseVolleyErrorHandler(UpdateStudentActivity.this, volleyError, snackbar);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        }

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
//        Intent intent = new Intent(this, CreateStudentActivity.class);
//        intent.putExtra("data", studentObj);
//        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));
//        if (updation) {
//            setResult(RESULT_OK, intent);
//
//        } else {
//            setResult(RESULT_CANCELED, intent);
//
//        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {

//        Intent intent = new Intent(this, CreateStudentActivity.class);
//        intent.putExtra("data", studentObj);
//        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));
//        if (updation) {
//            setResult(RESULT_OK, intent);
//
//        } else {
//            setResult(RESULT_CANCELED, intent);
//
//        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
