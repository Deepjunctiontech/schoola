package in.junctiontech.school.updatestudent;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.RegisteredStudent;
import in.junctiontech.school.models.country.CountryCode;

/**
 * Created by JAYDEVI BHADE on 11/3/2016.
 */

public class ContactDetailFragment extends
        Fragment {

    private ArrayList<String >  countryCodeList= new ArrayList<>();
    private RegisteredStudent studentObj;
    private EditText et_student_data_student_mobile_number, et_student_data_father_mobile_number,
            et_student_data_mother_mobile_number, et_student_data_landline_number, et_student_data_father_email,
            et_student_data_mother_email, et_student_data_present_address, et_student_data_permanent_address,
            et_landline_country_code;
    private AutoCompleteTextView et_student_country_code ,et_father_country_code,et_mother_country_code;
    private SharedPreferences sp ;
    private int  appColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_student_contact_detail, container, false);

        ((TextView) convertView.findViewById(R.id.et_student_data_student_mobile_number_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_mobile_number_tiitle)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_mobile_number_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_landline_number_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_father_email_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_mother_email_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_present_address_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_permanent_address_title)).setTextColor(appColor);

        et_student_data_student_mobile_number = (EditText) convertView.findViewById(R.id.et_student_data_student_mobile_number);
        et_student_data_father_mobile_number = (EditText) convertView.findViewById(R.id.et_student_data_father_mobile_number);
        et_student_data_mother_mobile_number = (EditText) convertView.findViewById(R.id.et_student_data_mother_mobile_number);
        et_student_data_landline_number = (EditText) convertView.findViewById(R.id.et_student_data_landline_number);
        et_student_data_father_email = (EditText) convertView.findViewById(R.id.et_student_data_father_email);
        et_student_data_mother_email = (EditText) convertView.findViewById(R.id.et_student_data_mother_email);
        et_student_data_present_address = (EditText) convertView.findViewById(R.id.et_student_data_present_address);
        et_student_data_permanent_address = (EditText) convertView.findViewById(R.id.et_student_data_permanent_address);
        Button  btn_student_data_contact_update=  (Button)convertView.findViewById(R.id.btn_student_data_contact_update);
        btn_student_data_contact_update.setBackgroundColor(appColor);
        btn_student_data_contact_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_student_data_student_mobile_number.getText().toString().trim().equalsIgnoreCase(""))
                    studentObj.setMobile("");
                if (et_student_data_father_mobile_number.getText().toString().trim().equalsIgnoreCase(""))
                    studentObj.setFatherMobile("");
                if (et_student_data_mother_mobile_number.getText().toString().trim().equalsIgnoreCase(""))
                    studentObj.setMotherMobile("");
                try {
                    ((UpdateStudentActivity) getActivity()).updateStudentBasicDetailData(1/*for Contact detail*/);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        et_student_country_code = (AutoCompleteTextView) convertView.findViewById(R.id.et_student_data_country_code);
        et_father_country_code = (AutoCompleteTextView) convertView.findViewById(R.id.et_father_data_country_code);
        et_mother_country_code = (AutoCompleteTextView) convertView.findViewById(R.id.et_mother_data_country_code);
        et_landline_country_code = (AutoCompleteTextView) convertView.findViewById(R.id.et_landline_country_code);

        String countryCode = sp.getString("SchoolCountryCode","91");
        et_student_country_code.setText(countryCode);
        et_father_country_code.setText(countryCode);
        et_mother_country_code.setText(countryCode);
        et_landline_country_code.setText(countryCode);

        et_student_country_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMobile(s.toString().trim()
                        +"-"+et_student_data_student_mobile_number.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_student_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMobile(et_student_country_code.getText().toString().trim()
                        +"-"+s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_father_country_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherMobile(s.toString().trim()
                        +"-"+et_student_data_father_mobile_number.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherMobile(et_father_country_code.getText().toString().trim()
                        +"-"+s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_mother_country_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherMobile(s.toString().trim()
                        +"-"+et_student_data_mother_mobile_number.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherMobile(et_mother_country_code.getText().toString().trim()
                        +"-"+s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_landline_country_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // studentObj.setLandline(s.toString().trim());
                studentObj.setLandline(s.toString().trim()
                        +"-"+et_student_data_landline_number.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_landline_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // studentObj.setLandline(s.toString().trim());
                studentObj.setLandline(et_landline_country_code.getText().toString().trim()
                        +"-"+s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_father_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setFatherEmail(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_mother_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setMotherEmail(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_student_data_present_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setPresentAddress(s.toString().trim().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_student_data_permanent_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setPermanentAddress(s.toString().trim().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return convertView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentObj = ((UpdateStudentActivity) getActivity()).getRegisterStudentData();
        sp = Prefs.with(getContext()).getSharedPreferences();
        appColor = ((UpdateStudentActivity) getActivity()).getAppColor();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (studentObj.getMobile() != null) {
            if (studentObj.getMobile().contains("-")) {
                String[] arr = studentObj.getMobile().split("-");
                et_student_country_code.setText(arr.length>=1?(arr[0].contains("+")?arr[0].replace("+",""):""+arr[0]):"");
                et_student_data_student_mobile_number.setText(arr.length>=2?arr[1]:"");
            } else
            et_student_data_student_mobile_number.setText(studentObj.getMobile());
        }
        if (studentObj.getFatherMobile() != null) {
            if (studentObj.getFatherMobile().contains("-")) {
                String[] arr = studentObj.getFatherMobile().split("-");
                et_father_country_code.setText(arr.length>=1?(arr[0].contains("+")?arr[0].replace("+",""):""+arr[0]):"");
                et_student_data_father_mobile_number.setText(arr.length>=2?arr[1]:"");
            } else
            et_student_data_father_mobile_number.setText(studentObj.getFatherMobile());
        }
        if (studentObj.getMotherMobile() != null) {
            if (studentObj.getMotherMobile().contains("-")) {
                String[] arr = studentObj.getMotherMobile().split("-");
                et_mother_country_code.setText(arr.length>=1?(arr[0].contains("+")?arr[0].replace("+",""):""+arr[0]):"");
                et_student_data_mother_mobile_number.setText(arr.length>=2?arr[1]:"");
            } else
            et_student_data_mother_mobile_number.setText(studentObj.getMotherMobile());
        }

        if (studentObj.getLandline() != null) {
            if (studentObj.getLandline().contains("-")) {
                String[] arr = studentObj.getLandline().split("-");
                et_landline_country_code.setText(arr.length>=1?(arr[0].contains("+")?arr[0].replace("+",""):""+arr[0]):"");
                et_student_data_landline_number.setText(arr.length>=2?arr[1]:"");
            } else
                et_student_data_landline_number.setText(studentObj.getLandline());
        }



        if (studentObj.getFatherEmail() != null)
        et_student_data_father_email.setText(studentObj.getFatherEmail());
        if (studentObj.getMotherEmail() != null)
        et_student_data_mother_email.setText(studentObj.getMotherEmail());
        if (studentObj.getPresentAddress() != null)
            et_student_data_present_address.setText(studentObj.getPresentAddress().replaceAll("_", " "));
        if (studentObj.getPermanentAddress() != null)
            et_student_data_permanent_address.setText(studentObj.getPermanentAddress().replaceAll("_", " "));

        String countryCodeString   = getActivity().getSharedPreferences(Config.COUNTRY_CODE, Context.MODE_PRIVATE).getString(Config.COUNTRY_CODE,"");
        if (!countryCodeString.equalsIgnoreCase("")){
            CountryCode obj = (new Gson()).fromJson(countryCodeString, new TypeToken<CountryCode>() {
            }.getType());
            ArrayList<CountryCode> countryCodeObjList = obj.getResult();

            for (CountryCode countryObj : countryCodeObjList)
                countryCodeList.add("+" +countryObj.getCode());
            ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, countryCodeList);
          adapterCountry.setDropDownViewResource(R.layout.myspinner_dropdown_item);
            //adapterSpinner.set
            et_student_country_code.setAdapter(adapterCountry);
            et_mother_country_code.setAdapter(adapterCountry);
            et_father_country_code.setAdapter(adapterCountry);
        }else Config.getCountryCode(getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}

