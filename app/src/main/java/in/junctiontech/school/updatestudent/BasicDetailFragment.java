package in.junctiontech.school.updatestudent;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import in.junctiontech.school.R;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.models.RegisteredStudent;

/**
 * Created by JAYDEVI BHADE on 11/3/2016.
 */

public class BasicDetailFragment extends
        Fragment {
    private ArrayList<CreateClass> classListData;
    private ArrayList<String> sectionNameList = new ArrayList<>(), sectionIdList = new ArrayList<>(),
            genderList = new ArrayList<>(), casteList = new ArrayList<>(), categoryList = new ArrayList<>(), bloodGroupList = new ArrayList<>();
    private AppCompatSpinner spinner_student_data_class, spinner_student_data_gender, spinner_student_data_category,
            spinner_student_data_caste, spinner_student_data_blood_group;
    private ArrayAdapter<String> classAdapter, genderTypeAdapter, casteTypeAdapter, categoryTypeAdapter,
            bloodGroupTypeAdapter;
    private ArrayList<MasterEntry> genderTypeListData, casteListData, categoryListData, bloodGroupListData;
    private RegisteredStudent studentObj;
    private EditText et_student_data_student_name;
    private Button btn_student_data_date_of_of_birth, btn_student_data_date_of_registration,
            btn_student_data_basic_detail_update;
    private TextView tv_admission_number;
    private int appColor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_student_basic_detail, container, false);
        tv_admission_number = (TextView) convertView.findViewById(R.id.tv_student_basic_detail_admission_number);
        ((TextView) convertView.findViewById(R.id.tv_student_basic_detail_admission_number_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.et_student_data_student_name_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.btn_student_data_date_of_registration_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.btn_student_data_date_of_of_birth_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.spinner_student_data_class_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.spinner_student_data_gender_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.spinner_student_data_caste_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.spinner_student_data_category_title)).setTextColor(appColor);
        ((TextView) convertView.findViewById(R.id.spinner_student_data_blood_group_title)).setTextColor(appColor);

        spinner_student_data_class = (AppCompatSpinner) convertView.findViewById(R.id.spinner_student_data_class);
        spinner_student_data_class.setEnabled(false);
        spinner_student_data_gender = (AppCompatSpinner) convertView.findViewById(R.id.spinner_student_data_gender);
        spinner_student_data_caste = (AppCompatSpinner) convertView.findViewById(R.id.spinner_student_data_caste);
        spinner_student_data_category = (AppCompatSpinner) convertView.findViewById(R.id.spinner_student_data_category);
        spinner_student_data_blood_group = (AppCompatSpinner) convertView.findViewById(R.id.spinner_student_data_blood_group);

        btn_student_data_basic_detail_update=   (Button) convertView.findViewById(R.id.btn_student_data_basic_detail_update);
        btn_student_data_basic_detail_update.setBackgroundColor(appColor);

        btn_student_data_basic_detail_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    ((UpdateStudentActivity) getActivity()).updateStudentBasicDetailData(0/*for basic detail*/);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });

        et_student_data_student_name = (EditText) convertView.findViewById(R.id.et_student_data_student_name);
        btn_student_data_date_of_registration = (Button) convertView.findViewById(R.id.btn_student_data_date_of_registration);
        btn_student_data_date_of_of_birth = (Button) convertView.findViewById(R.id.btn_student_data_date_of_of_birth);
        btn_student_data_date_of_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

                // Create the DatePickerDialog instance
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        myDateListenersession_date_of_of_birth, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });

        et_student_data_student_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                studentObj.setStudentName(s.toString().trim().replaceAll(" ", "_"));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        spinner_student_data_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    studentObj.setBloodGroup("0");
                else
                    studentObj.setBloodGroup(bloodGroupListData.get(position - 1).getMasterEntryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_student_data_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                studentObj.setGender(genderTypeListData.get(position).getMasterEntryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_student_data_caste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    studentObj.setCaste("0");
                else
                    studentObj.setCaste(casteListData.get(position - 1).getMasterEntryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_student_data_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    studentObj.setCategory("0");
                else
                    studentObj.setCategory(categoryListData.get(position - 1).getMasterEntryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return convertView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appColor = ((UpdateStudentActivity) getActivity()).getAppColor();
        classListData = ((UpdateStudentActivity) getActivity()).getClassData();

        genderTypeListData = ((UpdateStudentActivity) getActivity()).getGenderTypeData();

        casteListData = ((UpdateStudentActivity) getActivity()).getCasteListData();

        categoryListData = ((UpdateStudentActivity) getActivity()).getCategoryListData();

        studentObj = ((UpdateStudentActivity) getActivity()).getRegisterStudentData();

        bloodGroupListData = ((UpdateStudentActivity) getActivity()).getBloodGroupListData();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tv_admission_number.setText(studentObj.getAdmissionNo());
        et_student_data_student_name.setText(studentObj.getStudentName().replaceAll("_", " "));
        ((UpdateStudentActivity) getActivity()).convertStringToDate("R", studentObj.getDOR());
        if (studentObj.getDOB() != null)
            ((UpdateStudentActivity) getActivity()).convertStringToDate("S", studentObj.getDOB());
        /* class spinner */
        int studentClassPos = 0;
        sectionNameList = new ArrayList<>();
        for (int cls = 0; cls < classListData.size(); cls++) {
            CreateClass obj = classListData.get(cls);
            ArrayList<CreateClass> secList = obj.getSectionList();
            for (int sec = 0; sec < secList.size(); sec++) {
                sectionIdList.add(secList.get(sec).getSectionId());
                sectionNameList.add(obj.getClassName() + " " +
                        secList.get(sec).getSectionName());
                if (studentObj.getSectionId().equalsIgnoreCase(secList.get(sec).getSectionId()))
                    studentClassPos = sectionIdList.size() - 1;
            }
        }

        classAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, sectionNameList);
        classAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_student_data_class.setAdapter(classAdapter);
        spinner_student_data_class.setSelection(studentClassPos);

      /* gender type spinner */
        int studentGenderPos = 0;
        genderList = new ArrayList<>();
        for (int i = 0; i < genderTypeListData.size(); i++) {
            genderList.add(genderTypeListData.get(i).getMasterEntryValue());
            if (studentObj.getGender().equalsIgnoreCase(genderTypeListData.get(i).getMasterEntryId()))
                studentGenderPos = i;
        }

        genderTypeAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, genderList);
       genderTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_student_data_gender.setAdapter(genderTypeAdapter);
        spinner_student_data_gender.setSelection(studentGenderPos);

        /* caste type spinner */
        int studentCastePos = 0;
        casteList = new ArrayList<>();
        casteList.add(getString(R.string.select));
        for (int i = 0; i < casteListData.size(); i++) {
            casteList.add(casteListData.get(i).getMasterEntryValue());
            if (studentObj.getCaste() != null)
                if (studentObj.getCaste().equalsIgnoreCase(casteListData.get(i).getMasterEntryId()))
                    studentCastePos = i + 1;
        }

        casteTypeAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, casteList);
        casteTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_student_data_caste.setAdapter(casteTypeAdapter);
        spinner_student_data_caste.setSelection(studentCastePos);

        /* category type spinner */
        int studentCategoryPos = 0;
        categoryList = new ArrayList<>();
        categoryList.add(getString(R.string.select));
        for (int i = 0; i < categoryListData.size(); i++) {
            categoryList.add(categoryListData.get(i).getMasterEntryValue());
            if (studentObj.getCategory() != null)
                if (studentObj.getCategory().equalsIgnoreCase(categoryListData.get(i).getMasterEntryId()))
                    studentCategoryPos = i + 1;
        }

        categoryTypeAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, categoryList);
       categoryTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_student_data_category.setAdapter(categoryTypeAdapter);
        spinner_student_data_category.setSelection(studentCategoryPos);

         /* blood group type spinner */
        int studentBloodGroupPos = 0;
        bloodGroupList = new ArrayList<>();
        bloodGroupList.add(getString(R.string.select));
        for (int i = 0; i < bloodGroupListData.size(); i++) {
            bloodGroupList.add(bloodGroupListData.get(i).getMasterEntryValue());
            if (studentObj.getBloodGroup() != null)
                if (studentObj.getBloodGroup().equalsIgnoreCase(bloodGroupListData.get(i).getMasterEntryId()))
                    studentBloodGroupPos = i + 1;
        }

        bloodGroupTypeAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, bloodGroupList);
       bloodGroupTypeAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_student_data_blood_group.setAdapter(bloodGroupTypeAdapter);
        spinner_student_data_blood_group.setSelection(studentBloodGroupPos);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public DatePickerDialog.OnDateSetListener myDateListenersession_date_of_of_birth = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            String new_day = dayOfMonth + "";
            if (dayOfMonth < 10)
                new_day = "0" + dayOfMonth;

            String new_month = monthOfYear + 1 + "";
            if ((monthOfYear + 1) < 10)
                new_month = "0" + (monthOfYear + 1);

            btn_student_data_date_of_of_birth.setText(year + "-" + new_month + "-" + new_day);
            ((UpdateStudentActivity) getActivity()).convertDateOfBirthToString("S", year + "-" + new_month + "-" + new_day, 0);
        }
    };


    public void setDateOfRegistration(String dateOfRegistration) {
        btn_student_data_date_of_registration.setText(dateOfRegistration);
    }

    public void setDateOfBirth(String dateOfBirth) {
        btn_student_data_date_of_of_birth.setText(dateOfBirth);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {

        super.onDetach();
    }
}
