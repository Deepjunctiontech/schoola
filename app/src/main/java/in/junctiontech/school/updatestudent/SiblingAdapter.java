package in.junctiontech.school.updatestudent;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.Sibling;

/**
 * Created by JAYDEVI BHADE on 11/5/2016.
 */

public class SiblingAdapter extends RecyclerView.Adapter<SiblingAdapter.MyViewHolder> {
    private ArrayList<Sibling> siblingList;
    private Context context;
    private int appColor;

    public SiblingAdapter(Context context, ArrayList<Sibling> siblingList, int appColor) {
        this.siblingList = siblingList;
        this.context = context;
        this.appColor = appColor;

    }
    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public SiblingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_qualification, parent, false);

        return new SiblingAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final SiblingAdapter.MyViewHolder holder, final int position) {
        final Sibling siblingObj = siblingList.get(position);


        holder.tv_item_qualification_subtext.setText(
               context.getString(R.string.class_text) + " : " +
                        siblingObj.getSClass() + "\n" +
                        context.getString(R.string.school_name) + " : "+
                       siblingObj.getSSchool() );
        holder.tv_item_qualification_degree_name.setText( context.getString(R.string.child_name) + " : " +
                siblingObj.getSName());
        holder.tv_item_qualification_degree_name.setVisibility(View.VISIBLE);

        holder.tv_item_qualification_subtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UpdateStudentActivity) context).updateSibling(
                        siblingObj, position);

            }


        });
     //animate(holder);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return siblingList.size();
    }

    public void updateList(ArrayList<Sibling> siblingList) {
        this.siblingList = siblingList;
        this.notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_qualification_degree_name, tv_item_qualification_subtext;
        Button btn_item_qualification_delete;


        public MyViewHolder(View view) {
            super(view);
            (view.findViewById(R.id.ll_item_view_full_qualification_detail)).setVisibility(View.GONE);

            tv_item_qualification_degree_name = (TextView) view.findViewById(R.id.tv_item_qualification_degree_name);
            tv_item_qualification_degree_name.setTextColor(appColor);
            tv_item_qualification_subtext = (TextView) view.findViewById(R.id.tv_item_qualification_subtext);
            tv_item_qualification_subtext.setVisibility(View.VISIBLE);
            btn_item_qualification_delete = (Button) view.findViewById(R.id.btn_item_qualification_delete);
            btn_item_qualification_delete.setVisibility(View.VISIBLE);

            btn_item_qualification_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((UpdateStudentActivity)context).deleteSibling(
                            siblingList.get(getLayoutPosition()),getLayoutPosition());
                }
            });
            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }
}
