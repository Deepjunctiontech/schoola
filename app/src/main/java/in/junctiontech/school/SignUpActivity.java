package in.junctiontech.school;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.admin.userpermission.MobilePermission;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.models.SchoolSession;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;

public class SignUpActivity extends AppCompatActivity {

    private TextInputLayout user_text, pass_text, organization_text;
    private EditText username, password, organizationname;
    private LinearLayout rl;
    private ProgressDialog progressDialog;
    static public SharedPreferences sp;
    String user_type = "";
    String loggedUserName = "";
    String subtitleName = "";
    private String selectedRadioButton = "";
    private boolean alertIsShowing = false;
    private AlertDialog.Builder alert;
    static public DbHandler db;
    private String loggedUserID;
    private AlertDialog.Builder alert7;
    private TextView tv_sign_up_reset_password;
    private BroadcastReceiver  mRegistrationBroadcastReceiver;
    private CheckBox  ck_sign_up_remember_me;
    private   String blockCharacterSet = "#";
    /**
     * Created by Junction Software on 17-Oct-15.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sp = Prefs.with(this).getSharedPreferences();

        db = DbHandler.getInstance(this);
        String org = sp.getString("organization_name", null);
        super.onCreate(savedInstanceState);
        if (sp.getString("user_type", null) != null) {

            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else {
            Locale local_obj = Resources.getSystem().getConfiguration().locale;
            //LanguageSetup.changeLang(this, local_obj.getLanguage());


            setContentView(R.layout.activity_sign_up);


            /*   For Background display */
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                findViewById(R.id.rl).setBackgroundResource(R.drawable.ic_background_landscape);
            else findViewById(R.id.rl).setBackgroundResource(R.drawable.ic_background_verticle);

            alert7 = new AlertDialog.Builder(SignUpActivity.this, AlertDialog.THEME_HOLO_LIGHT);
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            check();
            user_text = (TextInputLayout) this.findViewById(R.id.user_text);
            pass_text = (TextInputLayout) this.findViewById(R.id.pass_text);
            organization_text = (TextInputLayout) this.findViewById(R.id.organization_text);

            username = (EditText) this.findViewById(R.id.user_edit);
            username.setFilters(new InputFilter[] {  new InputFilter() {

                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {


                    if (source != null && blockCharacterSet.contains(("" + source)) ) {
                        return "";
                    }

                    return null;
                }
            },
                    new InputFilter.LengthFilter(getResources().getInteger(R.integer.normal_text)) });


            password = (EditText) this.findViewById(R.id.pass_edit);
            organizationname = (EditText) this.findViewById(R.id.organization_edit);
            rl = (LinearLayout) this.findViewById(R.id.rl);
            ck_sign_up_remember_me = (CheckBox)findViewById(R.id.ck_sign_up_remember_me);

            password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        Config.hideKeyboard(SignUpActivity.this);
                        submit();
                        handled = true;
                    }
                    return handled;
                }
            });

            organizationname.setText(sp.getString(Config.SMS_ORGANIZATION_NAME, ""));
            username.setText(sp.getString("forLoginUserName", "").equalsIgnoreCase("") ? sp.getString("loggedUserName", "") : sp.getString("forLoginUserName", ""));
           // username.setText(sp.getString("forLoginUserName", ""));
            password.setText(sp.getString("loggedPassword", ""));
            /*****  When Comes From Registration******/
            if (getIntent().getBooleanExtra("dataAvailable", false)) {
                organizationname.setText(getIntent().getStringExtra("organizationKey"));
                username.setText(getIntent().getStringExtra("username"));
                password.setText(getIntent().getStringExtra("password"));
            }else  if (!(sp.getString(Config.DECODED_ORGANIZATION_KEY, "").equalsIgnoreCase("")
            || sp.getString(Config.SMS_ORGANIZATION_NAME, "").equalsIgnoreCase("")
            ) && takePermission()) {
                setSchoolLogo();
                organizationname.setText(sp.getString(Config.DECODED_ORGANIZATION_KEY, ""));
            }


            /***************  Reset Password   *****************/
            tv_sign_up_reset_password = (TextView) findViewById(R.id.tv_sign_up_reset_password);
            tv_sign_up_reset_password.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
            tv_sign_up_reset_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SignUpActivity.this, PasswordResetActivity.class));
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });
            /***********/


            setColorApp();


        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.DATA_LOADING_COMPLETE)) {
                   if (progressDialog!=null)progressDialog.dismiss();

                    Intent intentSignUp = new Intent(SignUpActivity.this, AdminNavigationDrawerNew.class);
                    intentSignUp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentSignUp);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }
        };
    }
    @Override
    protected void onPause() {
        // videoView.start();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        //  videoView.start();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.DATA_LOADING_COMPLETE));

        super.onResume();
    }

    private void setColorApp() {
        // FOR NAVIGATION VIEW ITEM TEXT COLOR
        int colorIs = Config.getAppColor(this, true);
        tv_sign_up_reset_password.setTextColor(colorIs);
        ((Button) findViewById(R.id.btn_signup_login)).setBackgroundColor(colorIs);
    }

    public boolean takePermission() {

        if (
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);


            }
            return false;
        } else return true;

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {

        if (permsRequestCode == Config.IMAGE_LOGO_CODE) {

            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //denied
                        Log.e("denied", permissions[0]);
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                            //allowed
                            Log.e("allowed", permissions[0]);
                            setSchoolLogo();

                        } else {
                            //set to never ask again
                            Log.e("set to never ask again", permissions[0]);
                            //do something here.
                            showRationale(getString(R.string.permission_denied), getString(
                                    R.string.permission_denied_external_storage));

                        }
                    }
                } else {
                    setSchoolLogo();

                }
            }


        }


    }

    private void setSchoolLogo() {
        try {
            File targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                    "/" + Config.FOLDER_NAME + "/",
                    sp.getString("organization_name", "") + "_logo" + ".jpg");

            ((CircleImageView) findViewById(R.id.civ_sign_up_page_school_logo)).setImageBitmap(BitmapFactory.decodeStream(new FileInputStream(targetLocation)));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showRationale(String permission, String permission_denied_location) {
        androidx.appcompat.app.AlertDialog.Builder alertLocationInfo = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, Config.LOCATION);
            }
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void getHostNameFromServer() {

        final Map<String, Object> param = new LinkedHashMap<>();
        param.put("applicationName", "apischoolerp");
        param.put("organizationKey", organizationname.getText().toString().trim().toUpperCase().toString());
        //   String url = "http://cpanel.zeroerp.com/Login/GetHostName";
        //   String url = "http://192.168.1.151/cpanel/Login/GetHostName";


        String hostNameUrl = Config.GET_HOST_NAME
                + "data=" + (new JSONObject(param));

        Log.e("HostNameUrl", hostNameUrl);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, hostNameUrl, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        String code = job.optString("code");
                        switch (code) {
                            case 200 + "":
                                sp.edit().clear().commit();
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString("HostName", job.optString("applicationURL"));
                                editor.putString("HostName", job.optString("applicationURL"));
                                editor.putString(Config.applicationVersionName, Config.applicationVersionNameValue);
                                // editor.putString(Config.applicationVersionName,job.optString("applicationVersionName"));
                           /* editor.putString("HostName", job.optString("hostName"));*/
                                boolean addVisibility = true;

                                if (job.optString("addStatus").equalsIgnoreCase("YES"))
                                    addVisibility = false;

                                editor.putString("licenceExpiry", job.optString("licenceExpiry"));
                                editor.putBoolean("addStatusGone", addVisibility);
                                editor.putString(Config.DECODED_ORGANIZATION_KEY,  organizationname.getText().toString().trim().toUpperCase().toString());
                                editor.commit();
                                // submit();

                           /* goToLogin();*/


                                try {
                                    loginToSchool(job.optString("databaseName"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                break;


                            default:
                                progressDialog.dismiss();
                                AlertDialog.Builder alert_internet =
                                        new AlertDialog.Builder(SignUpActivity.this, AlertDialog.THEME_HOLO_LIGHT);
                                alert_internet.setMessage(job.optString("message"));
                                alert_internet.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                alert_internet.setCancelable(false);
                                alert_internet.show();
                                break;

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                progressDialog.cancel();
                Config.responseVolleyErrorHandler(SignUpActivity.this,error,rl);
            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(req);
    }

    public void submit(View v) {
        Config.hideKeyboard(this);
       /* if (!(Config.checkInternet(SignUpActivity.this))) {
            AlertDialog.Builder alert_internet = new AlertDialog.Builder(SignUpActivity.this, AlertDialog.THEME_HOLO_LIGHT);
            alert_internet.setMessage(R.string.internet_not_available_please_check_your_internet_connectivity);
            alert_internet.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert_internet.setCancelable(false);
            alert_internet.show();
        } else {
        */    // getHostNameFromServer();

        submit();
        // }

    }

    private void submit() {
//        Utility.hideKeyboard(this);
        SharedPreferences sharedPreferences = Prefs.with(this).getSharedPreferences();


        boolean b1 = isEmptyEmail();
        boolean b2 = isEmptyPassword();
        boolean b3 = isEmptyOrganization();

        user_text.setError(null);
        pass_text.setError(null);
        organization_text.setError(null);

        if (b3 && b1 && b2) {
            Snackbar.make(rl, R.string.one_or_more_field_are_blank, Snackbar.LENGTH_LONG).setAction(R.string.dismiss, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            }).show();

        } else {
            if (b3 || b2 || b1) {
                if (b3) {
                    organization_text.setError(getString(R.string.Institute_Code_can_not_be_blank));
                }
                if (b1) {
                    user_text.setError(getString(R.string.login_id_can_not_be_blank));
                }
                if (b2) {
                    pass_text.setError(getString(R.string.password_can_not_be_blank));
                }
            } else {
                //   progressDialog = newtext ProgressDialog(SignUpActivity.this);
                //  if (sharedPreferences.getString("HostName", "Not Found").equalsIgnoreCase("Not Found")) {
                progressDialog.setMessage(getString(R.string.please_wait));
                progressDialog.show();
                getHostNameFromServer();
                // goToLogin();


                // } else goToLogin();


            }
        }

    }



    private void loginToSchool(final String db_name) throws JSONException {

        final JSONObject param = new JSONObject();
        param.put("database_name", db_name);
        param.put("username", username.getText().toString().replaceAll("#",""));
        param.put("password", password.getText().toString());
        param.put("url", "android");
        //  progressDialog.show();

        String loginUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "LoginApi.php";
        //  String loginUrl=  "http://192.168.1.181/lifepartner/Iosandroidtest.php";
        //  String loginUrl=  "http://192.168.1.181/apischoolerp/testapi.php";

        Log.e("LoginUrl", loginUrl);

        Log.e("LoginDate", param.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, loginUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressDialog.cancel();

                        Log.e("Response:", job.toString());

                        String code = job.optString("code");
                        boolean adminTeaPareStd = false;

                        switch (code) {
                            case "200": {

                                user_type = job.optString("userType");
                                boolean t = user_type.equalsIgnoreCase("teacher") || user_type.equalsIgnoreCase("Teacher") || user_type.equalsIgnoreCase("teachers") || user_type.equalsIgnoreCase("Teachers");
                                boolean p = user_type.equalsIgnoreCase("parent") || user_type.equalsIgnoreCase("parents") || user_type.equalsIgnoreCase("Parent") || user_type.equalsIgnoreCase("Parents");
                                boolean st = user_type.equalsIgnoreCase("student") || user_type.equalsIgnoreCase("students") || user_type.equalsIgnoreCase("Student") || user_type.equalsIgnoreCase("Students");
                                boolean admin = user_type.equalsIgnoreCase("admin");

                                if (admin) {
                                    user_type = "Administrator";
                                    loggedUserID = /*"Admin"*//*username.getText().toString()*/job.optString("userID");
                                    loggedUserName = username.getText().toString();
                                    subtitleName = user_type;

                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(Config.SMS_ORGANIZATION_NAME, organizationname.getText().toString().toUpperCase().toString());
                                    editor.putString("organization_name", db_name);
                                    editor.putString("user_type", user_type);
                                    editor.putString("loggedUserID", loggedUserID);
                                    editor.putString("loggedUserName", loggedUserName);
                                    editor.putString("forLoginUserName", loggedUserName);
                                    editor.putBoolean(Config.REMEMBER_ME, ck_sign_up_remember_me.isChecked());
                                    editor.putString("loggedPassword", password.getText().toString());

                                         /* **** For data auto sending attendance homework exam result ******/
                                    editor.putString("staffUserID", job.optString("staffUserID","1"));
                                    editor.putBoolean("MobileData_attendance", true);
                                    editor.putBoolean("MobileData_homework", true);
                                    editor.putBoolean("MobileData_result", true);
                                    editor.putBoolean("wifi_attendance", true);
                                    editor.putBoolean("wifi_homework", true);
                                    editor.putBoolean("wifi_result", true);
                                    editor.commit();

                                    progressDialog.dismiss();
                                    //db.getDataFromServer();
                                    Intent intentSignUp = new Intent(SignUpActivity.this, AdminNavigationDrawerNew.class);
                                    intentSignUp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intentSignUp);
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.exit);

                                    return;
                                } else if (t) {
                                    user_type = "Teacher";
                                    loggedUserID = job.optString("userID");
                                    loggedUserName = job.optString("userName");
                                    subtitleName = user_type;
                                    adminTeaPareStd = true;
                                } else if (p) {

                                    user_type = "Parent";
                                    loggedUserID = job.optString("userID");
                                    loggedUserName = job.optString("userName");
                                    if (loggedUserName.equalsIgnoreCase(""))
                                        loggedUserName = job.optString("studentName");
                                    String parentChildName = job.optString("studentName");
                                    subtitleName = user_type + " ( " + parentChildName + " )";
                                    adminTeaPareStd = true;
                                } else if (st) {
                                    user_type = "Student";
                                    loggedUserID = job.optString("userID");
                                    loggedUserName = job.optString("userName");
                                    subtitleName = user_type;
                                    adminTeaPareStd = true;
                                } else {

                                    user_type = job.optString("userType","");
                                    loggedUserID = job.optString("userID");
                                    loggedUserName = job.optString("userName");
                                    subtitleName = user_type;
                                    adminTeaPareStd = true;

                                }
                                if (adminTeaPareStd) {
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(Config.SMS_ORGANIZATION_NAME, organizationname.getText().toString().toUpperCase().toString());
                                    editor.putBoolean("MobileData_attendance", true);
                                    editor.putBoolean("MobileData_homework", true);
                                    editor.putBoolean("MobileData_result", true);
                                    editor.putBoolean("wifi_attendance", true);
                                    editor.putBoolean("wifi_homework", true);
                                    editor.putBoolean("wifi_result", true);

                                    editor.putString("organization_name", db_name);
                                    editor.putString("user_type", user_type);
                                    editor.putString("loggedUserID", loggedUserID);
                                    editor.putString("loggedUserName", loggedUserName);
                                    editor.putString("forLoginUserName", username.getText().toString());
                                    editor.putBoolean(Config.REMEMBER_ME, ck_sign_up_remember_me.isChecked());

                                    editor.putString("loggedPassword", password.getText().toString());
                                    editor.putString("subtitleName", subtitleName);
                                    editor.putBoolean("toastVisibility", false);
                                    editor.putBoolean("firstTimeHomeworkFetch", true);
                                    editor.putBoolean("defaultSetting", true);
                                    editor.putString("UserTypeId", job.optString("UserType", ""));
                                    editor.putString("staffUserID", job.optString("staffUserID"));
                                    editor.commit();

                                    progressDialog.dismiss();
                                    final ProgressDialog pg = new ProgressDialog(SignUpActivity.this);
                                    pg.setCancelable(false);
                                    pg.setMessage(getString(R.string.fetching_data));
                                    pg.show();

                                    getCurrentSession();
                                    pg.dismiss();

                                }
                                break;
                            }

                            case "300": {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivity.this);
                                alertAdmin.setMessage(job.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertAdmin.show();
                                break;
                            }
                            case "401": {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivity.this);

                                alertAdmin.setMessage(job.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertAdmin.show();
                                break;
                            }
                            case "402": {
                                progressDialog.dismiss();
                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivity.this);

                                alertAdmin.setMessage(job.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertAdmin.show();
                                break;
                            }
                            case "511": {
                                progressDialog.dismiss();

                                AlertDialog.Builder alertLogout = new AlertDialog.Builder(
                                        SignUpActivity.this, AlertDialog.THEME_TRADITIONAL);
                                alertLogout.setIcon(getResources().getDrawable(R.drawable.ic_alert));
                                alertLogout.setTitle(getString(R.string.alert));
                                alertLogout.setMessage(job.optString("message") + "\n" +
                                        getString(R.string.contact_message_to_junction_tech));
                                alertLogout.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                alertLogout.setCancelable(false);
                                alertLogout.show();

                                break;
                            }
                            default:
                                progressDialog.dismiss();
                                AlertDialog.Builder alertAdmin = new AlertDialog.Builder(SignUpActivity.this);

                                alertAdmin.setMessage(job.optString("message"));
                                alertAdmin.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertAdmin.show();
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.cancel();
                        Config.responseVolleyErrorHandler(SignUpActivity.this,volleyError,rl);
                    }
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }


    private boolean isEmptyOrganization() {
        return organizationname.getText() == null || organizationname.getText().toString() == null
                || organizationname.getText().toString().equals("") || organizationname.getText().toString().isEmpty();
    }

    private boolean isEmptyEmail() {
        return username.getText() == null || username.getText().toString() == null
                || username.getText().toString().equals("") || username.getText().toString().isEmpty();
    }

    private boolean isEmptyPassword() {
        return password.getText() == null || password.getText().toString() == null
                || password.getText().toString().equals("") || password.getText().toString().isEmpty();
    }

    private void check() {
        if (!sp.getString("user_name", "Not Found").equals("Not Found") &&
                !sp.getString("user_pass", "Not Found").equals("Not Found") &&
                !sp.getString("user_organization", "Not Found").equals("Not Found")) {
            Intent intentSignUp = new Intent(SignUpActivity.this, AdminNavigationDrawerNew.class);
            intentSignUp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentSignUp);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    @Override
    public void onBackPressed() {
        // startActivity(new Intent(this, FirstScreen.class));
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
        super.onBackPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            findViewById(R.id.rl).setBackgroundResource(R.drawable.ic_background_landscape);
        else findViewById(R.id.rl).setBackgroundResource(R.drawable.ic_background_verticle);
        super.onConfigurationChanged(newConfig);
    }

    public void getCurrentSession() {
      /*  if (!Config.checkInternet(this)) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();

        } else {*/

            /* -- session list fetch via annual , half yearly , quarterly */

        String sessUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found") +
                "SchoolSessionApi.php/getSession?databaseName=" + sp.getString("organization_name", "");
        Log.e("SessionListEntity",
                sessUrl
        );


        StringRequest request = new StringRequest(Request.Method.GET,
                sessUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("SessionListEntity", s);

                        if (s != "") {

                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {

                                        /*--  session List Saved in SharedPrefrence ---*/
                                    final SchoolSession schoolSessionObj = (new Gson()).fromJson(s,
                                            new TypeToken<SchoolSession>() {
                                            }.getType());
                                    ArrayList<SchoolSession> schoolSessionArrayList = schoolSessionObj.getSessionList();
                                    for (SchoolSession schoolSession : schoolSessionArrayList) {
                                        if (schoolSessionObj.getCurrentSession().equalsIgnoreCase(schoolSession
                                                .getSession().replaceAll("_", " "))) {

                                            sp.edit().putString("sessionStartDate", schoolSession.getSessionStartDate())
                                                    .putString("sessionEndDate", schoolSession.getSessionEndDate())
                                                    .putString("session", schoolSessionObj.getCurrentSession()).commit();

                                            getPermissionForUserTypeBasis();
                                            db.getDataFromServer();

                                            break;
                                        }
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("SessionListEntity", volleyError.toString());

                progressDialog.cancel();
                Config.responseVolleyErrorHandler(SignUpActivity.this,volleyError,rl);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }
    }
    private void getPermissionForUserTypeBasis() {
        progressDialog.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("UserId", sp.getString("loggedUserID", "NotFound"));
        param.put("UserType", sp.getString("UserTypeId", "NotFound"));
        //  param.put("UserId", keyName);




        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "NotFound")
                + "MobilepermissionApi.php?action=getPermission&databaseName=" + sp.getString("organization_name", "") +
                "&data=" + (new JSONObject(param));
        Log.e("keyName", url);


        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntryData", s);
                        progressDialog.cancel();
                       Gson gson= new Gson();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {

                                MobilePermission mobilePermissionObj = gson.fromJson(s,new TypeToken<MobilePermission>(){}.getType());
                                for (MobilePermission obj : mobilePermissionObj.getResult())
                                    obj.setPermission("1");

                                sp.edit().putString("mobilePermission",gson.toJson(mobilePermissionObj) ).commit();
                                LocalBroadcastManager.getInstance(SignUpActivity.this ).sendBroadcast(new Intent(Config.PERMISSION_FETCHED));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        progressDialog.cancel();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MasterEntryData", volleyError.toString());
                progressDialog.dismiss();
                LocalBroadcastManager.getInstance(SignUpActivity.this ).sendBroadcast(new Intent(Config.PERMISSION_FETCHED));


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }
}
