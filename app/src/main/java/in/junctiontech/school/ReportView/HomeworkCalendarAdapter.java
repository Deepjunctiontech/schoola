package in.junctiontech.school.ReportView;

import android.graphics.Color;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkData;

/**
 * Created by JAYDEVI BHADE on 01-08-2017.
 */

public class HomeworkCalendarAdapter extends RecyclerView.Adapter<HomeworkCalendarAdapter.MyViewHolder> {

    private ParentViewHomework context;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);


    private static final String TAG = HomeworkCalendarAdapter.class.getSimpleName();

    private List<Date> monthlyDates;

    private ArrayList<ArrayList<HomeworkData>> allEvents = new ArrayList<>();
    private int currentMonthDate = 0;
    private int animationView;
    private boolean animate = true;
    private int lastClickedPosition = 0;


    public HomeworkCalendarAdapter(ParentViewHomework parentViewHomework, List<Date> dayValueInCells, ArrayList<ArrayList<HomeworkData>> listOfEvents) {
        this.allEvents = listOfEvents;
        this.context = parentViewHomework;
        this.monthlyDates = dayValueInCells;
    }


    @Override
    public HomeworkCalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_homework, parent, false);
        return new HomeworkCalendarAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final HomeworkCalendarAdapter.MyViewHolder holder, final int position) {
if (position<7) {
    holder.tv_item_calendar_date.setTypeface(Typeface.DEFAULT_BOLD);
    holder.tv_item_calendar_date.setText(String.valueOf(ParentViewAttendance.weekDay[position]));
}else {
    Date mDate = monthlyDates.get(position);
    final Calendar dateCal = Calendar.getInstance();
    dateCal.setTime(mDate);

    final int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);


    if (allEvents != null && allEvents.size() >= position && allEvents.get(position) != null) {
        holder.tv_item_calendar_date.setBackground(context.getResources().getDrawable(R.drawable.green_circular_layout));
        holder.tv_item_calendar_date.setTextColor(Color.WHITE);
    } else {
        holder.tv_item_calendar_date.setBackground(null);
        holder.tv_item_calendar_date.setTextColor(Color.BLACK);
    }
    holder.tv_item_calendar_date.setText(String.valueOf(dayValue));
}
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return monthlyDates.size();
    }

    public void updateCalendar(List<Date> monthlyDates, ArrayList<ArrayList<HomeworkData>> allEvents) {
        this.monthlyDates = monthlyDates;
        this.allEvents = allEvents;
        this.notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_calendar_date;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_calendar_date = (TextView) itemView.findViewById(R.id.month_date);
            tv_item_calendar_date.setBackground(null);
            tv_item_calendar_date.setTextColor(Color.BLACK);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getLayoutPosition()<7);else {
                        Date mDate = monthlyDates.get(getLayoutPosition());
                        final Calendar dateCal = Calendar.getInstance();
                        dateCal.setTime(mDate);

                        Log.e(" holder.itemViewHolder", dateCal.getTime() + "");
                        currentMonthDate = dateCal.get(Calendar.DAY_OF_MONTH);
                        animate = false;

                        notifyDataSetChanged();
                        if (allEvents != null && allEvents.size() >= getLayoutPosition() && allEvents.get(getLayoutPosition()) != null)
                            context.openHomeworkList(allEvents.get(getLayoutPosition()));
                        else context.openHomeworkList(new ArrayList<HomeworkData>());
                    }
                }
            });
        }
    }

}


