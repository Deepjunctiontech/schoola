package in.junctiontech.school.ReportView;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkData;

/**
 * Created by JAYDEVI BHADE on 12-07-2017.
 */

public class ViewHomeworkAdapter extends RecyclerView.Adapter<ViewHomeworkAdapter.MyViewHolder> {

    Context context;
    List<HomeworkData> homeworkDataArrayList;
    int colorIs;

    public ViewHomeworkAdapter(Context context, List<HomeworkData> homeworkList,
                               int colorIs) {
        this.context = context;
        this.homeworkDataArrayList = homeworkList;
        this.colorIs = colorIs;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homework_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        HomeworkData homeworkData = homeworkDataArrayList.get(position);
        if (null != homeworkData) {

            if (homeworkData.getSubjectName() != null)
                holder.subjectName.setText(homeworkData.getSubjectName());
           /* if (  homeworkData.getHomework() == null ||
                    homeworkData.getHomework().equalsIgnoreCase(""))
                holder.homework.setText(context.getString(R.string.homework_not_available));
            else { //Spanned htmlAsSpanned = Html.fromHtml(homeworkData.getHomework());
                holder.webview_homework_of_particular_subject.loadDataWithBaseURL(null,homeworkData.getHomework() , "text/html", "utf-8", null);
                //holder.homework.setText(Html.fromHtml(homeworkData.getHomework()));
            }*/

            if (homeworkData.getEvaluation() != null) {

                if (homeworkData.getEvaluation().equalsIgnoreCase("NE") ||
                        homeworkData.getEvaluation().equalsIgnoreCase("")
                        ) {
                    holder.evaluation_status.setText(context.getString(R.string.not_evaluated));
                    holder.evaluation_status.setTextColor(context.getResources().getColor(android.R.color.black));
                } else if (homeworkData.getEvaluation().equalsIgnoreCase("C")) {

                    holder.evaluation_status.setTextColor(context.getResources().getColor(android.R.color.holo_green_dark));
                    holder.evaluation_status.setText(context.getString(R.string.completed));
                } else if (homeworkData.getEvaluation().equalsIgnoreCase("INC")) {

                    holder.evaluation_status.setText(context.getString(R.string.incomplete));
                    holder.evaluation_status.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                } else holder.evaluation_status.setText("");

            } else holder.evaluation_status.setText("");
        }
        holder.ll_item_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ViewHomeworkDetailActivity.class)
                        .putExtra("dataHomework", homeworkDataArrayList.get(position)));

            }
        });
        holder.subjectName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ViewHomeworkDetailActivity.class)
                        .putExtra("dataHomework", homeworkDataArrayList.get(position)));

            }
        });

    }

    @Override
    public int getItemCount() {
        return homeworkDataArrayList.size();
    }

    public void updateList(List<HomeworkData> homeworkDataArrayList) {
        this.homeworkDataArrayList = homeworkDataArrayList;
        /*this.homeworkList = homwork;
        this.evaluationList = eval;*/
        this.notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subjectName,/* homework,*/ evaluation_status;
        // WebView webview_homework_of_particular_subject;
        LinearLayout ll_item_holder;
        public MyViewHolder(View itemView) {
            super(itemView);
            ll_item_holder =(LinearLayout) itemView.findViewById(R.id.ll_item_holder);
            subjectName = (TextView) itemView.findViewById(R.id.tv_subject_name);
            subjectName.setTextColor(colorIs);
           /* homework = (TextView) itemView.findViewById(R.id.tv_homework_of_particular_subject);
            homework.setVisibility(View.GONE);*/
            evaluation_status = (TextView) itemView.findViewById(R.id.tv_subject_evaluation_status);
            //   webview_homework_of_particular_subject = (WebView) itemView.findViewById(R.id.webview_homework_of_particular_subject);
          /*  itemView.findViewById(R.id.tv_homework_of_particular_subject).setVisibility(View.GONE);*/

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));


           /* webview_homework_of_particular_subject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ViewHomeworkDetailActivity.class)
                            .putExtra("dataHomework",homeworkDataArrayList.get(getLayoutPosition())));
                }
            });*/
        }
    }
}