package in.junctiontech.school.ReportView;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.webkit.WebView;
import android.widget.TextView;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkData;

public class ViewHomeworkDetailActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private int  colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_homework_detail);
        sp = Prefs.with(this).getSharedPreferences();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        ((CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar)).setContentScrimColor(colorIs);
        ( findViewById(R.id.appBarLayout)).setBackgroundColor(colorIs);

      if (getIntent().getBooleanExtra("isFromAddHomework",false)){
          WebView  webview = (WebView)findViewById(R.id.tv_view_hom_detail_homework);
          webview.loadDataWithBaseURL(null, getIntent().getStringExtra("homework") , "text/html", "utf-8", null);
          // webview.getSettings().setBuiltInZoomControls(true);
          webview.setVerticalScrollBarEnabled(true);
          webview.setHorizontalScrollBarEnabled(true);


          TextView subject =   (TextView)findViewById(R.id.tv_view_hom_detail_subject) ;
          subject.setTextColor(colorIs);
          subject.setText( getIntent().getStringExtra("subjectName"));
      }else {
          HomeworkData homeworkData = (HomeworkData) getIntent().getSerializableExtra("dataHomework");
          WebView webview = (WebView) findViewById(R.id.tv_view_hom_detail_homework);
          webview.loadDataWithBaseURL(null, homeworkData.getHomework(), "text/html", "utf-8", null);
          // webview.getSettings().setBuiltInZoomControls(true);
          webview.setVerticalScrollBarEnabled(true);
          webview.setHorizontalScrollBarEnabled(true);

          TextView evaluation_status = (TextView) findViewById(R.id.tv_view_hom_detail_homework_status);
          TextView subject = (TextView) findViewById(R.id.tv_view_hom_detail_subject);
          subject.setTextColor(colorIs);
          subject.setText(homeworkData.getSubjectName());

          if (homeworkData.getEvaluation() != null) {

              if (homeworkData.getEvaluation().equalsIgnoreCase("NE") ||
                      homeworkData.getEvaluation().equalsIgnoreCase("")
                      ) {
                  evaluation_status.setText(getString(R.string.not_evaluated));
                  evaluation_status.setTextColor(getResources().getColor(android.R.color.black));
              } else if (homeworkData.getEvaluation().equalsIgnoreCase("C")) {

                  evaluation_status.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                  evaluation_status.setText(getString(R.string.completed));
              } else if (homeworkData.getEvaluation().equalsIgnoreCase("INC")) {

                  evaluation_status.setText(getString(R.string.incomplete));
                  evaluation_status.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
              } else evaluation_status.setText(getString(R.string.not_evaluated));

          } else evaluation_status.setText(getString(R.string.not_evaluated));
      }
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }
}
