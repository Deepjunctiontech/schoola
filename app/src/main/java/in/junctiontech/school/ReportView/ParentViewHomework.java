package in.junctiontech.school.ReportView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkData;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;

public class ParentViewHomework extends AppCompatActivity {

    private RecyclerView calander_rv;
    private TextView month_textview;
    private Calendar month_calendar;
    public String[] months_tab;
    private String[] weekDay;
    private ProgressDialog progressDialog;
    float x1, x2;
    private SharedPreferences sharedPsreferences;
    private AlertDialog.Builder alert_normal;
    private boolean logoutAlert = false;
    private int colorIs;
    private RecyclerView rv_viewReportHomework;
    private ViewHomeworkAdapter mAdapter;
    private ArrayList<HomeworkData> homeworkDataList = new ArrayList<>();
    private HomeworkCalendarAdapter calendarAdapter;
    private View snackbar;
    private SimpleDateFormat monthYear = new SimpleDateFormat("yyyy-MM", Locale.ENGLISH);
    private SimpleDateFormat monthName = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private ArrayList<StudentData> classSectionList = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>();
    private ArrayAdapter<String> clsAdapter;
    private Spinner sp_first_class, sp_first_student;
    private ArrayList<String> studentArrayList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private ArrayList<StudentData> studentObjArrayList = new ArrayList<>();
    private DbHandler db;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //    getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((Button) findViewById(R.id.btn_previous_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));
            ((Button) findViewById(R.id.btn_next_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));
        }
        ((TextView) findViewById(R.id.sp_reportview_subject_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_class)).setTextColor(colorIs);
        //((TextView)findViewById(R.id.tv_sp_timetable_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_section)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_section)).setText(getString(R.string.student));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));
        db = DbHandler.getInstance(this);
        month_calendar = Calendar.getInstance();
        setContentView(R.layout.activity_view_report_homework);
        setColorApp();
        if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Not Found")) {
//            startActivity(new Intent(this, FirstScreen.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        } else {
            if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                    sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
                ;
            else
                findViewById(R.id.ll_timetable_class_selection).setVisibility(View.VISIBLE);
            calander_rv = (RecyclerView) findViewById(R.id.rv_viewReportHomework_calendar);
            snackbar = findViewById(R.id.snack_bar_report);
/*
                AdView mAdView = (AdView) findViewById(R.id.adview_Report_View_Homework);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
                if (sharedPsreferences.getBoolean("addStatusGone", false))
                    mAdView.setVisibility(View.GONE);*/

            getSupportActionBar().setTitle(R.string.view_assignment);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.fetching_assignment));

            alert_normal = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alert_normal.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            sp_first_class = (Spinner) findViewById(R.id.sp_timetable_class);
            sp_first_student = (Spinner) findViewById(R.id.sp_timetable_section);

            classSectionList = db.getClassSectionList();
            for (StudentData obj : classSectionList)
                classSectionNameList.add(obj.getStudentName());

            try {
                setClassAdapter();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            month_textview = (TextView) findViewById(R.id.tv_viewrepoertHomework_month);
            month_textview.setTextColor(colorIs);


            weekDay = getResources().getStringArray(R.array.week_name_array);
//        mon = getResources().getStringArray(R.array.full_name_of_month_array);
            months_tab = getResources().getStringArray(R.array.full_name_of_month_array);
            month_textview.setText(monthName.format(month_calendar.getTime()));

            rv_viewReportHomework = (RecyclerView) findViewById(R.id.rv_viewReportHomework);
            mAdapter = new ViewHomeworkAdapter(this, new ArrayList<HomeworkData>(), colorIs);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rv_viewReportHomework.setLayoutManager(mLayoutManager);

            rv_viewReportHomework.setItemAnimator(new DefaultItemAnimator());
            rv_viewReportHomework.setAdapter(mAdapter);


        }
    }


    private void setClassAdapter() throws JSONException {
        clsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classSectionNameList);
        clsAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_class.setAdapter(clsAdapter);
        if (classSectionList.size() == 0)
            Config.responseSnackBarHandler(getString(R.string.classes_not_available), snackbar);
        else setStudentList();

        sp_first_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setStudentList();
                try {
                    mAdapter.updateList(new ArrayList<HomeworkData>());
                    getMonthHomework();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_first_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    mAdapter.updateList(new ArrayList<HomeworkData>());
                    getMonthHomework();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setStudentList() {

        studentObjArrayList = (ArrayList<StudentData>) db.getStudents("", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());
        studentArrayList.clear();
        for (StudentData obj : studentObjArrayList)
            studentArrayList.add(obj.getStudentName());

        studentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, studentArrayList);
        studentAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_student.setAdapter(studentAdapter);
        if (studentObjArrayList.size() == 0) {
            homeworkDataList.clear();
            setMonthCalender();
            openHomeworkList(new ArrayList<HomeworkData>());
            Config.responseSnackBarHandler(getString(R.string.students_not_available), snackbar);
        } else try {
            openHomeworkList(new ArrayList<HomeworkData>());
            getMonthHomework();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getMonthHomework() throws JSONException {
        String studentID = "";
        if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
            studentID = sharedPsreferences.getString("loggedUserID", "Not Found");
        else if (studentObjArrayList.size() > 0 && sp_first_student.getSelectedItemPosition() != -1)
            studentID = /*sharedPreferences.getString("loggedUserID", "Not Found")*/ studentObjArrayList.get(sp_first_student.getSelectedItemPosition()).getStudentID();

        if (!studentID.equalsIgnoreCase("")) {
            progressDialog.show();
            progressDialog.setCancelable(false);
            SharedPreferences sharedPreferences = Prefs.with(this).getSharedPreferences();
            String db_name = sharedPreferences.getString("organization_name", "Not Found");


            final JSONObject param = new JSONObject();
            //   param.put("DB_Name", db_name);
            param.put("userType", "Parent");
            param.put("viewRequest", "homework");
            param.put("studentID", studentID);
            param.put("monthName", monthYear.format(month_calendar.getTime()));
            param.put("session", sharedPreferences.getString("session", ""));

            String homeUrl = sharedPreferences.getString("HostName", "Not Found")
                    + sharedPreferences.getString(Config.applicationVersionName, "Not Found")
                    + "HomeworkApi.php?action=parent&databaseName=" + db_name;
            Log.e("homeUrl", homeUrl);
            Log.e("param", param.toString());
            homeworkDataList = new ArrayList<>();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, homeUrl, param,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.d("homeworkParent ", jsonObject.toString());


                            JSONObject resJson = jsonObject;
                            if (resJson.optString("code").equalsIgnoreCase("201")) {
                                HomeworkData homeworkData = (new Gson()).fromJson(jsonObject.toString(), new TypeToken<HomeworkData>() {
                                }.getType());
                                homeworkDataList = homeworkData.getResult();

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(ParentViewHomework.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (resJson.optString("code").equalsIgnoreCase("502")) {

                                alert_normal.setMessage(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min));
                                alert_normal.show();
                            } else {
                                alert_normal.setMessage(resJson.optString("message"));
                                alert_normal.show();
                            }


                            setMonthCalender();
                            progressDialog.dismiss();
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d("error volley", volleyError.toString());

                            String msg = "Error !";
                            if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                                msg = getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.internet_not_available_please_check_your_internet_connectivity);

                            } else if (volleyError instanceof AuthFailureError) {
                                msg = "AuthFailureError!";
                                //TODO
                            } else if (volleyError instanceof ServerError) {
                                msg = "ServerError!";
                                //TODO
                            } else if (volleyError instanceof NetworkError) {
                                msg = "NetworkError!";
                                //TODO
                            } else if (volleyError instanceof ParseError) {
                                msg = "ParseError!";
                                //TODO
                            }
                            android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(ParentViewHomework.this,
                                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                            alert.setTitle(getString(R.string.result));
                            alert.setMessage(msg);
                            alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
                                        startActivity(new Intent(ParentViewHomework.this, AdminNavigationDrawerNew.class));
                                        finish();

                                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                    } else {
                                        finish();
                                        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                    }

                                }
                            });
                            alert.show();
                            setMonthCalender();
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            jsonObjectRequest.setTag("parentFetchHomework");
            AppRequestQueueController.getInstance(this).cancelRequestByTag("parentFetchHomework");
            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest, "parentFetchHomework");
        }
    }

    public void previousMonth(View v) throws JSONException {

        month_calendar.add(Calendar.MONTH, -1);
        month_textview.setText(monthName.format(month_calendar.getTime()));
        getMonthHomework();

    }

    public void nextMonth(View v) throws JSONException {

        month_calendar.add(Calendar.MONTH, 1);
        month_textview.setText(monthName.format(month_calendar.getTime()));
        getMonthHomework();

    }

    public void setMonthCalender() {
        List<Date> dayValueInCells = new ArrayList<Date>();
        ArrayList<ArrayList<HomeworkData>> listOfEvents = new ArrayList<>();

        Calendar mCal = (Calendar) month_calendar.clone();
        mCal.set(Calendar.HOUR_OF_DAY, 0);
        mCal.set(Calendar.MINUTE, 0);
        mCal.set(Calendar.SECOND, 0);
        mCal.set(Calendar.MILLISECOND, 0);

        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);

        for (int a = 0; a < 7; a++) {
            dayValueInCells.add(mCal.getTime());
            listOfEvents.add(null);
        }

        while (dayValueInCells.size() < 48) {
            dayValueInCells.add(mCal.getTime());
            ArrayList<HomeworkData> localList = getHomeworkListOfDate(mCal);
            listOfEvents.add(localList.size() == 0 ? null : localList);

            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d("DATA", "Number of date " + dayValueInCells.size());
        if (calendarAdapter == null) {
            calendarAdapter = new HomeworkCalendarAdapter(this, dayValueInCells, listOfEvents);

            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
            // Attach the layout manager to the recycler view
            calander_rv.setLayoutManager(gridLayoutManager);
            calander_rv.setAdapter(calendarAdapter);

        } else
            calendarAdapter.updateCalendar(dayValueInCells, listOfEvents);
        progressDialog.dismiss();

    }

    private ArrayList<HomeworkData> getHomeworkListOfDate(Calendar calEventOpenDateObj) {
        Calendar calStartDate = Calendar.getInstance();

        if (calEventOpenDateObj == null) calEventOpenDateObj = Calendar.getInstance();
        calEventOpenDateObj.set(Calendar.HOUR_OF_DAY, 0);
        calEventOpenDateObj.set(Calendar.MINUTE, 0);
        calEventOpenDateObj.set(Calendar.SECOND, 0);
        calEventOpenDateObj.set(Calendar.MILLISECOND, 0);

        ArrayList<HomeworkData> eventCalList = new ArrayList<>();
        for (HomeworkData schObj : homeworkDataList) {
            try {
                Date startEventDateObj = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse(schObj.getDate());
                calStartDate.setTime(startEventDateObj);


                if (calEventOpenDateObj.compareTo(calStartDate) == 0) {
                    eventCalList.add(schObj);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        return eventCalList;
    }


    @Override
    public boolean onSupportNavigateUp() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(ParentViewHomework.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(ParentViewHomework.this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }

    }

    public void openHomeworkList(ArrayList<HomeworkData> dataHomework) {
        if (mAdapter != null)
            mAdapter.updateList(dataHomework);
    }
}
