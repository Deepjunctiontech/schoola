package in.junctiontech.school.ReportView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.attendance.AttendanceSlotWiseData;
import in.junctiontech.school.attendance.AttendanceSlotWiseDetailAdapter;
import in.junctiontech.school.models.StudentData;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;


public class ParentViewAttendance extends AppCompatActivity /*implements OnChartValueSelectedListener*/ {

    private Calendar month_calendar;
    private String month_date_array[] = new String[48];
    private String month_work_array[] = new String[48];
    final int month_background_array[] = new int[48];
    final int month_textcolor_array[] = new int[48];
    private String monthFullDateName_array[] = new String[48];
    private String presentAbsent_array[] = new String[48];
    private TextView month_textview;
    private GridView calander_gridview;
    public String[] months_tab = {"January", "February", "March", "April", "May", "Jun", "July", "August",
            "September", "October", "November", "December"};
    public static String[] weekDay = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private ProgressDialog progressDialog;
    private String[] mydate, prasentstatus;
    float x1, x2;
    private SharedPreferences sharedPsreferences;
    private DbHandler db;
    private int totalPresent = 0, totalAbsent = 0;
    private TextView tv_total_present_day, tv_total_absent_day;
    private AlertDialog.Builder alert_normal;
    private RelativeLayout snackbar;
    private boolean logoutAlert = false;
    private int colorIs;
    private ArrayList<StudentData> classSectionList = new ArrayList<>();
    private ArrayList<String> classSectionNameList = new ArrayList<>();
    private ArrayAdapter<String> clsAdapter;
    private Spinner sp_first_class, sp_first_student;
    private ArrayList<String> studentArrayList = new ArrayList<>();
    private ArrayAdapter<String> studentAdapter;
    private ArrayList<StudentData> studentObjArrayList = new ArrayList<>();
    private String db_name;
    static public SimpleDateFormat dateMonthYear = new SimpleDateFormat("yyyy-MM-", Locale.ENGLISH);

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ((Button) findViewById(R.id.btn_previous_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));
            ((Button) findViewById(R.id.btn_next_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));
        }
        ((TextView) findViewById(R.id.sp_reportview_subject_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_section)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_sp_timetable_section)).setText(getString(R.string.student));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPsreferences = Prefs.with(this).getSharedPreferences();
        //  LanguageSetup.changeLang(this, sharedPsreferences.getString("app_language", ""));
        db = DbHandler.getInstance(this);
        // super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_report_homework);
        snackbar = (RelativeLayout) findViewById(R.id.snack_bar_report);
        db_name = sharedPsreferences.getString("organization_name", "Not Found");
        getSupportActionBar().setTitle(R.string.view_attendance);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Not Found")) {
//            startActivity(new Intent(this, FirstScreen.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        } else {
           /* if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                    sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent")) {*/
               /* AdView mAdView = (AdView) findViewById(R.id.adview_Report_View_Homework);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);

                if (sharedPsreferences.getBoolean("addStatusGone", false))
                    mAdView.setVisibility(View.GONE);
*/
            progressDialog = new ProgressDialog(ParentViewAttendance.this);
            progressDialog.setMessage(getString(R.string.fetching_attendance));

            alert_normal = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alert_normal.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                    sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
                ;
            else
                findViewById(R.id.ll_timetable_class_selection).setVisibility(View.VISIBLE);
            tv_total_present_day = (TextView) findViewById(R.id.tv_report_view_total_present_day);
            tv_total_absent_day = (TextView) findViewById(R.id.tv_report_view_total_absent_day);
            sp_first_class = (Spinner) findViewById(R.id.sp_timetable_class);
            sp_first_student = (Spinner) findViewById(R.id.sp_timetable_section);


            calander_gridview = (GridView) findViewById(R.id.calander_gridview_viewReportHomework);
            month_textview = (TextView) findViewById(R.id.tv_viewrepoertHomework_month);
            month_textview.setTextColor(colorIs);
            month_calendar = Calendar.getInstance();
            weekDay = getResources().getStringArray(R.array.week_name_array);
            months_tab = getResources().getStringArray(R.array.full_name_of_month_array);

            classSectionList = db.getClassSectionList();
            for (StudentData obj : classSectionList)
                classSectionNameList.add(obj.getStudentName());

            try {
                setClassAdapter();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            findViewById(R.id.ll_view_description).setVisibility(View.VISIBLE);
            /*} else {
                android.support.v7.app.AlertDialog.Builder alertMsg = new android.support.v7.app.AlertDialog.Builder(this);
                alertMsg.setTitle(getString(R.string.sorry));
                alertMsg.setMessage(getString(R.string.attendance_report)+" "+getString(R.string.feature_is_only_for_student_and_parents));
                alertMsg.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
                            startActivity(new Intent(ParentViewAttendance.this, AdminNavigationDrawer.class));
                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        } else {
                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        }
                    }
                });
                alertMsg.setCancelable(false);
                alertMsg.show();
            }*/


        }
        /*PieChart pieChart = (PieChart) findViewById(R.id.piechart);
        pieChart.setUsePercentValues(true);

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        ArrayList<Entry> yvalues = new ArrayList<Entry>();
        yvalues.add(new Entry(8f, 0));
        yvalues.add(new Entry(15f, 1));
        yvalues.add(new Entry(12f, 2));


        PieDataSet dataSet = new PieDataSet(yvalues, "Attendance Report");

        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add("Present");
        xVals.add("Absent");
        xVals.add("No Attendance");
        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(77,207,11));
        colors.add(Color.rgb(239,21,21));
        colors.add(Color.rgb(255,255,255));
        colors.add(Color.rgb(160,239,136));
        colors.add(Color.rgb(200,246,139));
        colors.add(Color.rgb(176,219,233));
        colors.add(Color.rgb(183,176,253));

        dataSet.setColors(colors);

        ///// disabling chart legend
        pieChart.getLegend().setEnabled(false);
     //   dataSet.setColors(new int[] { android.R.color.holo_green_dark, android.R.color.holo_red_dark, android.R.color.darker_gray, android.R.color.holo_blue_bright, android.R.color.white });

        PieData data = new PieData(xVals, dataSet);
        // In Percentage term
        data.setValueFormatter(new PercentFormatter());
        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);
        pieChart.setDescription("Attendance Report");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(10f);
        pieChart.setHoleRadius(5f);


        data.setValueTextSize(8f);
        data.setValueTextColor(Color.DKGRAY);
        pieChart.setOnChartValueSelectedListener(this);

        pieChart.animateXY(1400, 1400);
*/
    }





/*    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
                        + ", DataSet index: " + dataSetIndex);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
    }*/
    private void setClassAdapter() throws JSONException {
        clsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classSectionNameList);
        clsAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);

        sp_first_class.setAdapter(clsAdapter);
        if (classSectionList.size() == 0)
            Config.responseSnackBarHandler(getString(R.string.classes_not_available), snackbar);
        else setStudentList();

        sp_first_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setStudentList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_first_student.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    getMonthAttendance(month_calendar);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setStudentList() {
        studentObjArrayList = (ArrayList<StudentData>) db.getStudents("", classSectionList.get(sp_first_class.getSelectedItemPosition()).getStudentID());
        studentArrayList.clear();
        for (StudentData obj : studentObjArrayList)
            studentArrayList.add(obj.getStudentName());

        studentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, studentArrayList);
        studentAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_first_student.setAdapter(studentAdapter);
        if (studentObjArrayList.size() == 0) {
            Config.responseSnackBarHandler(getString(R.string.students_not_available), snackbar);
        } else try {
            getMonthAttendance(month_calendar);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getMonthAttendance(final Calendar month_calendar) throws JSONException {


        String studentID = "";


        if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent")) {
            studentID = sharedPsreferences.getString("loggedUserID", "Not Found");
        } else if (studentObjArrayList.size() > 0 && sp_first_student.getSelectedItemPosition() != -1) {
                studentID = /*sharedPreferences.getString("loggedUserID", "Not Found")*/ studentObjArrayList.get(sp_first_student.getSelectedItemPosition()).getStudentID();
    }

            if (!studentID.equalsIgnoreCase("")){
            progressDialog.show();
            progressDialog.setCancelable(false);
            int month = month_calendar.get(Calendar.MONTH);
            String new_month = month + 1 + "";
            if ((month + 1) < 10)
                new_month = "0" + (month + 1);

            String monthname = month_calendar.get(Calendar.YEAR) + "-" + new_month;
            final JSONObject param = new JSONObject();
            // param.put("DB_Name", db_name);
            param.put("userType", "Parent");
            param.put("viewRequest", "SubjectWiseAttendance");
            param.put("studentID", studentID);
            param.put("monthName", monthname);
            param.put("subjectId", "0");
            String attUrl = sharedPsreferences.getString("HostName", "Not Found")
                    + sharedPsreferences.getString(Config.applicationVersionName, "Not Found")
                    + "StudentAttendanceApi.php?action=parent&databaseName=" + db_name;
            Log.e("attUrl", attUrl);
            Log.e("param", param.toString());

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, attUrl, param,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.e("attendance", jsonObject.toString());
                            mydate = new String[]{};
                            prasentstatus = new String[]{};

                            try {

                                JSONObject resJson = jsonObject;
                                if (resJson.optString("code").equalsIgnoreCase("201")) {
                                    JSONArray jarr = new JSONArray(resJson.optString("result"));

                                    mydate = new String[jarr.length()];
                                    prasentstatus = new String[jarr.length()];

                                    for (int i = 0; i < jarr.length(); i++) {
                                        JSONObject job = jarr.getJSONObject(i);
                                        mydate[i] = job.getString("onDate");
                                        prasentstatus[i] = job.getString("presentStatus");

                                    }


                                } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                        ||
                                        jsonObject.optString("code").equalsIgnoreCase("511")) {
                                    if (!logoutAlert & !isFinishing()) {
                                        Config.responseVolleyHandlerAlert(ParentViewAttendance.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                        logoutAlert = true;
                                    }
                                } else if (resJson.optString("code").equalsIgnoreCase("502")) {

                                    alert_normal.setMessage(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min));
                                    alert_normal.show();
                                } else {

                                    alert_normal.setMessage(resJson.optString("message"));
                                    alert_normal.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            setMonthCalender(month_calendar);
                            progressDialog.dismiss();

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();
                    String msg = "Error !";
                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                        msg = getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.internet_not_available_please_check_your_internet_connectivity);

                    } else if (volleyError instanceof AuthFailureError) {
                        msg = "AuthFailureError!";
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        msg = "ServerError!";
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        msg = "NetworkError!";
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        msg = "ParseError!";
                        //TODO
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder(ParentViewAttendance.this,
                            AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alert.setTitle(getString(R.string.result));
                    alert.setMessage(msg);
                    alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
                                startActivity(new Intent(ParentViewAttendance.this, AdminNavigationDrawerNew.class));
                                finish();

                                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                            } else {
                                finish();
                                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                            }
                        }
                    });
                    alert.show();

                }
            });


            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).cancelRequestByTag("parentFetchAttendance");
            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest, "parentFetchAttendance");

        }
    }

    public void previousMonth(View v) {
        /*if (month_calendar.get(Calendar.MONTH) == month_calendar.getActualMinimum(Calendar.MONTH)) {
            month_calendar.set((month_calendar.get(Calendar.YEAR) - 1), month_calendar.getActualMaximum(Calendar.MONTH), 1);
        } else {
            month_calendar.set(Calendar.MONTH, month_calendar.get(Calendar.MONTH) - 1);
        }*/
        month_calendar.add(Calendar.MONTH, -1);
        try {
            getMonthAttendance(month_calendar);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        setMonthCalender(month_calendar);
    }

    public void nextMonth(View v) {
      /*  if (month_calendar.get(Calendar.MONTH) == month_calendar.getActualMaximum(Calendar.MONTH)) {
            month_calendar.set((month_calendar.get(Calendar.YEAR) + 1), month_calendar.getActualMinimum(Calendar.MONTH), 1);
        } else {
            month_calendar.set(Calendar.MONTH, month_calendar.get(Calendar.MONTH) + 1);
        }*/
        month_calendar.add(Calendar.MONTH, 1);
        try {
            getMonthAttendance(month_calendar);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        setMonthCalender(month_calendar);
    }

    public void setMonthCalender(Calendar cal_obj) {
        totalPresent = 0;
        totalAbsent = 0;
        for (int i = 0; i < 48; i++) {
            month_work_array[i] = "";
            month_date_array[i] = "";
            monthFullDateName_array[i] = "";
            month_background_array[i] = R.drawable.borders;
            month_textcolor_array[i] = Color.BLACK;
            presentAbsent_array[i] = "";
        }

        month_textview.setText(months_tab[month_calendar.get(Calendar.MONTH)] + " " + month_calendar.get(Calendar.YEAR));

        Calendar mycal = new GregorianCalendar(cal_obj.get(Calendar.YEAR), cal_obj.get(Calendar.MONTH), 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);


        String aaa = (mycal.getTime()).toString();
        int pos = 0;
        if (pos == 0)
            month_textcolor_array[pos] = Color.RED;

        for (int a = 0; a < 7; a++) {
            month_date_array[pos] = weekDay[a];
            pos++;
        }
        for (int i = 0; i < Config.weekDayEng.length; i++) {
            if (pos % 7 == 0)
                month_textcolor_array[pos] = Color.RED;

            if (aaa.contains(Config.weekDayEng[i])) {
                break;
            } else {
                month_date_array[pos] = "";
                pos++;
            }
        }

        int month = cal_obj.get(Calendar.MONTH);

        String new_month = month + 1 + "";
        if ((month + 1) < 10)
            new_month = "0" + (month + 1);


        for (int i = 0; i < daysInMonth; i++) {
            if (pos % 7 == 0)
                month_textcolor_array[pos] = Color.RED;

            String a = (i + 1) + "";
            month_date_array[pos] = a;
            if ((i + 1) < 10)
                a = "0" + (i + 1);

            presentAbsent_array[pos] = "Absent";

            monthFullDateName_array[pos] = cal_obj.get(Calendar.YEAR) + "-" + new_month + "-" + a;
            for (int j = 0; j < mydate.length; j++) {

                if (monthFullDateName_array[pos].equalsIgnoreCase(mydate[j])) {
                    if (prasentstatus[j].equalsIgnoreCase("P")) {
                        month_background_array[pos] = R.drawable.green_circular_layout;
                        presentAbsent_array[pos] = "Present";
                        month_textcolor_array[pos] = Color.WHITE;
                        totalPresent++;
                        break;
                    } else {
                        totalAbsent++;
                        month_textcolor_array[pos] = Color.WHITE;
                        presentAbsent_array[pos] = "Absent";
                        month_background_array[pos] = R.drawable.red_circular_layout;
                        break;
                    }

                }

            }


            pos++;

        }
        calander_gridview.setAdapter(new MyMonthAdapter(ParentViewAttendance.this, month_date_array, month_background_array));
        progressDialog.dismiss();

       /* for (int obj : month_background_array) {
            if (obj == R.drawable.green_circular_layout) totalPresent++;
            else if (obj == R.drawable.red_circular_layout) totalAbsent++;
        }*/
        tv_total_absent_day.setText(totalAbsent + "\n" + getString(R.string.absent_status));
        tv_total_present_day.setText(totalPresent + "\n" + getString(R.string.present_status));
    }

    private class MyMonthAdapter extends ArrayAdapter<String> {
        Context context;
        String[] date_of_month;
        private int[] month_background_array;
        private LayoutInflater inflater;

        public MyMonthAdapter(Context context, String[] month_date_array, int[] month_background_array) {
            super(context, R.layout.day_homework, month_date_array);
            this.context = context;
            this.date_of_month = month_date_array;
            this.month_background_array = month_background_array;

        }

        @SuppressLint("NewApi")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.day_homework, null);
            }

            final ViewHolder holder = new ViewHolder();
            holder.convertview = convertView;

            holder.date_tv = (TextView) convertView.findViewById(R.id.month_date);
            holder.date_tv.setText(date_of_month[position]);

            if (date_of_month[position] == "" || position < 7)
                holder.date_tv.setBackground(null);
            else holder.date_tv.setBackgroundResource(month_background_array[position]);

            holder.date_tv.setTextColor(month_textcolor_array[position]);

            if (position < 7 || holder.date_tv.getText().toString().equalsIgnoreCase("") || holder.date_tv.getText().toString() == null)
                holder.date_tv.setEnabled(false);

            holder.date_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String dateName = date_of_month[position].length() == 1 ? "0" + date_of_month[position] : date_of_month[position];
                        getAttendance(dateMonthYear.format(month_calendar.getTime()) + dateName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (position < 7 || (holder.date_tv.getText().toString().equalsIgnoreCase("")))
                        ;
                    else
                        Toast.makeText(context, presentAbsent_array[position], Toast.LENGTH_SHORT).show();
                }
            });
            return convertView;
        }

        private class ViewHolder {
            TextView date_tv;
            View convertview;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(Config.isFromNotification, false)) {
            startActivity(new Intent(this, AdminNavigationDrawerNew.class));
            finish();

            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        } else {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }


    }

/*    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            // when user first touches the screen we get x and y coordinate
            case MotionEvent.ACTION_DOWN: {
                x1 = touchevent.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                x2 = touchevent.getX();

                //if left to right sweep event on screen
                if (x1 < x2) {
                  *//*  if (month_calendar.get(Calendar.MONTH) == month_calendar.getActualMinimum(Calendar.MONTH)) {
                        month_calendar.set((month_calendar.get(Calendar.YEAR) - 1), month_calendar.getActualMaximum(Calendar.MONTH), 1);
                    } else {
                        month_calendar.set(Calendar.MONTH, month_calendar.get(Calendar.MONTH) - 1);
                    }*//*
                    month_calendar.add(Calendar.MONTH, -1);
                    try {
                        getMonthAttendance(month_calendar);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    Toast.makeText(this, "Left to Right Swap Performed", Toast.LENGTH_LONG).show();
                }

                // if right to left sweep event on screen
                if (x1 > x2) {
                  *//*  if (month_calendar.get(Calendar.MONTH) == month_calendar.getActualMaximum(Calendar.MONTH)) {
                        month_calendar.set((month_calendar.get(Calendar.YEAR) + 1), month_calendar.getActualMinimum(Calendar.MONTH), 1);
                    } else {
                        month_calendar.set(Calendar.MONTH, month_calendar.get(Calendar.MONTH) + 1);
                    }*//*

                    month_calendar.add(Calendar.MONTH, 1);
                    try {
                        getMonthAttendance(month_calendar);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    Toast.makeText(this, "Right to Left Swap Performed", Toast.LENGTH_LONG).show();
                }

                break;
            }
        }
        return false;
    }*/

    private void getAttendance(final String date_name) throws JSONException {

        String studentID = "";
        if (sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Student") ||
                sharedPsreferences.getString("user_type", "Not Found").equalsIgnoreCase("Parent"))
            studentID = sharedPsreferences.getString("loggedUserID", "Not Found");
        else  if (studentObjArrayList.size() > 0 && sp_first_student.getSelectedItemPosition() != -1)
            studentID = /*sharedPreferences.getString("loggedUserID", "Not Found")*/ studentObjArrayList.get(sp_first_student.getSelectedItemPosition()).getStudentID();


        if (!studentID.equalsIgnoreCase("")) {
            progressDialog.show();
            progressDialog.setCancelable(false);

            final JSONObject param = new JSONObject();

            param.put("studentID", studentID);
            param.put("monthName", date_name);

            String attUrl = sharedPsreferences.getString("HostName", "Not Found")
                    + sharedPsreferences.getString(Config.applicationVersionName, "Not Found")
                    + ("StudentAttendanceDateWiseApi") + ".php?action=student&databaseName=" + db_name;
            Log.e("attUrl", attUrl);
            Log.e("param", param.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    attUrl, param
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject resJson) {
                    Log.d("TeacherAttRes", resJson.toString());
                    progressDialog.dismiss();
                    if (resJson.optString("code").equalsIgnoreCase("201")) {
                        AttendanceSlotWiseData resultObj = (new Gson()).fromJson(resJson.toString(), new TypeToken<AttendanceSlotWiseData>() {
                        }.getType());
                        ArrayList<AttendanceSlotWiseData> resultAttendanceArray = resultObj.getResult();

                        if (resultAttendanceArray.size() > 0) {
                            showSlotWiseAttendance(resultAttendanceArray, date_name);
                        }
                    } else if (resJson.optString("code").equalsIgnoreCase("503")
                            ||
                            resJson.optString("code").equalsIgnoreCase("511")) {
                        if (!isFinishing()) {
                            Config.responseVolleyHandlerAlert(ParentViewAttendance.this, resJson.optInt("code") + "", resJson.optString("message"));
                        }
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("error volley", volleyError.toString());
                    progressDialog.dismiss();

                    if (volleyError instanceof TimeoutError) {
                        Toast.makeText(ParentViewAttendance.this, "TimeoutError", Toast.LENGTH_LONG).show();
                    } else if (volleyError instanceof AuthFailureError) {
                        Toast.makeText(ParentViewAttendance.this, "AuthFailureError", Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        Toast.makeText(ParentViewAttendance.this, "ServerError", Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        Toast.makeText(ParentViewAttendance.this, "NetworkError", Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        Toast.makeText(ParentViewAttendance.this, "ParseError", Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (volleyError instanceof NoConnectionError) {
                        Toast.makeText(ParentViewAttendance.this, "NoConnectionError", Toast.LENGTH_LONG).show();
                        //TODO
                    }


                }
            });


            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request, "fetchAttendance");
        }
    }

    private void showSlotWiseAttendance(ArrayList<AttendanceSlotWiseData> resultAttendanceArray, String date_name) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        View itemTimeMappingView = LayoutInflater.from(this).inflate(R.layout.student_list_for_selection, null);
        itemTimeMappingView.findViewById(R.id.linear_layout_student_selection).setVisibility(View.GONE);
        ((FrameLayout) itemTimeMappingView.findViewById(R.id.student_list_for_selection_activity)).setBackground(null);
        RecyclerView recycler_view = (RecyclerView) itemTimeMappingView.findViewById(R.id.recycler_view_student_selection_layout);

        ArrayList<AttendanceSlotWiseData> resultAttendanceArrayNew = new ArrayList<AttendanceSlotWiseData>();
        for (AttendanceSlotWiseData obj : resultAttendanceArray) {
            if (obj.getSlotID() == null || obj.getSlotID().equalsIgnoreCase("0")) ;
            else resultAttendanceArrayNew.add(obj);
        }
        AttendanceSlotWiseDetailAdapter madapter = new AttendanceSlotWiseDetailAdapter(this, resultAttendanceArrayNew, colorIs);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);

        if (resultAttendanceArrayNew.size() > 0) {
            recycler_view.setAdapter(madapter);
            alertDialog.setMessage(getString(R.string.slot_information) + " : " + date_name);
            alertDialog.setView(itemTimeMappingView);
            alertDialog.show();
        } else Log.e("ritu", "size zero");


    }
}
