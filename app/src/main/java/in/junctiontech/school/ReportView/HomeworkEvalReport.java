package in.junctiontech.school.ReportView;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkEvaluations;

/**
 * Created by JAYDEVI BHADE on 9/2/2017.
 */

public class HomeworkEvalReport extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private DbHandler db;
    private ArrayList<HomeworkEvaluations> homeworkEvaluationsList;
    private MyRecycleAdapter madapter;
    private RecyclerView my_recycler_view;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_evaluation_third);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));
        db = DbHandler.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setSubtitle(getString(R.string.subject) + " : " + getIntent().getStringExtra("subjectName"));
        setColorApp();
        homeworkEvaluationsList =
                db.getStudentNameForHomeworkReportEval(getIntent().getStringExtra("evaluations"));


        //  homeworkEvaluationsList = (ArrayList<HomeworkEvaluations>) getIntent().getSerializableExtra("evaluationsArrayList");

        my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);

        madapter = new MyRecycleAdapter(this, homeworkEvaluationsList);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            my_recycler_view.setLayoutManager(gridLayoutManager);
        } else {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            my_recycler_view.setLayoutManager(gridLayoutManager);
        }

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        my_recycler_view.setItemAnimator(itemAnimator);
        my_recycler_view.setAdapter(madapter);
        madapter.notifyDataSetChanged();

        findViewById(R.id.ll_homework_evaluation_button).setVisibility(View.GONE);

    }


    public class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {
        Context context;

        List<HomeworkEvaluations> homeworkEvaluationsList;

        public MyRecycleAdapter(Context context, ArrayList<HomeworkEvaluations> dataList) {
            this.context = context;
            this.homeworkEvaluationsList = dataList;

        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_all_student, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            final HomeworkEvaluations obj = homeworkEvaluationsList.get(position);
            String next = "<font color='#727272'>" + "(" + obj.getId() + ")" + "</font>";
            holder.name_check.setText(Html.fromHtml(obj.getName() + " " + next));

            Log.e("ritu", obj.getName() + obj.getId() + obj.getStatus());

            if (obj.getStatus().equalsIgnoreCase("C")) {
                holder.name_check.setChecked(true);
                holder.ly_view_all_student.setBackgroundResource(R.drawable.selected_view);

            }
            if (obj.getStatus().equalsIgnoreCase("NE")) {
                holder.name_check.setChecked(true);
                holder.ly_view_all_student.setBackgroundResource(R.drawable.cliclable_view);

            } else if (obj.getStatus().equalsIgnoreCase("INC")) {
                holder.ly_view_all_student.setBackgroundResource(R.drawable.red_background);
                holder.name_check.setChecked(false);
            }


        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return homeworkEvaluationsList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            LinearLayout ly_view_all_student;
            CheckBox name_check;
//            TextView name_of_studence;

            public MyViewHolder(View itemView) {
                super(itemView);
                name_check = (CheckBox) itemView.findViewById(R.id.ck_name_check);
                name_check.setTextColor(getResources().getColor(android.R.color.black));
                //   name_of_studence = (TextView) itemView.findViewById(R.id.tv_name_of_studence);
                ly_view_all_student = (LinearLayout) itemView.findViewById(R.id.ly_view_all_student);

            }


        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

}
