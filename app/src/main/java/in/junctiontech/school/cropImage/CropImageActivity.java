package in.junctiontech.school.cropImage;

import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.StaffInformation;
import in.junctiontech.school.schoolnew.model.StudentInformation;

// probably not being used

public class CropImageActivity extends AppCompatActivity
        implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {

    SchoolDetailsEntity schoolDetails = new SchoolDetailsEntity();


    MainDatabase mDb ;
    private SharedPreferences sp;
    private static int IMG_RESULT = 1;
    private CropImageView mCropImageView;
    private Bitmap bitmap;
    private int aa = 1;
    private ProgressDialog progressBar;
    private File targetLocation, targetProfileImage, targetSchoolImageLocation;
    private RelativeLayout rl_school_logo;
    private Uri filePath;
    private Button btn_crop_image_pick_image;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        mDb = MainDatabase.getDatabase(this);
        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, new Observer<SchoolDetailsEntity>() {
            @Override
            public void onChanged(@Nullable SchoolDetailsEntity schoolDetailsEntity) {

                if(!(schoolDetailsEntity==null)) {
                    schoolDetails = schoolDetailsEntity;
                }
            }
        });

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage(getString(R.string.uploading));

        setColorApp();
        rl_school_logo =  findViewById(R.id.activity_crop_image);
        btn_crop_image_pick_image =  findViewById(R.id.btn_crop_image_pick_image);
        sp = Prefs.with(this).getSharedPreferences();
        targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                sp.getString("organization_name", "") + "_logo" + ".jpg");
        targetSchoolImageLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                sp.getString("organization_name", "") + "_school_image" + ".jpg");
        targetProfileImage = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/Profile" + "/",
                sp.getString("organization_name", "") + "_" + sp.getString("loggedUserID", "")
                        + "_" + sp.getString("user_type", "") + ".jpg");

     /*   byte[] bytes = getIntent().getByteArrayExtra("BMP");
         bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);*/
        //  bitmap = getIntent().getParcelableExtra("bitmap");

        mCropImageView = findViewById(R.id.cropImageView);
        mCropImageView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);

        File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME);
        if (!f.exists()) {
            f.mkdirs();
            //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
            Log.e("Directory Created", "");
        } else
            Toast.makeText(CropImageActivity.this, "Directory Created", Toast.LENGTH_LONG).show();


        pickImage();

        //  updateCurrentCropViewOptions();
        //   mCropImageView.setImageBitmap(bitmap);
        btn_crop_image_pick_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);


        Uri uriSavedImage = Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString()
                + "/" + getString(R.string.zeroerp_school)));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(intent, IMG_RESULT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_RESULT && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {

            filePath = data.getData();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = 1;
            bitmap = decodeFile(new File(getPath(filePath)));

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            if (getIntent().getBooleanExtra("isSchoolImage", false)) {
                try {
                    uploadImage(getStringImage(bitmap),"schoolImage");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else updateCurrentCropViewOptions();

            mCropImageView.setImageBitmap(bitmap);
            btn_crop_image_pick_image.setVisibility(View.GONE);

        } else {
            btn_crop_image_pick_image.setVisibility(View.VISIBLE);
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            // Decode image size

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 100;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;

            long length = f.length() / 1024;
            Log.e("imageSize", length + " size required " + REQUIRED_SIZE);
            if (length > 50) {
                while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                        o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                    scale *= 2;
                }
                // Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            } else {
                // Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            }


        } catch (FileNotFoundException e) {
        }
        return null;
    }

    void uploadImage(String stringImageBase, final String type) throws Exception {

        final JSONObject param = new JSONObject();

        switch (type){
                    case "Logo":
                        param.put("Logo", stringImageBase);
                        break;
                    case "schoolImage":
                        param.put("schoolImage", stringImageBase);
                        break;
                    case "Profile":
                        break;
                }

                String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "schoolDetail/schoolDetails/"
                + new JSONObject().put("id", schoolDetails.Id)
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{

                            schoolDetails = new Gson()
                                    .fromJson(job.getJSONObject("result").optString("schoolDetails"),
                                            SchoolDetailsEntity.class);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mDb.schoolDetailsModel().updateSchoolDetails(schoolDetails);
                                }
                            }).start();

                            HashMap<String , String> setDefaults = new HashMap<>();
                            switch (type){
                                case "Logo":
                                    setDefaults.put(Gc.LOGO_URL, schoolDetails.Logo);
                                    break;
                                case "schoolImage":
                                    setDefaults.put(Gc.SCHOOLIMAGE, schoolDetails.schoolImage);
                                    break;
                                case "Profile":
                                    break;
                            }
                            Gc.setSharedPreference(setDefaults, CropImageActivity.this);

                            Intent intent = new Intent();
                            intent.putExtra("message", getString(R.string.success));
                            setResult(RESULT_OK, intent);
                            finish();

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, CropImageActivity.this);
                        finish();

                    default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.activity_crop_image),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
            Config.responseVolleyErrorHandler(CropImageActivity.this,error,findViewById(R.id.activity_crop_image));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void updateCurrentCropViewOptions() {
        CropImageViewOptions options = new CropImageViewOptions();
        options.scaleType = mCropImageView.getScaleType();
        options.cropShape = mCropImageView.getCropShape();
        options.guidelines = mCropImageView.getGuidelines();
        options.aspectRatio = mCropImageView.getAspectRatio();
        options.fixAspectRatio = mCropImageView.isFixAspectRatio();
        options.showCropOverlay = mCropImageView.isShowCropOverlay();
        options.showProgressBar = mCropImageView.isShowProgressBar();
        options.autoZoomEnabled = mCropImageView.isAutoZoomEnabled();
        options.maxZoomLevel = mCropImageView.getMaxZoom();
        //setCurrentOptions(options);
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    private void handleCropResult(CropImageView.CropResult result) {
     /*   Intent intent= null;
      if (getIntent().getBooleanExtra("IsProfile",false))
           intent = new Intent(this, PersonalProfile.class);
        else   intent = new Intent(this, SchoolLogoActivity.class);

        if (result.getError() == null) {*/

          bitmap = mCropImageView.getCropShape() == CropImageView.CropShape.OVAL
                ? CropImage.toOvalBitmap(result.getBitmap())
                : result.getBitmap();


        if (getIntent().getBooleanExtra("IsProfile", false)) {
            try {
                uploadProfileImage(bitmap);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            try {
                uploadImage(getStringImage(bitmap), "Logo");
            } catch (Exception e) {
                e.printStackTrace();
            }


           /* ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bytes = stream.toByteArray();
            intent.putExtra("BMP", bytes);
          *//*  intent.putExtra("bitmap", bitmap);*/
        // setResult(RESULT_OK, intent);

/*
        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(this, "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();

            setResult(RESULT_CANCELED, intent);
        }

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);*/
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(this, "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(this, "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void finishActivity(boolean isSuccess) {
        progressBar.dismiss();
        Intent intent = new Intent();
       /* if (getIntent().getBooleanExtra("IsProfile", false))
            intent = new Intent(this, PersonalProfile.class);
        else intent = new Intent(this, SchoolLogoActivity.class);*/
        if (isSuccess) setResult(RESULT_OK, intent);
        else setResult(RESULT_CANCELED, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onBackPressed() {
        finishActivity(false);
        //  mCropImageView.getCroppedImageAsync();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_image_crop, menu);

        return getIntent().getBooleanExtra("isSchoolImage", false) ? false : true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.main_action_crop) {
            mCropImageView.getCroppedImageAsync();
            return true;
        } else if (item.getItemId() == R.id.main_action_rotate) {
            mCropImageView.rotateImage(90);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void uploadProfileImage(Bitmap stringImageBase) throws JSONException {

        final JSONObject param = new JSONObject();

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION;

        String role = getIntent().getStringExtra(Gc.SIGNEDINUSERROLE);
        switch (role){
            case Gc.STAFF:
                StaffInformation staff = new Gson().fromJson(getIntent().getStringExtra(Gc.STAFF), StaffInformation.class);
                url = url + "staff/staffRegistration/"
                        + new JSONObject().put("StaffId", staff.StaffId);

                param.put("staffProfileImage",
                        new JSONObject().put("Type", "jpeg").put("StaffProfile", stringImageBase));
                break;
            case Gc.STUDENTPARENT:
                StudentInformation student = new Gson().fromJson(getIntent().getStringExtra(Gc.STUDENTPARENT), StudentInformation.class);
                url = url + "studentParent/studentRegistration/"
                        + new JSONObject().put("RegistrationId",student.RegistrationId);

                param.put("studentProfileImage",
                        new JSONObject().put("Type", "png").put("StudentProfile",stringImageBase ));
                break;

                default:
                    return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.cancel();
                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:
                        try{
                            JSONObject resultjobj = job.getJSONObject("result");


                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                }
                            }).start();
                            //         Config.responseSnackBarHandler(job.optString("message"),
//                                findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_green);
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;



                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, CropImageActivity.this);
                        finish();

                    default:
//                    Config.responseSnackBarHandler(job.optString("message"),
//                            findViewById(R.id.ll_snackbar_create_fee_type),R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.cancel();
//            Config.responseVolleyErrorHandler(FeeTypeActivity.this,error,findViewById(R.id.ll_snackbar_create_fee_type));

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public void uploadProfile(Bitmap stringImageBase) throws JSONException {
      /*  if (!Config.checkInternet(this)) {

            Toast.makeText(this, getString(R.string.internet_not_available_please_check_your_internet_connectivity), Toast.LENGTH_SHORT).show();

        } else {*/
        progressBar.show();
        String identity = "";
        final JSONObject param = new JSONObject();
        param.put("userType", sp.getString("user_type", ""));

        if (sp.getString("user_type", "").equalsIgnoreCase("Parent")) {
            param.put("AdmissionId", sp.getString("loggedUserID", ""));
            identity = "parent";
            param.put("parentProfileImage", getStringImage(stringImageBase));

        } else if (sp.getString("user_type", "").equalsIgnoreCase("Student")) {
            param.put("AdmissionId", sp.getString("loggedUserID", ""));
            identity = "student";
            param.put("studentProfileImage", getStringImage(stringImageBase));
        } else {
            param.put("StaffId", sp.getString("loggedUserID", ""));
            identity = "staff";
            param.put("staffProfileImage", getStringImage(stringImageBase));

        }

        String profileUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                +Config.applicationVersionNameValue + "ImageUploadApi.php?" + "databaseName=" +
                Gc.getSharedPreference(Gc.ERPDBNAME,this) + "&action=profileImageUpload&identity=" + identity;

        Log.e("ProfileUploadUrl", profileUrl);
        Log.e("param", param.toString());
        DbHandler.longInfo(param.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, profileUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("profileRes", jsonObject.toString());

                        JSONObject jsonObjectUpload = jsonObject;
                        if (jsonObjectUpload.optString("code").equalsIgnoreCase("200")
                                ||
                                jsonObjectUpload.optString("code").equalsIgnoreCase("201")) {


                            sp.edit().putString(Gc.PROFILE_URL, jsonObjectUpload.optString("result")).apply();
                            //  generalSettingDataObj.setLogoUrl(jsonObjectUpload.optString("result"));

//                            saveBitmap(bitmap);
                            //write the bytes in file

                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,

                                    getString(R.string.uploaded_successfully),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                            snackbarObj.show();
                        } else {
                            progressBar.dismiss();
                            Toast.makeText(CropImageActivity.this, "Failed to upload Profile Image !", Toast.LENGTH_SHORT).show();
                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,

                                    getString(R.string.failed_to_upload),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
                            snackbarObj.show();
                        }
                        progressBar.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("profileResError", volleyError.toString());
                Toast.makeText(CropImageActivity.this, "Failed to upload Profile Image !", Toast.LENGTH_SHORT).show();
                progressBar.cancel();
                Config.responseVolleyErrorHandler(CropImageActivity.this, volleyError, rl_school_logo);

            }
        });

  /*      StringRequest request = new StringRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                profileUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());

                DbHandler.longInfo(new JSONObject(params).toString());
                Log.e("profileResPost", new JSONObject(params).toString());
                return params;
            }
        };
*/
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        // }
    }

    public void uploadLogo(String stringImageBase) throws Exception {

        progressBar.show();
        final JSONObject param = new JSONObject();
        param.put("logo", stringImageBase);
        param.put("Id", getIntent().getStringExtra("Id"));

        String logoUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "GeneralSettingApi.php?" + "databaseName=" +
                Gc.getSharedPreference(Gc.ERPDBNAME,this) + "&action=logoUpload";

        Log.e("SchoolLogoUrl", logoUrl);
        Log.e("param", param.toString());


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, logoUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("SchoolLogoRes", jsonObject.toString());

                        JSONObject jsonObjectUpload = jsonObject;
                        if (jsonObjectUpload.optString("code").equalsIgnoreCase("201")) {
                            progressBar.setMessage(getString(R.string.saving));
                            progressBar.show();
                            sp.edit().putString(Config.LOGO_URL, jsonObjectUpload.optString("result")).commit();
                            //  generalSettingDataObj.setLogoUrl(jsonObjectUpload.optString("result"));

                            Intent broadcastLogoUpdated = new Intent(Config.LOGO_UPDATED);
                            LocalBroadcastManager.getInstance(CropImageActivity.this).sendBroadcast(broadcastLogoUpdated);


                            /******** Save logo into directory *******/

//                            saveBitmap(bitmap);

                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,
                                    (getString(R.string.school_logo) + " " +
                                            getString(R.string.uploaded_successfully)),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                            snackbarObj.show();
                            /***********************************************/
                        } else {
                            Toast.makeText(CropImageActivity.this, getString(R.string.failed_to_upload),
                                    Toast.LENGTH_SHORT).show();
                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,
                                    (getString(R.string.school_logo) + " " +
                                            getString(R.string.failed_to_upload)),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources()
                                    .getColor(android.R.color.holo_red_dark));
                            snackbarObj.show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("SchoolLogoError", volleyError.toString());
                progressBar.cancel();

                Config.responseVolleyErrorHandler(CropImageActivity.this, volleyError, rl_school_logo);
            }
        });

        /*StringRequest request = new StringRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                logoUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());

                DbHandler.longInfo(new JSONObject(params).toString());
                Log.e("SchoolLogoPost", new JSONObject(params).toString());
                return params;
            }
        };
*/
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    private void uploadSchoolImage(String stringImageBase) throws Exception {

        progressBar.show();
        final JSONObject param = new JSONObject();
        param.put("schoolImage", stringImageBase);
        param.put("Id", getIntent().getStringExtra("Id"));

        //   progressBar.show();

        String logoUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "GeneralSettingApi.php?" + "databaseName=" +
                Gc.getSharedPreference(Gc.ERPDBNAME,this) + "&action=schoolImageUpload";

        Log.e("SchoolImageUrl", logoUrl);
        Log.e("param", param.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, logoUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("SchoolImage", jsonObject.toString());

                        JSONObject jsonObjectUpload = jsonObject;
                        if (jsonObjectUpload.optString("code").equalsIgnoreCase("201")) {
                            progressBar.setMessage(getString(R.string.saving));
                            progressBar.show();
                            sp.edit().putString("schoolImageUrl", jsonObjectUpload.optString("result")).commit();
                            //  generalSettingDataObj.setLogoUrl(jsonObjectUpload.optString("result"));

                            Intent broadcastLogoUpdated = new Intent(Config.SCHOOL_IMAGE_UPDATED);
                            LocalBroadcastManager.getInstance(CropImageActivity.this).sendBroadcast(broadcastLogoUpdated);


                            /******** Save logo into directory *******/

//                            saveBitmap(bitmap);


                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,
                                    (getString(R.string.school_image) + " " +
                                            getString(R.string.uploaded_successfully)),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                            snackbarObj.show();
                            /***********************************************/
                        } else {
                            Toast.makeText(CropImageActivity.this, getString(R.string.failed_to_upload),
                                    Toast.LENGTH_SHORT).show();
                            Snackbar snackbarObj = Snackbar.make(rl_school_logo,
                                    (getString(R.string.school_image) + " " +
                                            getString(R.string.failed_to_upload)),
                                    Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                            snackbarObj.getView().setBackgroundColor(getResources()
                                    .getColor(android.R.color.holo_red_dark));
                            snackbarObj.show();
                            finishActivity(false);
                        }
                        progressBar.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("school_imageError", volleyError.toString());
                progressBar.cancel();

                Config.responseVolleyErrorHandler(CropImageActivity.this, volleyError, rl_school_logo);
                finishActivity(false);
            }
        });


      /*  StringRequest request = new StringRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                logoUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("school_imageError", volleyError.toString());
                progressBar.cancel();

                String msg = "Error !";
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                    msg = getString(volleyError instanceof TimeoutError ? R.string.error_network_timeout : R.string.please_retry_error_occurred);

                } else if (volleyError instanceof AuthFailureError) {
                    msg = "AuthFailureError!";
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    msg = "ServerError!";
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    msg = "NetworkError!";
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    msg = "ParseError!";
                    //TODO
                }
                Snackbar.make(rl_school_logo, msg, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
                finishActivity(false);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", new JSONObject(param).toString());

                DbHandler.longInfo(new JSONObject(params).toString());
                Log.e("school_imagePost", new JSONObject(params).toString());
                return params;
            }
        };*/

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void saveBitmap(Bitmap bitmap) {

   /*     ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 *//*ignored for PNG*//*, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
       *//* File targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + "MySchool" + "/",
                "VVVVV" + aa + "_logo" + ".jpg");*//*
        try {
            fos = new FileOutputStream(targetLocation);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }*/

        Log.e("ritu", "Copy file successful.");
        File sourceLocation = new File(getPath(filePath));

        if (sourceLocation.exists()) {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();


            if (getIntent().getBooleanExtra("IsProfile", false)) {
                File dir = targetProfileImage.getParentFile();


                if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                    File f = new File(Environment.getExternalStorageDirectory(), Config.FOLDER_NAME + "/Profile");
                    if (!f.exists()) {
                        f.mkdirs();
                        //  Toast.makeText(getContext(),"Directory Created", Toast.LENGTH_LONG).show();
                        Log.e("Directory Created", "");
                    }

                }
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(targetProfileImage);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }


            } else {


                //write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(getIntent().getBooleanExtra("isSchoolImage", false) ? targetSchoolImageLocation : targetLocation);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }


                Log.e("ritu", "Copy file successful.");


            }
        }

        finishActivity(true);
    }



}
