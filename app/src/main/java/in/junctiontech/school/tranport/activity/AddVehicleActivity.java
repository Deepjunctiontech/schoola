package in.junctiontech.school.tranport.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.tranport.model.vehicledata.VehicleDataItem;


public class AddVehicleActivity extends AppCompatActivity {

    private EditText et_name, et_number;
    private String vehicle_id;
    private Button btn_add_vehicle;
    private ProgressBar pb;
    private SharedPreferences sp;
    private int  colorIs;
    private boolean isDataChanged= false;

    private void setColorApp() {
        colorIs = Config.getAppColor(this,true);
        //  getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView)findViewById(R.id.tv_add_vehicle_name)).setTextColor(colorIs);
        ((TextView)findViewById(R.id.tv_add_vehicle_vehicle_number)).setTextColor(colorIs);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);
        sp = Prefs.with(this).getSharedPreferences();
        actionBarEdit();
        initViews();
        setColorApp();
    }

    private void initViews() {
        Intent currentIntent = this.getIntent();
        et_name = (EditText) findViewById(R.id.activity_add_vehicle_et_name);
        et_number = (EditText) findViewById(R.id.activity_add_vehicle_et_number);
        btn_add_vehicle = (Button) findViewById(R.id.activity_add_vehicle_btn_add_vehicle);
        pb = (ProgressBar) findViewById(R.id.activity_add_vehicle_pb);

        if (currentIntent != null) {
            String vehicle_name = currentIntent.getStringExtra("vehicle_name");
            String vehicle_number = currentIntent.getStringExtra("vehicle_number");
            if (vehicle_name != null && vehicle_number != null) {
                et_name.setText(vehicle_name);
                et_name.setSelection(et_name.getText().length());

                et_number.setText(vehicle_number);
                et_number.setSelection(et_number.getText().length());
            }

            vehicle_id = currentIntent.getStringExtra("vehicle_id");

        }

     /*   InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                String filtered = "";
                for (int i = start; i < end; i++) {
                    char character = source.charAt(i);
                    if (!Character.isWhitespace(character)) {
                        filtered += character;
                    }
                }

                return filtered;
            }

        };
        et_number.setFilters(new InputFilter[] { filter });*/



       /* et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0)
                {
                   *//* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();*//*
                    AppController.getInstance().cancelPendingRequests("getVehicleDataFromServer");

                    "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                            "{\"filter\":{\"VehicleNumber\":\"RouteStoppage\",\"VehicleNumber\":\""+s.replace(" ","+")+"\"}}"
                    getVehicleDataFromServer(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });  */
        // TODO: 2/15/2017 Not Required to match vehicle name, name can be same,as per discussion

        et_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0)
                {
                   /* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();*/
                    AppRequestQueueController.getInstance(AddVehicleActivity.this).cancelRequestByTag("getVehicleDataFromServer");
                  //  AppRequestQueueController.getInstance(AddVehicleActivity.this).cancelPendingRequests("getVehicleDataFromServer");

                    String url= sp.getString("HostName", "Not Found")
                            + sp.getString(Config.applicationVersionName, "Not Found")
                            +"VehicleApi.php?databaseName="
                            +sp.getString("organization_name", "")
                            +"&data="+
                            "{\"filter\":{\"VehicleNumber\":\""+s.toString().replace(" ","+")+"\"}}";

                    Log.e("url",url);
                    getVehicleDataFromServer( url
                           /* "http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217&data=" +
                            "{\"filter\":{\"VehicleNumber\":\""+s.toString().replace(" ","+")+"\"}}"*/);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }

    public void onClickAddVehicle(View view) throws JSONException {
        String current_vehicle_name = et_name.getText().toString();
        String current_vehicle_no = et_number.getText().toString();
        if (current_vehicle_name.isEmpty() && current_vehicle_no.isEmpty()) {
            et_name.requestFocus();
            et_name.setError(getString(R.string.field_can_not_be_blank));
            et_number.setError(getString(R.string.field_can_not_be_blank));
        } else if (current_vehicle_name.isEmpty()) {
            et_name.requestFocus();
            et_name.setError(getString(R.string.field_can_not_be_blank));
            et_number.setError(null);
        } else if (current_vehicle_no.isEmpty()) {
            et_number.requestFocus();
            et_number.setError(getString(R.string.field_can_not_be_blank));
            et_name.setError(null);
        } else {
            et_number.setError(null);
            et_name.setError(null);
          //  Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();

            if (vehicle_id != null) {
                // Toast.makeText(this, "Update", Toast.LENGTH_SHORT).show();
                VehicleDataItem vehicleDataItem = new VehicleDataItem();
                vehicleDataItem.setVehicleId(vehicle_id);
                vehicleDataItem.setVehicleName(current_vehicle_name);
                vehicleDataItem.setVehicleNumber(current_vehicle_no);

                putVehicleDataToServer(vehicleDataItem);
            } else {
                // Toast.makeText(this, "New", Toast.LENGTH_SHORT).show();
                VehicleDataItem vehicleDataItem = new VehicleDataItem();
                vehicleDataItem.setVehicleStatus("Active");
                vehicleDataItem.setVehicleName(current_vehicle_name);
                vehicleDataItem.setVehicleNumber(current_vehicle_no);
                vehicleDataItem.setDOE("");
                vehicleDataItem.setDOL("");

                postVehicleDataToServer(vehicleDataItem);
            }


        }


    }

    private void actionBarEdit() {
        /*Toolbar tb = (Toolbar)findViewById(R.id.activity_transport_toolbar);
        setSupportActionBar(tb);*/
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
            TextView textview = new TextView(getApplicationContext());
            RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            textview.setLayoutParams(layoutparams);
            textview.setText(getString(R.string.vehicle));
            //textview = (TextView) this.findViewById(R.id.toolbar_title_feature);
            textview.setSingleLine();
            textview.setTextColor(Color.WHITE);
            textview.setEllipsize(TextUtils.TruncateAt.END);
            textview.setGravity(Gravity.CENTER);
            textview.setTextSize(18);
            textview.setTypeface(Typeface.SERIF);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            actionBar.setCustomView(textview, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //  actionBar.setIcon(R.drawable.rateourapp);
            // actionBar.setHomeButtonEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(isDataChanged?RESULT_OK:RESULT_CANCELED,intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent();
       setResult(isDataChanged?RESULT_OK:RESULT_CANCELED,intent);
        finish();
        // overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        return true;
    }

    private void postVehicleDataToServer(final VehicleDataItem vehicleDataItem) throws JSONException {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();

        JSONObject param = new JSONObject(new Gson().toJson(vehicleDataItem));

        String url= sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +"VehicleApi.php?databaseName="+sp.getString("organization_name", "")
              ;

        Log.e("url",url);
        Log.e("param",param.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                //"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107",
//                "http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217"
                url,param
                ,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        Log.d("postVehicleDataToServer", jsonObject.toString());

                            String data = jsonObject.optString("code", "key not found");
                            if ("201".equalsIgnoreCase(data)) {
                                isDataChanged = true;
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddVehicleActivity.this);
                                alertDialog.setMessage(getString(R.string.save_successfully));
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) { finish(); }
                                });
                                if (!AddVehicleActivity.this.isFinishing())
                                    alertDialog.show();

                             }



                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = getString(R.string.internet_not_available_please_check_your_internet_connectivity);
                }
                Config.showToast(AddVehicleActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        })  ;
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(strReq, "postVehicleDataToServer");
/*        AppController.getInstance().addToRequestQueue(strReq, "postVehicleDataToServer");*/
        //queue.add(strReq);


    }


    private void putVehicleDataToServer(final VehicleDataItem vehicleDataItem) {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();

        String url= sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +"VehicleApi.php?databaseName="+sp.getString("organization_name", "")
                +"&data=" +
                "{\"data\":{\"VehicleName\":\"" + vehicleDataItem.getVehicleName()
                .replace(" ", "+") + "\",\"VehicleNumber\":\"" +
                vehicleDataItem.getVehicleNumber().replace(" ", "+") + "\"},\"filter\":{\"VehicleId\":\""
                + vehicleDataItem.getVehicleId() + "\"}}"
                ;

        Log.e("url",url);

        StringRequest strReq = new StringRequest(Request.Method.PUT,
                url
                /*"http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217&data=" +
                        "{\"data\":{\"VehicleName\":\"" + vehicleDataItem.getVehicleName()
                        .replace(" ", "+") + "\",\"VehicleNumber\":\"" +
                        vehicleDataItem.getVehicleNumber().replace(" ", "+") + "\"},\"filter\":{\"VehicleId\":\""
                        + vehicleDataItem.getVehicleId() + "\"}}"*/
                ,
                //   "&data={\"data\":{\"VehicleNumber\":\""+vehicleDataItem.getVehicleNumber().replace(" ","+")+"\",\"VehicleName\": \""+vehicleDataItem.getVehicleName().replace(" ","+")+"\"},\"filter\":{\"VehicleId\":\""+vehicleDataItem.getVehicleId()+"\"}}",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("putVehicleDataToServer", response);
                      /*  Log.d("URL", "http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217&data=" +
                                "{\"data\":{\"VehicleName\":\"" + vehicleDataItem.getVehicleName().replace(" ", "+") + "\",\"VehicleNumber\":\"" + vehicleDataItem.getVehicleNumber().replace(" ", "+") + "\"},\"filter\":{\"VehicleId\":\"" + vehicleDataItem.getVehicleId() + "\"}}");
                       */ try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                isDataChanged = true;
                                /*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*/
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddVehicleActivity.this);
                                alertDialog.setMessage(getString(R.string.update_successfully));
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if (!AddVehicleActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {


                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = getString(R.string.internet_not_available_please_check_your_internet_connectivity);
                }
                Config.showToast(AddVehicleActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
     //   AppController.getInstance().addToRequestQueue(strReq, "putVehicleDataToServer");

        AppRequestQueueController.getInstance(this).addToRequestQueue(strReq, "putVehicleDataToServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }


    private void getVehicleDataFromServer(final String URL)  {
        pb.setVisibility(View.VISIBLE);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                /*"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s+"\"}}",*/
                URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("getVehicleData", response);
                        Log.d("URL",  URL);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                //  Toast.makeText(AddStopsActivity.this, "Found", Toast.LENGTH_SHORT).show();
                                //et_name.setError("Already exist");

                                if (Build.VERSION.SDK_INT >= 24)
                                {
                                    et_number.setError(Html.fromHtml("<font color='red'>"+getString(R.string.already_exist)+"</font>",Html.FROM_HTML_MODE_LEGACY));

                                }
                                else
                                {
                                    et_number.setError(Html.fromHtml("<font color='red'>"+getString(R.string.already_exist)+"</font>"));
                                }

                                btn_add_vehicle.setVisibility(View.INVISIBLE);
                               // btn_add_vehicle.animate().translationX(btn_add_vehicle.getWidth() + 16)
                               //         .setInterpolator(new AccelerateInterpolator(2)).start();
                                return;

                              /*  JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                   // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                    //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                    //stopDataItemList.add(0,new StopDataItem());
                                    // stopDataAdapter.insertAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();
                                    for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();

                                    //IMP
                                    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);
                                }*/

                            }
                            else {
                                et_name.setError(null);
                                btn_add_vehicle.setVisibility(View.VISIBLE);
                              //  btn_add_vehicle.animate().translationX(0)
                              //          .setInterpolator(new DecelerateInterpolator(2)).start();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pb.setVisibility(View.INVISIBLE);

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = getString(R.string.internet_not_available_please_check_your_internet_connectivity);
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put("placekey", s); old
                return params;
            }
*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
      //  AppController.getInstance().addToRequestQueue(strReq,"getVehicleDataFromServer");
        AppRequestQueueController.getInstance(this).addToRequestQueue(strReq, "getVehicleDataFromServer");

        //queue.add(strReq);


    }

}
