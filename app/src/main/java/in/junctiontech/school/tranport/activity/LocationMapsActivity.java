package in.junctiontech.school.tranport.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.tranport.model.stopdata.StopDataItem;
import in.junctiontech.school.tranport.model.vehicledata.VehicleDataItem;
import in.junctiontech.school.tranport.model.vehicleroutedata.VehicleRouteDataItem;


public class LocationMapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private GoogleMap mMap;
    /*private PMSDatabase pmsDatabase;
    private ArrayList<LatLng> latlng;*/
    private String start_destination = "start";
    private LatLng latlong_start, latlong_destination;
    // private IconGenerator iconFactory;
    private LatLng latlong_waypoint;
    private List<LatLng> latlong_waypoint_list = new ArrayList<>();
    private boolean route_create;
    private String route_url;
    private ArrayList<String> route_list;
    private ArrayAdapter adapterProject;
    private Spinner spn_select_route;
    private TabLayout tablayout_create_display;
    private TabLayout tablayout_create_map;
    private FloatingActionButton map_fab_edit, map_fab_delete;
    private LinearLayout ll_select_route;
    private String route_name = "";
    private boolean fab_click;
    private HashMap<Marker, String> mHashMap = new HashMap<Marker, String>();
    private String start_title, start_snippet, destination_title, destination_snippet;
    private List<String> time_waypoint_list = new ArrayList<>();
    private List<String> address_waypoint_list = new ArrayList<>();
    private String currentMarkerInfo;
    private Marker currentMarkerSelected;
    private String route_save_not = "default";
    private List<String> route_to = new ArrayList<>();
    private List<String> route_to_id = new ArrayList<>();
    private LocationManager lm;
    private Location default_current_lat_long;
    private float map_zoom_level;
    private ProgressBar pb;
    private ArrayAdapter adapter_suggest_dropdown;
    private List<String> stopList = new ArrayList<>();
    private List<StopDataItem> stopDataItemList = new ArrayList<>();
    private AutoCompleteTextView tet_edit_address;
    private AutoCompleteTextView tet_edit_vehicle_name;
    private List<String> vehicleList = new ArrayList<>();
    private List<VehicleDataItem> vehicleDataItemList = new ArrayList<>();
    private String vehicle_id_server;
    private StopDataItem current_stopDataItem;
    private StopDataItem start_stop;
    private StopDataItem destination_stop;
    private List<StopDataItem> stopDataItemList_stop = new ArrayList<>();
    private VehicleRouteDataItem vehicleRouteData;
    private List<VehicleRouteDataItem> vehicleRouteDataItemList = new ArrayList<>();
    private List<StopDataItem> stopDataItemList_route = new ArrayList<>();

    private VehicleRouteDataItem vehicleRoutedataItem_click_edit;
    private Marker currentMarker;
    private TextInputEditText tet_edit_vehicle_no;
    private SharedPreferences sp;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private ProgressDialog loader_get_current_location;
    private int colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this, true);
        //   getWindow().setStatusBarColor(colorIs);
        //    getWindow().setNavigationBarColor(colorIs);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_maps);
//        sp = Prefs.with(this).getSharedPreferences();
        actionBarEdit();
        setColorApp();
        // getCurrentLocationLatLong();


        buildGoogleApiClient();


        //   ArrayList<String> route_dropdown_value = (this.getResources().getStringArray(R.array.spinner_select_route) != null) ? projectListIDS : new ArrayList<String>(1);
        //    projectListIDS.add(0, "null");
        spn_select_route = (Spinner) findViewById(R.id.activity_location_maps_spn_select_route);
        map_fab_edit = (FloatingActionButton) findViewById(R.id.map_fab_edit);
        map_fab_delete = (FloatingActionButton) findViewById(R.id.map_fab_delete);

        route_list = new ArrayList<>();
        route_list.add("Select");
        //  PMSDatabase.getInstance(this).getRouteName(route_list);

        adapterProject = new ArrayAdapter(this, R.layout.map_spinner_layout, route_list) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the second item from Spinner
                    return true;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Hide the second item from Spinner
                    //   tv.setVisibility(View.GONE);
                    //  tv.setTextColor(Color.GRAY);
                } else {
                    //  tv.setVisibility(View.VISIBLE);
                    //  tv.setTextColor(Color.BLACK);
                }

             /*   if(position%2 == 1) {
                    // Set the item background color
                    tv.setBackgroundColor(Color.parseColor("#FFF9A600"));
                }
                else {
                    // Set the alternate item background color
                    tv.setBackgroundColor(Color.parseColor("#FFE49200"));
                }*/
                return view;
            }
        };
        adapterProject.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spn_select_route.setAdapter(adapterProject);
        spn_select_route.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    map_fab_edit.setVisibility(View.INVISIBLE);
                    map_fab_delete.setVisibility(View.INVISIBLE);
                    if (mMap != null) {
                        mMap.clear();
                        CameraPosition position = new CameraPosition.Builder()
                                .target(new LatLng(23.082567, 80.070910))
                                .zoom(1).build();
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                    }
                } else {
                    map_fab_edit.setVisibility(View.VISIBLE);
                    map_fab_delete.setVisibility(View.VISIBLE);
                }
                route_name = spn_select_route.getItemAtPosition(i) + "";
                if (!vehicleRouteDataItemList.isEmpty()) {
                    vehicleRoutedataItem_click_edit = vehicleRouteDataItemList.get(i);
                    try {
                        get_routeStoppageApi(vehicleRoutedataItem_click_edit.getRoute());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //   Toast.makeText(LocationMapsActivity.this,i+ " " +vehicleRoutedataItem_click_edit.getVehicleRouteId(), Toast.LENGTH_SHORT).show();
                }
               /* new Thread(new Runnable() {
                    @Override
                    public void run() {

                    }
                }).start();*/


                //   Toast.makeText(LocationMapsActivity.this, vehicleRoutedataItem.getRoute(), Toast.LENGTH_SHORT).show();

                //  String route_url = PMSDatabase.getInstance(LocationMapsActivity.this).getRouteMapURLByName(route_name);
               /* LatLng initialLoc= mMap.getCameraPosition().target;
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        initialLoc, 15);
                mMap.animateCamera(location);*/
                //Toast.makeText(LocationMapsActivity.this,route_url,Toast.LENGTH_SHORT).show();
               /* Bundle b = PMSDatabase.getInstance(LocationMapsActivity.this).getRouteMapDataByName(route_name);
                if (b != null) {
                    LatLng start = null, destination = null;
                    List<LatLng> midpoints = null;
                    String start_title = null, start_snippet = null, destination_title = null, destination_snippet = null;
                    try {
                        start = new LatLng(Double.parseDouble(b.getString("start_lat")), Double.parseDouble(b.getString("start_long")));
                        destination = new LatLng(Double.parseDouble(b.getString("destination_lat")), Double.parseDouble(b.getString("destination_long")));
                        //      midpoints = new LatLng(Double.parseDouble(b.getString("waypoint_lat")), Double.parseDouble(b.getString("waypoint_long")));

                        start_title = b.getString("start_title");
                        start_snippet = b.getString("start_snippet");
                        destination_title = b.getString("destination_title");
                        destination_snippet = b.getString("destination_snippet");

                        String lat_waypoint = b.getString("waypoint_lat");
                        String lat_waypoint_array[] = lat_waypoint.split(",");

                        String long_waypoint = b.getString("waypoint_long");
                        String long_waypoint_array[] = long_waypoint.split(",");


                        if (lat_waypoint_array.length == long_waypoint_array.length) {
                            midpoints = new ArrayList<>(lat_waypoint_array.length);
                            for (int j = 0; lat_waypoint_array.length > j; j++) {
                                midpoints.add(new LatLng(Double.parseDouble(lat_waypoint_array[j]), Double.parseDouble(long_waypoint_array[j])));
                            }
                        }

                        String waypoint_time = b.getString("waypoint_time");
                        String waypoint_time_array[] = waypoint_time.split(",");

                        String waypoint_address = b.getString("waypoint_address");
                        String waypoint_address_array[] = waypoint_address.split(",");

                        if (waypoint_time_array.length == waypoint_address_array.length) {
                            address_waypoint_list.clear();
                            time_waypoint_list.clear();

                            //  midpoints = new ArrayList<>(lat_waypoint_array.length);
                            for (int j = 0; waypoint_time_array.length > j; j++) {
                                address_waypoint_list.add(waypoint_address_array[j]);
                                time_waypoint_list.add(waypoint_time_array[j]);
                            }
                        }


                    } catch (Exception e) {

                    } finally {

                        latlong_start = start;
                        latlong_destination = destination;

                        createRouteByParam(start, destination, midpoints, start_title, start_snippet, destination_title, destination_snippet);
                        LocationMapsActivity.this.start_title = start_title;
                        LocationMapsActivity.this.start_snippet = start_snippet;
                        LocationMapsActivity.this.destination_title = destination_title;
                        LocationMapsActivity.this.destination_snippet = destination_snippet;


                        latlong_waypoint_list.clear();
                        if (midpoints != null)
                            latlong_waypoint_list.addAll(midpoints);
                        getDirection(route_url);
                    }

                } else ;*/
                //Toast.makeText(LocationMapsActivity.this, "Failed to fetch data from database!!!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        get_vehicleRouteApi();

        tablayout_create_display = (TabLayout) findViewById(R.id.activity_location_maps_tablayout_create_display);
        tablayout_create_display.setBackgroundColor(colorIs);
        tablayout_create_map = (TabLayout) findViewById(R.id.activity_location_maps_tablayout_create_map);
        tablayout_create_map.setBackgroundColor(colorIs);
        ((RelativeLayout) findViewById(R.id.activity_location_maps_ll_bottom)).setBackgroundColor(colorIs);
        ll_select_route = (LinearLayout) findViewById(R.id.activity_location_maps_ll_select_route);

        tablayout_create_display.addTab(tablayout_create_display.newTab().setText("Display Route").setIcon(android.R.drawable.ic_menu_mapmode));
        tablayout_create_display.addTab(tablayout_create_display.newTab().setText("Create Route").setIcon(android.R.drawable.ic_dialog_map));

        TabLayout.Tab save = null;
        tablayout_create_map.addTab(tablayout_create_map.newTab().setText("Start").setIcon(android.R.drawable.ic_menu_mylocation));
        tablayout_create_map.addTab(tablayout_create_map.newTab().setText("Destination").setIcon(android.R.drawable.ic_menu_myplaces));
        tablayout_create_map.addTab(tablayout_create_map.newTab().setText("Stops").setIcon(android.R.drawable.ic_menu_add));
        tablayout_create_map.addTab(tablayout_create_map.newTab().setText("Route").setIcon(android.R.drawable.ic_menu_directions));
        tablayout_create_map.addTab(save = tablayout_create_map.newTab().setText("Save").setIcon(android.R.drawable.ic_menu_save));
        // tablayout_create_map.addTab(tablayout_create_map.newTab().setText("Waypoints").setIcon(android.R.drawable.ic_input_delete));

        tablayout_create_map.setSmoothScrollingEnabled(true);


        //   tooltip library discontinued

        //  showToolTipForSpinner();

        //   showCustomTooltip(spn_select_route);
        //  Showcaseview
     /*  Button b = new Button(this);
        b.setGravity(Gravity.LEFT);
        b.setVisibility(View.GONE);
        new ShowcaseView.Builder(this)
                .setStyle(R.style.CustomShowcaseTheme1)
                .setTarget(new ViewTarget(spn_select_route))
                .setContentTitle("Demo Screen")
                .setContentText("Click on dropdown and select route")
                .hideOnTouchOutside()
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        Toast.makeText(LocationMapsActivity.this,"onShowcaseViewHide",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                        Toast.makeText(LocationMapsActivity.this,"onShowcaseViewDidHide",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {
                        Toast.makeText(LocationMapsActivity.this,"onShowcaseViewShow",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
                        Toast.makeText(LocationMapsActivity.this,"onShowcaseViewTouchBlocked",Toast.LENGTH_SHORT).show();
                    }
                })
                .withHoloShowcase()
                .replaceEndButton(b)
                .build();*/


        tablayout_create_map.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tablayout_create_map.getSelectedTabPosition() == 0) {
                    // Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    setRouteMarker(start_destination = "start");
                    route_create = false;
                } else if (tablayout_create_map.getSelectedTabPosition() == 1) {
                    // Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    setRouteMarker(start_destination = "destination");
                    route_create = false;
                } else if (tablayout_create_map.getSelectedTabPosition() == 2) {
                    //  Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    //   createWayPoints();

                    setRouteMarker(start_destination = "waypoints");
                    route_create = false;
                } else if (tablayout_create_map.getSelectedTabPosition() == 3) {
                    //   Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    if (latlong_start != null && latlong_destination != null) {
                        if (latlong_start.latitude != latlong_destination.latitude && latlong_start.longitude != latlong_destination.longitude) {
                            if (ActivityCompat.checkSelfPermission(LocationMapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(LocationMapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            mMap.setMyLocationEnabled(false);
                            start_destination = "";
                            getDirection("default");
                            createRoute();
                            route_create = true;
                        } else {
                            Toast.makeText(LocationMapsActivity.this, "start/destination points should not be same!!!", Toast.LENGTH_LONG).show();
                            tablayout_create_map.getTabAt(1).select();
                        }
                    } else {
                        tablayout_create_map.getTabAt(1).select();
                        Toast.makeText(LocationMapsActivity.this, "Choose start/destination point!!!", Toast.LENGTH_LONG).show();
                    }
                } else if (tablayout_create_map.getSelectedTabPosition() == 4) {
                    // Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    if (route_create == true) {
                        displayAlertDialog();

                    } else {
                        Toast.makeText(LocationMapsActivity.this, "Create Route First!!!", Toast.LENGTH_LONG).show();
                        route_create = false;
                    }


                }/*else if (tablayout_create_map.getSelectedTabPosition() == 5) {
                    // Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    Toast.makeText(LocationMapsActivity.this, "Select on midpoint to delete!!!", Toast.LENGTH_LONG).show();
                }*/

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (tablayout_create_map.getSelectedTabPosition() == 2) {
                    latlong_waypoint_list.add(mMap.getCameraPosition().target);
                    setRouteMarker(start_destination = "waypoints");
                    route_create = false;
                    //  createWayPoints();
                }
                if (tablayout_create_map.getSelectedTabPosition() == 4) {
                    // Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_map.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    if (route_create == true) {
                        displayAlertDialog();

                    } else {
                        Toast.makeText(LocationMapsActivity.this, "Create Route First!!!", Toast.LENGTH_LONG).show();
                        route_create = false;
                    }
                }
            }
        });


        tablayout_create_display.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if ("no".equalsIgnoreCase(route_save_not)) {
                    AlertDialog.Builder adb = new AlertDialog.Builder(LocationMapsActivity.this);
                    adb.setCancelable(false);
                    // adb.setTitle("Confirmation");
                    adb.setMessage("Are you sure want to switch to display route, your current changes will be lost !!!");
                    // adb.setIcon(android.R.drawable.ic_dialog_alert);
                    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            route_save_not = "default";
                            //  tablayout_create_display.setTag("fab");
                            //    tablayout_create_display.getTabAt(1).select();
                            tablayout_create_display.getTabAt(0).select();
                            map_zoom_level = (float) 0;

                        }
                    });

                    adb.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            tablayout_create_display.setTag("fab");
                            //    setRouteMarker(start_destination = "start");
                            //    route_create = false;
                            route_save_not = "default";
                            tablayout_create_display.getTabAt(1).select();

                           /* if(route_create && tablayout_create_map.getTabAt(4).isSelected())
                            {
                                tablayout_create_map.getTabAt(3).select();
                                tablayout_create_map.getTabAt(4).select();
                            }
                            else if(route_create && tablayout_create_map.getTabAt(3).isSelected()) {

                                tablayout_create_map.getTabAt(0).select();
                                tablayout_create_map.getTabAt(3).select();
                            }
                            else if(tablayout_create_map.getTabAt(1).isSelected()) {
                                setRouteMarker(start_destination = "destination");
                                route_create = false;
                            }
                            else if(tablayout_create_map.getTabAt(0).isSelected()) {
                                setRouteMarker(start_destination = "start");
                                route_create = false;
                            }
                            else if(tablayout_create_map.getTabAt(2).isSelected()) {
                                setRouteMarker(start_destination = "waypoints");
                                route_create = false;
                            }*/


                            // tablayout_create_map.getTabAt(0).select();
                            if (tablayout_create_map.getTabAt(0).isSelected()) {
                                setRouteMarker(start_destination = "start");
                                route_create = false;
                            } else
                                tablayout_create_map.getTabAt(0).select();
                        }
                    });

                    if (!LocationMapsActivity.this.isFinishing())
                        adb.show();
                    return;
                }


                mMap.clear();
                if (tablayout_create_display.getSelectedTabPosition() == 0) {
                    //  Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_display.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    tablayout_create_map.setVisibility(View.INVISIBLE);
                    ll_select_route.setVisibility(View.VISIBLE);
                    //  mMap.setMyLocationEnabled(true);
                    CameraPosition position = new CameraPosition.Builder()
                            .target(new LatLng(23.082567, 80.070910))
                            .zoom(map_zoom_level = (float) 0).build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                    spn_select_route.setSelection(0); // future select already create in last spinner

                } else if (tablayout_create_display.getSelectedTabPosition() == 1) {
                    route_save_not = "no";

                    // if(fab_click)
                    //      return;
                    String check = (String) tablayout_create_display.getTag();
                    if (check != null && "fab".equalsIgnoreCase(check)) {
                        tablayout_create_display.setTag("");
                        return;
                    }
                    route_name = "";
                    // because new route
                    //    Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_display.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    map_fab_edit.setVisibility(View.INVISIBLE);
                    map_fab_delete.setVisibility(View.INVISIBLE);
                    tablayout_create_map.setVisibility(View.VISIBLE);
                    ll_select_route.setVisibility(View.INVISIBLE);
                    //  tablayout_create_map.getTabAt(0).getCustomView().setSelected(true);

                    tablayout_create_map.getTabAt(0).select();
                    latlong_start = latlong_destination = latlong_waypoint = null;
                    start_title = start_snippet = destination_snippet = destination_title = null;
                    stopDataItemList_stop.clear();
                    latlong_waypoint_list.clear();
                    address_waypoint_list.clear();
                    time_waypoint_list.clear();
                    setRouteMarker(start_destination = "start");
                    route_create = false;
                    vehicleRoutedataItem_click_edit = null;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (tablayout_create_display.getSelectedTabPosition() == 0) {
                    mMap.clear();
                    //  Toast.makeText(LocationMapsActivity.this, "Tab " + tablayout_create_display.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    tablayout_create_map.setVisibility(View.INVISIBLE);
                    ll_select_route.setVisibility(View.VISIBLE);
                    //  mMap.setMyLocationEnabled(true);
                    CameraPosition position = new CameraPosition.Builder()
                            .target(new LatLng(23.082567, 80.070910))
                            .zoom(1).build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                    spn_select_route.setSelection(0); // future select already create in last spinner

                }

                // TODO: 2/2/2017 for   if (tablayout_create_display.getSelectedTabPosition() == 1)
            }
        });


        // get latlong from databse
      /*  pmsDatabase = PMSDatabase.getInstance(this);
        EmployeeOtherDetails employeeOtherDetails = pmsDatabase.getEmployeeData(new EmployeeOtherDetails(this), Utility.getDate());
        List<EmployeeLocation> employeeLocationList = employeeOtherDetails.getEmployeeLocationList();
        latlng = new ArrayList();
        LatLng bhopal = new LatLng(23.231116
                , 77.433592);
        LatLng sydney = new LatLng(-34, 151);
        if (employeeLocationList != null && !employeeLocationList.isEmpty()) {
            for (EmployeeLocation employeeLocation :
                    employeeLocationList) {
                latlng.add(new LatLng(employeeLocation.getEmployeeLocationLatitude(), employeeLocation.getEmployeeLocationLongitude()));
            }

        }
        else
        {
            latlng.add(bhopal);
            latlng.add(sydney);


        }*/
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        // iconFactory = new IconGenerator(this);

       /* BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_start:
                                setRouteMarker(start_destination = "start");
                                break;
                            case R.id.action_waypoints:
                                createWayPoints();
                                break;
                            case R.id.action_destination:
                               ;
                                setRouteMarker( start_destination = "destination");
                                break;
                            case R.id.action_route:
                                mMap.setMyLocationEnabled(false);
                                start_destination = "";
                                getDirection();
                                createRoute();

                                break;
                            case R.id.action_save:

                                break;
                        }
                        return true;
                    }
                });*/


        // popupsetting();
    }


    /*private void popupsetting() {
        ActionItem nextItem     = new ActionItem(ID_DOWN, "Next", getResources().getDrawable(R.drawable.menu_down_arrow));
        ActionItem prevItem     = new ActionItem(ID_UP, "Prev", getResources().getDrawable(R.drawable.menu_up_arrow));
        ActionItem searchItem   = new ActionItem(ID_SEARCH, "Find", getResources().getDrawable(R.drawable.menu_search));
        ActionItem infoItem     = new ActionItem(ID_INFO, "Info", getResources().getDrawable(R.drawable.menu_info));
        ActionItem eraseItem    = new ActionItem(ID_ERASE, "Clear", getResources().getDrawable(R.drawable.menu_eraser));
        ActionItem okItem       = new ActionItem(ID_OK, "OK", getResources().getDrawable(R.drawable.menu_ok));

        //use setSticky(true) to disable QuickAction dialog being dismissed after an item is clicked
        prevItem.setSticky(true);
        nextItem.setSticky(true);

        //create QuickAction. Use QuickAction.VERTICAL or QuickAction.HORIZONTAL param to define layout
        //orientation
        final QuickAction quickAction = new QuickAction(this, QuickAction.VERTICAL);

        //add action items into QuickAction
        quickAction.addActionItem(nextItem);
        quickAction.addActionItem(prevItem);
        quickAction.addActionItem(searchItem);
        quickAction.addActionItem(infoItem);
        quickAction.addActionItem(eraseItem);
        quickAction.addActionItem(okItem);

        //Set listener for action item clicked
        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                //here we can filter which action item was clicked with pos or actionId parameter
                ActionItem actionItem = quickAction.getActionItem(pos);

                Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();
            }
        });

        //set listnener for on dismiss event, this listener will be called only if QuickAction dialog was dismissed
        //by clicking the area outside the dialog.
        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
                Toast.makeText(getApplicationContext(), "Dismissed", Toast.LENGTH_SHORT).show();
            }
        });


                quickAction.show(spn_select_route);
                quickAction.setAnimStyle(QuickAction.ANIM_REFLECT);

    }*/



      /*  private void showCustomTooltip(@NonNull View anchor) {

            Resources res = getResources();

            int tooltipSize = res.getDimensionPixelOffset(R.dimen.activity_horizontal_margin);
            int tooltipColor = ContextCompat.getColor(this, R.color.colorPrimary);
            int tooltipPadding = res.getDimensionPixelOffset(R.dimen.activity_horizontal_margin);

            int tipSizeSmall = res.getDimensionPixelSize(R.dimen.view_height);
            int tipSizeRegular = res.getDimensionPixelSize(R.dimen.activity_vertical_margin);

            int tipRadius = res.getDimensionPixelOffset(R.dimen.action_bar_offset);
            View content = getLayoutInflater().inflate(R.layout.item_tooltip_view_1, null);

            final Tooltip customTooltip = new Tooltip.Builder(this)
                    .anchor(anchor, Tooltip.TOP)
                    .animate(new TooltipAnimation(TooltipAnimation.SCALE_AND_FADE, 400))
                    .autoAdjust(true)
                    .withPadding(tooltipPadding)
                    .content(content)
                    .cancelable(false)
                    .checkForPreDraw(true)
                    .withTip(new Tooltip.Tip(tipSizeRegular, tipSizeRegular, tooltipColor, tipRadius))
                    .into((RelativeLayout)findViewById(R.id.rl_root))
                    .debug(true)
                    .show();

            content.findViewById(R.id.dismiss_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customTooltip.dismiss(true);
                }
            });

        }*/


    private void displayAlertDialog() {
        get_routeToApi();

       /* final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        LinearLayout ll = new LinearLayout(this);
       // final EditText et = new EditText(this);
        final TextInputEditText et = new TextInputEditText(this);
        RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(layoutparams);
        et.setPadding(10,10,10,10);
        et.setLayoutParams(layoutparams);
        et.setHint("type route name");
        et.setText(route_name + "");
        ll.addView(et);
        ad.setView(ll);
        ad.setTitle("Enter Route Name ");
        ad.setMessage("");
        ad.setIcon(android.R.drawable.ic_menu_directions);
        ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                String textString = et.getText().toString(); // Converts the value of getText to a string.
                if (textString != null && textString.trim().length() == 0) {
                    Toast toast = Toast.makeText(LocationMapsActivity.this, "Please enter a route name", Toast.LENGTH_SHORT);
                    toast.show();
                  //  displayAlertDialog();
                 //   displayAlertDialogForRouteSave();


                } else {

                    savedRouteDataInDataBase(route_name, textString);
                    route_name = textString;
                    route_list.clear();
                    route_list.add("Select");
                    PMSDatabase.getInstance(LocationMapsActivity.this).getRouteName(route_list);
                    adapterProject.notifyDataSetChanged();
                    Toast.makeText(LocationMapsActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                    route_save_not = "yes";
                }
            }
        });

        ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(LocationMapsActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
        if (!LocationMapsActivity.this.isFinishing()) {
            ad.show();
        }*/

    }

    private void displayAlertDialogForRouteSave() {


        // if (!"".equalsIgnoreCase(route_name)) {
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        View custom_alert_dialog = View.inflate(this, R.layout.alertdialog_save, null);
        ((TextView) custom_alert_dialog.findViewById(R.id.alertdialog_save_title)).setBackgroundColor(colorIs);
        Button btn_cancel = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_save_btn_cancel);
        Button btn_save = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_save_btn_save);
        final Spinner spn_route_to = (Spinner) custom_alert_dialog.findViewById(R.id.alertdialog_save_spn_route_to);
        final TextInputEditText tet_edit_route_name = (TextInputEditText) custom_alert_dialog.findViewById(R.id.alertdialog_save_tet_edit_route_name);
        tet_edit_vehicle_name = (AutoCompleteTextView) custom_alert_dialog.findViewById(R.id.alertdialog_save_actv_edit_vehicle_name);

        tet_edit_vehicle_no = (TextInputEditText) custom_alert_dialog.findViewById(R.id.alertdialog_save_tet_edit_vehicle_no);

        final TextInputLayout til_edit_route_name = (TextInputLayout) custom_alert_dialog.findViewById(R.id.alertdialog_save_til_edit_route_name);
        final TextInputLayout til_edit_vehicle_name = (TextInputLayout) custom_alert_dialog.findViewById(R.id.alertdialog_save_til_edit_vehicle_name);
        final TextInputLayout til_edit_vehicle_no = (TextInputLayout) custom_alert_dialog.findViewById(R.id.alertdialog_save_til_edit_vehicle_no);

        pb = (ProgressBar) custom_alert_dialog.findViewById(R.id.alertdialog_save_pb);

        adapter_suggest_dropdown = new ArrayAdapter(this, R.layout.myspinner_dropdown_item/*, stopList*/);
        tet_edit_vehicle_name.setThreshold(1);
        tet_edit_vehicle_name.setAdapter(adapter_suggest_dropdown);
        tet_edit_vehicle_name.setTag("default");

        tet_edit_vehicle_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    AppRequestQueueController.getInstance(LocationMapsActivity.this).cancelRequestByTag("getVehicleDataFromServer");

                    try {
                        getVehicleDataFromServer(s.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //  Toast.makeText(LocationMapsActivity.this, tet_edit_vehicle_name.getTag().toString(), Toast.LENGTH_SHORT).show();

                    if ("server".equalsIgnoreCase(tet_edit_vehicle_name.getTag().toString()))
                        tet_edit_vehicle_name.setTag("default");
                    else
                        tet_edit_vehicle_no.setText("");
                }

                //   Toast.makeText(LocationMapsActivity.this, "" + start + " " + before + " " + count, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tet_edit_vehicle_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                String selection = (String) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
                //  Toast.makeText(LocationMapsActivity.this, selection, Toast.LENGTH_SHORT).show();
                String arr[] = selection.split(", ");
                tet_edit_vehicle_name.setText(arr[0]);
                tet_edit_vehicle_no.setText(arr[1]);
                vehicle_id_server = vehicleDataItemList.get(position).getVehicleId();


            }
        });


        // spn_route_to.setPrompt("Selectdfdddvcdv");  for spinner mode = dialog

        ArrayAdapter adapterProject_routeTo = new ArrayAdapter(this, R.layout.map_spinner_layout_new, route_to) {

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the second item from Spinner
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Hide the second item from Spinner
                    //   tv.setVisibility(View.GONE);
                    //     tv.setBackgroundColor(Color.BLUE);
                    //   tv.setTextColor(Color.GRAY);
                } else {
                    //  tv.setVisibility(View.VISIBLE);
                    //  tv.setTextColor(Color.BLACK);
                }

                /*if(position%2 == 1) {
                    // Set the item background color
                    tv.setBackgroundColor(Color.parseColor("#FFF9A600"));
                }
                else {
                    // Set the alternate item background color
                    tv.setBackgroundColor(Color.parseColor("#FFE49200"));
                }*/
                return view;
            }
        };
        adapterProject_routeTo.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spn_route_to.setAdapter(adapterProject_routeTo);
        spn_route_to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(LocationMapsActivity.this, spn_route_to.getItemAtPosition(i) + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        ad.setView(custom_alert_dialog);

        AlertDialog alertDialog = null;
        if (!LocationMapsActivity.this.isFinishing()) {
            ad.create();
            alertDialog = ad.show();
        }

        final AlertDialog finalAlertDialog = alertDialog;
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LocationMapsActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
                finalAlertDialog.dismiss();


            }
        });


        if (vehicleRoutedataItem_click_edit != null) {
            tet_edit_route_name.setText(vehicleRoutedataItem_click_edit.getVehicleRouteName());
            //   Toast.makeText(LocationMapsActivity.this,vehicleRoutedataItem_click_edit.getVehicleId(),Toast.LENGTH_SHORT).show();
            get_existingVehicleDetail(vehicleRoutedataItem_click_edit.getVehicleId());

            String current_route_to_id = vehicleRoutedataItem_click_edit.getRouteTo();
            spn_route_to.setSelection(route_to_id.indexOf(current_route_to_id));
        }

        tet_edit_route_name.setText(route_name);


        // TODO: 2/24/2017 old coding database saving
        /*
        Bundle b = PMSDatabase.getInstance(LocationMapsActivity.this).getDataDisplayInAlert(route_name);
        if (b != null) {
            String route_to_local = b.getString("route_to");
            if (route_to_local != null) {
                //   Toast.makeText(this, ""+route_to.indexOf(route_to_local), Toast.LENGTH_SHORT).show();
                spn_route_to.setSelection(route_to.indexOf(route_to_local));
            }


            String vehicle_name_local = b.getString("vehicle_name");
            if (vehicle_name_local != null)
                tet_edit_vehicle_name.setText(vehicle_name_local);

            String vehicle_no = b.getString("vehicle_no");
            if (vehicle_no != null)
                tet_edit_vehicle_no.setText(vehicle_no);

        }*/

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String route_name_current = tet_edit_route_name.getText().toString();
                String vehicle_no = tet_edit_vehicle_no.getText().toString();
                String vehicle_name = tet_edit_vehicle_name.getText().toString();

                if (route_name_current.isEmpty() && vehicle_no.isEmpty() && vehicle_name.isEmpty()) {
                    // tet_edit_address.requestFocus();
                    til_edit_route_name.setError("cannot be blank!!!");
                    til_edit_vehicle_no.setError("cannot be blank!!!");
                    til_edit_vehicle_name.setError("cannot be blank!!!");
                    til_edit_vehicle_name.requestFocus();
                    return;
                } else if (vehicle_name.isEmpty()) {
                    til_edit_vehicle_no.setError(null);
                    til_edit_route_name.setError(null);
                    til_edit_vehicle_name.requestFocus();
                    til_edit_vehicle_name.setError("cannot be blank!!!");
                    return;
                } else if (vehicle_no.isEmpty()) {
                    til_edit_route_name.setError(null);
                    til_edit_vehicle_name.setError(null);
                    til_edit_vehicle_no.requestFocus();
                    til_edit_vehicle_no.setError("cannot be blank!!!");
                    return;
                } else if (route_name_current.isEmpty()) {
                    til_edit_vehicle_no.setError(null);
                    til_edit_vehicle_name.setError(null);
                    til_edit_route_name.requestFocus();
                    til_edit_route_name.setError("cannot be blank!!!");
                    return;
                } else if (spn_route_to.getSelectedItemPosition() == 0) {
                    TextView errorText = (TextView) spn_route_to.getSelectedView();
                    errorText.setError("Please select route");
                    errorText.setFocusable(true);
                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                    //errorText.setText("Please select route");//changes the selected item text to this
                    return;
                } else {
                    finalAlertDialog.dismiss();
                    savedRouteDataInDataBase(route_name, route_name_current, vehicle_name,
                            vehicle_no, (String) spn_route_to.getItemAtPosition(spn_route_to.getSelectedItemPosition()));

                    vehicleRouteData = new VehicleRouteDataItem("Active", route_name_current,
                            vehicle_id_server, "", route_to_id.get(spn_route_to.getSelectedItemPosition())
                    );
                    try {
                        prepareStopdata();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    route_name = route_name_current;
                    //   route_list.clear();
                    //   route_list.add("Select");
                    //PMSDatabase.getInstance(LocationMapsActivity.this).getRouteName(route_list);
                    //   adapterProject.notifyDataSetChanged();
                    //    Toast.makeText(LocationMapsActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                    route_save_not = "yes";
                }

            }
        });


    }

    private void get_existingVehicleDetail(final String vehicleId) {

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleApi.php?databaseName=" + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&data=" +
                "{\"filter\":{\"VehicleId\":\"" + vehicleId + "\"}}";

        Log.e("url", url);


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url
              /*  "http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"VehicleId\":\"" + vehicleId + "\"}}"*/
                ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.e("get_existingVehicleData", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<VehicleDataItem>>() {
                                    }.getType();
                                    ArrayList<VehicleDataItem> vehicle_local = (ArrayList<VehicleDataItem>) gson.fromJson(jsonOutput, listType);
                                    if (vehicle_local != null && !vehicle_local.isEmpty() && vehicle_local.size() > 0) {
                                        tet_edit_vehicle_no.setText(vehicle_local.get(0).getVehicleNumber());
                                        tet_edit_vehicle_name.setTag("server");
                                        tet_edit_vehicle_name.setText(vehicle_local.get(0).getVehicleName());

                                        vehicle_id_server = vehicle_local.get(0).getVehicleId();
                                    }

                                    return;
                                }


                            } else {
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pb.setVisibility(View.INVISIBLE);

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put("placekey", s); old
                return params;
            }
*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
/*        AppController.getInstance().addToRequestQueue(strReq, "get_existingVehicleDetail");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "get_existingVehicleDetail");
        //queue.add(strReq);


    }

    /*private void get_existingRouteToDetail(final String routeToId) {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteTo\",\"MasterEntryId\":\""+routeToId+"\"}}",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.e("get_existingVehicleData", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {

                                    for(int initializer = 0; jsonArray_result_length> initializer ; initializer++)
                                    {
                                        JSONObject masterEntryTableRow = jsonArray_result.getJSONObject(initializer);
                                        String MasterEntryValue = masterEntryTableRow.getString("MasterEntryValue");
                                        String MasterEntryId = masterEntryTableRow.getString("MasterEntryId");

                                        Toast.makeText(LocationMapsActivity.this, MasterEntryValue+" "+MasterEntryId, Toast.LENGTH_SHORT).show();

                                        break;
                                    }
                                    return;
                                }


                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pb.setVisibility(View.INVISIBLE);

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        }) {
            *//*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put("placekey", s); old
                return params;
            }
*//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppController.getInstance().addToRequestQueue(strReq,"get_existingVehicleDetail");
        //queue.add(strReq);


    }*/

    public void onClickGoogleApi(View view) {
        //  currentViewSelect = (EditText) view;
        //  Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                    // .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    //  .setTypeFilter(Place.TYPE_COUNTRY).setCountry("IN")  // work up to 8.4 play service version
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE)
                    .build();


            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, 1000);

        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("GoogleAddress", "Place: " + place.getName());
                place.getAddress();
                //      currentViewSelect.setText(place.getAddress() + "");
                LatLng latlong = place.getLatLng();
                //   Toast.makeText(this, " " + latlong.latitude + " " + latlong.longitude, Toast.LENGTH_SHORT).show();

                String placeDetailsStr = place.getName() + "\n"
                        + place.getId() + "\n"
                        + place.getLatLng().toString() + "\n"
                        + place.getAddress() + "\n"
                        + place.getAttributions();
                //   txtPlaceDetails.setText(placeDetailsStr);
                //       Toast.makeText(this, " " + placeDetailsStr, Toast.LENGTH_SHORT).show();

                tet_edit_address.setText(place.getName());
                // stop_lat = latlong.latitude+"";
                //  stop_long = latlong.longitude+"";
                currentMarker.setPosition(latlong);
                CameraPosition position = new CameraPosition.Builder()
                        .target(latlong)
                        .zoom(map_zoom_level).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

                if ("start".equalsIgnoreCase(start_destination)) {
                    //   start_stop = stopDataItemList.get(position);
                    latlong_start = latlong;
                } else if ("destination".equalsIgnoreCase(start_destination)) {
                    //   destination_stop = stopDataItemList.get(position);
                    latlong_destination = latlong;
                } else if ("waypoints".equalsIgnoreCase(start_destination)) {
                    int a = (int) currentMarker.getTag();
                    //  latlong_waypoint_list.set(a, null);
                    //   stopDataItemList_stop.set(a,stopDataItemList.get(position));

                    try {
                        latlong_waypoint_list.set(a, latlong);
                    } catch (IndexOutOfBoundsException e) {
                        latlong_waypoint_list.add(a, latlong);


                    }


                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.
                    Log.i("GoogleAddress", status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }

        }

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                getCurrentLocationLatLong();
                //  requestLocation();

            } else {
                onSupportNavigateUp();
                //Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void displayAlertDialogForMarkerClick(final Marker marker, final boolean check_for_new_old_marker) {
        // if (!"".equalsIgnoreCase(route_name)) {

        this.currentMarker = marker;
        current_stopDataItem = null; // because set marker according to lat long coming from server when user select
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        View custom_alert_dialog = View.inflate(this, R.layout.alertdialog_marker, null);
        ad.setView(custom_alert_dialog);

        AlertDialog alertDialog = null;
        if (!LocationMapsActivity.this.isFinishing()) {
            ad.create();
            alertDialog = ad.show();
        }
        final AlertDialog finalAlertDialog = alertDialog;
        ((TextView) custom_alert_dialog.findViewById(R.id.alertdialog_tv_marker_detail)).setBackgroundColor(colorIs);
        ((FrameLayout) custom_alert_dialog.findViewById(R.id.framelayout_search_place)).setBackgroundColor(colorIs);

        Button btn_cancel = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_btn_cancel);
        Button btn_delete = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_btn_delete);
        final Button btn_save = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_btn_save);
        //  final Button btn_ok = (Button) custom_alert_dialog.findViewById(R.id.alertdialog_btn_ok);
        //   final RadioButton rb_delete = (RadioButton) custom_alert_dialog.findViewById(R.id.alertdialog_rb_delete);
        //   final RadioButton rb_change_address = (RadioButton) custom_alert_dialog.findViewById(R.id.alertdialog_rb_edit);

        TextView tv_marker_detail = (TextView) custom_alert_dialog.findViewById(R.id.alertdialog_tv_marker_detail);


        if ("start".equalsIgnoreCase(start_destination)) {
            tv_marker_detail.setText("Enter details of Start Marker");
        } else if ("destination".equalsIgnoreCase(start_destination)) {
            tv_marker_detail.setText("Enter details of End Marker");
        } else if ("waypoints".equalsIgnoreCase(start_destination)) {
            tv_marker_detail.setText("Enter details of Stop Marker");


        }


        if (check_for_new_old_marker) {
            //btn_cancel.setVisibility(View.GONE);
            btn_delete.setVisibility(View.GONE);
        }

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(LocationMapsActivity.this, "cancel click", Toast.LENGTH_SHORT).show();

                if (check_for_new_old_marker) {
                    if (start_destination.equalsIgnoreCase("start")) {
                        if (finalAlertDialog != null)
                            finalAlertDialog.dismiss();
                        route_save_not = "default";
                        tablayout_create_display.getTabAt(0).select();
                    } else if (start_destination.equalsIgnoreCase("destination")) {
                        if (finalAlertDialog != null)
                            finalAlertDialog.dismiss();

                        destination_title = destination_snippet = null;
                        latlong_destination = null;
                        tablayout_create_map.getTabAt(0).select();
                    } else if (start_destination.equalsIgnoreCase("waypoints")) {
                        int currentMarkerPosition = (int) marker.getTag();
                        latlong_waypoint_list.remove(currentMarkerPosition);

                        if (finalAlertDialog != null)
                            finalAlertDialog.dismiss();

                        if (currentMarkerPosition == 0) {
                            tablayout_create_map.getTabAt(0).select();
                        } else {
                            // tablayout_create_map.getTabAt(0).select();
                            // tablayout_create_map.getTabAt(2).select();
                            setRouteMarker(start_destination = "waypoints");
                        }

                    }
                } else if (finalAlertDialog != null)
                    finalAlertDialog.dismiss();
            }
        });


        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (start_destination.equalsIgnoreCase("waypoints")) {
                    marker.hideInfoWindow();
                    try {
                        int a = (int) marker.getTag();
                        //latlong_waypoint_list.set(a, marker.getPosition());
                        latlong_waypoint_list.remove(a);
                        address_waypoint_list.remove(a);
                        time_waypoint_list.remove(a);


                        if (!latlong_waypoint_list.isEmpty())
                            setRouteMarker(start_destination = "waypoints");
                        else {
                            //setRouteMarker(start_destination = "start");
                            tablayout_create_map.getTabAt(0).select();
                        }

                        stopDataItemList_stop.remove(a);

                        Toast.makeText(LocationMapsActivity.this, "Marker Deleted", Toast.LENGTH_SHORT).show();
                        //   marker.setSnippet("" + marker.getPosition().latitude
                        //           + ", " + marker.getPosition().longitude);
                        //  marker.setTitle("Waypoint" + (a + 1));
                    } catch (Exception e) {
                        // Toast.makeText(LocationMapsActivity.this, "exception", Toast.LENGTH_SHORT).show();

                    } finally {
                        marker.showInfoWindow();
                    }

                }

                // Toast.makeText(LocationMapsActivity.this, "delete click", Toast.LENGTH_SHORT).show();
                if (finalAlertDialog != null)
                    finalAlertDialog.dismiss();
            }
        });


        if (!start_destination.equalsIgnoreCase("waypoints")) {

            btn_delete.setVisibility(View.GONE);
            // btn_delete.setClickable(false);
        } else if (!marker.isDraggable()) {
            btn_delete.setVisibility(View.GONE);
        }

        tet_edit_address = (AutoCompleteTextView) custom_alert_dialog.findViewById(R.id.alertdialog_actv_edit_address);
        final TextInputLayout til_edit_address = (TextInputLayout) custom_alert_dialog.findViewById(R.id.alertdialog_til_edit_address);
        final TextInputEditText tet_edit_time = (TextInputEditText) custom_alert_dialog.findViewById(R.id.alertdialog_tet_edit_time);
        final TextInputLayout til_edit_time = (TextInputLayout) custom_alert_dialog.findViewById(R.id.alertdialog_til_edit_time);

        pb = (ProgressBar) custom_alert_dialog.findViewById(R.id.alertdialog_marker_pb);
        adapter_suggest_dropdown = new ArrayAdapter(this, R.layout.myspinner_dropdown_item/*, stopList*/);
        tet_edit_address.setThreshold(1);
        tet_edit_address.setAdapter(adapter_suggest_dropdown);

        tet_edit_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                   /* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();*/

                    if ("start".equalsIgnoreCase(start_destination)) {
                        start_stop = null;
                    } else if ("destination".equalsIgnoreCase(start_destination)) {
                        destination_stop = null;
                    } else if ("waypoints".equalsIgnoreCase(start_destination)) {
                        int a = (int) marker.getTag();
                        //  latlong_waypoint_list.set(a, null);
                        try {
                            stopDataItemList_stop.set(a, null);
                        } catch (IndexOutOfBoundsException e) {
                            stopDataItemList_stop.add(a, null);
                        }
                    }


                    AppRequestQueueController.getInstance(LocationMapsActivity.this).cancelRequestByTag("getStopDataFromServer");
                   /* AppController.getInstance().cancelPendingRequests("getStopDataFromServer");*/
                    try {
                        getStopDataFromServer(s.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tet_edit_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                final String selection = (String) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
                //   Toast.makeText(LocationMapsActivity.this, selection, Toast.LENGTH_SHORT).show();


                //   current_stopDataItem = stopDataItemList.get(position);

                /*List<StopDataItem> stopDataItemList_new =
                                    stopDataItemList
                                .stream()
                                .filter(p-> p.getMasterEntryValue().equals((selection)))
                                .collect(Collectors.toList());*/
                for (StopDataItem stopData : stopDataItemList) {
                    if (stopData.getRouteStoppageName() != null && stopData.getRouteStoppageName().equalsIgnoreCase(selection))
                        current_stopDataItem = stopData;
                }


                //   Toast.makeText(LocationMapsActivity.this, current_stopDataItem.getStopLat()
                //           + "," + current_stopDataItem.getStopLong(), Toast.LENGTH_SHORT).show();
                //    stopDataItemList_save_route.add(stopDataItemList.get(position));

                if ("start".equalsIgnoreCase(start_destination)) {
                    //   start_stop = stopDataItemList.get(position);
                    start_stop = current_stopDataItem;
                } else if ("destination".equalsIgnoreCase(start_destination)) {
                    //   destination_stop = stopDataItemList.get(position);
                    destination_stop = current_stopDataItem;
                } else if ("waypoints".equalsIgnoreCase(start_destination)) {
                    int a = (int) marker.getTag();
                    //  latlong_waypoint_list.set(a, null);
                    //   stopDataItemList_stop.set(a,stopDataItemList.get(position));
                    stopDataItemList_stop.set(a, current_stopDataItem);

                }

                /*Toast.makeText(LocationMapsActivity.this, ""+stopDataItem.getStopLat()+" "+stopDataItem.getStopLong(),
                        Toast.LENGTH_SHORT).show();
*/


                //  String arr[] = selection.split(" ");
                //  tet_edit_vehicle_name.setText(arr[0]);
                //  tet_edit_vehicle_no.setText(arr[1]);
                //  vehicle_id_server = vehicleDataItemList.get(position).getVehicleId();


            }
        });


        /*
        * Set a special listener to be called when an action is performed on the text view.
         * This will be called when the enter key is pressed, or when an action supplied to the IME
          * is selected by the user. Setting this means that the normal hard key event will not
          * insert a newline into the text view, even if it is multi-line; holding down the ALT
          * modifier will, however, allow the user to insert a newline character.
        * */
        //tet_edit_address.setOnEditorActionListener();

       /* tet_edit_time.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                {
                    final Calendar c = Calendar.getInstance();
                    int mHour = c.get(Calendar.HOUR_OF_DAY);
                    int mMinute = c.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(LocationMapsActivity.this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    tet_edit_time.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, true);
                    timePickerDialog.show();

                }
            }
        });*/


      /*  rb_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_ok.setVisibility(View.VISIBLE);
                til_edit_address.setVisibility(View.GONE);
            }
        });*/

    /*    rb_change_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                til_edit_address.setVisibility(View.VISIBLE);
                til_edit_address.setError("");
                tet_edit_address.requestFocus();
                btn_ok.setVisibility(View.VISIBLE);
            }
        });*/


        // text change listener  - hide save button when text is blank but this functionality is
        // already managed by save button if text is null we display appropriate error message
       /* tet_edit_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Toast.makeText(LocationMapsActivity.this, charSequence+ " beforeTextChanged", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           //     Toast.makeText(LocationMapsActivity.this, charSequence+ " onTextChanged\n"+ i+"\n"+i1+"\n"+i2, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!tet_edit_time.getText().toString().isEmpty() && !tet_edit_address.getText().toString().isEmpty())
                {
                    btn_save.setVisibility(View.VISIBLE);
                }
                else
                    btn_save.setVisibility(View.GONE);
               // Toast.makeText(LocationMapsActivity.this, editable.toString()+ " afterTextChanged", Toast.LENGTH_SHORT).show();

            }
        });

        tet_edit_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Toast.makeText(LocationMapsActivity.this, charSequence+ " beforeTextChanged", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //     Toast.makeText(LocationMapsActivity.this, charSequence+ " onTextChanged\n"+ i+"\n"+i1+"\n"+i2, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void afterTextChanged(Editable editable) {
               if(!tet_edit_time.getText().toString().isEmpty() && !tet_edit_address.getText().toString().isEmpty())
               {
                   btn_save.setVisibility(View.VISIBLE);
               }
                else
                    btn_save.setVisibility(View.GONE);

                // Toast.makeText(LocationMapsActivity.this, editable.toString()+ " afterTextChanged", Toast.LENGTH_SHORT).show();

            }
        });*/


        // default value setting
        String default_address = marker.getSnippet();
        if (default_address != null && !default_address.isEmpty()) {
            tet_edit_address.setText("");
            tet_edit_address.append(default_address);
            //editText.setSelection(editText.getText().length());
        }
       /* else
            btn_save.setVisibility(View.GONE);*/

        String default_time = marker.getTitle();
        if (default_time != null && !default_time.isEmpty()) {
            tet_edit_time.setText("");
            tet_edit_time.append(default_time);
        }
        /*else
            btn_save.setVisibility(View.GONE);*/


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String address = tet_edit_address.getText().toString();
                String time = tet_edit_time.getText().toString();

                if (address.isEmpty() && time.isEmpty()) {
                    // tet_edit_address.requestFocus();
                    til_edit_address.setError("Address cannot be blank!!!");
                    tet_edit_address.requestFocus();
                    til_edit_time.setError("time cannot be blank!!!");
                    return;
                } else if (address.isEmpty()) {
                    til_edit_time.setError(null);
                    til_edit_address.requestFocus();
                    til_edit_address.setError("Address cannot be blank!!!");
                    return;
                } else if (time.isEmpty()) {
                    til_edit_address.setError(null);
                    til_edit_time.requestFocus();
                    til_edit_time.setError("time cannot be blank!!!");
                    return;
                } else {

                    if (finalAlertDialog != null)
                        finalAlertDialog.dismiss();

                    if (current_stopDataItem != null) {
                        Double stop_lat = Double.parseDouble(current_stopDataItem.getStopLat());
                        Double stop_long = Double.parseDouble(current_stopDataItem.getStopLong());

                        LatLng current = new LatLng(stop_lat, stop_long);
                        marker.setPosition(current);

                        //animateMarker(marker,new LatLng(stop_lat,stop_long),false);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, map_zoom_level));
                        if ("start".equalsIgnoreCase(start_destination)) {
                            latlong_start = current;
                        } else if ("destination".equalsIgnoreCase(start_destination)) {
                            latlong_destination = current;
                        } else if ("waypoints".equalsIgnoreCase(start_destination)) {
                            int a = (int) marker.getTag();
                            //latlong_waypoint_list.set(a, marker.getPosition());
                            latlong_waypoint_list.set(a, current);
                        }
                    }

                    marker.hideInfoWindow();
                    marker.setTitle(time);
                    marker.setSnippet(address);
                    marker.showInfoWindow();
                    if ("start".equalsIgnoreCase(start_destination)) {
                        start_title = time;
                        start_snippet = address;

                    } else if ("destination".equalsIgnoreCase(start_destination)) {
                        destination_title = time;
                        destination_snippet = address;
                    } else if ("waypoints".equalsIgnoreCase(start_destination)) {
                        int a = (int) marker.getTag();
                        address_waypoint_list.add(a, address);
                        time_waypoint_list.add(a, time);
                    }
                    //   marker.setTitle("Waypoint" + (a + 1));
                    //  Toast.makeText(LocationMapsActivity.this, "OK click", Toast.LENGTH_SHORT).show();

                }


            }
        });



        /*LinearLayout ll = new LinearLayout(this);
        final Button edit = new Button(this);
        final Button delete = new Button(this);
        RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(layoutparams);
   //     et.setLayoutParams(layoutparams);
     //   et.setHint("route name");
    //    et.setText(route_name + "");

        edit.setLayoutParams(layoutparams);
        delete.setLayoutParams(layoutparams);

        edit.setText("Edit");
        delete.setText("Delete");
        ll.addView(edit);
        ll.addView(delete);
        ad.setView(ll);
        ad.setTitle("Marker");
        ad.setMessage("");
        ad.setIcon(android.R.drawable.ic_menu_directions);
        ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                *//*String textString = et.getText().toString(); // Converts the value of getText to a string.
                if (textString != null && textString.trim().length() == 0) {
                    Toast toast = Toast.makeText(LocationMapsActivity.this, "Please enter a route name", Toast.LENGTH_SHORT);
                    toast.show();
                    displayAlertDialog();

                } else {

                    savedRouteDataInDataBase(route_name, textString);
                    route_name = textString;
                    route_list.clear();
                    route_list.add("Select");
                    PMSDatabase.getInstance(LocationMapsActivity.this).getRouteName(route_list);
                    adapterProject.notifyDataSetChanged();
                    Toast.makeText(LocationMapsActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                }*//*
            }
        });

        ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(LocationMapsActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
        if (!LocationMapsActivity.this.isFinishing()) {
            ad.show();
        }*/

    }

    private void sendMultiplyStopDataToServer(final List<StopDataItem> stopDataItemList) throws JSONException {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "RouteStoppagesApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&action=multipleStopageMasterEntry";

        Log.e("url", url);

        JSONObject param = new JSONObject();
        param.put("data",new JSONArray(new Gson().toJson(stopDataItemList)));
        Log.e("param", param.toString());
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                //"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107",
             /*   "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&action=multipleStopageMasterEntry"*/
                url, param
                ,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        Log.e("sendMultiplyStopData", jsonObject.toString());


                        if ("200".equalsIgnoreCase(jsonObject.optString("code"))) {
                            JSONArray jsonArray = jsonObject.optJSONArray("result");

                            for (int i = 0; jsonArray.length() > i; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                String status = jsonObject1.optString("status", "key not found");


                                //         Toast.makeText(LocationMapsActivity.this, status, Toast.LENGTH_SHORT).show();
                                // if ("success".equalsIgnoreCase(status) && "No Changs".equalsIgnoreCase(status))
                                {
                                    String id = jsonObject1.optString("MasterEntryId");
                                    vehicleRouteData.setRoute(vehicleRouteData.getRoute() + id);
                                    if ((i + 1) != jsonArray.length())
                                        vehicleRouteData.setRoute(vehicleRouteData.getRoute() + ",");

                                }
                            }
                            if (vehicleRoutedataItem_click_edit == null) {
                                try {
                                    postRouteDataToServer(vehicleRouteData);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //  Toast.makeText(LocationMapsActivity.this,""+ vehicleRoutedataItem_click_edit.getVehicleRouteId(), Toast.LENGTH_SHORT).show();
                                vehicleRouteData.setVehicleRouteId(vehicleRoutedataItem_click_edit.getVehicleRouteId());
                                //  vehicleRouteData.setVehicleRouteId("6");
                                putRouteDataToServer(vehicleRouteData);
                                Log.e("test", new Gson().toJson(vehicleRouteData).toString());
                            }
                        } else {
                            Toast.makeText(LocationMapsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }


                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();
            }
        }

        );
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //   AppController.getInstance().addToRequestQueue(strReq, "sendMultiplyStopDataToServer");
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "sendMultiplyStopDataToServer");
        //queue.add(strReq);


    }

   /* private void putStopDataToServer(final StopDataItem stopDataItem) {
        Utility.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();
        StringRequest strReq = new StringRequest(Request.Method.PUT,
                //"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107",
                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217",
                "http://192.168.1.161/apischoolerp/RouteStoppageApi.php?databaseName=CBA98217" +
                        "&data={\"data\":{\"MasterEntryValue\":\""+stopDataItem.getMasterEntryValue().replace(" ","+")+"\"" +
                        ",\"StopLat\":\""+stop_lat+"\",\"StopLong\":\""+stop_long+"\"},\"filter\":{\"MasterEntryId\":\""+stopDataItem.getMasterEntryId()+"\"}}",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("putStopDataToServer", response);
                        Log.d("URL", "http://192.168.1.161/apischoolerp/RouteStoppageApi.php?databaseName=CBA98217" +
                                "&data={\"data\":{\"MasterEntryValue\":\""+stopDataItem.getMasterEntryValue().replace(" ","+")+"\"" +
                                ",\"StopLat\":\""+stop_lat+"\",\"StopLong\":\""+stop_long+"\"},\"filter\":{\"MasterEntryId\":\""+stopDataItem.getMasterEntryId()+"\"}}");
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                *//*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*//*
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LocationMapsActivity.this);
                                alertDialog.setMessage("Updated Successfully");
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if(!LocationMapsActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {


                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Utility.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            *//*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppController.getInstance().addToRequestQueue(strReq,"putStopDataToServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }*/

    private void prepareStopdata() throws JSONException {

        List<StopDataItem> stopDataItemList_save_route = new ArrayList<>();
        if (start_stop == null) {
           /* start_stop = new StopDataItem();
            start_stop.setMasterEntryName("RouteStoppage");
            start_stop.setMasterEntryValue(start_snippet);
            start_stop.setMasterEntryStatus("Active");
            start_stop.setGradeRange("");
            start_stop.setGrade("");
            start_stop.setStopLat(latlong_start.latitude + "");
            start_stop.setStopLong(latlong_start.longitude + "");
            start_stop.setStopTime(start_title);
            //    postStopDataToServer(start_stop);
            stopDataItemList_save_route.add(0, start_stop);*/

        } else {
            //    start_stop.setMasterEntryName("RouteStoppage");
            //     start_stop.setMasterEntryStatus("Active");
            //     start_stop.setGradeRange("");
            //     start_stop.setGrade("");

           /* start_stop.setMasterEntryValue(start_snippet);
            start_stop.setStopLat(latlong_start.latitude + "");
            start_stop.setStopLong(latlong_start.longitude + "");
            start_stop.setStopTime(start_title);
            stopDataItemList_save_route.add(0, start_stop);*/
            //  putStopDataToServer(start_stop);
        }

        if (destination_stop == null) {
           /* destination_stop = new StopDataItem();
            destination_stop.setMasterEntryName("RouteStoppage");
            destination_stop.setMasterEntryValue(destination_snippet);
            destination_stop.setMasterEntryStatus("Active");
            destination_stop.setGradeRange("");
            destination_stop.setGrade("");
            destination_stop.setStopLat(latlong_destination.latitude + "");
            destination_stop.setStopLong(latlong_destination.longitude + "");
            destination_stop.setStopTime(destination_title);
            //  postStopDataToServer(start_stop);
            stopDataItemList_save_route.add(1, destination_stop);*/

        } else {

            //    start_stop.setMasterEntryName("RouteStoppage");
            //     start_stop.setMasterEntryStatus("Active");
            //     start_stop.setGradeRange("");
            //     start_stop.setGrade("");

          /*  destination_stop.setMasterEntryValue(destination_snippet);
            destination_stop.setStopLat(latlong_destination.latitude + "");
            destination_stop.setStopLong(latlong_destination.longitude + "");
            destination_stop.setStopTime(destination_title);
            //  putStopDataToServer(start_stop);*/
            stopDataItemList_save_route.add(1, destination_stop);
        }

        if (!latlong_waypoint_list.isEmpty() && latlong_waypoint_list.size() > 0) {
            for (int i = 0; i < latlong_waypoint_list.size(); i++) {
                try {
                    StopDataItem midDataItem = stopDataItemList_stop.get(i);
                    if (midDataItem == null) {
                      /*  midDataItem = new StopDataItem();
                        midDataItem.setMasterEntryName("RouteStoppage");
                        midDataItem.setMasterEntryValue(address_waypoint_list.get(i));
                        midDataItem.setMasterEntryStatus("Active");
                        midDataItem.setGradeRange("");
                        midDataItem.setGrade("");
                        midDataItem.setStopLat(latlong_waypoint_list.get(i).latitude + "");
                        midDataItem.setStopLong(latlong_waypoint_list.get(i).longitude + "");
                        midDataItem.setStopTime(time_waypoint_list.get(i));*/
                    } else {
                      /*  midDataItem.setMasterEntryValue(address_waypoint_list.get(i));
                        midDataItem.setStopLat(latlong_waypoint_list.get(i).latitude + "");
                        midDataItem.setStopLong(latlong_waypoint_list.get(i).longitude + "");
                        midDataItem.setStopTime(time_waypoint_list.get(i));*/
                    }
                    stopDataItemList_save_route.add(midDataItem);

                } catch (Exception e) {
                    Toast.makeText(this, e.toString() + "", Toast.LENGTH_SHORT).show();
                }
            }
        }

        sendMultiplyStopDataToServer(stopDataItemList_save_route);
    }

    private void savedRouteDataInDataBase(final String old_route_name, final String route_name,
                                          final String vehicle_name, final String vehicle_no,
                                          final String route_to) {


        ContentValues contentValue = new ContentValues();
        contentValue.put("route_name", route_name);
        contentValue.put("route_to", route_to);
        contentValue.put("vehicle_name", vehicle_name);
        contentValue.put("vehicle_no", vehicle_no);
        contentValue.put("start_lat", latlong_start.latitude);
        contentValue.put("start_long", latlong_start.longitude);
        contentValue.put("destination_lat", latlong_destination.latitude);
        contentValue.put("destination_long", latlong_destination.longitude);

        contentValue.put("start_title", start_title);
        contentValue.put("start_snippet", start_snippet);
        contentValue.put("destination_title", destination_title);
        contentValue.put("destination_snippet", destination_snippet);


        contentValue.put("route_url", route_url);
        /*if (latlong_waypoint != null) {
            contentValue.put("waypoint_lat", latlong_waypoint.latitude);
            contentValue.put("waypoint_long", latlong_waypoint.longitude);
        }*/
        if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            StringBuilder waypoint_lat = new StringBuilder();
            StringBuilder waypoint_long = new StringBuilder();
            for (int init = 0; latlong_waypoint_list.size() > init; init++) {
                LatLng l = latlong_waypoint_list.get(init);
                waypoint_lat.append(l.latitude);
                waypoint_long.append(l.longitude);
                if (latlong_waypoint_list.size() != (init + 1)) {
                    waypoint_lat.append(",");
                    waypoint_long.append(",");

                }
            }
            contentValue.put("waypoint_lat", waypoint_lat.toString());
            contentValue.put("waypoint_long", waypoint_long.toString());
        } else {
            contentValue.put("waypoint_lat", "");
            contentValue.put("waypoint_long", "");
        }

        if (address_waypoint_list != null && time_waypoint_list != null
                && !address_waypoint_list.isEmpty() && !time_waypoint_list.isEmpty()) {

            StringBuilder address_waypoint = new StringBuilder();
            StringBuilder time_waypoint = new StringBuilder();
            for (int init = 0; address_waypoint_list.size() > init; init++) {
                String address = address_waypoint_list.get(init);
                String time = time_waypoint_list.get(init);

                address_waypoint.append(address);
                time_waypoint.append(time);
                if (address_waypoint_list.size() != (init + 1)) {
                    address_waypoint.append(",");
                    time_waypoint.append(",");

                }
            }
            contentValue.put("waypoint_address", address_waypoint.toString());
            contentValue.put("waypoint_time", time_waypoint.toString());
        } else {
            contentValue.put("waypoint_address", "");
            contentValue.put("waypoint_time", "");
        }


        // PMSDatabase.getInstance(this).setRouteMapData(contentValue, old_route_name);
    }

    /*private void createWayPoints() {
        // mMap.setMyLocationEnabled(true);

        latlong_waypoint_list.add(new LatLng(23.231116, 77.433592));

        final int latlong_waypoint_list_size = latlong_waypoint_list.size();

        Toast.makeText(LocationMapsActivity.this,"size " +latlong_waypoint_list_size ,Toast.LENGTH_SHORT).show();

        for (int init = 0; latlong_waypoint_list_size > init; init++) {

            Marker mw =  mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init)).title("Waypoint " + (init + 1))//.snippet("-")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).draggable(true));
            mw.showInfoWindow();
            mw.setTag(init);
            final int finalInit = init;
            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    marker.showInfoWindow();
                    int a = (int)marker.getTag();
                    //   Toast.makeText(LocationMapsActivity.this, "Hello world " +a, Toast.LENGTH_SHORT).show();
                    //   Toast.makeText(LocationMapsActivity.this, "qwerty " + latlong_waypoint_list_size, Toast.LENGTH_SHORT).show();
                    latlong_waypoint_list.set(a, marker.getPosition());

                    //     if(start_destination.equalsIgnoreCase("waypoints_new"))


                }
            });
        }

        }*/


    private void actionBarEdit() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            TextView textview = new TextView(getApplicationContext());
            RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            textview.setLayoutParams(layoutparams);
            textview.setText(actionBar.getTitle() + "");
            //textview = (TextView) this.findViewById(R.id.toolbar_title_feature);
            textview.setSingleLine();
            textview.setTextColor(Color.WHITE);
            textview.setEllipsize(TextUtils.TruncateAt.END);
            textview.setGravity(Gravity.CENTER);
            textview.setTextSize(18);
            textview.setTypeface(Typeface.SERIF);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            actionBar.setCustomView(textview, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //  actionBar.setIcon(R.drawable.rateourapp);
            // actionBar.setHomeButtonEnabled(true);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.bottom_navigation_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        // overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        return true;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {


      /*  mMap = googleMap;

        mMap.addMarker(new MarkerOptions().position(latlng.get(0)).title("Last Location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mMap.addMarker(new MarkerOptions().position(latlng.get(latlng.size()-1)).title("Current Location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));



        //     mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //      mMap.setMyLocationEnabled(true);
        CameraPosition position = new CameraPosition.Builder()
                .target(latlng.get(latlng.size()-1))
                .zoom(20).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                .geodesic(true)
                .addAll(latlng)
                .strokeColor(Color.BLUE)
                .fillColor(Color.GREEN)
                .strokeWidth(5)
        );*/

        // old coding

       /* PolylineOptions options = new PolylineOptions()
                .width(5)
                .color(Color.BLUE)
                .geodesic(true);
        for (int z = 0; z < latlng.size(); z++) {
            LatLng point = latlng.get(z);
            options.add(point);
        }

        mMap.addPolyline(options);*/ //old code

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //   mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        //  mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        // setMarkerAndCamere();
        //  getDirection();


        //for display route

        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(23.082567, 80.070910))
                .zoom(1).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        // for start marker
        //  setRouteMarker("");


    }

    private Marker m;

    private void setRouteMarker(final String check) {
        mHashMap.clear();

        mMap.clear();
        //   mMap.setMyLocationEnabled(true);
        LatLng latlng_default;
        if (default_current_lat_long == null)
            latlng_default = new LatLng(23.231116, 77.433592);
        else
            latlng_default = new LatLng(default_current_lat_long.getLatitude(), default_current_lat_long.getLongitude());

        if (latlong_start != null)
            latlng_default = mMap.getCameraPosition().target;




     /*   mMarkerA = mMap.addMarker(new MarkerOptions().position(new LatLng(12, 34)));
        mMarkerA.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("Marker A")));
        mMarkerB = mMap.addMarker(new MarkerOptions().position(new LatLng(13, 35)));
        mMarkerB.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("Marker B")));


*/

        if (check.equalsIgnoreCase("waypoints")) {
            if (latlong_start != null && latlong_destination != null) {

                /*   old title("Start")//.snippet("-") */
                Marker st = mMap.addMarker(new MarkerOptions().position(latlong_start)
                        .title(start_title)
                        .snippet(start_snippet)
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
                st.setTag("start");
                 /*   old title("Destination")//.snippet("-") */
                Marker dt = mMap.addMarker(new MarkerOptions().position(latlong_destination)
                        .title(destination_title)
                        .snippet(destination_snippet)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)));
                // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                dt.setTag("destination");
                //  mHashMap.put(m,"Start");
                // mHashMap.get(m);


          /*      if (latlong_waypoint != null) {
                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_waypoint).title("Waypoints")//.snippet("-")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).draggable(true));
                } else {
                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_start).title("Waypoint")//.snippet("-")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).draggable(true));
                }*/
            } else if (latlong_start != null) {
                Marker st = mMap.addMarker(new MarkerOptions().position(latlong_start)
                        .title(start_title).snippet(start_title)
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
                st.setTag("start");

/*
                if (latlong_waypoint != null) {
                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_waypoint).title("Waypoints")//.snippet("-")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).draggable(true));
                 //   latlong_waypoint_list.add(0, latlng_default);

                } else {
                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_start).title("Waypoint")//.snippet("-")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).draggable(true));
                  //  latlong_waypoint_list.add(0, latlng_default);
                }*/

            }
            if (latlong_waypoint_list.isEmpty())
                latlong_waypoint_list.add(latlng_default);
               /* latlong_waypoint_list.add(
                        new LatLng(( latlong_start.latitude+latlong_destination.latitude)/2,
                                ( latlong_start.longitude+latlong_destination.longitude)/2));
*/


            final int latlong_waypoint_list_size = latlong_waypoint_list.size();

            for (int init = 0; latlong_waypoint_list_size > init; init++) {

                /* old
                *  Marker mw = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_waypoint_list.get(init)).title("Stop " + (init + 1))//.snippet("-")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).draggable(true));

                * */
                try {

                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_waypoint_list.get(init))
                            .title(time_waypoint_list.get(init))
                            .snippet(address_waypoint_list.get(init))
                            // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                            .draggable(true));
                    m.setTag(init);
                    if (time_waypoint_list.get(init).equalsIgnoreCase(currentMarkerInfo)) {
                        currentMarkerSelected = m;
                    }

                } catch (Exception e) {
                    m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_waypoint_list.get(init))
                            .title(Config.getTime())
                           /*.snippet(address_waypoint_list.get(init))*/
                            // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                            .draggable(true));
                    m.setTag(init);
                    displayAlertDialogForMarkerClick(m, true);
                } finally {
                    m.showInfoWindow();

                }






               /* mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                        if (check.equalsIgnoreCase("waypoints")) {
                            marker.hideInfoWindow();
                            int a = (int) marker.getTag();
                            latlong_waypoint_list.set(a, marker.getPosition());
                            Toast.makeText(LocationMapsActivity.this, "Hello world " +a, Toast.LENGTH_SHORT).show();
                            marker.setSnippet("" + marker.getPosition().latitude
                                        + ", " + marker.getPosition().longitude);
                            marker.setTitle("Waypoint" +a);
                            marker.showInfoWindow();
                            }
                    }
                });*/


            }

            //  // TODO: 1/16/2017 handling when user direct select waypoint without destination

        } else if (check.equalsIgnoreCase("start")) {
            if (latlong_start != null) {

                /* old title = "start" */
                m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_start).title(start_title)
                        .snippet(start_snippet)
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green))
                        .draggable(true));
                m.setTag("start");
            } else {
                /* old title = "Default Location" */
                m = mMap.addMarker(new MarkerOptions().position(latlong_start = latlng_default).title(start_title = Config.getTime())//.snippet("Hello world")
                        //  .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green))
                        .draggable(true));
                m.setTag("start");
                displayAlertDialogForMarkerClick(m, true); // for new marker
                /*CameraPosition position = new CameraPosition.Builder()
                        .target(latlng_default)
                        .zoom(13).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/
            }

            if (latlong_destination != null) {
                Log.e("final", latlong_destination.latitude + " " + latlong_destination.longitude);

                mMap.addMarker(new MarkerOptions().position(latlong_destination)
                        .title(destination_title)
                        .snippet(destination_snippet)
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)))
                        .setTag("destination");
            }
/*
            if (latlong_waypoint != null) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint).title("Waypoint")//.snippet("-")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            }*/

            final int latlong_waypoint_list_size = latlong_waypoint_list.size();
/*  old*/
            /*for (int init = 0; latlong_waypoint_list_size > init; init++) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init)).title("Stop " + (init + 1))//.snippet("-")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).draggable(false)).showInfoWindow();
            }*/

            for (int init = 0; latlong_waypoint_list_size > init; init++) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init))
                        .title(time_waypoint_list.get(init))
                        .snippet(address_waypoint_list.get(init))
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false)).showInfoWindow();
            }

        } else if (check.equalsIgnoreCase("destination")) {
            if (latlong_destination != null) {
                m = mMap.addMarker(new MarkerOptions().position(latlng_default = latlong_destination).title(destination_title)
                        .snippet(destination_snippet)
                        //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red))
                        .draggable(true));
                m.setTag("destination");
            } else {
                m = mMap.addMarker(new MarkerOptions().position(latlong_destination = latlng_default).title(destination_title =
                        Config.getTime())//.snippet("Hello world")
                        //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red))
                        .draggable(true));
                m.setTag("destination");
                displayAlertDialogForMarkerClick(m, true);  // new marker
            }

            if (latlong_start != null) {
                mMap.addMarker(new MarkerOptions().position(latlong_start).title(start_title)
                        .snippet(start_snippet)
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)))
                        .setTag("start");
            }

            /*if (latlong_waypoint != null) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint).title("Waypoint")//.snippet("-")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            }*/

            final int latlong_waypoint_list_size = latlong_waypoint_list.size();
/*old*/
           /* for (int init = 0; latlong_waypoint_list_size > init; init++) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init)).title("Stop " + (init + 1))//.snippet("-")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).draggable(false)).showInfoWindow();
            }*/

            for (int init = 0; latlong_waypoint_list_size > init; init++) {
                mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init))
                        .title(time_waypoint_list.get(init))
                        .snippet(address_waypoint_list.get(init))
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false)).showInfoWindow();
            }

        } else {
            m = mMap.addMarker(new MarkerOptions().position(latlng_default).title("Default Location")//.snippet("Hello world")
                    //  .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green))
                    .draggable(true));

        }
        //  m.showInfoWindow();  // show before set map position


        // map_zoom_level =  mMap.getCameraPosition().zoom;

        //      Toast.makeText(this, "" + map_zoom_level, Toast.LENGTH_SHORT).show();

        if (map_zoom_level != (float) 0) {

            CameraPosition position = new CameraPosition.Builder()
                    .target(latlng_default)
                    .zoom(map_zoom_level = mMap.getCameraPosition().zoom).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        } else {
            CameraPosition position = new CameraPosition.Builder()
                    .target(latlng_default)
                    .zoom(13).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
            map_zoom_level = 13;
        }


        // TODO: 2/16/2017 as per requirement code comment, this code is in lat_start == null





       /* mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            public void onCameraChange(CameraPosition arg0) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(arg0.target));
            }
        });*/

      /*  mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                animateMarker(m,mMap.getCameraPosition().target,false);
            }
        });

        mMap.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
            @Override
            public void onCameraMoveCanceled() {

            }
        });*/


      /*  mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                m.hideInfoWindow();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(start_destination.equalsIgnoreCase("destination"))
                        {
                            m.hideInfoWindow();
                              animateMarker(m, mMap.getCameraPosition().target, false);
                            //  m.setTitle("Latitude Longitude:");
                            m.setSnippet(""+mMap.getCameraPosition().target.latitude
                                    +", "+mMap.getCameraPosition().target.longitude);

                            latlong_destination = mMap.getCameraPosition().target;
                            m.setTitle("Destination");
                            m.showInfoWindow();
                        }
                        else if(start_destination.equalsIgnoreCase("start"))
                        {
                            m.hideInfoWindow();
                            animateMarker(m, mMap.getCameraPosition().target, false);
                            //  m.setTitle("Latitude Longitude:");
                            m.setSnippet(""+mMap.getCameraPosition().target.latitude
                                    +", "+mMap.getCameraPosition().target.longitude);

                            latlong_start = mMap.getCameraPosition().target;
                            m.setTitle("Start");
                            m.showInfoWindow();
                        }
                        else if(start_destination.equalsIgnoreCase("waypoints"))
                        {
                            m.hideInfoWindow();
                            animateMarker(m, mMap.getCameraPosition().target, false);
                            //  m.setTitle("Latitude Longitude:");
                            m.setSnippet(""+mMap.getCameraPosition().target.latitude
                                    +", "+mMap.getCameraPosition().target.longitude);

                            latlong_waypoint = mMap.getCameraPosition().target;
                            m.setTitle("Waypoint");
                            m.showInfoWindow();
                        }
                    }
                    //Ex.. using progressbar to set the pogress
                    //Updating the UI is done inside the Handler
                },1000);

                  *//*  new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            animateMarker(m, mMap.getCameraPosition().target, false);
                            //  m.setTitle("Latitude Longitude:");
                            m.setSnippet(""+mMap.getCameraPosition().target.latitude
                                    +", "+mMap.getCameraPosition().target.longitude);
                        }
                    }).start();


                m.showInfoWindow();*//*
                return false;
            }
        });
*/   // not working onLocationButtonClick

        /*mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;  // null for default
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                // Getting the position from the marker
                final LatLng latLng = arg0.getPosition();

                // Getting reference to the TextView to set latitude
                final TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);

                // Getting reference to the TextView to set longitude
                final TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);

                ImageButton ib_lat_edit = (ImageButton) v.findViewById(R.id.ib_lat_edit);
                ib_lat_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tvLat.setText("lat edit Click");
                    }
                });

                ImageButton ib_lng_edit = (ImageButton) v.findViewById(R.id.ib_lng_edit);
                ib_lng_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tvLng.setText("lng edit Click");
                    }
                });




                // Setting the latitude
                tvLat.setText("Latitude:" + latLng.latitude);

                // Setting the longitude
                tvLng.setText("Longitude:"+ latLng.longitude);

                // Returning the view containing InfoWindow contents
                return v;

            }
        });*/


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // Toast.makeText(LocationMapsActivity.this, "info click", Toast.LENGTH_SHORT).show();
                // marker.hideInfoWindow();
                //  marker.setTitle("Marker Info window clicked");
                // marker.showInfoWindow();

                if (marker.isDraggable())
                    if (!tablayout_create_display.getTabAt(0).isSelected())
                        displayAlertDialogForMarkerClick(marker, false);





               /* PopupMenu a_popup = new PopupMenu(getApplicationContext());
                a_popup.getMenuInflater().inflate(R.menu.popup_a, a_popup.getMenu());


                a_popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(getApplicationContext(), "Clicked "+item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                a_popup.show();
            */


            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //  mMap.clear();
                //     mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target));
                //   m.setPosition(mMap.getCameraPosition().target);
                if (start_destination.equalsIgnoreCase("destination")) {
                    m.hideInfoWindow();
                    /*
                    animateMarker(m, mMap.getCameraPosition().target, false);
                    //  m.setTitle("Latitude Longitude:");
                    m.setSnippet(""+mMap.getCameraPosition().target.latitude
                            +", "+mMap.getCameraPosition().target.longitude);

                    latlong_destination = mMap.getCameraPosition().target;
                    */
                    // TODO: 1/19/2017 if this uncomment then remove all coding from onMarkerDrag if else coding and below 3 line coding

                    //      latlong_destination = m.getPosition();
                  /*  m.setSnippet("" + m.getPosition().latitude
                            + ", " + m.getPosition().longitude);*/
                    //   m.setTitle("Destination");
                    m.showInfoWindow();
                } else if (start_destination.equalsIgnoreCase("start")) {
                    m.hideInfoWindow();

                    /*
                   animateMarker(m, mMap.getCameraPosition().target, false);
                    //  m.setTitle("Latitude Longitude:");
                    m.setSnippet(""+mMap.getCameraPosition().target.latitude
                            +", "+mMap.getCameraPosition().target.longitude);

                    latlong_start = mMap.getCameraPosition().target;
                    */
                    // TODO: 1/19/2017 if this uncomment then remove all coding from onMarkerDrag if else coding and below 3 line coding

                    //      latlong_start = m.getPosition();
                 /*   m.setSnippet("" + m.getPosition().latitude
                            + ", " + m.getPosition().longitude);
*/
                    //    m.setTitle("Start");
                    m.showInfoWindow();
                } else if (start_destination.equalsIgnoreCase("waypoints")) {
                    //   m.hideInfoWindow();
                    //  int a = (int) m.getTag();
                  /*  m.setSnippet("" + m.getPosition().latitude
                            + ", " + m.getPosition().longitude);*/
                    //   m.setTitle("Stop " + (a + 1));
                    //  m.showInfoWindow();
                }


            }
        });

      /*  mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
             //   .setPosition(mMap.getCameraPosition().target);//to center in map
              //  mMap.set
              //  mMap.clear();
             //   mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target));

              //  animateMarker(m,mMap.getCameraPosition().target,false);

            }
        });*/


//note marker is global
        mMap.setOnMarkerClickListener(
                new GoogleMap.OnMarkerClickListener() {
                    // boolean doNotMoveCameraToCenterMarker = true;
                    public boolean onMarkerClick(Marker marker) {
                        //Do whatever you need to do here ....

                        if (tablayout_create_display.getTabAt(0).isSelected() /*|| tablayout_create_map.getTabAt(3).isSelected()*/)
                            return false;

                        try {
                            if ("start".equalsIgnoreCase((String) marker.getTag())) {
                                tablayout_create_map.getTabAt(0).select();
                                return false;
                            } else if ("destination".equalsIgnoreCase((String) marker.getTag())) {
                                tablayout_create_map.getTabAt(1).select();
                                return false;
                            } else {
                                currentMarkerInfo = marker.getTitle();
                                tablayout_create_map.getTabAt(2).select();
                                if (currentMarkerSelected != null)
                                    currentMarkerSelected.showInfoWindow();
                                return false;  // TODO: 1/28/2017 get lat long of current marker and in current list of marker then display showinfo
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            //     Toast.makeText(LocationMapsActivity.this, "exception", Toast.LENGTH_SHORT).show();
                            if (!tablayout_create_map.getTabAt(2).isSelected())
                                tablayout_create_map.getTabAt(2).select();
                            else return false;

                            return false;
                        }
                        // return true;
                    }
                });


        // set on marker click listener
       /* mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!"".equalsIgnoreCase(route_name)) {
                    if (start_destination.equalsIgnoreCase("waypoints")) {
                        marker.hideInfoWindow();
                       *//* try {
                            int a = (int) marker.getTag();
                            //latlong_waypoint_list.set(a, marker.getPosition());
                            latlong_waypoint_list.remove(a);
                            if (!latlong_waypoint_list.isEmpty())
                                setRouteMarker(start_destination = "waypoints");
                            else
                                setRouteMarker(start_destination = "start");

                            Toast.makeText(LocationMapsActivity.this, "Remove" + a, Toast.LENGTH_SHORT).show();
                            marker.setSnippet("" + marker.getPosition().latitude
                                    + ", " + marker.getPosition().longitude);
                            marker.setTitle("Waypoint" + (a + 1));
                        } catch (Exception e) {

                        } finally {
                            marker.showInfoWindow();
                        }*//*
                    } else
                        return false;

                    return false;
                }
                return false;
            }
        });*/

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragStart..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragEnd..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);

                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                //     mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(arg0.getPosition(),13));

                if (start_destination.equalsIgnoreCase("destination")) {
                    m.hideInfoWindow();
                    //  animateMarker(m, mMap.getCameraPosition().target, false);
                    //  m.setTitle("Latitude Longitude:");
                    //   latlong_destination = mMap.getCameraPosition().target;  // latlong directly get from marker object
                    latlong_destination = arg0.getPosition();  // latlong directly get from marker object

               /*     m.setSnippet("" + mMap.getCameraPosition().target.latitude
                            + ", " + mMap.getCameraPosition().target.longitude);
*/

                    //   m.setTitle("Destination");
                    m.showInfoWindow();
                } else if (start_destination.equalsIgnoreCase("start")) {
                    m.hideInfoWindow();
                    // animateMarker(m, mMap.getCameraPosition().target, false);
                    //  m.setTitle("Latitude Longitude:");
                    //  latlong_start = mMap.getCameraPosition().target; old
                    latlong_start = arg0.getPosition();

             /*       m.setSnippet("" + mMap.getCameraPosition().target.latitude
                            + ", " + mMap.getCameraPosition().target.longitude);
*/

                    //       m.setTitle("Start");
                    m.showInfoWindow();
                }/* else if (start_destination.equalsIgnoreCase("waypoints")) {
                    m.hideInfoWindow();
                    //   animateMarker(m, mMap.getCameraPosition().target, false);
                    //  m.setTitle("Latitude Longitude:");
                    m.setSnippet("" + mMap.getCameraPosition().target.latitude
                            + ", " + mMap.getCameraPosition().target.longitude);

                    latlong_waypoint = mMap.getCameraPosition().target;
                    m.setTitle("Waypoint");
                    m.showInfoWindow();
                }*/ else if (start_destination.equalsIgnoreCase("waypoints")) {
                    arg0.hideInfoWindow();
                    int a = (int) arg0.getTag();
                    latlong_waypoint_list.set(a, arg0.getPosition());
                    // Toast.makeText(LocationMapsActivity.this, "Hello world " +a, Toast.LENGTH_SHORT).show();
                 /*   arg0.setSnippet("" + arg0.getPosition().latitude
                            + ", " + arg0.getPosition().longitude);
                */

                    arg0.setTitle(time_waypoint_list.get(a));
                    arg0.setSnippet(address_waypoint_list.get(a));
                    arg0.showInfoWindow();
                }


            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
                Log.i("System out", "onMarkerDrag...");
                //      mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
            }
        });

//Don't forget to Set draggable(true) to marker, if this not set marker does not drag.

        //mMap.addMarker(new MarkerOptions().position(new LatLng(23.231116,77.433592)).icon(BitmapDescriptorFactory .fromResource(R.drawable.ic_menu_camera)).draggable(true));

    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void createRoute() {

        mMap.clear();
        Marker marker1 = mMap.addMarker(new MarkerOptions().position(latlong_start).title(start_title)
                .snippet(start_snippet)
                // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
        marker1.setTag("start");
        marker1.showInfoWindow();
        Marker marker2 = mMap.addMarker(new MarkerOptions().position(latlong_destination).title(destination_title)
                .snippet(destination_snippet)
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)));
        marker2.setTag("destination");
        marker2.showInfoWindow();
       /* Marker marker3 = null;
        if (latlong_waypoint != null) {
            marker3 = mMap.addMarker(new MarkerOptions().position(latlong_waypoint).title("waypoint")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            marker3.showInfoWindow();
        }*/

        ArrayList<Marker> marker__waypoint_list = null;
        if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            marker__waypoint_list = new ArrayList<>(latlong_waypoint_list.size());
            final int latlong_waypoint_list_size = latlong_waypoint_list.size();

            for (int init = 0; latlong_waypoint_list_size > init; init++) {
                Marker m = null;

                m = mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init))
                        .title(time_waypoint_list.get(init))
                        .snippet(address_waypoint_list.get(init))
                        //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false));
                //   m.setTag("waypoints");
                marker__waypoint_list.add(m);
            }

        }


     /*   //       For multiple marker in map
        Marker marker3 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.247223, 77.444381)).title("Govind Pura")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker4 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.249156, 77.476120)).title("Piplani Raisen Road")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker0 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.205012, 77.085078)).title("Sehore")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));*/


        //     mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //      mMap.setMyLocationEnabled(true);
       /* CameraPosition position = new CameraPosition.Builder()
                .target(latlong_start)
                .target(latlong_destination)
                .zoom(13).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/


        // TODO: 1/12/2017 show all marker without use zoom functionality, but in case of bus we use zoom functionality see above
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
        builder.include(marker1.getPosition());
        // builder.include(marker0.getPosition());
        builder.include(marker2.getPosition());
       /* if (marker3 != null) {
            builder.include(marker3.getPosition());
        }*/
        if (marker__waypoint_list != null) {
            for (int init = 0; marker__waypoint_list.size() > init; init++) {
                builder.include(marker__waypoint_list.get(init).getPosition());
            }
        }

        //  builder.include(marker3.getPosition());
        //  builder.include(marker4.getPosition());

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        // int padding = (int) (height * 0.90); // offset from edges of the map 12% of screen

        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.35);
        // Default int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);  // mMap.moveCamera;
   /*     float currentZoom = mMap.getCameraPosition().zoom;
     //   And if this level is greater than 16, which it will only be if there are very less markers or all the markers are very close to each other, then simply zoom out your map at that particular position only by seting the zoom level to 16.
            if(currentZoom>16)
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));  // animate camera take time i.e. this will not work try to change animate camera to move, problem will solve by setOncameraIdleListener setting by test this condition

*/
    }

    private void createRouteByParam(final LatLng latlong_start,
                                    final LatLng latlong_destination,
                                    final List<LatLng> latlong_waypoint_list,
                                    final String start_title,
                                    final String start_snippet,
                                    final String destination_title,
                                    final String destination_snippet) {

        mMap.clear();
        Marker marker1 = mMap.addMarker(new MarkerOptions().position(latlong_start).title(start_title)
                .snippet(start_snippet)
                //  .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
        marker1.showInfoWindow();
        Marker marker2 = mMap.addMarker(new MarkerOptions().position(latlong_destination).title(destination_title)
                .snippet(destination_snippet)
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)));
        marker2.showInfoWindow();
       /* Marker marker3 = null;
        if (latlong_waypoint != null) {
            marker3 = mMap.addMarker(new MarkerOptions().position(latlong_waypoint).title("waypoint")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            marker3.showInfoWindow();
        }*/


        ArrayList<Marker> marker__waypoint_list = null;
        if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            marker__waypoint_list = new ArrayList<>(latlong_waypoint_list.size());
            final int latlong_waypoint_list_size = latlong_waypoint_list.size();

            for (int init = 0; latlong_waypoint_list_size > init; init++) {
                marker__waypoint_list.add(mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init))
                        .title(time_waypoint_list.get(init))
                        .snippet(address_waypoint_list.get(init))
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false)));
            }

        }

     /*   //       For multiple marker in map
        Marker marker3 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.247223, 77.444381)).title("Govind Pura")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker4 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.249156, 77.476120)).title("Piplani Raisen Road")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker0 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.205012, 77.085078)).title("Sehore")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));*/


        //     mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //      mMap.setMyLocationEnabled(true);
       /* CameraPosition position = new CameraPosition.Builder()
                .target(latlong_start)
                .target(latlong_destination)
                .zoom(13).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/


        // TODO: 1/12/2017 show all marker without use zoom functionality, but in case of bus we use zoom functionality see above
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
        builder.include(marker1.getPosition());
        // builder.include(marker0.getPosition());
        builder.include(marker2.getPosition());
       /* if (marker3 != null) {
            builder.include(marker3.getPosition());
        }*/
        //  builder.include(marker3.getPosition());
        //  builder.include(marker4.getPosition());


        if (marker__waypoint_list != null) {
            for (int init = 0; marker__waypoint_list.size() > init; init++) {
                builder.include(marker__waypoint_list.get(init).getPosition());
            }
        }

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        //int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.35);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);  // mMap.moveCamera;
       /* float currentZoom = mMap.getCameraPosition().zoom;
        //   And if this level is greater than 16, which it will only be if there are very less markers or all the markers are very close to each other, then simply zoom out your map at that particular position only by seting the zoom level to 16.
        if(currentZoom>16)
            mMap.moveCamera(CameraUpdateFactory.zoomTo(16));  // animate camera take time i.e. this will not work try to change animate camera to move, problem will solve by setOncameraIdleListener setting by test this condition

*/
    }

    private void setMarkerAndCamere() {

        Marker marker1 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.231116, 77.433592)).title("Last Location")
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)));

        Marker marker2 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.302777, 77.402655)).title("Current Location")
                // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));


        //       For multiple marker in map
        Marker marker3 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.247223, 77.444381)).title("Govind Pura")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker4 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.249156, 77.476120)).title("Piplani Raisen Road")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker0 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.205012, 77.085078)).title("Sehore")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));


        //     mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //      mMap.setMyLocationEnabled(true);
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(23.302777, 77.402655))
                .zoom(13).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));


        // TODO: 1/12/2017 show all marker without use zoom functionality, but in case of bus we use zoom functionality see above
     /*   LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
        builder.include(marker1.getPosition());
        builder.include(marker0.getPosition());
        builder.include(marker2.getPosition());
        builder.include(marker3.getPosition());
        builder.include(marker4.getPosition());

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);  // mMap.moveCamera;
        float currentZoom = mMap.getCameraPosition().zoom;
     //   And if this level is greater than 16, which it will only be if there are very less markers or all the markers are very close to each other, then simply zoom out your map at that particular position only by seting the zoom level to 16.
            if(currentZoom>16)
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));  // animate camera take time i.e. this will not work try to change animate camera to move, problem will solve by setOncameraIdleListener setting by test this condition

*/

    }


    //The parameter is the server response
    public void drawPath(String result) {
        //Getting both the coordinates
        //     LatLng from = new LatLng(fromLatitude,fromLongitude);
        //     LatLng to = new LatLng(toLatitude,toLongitude);

        //Calculating the distance in meters

        // Double distance = SphericalUtil.computeDistanceBetween(from, to);

        //Displaying the distance
        //  Toast.makeText(this,String.valueOf(distance+" Meters"),Toast.LENGTH_SHORT).show();


        try {
            //Parsing json
            final JSONObject json = new JSONObject(result);

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            //  try {
            jRoutes = json.getJSONArray("routes");
            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    PolylineOptions polyoption = new PolylineOptions();
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                .get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        polyoption.addAll(list);
                            /*Polyline line = mMap.addPolyline(new PolylineOptions()
                                    .addAll(list)
                                    .width(5)
                                    .color(Color.RED)
                                    .geodesic(true)  // // TODO: 1/12/2017 put this outside and add list one time rather create polylin each time 
                            );*/

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat",
                                    Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng",
                                    Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    Polyline line = mMap.addPolyline(polyoption
                            .width(5)
                            .color(Color.RED)
                            .geodesic(true)  // // TODO: 1/12/2017 put this outside and add list one time rather create polylin each time
                    );
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        //  return routes;



           /* JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(20)
                    .color(Color.RED)
                    .geodesic(true)
            );


        }
        catch (JSONException e) {

        }*/
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void getDirection(final String route_url_default) {
        //Getting the URL
        //    String url = makeURL(fromLatitude, fromLongitude, toLatitude, toLongitude);
        if (!"default".equalsIgnoreCase(route_url_default)) {
            route_url = route_url_default;
        } else
            route_url = makeURL(latlong_start.latitude, latlong_start.longitude,
                    latlong_destination.latitude, latlong_destination.longitude); // for testing

        //Showing a dialog till we get the route
        final ProgressDialog loading = ProgressDialog.show(this, "Getting Route", "Please wait...", false, false);

        //Creating a string request
        StringRequest stringRequest = new StringRequest(route_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        //Calling the method drawPath to draw the path
                        //   Log.e("vishal_log",response);
                        // Utility.longInfo(response);
                        drawPath(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        Log.e("vishal_log", "error");
                    }
                });

        //Adding the request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
       /* if (latlong_waypoint != null) {
            urlString.append("&waypoints=optimize:true|");
            urlString.append(latlong_waypoint.latitude);
            urlString.append(",");
            urlString.append(latlong_waypoint.longitude);
        }*/

        if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            urlString.append("&waypoints=optimize:true");
            for (int init = 0; latlong_waypoint_list.size() > init; init++) {
                LatLng l = latlong_waypoint_list.get(init);
                urlString.append("|");
                urlString.append(l.latitude);
                urlString.append(",");
                urlString.append(l.longitude);
            }
        }
        urlString.append("&sensor=false&mode=driving&alternatives=false"); //*&sensor=false&mode=driving&alternatives=true*/
        //  urlString.append("&key=SERVER-KEY");
        return urlString.toString();

      /*  return
                "https://maps.googleapis.com/maps/api/directions/json?" +
                        "origin=23.231116,77.433592" +
                        "&destination=23.302777,77.402655" +
                        "&waypoints=optimize:true" +
                        "|23.247223,77.444381" +       // govindpura
                        "|23.249156,77.476120" +    // piplani
                        "|sehore" +    // sehore
                        "|23.255772,77.478451" +   // ayodhya bypass road
                        "|23.208038,77.444558" +  // habibganj
                        "|piplani" +  // piplani
                        "|Nadra+bus+stand" +  // bus stand

                        "&sensor=false";
*/
// return "https://maps.googleapis.com/maps/api/directions/json?origin=New+York,+NY&destination=Boston,+MA&waypoints=optimize:true|Providence,+RI|Hartford,+CT";

    }

    public void onClickMapRouteEdit(View view) {
        // displayFloatingButton(view);
        fab_click = true;
        map_fab_delete.setVisibility(View.INVISIBLE);
        view.setVisibility(View.INVISIBLE);
        tablayout_create_display.setTag("fab");
        //  Toast.makeText(this, "FabClick", Toast.LENGTH_SHORT).show();
        tablayout_create_map.setVisibility(View.VISIBLE);
        ll_select_route.setVisibility(View.INVISIBLE);
        //  tablayout_create_map.getTabAt(0).getCustomView().setSelected(true);

        tablayout_create_display.getTabAt(1).select();
        tablayout_create_map.getTabAt(0).select();

        //latlong_start = latlong_destination = latlong_waypoint = null;
        //latlong_waypoint_list.clear();
        // latlong_start =
        setRouteMarker(start_destination = "start");
        route_create = false;
        tablayout_create_map.getTabAt(3).select(); // select route for edit case

    }

    public void onClickMapRouteDelete(View view) {
        //  displayFloatingButton(view);
        //Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
        if (spn_select_route.getSelectedItemPosition() != 0) {

            AlertDialog.Builder adb = new AlertDialog.Builder(LocationMapsActivity.this);
            adb.setCancelable(false);
            // adb.setTitle("Confirmation");
            adb.setMessage("Are you sure want to delete ?");
            // adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    VehicleRouteDataItem vehicleRouteData = vehicleRouteDataItemList.get(spn_select_route.getSelectedItemPosition());
                    deleteVehicleRouteDataFromServer(vehicleRouteData);
                }
            });

            adb.setNegativeButton("CANCEL", null);

            if (!LocationMapsActivity.this.isFinishing())
                adb.show();
            return;
        }
    }

    private void deleteVehicleRouteDataFromServer(final VehicleRouteDataItem vehicleRouteData) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleRouteApi.php?databaseName=" + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&data="
                +
                "{\"filter\":{\"VehicleRouteId\":\"" + vehicleRouteData.getVehicleRouteId() + "\"}}";

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.DELETE,
                url
                /*"http://192.168.1.161/apischoolerp/VehicleRouteApi.php?" +
                        "databaseName=CBA98217&data=" +
                        "{\"filter\":{\"VehicleRouteId\":\"" + vehicleRouteData.getVehicleRouteId() + "\"}}"*/
                ,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("deleteVehicleDataFrom", response);
                       /* Log.d("URL", "http://192.168.1.161/apischoolerp/VehicleApi.php?" +
                                "databaseName=CBA98217&data=" +
                                "{\"filter\":{\"VehicleRouteId\":\"" + vehicleRouteData.getVehicleRouteId() + "\"}}");
                       */
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                get_vehicleRouteApi();
                                // remove(position);
                                // mExplosionField.explode(v);
                                Toast.makeText(LocationMapsActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                spn_select_route.setSelection(0);
                            } else
                                Toast.makeText(LocationMapsActivity.this, "Try again later !!!", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
/*        AppController.getInstance().addToRequestQueue(strReq, "deleteVehicleDataFromServer");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "deleteVehicleDataFromServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }


 /*   private void displayFloatingButton(View view) {

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        //   layoutParams.rightMargin += (int) (view.getWidth() * 1.7);
        //  layoutParams.bottomMargin += (int) (view.getHeight() * 0.25);
        //   view.setLayoutParams(layoutParams);
        view.startAnimation(show_fab_1);
        view.setClickable(true);
    }
*/

    private void get_routeToApi() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "MasterEntryApi.php?" +
                "databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&data={\"filter\":{\"MasterEntryName\":\"RouteTo\"}}";
        Log.e("get_routeToApi", url);
        StringRequest strReq = new StringRequest(Request.Method.GET,

                url
                ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("get_routeToApi", response.toString());


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    route_to_id.clear();
                                    route_to.clear();
                                    route_to.add("Select");
                                    route_to_id.add("0");

                                    for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        route_to.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                        route_to_id.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryId"));
                                    }
                                    displayAlertDialogForRouteSave();

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pDialog.dismiss();

                        }
                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Affiliates Id Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        )

        {

            // TODO: 2/6/2017 get k case me getParams call nai hota
          /*  @Override
            protected Map<String, String> getParams() {
                JSONObject jsonObject_masterentry = new JSONObject();
                Map<String, String> param = new HashMap<>();
                param.put("databaseName","A994A107");
                try {
                    jsonObject_masterentry.put("MasterEntryName", "RouteTo");
                    JSONObject jsonObject_filter = new JSONObject();
                    jsonObject_filter.put("filter", jsonObject_masterentry);
                    param.put("data", jsonObject_filter.toString());
                    Log.e("qwerty",jsonObject_filter.toString());
                    Log.e("qwerty",param.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return param;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
        //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
/*        AppController.getInstance().addToRequestQueue(strReq, "get_routeToApi");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "get_routeToApi");
    }


    @Override
    public void onLocationChanged(Location location) {
        loader_get_current_location.dismiss();
        default_current_lat_long = location;
        //  Toast.makeText(this, "onLocationChanged", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void getCurrentLocationLatLong() {
        loader_get_current_location = ProgressDialog.show(this, "Getting Current Location", "Please wait...", false, false);
        lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (lm != null) {
            //    Toast.makeText(this, "call", Toast.LENGTH_SHORT).show();
            //   default_current_lat_long = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            //   Toast.makeText(this, "call "+default_current_lat_long, Toast.LENGTH_SHORT).show();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "Location Services Disabled !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        lm.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0,
                0, this);
        Log.d("Network", "Network");
        return;
    }

    private void getStopDataFromServer(final String s) throws JSONException {
        pb.setVisibility(View.VISIBLE);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "RouteStoppagesApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&action=search";
        JSONObject paramnew = new JSONObject();
        paramnew.put("value", s);
        paramnew.put("MasterEntryName", "RouteStoppage");

        Log.e("url", url);
        Log.e("paramnew", paramnew.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,  // // TODO: 2/20/2017 for search change to post previous get method used
              /*  "http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*//*,"MasterEntryValue":"mp magr"*/

               /* "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s.replace(" ","+")+"\"}}",*/
                // Old with get method


                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&action=search",  search only in single table ie. master entry

                // TODO: 2/28/2017   changes according to sir
               /* "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&action=stopageMasterEntry",*/

                /*"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&action=search"*/
                url, paramnew
                ,


                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        Log.d("getStopDataFromServer", jsonObject.toString());

                        String data = jsonObject.optString("code", "key not found");
                        if ("200".equalsIgnoreCase(data)) {

                            JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                            int jsonArray_result_length;
                            if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                adapter_suggest_dropdown.clear();
                                stopList.clear();
                                stopDataItemList.clear();

                                Gson gson = new Gson();
                                String jsonOutput = jsonArray_result.toString();
                                Type listType = new TypeToken<List<StopDataItem>>() {
                                }.getType();
                                //stopDataItemList.add(0,new StopDataItem());
                                //  ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                // LocationMapsActivity.this.stopDataItemList.addAll(stopDataItemList);

                                // stopDataAdapter.notifyDataSetChanged();

                                stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);

                             /*   for (StopDataItem stopDataItem : stopDataItemList) {
                                    adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                    stopList.add(stopDataItem.getMasterEntryValue());
                                    //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                }
*/

                                //adapter_suggest_dropdown.notifyDataSetChanged();
                                adapter_suggest_dropdown.getFilter().filter(tet_edit_address.getText(), tet_edit_address);

                                //stopDataItemList.add(0,new StopDataItem());
                                // stopDataAdapter.insertAll(stopDataItemList);
                                // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                            }

                        }


                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        // AppController.getInstance().addToRequestQueue(strReq, "getStopDataFromServer");
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "getStopDataFromServer");


    }

    private void getVehicleDataFromServer(final String s) throws JSONException {
        pb.setVisibility(View.VISIBLE);

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&action=search";
        JSONObject params = new JSONObject();
        params.put("value", s);
        Log.e("url", url);
        Log.e("params", params.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,  // // TODO: 2/20/2017 for search change to post previous get method used
              /*  "http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*//*,"MasterEntryValue":"mp magr"*/

               /* "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s.replace(" ","+")+"\"}}",*/
                // Old with get method

                url, params
                // "http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217&action=search"
                ,

                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        Log.d("getVehicleFromServer", jsonObject.toString());


                        String data = jsonObject.optString("code", "key not found");
                        if ("200".equalsIgnoreCase(data)) {

                            JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                            int jsonArray_result_length;
                            if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                adapter_suggest_dropdown.clear();
                                vehicleList.clear();
                                vehicleDataItemList.clear();

                                Gson gson = new Gson();
                                String jsonOutput = jsonArray_result.toString();
                                Type listType = new TypeToken<List<VehicleDataItem>>() {
                                }.getType();
                                //stopDataItemList.add(0,new StopDataItem());
                                ArrayList<VehicleDataItem> vehicleDataItemList = (ArrayList<VehicleDataItem>) gson.fromJson(jsonOutput, listType);
                                LocationMapsActivity.this.vehicleDataItemList.addAll(vehicleDataItemList);
                                // stopDataAdapter.notifyDataSetChanged();

                                for (VehicleDataItem vehicleDataItem : vehicleDataItemList) {
                                    adapter_suggest_dropdown.add(vehicleDataItem.getVehicleName() + ", " + vehicleDataItem.getVehicleNumber());
                                    vehicleList.add(vehicleDataItem.getVehicleId());
                                    //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                }


                                //adapter_suggest_dropdown.notifyDataSetChanged();
                                adapter_suggest_dropdown.getFilter().filter(tet_edit_vehicle_name.getText(), tet_edit_vehicle_name);

                                //stopDataItemList.add(0,new StopDataItem());
                                // stopDataAdapter.insertAll(stopDataItemList);
                                // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                            }

                        }


                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        });
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
/*        AppController.getInstance().addToRequestQueue(strReq, "getVehicleDataFromServer");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "getVehicleDataFromServer");
        //queue.add(strReq);


    }

    private void putRouteDataToServer(final VehicleRouteDataItem vehicleRouteData) {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();

        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleRouteApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&data=" +
                "{\"data\":{\"VehicleRouteName\":\"" + vehicleRouteData.getVehicleRouteName()
                .replace(" ", "+") + "\",\"VehicleId\":\"" + vehicleRouteData.getVehicleId()
                + "\",\"Route\":\"" + vehicleRouteData.getRoute() + "\",\"RouteTo\":\"" +
                vehicleRouteData.getRouteTo() + "\"},\"filter\":{\"VehicleRouteId\":\"" +
                vehicleRouteData.getVehicleRouteId() + "\"}}";

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.PUT,

                /*"http://192.168.1.161/apischoolerp/VehicleRouteApi.php?databaseName=CBA98217&data=" +
                        "{\"data\":{\"VehicleRouteName\":\"" + vehicleRouteData.getVehicleRouteName()
                        .replace(" ", "+") + "\",\"VehicleId\":\"" + vehicleRouteData.getVehicleId()
                        + "\",\"Route\":\"" + vehicleRouteData.getRoute() + "\",\"RouteTo\":\"" +
                        vehicleRouteData.getRouteTo() + "\"},\"filter\":{\"VehicleRouteId\":\"" +
                        vehicleRouteData.getVehicleRouteId() + "\"}}"*/
                url
                ,
                //   "&data={\"data\":{\"VehicleNumber\":\""+vehicleDataItem.getVehicleNumber().replace(" ","+")+"\",\"VehicleName\": \""+vehicleDataItem.getVehicleName().replace(" ","+")+"\"},\"filter\":{\"VehicleId\":\""+vehicleDataItem.getVehicleId()+"\"}}",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("putRouteDataToServer", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                /*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*/
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LocationMapsActivity.this);
                                alertDialog.setMessage("Updated Successfully");
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if (!LocationMapsActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {


                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //AppController.getInstance().addToRequestQueue(strReq, "putRouteDataToServer");
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "putRouteDataToServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }

    private void postRouteDataToServer(final VehicleRouteDataItem vehicleRouteData) throws JSONException {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleRouteApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this);
        JSONObject param = new JSONObject(new Gson().toJson(vehicleRouteData));
        Log.e("url", url);
        Log.e("param", param.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,

          /*      "http://192.168.1.161/apischoolerp/VehicleRouteApi.php?databaseName=CBA98217"*/
                url,param
                ,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        Log.d("postRouteDataToServer", jsonObject.toString());

                            String data = jsonObject.optString("code", "key not found");
                            if ("201".equalsIgnoreCase(data)) {

                                /*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*/
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LocationMapsActivity.this);
                                alertDialog.setMessage("Saved Successfully");
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if (!LocationMapsActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }


                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        })  ;
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        // AppController.getInstance().addToRequestQueue(strReq, "postRouteDataToServer");
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "postRouteDataToServer");
        //queue.add(strReq);


    }

    private void get_vehicleRouteApi() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Getting route name...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "VehicleRouteApi.php?databaseName="
                + Gc.getSharedPreference(Gc.ERPDBNAME,this);

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.GET,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
         /*       "http://192.168.1.161/apischoolerp/VehicleRouteApi.php?databaseName=CBA98217"*/
                url
                ,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("get_vehicleRouteApi", response.toString());


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;


                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    vehicleRouteDataItemList.clear();

                                    vehicleRouteDataItemList.add(0, new VehicleRouteDataItem());

                                    route_list.clear();
                                    route_list.add("Select");

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<VehicleRouteDataItem>>() {
                                    }.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<VehicleRouteDataItem> vehicleRouteDataItemList = (ArrayList<VehicleRouteDataItem>) gson.fromJson(jsonOutput, listType);

                                    LocationMapsActivity.this.vehicleRouteDataItemList.addAll(vehicleRouteDataItemList);
                                    for (VehicleRouteDataItem vehicleRouteData : vehicleRouteDataItemList) {
                                        // Toast.makeText(LocationMapsActivity.this, ""+vehicleRouteData.getVehicleRouteName(), Toast.LENGTH_SHORT).show();
                                        route_list.add(vehicleRouteData.getVehicleRouteName());
                                    }
                                    adapterProject.notifyDataSetChanged();


                                    //stopDataItemList.add(0,new StopDataItem());
                                    // stopDataAdapter.insertAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                    //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                                }
                                adapterProject.notifyDataSetChanged();

                            } else if ("401".equalsIgnoreCase(data)) {
                                vehicleRouteDataItemList.clear();
                                vehicleRouteDataItemList.add(0, new VehicleRouteDataItem());
                                route_list.clear();
                                route_list.add("Select");
                                spn_select_route.setSelection(0);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pDialog.dismiss();

                        }
                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        )

        {

            // TODO: 2/6/2017 get k case me getParams call nai hota
          /*  @Override
            protected Map<String, String> getParams() {
                JSONObject jsonObject_masterentry = new JSONObject();
                Map<String, String> param = new HashMap<>();
                param.put("databaseName","A994A107");
                try {
                    jsonObject_masterentry.put("MasterEntryName", "RouteTo");
                    JSONObject jsonObject_filter = new JSONObject();
                    jsonObject_filter.put("filter", jsonObject_masterentry);
                    param.put("data", jsonObject_filter.toString());
                    Log.e("qwerty",jsonObject_filter.toString());
                    Log.e("qwerty",param.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return param;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
        //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
/*        AppController.getInstance().addToRequestQueue(strReq, "get_vehicleRouteApi");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "get_vehicleRouteApi");

    }

    private void get_routeStoppageApi(final String route) throws JSONException {
        if (route == null || route.isEmpty())
            return;
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
        pDialog.setCancelable(false);
        pDialog.show();

        String url =Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "RouteStoppagesApi.php?databaseName=" + Gc.getSharedPreference(Gc.ERPDBNAME,this)
                + "&action=stopageMasterEntryById";
        JSONObject jsonObject_masterentry = new JSONObject();
        jsonObject_masterentry.put("MasterEntryId", route);

        Log.e("url", url);
        Log.e("param", jsonObject_masterentry.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
                //todo routestoppage in url
                url,jsonObject_masterentry
                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",
                /*"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&action=stopageMasterEntryById"*/,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("get_routeStoppageApi", jsonObject.toString());

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    stopDataItemList_route.clear();

                                    //     stopDataItemList_route.add(new StopDataItem());

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>() {
                                    }.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    LocationMapsActivity.this.stopDataItemList_route.addAll(stopDataItemList);

                                    try {
                                        List<LatLng> midpoints = null;
                                        address_waypoint_list.clear();
                                        time_waypoint_list.clear();
                                        stopDataItemList_stop.clear();
                                        if (stopDataItemList_route.size() > 2)
                                            midpoints = new ArrayList<>(stopDataItemList_route.size() - 2);
                                        for (int i = 0; stopDataItemList_route.size() > i; i++) {
                                            StopDataItem stopDataItem = stopDataItemList_route.get(i);

                                            if (i == 0) {
                                                Log.e("Latlong", "Lat = " + stopDataItem.getStopLat() + " Long = " + stopDataItem.getStopLong());
                                                latlong_start = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                                                // LocationMapsActivity.this.start_title = stopDataItem.getMasterEntryValue();
                                                LocationMapsActivity.this.start_snippet = stopDataItem.getRouteStoppageName();
                                                LocationMapsActivity.this.start_title = stopDataItem.getTime();
                                                start_stop = stopDataItem;

                                            } else if (i == 1) {
                                                latlong_destination = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                                                //  LocationMapsActivity.this.destination_title = stopDataItem.getMasterEntryValue();
                                                LocationMapsActivity.this.destination_snippet = stopDataItem.getRouteStoppageName();
                                                LocationMapsActivity.this.destination_title = stopDataItem.getTime();

                                                destination_stop = stopDataItem;
                                            } else {
                                                midpoints.add(new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong())));
                                                address_waypoint_list.add(stopDataItem.getRouteStoppageName());
                                                //  time_waypoint_list.add(stopDataItem.getMasterEntryValue());
                                                time_waypoint_list.add(stopDataItem.getTime());

                                                stopDataItemList_stop.add(stopDataItem);

                                            }

                                        }
                                        pDialog.dismiss();
                                        createRouteByParam(latlong_start, latlong_destination, midpoints, start_title, start_snippet, destination_title, destination_snippet);
                                        latlong_waypoint_list.clear();
                                        if (midpoints != null)
                                            latlong_waypoint_list.addAll(midpoints);
                                        //     getDirection(route_url);
                                        getDirection("default");
                                    } catch (Exception e) {
                                        Toast.makeText(LocationMapsActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                    }

                                }pDialog.cancel();
                                //   stopDataAdapter.notifyDataSetChanged();
                            }



                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(LocationMapsActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        )

        ;
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
        //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
/*        AppController.getInstance().addToRequestQueue(strReq, "get_routeStoppageApi");*/
        AppRequestQueueController.getInstance(LocationMapsActivity.this).addToRequestQueue(strReq, "get_routeStoppageApi");

    }


    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();  //  call on onStart()   GoogleApiClient is not connected yet
        locationRequest = LocationRequest.create();
        // locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // locationRequest.setInterval(30 * 1000 * 4);
        // locationRequest.setFastestInterval(30000);  // 30 sec
        // locationRequest.setSmallestDisplacement(DISPLACEMENT);


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);*/
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                getCurrentLocationLatLong();
                //  requestLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(LocationMapsActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    private void requestLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED /*&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED*/) {
            // TODO: Consider calling
            Toast.makeText(this, "Please enable location services", Toast.LENGTH_SHORT).show();
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        /*Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);*/
      /*  if (mLastLocation != null) {
            Toast.makeText(TaxiActivity.this, "LastLocation - \n" + String.valueOf(mLastLocation.getLatitude())
                    + "\n" + String.valueOf(mLastLocation.getLongitude()), Toast.LENGTH_LONG).show();
        }
*/
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new PendingIntent());


    }

}
