package in.junctiontech.school.tranport.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.tranport.activity.AddStopsActivity;
import in.junctiontech.school.tranport.model.stopdata.StopDataItem;
import tyrantgit.explosionfield.ExplosionField;


/**
 * Created by JAYDEVI BHADE on 2/8/2017.
 */

public class StopDataAdapter extends RecyclerView.Adapter<StopDataAdapter.ViewHolder> {

    private Context context;
    private List<StopDataItem> stopDataItemList;
    private int mPreviousPosition = 0;
    private ExplosionField mExplosionField;
    private SharedPreferences sp;

    public StopDataAdapter(Context context, List<StopDataItem> stopDataItemList) {
        this.stopDataItemList = stopDataItemList;
        this.context = context;
        mExplosionField = ExplosionField.attach2Window((Activity) context);
        Log.e("explosion", "explode");
        sp = Prefs.with(context).getSharedPreferences();
    }

    public void setStopDataItemList(List<StopDataItem> stopDataItemList) {
        this.stopDataItemList = stopDataItemList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_stop, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        StopDataItem stopDataItem = stopDataItemList.get(position);

       // if (stopDataItem.getMasterEntryId() != null) {
            holder.civ_stop.setImageResource(R.drawable.ic_stop);
            holder.ll_add_stop.setVisibility(View.INVISIBLE);
            holder.ll_exist_stop.setVisibility(View.VISIBLE);
            holder.tv_stop_name.setText(stopDataItem.getRouteStoppageName());
            holder.tv_lat.setText(stopDataItem.getStopLat());
            holder.tv_long.setText(stopDataItem.getStopLong());
      /*  } else {
            holder.civ_stop.setImageResource(R.drawable.ic_add);
            holder.ll_add_stop.setVisibility(View.VISIBLE);
            holder.ll_exist_stop.setVisibility(View.INVISIBLE);
        }*/


        //    holder.tv_name.setText(holidayDataItem.getHoliday_title());


       /* Glide.with(context)
                .load("http://www.searchb4kharch.com/frontend/images/AppIconDesign3.png")
                .placeholder(R.drawable.expensesdashboard)
                //.error(R.drawable.oops)
                .crossFade(1000)
                .override(500, 500)
                .thumbnail(0.5f)
                .centerCrop()// good for profile image
                .into(holder.civ_vehicle);*/

      /*  if (position > mPreviousPosition) {
            Animate.animateSunblind(holder, true);  //  for View animation
            //            AnimationUtils.animateSunblind(holder, true);
            //  AnimationUtils.animate1(holder, true);
            //     AnimationUtils.animate(holder, true);
            //   AnimationUtils.animateScatter(holder, true);
            Animate.scaleXYV(holder.civ_stop);
        } else {
            Animate.animateSunblind(holder, false);  //  for View animation
            //            AnimationUtils.animateSunblind(holder, true);
            //  AnimationUtils.animate1(holder, true);
            //     AnimationUtils.animate(holder, true);
            //   AnimationUtils.animateScatter(holder, true);
            Animate.scaleXYV(holder.civ_stop);
        }
        mPreviousPosition = position;
*/

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return stopDataItemList.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, StopDataItem stopDataItem) {
        stopDataItemList.add(position, stopDataItem);
        notifyItemInserted(position);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insertAll(List stopDataItemList) {
        this.stopDataItemList.clear();
        this.stopDataItemList.addAll(stopDataItemList);
        notifyDataSetChanged();
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(StopDataItem stopDataItem) {
        int position = stopDataItemList.indexOf(stopDataItem);
        stopDataItemList.remove(position);
        notifyItemRemoved(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(int position) {
        // Log.e("List Length", position + " - " + claimDataItemList.size());
        stopDataItemList.remove(position);
        notifyItemRemoved(position);
    }

    public void updateList(ArrayList<StopDataItem> stopDataItemList) {
        this.stopDataItemList.clear();
        this.stopDataItemList   = stopDataItemList;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView tv_stop_name, tv_lat, tv_long;
        private CircleImageView civ_stop;
        private ImageView btn_stop_edit, btn_stop_delete;
        private LinearLayout ll_add_stop;
        private LinearLayout ll_exist_stop;


        public ViewHolder(View v) {
            super(v);
            tv_stop_name = (TextView) v.findViewById(R.id.item_stop_tv_stop_name);
            tv_lat = (TextView) v.findViewById(R.id.item_stop_tv_lat);
            tv_long = (TextView) v.findViewById(R.id.item_stop_tv_long);

            civ_stop = (CircleImageView) v.findViewById(R.id.item_stop_civ_stop);
            btn_stop_edit = (ImageView) v.findViewById(R.id.item_stop_btn_stop_edit);
            btn_stop_delete = (ImageView) v.findViewById(R.id.item_stop_btn_stop_delete);

            ll_add_stop = (LinearLayout) v.findViewById(R.id.item_stop_ll_add_stop);
            ll_exist_stop = (LinearLayout) v.findViewById(R.id.item_stop_ll_exist_stop);


            btn_stop_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int position = getLayoutPosition();
                    int adapposition = getAdapterPosition();
                    Log.e("List Length", position + " -- " + adapposition + " --- " + stopDataItemList.size());
                    StopDataItem stopDataItem = stopDataItemList.get(position);

                        context.startActivity(new Intent(context, AddStopsActivity.class)
                                .putExtra("stop_name", stopDataItem.getRouteStoppageName())
                                .putExtra("stop_id", stopDataItem.getRouteStoppageId())
                                .putExtra("stop_lat", stopDataItem.getStopLat())
                                .putExtra("stop_long", stopDataItem.getStopLong())
                        );


                   /* if (position > -1 && position < vehicleDataItemList.size()) {
                        ViewParent v1 = v.getParent();
                        remove(position);               // may be it works by interchanging two lines first remove then explode
                        mExplosionField.explode(v);

                    } else
                        Log.e("List Length", position + " --- " + vehicleDataItemList.size());*/
                }
            });

            btn_stop_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final int position = getLayoutPosition();
                    int adapposition = getAdapterPosition();
                    Log.e("List Length", position + " -- " + adapposition + " --- " + stopDataItemList.size());

                    if (position > -1 && position < stopDataItemList.size()) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        // alertDialogBuilder.setTitle("Confirmation");
                        alertDialogBuilder.setMessage("Are you sure, you want to delete?");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                deleteStopDataFromServer(stopDataItemList.get(position), v, position);
                                //   ViewParent v1 = v.getParent();
                                //  remove(position);               // may be it works by interchanging two lines first remove then explode
                                //   mExplosionField.explode(v);


                            }
                        });
                        alertDialogBuilder.setNegativeButton("CANCEL", null);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                    } else
                        Log.e("List Length", position + " --- " + stopDataItemList.size());
                }
            });




            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();
                    in.junctiontech.searchb4kharch.pojo.navigationcategory.CategoryNameItem
                            categoryNameItem = categoryNameItemList.get(position);
                    //  Toast.makeText(context,categoryNameItem.getCategoryName(),Toast.LENGTH_LONG).show();

                    if (categoryNameItem.getLevel().equalsIgnoreCase("true")) {
                        context.startActivity(new Intent(context, ProductCategoryActivity.class)
                                .putExtra("categoryName", categoryNameItem.getCategoryName())
                                .putExtra("categoryKey", categoryNameItem.getCategoryKey()));
                    } else if (categoryNameItem.getLevel().equalsIgnoreCase("false")) {
                        context.startActivity(new Intent(context, ProductSearchActivity.class)
                                .putExtra("categoryName", categoryNameItem.getCategoryName())
                                .putExtra("categoryKey", categoryNameItem.getCategoryKey()));
                    }

                    ((Activity) context).overridePendingTransition(R.anim.enter, R.anim.exit);

                   *//* ExplosionField mExplosionField = ExplosionField.attach2Window((Activity)context);
                  *//*          mExplosionField.explode(v);
*//**//*


                    int position = getLayoutPosition();
                    HolidayDataItem productdataItem = productdataItemList.get(position);
                    context.startActivity(new Intent(context, ProductDetailActivity.class).
                            putExtra("can_compare", productdataItem.isCan_compare() + "")
                            .putExtra("product_url", productdataItem.getProduct_link())
                            .putExtra("product_id", productdataItem.getProduct_id())
                            .putExtra("product_sub_cate", productdataItem.getProduct_sub_cate()));
                    ((Activity) context).overridePendingTransition(R.anim.enter, R.anim.exit);*//*


                }
            });*/
        }


    }


    private void deleteStopDataFromServer(final StopDataItem stopDataItem, final View v, final int position) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "RouteStoppagesApi.php?databaseName=" + sp.getString("organization_name", "")
                + "&data=" +
                "{\"filter\":{\"routeStoppageId\":\"" + stopDataItem.getRouteStoppageId() + "\"}}";

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.DELETE,

                url
          /*    "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?" +
                        "databaseName=CBA98217&data=" +
                      "{\"filter\":{\"MasterEntryId\":\""+stopDataItem.getMasterEntryId()+"\"}}"
                */
                ,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("deleteStopDataFrom", response);
                      /*  Log.d("URL", "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?" +
                                        "databaseName=CBA98217&data=" +
                                "{\"filter\":{\"MasterEntryId\":\""+stopDataItem.getMasterEntryId()+"\"}}");
                      */
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                remove(position);
                                mExplosionField.explode(v);
                                Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(context, "Try again later !!!", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(context, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppRequestQueueController.getInstance(context).addToRequestQueue(strReq, "deleteStopDataFromServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }


}
