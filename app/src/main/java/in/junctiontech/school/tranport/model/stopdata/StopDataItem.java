package in.junctiontech.school.tranport.model.stopdata;

import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 2/8/2017.
 */

public class StopDataItem {
    /*private String MasterEntryId;
    private String MasterEntryStatus;
    private String MasterEntryName;
    private String MasterEntryValue;
    private String Grade;
    private String GradeRange;
    private String StopLat;
    private String StopLong;
    private String Time;*/
String routeStoppageId,routeStoppageName,stopLat,stopLong;
String RouteID,StoppageID,Time,Type,VehicleRouteName;

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getStoppageID() {
        return StoppageID;
    }

    public void setStoppageID(String stoppageID) {
        StoppageID = stoppageID;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getVehicleRouteName() {
        return VehicleRouteName;
    }

    public void setVehicleRouteName(String vehicleRouteName) {
        VehicleRouteName = vehicleRouteName;
    }

    public String getRouteStoppageId() {
        return routeStoppageId;
    }

    public void setRouteStoppageId(String routeStoppageId) {
        this.routeStoppageId = routeStoppageId;
    }

    public String getRouteStoppageName() {
        return routeStoppageName;
    }

    public void setRouteStoppageName(String routeStoppageName) {
        this.routeStoppageName = routeStoppageName;
    }

    public String getStopLat() {
        return stopLat;
    }

    public void setStopLat(String stopLat) {
        this.stopLat = stopLat;
    }

    public String getStopLong() {
        return stopLong;
    }

    public void setStopLong(String stopLong) {
        this.stopLong = stopLong;
    }
    /*      Constructor     */

  /*  public StopDataItem(String masterEntryId, String masterEntryStatus, String masterEntryName, String masterEntryValue, String grade, String gradeRange, String stopLat, String stopLong, String stopTime) {
        MasterEntryId = masterEntryId;
        MasterEntryStatus = masterEntryStatus;
        MasterEntryName = masterEntryName;
        MasterEntryValue = masterEntryValue;
        Grade = grade;
        GradeRange = gradeRange;
        StopLat = stopLat;
        StopLong = stopLong;
        Time = stopTime;
    }

    public StopDataItem(String masterEntryId, String masterEntryStatus, String masterEntryName, String masterEntryValue, String grade, String gradeRange, String stopLat, String stopLong) {
        MasterEntryId = masterEntryId;
        MasterEntryStatus = masterEntryStatus;
        MasterEntryName = masterEntryName;
        MasterEntryValue = masterEntryValue;
        Grade = grade;
        GradeRange = gradeRange;
        StopLat = stopLat;
        StopLong = stopLong;
    }

    public StopDataItem(String masterEntryId, String masterEntryStatus, String masterEntryName, String masterEntryValue, String grade, String gradeRange) {
        MasterEntryId = masterEntryId;
        MasterEntryStatus = masterEntryStatus;
        MasterEntryName = masterEntryName;
        MasterEntryValue = masterEntryValue;
        Grade = grade;
        GradeRange = gradeRange;
    }

    public StopDataItem() {

    }


    *//*      Getter      *//*

    public String getStopTime() {
        return Time;
    }

    public String getStopLat() {
        return StopLat;
    }

    public String getStopLong() {
        return StopLong;
    }

    public String getMasterEntryId() {
        return MasterEntryId;
    }

    public String getMasterEntryStatus() {
        return MasterEntryStatus;
    }

    public String getMasterEntryName() {
        return MasterEntryName;
    }

    public String getMasterEntryValue() {
        return MasterEntryValue;
    }

    public String getGrade() {
        return Grade;
    }

    public String getGradeRange() {
        return GradeRange;
    }



    public void setStopTime(String stopTime) {
        Time = stopTime;
    }

    public void setStopLat(String stopLat) {
        StopLat = stopLat;
    }

    public void setStopLong(String stopLong) {
        StopLong = stopLong;
    }

    public void setMasterEntryId(String masterEntryId) {
        MasterEntryId = masterEntryId;
    }

    public void setMasterEntryStatus(String masterEntryStatus) {
        MasterEntryStatus = masterEntryStatus;
    }

    public void setMasterEntryName(String masterEntryName) {
        MasterEntryName = masterEntryName;
    }

    public void setMasterEntryValue(String masterEntryValue) {
        MasterEntryValue = masterEntryValue;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public void setGradeRange(String gradeRange) {
        GradeRange = gradeRange;
    }*/
}
