package in.junctiontech.school.tranport.model.vehicleroutedata;

/**
 * Created by JAYDEVI BHADE on 2/20/2017.
 */

public class VehicleRouteDataItem {

    private String VehicleRouteId;
    private String Session;
    private String VehicleRouteStatus;
    private String VehicleRouteName;
    private String VehicleId;
    private String Route;
    private String RouteTo;
    private String VehicleRouteRemarks;
    private String DOE;
    private String DOL;

    /*      Constructor     */

    public VehicleRouteDataItem(String vehicleRouteId, String session, String vehicleRouteStatus, String vehicleRouteName, String vehicleId, String route, String routeTo, String vehicleRouteRemarks, String DOE, String DOL) {
        VehicleRouteId = vehicleRouteId;
        Session = session;
        VehicleRouteStatus = vehicleRouteStatus;
        VehicleRouteName = vehicleRouteName;
        VehicleId = vehicleId;
        Route = route;
        RouteTo = routeTo;
        VehicleRouteRemarks = vehicleRouteRemarks;
        this.DOE = DOE;
        this.DOL = DOL;
    }

    public VehicleRouteDataItem() {

    }

    public VehicleRouteDataItem(String vehicleRouteStatus, String vehicleRouteName, String vehicleId, String route, String routeTo) {
        VehicleRouteStatus = vehicleRouteStatus;
        VehicleRouteName = vehicleRouteName;
        VehicleId = vehicleId;
        Route = route;
        RouteTo = routeTo;
    }

    /*          Getter      */

    public String getVehicleRouteId() {
        return VehicleRouteId;
    }

    public String getSession() {
        return Session;
    }

    public String getVehicleRouteStatus() {
        return VehicleRouteStatus;
    }

    public String getVehicleRouteName() {
        return VehicleRouteName;
    }

    public String getVehicleId() {
        return VehicleId;
    }

    public String getRoute() {
        return Route;
    }

    public String getRouteTo() {
        return RouteTo;
    }

    public String getVehicleRouteRemarks() {
        return VehicleRouteRemarks;
    }

    public String getDOE() {
        return DOE;
    }

    public String getDOL() {
        return DOL;
    }


    /*      Setter      */

    public void setVehicleRouteId(String vehicleRouteId) {
        VehicleRouteId = vehicleRouteId;
    }

    public void setSession(String session) {
        Session = session;
    }

    public void setVehicleRouteStatus(String vehicleRouteStatus) {
        VehicleRouteStatus = vehicleRouteStatus;
    }

    public void setVehicleRouteName(String vehicleRouteName) {
        VehicleRouteName = vehicleRouteName;
    }

    public void setVehicleId(String vehicleId) {
        VehicleId = vehicleId;
    }

    public void setRoute(String route) {
        Route = route;
    }

    public void setRouteTo(String routeTo) {
        RouteTo = routeTo;
    }

    public void setVehicleRouteRemarks(String vehicleRouteRemarks) {
        VehicleRouteRemarks = vehicleRouteRemarks;
    }

    public void setDOE(String DOE) {
        this.DOE = DOE;
    }

    public void setDOL(String DOL) {
        this.DOL = DOL;
    }
}
