package in.junctiontech.school.tranport.model.vehicledata;

/**
 * Created by JAYDEVI BHADE on 2/7/2017.
 */

public class VehicleDataItem {
    private String VehicleId;
    private String VehicleStatus;
    private String VehicleNumber;
    private String VehicleName;
    private String DOE;
    private String DOL;

    /*   Constructor   */

    public VehicleDataItem(String vehicleId, String vehicleStatus, String vehicleNumber, String vehicleName, String DOE, String DOL) {
        VehicleId = vehicleId;
        VehicleStatus = vehicleStatus;
        VehicleNumber = vehicleNumber;
        VehicleName = vehicleName;
        this.DOE = DOE;
        this.DOL = DOL;
    }

    public VehicleDataItem() {

    }

    /*     Getter   */

    public String getVehicleId() {
        return VehicleId;
    }

    public String getVehicleStatus() {
        return VehicleStatus;
    }

    public String getVehicleNumber() {
        return VehicleNumber;
    }

    public String getVehicleName() {
        return VehicleName;
    }

    public String getDOE() {
        return DOE;
    }

    public String getDOL() {
        return DOL;
    }

    /*      Setter      */

    public void setVehicleId(String vehicleId) {
        VehicleId = vehicleId;
    }

    public void setVehicleStatus(String vehicleStatus) {
        VehicleStatus = vehicleStatus;
    }

    public void setVehicleNumber(String vehicleNumber) {
        VehicleNumber = vehicleNumber;
    }

    public void setVehicleName(String vehicleName) {
        VehicleName = vehicleName;
    }

    public void setDOE(String DOE) {
        this.DOE = DOE;
    }

    public void setDOL(String DOL) {
        this.DOL = DOL;
    }
}
