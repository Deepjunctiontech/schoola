package in.junctiontech.school.tranport.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by LENEVO on 07-09-2017.
 */

public class AssignStopData implements Serializable {
    String RouteID,StoppageID,Time,Type,routeStoppageName,stopLat,stopLong,VehicleRouteName;
    ArrayList<AssignStopData>result;

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getStoppageID() {
        return StoppageID;
    }

    public void setStoppageID(String stoppageID) {
        StoppageID = stoppageID;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getRouteStoppageName() {
        return routeStoppageName;
    }

    public void setRouteStoppageName(String routeStoppageName) {
        this.routeStoppageName = routeStoppageName;
    }

    public String getStopLat() {
        return stopLat;
    }

    public void setStopLat(String stopLat) {
        this.stopLat = stopLat;
    }

    public String getStopLong() {
        return stopLong;
    }

    public void setStopLong(String stopLong) {
        this.stopLong = stopLong;
    }

    public String getVehicleRouteName() {
        return VehicleRouteName;
    }

    public void setVehicleRouteName(String vehicleRouteName) {
        VehicleRouteName = vehicleRouteName;
    }

    public ArrayList<AssignStopData> getResult() {
        return result;
    }

    public void setResult(ArrayList<AssignStopData> result) {
        this.result = result;
    }
}
