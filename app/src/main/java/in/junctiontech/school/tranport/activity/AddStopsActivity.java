package in.junctiontech.school.tranport.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.tranport.model.stopdata.StopDataItem;


public class AddStopsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private List<String> stopList = new ArrayList<>();
    private List<StopDataItem> stopDataItemList = new ArrayList<>();
    private ArrayAdapter adapter_suggest_dropdown;
    private AutoCompleteTextView et_name;
    private ProgressBar pb;
    private String stop_id;
    private FloatingActionButton btn_add_stop;
    private GoogleMap mMap;
    private String stop_lat;
    private String stop_long;
    private Marker st;
    private SharedPreferences sp;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private LocationManager lm;
    private ProgressDialog loading;

    private int colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((AppBarLayout)findViewById(R.id.app_bar_layout)).setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stops);
        sp = Prefs.with(this).getSharedPreferences();
        actionBarEdit();
        setColorApp();
        initViews();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);*/
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }


    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();  //  call on onStart()   GoogleApiClient is not connected yet
        locationRequest = LocationRequest.create();

        // locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // locationRequest.setInterval(30 * 1000 * 4);
        // locationRequest.setFastestInterval(30000);  // 30 sec
        // locationRequest.setSmallestDisplacement(DISPLACEMENT);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                getCurrentLocationLatLong();
                //  requestLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(AddStopsActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    private void requestLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED /*&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED*/) {
            // TODO: Consider calling
            Toast.makeText(this, "Please enable location services", Toast.LENGTH_SHORT).show();
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        /*Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);*/
      /*  if (mLastLocation != null) {
            Toast.makeText(TaxiActivity.this, "LastLocation - \n" + String.valueOf(mLastLocation.getLatitude())
                    + "\n" + String.valueOf(mLastLocation.getLongitude()), Toast.LENGTH_LONG).show();
        }
*/
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new PendingIntent());


    }

    public void getCurrentLocationLatLong() {
        loading = ProgressDialog.show(this, getString(R.string.getting_current_location), getString(R.string.please_wait), false, true);
        lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (lm != null) {
            //    Toast.makeText(this, "call", Toast.LENGTH_SHORT).show();
            //   default_current_lat_long = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            //   Toast.makeText(this, "call "+default_current_lat_long, Toast.LENGTH_SHORT).show();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "Location Services Disabled !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        lm.requestLocationUpdates(
                LocationManager.PASSIVE_PROVIDER,
                0,
                0, this);
        Log.d("Network", "Network");
        return;
    }

  /*  private void test() {

        final AutoCompleteTextView countrySearch = (AutoCompleteTextView) findViewById(R.id.activity_add_stop_auet_name);
        final AutoCompleteAdapter adapter = new AutoCompleteAdapter(this,android.R.layout.simple_dropdown_item_1line);
        countrySearch.setAdapter(adapter);

        //when autocomplete is clicked
        countrySearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String countryName = adapter.getItem(position).getName();
                countrySearch.setText(countryName);
            }
        });
    }*/


    public void onClickAddStop(View view) {


        String current_stop_name = et_name.getText().toString();
        if (current_stop_name.isEmpty()) {
            et_name.requestFocus();
            et_name.setError(getString(R.string.field_can_not_be_blank));
            if (Build.VERSION.SDK_INT >= 24) {
                et_name.setError(Html.fromHtml("<font color='red'>"+getString(R.string.field_can_not_be_blank)+ "</font>", Html.FROM_HTML_MODE_LEGACY));

            } else {
                et_name.setError(Html.fromHtml("<font color='red'>"+getString(R.string.field_can_not_be_blank)+"</font>"));
            }

            return;
        }
        et_name.setError(null);

        if (stop_id != null) {
            // Toast.makeText(this, "Update", Toast.LENGTH_SHORT).show();
            StopDataItem stopDataItem = new StopDataItem();
            stopDataItem.setRouteStoppageId(stop_id);
            stopDataItem.setStopLat(stop_lat);
            stopDataItem.setStopLong(stop_long);
            stopDataItem.setRouteStoppageName(current_stop_name);
            putStopDataToServer(stopDataItem);
        } else {
            // Toast.makeText(this, "New", Toast.LENGTH_SHORT).show();
            StopDataItem stopDataItem = new StopDataItem();
            //stopDataItem.setMasterEntryStatus("Active");
          //  stopDataItem.setMasterEntryName("RouteStoppage");
            stopDataItem.setRouteStoppageName(current_stop_name);
            //stopDataItem.setGrade("");
            //stopDataItem.setGradeRange("");
            stopDataItem.setStopLat(stop_lat);
            stopDataItem.setStopLong(stop_long);
            try {
                postStopDataToServer(stopDataItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initViews() {

        Intent currentIntent = this.getIntent();
        et_name = (AutoCompleteTextView) findViewById(R.id.activity_add_stop_auet_name);
        btn_add_stop = (FloatingActionButton) findViewById(R.id.activity_add_stop_btn_add_stop);
        btn_add_stop.setBackgroundTintList(ColorStateList.valueOf(colorIs));

        pb = (ProgressBar) findViewById(R.id.activity_add_stops_pb);


        adapter_suggest_dropdown = new ArrayAdapter(this, R.layout.myspinner_dropdown_item/*, stopList*/);
        et_name.setThreshold(3);
        et_name.setAdapter(adapter_suggest_dropdown);


        if (currentIntent != null) {
            String stop_name = currentIntent.getStringExtra("stop_name");

            if (stop_name != null) {

                et_name.setText(stop_name);
                et_name.setSelection(et_name.getText().length());
            } else
                buildGoogleApiClient();

            stop_id = currentIntent.getStringExtra("stop_id");
            stop_lat = currentIntent.getStringExtra("stop_lat");
            stop_long = currentIntent.getStringExtra("stop_long");


        }

        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                   /* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();*/
                    //  AppController.getInstance().cancelPendingRequests("getStopDataFromServer");
                    AppRequestQueueController.getInstance(AddStopsActivity.this).cancelRequestByTag("getStopDataFromServer");
                    getStopDataFromServer(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_add_stop_map);
        mapFragment.getMapAsync(this);
    }


    private void getStopDataFromServer(final String s) {
        pb.setVisibility(View.VISIBLE);
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "RouteStoppagesApi.php?databaseName=" + sp.getString("organization_name", "")
                + "&data=" +
                "{\"filter\":{ \"routeStoppageName\":\""
                + s.replace(" ", "+") + "\"}}";

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.GET,
                /*"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s+"\"}}",*/

                /*"http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s.replace(" ","+")+"\"}}",*/

                /* there is no need to check stop in stopLatlong table*/


                /* "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""+s.replace(" ","+")+"\"}}",*/
                // TODO: 2/28/2017  after changes according to sir

                // New Api new Table
                /*"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&data=" +
                        "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\""
                        + s.replace(" ", "+") + "\"}}"*/
                url
                ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("getStopDataFromServer", response);
                      /*  Log.d("URL", "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&data=" +
                                "{\"filter\":{\"MasterEntryName\":\"RouteStoppage\",\"MasterEntryValue\":\"" + s.replace(" ", "+") + "\"}}");
                       */
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {
                                //  Toast.makeText(AddStopsActivity.this, "Found", Toast.LENGTH_SHORT).show();
                                //et_name.setError("Already exist");

                                if (Build.VERSION.SDK_INT >= 24) {
                                    et_name.setError(Html.fromHtml("<font color='red'>Already exist !</font>", Html.FROM_HTML_MODE_LEGACY));

                                } else {
                                    et_name.setError(Html.fromHtml("<font color='red'>Already exist !</font>"));
                                }

                                // btn_add_stop.setVisibility(View.INVISIBLE);
                                btn_add_stop.animate().translationY(btn_add_stop.getHeight() + 16)
                                        .setInterpolator(new AccelerateInterpolator(2)).start();
                                return;

                               /* JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                   // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                    //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                    //stopDataItemList.add(0,new StopDataItem());
                                    // stopDataAdapter.insertAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();
                                    *//*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*//*

                                    //IMP
                                *//*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*//*
                                }*/

                            } else {
                                et_name.setError(null);
                                // btn_add_stop.setVisibility(View.VISIBLE);
                                btn_add_stop.animate().translationY(0)
                                        .setInterpolator(new DecelerateInterpolator(2)).start();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pb.setVisibility(View.INVISIBLE);

                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                // Utility.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();
                pb.setVisibility(View.INVISIBLE);

            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //  params.put("placekey", s); old
                return params;
            }
*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppRequestQueueController.getInstance(AddStopsActivity.this).addToRequestQueue(strReq, "getStopDataFromServer");
        //queue.add(strReq);


    }


    private void postStopDataToServer(final StopDataItem stopDataItem) throws JSONException {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.show();
        JSONObject param = new JSONObject(new Gson().toJson(stopDataItem));


        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "RouteStoppagesApi.php?databaseName=" + sp.getString("organization_name", "");

        Log.e("url", url);
        Log.e("param", param.toString());

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                //"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107",

                // TODO: 2/28/2017 changes according to sir, new api new table
              /*  "http://192.168.1.161/apischoolerp/RouteStoppageApi.php?databaseName=CBA98217",*/
                //"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217"
                url,param
                ,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        progressDialog.dismiss();
                        Log.d("postStopDataToServer", jsonObject.toString());


                            String data = jsonObject.optString("code", "key not found");
                            if ("201".equalsIgnoreCase(data)) {

                                /*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*/
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddStopsActivity.this);
                                alertDialog.setMessage(getString(R.string.save_successfully));
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if (!AddStopsActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }






                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err =getString(R.string.internet_not_available_please_check_your_internet_connectivity);
                }
                Config.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        })  ;
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppRequestQueueController.getInstance(AddStopsActivity.this).addToRequestQueue(strReq, "postStopDataToServer");
        //queue.add(strReq);


    }


    private void putStopDataToServer(final StopDataItem stopDataItem) {
        Config.hideKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting...");
        progressDialog.show();

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "RouteStoppagesApi.php?databaseName=" + sp.getString("organization_name", "")
                +
                "&data={\"data\":{\"routeStoppageName\":\"" + stopDataItem.getRouteStoppageName().replace(" ", "+")
                + "\"" +
                ",\"stopLat\":\"" + stop_lat + "\",\"stopLong\":\"" + stop_long + "\"},\"filter\":{\"routeStoppageId\":\"" + stopDataItem.getRouteStoppageId() + "\"}}";

        Log.e("url", url);

        StringRequest strReq = new StringRequest(Request.Method.PUT,
                //"http://apischoolerp.zeroerp.com/MasterEntryApi.php?databaseName=A994A107",
                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217",

                // TODO: 2/28/2017 changes according to sir, new api new table
             /*   "http://192.168.1.161/apischoolerp/RouteStoppageApi.php?databaseName=CBA98217" +
                        "&data={\"data\":{\"MasterEntryValue\":\""+stopDataItem.getMasterEntryValue().replace(" ","+")+"\"" +
                        ",\"StopLat\":\""+stop_lat+"\",\"StopLong\":\""+stop_long+"\"},\"filter\":{\"MasterEntryId\":\""+stopDataItem.getMasterEntryId()+"\"}}",
*/
                /*"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217" +
                        "&data={\"data\":{\"MasterEntryValue\":\"" + stopDataItem.getMasterEntryValue().replace(" ", "+")
                        + "\"" +
                        ",\"StopLat\":\"" + stop_lat + "\",\"StopLong\":\"" + stop_long + "\"},\"filter\":{\"MasterEntryId\":\"" + stopDataItem.getMasterEntryId() + "\"}}"
               */
                url
                ,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("putStopDataToServer", response);
                        /*Log.d("URL", "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217" +
                                "&data={\"data\":{\"MasterEntryValue\":\"" + stopDataItem.getMasterEntryValue().replace(" ", "+") + "\"" +
                                ",\"StopLat\":\"" + stop_lat + "\",\"StopLong\":\"" + stop_long + "\"},\"filter\":{\"MasterEntryId\":\"" + stopDataItem.getMasterEntryId() + "\"}}");
                        */
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                /*JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    adapter_suggest_dropdown.clear();
                                    stopList.clear();
                                    stopDataItemList.clear();

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>(){}.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    AddStopsActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();

                                    for(StopDataItem stopDataItem:stopDataItemList)
                                    {
                                        adapter_suggest_dropdown.add(stopDataItem.getMasterEntryValue());
                                        stopList.add(stopDataItem.getMasterEntryValue());
                                        //    Toast.makeText(AddStopsActivity.this, stopDataItem.getMasterEntryValue(), Toast.LENGTH_SHORT).show();
                                    }


                                    //adapter_suggest_dropdown.notifyDataSetChanged();
                                    adapter_suggest_dropdown.getFilter().filter(et_name.getText(), et_name);

                                }*/
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddStopsActivity.this);
                                alertDialog.setMessage(getString(R.string.update_successfully));
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                if (!AddStopsActivity.this.isFinishing())
                                    alertDialog.show();

                                //  Toast.makeText(AddStopsActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {


                        }

                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = getString(R.string.internet_not_available_please_check_your_internet_connectivity);
                }
                Config.showToast(AddStopsActivity.this, err);
                //displayAlertDialogForRouteSave();


            }


        }) {
            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Log.e("getParamData", new Gson().toJson(stopDataItem).toString());
                params.put("data", new Gson().toJson(stopDataItem));
                return params;
            }*/
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        AppRequestQueueController.getInstance(AddStopsActivity.this).addToRequestQueue(strReq, "putStopDataToServer");
        //queue.add(strReq);

        // TODO: 2/14/2017 for 2 sec delay simply decrease the timeout which means first request fail and second request will hit


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        // overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        return true;
    }


    private void actionBarEdit() {
        Toolbar tb = (Toolbar) findViewById(R.id.activity_add_stop_tb);
        setSupportActionBar(tb);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
            TextView textview = new TextView(getApplicationContext());
            RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            textview.setLayoutParams(layoutparams);
            textview.setText(getString(R.string.route_stopage));
            //textview = (TextView) this.findViewById(R.id.toolbar_title_feature);
            textview.setSingleLine();
            textview.setTextColor(Color.WHITE);
            textview.setEllipsize(TextUtils.TruncateAt.END);
            textview.setGravity(Gravity.CENTER);
            textview.setTextSize(18);
            textview.setTypeface(Typeface.SERIF);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            actionBar.setCustomView(textview, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //  actionBar.setIcon(R.drawable.rateourapp);
            // actionBar.setHomeButtonEnabled(true);

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng latlng_default;

        if (stop_lat != null && stop_long != null && !stop_lat.isEmpty() && !stop_long.isEmpty()) {
            latlng_default = new LatLng(Double.parseDouble(stop_lat), Double.parseDouble(stop_long));
        } else {
            latlng_default = new LatLng(23.231116, 77.433592);  //  default current location
            stop_lat = "23.231116";
            stop_long = " 77.433592";
        }


        st = mMap.addMarker(new MarkerOptions().position(latlng_default)
                // .title(et_name.getText().toString().isEmpty()?"Current Location":et_name.getText().toString())
                .title("Stop")
                .snippet("Lat: " + stop_lat + ", Long: " + stop_long)
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red))
        );
        st.setDraggable(true);
        st.showInfoWindow();

        CameraPosition position = new CameraPosition.Builder()
                .target(latlng_default)
                .zoom(13).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // marker.setTitle(et_name.getText().toString());
                stop_lat = String.format("%.6f", marker.getPosition().latitude);
                stop_long = String.format("%.6f", marker.getPosition().longitude);

                marker.setSnippet("Lat: " + stop_lat + ", Long: " + stop_long);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                marker.showInfoWindow();

            }
        });
    }

    public void onClickGoogleApi(View view) {
        //  currentViewSelect = (EditText) view;
        //  Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                    // .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    //  .setTypeFilter(Place.TYPE_COUNTRY).setCountry("IN")  // work up to 8.4 play service version
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE)
                    .build();


            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, 1000);

        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("GoogleAddress", "Place: " + place.getName());
                place.getAddress();
                //      currentViewSelect.setText(place.getAddress() + "");
                LatLng latlong = place.getLatLng();
                // Toast.makeText(this, " " + latlong.latitude + " " + latlong.longitude, Toast.LENGTH_SHORT).show();

             /*   String placeDetailsStr = place.getName() + "\n"
                        + place.getId() + "\n"
                        + place.getLatLng().toString() + "\n"
                        + place.getAddress() + "\n"
                        + place.getAttributions();*/
                //   txtPlaceDetails.setText(placeDetailsStr);
                // Toast.makeText(this, " " + placeDetailsStr, Toast.LENGTH_SHORT).show();

                et_name.setText(place.getName());
                stop_lat = latlong.latitude + "";
                stop_long = latlong.longitude + "";
                st.setPosition(latlong);
                CameraPosition position = new CameraPosition.Builder()
                        .target(latlong)
                        .zoom(13).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("GoogleAddress", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                getCurrentLocationLatLong();
                //  requestLocation();

            } else {
                onSupportNavigateUp();
                //Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        loading.dismiss();
        //    Toast.makeText(this, "LocationChanged", Toast.LENGTH_SHORT).show();
        if (location != null) {
            stop_lat = "" + location.getLatitude();
            stop_long = "" + location.getLongitude();
            st.setSnippet("Lat: " + stop_lat + ", Long: " + stop_long);
            LatLng current;
            st.hideInfoWindow();
            st.showInfoWindow();
            st.setPosition(current = new LatLng(location.getLatitude(), location.getLongitude()));
            CameraPosition position = new CameraPosition.Builder()
                    .target(current)
                    .zoom(13).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
