package in.junctiontech.school.tranport.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.tranport.activity.AddStopsActivity;
import in.junctiontech.school.tranport.adapter.StopDataAdapter;
import in.junctiontech.school.tranport.model.stopdata.StopDataItem;

/**
 * Created by LENEVO on 06-09-2017.
 */

public class StopListFragment extends Fragment {
    private BroadcastReceiver mRegistrationBroadcastReceiver;
        private FloatingActionButton btn_add_class;
        private RecyclerView recycler_view_vehicle_list;
        private RelativeLayout snackbar;
        private SharedPreferences sp;
        private Gson gson;
        private String dbName;
        private ProgressDialog progressbar;
        private int  appColor;
        private ArrayList<StopDataItem> stopDataItemList= new ArrayList<>();
        private StopDataAdapter stopDataAdapter;


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View convertView = inflater.inflate(R.layout.fragment_list_and_one_button, container, false);
            convertView.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

            snackbar = (RelativeLayout) convertView.findViewById(R.id.ll_fragment_list_and_one_button);
            //  et_class_name = (EditText) convertView.findViewById(R.id.et_create_school_class_class_name);
            FloatingActionButton fab_fragment_add   =(FloatingActionButton) convertView.findViewById(R.id.fab_fragment_add);
            fab_fragment_add.setBackgroundTintList( ColorStateList.valueOf(appColor));
            fab_fragment_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StopDataItem stopDataItem = new StopDataItem();
                    startActivity(new Intent(getActivity(), AddStopsActivity.class)
                            .putExtra("stop_name", stopDataItem.getRouteStoppageName()));
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.nothing);

                }
            });

            recycler_view_vehicle_list = (RecyclerView) convertView.findViewById(R.id.rv_fragment_list);
            recycler_view_vehicle_list.setPadding(0,0,0,60);
            setHasOptionsMenu(true);
            return convertView;
        }

        private void setupRecycler() {
            RecyclerView.LayoutManager llm_list = new LinearLayoutManager(getActivity());
            recycler_view_vehicle_list.setHasFixedSize(true);
            recycler_view_vehicle_list.setLayoutManager(llm_list);
            recycler_view_vehicle_list.setNestedScrollingEnabled(true);

            stopDataAdapter = new StopDataAdapter(getActivity(), stopDataItemList);
            recycler_view_vehicle_list.setAdapter(stopDataAdapter);

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            sp = Prefs.with(getActivity()).getSharedPreferences();
            progressbar = new ProgressDialog(getActivity());
            gson = new Gson();
            dbName = sp.getString("organization_name", "");
            progressbar.setCancelable(false);
            progressbar.setMessage(getString(R.string.please_wait));
            appColor =  Config.getAppColor(getActivity(),false);
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            setupRecycler();

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                        Log.e("internet", "available");
                        if (stopDataItemList.size()==0)
                            get_routeStoppageApi();

                    }
                }
            };

        }

        private void get_routeStoppageApi() {
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage(getString(R.string.please_wait));
            pDialog.setCancelable(false);
            pDialog.show();

            String url= sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +"RouteStoppagesApi.php?databaseName="+sp.getString("organization_name", "");

            Log.e("url",url);
            StringRequest strReq = new StringRequest(Request.Method.GET,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
                    //todo routestoppage in url
                    // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",
                    url
             /*   "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217"*/
                    ,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.e("get_routeStoppageApi", response.toString());


                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String data = jsonObject.optString("code", "key not found");
                                if ("200".equalsIgnoreCase(data)) {

                                    JSONArray jsonArray_result = jsonObject.optJSONArray("result");

                                    if (jsonArray_result != null   ) {
                                        stopDataItemList.clear();


                                        String jsonOutput = jsonArray_result.toString();
                                        Type listType = new TypeToken<List<StopDataItem>>() {
                                        }.getType();
                                        //stopDataItemList.add(0,new StopDataItem());
                                      stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);

                                        stopDataAdapter.updateList(stopDataItemList);


                                        //stopDataItemList.add(0,new StopDataItem());
                                        // stopDataAdapter.insertAll(stopDataItemList);
                                        // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                        //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } finally {
                                pDialog.dismiss();

                            }
                        }
                    }

                    , new Response.ErrorListener()

            {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    Log.e("TAG", "Error: " + error.getMessage());
                    Config.responseVolleyErrorHandler(getActivity(),error,snackbar);
                }

            }

            )

            {

                // TODO: 2/6/2017 get k case me getParams call nai hota
          /*  @Override
            protected Map<String, String> getParams() {
                JSONObject jsonObject_masterentry = new JSONObject();
                Map<String, String> param = new HashMap<>();
                param.put("databaseName","A994A107");
                try {
                    jsonObject_masterentry.put("MasterEntryName", "RouteTo");
                    JSONObject jsonObject_filter = new JSONObject();
                    jsonObject_filter.put("filter", jsonObject_masterentry);
                    param.put("data", jsonObject_filter.toString());
                    Log.e("qwerty",jsonObject_filter.toString());
                    Log.e("qwerty",param.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return param;
            }*/

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
            //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
            //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
            // AppController.getInstance().addToRequestQueue(strReq, "get_routeStoppageApi");
            AppRequestQueueController.getInstance(getActivity()).addToRequestQueue(strReq, "get_routeStoppageApi");
        }

        @Override
        public void onResume() {
            get_routeStoppageApi();
            super.onResume();
        }
}
