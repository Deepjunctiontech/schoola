package in.junctiontech.school.tranport.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.tranport.adapter.StopDataAdapter;
import in.junctiontech.school.tranport.adapter.VehicleDataAdapter;
import in.junctiontech.school.tranport.model.stopdata.StopDataItem;
import in.junctiontech.school.tranport.model.vehicledata.VehicleDataItem;


public class TransportActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private TabLayout tab;
    private RecyclerView rcv_list;
    private List<VehicleDataItem> vehicleDataItemList;
    private List<VehicleDataItem> searchedVehicleDataItemList;
    private List<StopDataItem> stopDataItemList;
    private StopDataAdapter stopDataAdapter;
    private VehicleDataAdapter vehicleDataAdapter;
    private SearchView searchView;
    private MenuItem item;
    private SharedPreferences sp;
    private int colorIs;

    private void setColorApp() {
        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
      //  getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((AppBarLayout)findViewById(R.id.appbar_transport)).setBackgroundColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport);
        sp = Prefs.with(this).getSharedPreferences();
 /*  ********************************    Demo Screen *******************************/
        final RelativeLayout   page_demo = (RelativeLayout) findViewById(R.id.rl_transport_demo);
        if (sp.getBoolean("DemoTransportPage", true)) {
            page_demo.setVisibility(View.VISIBLE);

        } else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoTransportPage", false).commit();
            }
        });
           /* ***************************************************************/

        actionBarEdit();
        setColorApp();
        initViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_search, menu);

        item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);


      //  searchView.setIconified(false);

      /*  MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        if(tab.getTabAt(0).isSelected()) {
                            updateApapter_Vehicle(vehicleDataItemList);
                        }
                        else if(tab.getTabAt(2).isSelected())
                            updateApapter_Stop(stopDataItemList);

                        Toast.makeText(TransportActivity.this, "collapsed", Toast.LENGTH_SHORT).show();

                        //searchView.setIconified(true);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        //searchView.setIconified(true);
                        Toast.makeText(TransportActivity.this, "expanded", Toast.LENGTH_SHORT).show();
                        return true; // Return true to expand action view
                    }
                });*/

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onQueryTextChange(final String text_to_be_search) {

        if(tab.getTabAt(0).isSelected()) {
            updateApapter_Vehicle(filterList_Vehicle(vehicleDataItemList, text_to_be_search));
        }
        else if(tab.getTabAt(2).isSelected())
            updateApapter_Stop(filterList_Stop(stopDataItemList, text_to_be_search));

        return true;
    }

    private void updateApapter_Vehicle(List<VehicleDataItem> filteredModelList) {
        vehicleDataAdapter.setVehicleDataItemList(filteredModelList);
        vehicleDataAdapter.notifyDataSetChanged();
    }

    private void updateApapter_Stop(List<StopDataItem> filteredModelList) {
        stopDataAdapter.setStopDataItemList(filteredModelList);
        stopDataAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<VehicleDataItem> filterList_Vehicle(List<VehicleDataItem> vehicleDataItemList, String text_to_be_search) {

       /* org.apache.commons.lang3.StringUtils.containsIgnoreCase("ABCDEFGHIJKLMNOP", "gHi");
        This one may be better than regex as regex is always expensive in terms of performance.*/

        text_to_be_search = text_to_be_search.toLowerCase();
        final List<VehicleDataItem> filteredModelList = new ArrayList<>();

        for (VehicleDataItem vehicleDataItem : vehicleDataItemList) {
            if (vehicleDataItem.getVehicleName() != null) {
                if (vehicleDataItem.getVehicleName().toLowerCase().contains(text_to_be_search)
                    /*||
                    model.getMasterEntryValue().toLowerCase().contains(query)*/) {
                    filteredModelList.add(vehicleDataItem);
                }
            }
            else {
                filteredModelList.add(vehicleDataItem);
            }
        }
        return filteredModelList;
    }

    private List<StopDataItem> filterList_Stop(List<StopDataItem> stopDataItemList, String text_to_be_search) {

       /* org.apache.commons.lang3.StringUtils.containsIgnoreCase("ABCDEFGHIJKLMNOP", "gHi");
        This one may be better than regex as regex is always expensive in terms of performance.*/

        text_to_be_search = text_to_be_search.toLowerCase();
        final List<StopDataItem> filteredModelList = new ArrayList<>();

        for (StopDataItem stopDataItem : stopDataItemList) {
            if (stopDataItem.getRouteStoppageName() != null) {
                if (stopDataItem.getRouteStoppageName().toLowerCase().contains(text_to_be_search)
                    /*||
                    model.getMasterEntryValue().toLowerCase().contains(query)*/) {
                    filteredModelList.add(stopDataItem);
                }
            }

                else{
                filteredModelList.add(stopDataItem);
            }
        }
        return filteredModelList;
    }


    private void initViews() {
        tab = (TabLayout) findViewById(R.id.activity_transport_tab);
        tab.setBackgroundColor(colorIs);
        tab.addTab(tab.newTab().setText("Vehicle").setIcon(R.drawable.ic_directions_car_white_24dp));
        tab.addTab(tab.newTab().setText("Route").setIcon(R.drawable.ic_local_movies_white_24dp));
        tab.addTab(tab.newTab().setText("Stop").setIcon(android.R.drawable.ic_dialog_map));

        tab.setSmoothScrollingEnabled(true);

        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tabClick) {

                MenuItemCompat.collapseActionView(item);

                if (tab.getSelectedTabPosition() == 1) {

                    if (vehicleDataItemList.size() > 1) {
                        startActivity(new Intent(TransportActivity.this, LocationMapsActivity.class));
                    } else
                        Toast.makeText(TransportActivity.this, "Add Vehicle First !!!", Toast.LENGTH_SHORT).show();

                    tab.getTabAt(0).select();
                } else if (tab.getSelectedTabPosition() == 2) {
                    displayTabSelectedData(true);
                } else
                    displayTabSelectedData(false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        LinearLayoutManager llm_list
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rcv_list = (RecyclerView) findViewById(R.id.activity_transport_rcv_list);

        // rcv_directory_list.setOnLongClickListener(new V);
        rcv_list.setHasFixedSize(true);
        rcv_list.setLayoutManager(llm_list);
        rcv_list.setNestedScrollingEnabled(true);

        stopDataItemList = new ArrayList<>();
        stopDataItemList.add(0, new StopDataItem());
        stopDataAdapter = new StopDataAdapter(TransportActivity.this, stopDataItemList);
        // rcv_list.setAdapter(stopDataAdapter); default false vehicle display

        vehicleDataItemList = new ArrayList<>();
        vehicleDataItemList.add(0, new VehicleDataItem());
     //   vehicleDataAdapter = new VehicleDataAdapter(TransportActivity.this, vehicleDataItemList);
        //  rcv_list.setAdapter(vehicleDataAdapter);

        // displayTabSelectedData(false);  // display default tab select value
        //get_routeStoppageApi();

    }

    private void displayTabSelectedData(boolean b) {
        if (b) {
            rcv_list.setAdapter(stopDataAdapter);

            get_routeStoppageApi();
        } else {

            rcv_list.setAdapter(vehicleDataAdapter);
            get_vehicleApi();

           /* runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(vehicleDataItemList.size()==1)
                    for (int i = 0; i < 1000; i++) {
                        VehicleDataItem vehicleDataItem = new VehicleDataItem("Junction001",
                                "Vishal Yadav", "R25 "+(i+1), "MP04 SB 7606"," "," ");
                        vehicleDataItemList.add(vehicleDataItem);
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    vehicleDataAdapter.notifyDataSetChanged();
                }
            });*/


        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
}

    private void actionBarEdit() {
        Toolbar tb = (Toolbar) findViewById(R.id.activity_transport_toolbar);
        setSupportActionBar(tb);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // actionBar.setHideOnContentScrollEnabled(true);
            actionBar.setElevation(0);
            TextView textview = new TextView(getApplicationContext());
            RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            textview.setLayoutParams(layoutparams);
            textview.setText("Transport");
            //textview = (TextView) this.findViewById(R.id.toolbar_title_feature);
            textview.setSingleLine();
            textview.setTextColor(Color.WHITE);
            textview.setEllipsize(TextUtils.TruncateAt.END);
            textview.setGravity(Gravity.CENTER);
            textview.setTextSize(18);
            textview.setTypeface(Typeface.SERIF);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            actionBar.setCustomView(textview, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //  actionBar.setIcon(R.drawable.rateourapp);
            // actionBar.setHomeButtonEnabled(true);

        }
    }

    private void get_routeStoppageApi() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
        pDialog.setCancelable(false);
        pDialog.show();

        String url= sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +"RouteStoppagesApi.php?databaseName="+sp.getString("organization_name", "");

        Log.e("url",url);
        StringRequest strReq = new StringRequest(Request.Method.GET,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
                //todo routestoppage in url
                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",
                url
             /*   "http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217"*/
                ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("get_routeStoppageApi", response);


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    stopDataItemList.clear();

                                    stopDataItemList.add(new StopDataItem());

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<StopDataItem>>() {
                                    }.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<StopDataItem> stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    TransportActivity.this.stopDataItemList.addAll(stopDataItemList);
                                    stopDataAdapter.notifyDataSetChanged();


                                    //stopDataItemList.add(0,new StopDataItem());
                                    // stopDataAdapter.insertAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                    //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pDialog.dismiss();

                        }
                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(TransportActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        )

        {

            // TODO: 2/6/2017 get k case me getParams call nai hota
          /*  @Override
            protected Map<String, String> getParams() {
                JSONObject jsonObject_masterentry = new JSONObject();
                Map<String, String> param = new HashMap<>();
                param.put("databaseName","A994A107");
                try {
                    jsonObject_masterentry.put("MasterEntryName", "RouteTo");
                    JSONObject jsonObject_filter = new JSONObject();
                    jsonObject_filter.put("filter", jsonObject_masterentry);
                    param.put("data", jsonObject_filter.toString());
                    Log.e("qwerty",jsonObject_filter.toString());
                    Log.e("qwerty",param.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return param;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
        //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
       // AppController.getInstance().addToRequestQueue(strReq, "get_routeStoppageApi");
        AppRequestQueueController.getInstance(TransportActivity.this).addToRequestQueue(strReq, "get_routeStoppageApi");
    }

    private void get_vehicleApi() {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
        pDialog.setCancelable(false);
        pDialog.show();

        String url= sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                +"VehicleApi.php?databaseName="+sp.getString("organization_name", "");

        Log.e("url",url);

        StringRequest strReq = new StringRequest(Request.Method.GET,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
                //"http://192.168.1.161/apischoolerp/VehicleApi.php?databaseName=CBA98217"

              url  ,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("get_vehicleApi", response.toString());


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String data = jsonObject.optString("code", "key not found");
                            if ("200".equalsIgnoreCase(data)) {

                                JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                int jsonArray_result_length;
                                if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                    vehicleDataItemList.clear();

                                    vehicleDataItemList.add(new VehicleDataItem());

                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<VehicleDataItem>>() {
                                    }.getType();
                                    //stopDataItemList.add(0,new StopDataItem());
                                    ArrayList<VehicleDataItem> vehicleDataItemList = (ArrayList<VehicleDataItem>) gson.fromJson(jsonOutput, listType);
                                    TransportActivity.this.vehicleDataItemList.addAll(vehicleDataItemList);
                                    vehicleDataAdapter.notifyDataSetChanged();


                                    //stopDataItemList.add(0,new StopDataItem());
                                    // stopDataAdapter.insertAll(stopDataItemList);
                                    // stopDataAdapter.notifyDataSetChanged();
                                    /*for (int jsonArray_result_init = 0; jsonArray_result_length > jsonArray_result_init; jsonArray_result_init++) {
                                        stopDataItemList.add(jsonArray_result.getJSONObject(jsonArray_result_init).getString("MasterEntryValue"));
                                    }
                                    displayAlertDialogForRouteSave();*/

                                    //IMP
                                /*    stopDataItemList = (ArrayList<StopDataItem>) gson.fromJson(jsonOutput, listType);
                                    //  stopDataItemList.addAll(posts);
                                    stopDataItemList.add(0,new StopDataItem());
                                    stopDataAdapter.insertAll(stopDataItemList);*/
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            pDialog.dismiss();

                        }
                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(TransportActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        )

        {

            // TODO: 2/6/2017 get k case me getParams call nai hota
          /*  @Override
            protected Map<String, String> getParams() {
                JSONObject jsonObject_masterentry = new JSONObject();
                Map<String, String> param = new HashMap<>();
                param.put("databaseName","A994A107");
                try {
                    jsonObject_masterentry.put("MasterEntryName", "RouteTo");
                    JSONObject jsonObject_filter = new JSONObject();
                    jsonObject_filter.put("filter", jsonObject_masterentry);
                    param.put("data", jsonObject_filter.toString());
                    Log.e("qwerty",jsonObject_filter.toString());
                    Log.e("qwerty",param.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return param;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, 0));
        //  strReq.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 2));
        //   Request.setShouldCache(false);
     /*   strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
/*        AppController.getInstance().addToRequestQueue(strReq, "get_vehicleApi");*/
        AppRequestQueueController.getInstance(TransportActivity.this).addToRequestQueue(strReq, "get_vehicleApi");

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (tab.getSelectedTabPosition() == 0) {
            displayTabSelectedData(false);
        } else {
            displayTabSelectedData(true);
        }


    }
}
