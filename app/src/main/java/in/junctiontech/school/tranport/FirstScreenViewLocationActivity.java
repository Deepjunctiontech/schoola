package in.junctiontech.school.tranport;

import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.MasterEntryEntity;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.tranport.model.vehicleroutedata.VehicleRouteDataItem;

public class FirstScreenViewLocationActivity extends AppCompatActivity {

    MainDatabase mDb ;
    private ArrayList<String> vehicleRouteTextList = new ArrayList<>();
    private ArrayList<String> routeToTextList = new ArrayList<>();
    private ArrayAdapter<String> vehicleAdapter, routeToAdapter;
    private AppCompatSpinner spinner_select_route, spinner_track_through, spinner_route_to;
   // private RadioButton rb_track_location_to_school, rb_track_location_to_home;

    private String dbName;
    private ArrayList<VehicleRouteDataItem> vehicleRouteDataItemList = new ArrayList<>();
    private SharedPreferences sp;
    private ProgressDialog pDialog;
    private ArrayList<MasterEntryEntity>  routeToListData= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen_view_location);

        mDb = MainDatabase.getDatabase(this);

        sp = Prefs.with(this).getSharedPreferences();
        dbName = Gc.getSharedPreference(Gc.ERPDBNAME, this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
        pDialog.setCancelable(false);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int color = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        ((TextView) findViewById(R.id.tv_track_location_select_route)).setTextColor(color);
        ((TextView) findViewById(R.id.tv_track_location_track_through)).setTextColor(color);
        ((TextView) findViewById(R.id.tv_track_location_route_to)).setTextColor(color);
        ((Button) findViewById(R.id.btn_track_location_view_location)).setBackgroundColor(color);

        spinner_select_route =  findViewById(R.id.spinner_track_location_select_route);
        spinner_track_through =  findViewById(R.id.spinner_track_location_track_through);
        spinner_route_to =  findViewById(R.id.spinner_track_location_route_to);
      /*  rb_track_location_to_school = (RadioButton) findViewById(R.id.rb_track_location_to_school);
        rb_track_location_to_home = (RadioButton) findViewById(R.id.rb_track_location_to_home);
        rb_track_location_to_school.setChecked(true);*/

        setRouteToList();
//        getRouteToFromServer();

        setRouteList();
        getRouteList();


        if (sp.getString("trackThrough","Vehicle").equalsIgnoreCase("Vehicle"))
            spinner_track_through.setSelection(0);
        else spinner_track_through.setSelection(1);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDb.masterEntryModel().getMasterEntryValuesGeneric("RouteTo").observe(this, new Observer<List<MasterEntryEntity>>() {
            @Override
            public void onChanged(@Nullable List<MasterEntryEntity> masterEntryEntities) {
                routeToListData.clear();
                routeToListData.addAll(masterEntryEntities);
                setRouteToList();
            }
        });
    }

    public void viewLocation(View view) {
        if (spinner_select_route.getSelectedItemPosition()>0 && spinner_route_to.getSelectedItemPosition()>0){
            sp.edit()
                    .putString("vehicleRouteId",vehicleRouteDataItemList.get(spinner_select_route.getSelectedItemPosition() - 1).getVehicleRouteId())
                    .putString("routeType",routeToListData.get(spinner_route_to.getSelectedItemPosition() - 1).MasterEntryId)
                    .putString("trackThrough",spinner_track_through.getSelectedItem().toString())
                    .commit();
            startActivity(new Intent(this, TrackLocationActivity.class));
            finish();
            overridePendingTransition(R.anim.enter, R.anim.nothing);
        }else {
            if (  spinner_select_route.getSelectedItemPosition()<=0){
            Toast.makeText(this,"Select route",Toast.LENGTH_SHORT).show();
                TextView errorText = (TextView) spinner_select_route.getSelectedView();

                errorText.setError(getString(R.string.select_route));   }
            if (  spinner_route_to.getSelectedItemPosition()<=0)
            Toast.makeText(this,"Select route to",Toast.LENGTH_SHORT).show();
        }

    }

    public void getRouteToFromServer() {
        pDialog.setMessage("Getting Route to List...");
        pDialog.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryName", "RouteTo");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));

        String bloodFetchUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Config.applicationVersionNameValue
                + "MasterEntryApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param1));
        Log.d("bloodFetchUrl", bloodFetchUrl);
        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                bloodFetchUrl
                //  "http://192.168.1.151/apischoolerp/MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntry", s);
                        pDialog.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = (new Gson()).fromJson(s, new TypeToken<MasterEntry>(){}.getType());
//                                routeToListData = obj.getResult();
                                setRouteToList();
                            }

                            //    fragmentStudentAdmission.updateDistanceType(distanceList);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MasterEntry", volleyError.toString());
                pDialog.cancel();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", (new JSONObject(param1)).toString());
                //  Log.d("loginjson", new JSONObject(param).toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    private void  setRouteToList() {
        routeToTextList.clear();
        routeToTextList.add(getString(R.string.select));
        int lastSelectedPos=0, i=1;
        String  routeType =    sp.getString("routeType","");

        for (MasterEntryEntity vehicleData : routeToListData) {
            routeToTextList.add(vehicleData.MasterEntryValue == null ? "" : vehicleData.MasterEntryValue);
            if (routeType.equalsIgnoreCase(vehicleData.MasterEntryId))
                lastSelectedPos = i;

            i++;
        }
        routeToAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, routeToTextList);
        routeToAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_route_to.setAdapter(routeToAdapter);

        spinner_route_to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position!=0)
                    getRouteList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (routeToTextList.size()>=lastSelectedPos) {
            spinner_route_to.setSelection(lastSelectedPos);
            getRouteList();
        }

    }

    public void getRouteList() {
        if ( spinner_route_to.getSelectedItemPosition()>0) {
            pDialog.setMessage("Getting Route List...");
            pDialog.show();
            Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("RouteTo", routeToListData.get(spinner_route_to.getSelectedItemPosition() - 1).MasterEntryId);

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));

            String notifiUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                    + Config.applicationVersionNameValue
                    + "VehicleRouteApi.php?" + "databaseName=" + dbName+"&data=" + (new JSONObject(param1));
            Log.d("RouteId", notifiUrl);
            vehicleRouteDataItemList.clear();
            vehicleRouteTextList.clear();
            StringRequest request = new StringRequest(Request.Method.GET,
                    notifiUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("RouteId", s);
                            pDialog.dismiss();

                            //stopDataItemList.add(0,new StopDataItem());
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                                    Gson gson = new Gson();
                                    String jsonOutput = jsonArray_result.toString();
                                    Type listType = new TypeToken<List<VehicleRouteDataItem>>() {
                                    }.getType();
                                    vehicleRouteDataItemList = (ArrayList<VehicleRouteDataItem>) gson.fromJson(jsonOutput, listType);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            setRouteList();

                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("RouteApi", volleyError.toString());
                    pDialog.dismiss();
                    setRouteList();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();

                    return params;
                }
            };


            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            AppRequestQueueController.getInstance(this).addToRequestQueue(request, "RouteList");
        }
    }

    private void setRouteList() {
        vehicleRouteTextList.clear();
        vehicleRouteTextList.add(getString(R.string.select));
       int lastSelectedPos=0, i=1;
       String  vehicleRouteId =    sp.getString("vehicleRouteId","");
        for (VehicleRouteDataItem vehicleData : vehicleRouteDataItemList) {
            vehicleRouteTextList.add(vehicleData.getVehicleRouteName() == null ? "" : vehicleData.getVehicleRouteName());
            if (vehicleRouteId.equalsIgnoreCase(vehicleData.getVehicleRouteId()))
                lastSelectedPos = i;

            i++;
        }
        vehicleAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, vehicleRouteTextList);
        vehicleAdapter.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        spinner_select_route.setAdapter(vehicleAdapter);

        if (vehicleRouteTextList.size()>=lastSelectedPos)
            spinner_select_route.setSelection(lastSelectedPos);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
