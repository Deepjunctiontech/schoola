package in.junctiontech.school.tranport.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by LENEVO on 08-09-2017.
 */

public class GpsData implements Serializable {
    String routeID,identifier,type,id,date,time,lat,lng, createdON;
ArrayList<GpsData> result;

    public String getCreatedON() {
        return createdON;
    }

    public void setCreatedON(String createdON) {
        this.createdON = createdON;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public ArrayList<GpsData> getResult() {
        return result;
    }

    public void setResult(ArrayList<GpsData> result) {
        this.result = result;
    }
}
