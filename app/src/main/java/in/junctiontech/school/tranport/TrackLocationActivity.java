package in.junctiontech.school.tranport;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.tranport.model.AssignStopData;
import in.junctiontech.school.tranport.model.GpsData;

public class TrackLocationActivity extends AppCompatActivity implements OnMapReadyCallback {
    BlinkingMarker markerLastUpdatedLoc = null;
    private GoogleMap mMap;
    private String route_url;
    private SharedPreferences sp;
    private String dbName;
    private String vehicleRouteId, routeType, trackThrough;


    private ArrayList<LatLng> demoLatLongList = new ArrayList<>();
    private ArrayList<AssignStopData> stopDataItemList_route = new ArrayList<>();
    private ArrayList<LatLng> lastCurrentLocationList = new ArrayList<>();
    private ArrayList<AssignStopData> midStopList = new ArrayList<>();
    private GpsData latlongLastUpdatedLocation;
    private AssignStopData startAssignStopDataObj, destinationAssignStopDataObj;
    private ProgressDialog pDialog;
    private Handler  handler;
    private Runnable  runnable;
    private String identifier ="";
    private String timingLastRunningPoint ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_location);
        sp = Prefs.with(this).getSharedPreferences();
        dbName = Gc.getSharedPreference(Gc.ERPDBNAME, this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...");
       // pDialog.setCancelable(false);
       // pDialog.show();
        FloatingActionButton fab_fragment_edit = ((FloatingActionButton) findViewById(R.id.fab_fragment_edit));
        fab_fragment_edit.setVisibility(View.VISIBLE);
        fab_fragment_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppRequestQueueController.getInstance(TrackLocationActivity.this).cancelRequestByTag("lastLocation");
                handler.removeCallbacks(runnable);
                startActivity(new Intent(TrackLocationActivity.this, FirstScreenViewLocationActivity.class));
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

            }
        });
        FloatingActionButton fab_fragment_focus_to_current = ((FloatingActionButton) findViewById(R.id.fab_fragment_focus_to_current));
        fab_fragment_focus_to_current.setVisibility(View.VISIBLE);
        fab_fragment_focus_to_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveFocuToCurrentLocation();
            }
        });

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        int color = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        vehicleRouteId = sp.getString("vehicleRouteId", "");
        routeType = sp.getString("routeType", "");
        trackThrough = sp.getString("trackThrough", "");
        try {
            getIMEIOfSelectedRoute(vehicleRouteId, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

     /*   demoLatLongList.add(new LatLng(Double.parseDouble("23.229706666667"), Double.parseDouble("77.485688333333")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.23017"), Double.parseDouble("77.484901666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.230326666667"), Double.parseDouble("77.481845")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.231318333333"), Double.parseDouble("77.477788333333")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.23203"), Double.parseDouble("77.474315")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.231988333333"), Double.parseDouble("77.47233")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.233525"), Double.parseDouble("77.472073333333")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.235961666667"), Double.parseDouble("77.472501666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.24081"), Double.parseDouble("77.473")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.242251666667"), Double.parseDouble("77.473035")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.24507"), Double.parseDouble("77.472526666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.247118333333"), Double.parseDouble("77.471416666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.248708333333"), Double.parseDouble("77.470956666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.249388333333"), Double.parseDouble("77.470956666667")));
        demoLatLongList.add(new LatLng(Double.parseDouble("23.25088"), Double.parseDouble("77.463023333333")));
*/
         handler =  new Handler();
       runnable= new Runnable() {
            @Override
            public void run() {
               Log.e("BBBBB", "BBB");
              /* if (demoLatLongList.size() > lastCurrentLocationList.size()) {
                     lastCurrentLocationList.add(demoLatLongList.get(lastCurrentLocationList.size()));

                      addNewMarker();
                }*/

                try {
                    getIMEIOfSelectedRoute(vehicleRouteId, false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                fetchDataCountinue();

            }
        };
        fetchDataCountinue();


    }

    private void fetchDataCountinue() {

        handler.postDelayed(runnable, 20*1000);
    }

    private void moveFocuToCurrentLocation() {
        if (latlongLastUpdatedLocation != null) {

            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(latlongLastUpdatedLocation.getLat()),
                            Double.parseDouble(latlongLastUpdatedLocation.getLng())))
                    .zoom(19).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }
    }

    private void getIMEIOfSelectedRoute(final String vehicleRouteId, final boolean isNeededToRecreateRoute) throws JSONException {
       if (identifier.equalsIgnoreCase("")) {
           latlongLastUpdatedLocation = null;
           JSONObject jsonData = new JSONObject();
           jsonData.put("VehicleRouteId", vehicleRouteId);


           JSONObject jsonfilter = new JSONObject();
           jsonfilter.put("filter", jsonData);
           jsonfilter.put("type", trackThrough);
           pDialog.show();

           String addNotiUrl = Gc.getSharedPreference(Gc.ERPHOSTAPIURL,this)
                   + Config.applicationVersionNameValue
                   + "Route.php?databaseName="
                   + dbName + "&action=routeDetails&data=" + jsonfilter;

           Log.d("IMEIData", addNotiUrl);


           JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, addNotiUrl, null,
                   new Response.Listener<JSONObject>() {
                       @Override
                       public void onResponse(JSONObject jsonObject) {
                           DbHandler.longInfo(jsonObject.toString());
                           Log.e("IMEIData", jsonObject.toString());
                           pDialog.dismiss();
                           if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                               JSONArray jar = jsonObject.optJSONArray("result");

                               try {
                                   JSONObject objRes= jar.optJSONObject(0);
                                   identifier = objRes.optString("VehicleDeviceId");
                                   getSupportActionBar().setSubtitle( objRes.optString("VehicleNumber")+" : "+ objRes.optString("VehicleRouteName"));
                                   getLastLocationOfSelectedRoute(identifier, vehicleRouteId, isNeededToRecreateRoute);
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }

                           }

                       }
                   },
                   new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError volleyError) {
                           Log.e("IMEIData", volleyError.toString());
                           pDialog.dismiss();
                           try {
                               getAssignStopToRouteApi(vehicleRouteId);
                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }
                   });
           jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                   DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                   DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

           AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);

       }else getLastLocationOfSelectedRoute(identifier, vehicleRouteId, isNeededToRecreateRoute);
    }

    private void getLastLocationOfSelectedRoute(final String imei, final String vehicleRouteId, final boolean isNeededToRecreateRoute) throws JSONException {
        if (!imei.equalsIgnoreCase("")) {
            JSONObject jsonData = new JSONObject();
            jsonData.put("identifier", imei);

            JSONObject jsonfilter = new JSONObject();
            jsonfilter.put("filter", jsonData);
         //   pDialog.show();

            String addNotiUrl = Config.GPS_DATA_URL +
                    "?data=" + jsonfilter;

            Log.e("GpsLastLocationData", addNotiUrl);


            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, addNotiUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            DbHandler.longInfo(jsonObject.toString());
                            Log.e("GpsData", jsonObject.toString());
                        //    pDialog.dismiss();
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                GpsData gpsDataObj = (new Gson()).fromJson(jsonObject.toString(), new TypeToken<GpsData>() {
                                }.getType());

                                ArrayList<GpsData> jasonArRes = gpsDataObj.getResult();
                                if (jasonArRes.size() > 0) {
                                    latlongLastUpdatedLocation = jasonArRes.get(0);

                                }


                            }
                            try {
                                if (isNeededToRecreateRoute || (markerLastUpdatedLoc == null))
                                    getAssignStopToRouteApi(vehicleRouteId);
                                else {
                                    if (latlongLastUpdatedLocation != null) {
                                        LatLng latLongGpsData = new LatLng(Double.parseDouble(latlongLastUpdatedLocation.getLat()), Double.parseDouble(latlongLastUpdatedLocation.getLng()));
                                      if (lastCurrentLocationList.size()>1) {
                                          if (!(lastCurrentLocationList.get(lastCurrentLocationList.size() - 1).latitude == latLongGpsData.latitude
                                                  && lastCurrentLocationList.get(lastCurrentLocationList.size() - 1).longitude == latLongGpsData.longitude)) {
                                              lastCurrentLocationList.add(latLongGpsData);
                                              timingLastRunningPoint = latlongLastUpdatedLocation.getCreatedON();
                                              addNewMarker();
                                              markerLastUpdatedLoc.moveMarker(new LatLng(latLongGpsData.latitude, latLongGpsData.longitude));
                                              moveFocuToCurrentLocation();
                                          }
                                      }else  lastCurrentLocationList.add(latLongGpsData);

                                        // markerLastUpdatedLoc.addToMap(new LatLng(latLongGpsData.latitude, latLongGpsData.longitude));
                                    } else
                                        Toast.makeText(TrackLocationActivity.this, "Location not available !", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("GpsData", volleyError.toString());
                         //  pDialog.dismiss();
                            try {
                                getAssignStopToRouteApi(vehicleRouteId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest,"lastLocation");
        } else {
            Toast.makeText(TrackLocationActivity.this, "Location not available !", Toast.LENGTH_SHORT).show();
            if (isNeededToRecreateRoute)
                getAssignStopToRouteApi(vehicleRouteId);

        }
    }

    private void addNewMarker() {
        if (lastCurrentLocationList.size() > 1) {
            Marker marker1 = mMap.addMarker(new MarkerOptions().position(lastCurrentLocationList.get(lastCurrentLocationList.size() - 2))
                    .title("")
                   .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                  //  .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_markar_moving_path)));
            marker1.showInfoWindow();


            LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
            if (marker1 != null)
                builder.include(marker1.getPosition());
           // Toast.makeText(TrackLocationActivity.this, "Add", Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
/*
        // Add a marker in Sydney and move the camera
        LatLng junction = new LatLng(23.235771, 77.434591);
        //  LatLng junction  =new LatLng(23.235771, 77.434591);
        mMap.addMarker(new MarkerOptions().position(new LatLng(21.146633, 79.088860)).title("Nagpur")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
        //  m1.setDraggable(true);

        mMap.addMarker(new MarkerOptions().position(new LatLng(22.719568, 75.857727)).title("Indore")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue)));
        //   m2.setDraggable(true);
        // mMap.moveCamera(CameraUpdateFactory.newLatLng(junction));

        mMap.addMarker(new MarkerOptions().position(new LatLng(21.910803, 77.901184)).title("Betul")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_maps_directions_car)));
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(21.910803, 77.901184))
                .zoom(7).build();
      *//*  if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
      //  mMap.setMyLocationEnabled(true);*//*
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        getDirection();*/

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //   mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        //  mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        // setMarkerAndCamere();
        //  getDirection();


        //for display route

        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(23.082567, 80.070910))
                .zoom(1).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));


    }

    private void getDirection(String route_url_default, LatLng latlong_start, LatLng latlong_des, ArrayList<LatLng> waypoint_list) {
        //Getting the URL

        /*route_url = makeURL(22.719568, 75.857727,
                21.146633, 79.088860); // for testing*/
        if (!"default".equalsIgnoreCase(route_url_default) && (latlong_start == null || latlong_des == null)) {
            route_url = route_url_default;
        } else {


            route_url = makeURL(latlong_start.latitude, latlong_start.longitude,
                    latlong_des.latitude, latlong_des.longitude, waypoint_list);

        }

        Log.e("route_url", route_url);
        //Showing a dialog till we get the route
        final ProgressDialog loading = ProgressDialog.show(this, "Getting Route", "Please wait...", false, false);

        //Creating a string request
        StringRequest stringRequest = new StringRequest(route_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        //Calling the method drawPath to draw the path
                        //   Log.e("vishal_log",response);
                        DbHandler.longInfo(response);

                        drawPath(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        Log.e("vishal_log", "error");
                    }
                });

        //Adding the request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void makUrlList(ArrayList<LatLng> waypoint_list) {


        ArrayList<LatLng> newList = new ArrayList<>();
        LatLng lastLoc = null;
        if (waypoint_list.size() > 0) {
            for (int i = 0; i < waypoint_list.size(); i++) {

                if (newList.size() > (lastLoc == null ? 8 : 7) || i == waypoint_list.size() - 1) {
                    ArrayList<LatLng> bb = new ArrayList<>();
                    if (newList.size() >= 3) {
                        for (int j = 0; j < newList.size(); j++) {
                            if (j != 0 && j != newList.size())
                                bb.add(newList.get(j));
                        }
                    } else bb.addAll(newList);

                    if (newList.size() >= 2) {
                        getDirection("default", lastLoc == null ? newList.get(0) : lastLoc, newList.get(newList.size() - 1), bb);
                        lastLoc = newList.get(newList.size() - 1);
                    } else if (waypoint_list.size() >= 2)
                        getDirection("default", waypoint_list.get(waypoint_list.size() - 2), waypoint_list.get(waypoint_list.size() - 1), new ArrayList<LatLng>());


                    newList.clear();
                } else newList.add(waypoint_list.get(i));

            }

        }

    }

    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog, ArrayList<LatLng> waypoint_list) {


        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        if (waypoint_list.size() > 0) {
            urlString.append("&waypoints=optimize:true");

            for (LatLng latLong : waypoint_list) {
                urlString.append("|");
                urlString.append(Double.toString(latLong.latitude));
                urlString.append(",");
                urlString.append(Double.toString(latLong.longitude));
            }
        }

       /* if (latlong_waypoint != null) {
            urlString.append("&waypoints=optimize:true|");
            urlString.append(latlong_waypoint.latitude);
            urlString.append(",");
            urlString.append(latlong_waypoint.longitude);
        }*/

      /*  if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            urlString.append("&waypoints=optimize:true");
            for (int init = 0; latlong_waypoint_list.size() > init; init++) {
                LatLng l = latlong_waypoint_list.get(init);
                urlString.append("|");
                urlString.append(l.latitude);
                urlString.append(",");
                urlString.append(l.longitude);
            }
        }*/
        urlString.append("&sensor=false&mode=driving&alternatives=false"); //*&sensor=false&mode=driving&alternatives=true*/
        //  urlString.append("&key=SERVER-KEY");

        Log.d("URLSTRING", urlString.toString());
        return urlString.toString();

      /*  return
                "https://maps.googleapis.com/maps/api/directions/json?" +
                        "origin=23.231116,77.433592" +
                        "&destination=23.302777,77.402655" +
                        "&waypoints=optimize:true" +
                        "|23.247223,77.444381" +       // govindpura
                        "|23.249156,77.476120" +    // piplani
                        "|sehore" +    // sehore
                        "|23.255772,77.478451" +   // ayodhya bypass road
                        "|23.208038,77.444558" +  // habibganj
                        "|piplani" +  // piplani
                        "|Nadra+bus+stand" +  // bus stand

                        "&sensor=false";
*/
// return "https://maps.googleapis.com/maps/api/directions/json?origin=New+York,+NY&destination=Boston,+MA&waypoints=optimize:true|Providence,+RI|Hartford,+CT";

    }

    public void drawPath(String result) {
        //Getting both the coordinates
        //     LatLng from = new LatLng(fromLatitude,fromLongitude);
        //     LatLng to = new LatLng(toLatitude,toLongitude);

        //Calculating the distance in meters

        // Double distance = SphericalUtil.computeDistanceBetween(from, to);

        //Displaying the distance
        //  Toast.makeText(this,String.valueOf(distance+" Meters"),Toast.LENGTH_SHORT).show();


        try {
            //Parsing json
            final JSONObject json = new JSONObject(result);

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            //  try {
            jRoutes = json.getJSONArray("routes");
            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    PolylineOptions polyoption = new PolylineOptions();
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                .get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        polyoption.addAll(list);
                            /*Polyline line = mMap.addPolyline(new PolylineOptions()
                                    .addAll(list)
                                    .width(5)
                                    .color(Color.RED)
                                    .geodesic(true)  // // TODO: 1/12/2017 put this outside and add list one time rather create polylin each time
                            );*/

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat",
                                    Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng",
                                    Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    Polyline line = mMap.addPolyline(polyoption
                            .width(7)
                            .color(getColor(android.R.color.holo_green_dark))
                            .geodesic(true)  // // TODO: 1/12/2017 put this outside and add list one time rather create polylin each time
                    );
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        //  return routes;



           /* JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(20)
                    .color(Color.RED)
                    .geodesic(true)
            );


        }
        catch (JSONException e) {

        }*/
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void getAssignStopToRouteApi(final String route) throws JSONException {
        if (!TrackLocationActivity.this.isFinishing())
            pDialog.show();


        JSONObject jsonData = new JSONObject();
        jsonData.put("RouteID", route);
        JSONObject jsonFilter = new JSONObject();
        jsonFilter.put("filter", jsonData);


        String url = Gc.getSharedPreference(Gc.ERPHOSTAPIURL, this )
                + Config.applicationVersionNameValue
                + "AssignStopToRoute.php?databaseName=" + Gc.getSharedPreference(Gc.ERPDBNAME, this) +
                "&data=" + jsonFilter;

        Log.e("url", url);


        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET,
               /* "http://apischoolerp.zeroerp.com/MasterEntryApi.php?" +
                        "databaseName=A994A107&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",*/
                //todo routestoppage in url
                url, null
                // "http://192.168.1.161/apischoolerp/MasterEntryApi.php?databaseName=CBA98217&data={\"filter\":{\"MasterEntryName\":\"RouteStoppage\"}}",
                /*"http://192.168.1.161/apischoolerp/RouteStoppagesApi.php?databaseName=CBA98217&action=stopageMasterEntryById"*/,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("AssignStopToRoute", jsonObject.toString());
                        pDialog.dismiss();
                        String data = jsonObject.optString("code", "key not found");
                        if ("200".equalsIgnoreCase(data)) {

                            JSONArray jsonArray_result = jsonObject.optJSONArray("result");
                            int jsonArray_result_length;
                            if (jsonArray_result != null && (jsonArray_result_length = jsonArray_result.length()) > 0) {
                                stopDataItemList_route.clear();

                                //     stopDataItemList_route.add(new StopDataItem());

                                Gson gson = new Gson();
                                String jsonOutput = jsonArray_result.toString();
                                Type listType = new TypeToken<AssignStopData>() {
                                }.getType();

                                AssignStopData assinDataObj = (AssignStopData) gson.fromJson(jsonObject.toString(), listType);
                                stopDataItemList_route.addAll(assinDataObj.getResult());
                                createRouteByParam(stopDataItemList_route);

                                try {
                                    List<LatLng> midpoints = null;
                                    midStopList.clear();

                                /*    if (stopDataItemList_route.size() > 2)
                                        midpoints = new ArrayList<>(stopDataItemList_route.size() - 2);
                                    for (int i = 0; stopDataItemList_route.size() > i; i++) {
                                        AssignStopData stopDataItem = stopDataItemList_route.get(i);

                                        if (stopDataItem.getType().equalsIgnoreCase("Source")) {
                                            Log.e("Latlong", "Lat = " + stopDataItem.getStopLat() + " Long = " + stopDataItem.getStopLong());
                                          //  latlong_start = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                                            // LocationMapsActivity.this.start_title = stopDataItem.getMasterEntryValue();

                                            startAssignStopDataObj = stopDataItem;

                                        } else if (stopDataItem.getType().equalsIgnoreCase("Destination")) {
                                           // latlong_destination = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                                            //  LocationMapsActivity.this.destination_title = stopDataItem.getMasterEntryValue();

                                            destinationAssignStopDataObj = stopDataItem;
                                        } else {

                                            midpoints.add(new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong())));
                                            address_waypoint_list.add(stopDataItem.getMasterEntryValue());
                                            //  time_waypoint_list.add(stopDataItem.getMasterEntryValue());
                                            time_waypoint_list.add(stopDataItem.getStopTime());

                                            stopDataItemList_stop.add(stopDataItem);

                                        }

                                    }
                                    pDialog.dismiss();
                                    createRouteByParam(latlong_start, latlong_destination, midpoints, start_title, start_snippet, destination_title, destination_snippet);
                                    latlong_waypoint_list.clear();
                                    if (midpoints != null)
                                        latlong_waypoint_list.addAll(midpoints);*/
                                    //     getDirection(route_url);

                                } catch (Exception e) {
                                    Toast.makeText(TrackLocationActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                }

                            }
                            pDialog.cancel();
                            //   stopDataAdapter.notifyDataSetChanged();
                        }


                    }
                }

                , new Response.ErrorListener()

        {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Log.e("TAG", "Error: " + error.getMessage());
                String err = error.getMessage();
                if (error instanceof NoConnectionError) {
                    err = "No Internet Access\nCheck Your Internet Connection.";
                }
                Config.showToast(TrackLocationActivity.this, err);
                //displayAlertDialogForRouteSave();
            }

        }

        );
        strReq.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(TrackLocationActivity.this).addToRequestQueue(strReq, "AssignStopToRoute");

    }

    private void createRouteByParam(ArrayList<AssignStopData> assignDataRoutesList) {

        mMap.clear();

        Marker marker1 = null, marker2 = null;
        LatLng latlong_start = null, latlong_des = null;
        ArrayList<Marker> marker__waypoint_list = new ArrayList<>();
        ArrayList<LatLng> waypoint_list = new ArrayList<>();
        for (AssignStopData stopDataItem : assignDataRoutesList) {
            if (null != stopDataItem.getType() && stopDataItem.getType().equalsIgnoreCase("Source")) {
                latlong_start = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                marker1 = mMap.addMarker(new MarkerOptions().position(latlong_start)
                        .anchor(0.5f, 0.5f)
                        .title(stopDataItem.getRouteStoppageName() == null ? "Source" : stopDataItem.getRouteStoppageName())
                        .snippet(stopDataItem.getTime() == null ? "00:00" : stopDataItem.getTime())
                        //  .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green)));
                marker1.showInfoWindow();
            } else if (null != stopDataItem.getType() && stopDataItem.getType().equalsIgnoreCase("Destination")) {
                latlong_des = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                marker2 = mMap.addMarker(new MarkerOptions().position(latlong_des).title(stopDataItem.getRouteStoppageName() == null ? "Source" : stopDataItem.getRouteStoppageName())
                        .anchor(0.5f, 0.5f)
                        .snippet(stopDataItem.getTime() == null ? "00:00" : stopDataItem.getTime())
                        //  .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)));
                marker2.showInfoWindow();
            } else {
                LatLng latlong_mid = new LatLng(Double.parseDouble(stopDataItem.getStopLat()), Double.parseDouble(stopDataItem.getStopLong()));
                waypoint_list.add(latlong_mid);
                marker__waypoint_list.add(mMap.addMarker(new MarkerOptions().position(latlong_mid)
                        .anchor(0.5f, 0.5f)
                        .title(stopDataItem.getTime())
                        .snippet(stopDataItem.getRouteStoppageName())
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false)));
            }
        }
        if (latlongLastUpdatedLocation != null) {
            LatLng latLongGpsData = new LatLng(Double.parseDouble(latlongLastUpdatedLocation.getLat()), Double.parseDouble(latlongLastUpdatedLocation.getLng()));

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.location_blink);
            markerLastUpdatedLoc = new BlinkingMarker(bitmap, mMap, 10, 1000);
            markerLastUpdatedLoc.addToMap(new LatLng(latLongGpsData.latitude, latLongGpsData.longitude));
            markerLastUpdatedLoc.startBlinking();
          /*  markerLastUpdatedLoc = mMap.addMarker(new MarkerOptions().position(latLongGpsData).title(latlongLastUpdatedLocation.getTime() == null ? "Current" : latlongLastUpdatedLocation.getTime())
                    .snippet(latlongLastUpdatedLocation.getType() == null ? "Vehicle" : latlongLastUpdatedLocation.getType())
                     // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                  .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_blink)));
           */
            //markerLastUpdatedLoc.showInfoWindow();
        }


    /*  ArrayList<LatLng> aaa =  new ArrayList<LatLng>();
        aaa.add(latlong_start);
        aaa.addAll(waypoint_list);  // for making route which waypoints is greater than 8
        aaa.add(latlong_des);
        makUrlList(aaa);*/
        //getDirection("default", latlong_start, latlong_des, waypoint_list);


       /* ArrayList<Marker> marker__waypoint_list = null;
        if (latlong_waypoint_list != null && !latlong_waypoint_list.isEmpty()) {
            marker__waypoint_list = new ArrayList<>(latlong_waypoint_list.size());
            final int latlong_waypoint_list_size = latlong_waypoint_list.size();

            for (int init = 0; latlong_waypoint_list_size > init; init++) {
                marker__waypoint_list.add(mMap.addMarker(new MarkerOptions().position(latlong_waypoint_list.get(init))
                        .title(time_waypoint_list.get(init))
                        .snippet(address_waypoint_list.get(init))
                        // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
                        .draggable(false)));
            }

        }*/

     /*   //       For multiple marker in map
        Marker marker3 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.247223, 77.444381)).title("Govind Pura")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker4 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.249156, 77.476120)).title("Piplani Raisen Road")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Marker marker0 = mMap.addMarker(new MarkerOptions().position(new LatLng(23.205012, 77.085078)).title("Sehore")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));*/


        //     mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //      mMap.setMyLocationEnabled(true);
       /* CameraPosition position = new CameraPosition.Builder()
                .target(latlong_start)
                .target(latlong_destination)
                .zoom(13).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/


        // TODO: 1/12/2017 show all marker without use zoom functionality, but in case of bus we use zoom functionality see above
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
        if (marker1 != null)
            builder.include(marker1.getPosition());
        // builder.include(marker0.getPosition());

        if (marker2 != null) builder.include(marker2.getPosition());
        if (markerLastUpdatedLoc != null) {
            // builder.include(markerLastUpdatedLoc.getPosition());
            //   markerLastUpdatedLoc.addToMap(BUDAPEST);
        }
       /* if (marker3 != null) {
            builder.include(marker3.getPosition());
        }*/
        //  builder.include(marker3.getPosition());
        //  builder.include(marker4.getPosition());


        if (marker__waypoint_list != null) {
            for (int init = 0; marker__waypoint_list.size() > init; init++) {
                builder.include(marker__waypoint_list.get(init).getPosition());
            }
        }


        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        //int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.35);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);  // mMap.moveCamera;
       /* float currentZoom = mMap.getCameraPosition().zoom;
        //   And if this level is greater than 16, which it will only be if there are very less markers or all the markers are very close to each other, then simply zoom out your map at that particular position only by seting the zoom level to 16.
        if(currentZoom>16)
            mMap.moveCamera(CameraUpdateFactory.zoomTo(16));  // animate camera take time i.e. this will not work try to change animate camera to move, problem will solve by setOncameraIdleListener setting by test this condition

*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        pDialog.dismiss();
        AppRequestQueueController.getInstance(this).cancelRequestByTag("lastLocation");
        AppRequestQueueController.getInstance(this).cancelRequestByTag("AssignStopToRoute");
       handler.removeCallbacks(runnable);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        pDialog.dismiss();
        AppRequestQueueController.getInstance(this).cancelRequestByTag("lastLocation");
        AppRequestQueueController.getInstance(this).cancelRequestByTag("AssignStopToRoute");
        handler.removeCallbacks(runnable);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int ids = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (ids == R.id.refresh) {
            try {

                getIMEIOfSelectedRoute(vehicleRouteId,/*don't recrete route*/false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
