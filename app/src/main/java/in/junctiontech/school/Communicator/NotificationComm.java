package in.junctiontech.school.Communicator;

import java.util.ArrayList;

import in.junctiontech.school.models.NotificationEvent;

/**
 * Created by JAYDEVI BHADE on 10/10/2016.
 */

public interface NotificationComm {

    public void setData(ArrayList<NotificationEvent> data);
    public void updateNotification(NotificationEvent notification);
}
