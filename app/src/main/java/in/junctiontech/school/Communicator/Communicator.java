package in.junctiontech.school.Communicator;

import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;

/**
 * Created by JAYDEVI BHADE on 10/7/2016.
 */

public interface Communicator {
    public void setBasicDetail(SchoolDetailsEntity obj);
    public void setSchoolAddress(SchoolDetailsEntity obj);
    public void setSchoolLogo(SchoolDetailsEntity obj);
    public void callPrevious();
    public void callNext();

}
