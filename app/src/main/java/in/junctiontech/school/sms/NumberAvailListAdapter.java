package in.junctiontech.school.sms;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.SMSContact;

/**
 * Created by JAYDEVI BHADE on 13/1/2017.
 */

public class NumberAvailListAdapter extends RecyclerView.Adapter<NumberAvailListAdapter.MyViewHolder> {
    private final int[][]  states;
    private final int[]  thumbColors;
    private final int[]  trackColors;
    private ArrayList<SMSContact> smsContactArrayList;
    private Context context;
    private boolean updateData = false;
    private int colorIs;

    public NumberAvailListAdapter(Context context, ArrayList<SMSContact> smsContactArrayList, boolean updateData, int colorIs) {
        this.smsContactArrayList = smsContactArrayList;
        this.context = context;
        this.colorIs = colorIs;
        this.updateData =updateData;
         states = new int[][] {
                new int[] {-android.R.attr.state_checked},
                new int[] {android.R.attr.state_checked},
        };

         thumbColors = new int[] {
                Color.LTGRAY,
                colorIs,
        };

          trackColors = new int[] {
                Color.GRAY,
                Color.LTGRAY,
        };
    }


    @Override
    public NumberAvailListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sms_list_number_availability, parent, false);

        return new NumberAvailListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final NumberAvailListAdapter.MyViewHolder holder, final int position) {
        SMSContact obj = (SMSContact) smsContactArrayList.get(position);

        holder.sms_switch_compact_name.setText(obj.getName());


        if (obj.getMobileHolderNumber()==null|| obj.getMobileHolderNumber().equalsIgnoreCase("")) {
            holder.sms_switch_compact_name.setEnabled(false);
            holder.sms_switch_compact_name.setChecked(false);
            holder.tv_number.setText(obj.getMobileHolderType() + " : " +
                    context.getString(R.string.mobile_number_not_available));
        } else {


            if (obj.isSmsSendStatus())
                holder.sms_switch_compact_name.setChecked(true);
            else  holder.sms_switch_compact_name.setChecked(false);

            holder.sms_switch_compact_name.setEnabled(true);

            holder.tv_number.setText(obj.getMobileHolderType() + " : " +
                    obj.getMobileHolderNumber());

            if (holder.sms_switch_compact_name.isChecked())
                obj.setSmsSendStatus(true);
            else obj.setSmsSendStatus(false);
        }




    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return smsContactArrayList.size();
    }

    public ArrayList<SMSContact> getListOfContacts() {
        return this.smsContactArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_number;
        private SwitchCompat sms_switch_compact_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            sms_switch_compact_name = (SwitchCompat) itemView.findViewById(R.id.sms_switch_compact_name);
            tv_number = (TextView) itemView.findViewById(R.id.sms_switch_compact_number);
            setSwitchConpactColor(sms_switch_compact_name );
            sms_switch_compact_name.setTextColor(colorIs);
            sms_switch_compact_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    smsContactArrayList.get(getLayoutPosition()).setSmsSendStatus(b);
                }
            });

            //    itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }
    private void  setSwitchConpactColor(SwitchCompat switchButton ) {
        DrawableCompat.setTintList(DrawableCompat.wrap(switchButton.getThumbDrawable()), new ColorStateList(states, thumbColors));
        DrawableCompat.setTintList(DrawableCompat.wrap(switchButton.getTrackDrawable()), new ColorStateList(states, trackColors));

    }
}

