package in.junctiontech.school.sms;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;
import in.junctiontech.school.models.CreateClass;
import in.junctiontech.school.models.SMSContact;
import in.junctiontech.school.models.StaffDetail;
import in.junctiontech.school.models.StudentData;

/**
 * Created by JAYDEVI BHADE on 1/4/2017.
 */

public class SmsContactListActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private boolean selectedTab;
    private DbHandler db;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private boolean isDialogShowing = false;
    private SharedPreferences sp;
    private String dbName;
    private Gson gson;
    private Type type;
    private ArrayList<CreateClass> classListData = new ArrayList<>();
    private ArrayList<StudentData> classListStudentModelList = new ArrayList<>();
    private SmsStudentListFragment studentListFragment;
    private SmsParentListFragment parentListFragment;
    private SmsTeacherListFragment teacherListFragment;
    private Type typeStaffDetail;

    private ArrayList<StaffDetail> staffListData = new ArrayList<>();
    private ArrayList<StudentData> staffListStudentModelList = new ArrayList<>();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean logoutAlert = false;
    private FrameLayout snackbar;
    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_student2);
        snackbar = (FrameLayout) findViewById(R.id.snackbar_view_pager);

        sp = Prefs.with(this).getSharedPreferences();
        dbName = sp.getString("organization_name", "");
        gson = new Gson();
        type = new TypeToken<CreateClass>() {
        }.getType();
        typeStaffDetail = new TypeToken<StaffDetail>() {
        }.getType();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);


        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
//                    case 0:
//                        chatListFragment.setMenuVisibility(true);
//                        contactListFragment.setCustumMenuVisibility(false);
//                        getSupportActionBar().setTitle(getString(R.string.chats));
//                        break;
//
//                    case 1:
//                        chatListFragment.setMenuVisibility(false);
//                        contactListFragment.setFirstMenuVisibility(true);
//                        contactListFragment.setCustumMenuVisibility(true);
//                        getSupportActionBar().setTitle(getString(R.string.contacts) + " " +
//                                getString(R.string.list));
//                        break;
//                    case 2:
//                        chatListFragment.setMenuVisibility(false);
//                        contactListFragment.setFirstMenuVisibility(false);
//                        contactListFragment.setCustumMenuVisibility(false);
//                        getSupportActionBar().setTitle(getString(R.string.class_text) + " " +
//                                getString(R.string.list));
//                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(colorIs);
        getAllClassAndSection();

        getStaffDataFromServer();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
                    if (classListData.size() == 0) {
                        getAllClassAndSection();
                    }
                    if (staffListData.size() == 0) {
                        getStaffDataFromServer();
                    }

                }

            }
        };
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));

        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

        super.onPause();
    }

    public void getStaffDataFromServer() {

       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing && !isFinishing()) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }
        } else {*/
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffApi.php?&action=join&databaseName=" +
                dbName;

        Log.e("getStaffData", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url

                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStaffData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                StaffDetail obj = gson.fromJson(s, typeStaffDetail);
                                staffListData = obj.getResult();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(SmsContactListActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }
                            for (StaffDetail staffDetail : staffListData) {
                                staffListStudentModelList.add(new StudentData(
                                        staffDetail.getStaffName(),
                                        staffDetail.getMasterEntryValue()
                                ));

                            }

                            if (!isFinishing() && (viewPager.getCurrentItem() == 1
                                    || viewPager.getCurrentItem() == 2)) {

                                teacherListFragment.updateStaffList(staffListStudentModelList);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStaffData", volleyError.toString());
                progressbar.cancel();
                Config.responseVolleyErrorHandler(SmsContactListActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        //}
    }

    private void setupViewPager(ViewPager viewPager) {
        SmsContactListActivity.ViewPagerAdapter adapter = new SmsContactListActivity.
                ViewPagerAdapter(getSupportFragmentManager());

        parentListFragment = new SmsParentListFragment();
        adapter.addFragment(parentListFragment, getString(R.string.parent));

        studentListFragment = new SmsStudentListFragment();
        adapter.addFragment(studentListFragment, getString(R.string.student));


        teacherListFragment = new SmsTeacherListFragment();
        adapter.addFragment(teacherListFragment, getString(R.string.staff));

        viewPager.setAdapter(adapter);

    }

    private void showSnack() {
        Snackbar snackbarObj = null;
        if (classListData.size() == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.classes_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                Toast.makeText(SmsContactListActivity.this,"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                            }else {   startActivity(new Intent(SmsContactListActivity.this, ClassSectionActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });
        } else if (classListData.size() == 0) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.staff_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_staff),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_STAFF_PERMISSION,false)) {
                                Toast.makeText(SmsContactListActivity.this,"Permission not available : CREATE STAFF !",Toast.LENGTH_SHORT).show();
                            }else {
//                                startActivity(new Intent(SmsContactListActivity.this, CreateStaffActivity.class));
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                            }
                        }
                    });
        }
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();
    }


    public ArrayList<StudentData> getClassList() {
        return classListStudentModelList;
    }

    public ArrayList<StudentData> getStaffList() {
        if (staffListStudentModelList.size() > 0) {
            return staffListStudentModelList;
        } else {

            if (staffListData.size() > 0) {
                for (StaffDetail staffDetail : staffListData) {
                    staffListStudentModelList.add(new StudentData(
                            staffDetail.getStaffName(),
                            staffDetail.getMasterEntryValue()
                    ));

                }
            }
            return staffListStudentModelList;
        }

    }

    public void startSMS(StudentData studentData, String userType) {
        if (userType.equalsIgnoreCase("Teacher")) {
            ArrayList<SMSContact> smsContactList = new ArrayList<>();
            StaffDetail obj = staffListData.get(staffListStudentModelList.indexOf(studentData));
            smsContactList.add(new SMSContact(obj.getStaffId(),
                    obj.getStaffName(),
                    "Teacher",
                    obj.getStaffMobile()));

            startActivity(new Intent(this, SmsSendActivity.class).
                    putExtra("userType", userType).
                    putExtra("data", smsContactList));
        } else {

            startActivity(new Intent(this, SmsSendActivity.class).
                    putExtra("userType", userType).
                    putExtra("data", studentData));

        }

    }

    public void fetchClassSectionList() {
        getAllClassAndSection();
    }

    public int getAppColor() {
        return colorIs;
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getAllClassAndSection() {
      /*  if (!Config.checkInternet(this)) {
            if (!isDialogShowing && !isFinishing()) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }
        } else {*/
        progressbar.show();
        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "ClassApi.php?databaseName=" + dbName +
                "&action=join&session=" + sp.getString(Config.SESSION, "");
        Log.d("url", url);
        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getClassData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                CreateClass obj = gson.fromJson(s, type);
                                classListData = obj.getResult();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & ! isFinishing()) {
                                    Config.responseVolleyHandlerAlert(SmsContactListActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            }else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }
                            if (viewPager.getCurrentItem() == 0)
                                parentListFragment.closeSwipe();
                            else if (viewPager.getCurrentItem() == 1)
                                studentListFragment.closeSwipe();
                            else if (viewPager.getCurrentItem() == 2)
                                teacherListFragment.closeSwipe();

                            for (CreateClass cls : classListData) {
                                ArrayList<CreateClass> secList = cls.getSectionList();
                                for (CreateClass sec : secList)
                                    classListStudentModelList.add(
                                            new StudentData(cls.getClassName() + " " +
                                                    sec.getSectionName(), sec.getSectionId())
                                    );
                            }

                            if (viewPager.getCurrentItem() == 0 || viewPager.getCurrentItem() == 1) {
                                parentListFragment.updateList(classListStudentModelList);
                                studentListFragment.updateList(classListStudentModelList);
                            } else if (viewPager.getCurrentItem() == 1 || viewPager.getCurrentItem() == 2) {
                                studentListFragment.updateList(classListStudentModelList);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getClassData", volleyError.toString());
                progressbar.cancel();
                if (viewPager.getCurrentItem() == 0)
                    parentListFragment.closeSwipe();
                else if (viewPager.getCurrentItem() == 1)
                    studentListFragment.closeSwipe();
                else if (viewPager.getCurrentItem() == 2)
                    teacherListFragment.closeSwipe();


                Config.responseVolleyErrorHandler(SmsContactListActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        //}
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
