package in.junctiontech.school.sms;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.SMSContact;

public class NumberAvailActivity extends AppCompatActivity {


    private TextView tv_view_all_data_not_data_available;
    private  SwipeRefreshLayout swipe_refresh_list;
    private ArrayList<SMSContact> smsContactList;
    private RecyclerView recycler_view_list_of_data;
    private NumberAvailListAdapter adapter;
    private int  colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_all_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       setColorApp();
        recycler_view_list_of_data = (RecyclerView) findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available = (TextView) findViewById(R.id.tv_view_all_data_not_data_available);
        swipe_refresh_list =(SwipeRefreshLayout)findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipe_refresh_list.isRefreshing()) swipe_refresh_list.setRefreshing(false);
            }
        });

        smsContactList= (ArrayList<SMSContact>) getIntent().getSerializableExtra("data");
        setupRecycler();
        findViewById(R.id.fl_fragment_all_list).setBackgroundResource(R.color.backgroundColor);
    }

    @Override
    public boolean onSupportNavigateUp() {
        smsContactList = adapter.getListOfContacts();
        Intent intent = new Intent(this, SmsSendActivity.class);
        intent.putExtra("data", smsContactList);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        smsContactList = adapter.getListOfContacts();
        Intent intent = new Intent(this, SmsSendActivity.class);
        intent.putExtra("data", smsContactList);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new NumberAvailListAdapter(this, smsContactList, getIntent().getBooleanExtra("updateData",false),colorIs);
        recycler_view_list_of_data.setAdapter(adapter);
    }
}
