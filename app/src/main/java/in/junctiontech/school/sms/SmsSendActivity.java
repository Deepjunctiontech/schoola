package in.junctiontech.school.sms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.RegisteredStudent;
import in.junctiontech.school.models.SMSContact;
import in.junctiontech.school.models.StudentData;

import static android.app.PendingIntent.getBroadcast;

public class SmsSendActivity extends AppCompatActivity {
    private SharedPreferences sp;
    private ProgressDialog progressbar;
    private RadioButton rb_sms_api, rb_sms_mobile_plan;
    private TextView tv_sms_api_balance;
    private StudentData obj;
    private BroadcastReceiver sendBroadcastReceiver;
    private BroadcastReceiver deliveryBroadcastReceiver;
    // private ArrayList<PendingIntent> deliveredPIArrayList;
    // private ArrayList<PendingIntent> sentPIArrayList;
    // private ArrayList<String> mobileNumbers = new ArrayList<>();
    private EditText et_sms;
    private PendingIntent sentPI, deliveredPI;
    private boolean isDialogShowing;
    private AlertDialog.Builder alert;
    private String dbName;
    private Gson gson;

    private ArrayList<SMSContact> smsContactList = new ArrayList<>();

    private int SMSCode = 111;
    private TextView tv_sms;

    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
        ((TextView) findViewById(R.id.tv_sms_title)).setTextColor(colorIs);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        sp = Prefs.with(this).getSharedPreferences();
        dbName = sp.getString("organization_name", "");
        gson = new Gson();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);
        setColorApp();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setSubtitle(getString(R.string.user_type) + " : " +
                getIntent().getStringExtra("userType"));
        initViews();

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SmsSendActivity.this, NumberAvailActivity.class)
                        .putExtra("data", smsContactList)
                        .putExtra("updateData", true), SMSCode);
            }
        });

        et_sms = (EditText) findViewById(R.id.et_sms);
        tv_sms = (TextView) findViewById(R.id.tv_sms);

//        mobileNumbers.add("8823819994");
//        mobileNumbers.add("8889552622");
//        mobileNumbers.add("9584485416");

        if (getIntent().getStringExtra("userType").equalsIgnoreCase("Teacher")) {
            // getStaffDataFromServer();
            smsContactList = (ArrayList<SMSContact>) getIntent().getSerializableExtra("data");
        } else {
            obj = (StudentData) getIntent().getSerializableExtra("data");
            getRequestedMobileNumbers();
        }

    }

    private void initViews() {
        tv_sms_api_balance = (TextView) findViewById(R.id.tv_sms_api_balance);
        rb_sms_api = (RadioButton) findViewById(R.id.rb_sms_api);
        rb_sms_mobile_plan = (RadioButton) findViewById(R.id.rb_sms_mobile_plan);

        rb_sms_api.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                    tv_sms_api_balance.setVisibility(View.VISIBLE);
                else tv_sms_api_balance.setVisibility(View.GONE);
               /* if (!isChecked) {
                    rb_sms_api.setBackgroundResource(0);
                    rb_sms_api.setTextColor(getResources().getColor(R.color.ColorPrimaryDark));
                    tv_sms_api_balance.setVisibility(View.GONE);
                } else {
                    rb_sms_api.setBackgroundResource(R.color.selector_color);
                    rb_sms_api.setTextColor(Color.WHITE);
                    tv_sms_api_balance.setVisibility(View.VISIBLE);
                }*/
            }
        });
       /* rb_sms_mobile_plan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    tv_sms_api_balance.setVisibility(View.VISIBLE);
                else  tv_sms_api_balance.setVisibility(View.GONE);

               *//* if (!isChecked) {
                    rb_sms_mobile_plan.setTextColor(getResources().getColor(R.color.ColorPrimaryDark));
                    rb_sms_mobile_plan.setBackgroundResource(0);

                } else {
                    rb_sms_mobile_plan.setBackgroundResource(R.color.selector_color);
                    rb_sms_mobile_plan.setTextColor(Color.WHITE);
                }*//*
            }
        });*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    private void sendSMS(final ArrayList<SMSContact> phoneNumbers, final String message) {
        final String SENT = "SMS_SENT";
        final String DELIVERED = "SMS_DELIVERED";

        progressbar.setMessage(getString(R.string.sending));
        // progressbar.show();
        //---SMS has been sent---

        for (final SMSContact phoneNumber : phoneNumbers) {

            /*phoneNumber.setMobileHolderNumber((phoneNumber.getMobileHolderNumber().replaceAll("-","")).contains("+")
                    ?phoneNumber.getMobileHolderNumber().replaceAll("-",""):"+"+phoneNumber.getMobileHolderNumber().replaceAll("-",""));*/

            if (phoneNumber.getMobileHolderNumber() != null) {
                if (phoneNumber.getMobileHolderNumber() != "" && phoneNumber.isSmsSendStatus()) {

                    sentPI = getBroadcast(this, 0, new Intent(SENT), 0);
                    deliveredPI = getBroadcast(this, 0, new Intent(DELIVERED), 0);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendBroadcastReceiver = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context arg0, Intent arg1) {
                                    switch (getResultCode()) {
                                        case Activity.RESULT_OK:
//                                            Toast.makeText(getBaseContext(), "SMS sent " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
//                                                            + phoneNumber.getMobileHolderNumber(),
//                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("SMS sent ", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());

                                            phoneNumber.setDeliveryStatus(true);

                                            break;
                                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                            Toast.makeText(getBaseContext(), "Generic failure " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("Generic failure", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            break;
                                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                                            Toast.makeText(getBaseContext(), "No service " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("No service", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            break;
                                        case SmsManager.RESULT_ERROR_NULL_PDU:
                                            Toast.makeText(getBaseContext(), "Null PDU " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("Null PDU", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            break;
                                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                                            Toast.makeText(getBaseContext(), "Radio off " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("Radio off", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            break;
                                    }
                                }
                            };

                            //  sendBroadcastReceiverArrayList.add(senbroadcastReceiver);

                            registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));
                            //---when the SMS has been delivered---
                            deliveryBroadcastReceiver = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context arg0, Intent arg1) {
                                    switch (getResultCode()) {
                                        case Activity.RESULT_OK:
                                            Toast.makeText(getBaseContext(), "SMS delivered " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("SMS delivered", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            phoneNumber.setDeliveryStatus(true);
                                            break;
                                        case Activity.RESULT_CANCELED:
                                            Toast.makeText(getBaseContext(), "SMS not delivered " + phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                            + phoneNumber.getMobileHolderNumber(),
                                                    Toast.LENGTH_SHORT).show();
                                            Log.e("SMS not delivered", phoneNumber.getName() + " " + phoneNumber.getMobileHolderType()
                                                    + phoneNumber.getMobileHolderNumber());
                                            break;
                                    }
                                }
                            };
                            //   deliveryBroadcastReceiverArrayList.add(deliverybroadcastReceiver);

                            registerReceiver(deliveryBroadcastReceiver, new IntentFilter(DELIVERED));

                            SmsManager sms = SmsManager.getDefault();
                           /* sms.sendTextMessage( phoneNumber.getMobileHolderNumber(), null, message, sentPI, deliveredPI);*/

                            ArrayList<PendingIntent> sentPIArray = new ArrayList<PendingIntent>();
                            sentPIArray.add(sentPI);
                            ArrayList<PendingIntent> deliveredPIArray = new ArrayList<PendingIntent>();
                            deliveredPIArray.add(deliveredPI);

                            String num = (phoneNumber.getMobileHolderNumber().contains("+") ? phoneNumber.getMobileHolderNumber() : "+" + phoneNumber.getMobileHolderNumber()).replaceAll("-", "");
                            // Toast.makeText(SmsSendActivity.this,num,Toast.LENGTH_SHORT).show();

                            ArrayList<String> texts = sms.divideMessage(message);
                          /*  sms.sendMultipartTextMessage( "8823819994", null, sms.divideMessage("aaaaaaaaaaaa"),
                                    sentPIArray , deliveredPIArray);
                            sms.sendMultipartTextMessage( "+918823819994", null, sms.divideMessage("bbbbbbbbbb"),
                                    sentPIArray , deliveredPIArray);
                            sms.sendMultipartTextMessage( "+91-8823819994", null, sms.divideMessage("ccccccccc"),
                                    sentPIArray , deliveredPIArray);*/
                            sms.sendMultipartTextMessage(num, null, texts,
                                    sentPIArray, deliveredPIArray);
                        }
                    }, 1000);


                }
            }


        }


    }

    @Override
    protected void onStop() {
        try {
            if (sentPI != null) {
                //  for (BroadcastReceiver receiver : sendBroadcastReceiverArrayList) {
                if (sendBroadcastReceiver != null)
                    unregisterReceiver(sendBroadcastReceiver);

                //   }
                sentPI = null;

            }

            if (deliveredPI != null) {
                //     for (BroadcastReceiver receiver : deliveryBroadcastReceiverArrayList) {
                if (deliveryBroadcastReceiver != null)
                    unregisterReceiver(deliveryBroadcastReceiver);
                //  }
                deliveredPI = null;
            }
        } catch (Exception e) {
            Log.e("ExceptionStop", e.getMessage());
        }
        super.onStop();
    }

    public void sendSMS(View view) {

        if (rb_sms_mobile_plan.isChecked()) {

            if (
                    ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED

                    ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{
                            Manifest.permission.SEND_SMS

                    }, 100);
                }
            } else {
                afterPermissionGrantedOfSendSMS();
            }
        } else {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.you) + " " +
                        getString(R.string.send_sms_via) + " API & " + getString(R.string.you_have_not_enough_balance_to_send_this_sms));
                alert.show();
                isDialogShowing = true;
            }
        }

    }

    private void afterPermissionGrantedOfSendSMS() {
        if (et_sms.getText().toString().length() > 0) {
            AlertDialog.Builder alertSms = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alertSms.setIcon(getResources().getDrawable(R.drawable.ic_coins));
            alertSms.setTitle(getString(R.string.sms_cost_alert));
            alertSms.setMessage(getString(R.string.sms_alert));
            alertSms.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    tv_sms.setVisibility(View.VISIBLE);
                    tv_sms.setText(et_sms.getText().toString());

                    sendSMS(smsContactList, et_sms.getText().toString());
                    et_sms.setText("");
                }
            });
            alertSms.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertSms.show();
        } else Toast.makeText(this, getString(R.string.please_write_some_text_to_send_sms),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            afterPermissionGrantedOfSendSMS();
        } else if (requestCode == 100 && resultCode == RESULT_CANCELED) {
            AlertDialog.Builder alertPermission = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
            alertPermission.setMessage(getString(R.string.if_you_deny_this_permission_you_will_not_be_able_to_send_sms));
            alertPermission.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    sendSMS(et_sms);
                }
            });
            alertPermission.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertPermission.setCancelable(false);
            alertPermission.show();
        } else if (requestCode == SMSCode && resultCode == RESULT_OK) {
            setUpSmsContactList((ArrayList<SMSContact>) data.getSerializableExtra("data"));

        }
    }


    public void setUpSmsContactList(ArrayList<SMSContact> listData) {

        int totalCount = 0;
        int i = 0;
        for (SMSContact smsContact : listData) {
            smsContactList.get(i).setSmsSendStatus(smsContact.isSmsSendStatus());
            i++;


            if (smsContact.getMobileHolderNumber() != null &&
                    smsContact.getMobileHolderNumber().length() > 9 && smsContact.isSmsSendStatus())
                totalCount++;
        }

        if (!getIntent().getStringExtra("userType").equalsIgnoreCase("Teacher")) {
            getSupportActionBar().setTitle(getString(R.string.sms) +
                    " : " + obj.getStudentName() + " "
                    + "(" + totalCount + "/" + smsContactList.size() + ")");
        }
    }

    public void getRequestedMobileNumbers() {

        /*if (!Config.checkInternet(this)) {
            if (!isDialogShowing && !isFinishing()) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }
        } else {*/
        progressbar.show();


        final Map<String, String> param = new LinkedHashMap<String, String>();

        param.put("SectionId", obj.getStudentID());
        param.put("Status", "Studying");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String url = "";
        url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StudentRegistrationApi.php?" +
                "databaseName=" + sp.getString("organization_name", "not found") +
                "&action=sectionWise&data=" + new JSONObject(param1);


        Log.e("url", url);

        StringRequest request = new StringRequest(Request.Method.GET,
                url
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("DataSMS", s);
                        progressbar.cancel();
                        if (s != null || !s.equalsIgnoreCase("")) {
                            try {
                                JSONObject jsonObject = new JSONObject(s);

                                if (jsonObject.optString("code").equalsIgnoreCase("200")) {
                                    RegisteredStudent registerStudentList = gson.fromJson(s,
                                            new TypeToken<RegisteredStudent>() {
                                            }.getType());
                                    ArrayList<RegisteredStudent> studentArrayList = registerStudentList.getResult();
                                    smsContactList = new ArrayList<>();
                                    for (RegisteredStudent obj : studentArrayList) {

                                        switch (getIntent().getStringExtra("userType")) {
                                            case "Student":
                                                smsContactList.add(new SMSContact(
                                                        obj.getRegistrationId(),
                                                        obj.getStudentName(),
                                                        getIntent().getStringExtra("userType"),
                                                        obj.getMobile()
                                                ));
                                                break;

                                            case "Parent":
                                                smsContactList.add(new SMSContact(
                                                        obj.getRegistrationId(),
                                                        obj.getStudentName(),
                                                        "Father",
                                                        obj.getFatherMobile()
                                                ));
                                                smsContactList.add(new SMSContact(
                                                        obj.getRegistrationId(),
                                                        obj.getStudentName(),
                                                        "Mother",
                                                        obj.getMotherMobile()
                                                ));
                                                break;

                                        }


                                        for (SMSContact smsContactObj : smsContactList) {
                                            if (smsContactObj.getMobileHolderNumber() != null
                                                    && smsContactObj.getMobileHolderNumber().length() > 9) {
                                                smsContactObj.setSmsSendStatus(true);

                                            }
                                        }
                                        setUpSmsContactList(smsContactList);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("DataSMS", volleyError.toString());
                progressbar.dismiss();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                    if (!isDialogShowing) {
                        alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                        alert.show();
                        isDialogShowing = true;
                    }
                } else if (volleyError instanceof AuthFailureError) {
                    //TODO
                } else if (volleyError instanceof ServerError) {
                    //TODO
                } else if (volleyError instanceof NetworkError) {
                    //TODO
                } else if (volleyError instanceof ParseError) {
                    //TODO
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("json", new JSONObject(param).toString());
                Log.e("DataSMS", (new JSONObject(params).toString()));
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);



           /* Map<String, String> param = new LinkedHashMap<String, String>();
            param.put("SectionId", obj.getStudentID());
            param.put("type", getIntent().getStringExtra("userType"));

            final Map<String, JSONObject> param1 = new LinkedHashMap<>();
            param1.put("filter", (new JSONObject(param)));


            String  url=sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    +
                    "UserApi.php?databaseName=" +
                    dbName + "&session=" + sp.getString(Config.SESSION, "") +
                    "&action=join&data=" + (new JSONObject(param1));


            Log.e("SMS_URL", url);

            StringRequest request = new StringRequest(Request.Method.GET,
//                    "http://192.168.1.151/apischoolerp/StudentRegistrationApi.php?databaseName=" +
//                            dbName
                    url
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getStudentData", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {
                                    SMSContact smsContact = gson.fromJson(s, new TypeToken<SMSContact>() {
                                    }.getType());
                                    smsContactList = smsContact.getResult();
                                    getSupportActionBar().setTitle(getString(R.string.sms) +
                                            " : " + obj.getStudentName() + " "
                                            + jsonObject.optString("totalCount"));

                                    for (SMSContact smsContactObj : smsContactList) {
                                        if (smsContactObj.getMobileHolderNumber()!=null) {
                                            if (!smsContactObj.getMobileHolderNumber().equalsIgnoreCase(""))
                                                smsContactObj.setSmsSendStatus(true);
                                        }
                                    }


                                } else {
//                                    alert.setMessage(jsonObject.optString("message"));
//                                    alert.show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getStudentData", volleyError.toString());
                    progressbar.cancel();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);*/
        //}

    }

   /* public void getStaffDataFromServer() {

        if (!Config.checkInternet(this)) {
            if (!isDialogShowing && !isFinishing()) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }
        } else {
            progressbar.show();
            String url = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "StaffApi.php?databaseName=" +
                    dbName;

            Log.e("getStaffData",
                    url);
            StringRequest request = new StringRequest(Request.Method.GET,
                    url

                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            DbHandler.longInfo(s);
                            Log.e("getStaffData", s);
                            progressbar.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.optInt("code") == 200) {
                                    StaffDetail obj = gson.fromJson(s, new TypeToken<StaffDetail>() {
                                    }.getType());

                                    for (StaffDetail staffDetail : obj.getResult()) {
                                        smsContactList.add(new SMSContact(staffDetail.getStaffId(),
                                                staffDetail.getStaffName(),
                                                "Teacher",
                                                staffDetail.getStaffMobile()));
                                    }
                                } else {
                                    //   alert.setMessage(jsonObject.optString("message"));
                                    //   alert.show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("getStaffData", volleyError.toString());
                    progressbar.cancel();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        }
    }*/


}
