package in.junctiontech.school.sms;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.StudentData;

/**
 * Created by JAYDEVI BHADE on 1/4/2017.
 */

public class SmsStudentListAdapter  extends RecyclerView.Adapter<SmsStudentListAdapter.MyViewHolder> {
    private ArrayList<StudentData> studentList;

    private Context context;
    private int appColor;

    public SmsStudentListAdapter(Context context, ArrayList<StudentData> studentList, int appColor) {
        this.studentList = studentList;
        this.context = context;
        this.appColor = appColor;
    }

    @Override
    public SmsStudentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
        return new SmsStudentListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final SmsStudentListAdapter.MyViewHolder holder, int position) {
        final StudentData studentObj = studentList.get(position);
        String next = "<font color='#727272'>" + "(" + studentObj.getStudentID() + ")" + "</font>";
        holder.tv_item_name.setText(Html.fromHtml(studentObj.getStudentName().replace("_", " ") + " " + next));
        holder.tv_item_name.setText(studentObj.getStudentName().replace("_", " "));
//
//        holder.tv_designation.setText(context.getString(R.string.class_text)+
//                " : "+ studentObj.getClassName()+" "+studentObj.getSectionName());

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;
        //  LinearLayout ll_item_view_section, ly_item_create_class_sections;
        // Button btn_item_create_class_add_section;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_slot_start_time);
            tv_item_name.setTextColor(appColor);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((SmsContactListActivity) context).
                            startSMS(
                                    studentList.get(getLayoutPosition()),
                                    "Student"
                            );
                }
            });


        }
    }

    public void setFilter(ArrayList<StudentData> studentList) {
        this.studentList = new ArrayList<>();
        this.studentList.addAll(studentList);
        notifyDataSetChanged();
    }
}

