package in.junctiontech.school.sms;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.StudentData;

/**
 * Created by JAYDEVI BHADE on 1/4/2017.
 */

public class SmsTeacherListFragment  extends
        Fragment implements SearchView.OnQueryTextListener{
    private RecyclerView recycler_view_list_of_data;
    private TextView tv_view_all_data_not_data_available;
    private SwipeRefreshLayout swipe_refresh_list;

    private ArrayList<StudentData> teacherListData= new ArrayList<>();
    private SmsTeacherListAdapter adapter;
    private int  appColor;
    private boolean  smsToTeacher;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.fragment_view_all_list, container, false);
        recycler_view_list_of_data = (RecyclerView) convertView.findViewById(R.id.recycler_view_list_of_data);
        tv_view_all_data_not_data_available = (TextView) convertView.findViewById(R.id.tv_view_all_data_not_data_available);
        swipe_refresh_list =(SwipeRefreshLayout)convertView.findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setColorSchemeResources(R.color.ColorPrimaryDark,R.color.heading,R.color.back);
        if (!smsToTeacher) {
            tv_view_all_data_not_data_available.setText(getString(R.string.you_dont_have_permission_to_send_messages_to) + " " +
                    getString(R.string.teacher));
        }
        setHasOptionsMenu(true);
        return convertView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (smsToTeacher) {
            teacherListData =((SmsContactListActivity)getActivity()).getStaffList();
            Log.e("onActivitySMSTeacher",teacherListData.size()+" size");

            if (teacherListData.size()==0){
                teacherListData =((SmsContactListActivity)getActivity()).getStaffList();
            }
        }



        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();
    }


    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_view_list_of_data.setLayoutManager(layoutManager);
        adapter = new SmsTeacherListAdapter(getContext(), teacherListData,appColor);
        recycler_view_list_of_data.setAdapter(adapter);
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appColor = ((SmsContactListActivity)getActivity()).getAppColor();
        SharedPreferences sp = Prefs.with(getActivity()).getSharedPreferences();
        smsToTeacher = sp.getBoolean("SMSToTeacher",false);
}



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setFilter(ArrayList<StudentData> teacherListData) {
        adapter.setFilter(teacherListData);

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);


        getActivity().getMenuInflater().inflate(R.menu.menu_search, menu);

        /* --  toolbar search ---*/
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
// Do something when collapsed
                        setFilter(teacherListData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
// Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<StudentData> filteredModelList = filter(teacherListData, newText);
        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<StudentData> filter(ArrayList<StudentData> models, String query) {
        query = query.toLowerCase();final ArrayList<StudentData> filteredModelList = new ArrayList<>();

        for (StudentData model : models) {

            if (model.getStudentName().toLowerCase().contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void closeSwipe() {
        if (swipe_refresh_list!=null && swipe_refresh_list.isRefreshing()) swipe_refresh_list.setRefreshing(false);
    }

    public void updateStaffList(ArrayList<StudentData> staffListStudentModelList) {
        if (smsToTeacher)
        setFilter(staffListStudentModelList);
    }
}
