package in.junctiontech.school.homework;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.ReportView.ViewHomeworkDetailActivity;
import in.junctiontech.school.models.StudentData;

public class AddHomework extends AppCompatActivity {

    private String[] sub_id, sub_name;
    private Spinner sp_subject_name;
    private EditText et_homework;
    private RecyclerView my_recycler_view_add_homework;
    private int selectedSubNamePosition = 0;
    private MyAdapter mAdapter;
    private TextView tv_selected_sub_name;
    private DbHandler db;
    private List<StudentData> listOfHomework;
    private ArrayList<Integer> leftIndex;
    private SharedPreferences sharedPreferences;
    private boolean homeworkAdded = false;
    private RelativeLayout page_demo;
    private int colorIs = 0;
    private LinearLayout ll_add_homework_demo, ll_click_homework_to_evaluate_demo;
    private View snackbar;

    public void setDemoForEvaluation(boolean visibility) {
        if (visibility) {
            page_demo.setVisibility(View.VISIBLE);

            ll_add_homework_demo.setVisibility(View.INVISIBLE);
            ll_click_homework_to_evaluate_demo.setVisibility(View.VISIBLE);
            ll_click_homework_to_evaluate_demo.startAnimation(AnimationUtils.loadAnimation(AddHomework.this, R.anim.vibrate_shake));

            int counter = sharedPreferences.getInt(Config.EVALUATION_HINT_COUNTER, 0)+1;
            Log.e("counter",sharedPreferences.getInt(Config.EVALUATION_HINT_COUNTER, 0)+"");
            Log.e("counter",counter+"");
            sharedPreferences.edit().putInt(Config.EVALUATION_HINT_COUNTER ,counter).commit();

        } else {
            page_demo.setVisibility(View.INVISIBLE);
            ll_click_homework_to_evaluate_demo.clearAnimation();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
        // LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));
        setContentView(R.layout.activity_add_homework);
        db = DbHandler.getInstance(this);
       /* AdView mAdView = (AdView) findViewById(R.id.adview_add_homework);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (sharedPreferences.getBoolean("addStatusGone", false))
            mAdView.setVisibility(View.GONE);*/

        /*********************************    Demo Screen *******************************/
        page_demo = (RelativeLayout) findViewById(R.id.rl_teacher_add_homework_demo);
        ll_add_homework_demo = (LinearLayout) findViewById(R.id.ll_add_homework_demo);
        ll_click_homework_to_evaluate_demo = (LinearLayout) findViewById(R.id.ll_click_homework_to_evaluate_demo);

        if (sharedPreferences.getBoolean("DemoAddHomework", true)) {
            page_demo.setVisibility(View.VISIBLE);
        } else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDemoForEvaluation(false);
                page_demo.setVisibility(View.GONE);
                sharedPreferences.edit().putBoolean("DemoAddHomework", false).commit();
            }
        });

        /****************************************************************/

        getSupportActionBar().setTitle(R.string.title_activity_add_assignment);
        getSupportActionBar().setSubtitle("Date : " + getIntent().getStringExtra("date_of_work"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        snackbar = (View) findViewById(R.id.rl_activity_add_homework);
        tv_selected_sub_name = (TextView) findViewById(R.id.tv_selected_sub_name);
        tv_selected_sub_name.setTextColor(colorIs);

        sp_subject_name = (Spinner) findViewById(R.id.sp_subject_name);
        et_homework = (EditText) findViewById(R.id.et_homework);
        sub_id = getIntent().getStringArrayExtra("sub_id");
        sub_name = getIntent().getStringArrayExtra("sub_name");
        ArrayAdapter<String> arr_cls = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, sub_name);
        arr_cls.setDropDownViewResource(R.layout.myspinner_dropdown_item);
        sp_subject_name.setAdapter(arr_cls);

        Bundle b = db.getHomeworkForAddWork(getIntent().getStringExtra("section_selected_id"),
                getIntent().getStringExtra("date_of_work"), sub_id, sub_name);

        listOfHomework = (List<StudentData>) b.getSerializable("listOfHomework");
        leftIndex = b.getIntegerArrayList("leftIndex");

        my_recycler_view_add_homework = (RecyclerView) findViewById(R.id.my_recycler_view_add_homework);

        mAdapter = new MyAdapter(this, listOfHomework);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        my_recycler_view_add_homework.setLayoutManager(mLayoutManager);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        my_recycler_view_add_homework.setItemAnimator(itemAnimator);

        my_recycler_view_add_homework.setAdapter(mAdapter);
        my_recycler_view_add_homework.setNestedScrollingEnabled(false);

        sp_subject_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSubNamePosition = position;
                tv_selected_sub_name.setText(sub_name[position] + " :");

                if (leftIndex.contains(position)) {
                    //   listOfHomework.add(newtext CustomDataForTimeTable(et_homework.getText().toString(),sub_name[position],sub_id[position]));
                    et_homework.setText("");
                } else {

                    for (int i = 0; i < listOfHomework.size(); i++) {
                        StudentData obj = listOfHomework.get(i);
                        if (obj.getLinkUrl().equalsIgnoreCase(sub_id[position])) {
                            et_homework.setText(Html.fromHtml(obj.getDescription()));
                        }
                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_subject_name)).setTextColor(colorIs);

    }

    public void addHomework(View v) {
        //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Config.hideKeyboard(this);

        if (et_homework.getText().toString().trim().equalsIgnoreCase("")) {
            Snackbar.make(snackbar, R.string.please_write_some_text, Snackbar.LENGTH_LONG).setAction(R.string.retry, null).show();

        } else {
            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(this);
            alertAdmin.setIcon(getResources().getDrawable(R.drawable.ic_hand_arrow));
            alertAdmin.setTitle(getString(R.string.confirmation));

            alertAdmin.setMessage(getString(R.string.assignment_confirmation_msg));
            alertAdmin.setCancelable(false);

            alertAdmin.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    db.saveHomeWork(getIntent().getStringExtra("cls_selected_id"),
                            getIntent().getStringExtra("section_selected_id"),
                            getIntent().getStringExtra("date_of_work"), sub_id[selectedSubNamePosition],  et_homework.getText().toString().trim().replaceAll("'","")  );

                    if (leftIndex.contains(selectedSubNamePosition)) {
                        listOfHomework.add(new StudentData(et_homework.getText().toString(),
                                sub_name[selectedSubNamePosition], sub_id[selectedSubNamePosition]));

                        leftIndex.remove(leftIndex.indexOf(selectedSubNamePosition));
                    } else {
                        for (int i = 0; i < listOfHomework.size(); i++) {
                            StudentData obj = listOfHomework.get(i);
                            if (obj.getLinkUrl().equalsIgnoreCase(sub_id[selectedSubNamePosition])) {
                                obj.setDescription(et_homework.getText().toString());

                            }
                        }
                    }


                    mAdapter.notifyDataSetChanged();
                    homeworkAdded = true;
                    if (sharedPreferences.getInt(Config.EVALUATION_HINT_COUNTER, 0) < 3)
                        setDemoForEvaluation(true);
                }
            });
            alertAdmin.setNegativeButton(getString(R.string.recheck), null);
            alertAdmin.show();

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, TeacherHomework.class);
        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));

        if (homeworkAdded) {
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED, intent);
        }

        finish();
        overridePendingTransition(R.anim.enter, R.anim.nothing);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, TeacherHomework.class);
        intent.putExtra("pos", getIntent().getIntExtra("pos", 0));

        if (homeworkAdded) {
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED, intent);
        }

        finish();
        overridePendingTransition(R.anim.enter, R.anim.nothing);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        Context context;
        List<StudentData> homeworkList;

        public MyAdapter(Context context, List<StudentData> homeworkList) {
            this.context = context;
            this.homeworkList = homeworkList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.homework_view, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final StudentData obj = homeworkList.get(position);
            holder.subjectName.setText(obj.getDate());
            //holder.homework.setText(obj.getDescription());
          //  holder.webview_homework_of_particular_subject.loadDataWithBaseURL(null,obj.getDescription() , "text/html", "utf-8", null);

            holder.iv_evaluation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddHomework.this, HomeworkEvaluation.class);
                    intent.putExtra("classID", getIntent().getStringExtra("cls_selected_id"));

                    intent.putExtra("sectionId", getIntent().getStringExtra("section_selected_id"));
                    intent.putExtra("dateOfHomework", getIntent().getStringExtra("date_of_work"));
                    intent.putExtra("subjectID", obj.getLinkUrl());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                }
            });

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ViewHomeworkDetailActivity.class)
                            .putExtra("isFromAddHomework",true )
                            .putExtra("subjectName",obj.getDate() )
                            .putExtra("homework",obj.getDescription()
                            ));
                }
            });

        }

        @Override
        public int getItemCount() {
            return homeworkList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView subjectName;
          /*  WebView webview_homework_of_particular_subject;*/
          ImageView iv_evaluation;
            View view;

            public MyViewHolder(View itemView) {
                super(itemView);
                iv_evaluation = (ImageView) itemView.findViewById(R.id.iv_evaluation);
                ( itemView.findViewById(R.id.tv_subject_evaluation_status)).setVisibility(View.GONE);
                iv_evaluation.setVisibility(View.VISIBLE);
                subjectName = (TextView) itemView.findViewById(R.id.tv_subject_name);
                subjectName.setTextColor(colorIs);
              /*  webview_homework_of_particular_subject = (WebView) itemView.findViewById(R.id.webview_homework_of_particular_subject);*/
               /* itemView.findViewById(R.id.tv_homework_of_particular_subject).setVisibility(View.GONE);*/
                view = itemView;
            }
        }
    }

}
