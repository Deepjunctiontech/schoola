package in.junctiontech.school.homework;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.HomeworkEvaluations;

public class HomeworkEvaluation extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private DbHandler db;
    private RecyclerView my_recycler_view;
    private Button button_selectall, button_completed;
    //  private StudentData obj;
    private String[] stId, stName;
    private HomeworkAdapter madapter;
    // private ArrayList<String> completedStudents = new ArrayList<>();
    private boolean selectAllIsTrue = true;
    private List<HomeworkEvaluations> homeworkEvaluationsList;
    private String classId, sectionId, subId, dateofHomework;
    private int  colorIs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_evaluation_third);
        sharedPreferences = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sharedPreferences.getString("app_language", ""));
        db = DbHandler.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();
        initViews();

        // obj = (StudentData) getIntent().getSerializableExtra("dataObj");
        Intent intent = getIntent();
        classId = getIntent().getStringExtra("classID");
        sectionId = intent.getStringExtra("sectionId");
        subId = intent.getStringExtra("subjectID");
        dateofHomework = intent.getStringExtra("dateOfHomework");
        Log.e("sectionId", sectionId);
        Log.e("subId", sectionId);
        Log.e("dateofHomework", dateofHomework);
        homeworkEvaluationsList = db.getHomeworkEvalData(dateofHomework,
                sectionId,
                subId
        );
//        stName = data[0];
//        stId = data[1];

        madapter = new HomeworkAdapter(this, homeworkEvaluationsList);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            my_recycler_view.setLayoutManager(gridLayoutManager);
        } else {
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
            my_recycler_view.setLayoutManager(gridLayoutManager);
        }

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        my_recycler_view.setItemAnimator(itemAnimator);
        my_recycler_view.setAdapter(madapter);
        madapter.notifyDataSetChanged();
    }
    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        my_recycler_view.setLayoutManager(new GridLayoutManager(this,
                newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? 6 : 3));

        super.onConfigurationChanged(newConfig);
    }

    private void initViews() {
        my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);
        button_selectall = (Button) findViewById(R.id.btn_selectall);
        button_completed = (Button) findViewById(R.id.btn_completed);

        button_selectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectAllIsTrue) {
                    button_selectall.setText(getString(R.string.un_select_all));

                    for (int i = 0; i < homeworkEvaluationsList.size(); i++)
                        homeworkEvaluationsList.get(i).setStatus("C");

                    selectAllIsTrue = false;

                } else {
                    button_selectall.setText(getString(R.string.select_all));
                    selectAllIsTrue = true;
                    for (int i = 0; i < homeworkEvaluationsList.size(); i++)
                        homeworkEvaluationsList.get(i).setStatus("INC");
                }
                madapter.notifyDataSetChanged();
            }
        });

        button_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   final String db_name = sharedPreferences.getString("organization_name", "Not Found");
            //    final Map<String, Object> param = new LinkedHashMap<String, Object>();
                //  param.put("DB_Name", db_name);
             /*   param.put("classId", classId);
                param.put("sectionid", sectionId);
                param.put("subjectid", subId);
                param.put("dateOfHomework", dateofHomework);*/

               String homeworkEval = "";

                JSONArray jarrStId = new JSONArray();
                JSONArray jarrStatus = new JSONArray();
                for (int i = 0; i < homeworkEvaluationsList.size(); i++) {
                    HomeworkEvaluations obj = homeworkEvaluationsList.get(i);
                    Log.e(i + "", obj.getName() + obj.getId() + obj.getStatus());
                    jarrStId.put(obj.getId());
                    jarrStatus.put(obj.getStatus());

                    if (i == homeworkEvaluationsList.size())
                        homeworkEval = homeworkEval + obj.getId() + "-" + obj.getStatus();
                    else
                        homeworkEval = homeworkEval + obj.getId() + "-" + obj.getStatus() + ",";
                }
                db.saveHomeworkEvaluation(classId, sectionId, subId, dateofHomework, homeworkEval);

                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
               /* param.put("stId", jarrStId);
                param.put("homeworkStatus", jarrStatus);

                if (!(Config.checkInternet(HomeworkEvaluation.this))) {
                    AlertDialog.Builder alert_internet = new AlertDialog.Builder(HomeworkEvaluation.this,
                            AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    alert_internet.setMessage(R.string.internet_not_available_please_check_your_internet_connectivity);
                    alert_internet.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert_internet.setCancelable(false);
                    alert_internet.show();
                } else {

                    // RequestQueue rqueue = Volley.newRequestQueue(this);

                    Log.e("HomeworkEvaURL", sharedPreferences.getString("HostName", "Not Found") +
                            "version2.0/HomeworkEvaluationApi.php?action=homeworkEvaluationInsert&databaseName=" + db_name);

                    StringRequest string_req = new StringRequest(Request.Method.POST,
                            sharedPreferences.getString("HostName", "Not Found") +
                                    "version2.0/HomeworkEvaluationApi.php?action=homeworkEvaluationInsert&databaseName=" + db_name
//                sharedPreferences.getString("HostName", "Not Found") +
//                        "homeworkEvaluation.php?action=evaluate"
                            , new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {

                            Log.e("HomeworkEvaRes", s);

                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d("errorvolley", volleyError.toString());
                            //    Toast.makeText(FCMIntentService.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                            Toast.makeText(HomeworkEvaluation.this, "Homework Evaluation Sending Failed !", Toast.LENGTH_LONG).show();
                            finish();
                            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {

                            Map<String, String> header = new LinkedHashMap<String, String>();
                            header.put("Content-Type", "application/x-www-form-urlencoded");
                            return super.getHeaders();
                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new LinkedHashMap<String, String>();
                            params.put("data", new JSONObject(param).toString());
                            Log.d("HomeworkEvaRes", new JSONObject(params).toString());
                            return params;
                        }
                    };

//        Toast.makeText(context, newtext JSONObject(param).toString(),Toast.LENGTH_LONG).show();
                    string_req.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
                    AppRequestQueueController.getInstance(HomeworkEvaluation.this).addToRequestQueue(string_req);
                }
*/

            }
       });
    }


    private void submit() {

    }

    public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.MyHolder> {
        Context context;

        List<HomeworkEvaluations> homeworkEvaluationsList;

        public HomeworkAdapter(Context context, List<HomeworkEvaluations> dataList) {
            this.context = context;
            this.homeworkEvaluationsList = dataList;

        }


        @Override
        public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_all_student, parent, false);
            return new MyHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyHolder holder, final int position) {

            final HomeworkEvaluations obj = homeworkEvaluationsList.get(position);
            String next = "<font color='#727272'>" + "(" + obj.getId() + ")" + "</font>";
            holder.name_check.setText(Html.fromHtml(obj.getName() + " " + next));

            Log.e("ritu", obj.getName() + obj.getId() + obj.getStatus());

            if (obj.getStatus().equalsIgnoreCase("C")) {
                holder.name_check.setChecked(true);
                holder.ly_view_all_student.setBackgroundResource(R.drawable.selected_view);

            } if (obj.getStatus().equalsIgnoreCase("NE")) {
                holder.name_check.setChecked(true);
                holder.ly_view_all_student.setBackgroundResource(R.drawable.cliclable_view);

            } else  if (obj.getStatus().equalsIgnoreCase("INC")){
                holder.ly_view_all_student.setBackgroundResource(R.drawable.red_background);
                holder.name_check.setChecked(false);
            }

            holder.name_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (obj.getStatus().equalsIgnoreCase("NE")) {
                        holder.ly_view_all_student.setBackgroundResource(R.drawable.red_background);
                        obj.setStatus("INC");
                    } else  if (obj.getStatus().equalsIgnoreCase("C")) {
                        holder.ly_view_all_student.setBackgroundResource(R.drawable.cliclable_view);
                        obj.setStatus("NE");
                    }  else  if (obj.getStatus().equalsIgnoreCase("INC")) {
                        holder.ly_view_all_student.setBackgroundResource(R.drawable.selected_view);
                        obj.setStatus("C");
                    }

                }
            });
        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context,
                    R.anim.animator_for_bounce);
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }

        @Override
        public int getItemCount() {
            return homeworkEvaluationsList.size();
        }

        public class MyHolder extends RecyclerView.ViewHolder {
            LinearLayout ly_view_all_student;
            CheckBox name_check;
//            TextView name_of_studence;

            public MyHolder(View itemView) {
                super(itemView);
                name_check = (CheckBox) itemView.findViewById(R.id.ck_name_check);
                name_check.setTextColor(getResources().getColor(android.R.color.black));
                //   name_of_studence = (TextView) itemView.findViewById(R.id.tv_name_of_studence);
                ly_view_all_student = (LinearLayout) itemView.findViewById(R.id.ly_view_all_student);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final HomeworkEvaluations obj = homeworkEvaluationsList.get(getLayoutPosition());


                        if (obj.getStatus().equalsIgnoreCase("NE")) {
                            ly_view_all_student.setBackgroundResource(R.drawable.red_background);
                            obj.setStatus("INC");
                        } else  if (obj.getStatus().equalsIgnoreCase("C")) {
                            ly_view_all_student.setBackgroundResource(R.drawable.cliclable_view);
                            obj.setStatus("NE");
                        }  else  if (obj.getStatus().equalsIgnoreCase("INC")) {
                            ly_view_all_student.setBackgroundResource(R.drawable.selected_view);
                            obj.setStatus("C");
                        }



//                        if (name_check.isChecked()) {
//                            obj.setStatus("INC");
//                            name_check.setChecked(false);
//                            ly_view_all_student.setBackgroundResource(R.drawable.cliclable_view);
//
//                        } else {
//                            obj.setStatus("C");
//                            name_check.setChecked(true);
//                            ly_view_all_student.setBackgroundResource(R.drawable.selected_view);
//
//                        }

                    }
                });
            }


        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_sort_by_alphabetical:
                Toast.makeText(this, getString(R.string.sort_by_alphabetical), Toast.LENGTH_LONG).show();
                Collections.sort(homeworkEvaluationsList, new Comparator<HomeworkEvaluations>() {
                    @Override
                    public int compare(HomeworkEvaluations o1, HomeworkEvaluations o2) {
                        String lname = o1.getName().toLowerCase();
                        String rname = o2.getName().toLowerCase();

                        //  Log.e("compareTo",lname.compareTo(rname)+" ritu");
                        //  Log.e("ArrayListSort","INTEGER");
                        return lname.compareTo(rname);


                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                madapter.notifyDataSetChanged();
                break;

            case R.id.menu_sort_by_number:
                Toast.makeText(this, getString(R.string.sort_by_number), Toast.LENGTH_LONG).show();

                Collections.sort(homeworkEvaluationsList, new Comparator<HomeworkEvaluations>() {
                    @Override
                    public int compare(HomeworkEvaluations o1, HomeworkEvaluations o2) {
                        String lid = o1.getId();
                        String rid = o2.getId();

                        try {
                            int ld = Integer.parseInt(lid);
                            int rd = Integer.parseInt(rid);
                            //  Log.e("ArrayListSort","INTEGER");
                            if (ld > rd)
                                return 1;
                            else if (ld < rd)
                                return -1;
                            else
                                return 0;

                        } catch (NumberFormatException e) {
                            // Log.e("ArrayListSort","String");
                            return lid.compareTo(rid);
                        }

                        //  return lid.compareToIgnoreCase(rid);

                    }
                });
                madapter.notifyDataSetChanged();
                break;


        }

        return super.onOptionsItemSelected(item);
    }
}
