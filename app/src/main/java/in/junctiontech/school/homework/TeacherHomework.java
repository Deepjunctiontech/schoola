package in.junctiontech.school.homework;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

import in.junctiontech.school.schoolnew.classsection.ClassSectionActivity;

public class TeacherHomework extends AppCompatActivity implements TabHost.OnTabChangeListener {
    private TabHost tb;
    private String selectedTab = "";
    private Calendar week_calendar, month_tab_calendar;
    private String[] weekDay = new String[7];
    public String[] mon = new String[12];
    public String[] months_tab = new String[12];
    private Spinner homework_class, homework_section;

    private Button week_left, week_right;
    public ListView week_listview;
    private TextView week_text;
    private LayoutInflater inflater;
    private String week_list[] = new String[7];
    private String date_of_work[] = new String[7];
    private String day_name_of_week[] = new String[7];
    private String work_array[] = new String[7];

    private String month_date_array[] = new String[48];
    private String month_work_array[] = new String[48];
    final int month_background_array[] = new int[48];
    final int month_textcolor_array[] = new int[48];
    private String monthFullDateName_array[] = new String[48];
    private SharedPreferences sp;

    private String[] class_name, section_name;
    public String cls_selected_id = "", section_selected_id = "";
    public GridView calander_gridview;
    private TextView month_textview;
    private Button tab3_leftarrow, tab3_rightarrow;
    private String[] class_id, section_id;

    //private AlertDialog.Builder alert;
    private float x1, x2;
    private DbHandler db;
    private String[] sessionStartDate, sessionEndDate;
    private int selected_class_id_pos, selected_section_id_pos;
   // private AlertDialog.Builder alertInternet;
 //   private boolean alertInternetVisible = false;
    private MonthAdapter monthAdapter;
    private WeekAdapter weekAdapter;
    private RelativeLayout  snackbar;
    private RelativeLayout  page_demo;
    private int  colorIs =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_teacher_homework);
                /*********************************    Demo Screen *******************************/
                page_demo =(RelativeLayout)findViewById(R.id.rl_teacher_homework_demo);
        if (sp.getBoolean("DemoHomework",true)){
            page_demo.setVisibility(View.VISIBLE);
        }else page_demo.setVisibility(View.GONE);

        page_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_demo.setVisibility(View.GONE);
                sp.edit().putBoolean("DemoHomework",false).commit();
            }
        });
        /****************************************************************/




        snackbar =(RelativeLayout)findViewById(R.id.activity_teacher_homework);
        homework_class = (Spinner) findViewById(R.id.homework_class);
        homework_section = (Spinner) findViewById(R.id.homework_section);
        week_text = (TextView) findViewById(R.id.calendar_month_year_textview);
        tb = (TabHost) findViewById(R.id.tabHost);
        calander_gridview = (GridView) findViewById(R.id.calander_gridview);
        week_listview = (ListView) findViewById(R.id.weekday_listview);
        month_textview = (TextView) findViewById(R.id.tab3_month_textview);
        tab3_leftarrow = (Button) findViewById(R.id.tab3_left_arrow);
        tab3_rightarrow = (Button) findViewById(R.id.tab3_right_arrow);
        week_left = (Button) findViewById(R.id.tab2_left_arrow);
        week_right = (Button) findViewById(R.id.tab2_right_arrow);

        db = DbHandler.getInstance(this);
        getSupportActionBar().setTitle(R.string.assignment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        weekDay = getResources().getStringArray(R.array.week_name_array);
        mon = getResources().getStringArray(R.array.full_name_of_month_array);
        months_tab = getResources().getStringArray(R.array.full_name_of_month_array);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.my_pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);



        week_calendar = Calendar.getInstance();
        month_tab_calendar = Calendar.getInstance();

        sessionStartDate = sp.getString("sessionStartDate", "").split("-");
        sessionEndDate = sp.getString("sessionEndDate", "").split("-");

        String[][] data = db.getClassName();
        class_id = data[0];
        class_name = data[1];

        if (class_id.length == 0) {

            showSnack(true,getString(R.string.classes_not_available));
        } else {
            cls_selected_id = class_id[0];
            ArrayAdapter<String> arr_cls = new ArrayAdapter<String>(TeacherHomework.this,
                    android.R.layout.simple_list_item_1, class_name);
          arr_cls .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            homework_class.setAdapter(arr_cls);

           /* selected_class_id_pos = sp.getInt("defaultClass", 0);
            homework_class.setSelection(selected_class_id_pos);*/

            int lastUsedClassIdPos = 0;
            int jj = 0;
            for (String classID : class_id) {
                if (sp.getString(Config.LAST_USED_CLASS_ID, "").
                        equalsIgnoreCase(classID)) {
                    lastUsedClassIdPos = jj;

                }
                jj++;
            }
            if (class_id.length > 0)
                homework_class.setSelection(lastUsedClassIdPos);


            String[][] sec_data = db.getSectionName(cls_selected_id);
            section_id = sec_data[0];
            section_name = sec_data[1];

            if (section_id.length == 0) {
              //  alert.setMessage(getString(R.string.sections_not_available));
                showSnack(true,getString(R.string.sections_not_available));
            } else
                section_selected_id = section_id[0];

            homework_class.setOnItemSelectedListener(new MyclassAdapter());
            homework_section.setOnItemSelectedListener(new MysectionAdapter());


            tb.setup();
            TabHost.TabSpec spec;

            spec = tb.newTabSpec(getString(R.string.month));
            spec.setContent(R.id.tab3);
            spec.setIndicator(getString(R.string.month));
            tb.addTab(spec);

            spec = tb.newTabSpec(getString(R.string.week));
            spec.setContent(R.id.tab2);
            spec.setIndicator(getString(R.string.week));
            tb.addTab(spec);

            for (int i = 0; i < tb.getTabWidget().getChildCount(); i++) {
                TextView tv = (TextView) tb.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                tv.setTextColor(Color.BLACK);
            }


            tb.setOnTabChangedListener(this);

//        int tabid=  tb.getCurrentTab();
//        tb.getTabWidget().getChildAt(tabid).setBackgroundResource(R.drawable.btn_save);



            // if (Config.checkInternet(TeacherHomework.this)) {
            if (sp.getBoolean("firstTimeHomeworkFetch", true)) {
                sp.edit().putBoolean("firstTimeHomeworkFetch", false).commit();
                try {
                    getHomeworkFromServer();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        /*} else {
            //internet not available
            setMonthCalender(month_tab_calendar);
            //internet not available
            if (!alertInternetVisible)
                alertInternet.show();
            alertInternetVisible = true;
        }*/




            tab3_leftarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monthLeft();
                }


            });


            tab3_rightarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monthRight();
                }

            });

                for (int i = 0; i < 7; i++) {
                    date_of_work[i] = week_calendar.get(Calendar.YEAR) + "-" +
                            (week_calendar.get(Calendar.MONTH) + 1) + "-" + week_calendar.get(Calendar.DAY_OF_MONTH);
                    week_list[i] = week_calendar.get(Calendar.DAY_OF_MONTH) + " "
                            + mon[week_calendar.get(Calendar.MONTH)];
                    work_array[i] = "";

                    String aaa = (week_calendar.getTime()).toString();
                    for (int pos = 0; pos < 7; pos++) {
                        if (aaa.contains(weekDay[pos]))
                            day_name_of_week[i] = weekDay[pos];
                    }
                    week_calendar.add(Calendar.DAY_OF_YEAR, 1);
                }
                if (class_id.length == 0 || section_id.length == 0) {
                    showSnack(true,getString(R.string.classes_not_available));
                } else {
                    work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);

                    week_calendar.add(Calendar.DAY_OF_YEAR, -1);

                    weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
                    week_listview.setAdapter(weekAdapter);
                    week_text.setText(week_calendar.get(Calendar.YEAR) + ", "
                            + week_list[0] + "-" + week_list[6]);
                }




            week_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    weekLeft();
                }
            });


            week_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    weekRight();

                }
            });

        }


        //  }

        setColorApp();

    }

    public int getAppColor() {
        return colorIs;
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this,true);
      //  getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_homework_class)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_homework_section)).setTextColor(colorIs);
         month_textview.setTextColor(colorIs);
        week_text.setTextColor(colorIs);
        if (  Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tab3_leftarrow.setBackgroundTintList(ColorStateList.valueOf(colorIs));
            tab3_rightarrow.setBackgroundTintList(ColorStateList.valueOf(colorIs));
            week_left.setBackgroundTintList(ColorStateList.valueOf(colorIs));
            week_right.setBackgroundTintList(ColorStateList.valueOf(colorIs));
        }
    }


    public void showSnack(final boolean isNotInternet , String msg){

    Snackbar snackbarObj= Snackbar.make(snackbar,
            msg,
            Snackbar.LENGTH_LONG).setAction(R.string.reload_school_data, new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           if (isNotInternet) {
               db.getDataFromServer();
               finish();
               overridePendingTransition(R.anim.nothing, R.anim.slide_out);
           }
        }
    });
        snackbarObj.setDuration(5000);
        snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
        snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
        snackbarObj.show();

    }

    private void showSnack() {
        Snackbar snackbarObj = null;
        if (class_id.length == 0 || section_id.length == 0  ) {
            snackbarObj = Snackbar.make(
                    snackbar,
                    getString(R.string.classes_not_available),
                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_class),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!sp.getBoolean(Config.CREATE_CLASS_SECTION_PERMISSION,false)) {
                                Toast.makeText(TeacherHomework.this,"Permission not available : CREATE CLASS SECTION !",Toast.LENGTH_SHORT).show();

                            }else {  startActivity(new Intent(TeacherHomework.this, ClassSectionActivity.class));
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
                        }
                    });

        }
        if (snackbarObj != null) {
            snackbarObj.setDuration(5000);
            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
            snackbarObj.show();
        }

    }

    public void weekLeft() {
        try {
            getHomeworkFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        week_calendar.add(Calendar.DAY_OF_YEAR, -7);
        for (int i = 6; i >= 0; i--) {

            date_of_work[i] = week_calendar.get(Calendar.YEAR) + "-" +
                    ((week_calendar.get(Calendar.MONTH) + 1)<10?("0" +(week_calendar.get(Calendar.MONTH) + 1)):

            (week_calendar.get(Calendar.MONTH) + 1)) + "-" + (week_calendar.get(Calendar.DAY_OF_MONTH)<10?
                    "0" +week_calendar.get(Calendar.DAY_OF_MONTH):week_calendar.get(Calendar.DAY_OF_MONTH));
            week_list[i] = week_calendar.get(Calendar.DAY_OF_MONTH) + " "
                    + mon[week_calendar.get(Calendar.MONTH)];
            work_array[i] = "";

            week_calendar.add(Calendar.DAY_OF_YEAR, -1);
        }
        work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);

        week_text.setText(week_calendar.get(Calendar.YEAR) + " , " + week_list[0] + "-" + week_list[6]);

        weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
        week_listview.setAdapter(weekAdapter);
        week_calendar.add(Calendar.DAY_OF_YEAR, 7);
    }

    public void weekRight() {
        try {
            getHomeworkFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 7; i++) {
            week_calendar.add(Calendar.DAY_OF_YEAR, 1);
            date_of_work[i] = week_calendar.get(Calendar.YEAR) + "-" +
                    ((week_calendar.get(Calendar.MONTH) + 1)<10?("0" +(week_calendar.get(Calendar.MONTH) + 1)):

                            (week_calendar.get(Calendar.MONTH) + 1)) + "-" + (week_calendar.get(Calendar.DAY_OF_MONTH)<10?
                    "0" +week_calendar.get(Calendar.DAY_OF_MONTH):week_calendar.get(Calendar.DAY_OF_MONTH));
            week_list[i] = week_calendar.get(Calendar.DAY_OF_MONTH) + " " + mon[week_calendar.get(Calendar.MONTH)];
            work_array[i] = "";

            String aaa = (week_calendar.getTime()).toString();
            for (int pos = 0; pos < 7; pos++) {
                if (aaa.contains(weekDay[pos]))
                    day_name_of_week[i] = weekDay[pos];
            }

        }
        week_text.setText(week_calendar.get(Calendar.YEAR) + ", "
                + week_list[0] + "-" + week_list[6]);
        work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);

        weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
        week_listview.setAdapter(weekAdapter);
    }

    public void monthLeft() {
        /*if (month_tab_calendar.get(Calendar.MONTH) == month_tab_calendar.getActualMinimum(Calendar.MONTH)) {
            month_tab_calendar.set((month_tab_calendar.get(Calendar.YEAR) - 1), month_tab_calendar.getActualMaximum(Calendar.MONTH), 1);
        } else {
            month_tab_calendar.set(Calendar.MONTH, month_tab_calendar.get(Calendar.MONTH) - 1);
        }
*/month_tab_calendar.add(Calendar.MONTH,-1);
        try {
            getHomeworkFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setMonthCalender(month_tab_calendar);
    }

    public void monthRight() {
        month_tab_calendar.add(Calendar.MONTH,+1);
      /*  if (month_tab_calendar.get(Calendar.MONTH) == month_tab_calendar.getActualMaximum(Calendar.MONTH)) {
            month_tab_calendar.set((month_tab_calendar.get(Calendar.YEAR) + 1), month_tab_calendar.getActualMinimum(Calendar.MONTH), 1);
        } else {
            month_tab_calendar.set(Calendar.MONTH, month_tab_calendar.get(Calendar.MONTH) + 1);
        }
*/
        try {
            getHomeworkFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setMonthCalender(month_tab_calendar);
    }

    public void setMonthCalender(Calendar cal_obj) {

        for (int i = 0; i < month_work_array.length; i++) {
            month_work_array[i] = "";
            month_date_array[i] = "";
            monthFullDateName_array[i] = "";
            month_background_array[i] = R.drawable.borders;
            month_textcolor_array[i] = Color.BLACK;
        }
        month_textview.setText(months_tab[month_tab_calendar.get(Calendar.MONTH)] + " " + month_tab_calendar.get(Calendar.YEAR));

        Calendar mycal = new GregorianCalendar(cal_obj.get(Calendar.YEAR), cal_obj.get(Calendar.MONTH), 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

//          db.getHomeworkData(cls_selected_id,class_name[selected_class_id_pos],
//        section_selected_id,section_name[selected_section_id_pos], daysInMonth,month_tab_calendar);


        String aaa = (mycal.getTime()).toString();
        Log.d("right arrow", aaa);

//                Toast.makeText(TeacherHomework.this, "right  "+ mycal.getTime() , Toast.LENGTH_LONG).show();
        int pos = 0;
        if (pos == 0)
            month_textcolor_array[pos] = Color.RED;
        for (int a = 0; a < 7; a++) {
            month_date_array[pos] = weekDay[a];
            pos++;
        }

        for (int i = 0; i < Config.weekDayEng.length; i++) {
            if (pos % 7 == 0)
                month_textcolor_array[pos] = Color.RED;

            if (aaa.contains(Config.weekDayEng[i])) {
                break;
            } else {
                month_date_array[pos] = "";
                month_work_array[pos] = "";

                pos++;
            }
        }


        for (int i = 0; i < daysInMonth; i++) {
            if (pos % 7 == 0) {
                month_textcolor_array[pos] = Color.RED;

            }
            month_date_array[pos] = (i + 1) + "";

            String new_day = (i + 1) + "";
            if ((i + 1) < 10)
                new_day = "0" + (i + 1);

            String new_month = (month_tab_calendar.get(Calendar.MONTH) + 1) + "";
//            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
            if (((month_tab_calendar.get(Calendar.MONTH) + 1)) < 10)
                new_month = "0" + ((month_tab_calendar.get(Calendar.MONTH) + 1));

            monthFullDateName_array[pos] = month_tab_calendar.get(Calendar.YEAR) + "-" + new_month + "-" + (new_day);


            if (class_id.length != 0) {
                String[] bb = db.getHomework(cls_selected_id, section_selected_id,
                        new String[]{monthFullDateName_array[pos]});

                if (bb[0] == null) {
                    month_work_array[pos] = "";
                    month_background_array[pos] = R.drawable.borders;

                } else {
                    month_work_array[pos] = bb[0];
                    month_background_array[pos] = R.drawable.green_circular_layout;


                }
                pos++;
            } else {
                showSnack(true,getString(R.string.classes_not_available));
                break;
            }


        }

        monthAdapter = new MonthAdapter(TeacherHomework.this,
                month_date_array, month_work_array, monthFullDateName_array,
                month_background_array,
                cal_obj);
        calander_gridview.setAdapter(monthAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
     //   getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }

    public void getHomeworkFromServer() throws JSONException {

    /*    if (Config.checkInternet(this)) {*/
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.fetching_assignment));
            progressDialog.show();

            final String db_name = sp.getString("organization_name", "Not Found");
            final JSONObject param = new JSONObject();
           // param.put("DB_Name", db_name);
            param.put("session", sp.getString("session", ""));
            param.put("sectionid", section_selected_id);

            Calendar mycal = new GregorianCalendar(month_tab_calendar.get(Calendar.YEAR), month_tab_calendar.get(Calendar.MONTH), 1);
            int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

            param.put("monthStartDate", 1 + "-" + (month_tab_calendar.get(Calendar.MONTH) + 1) + "-" + month_tab_calendar.get(Calendar.YEAR));
            param.put("monthEndDate", daysInMonth + "-" + (month_tab_calendar.get(Calendar.MONTH) + 1) + "-" + month_tab_calendar.get(Calendar.YEAR));

            // RequestQueue rqueue = Volley.newRequestQueue(this);

            String url= sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")+ "HomeworkApi.php?action=teacher" +
                    "&databaseName="+db_name;
            Log.e("HomeFetchUrl",url);
            Log.e("param",param.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject1) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean("firstTimeHomeworkFetch", false);
                        editor.commit();
                        Log.e("HomeworkEvaRes", jsonObject1.toString());
                        DbHandler.longInfo(jsonObject1.toString());

                        try {
                            JSONObject resJsonObj= jsonObject1;

                            if (resJsonObj.optString("code").equalsIgnoreCase("201")){
                                JSONArray jsonArray = new JSONArray(resJsonObj.optString("result"));
                                List<ContentValues> dataList = new ArrayList();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    ContentValues cv = new ContentValues();

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    cv.put("class_id", jsonObject.optString("ClassId"));
                                    cv.put("section_id", jsonObject.optString("SectionId"));
                                    cv.put("date", jsonObject.optString("Dateofhomework"));
                                    cv.put("subjectid", jsonObject.optString("SubjectId"));
                                    cv.put("subjectname", jsonObject.optString("SubjectName"));
                                    cv.put("work", jsonObject.optString("Homework"));
                                    cv.put("homeworkEvaluation", jsonObject.optString("StudentId"));
                                    cv.put("sending_work_status", "sent");
                                    dataList.add(cv);
                                }
                                db.insertHomeworkFromServer(dataList);
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                        setMonthCalender(month_tab_calendar);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("errorvolley", volleyError.toString());
                //    Toast.makeText(FCMIntentService.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                setMonthCalender(month_tab_calendar);
                Config.responseVolleyErrorHandler(TeacherHomework.this,volleyError,snackbar);
            }
        });

            /*StringRequest string_req = new StringRequest(Request.Method.POST,
                    url
                   *//* sp.getString("HostName", "Not Found") + "homeworkEvaluation.php?action=get"*//*
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("data", new JSONObject(param).toString());
                    Log.d("HomeworkEvaRes", new JSONObject(params).toString());
                    return params;
                }
            };*/

//        Toast.makeText(context, newtext JSONObject(param).toString(),Toast.LENGTH_LONG).show();
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
        /*} else {
            //internet not available
            if (!alertInternetVisible)
                alertInternet.show();
            alertInternetVisible = true;
        }*/


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }


    @Override
    public void onTabChanged(String tabId) {

        if (tabId.equalsIgnoreCase(getString(R.string.week))) {
            work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);
            weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
            week_listview.setAdapter(weekAdapter);

            selectedTab = tabId;
        } else if (tabId.equalsIgnoreCase(getString(R.string.month))) {

            setMonthCalender(month_tab_calendar);
            selectedTab = tabId;

        }


    }

    private class MyclassAdapter implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            cls_selected_id = class_id[position];
            selected_class_id_pos = position;

            try {
                getHomeworkFromServer();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String[][] sec_data = db.getSectionName(cls_selected_id);
            section_id = sec_data[0];
            section_name = sec_data[1];


            ArrayAdapter<String> arr_sec = new ArrayAdapter<String>(TeacherHomework.this,
                    android.R.layout.simple_list_item_1, section_name);
          arr_sec .setDropDownViewResource(R.layout.myspinner_dropdown_item);
            homework_section.setAdapter(arr_sec);

            int lastUsedSecIdPos = 0;
            int jj = 0;
            for (String secID : section_id) {
                if (sp.getString(Config.LAST_USED_SECTION_ID, "").
                        equalsIgnoreCase(secID)) {
                    lastUsedSecIdPos = jj;

                }
                jj++;
            }
            if (section_id.length > 0) {
                homework_section.setSelection(lastUsedSecIdPos);

                sp.edit().
                        putString(Config.LAST_USED_SECTION_ID, section_id[homework_section.getSelectedItemPosition()])
                        .putString(Config.LAST_USED_CLASS_ID, class_id[position])
                        .commit();
            }

            work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);

            weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
            week_listview.setAdapter(weekAdapter);

            setMonthCalender(month_tab_calendar);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class MysectionAdapter implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//            Toast.makeText(TeacherHomework.this, cls_selected, Toast.LENGTH_LONG).show();

            selected_section_id_pos = position;
            section_selected_id = section_id[position];

            try {
                getHomeworkFromServer();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            work_array = db.getHomework(cls_selected_id, section_selected_id, date_of_work);
            weekAdapter = new WeekAdapter(TeacherHomework.this, week_list, work_array, date_of_work, day_name_of_week);
            week_listview.setAdapter(weekAdapter);

            sp.edit().
                    putString(Config.LAST_USED_SECTION_ID, section_id[homework_section.getSelectedItemPosition()])
                    .putString(Config.LAST_USED_CLASS_ID,class_id[homework_class.getSelectedItemPosition()])
                    .commit();

            setMonthCalender(month_tab_calendar);
//            Toast.makeText(TeacherHomework.this, section_selected, Toast.LENGTH_LONG).show();


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class WeekAdapter extends ArrayAdapter<String> {
        Context context;
        String[] values = {};
        String[] addwork = {};
        String[] date_of_weeks = {};
        String[] day_name_of_week = {};


        public WeekAdapter(Context context, String[] values, String[] week_work, String[] date_of_weeks, String[] day_name_of_week) {
            super(context, R.layout.month_homework, values);
            this.context = context;
            this.values = values;
            this.addwork = week_work;
            this.date_of_weeks = date_of_weeks;
            this.day_name_of_week = day_name_of_week;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.month_homework, null);
            }


            TextView dateview_tv = (TextView) convertView.findViewById(R.id.number_days);
            TextView datework_tv = (TextView) convertView.findViewById(R.id.weekhomework_textview);
//            TextView day_name_tv = (TextView) convertView.findViewById(R.id.day_name);

            dateview_tv.setText(values[position]);
            // day_name_tv.setText(day_name_of_week[position]);
            datework_tv.setText(addwork[position]);

            convertView.setOnClickListener(new View.OnClickListener() {
                private String[] sub_name;
                private String[] sub_id;

                @Override
                public void onClick(View v) {

                    String[] ab = date_of_weeks[position].split("-");

                    Calendar cal0 = Calendar.getInstance();
                    cal0.set(Integer.parseInt(ab[0]), Integer.parseInt(ab[1]), Integer.parseInt(ab[2]));

                    Calendar cal1 = Calendar.getInstance();
                    cal1.set(Integer.parseInt(sessionStartDate[2]), Integer.parseInt(sessionStartDate[1]), Integer.parseInt(sessionStartDate[0]));

                    Calendar cal2 = Calendar.getInstance();
                    cal2.set(Integer.parseInt(sessionEndDate[2]), Integer.parseInt(sessionEndDate[1]), Integer.parseInt(sessionEndDate[0]));


                    if (cal0.compareTo(cal1) >= 0 && cal0.compareTo(cal2) <= 0) {
                        String[][] sub_data = db.getSubjectsClasswise(cls_selected_id, section_selected_id);
                        sub_id = sub_data[0];
                        sub_name = sub_data[1];

                        Intent intent = new Intent(context, AddHomework.class);
                        intent.putExtra("sub_id", sub_id);
                        intent.putExtra("sub_name", sub_name);
                        intent.putExtra("cls_selected_id", cls_selected_id);
                        intent.putExtra("section_selected_id", section_selected_id);
                        intent.putExtra("date_of_work", date_of_weeks[position]);
                        intent.putExtra("pos", position);

                        if (sub_id.length == 0) {
                            AlertDialog.Builder alertAdmin = new AlertDialog.Builder(TeacherHomework.this);
                            alertAdmin.setMessage(getString(R.string.sections_not_available));
                            alertAdmin.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                }
                            });
                            alertAdmin.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    homework_class.setSelection(0);
                                }
                            });
                            alertAdmin.show();
                        } else {
                            startActivityForResult(intent, 101);
                            // finish();
                            overridePendingTransition(R.anim.enter, R.anim.nothing);
                        }


                    } else {
                        AlertDialog.Builder session_expire =
                                new AlertDialog.Builder(TeacherHomework.this, AlertDialog.THEME_HOLO_LIGHT);
                        session_expire.setMessage(
                                getString(R.string.session_starts_from) +
                                        " " + sp.getString("sessionStartDate", "") + " " +
                                        getString(R.string.to) +
                                        " " + sp.getString("sessionEndDate", "") + ".\n" +
                                        getString(R.string.to_fill_assignment_for_other_session_change_session_from_dashboard) + "."

                        );
                        session_expire.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //  finish();
                            }
                        });
                        session_expire.setCancelable(false);
                        session_expire.show();
                    }
                }
            });
            convertView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

            return convertView;
        }


        public void update(int pos) {
            this.addwork[pos] = getString(R.string.assignment_available);
            notifyDataSetChanged();
        }
    }

    private class MonthAdapter extends ArrayAdapter<String> {
        Context context;
        String[] date_of_month, work_array;
        private int[] month_background_array;
        private String[] monthFullDateName_array;
        private Calendar obj;

        public MonthAdapter(Context context, String[] month_date_array, String[] month_work_array, String[] monthFullDateName_array, int[] month_background_array, Calendar obj) {
            super(context, R.layout.day_homework, month_date_array);
            this.context = context;
            this.date_of_month = month_date_array;
            this.work_array = month_work_array;
            this.monthFullDateName_array = monthFullDateName_array;
            this.month_background_array = month_background_array;
            this.obj = obj;
        }

        @SuppressLint("NewApi")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.day_homework, null);
            }

            final ViewHolder holder = new ViewHolder();
            holder.convertview = convertView;

            holder.date_tv = (TextView) convertView.findViewById(R.id.month_date);
            holder.date_tv.setText(date_of_month[position]);

            holder.date_tv.setTextColor(month_textcolor_array[position]);

            if (date_of_month[position] == "" || position < 7)
                holder.date_tv.setBackground(null);
            else if (work_array[position] != "") {
                holder.date_tv.setBackgroundResource(month_background_array[position]);
                holder.date_tv.setTextColor(Color.WHITE);
            } else {
                month_background_array[position] = R.drawable.borders;
                holder.date_tv.setBackgroundResource(month_background_array[position]);
            }


            if (position < 7 || holder.date_tv.getText().toString().equalsIgnoreCase("") || holder.date_tv.getText().toString() == null)
                holder.date_tv.setEnabled(false);
            else {
                holder.date_tv.setOnClickListener(new View.OnClickListener() {
                    private String[] sub_name;
                    private String[] sub_id;
                    boolean expire = true;

                    @Override
                    public void onClick(final View v) {
                        String[] ab = monthFullDateName_array[position].split("-");

                        Calendar cal0 = Calendar.getInstance();
                        cal0.set(Integer.parseInt(ab[0]), Integer.parseInt(ab[1]), Integer.parseInt(ab[2]));

                        Calendar cal1 = Calendar.getInstance();
                        if (sessionStartDate.length >= 3) {
                            if (sessionStartDate[0].length() == 4)
                                cal1.set(Integer.parseInt(sessionStartDate[0]), Integer.parseInt(sessionStartDate[1]), Integer.parseInt(sessionStartDate[2]));
                            else if (sessionStartDate[2].length() == 4)
                                cal1.set(Integer.parseInt(sessionStartDate[2]), Integer.parseInt(sessionStartDate[1]), Integer.parseInt(sessionStartDate[0]));

                        }
                        Calendar cal2 = Calendar.getInstance();
                        if (sessionEndDate.length >= 3) {
                            if (sessionEndDate[0].length() == 4)
                                cal2.set(Integer.parseInt(sessionEndDate[0]), Integer.parseInt(sessionEndDate[1]), Integer.parseInt(sessionEndDate[2]));
                            else if (sessionEndDate[2].length() == 4)
                                cal2.set(Integer.parseInt(sessionEndDate[2]), Integer.parseInt(sessionEndDate[1]), Integer.parseInt(sessionEndDate[0]));

                        }


                        if (cal0.compareTo(cal1) >= 0 && cal0.compareTo(cal2) <= 0) {
                            expire = false;

                            String[][] sub_data = db.getSubjectsClasswise(cls_selected_id, section_selected_id);
                            sub_id = sub_data[0];
                            sub_name = sub_data[1];

                            Intent intent = new Intent(context, AddHomework.class);
                            intent.putExtra("sub_id", sub_id);
                            intent.putExtra("sub_name", sub_name);
                            intent.putExtra("cls_selected_id", cls_selected_id);
                            intent.putExtra("section_selected_id", section_selected_id);
                            intent.putExtra("date_of_work", monthFullDateName_array[position]);
                            intent.putExtra("pos", position);

                            if (sub_id.length == 0) {


                                showSnack(true,getString(R.string.subjects_not_available));
                            } else {
                                startActivityForResult(intent, 100);
                                //finish();
                                overridePendingTransition(R.anim.enter, R.anim.nothing);
                            }
                        } else {
                            AlertDialog.Builder session_expire =
                                    new AlertDialog.Builder(TeacherHomework.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                            session_expire.setMessage(
                                    getString(R.string.session_starts_from) +
                                            " " + sp.getString("sessionStartDate", "") + " " +
                                            getString(R.string.to) +
                                            " " + sp.getString("sessionEndDate", "") + ".\n" +
                                            getString(R.string.to_fill_assignment_for_other_session_change_session_from_dashboard) + "."

                            );
                            session_expire.setPositiveButton(getString(R.string.go_to_dashboard), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                                }
                            });
                            session_expire.setNegativeButton(getString(R.string.cancel),null);
                            session_expire.setCancelable(false);
                            session_expire.show();
                        }


                    }

                });
            }


            return convertView;
        }

        public void update(int pos) {
            this.work_array[pos] = "yes";
            this.month_background_array[pos] = R.drawable.green_circular_layout;
            notifyDataSetChanged();
        }

        private class ViewHolder {
            TextView date_tv;
            View convertview;
        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {

            monthAdapter.update(data.getIntExtra("pos", 0));
        } else if (requestCode == 101 && resultCode == RESULT_OK) {
            weekAdapter.update(data.getIntExtra("pos", 0));
        }
    }



   /* public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {

            case MotionEvent.ACTION_DOWN: {
                x1 = touchevent.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                x2 = touchevent.getX();

                if (x1 < x2) {
                    if (tb.getCurrentTabTag().equalsIgnoreCase(getString(R.string.week)))
                        weekLeft();
                    else monthRight();
                }

                // if right to left sweep event on screen
                if (x1 > x2) {
                    if (tb.getCurrentTabTag().equalsIgnoreCase(getString(R.string.week)))
                        weekRight();
                    else monthRight();
                }

                break;
            }
        }
        return false;
    }*/

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a newtext instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_tabbed, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
            return null;
        }
    }
}
