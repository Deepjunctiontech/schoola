package in.junctiontech.school;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.schoolnew.FirstScreenActivity;
import in.junctiontech.school.schoolnew.SignUpActivityNew;

public class PasswordResetActivity extends AppCompatActivity {

    private LinearLayout ll_reset_password_request_email, ll_reset_password_request_reset;
    private EditText et_reset_password_email, et_reset_password_email2, et_reset_password_otp,
            et_reset_password_enter_password, et_reset_password_confirm_password;
    private TextView tv_reset_password_already_have_otp;
    private SharedPreferences sp;
    private RelativeLayout snack_bar_rl_reset_password;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder alert;

    ImageView iv_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_confirmation);
        sp = Prefs.with(this).getSharedPreferences();
        intViews();
    }

    private void intViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        alert = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.setCancelable(false);

        snack_bar_rl_reset_password =  findViewById(R.id.snack_bar_rl_reset_password);
        ll_reset_password_request_email =  findViewById(R.id.ll_reset_password_request_email);
        ll_reset_password_request_reset =  findViewById(R.id.ll_reset_password_request_reset);

        tv_reset_password_already_have_otp =  findViewById(R.id.tv_reset_password_already_have_otp);
        et_reset_password_email =  findViewById(R.id.et_reset_password_email);
        et_reset_password_email2 =  findViewById(R.id.et_reset_password_email2);

        et_reset_password_otp =  findViewById(R.id.et_reset_password_otp);
        et_reset_password_enter_password =  findViewById(R.id.et_reset_password_enter_password);
        et_reset_password_confirm_password =  findViewById(R.id.et_reset_password_confirm_password);

        tv_reset_password_already_have_otp.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_reset_password_already_have_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_reset_password_request_email.setVisibility(View.GONE);
                ll_reset_password_request_reset.setVisibility(View.VISIBLE);
                ll_reset_password_request_reset.startAnimation(AnimationUtils.loadAnimation(PasswordResetActivity.this,
                        R.anim.bottom_up));

            }
        });

        ((Button) findViewById(R.id.sendOtpToResetPassword)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateOtpforPasswordReset();
            }
        });

//        iv_logo = findViewById(R.id.iv_logo);

//        Glide.with(this)
//                .load(Gc.getSharedPreference(Gc.LOGO_URL,this))
//                .apply(RequestOptions.skipMemoryCacheOf(true))
//                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
//                .apply(RequestOptions.circleCropTransform())
//                .into(iv_logo);
    }

    void generateOtpforPasswordReset(){
        // TODO reset password feature
        if (et_reset_password_email.getText().toString().trim().equalsIgnoreCase("")){

            return;
        }
        progressDialog.show();

        final JSONObject param = new JSONObject();
        try {
            param.put("email", et_reset_password_email.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void sendOtpToResetPassword() {

        if (!(et_reset_password_email.getText().toString().equalsIgnoreCase("")

        )) {

            et_reset_password_email.setError(null);

            progressDialog.show();

            final Map<String, Object> param = new LinkedHashMap<>();
            param.put("email", et_reset_password_email.getText().toString());

            String hostNameUrl =Config.resetPasswordSendOtp;
            Log.e("sendOtpToEmail",
                    hostNameUrl
            );


            StringRequest request = new StringRequest(Request.Method.POST,
                    hostNameUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.d("HostName1", s + "  ritu");
                            progressDialog.dismiss();
                            if (s.length() > 0) {
                                try {
                                    JSONObject resJson = new JSONObject(s);
                                    if (resJson.optString("code").equalsIgnoreCase("200")) {
                                        alert.setMessage(getString(R.string.otp_sent_successfully_on) + " " +
                                                et_reset_password_email.getText().toString() +
                                                " " + getString(R.string.please_check_it));
                                        alert.show();


                                        ll_reset_password_request_email.setVisibility(View.GONE);
                                        ll_reset_password_request_reset.setVisibility(View.VISIBLE);
                                        ll_reset_password_request_reset.startAnimation(AnimationUtils.loadAnimation(PasswordResetActivity.this,
                                                R.anim.bottom_up));

                                        et_reset_password_email2.setEnabled(false);
                                        et_reset_password_email2.setText(et_reset_password_email.getText().toString());
                                    } else {
                                        Snackbar.make(snack_bar_rl_reset_password, R.string.invalid_data, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                            }
                                        }).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(snack_bar_rl_reset_password, R.string.invalid_data, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();

                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                        Snackbar.make(snack_bar_rl_reset_password, R.string.please_retry_error_occurred, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();

                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("email", new JSONObject(param).toString());
                    Log.d("email", new JSONObject(param).toString());

                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);


        } else {
            et_reset_password_email.setError(getString(R.string.please_enter_valid_email_id));
            Snackbar.make(snack_bar_rl_reset_password, R.string.please_enter_valid_email_id, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            }).show();
        }
    }

    public void sendRequestToResetPassword(View view) {
        if (!(et_reset_password_email2.getText().toString().equalsIgnoreCase("")
                || et_reset_password_otp.getText().toString().equalsIgnoreCase("")
                ||
                !(et_reset_password_enter_password.getText().toString().equalsIgnoreCase(
                        et_reset_password_confirm_password.getText().toString()
                )
                )
        )) {

            et_reset_password_email2.setError(null);

            progressDialog.show();
            final Map<String, Object> param = new LinkedHashMap<>();
            param.put("email", et_reset_password_email2.getText().toString());
            param.put("password", et_reset_password_confirm_password.getText().toString().trim());
            param.put("otp", et_reset_password_otp.getText().toString());

            String hostNameUrl = Config.resetPasswordVerifyOtp;
            Log.e("updatePassword",
                    hostNameUrl
            );


            StringRequest request = new StringRequest(Request.Method.POST,
                    hostNameUrl
                    ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.d("updatePassword", s + "  ritu");
                            progressDialog.dismiss();
                            if (s.length() > 0) {
                                try {
                                    JSONObject resJson = new JSONObject(s);
                                    if (resJson.optString("code").equalsIgnoreCase("200")) {
                                        AlertDialog.Builder alertSuccess = new AlertDialog.Builder(PasswordResetActivity.this,
                                                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                                        alertSuccess.setTitle(getString(R.string.success));
                                        alertSuccess.setMessage(getString(R.string.password_reset_successfully_done));
                                        alertSuccess.setPositiveButton(getString(R.string.login),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        startActivity(new Intent(PasswordResetActivity.this, SignUpActivityNew.class)
                                                                .putExtra("dataAvailable", true)
                                                                .putExtra("organizationKey", "")
                                                                .putExtra("username", et_reset_password_email2.getText().toString())
                                                                .putExtra("password", et_reset_password_confirm_password.getText().toString())
                                                        );
                                                        finish();
                                                        overridePendingTransition(R.anim.enter, R.anim.exit);
                                                    }
                                                });
                                        alertSuccess.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                        alertSuccess.setCancelable(false);
                                        alertSuccess.show();
                                    }else {
                                        Snackbar.make(snack_bar_rl_reset_password, R.string.failled, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                            }
                                        }).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Snackbar.make(snack_bar_rl_reset_password, R.string.invalid_data, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();

                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {

                        Snackbar.make(snack_bar_rl_reset_password, R.string.please_retry_error_occurred, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }).show();

                    } else if (volleyError instanceof AuthFailureError) {
                        //TODO
                    } else if (volleyError instanceof ServerError) {
                        //TODO
                    } else if (volleyError instanceof NetworkError) {
                        //TODO
                    } else if (volleyError instanceof ParseError) {
                        //TODO
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String, String> header = new LinkedHashMap<String, String>();
                    header.put("Content-Type", "application/x-www-form-urlencoded");
                    return super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new LinkedHashMap<String, String>();
                    params.put("json", new JSONObject(param).toString());
                    Log.d("json", new JSONObject(param).toString());

                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request);


        } else {
            if (et_reset_password_email2.getText().toString().equalsIgnoreCase("") ||
                    !et_reset_password_email2.getText().toString().contains("@")) {

                et_reset_password_email2.setError(getString(R.string.please_enter_valid_email_id));
                Snackbar.make(snack_bar_rl_reset_password, R.string.please_enter_valid_email_id, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();

            } else if (et_reset_password_otp.getText().toString().equalsIgnoreCase("")) {

                et_reset_password_otp.setError(getString(R.string.please_enter_otp));
                Snackbar.make(snack_bar_rl_reset_password, R.string.please_enter_otp, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            } else if (!et_reset_password_enter_password.getText().toString().equalsIgnoreCase(
                    et_reset_password_confirm_password.getText().toString()
            )) {

                et_reset_password_confirm_password.setError(getString(R.string.password_mismatch));
                Snackbar.make(snack_bar_rl_reset_password, R.string.password_mismatch, Snackbar.LENGTH_LONG).setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }

        }
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, FirstScreenActivity.class));
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
        super.onBackPressed();
    }

}
