package in.junctiontech.school.schoolinformation;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class SchoolLocationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private GoogleMap mMap;
    private Double stop_lat=-37.82151777;
    private Double stop_long =144.97862575;
    private Marker st;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private LocationManager lm;
    private ProgressDialog loading;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this,true);
     //   getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_location);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buildGoogleApiClient();

        setColorApp();
        stop_lat = getIntent().getDoubleExtra("school_lat", -37.82151777);
        stop_long = getIntent().getDoubleExtra("school_long", 144.97862575);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();  //  call on onStart()   GoogleApiClient is not connected yet
        locationRequest = LocationRequest.create();

        // locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // locationRequest.setInterval(30 * 1000 * 4);
        // locationRequest.setFastestInterval(30000);  // 30 sec
        // locationRequest.setSmallestDisplacement(DISPLACEMENT);


    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                getCurrentLocationLatLong();
                //  requestLocation();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(SchoolLocationActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }


    public void getCurrentLocationLatLong() {
        // loading = ProgressDialog.show(this, "Getting Current Location", "Please wait...", false, false);
        lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (lm != null) {
            //    Toast.makeText(this, "call", Toast.LENGTH_SHORT).show();
            //   default_current_lat_long = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            //   Toast.makeText(this, "call "+default_current_lat_long, Toast.LENGTH_SHORT).show();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "Location Services Disabled !!!", Toast.LENGTH_SHORT).show();
            return;
        }
        lm.requestLocationUpdates(
                LocationManager.PASSIVE_PROVIDER,
                0,
                0, this);
        Log.d("Network", "Network");
        return;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);*/
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng latlng_default;

        if (stop_lat == null || stop_long == null) {
            //  default current location
            stop_lat = -37.82151777;
            stop_long = 144.97862575;
        }
        latlng_default = new LatLng(stop_lat, stop_long);

        st = mMap.addMarker(new MarkerOptions().position(latlng_default)
                // .title(et_name.getText().toString().isEmpty()?"Current Location":et_name.getText().toString())
                .title(getString(R.string.school_location))
                .snippet("Lat: " + stop_lat + ", Long: " + stop_long)
                //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red))
        );
        st.setDraggable(true);
        st.showInfoWindow();

        CameraPosition position = new CameraPosition.Builder()
                .target(latlng_default)
                .zoom(13).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // marker.setTitle(et_name.getText().toString());
                stop_lat = Double.parseDouble(String.format("%.6f", marker.getPosition().latitude));
                stop_long = Double.parseDouble(String.format("%.6f", marker.getPosition().longitude));

                marker.setSnippet("Lat: " + stop_lat + ", Long: " + stop_long);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                marker.showInfoWindow();

            }
        });
    }


    public void onClickGoogleApi() {
        //  currentViewSelect = (EditText) view;
        //  Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                    // .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    //  .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    //  .setTypeFilter(Place.TYPE_COUNTRY).setCountry("IN")  // work up to 8.4 play service version
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE)
                    .build();


            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, 1000);

        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("GoogleAddress", "Place: " + place.getName());
                place.getAddress();
                //      currentViewSelect.setText(place.getAddress() + "");
                LatLng latlong = place.getLatLng();
                // Toast.makeText(this, " " + latlong.latitude + " " + latlong.longitude, Toast.LENGTH_SHORT).show();

                String placeDetailsStr = place.getName() + "\n"
                        + place.getId() + "\n"
                        + place.getLatLng().toString() + "\n"
                        + place.getAddress() + "\n"
                        + place.getAttributions();

                stop_lat = latlong.latitude;
                stop_long = latlong.longitude;
                st.setPosition(latlong);
                CameraPosition position = new CameraPosition.Builder()
                        .target(latlong)
                        .zoom(13).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("GoogleAddress", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
                //Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                getCurrentLocationLatLong();
                //  requestLocation();

            } else {
                onSupportNavigateUp();
                //Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        // loading.dismiss();
        //    Toast.makeText(this, "LocationChanged", Toast.LENGTH_SHORT).show();
        if (location != null) {
            stop_lat = location.getLatitude();
            stop_long = location.getLongitude();
            st.setSnippet("Lat: " + "" + location.getLatitude() + ", Long: " + "" + location.getLongitude());
            LatLng current;
            st.hideInfoWindow();
            st.showInfoWindow();
            st.setPosition(current = new LatLng(location.getLatitude(), location.getLongitude()));
            CameraPosition position = new CameraPosition.Builder()
                    .target(current)
                    .zoom(13).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onSupportNavigateUp() {

        Intent intent = new Intent(this, SchoolInformationActivity.class);
        if (stop_lat == null || stop_long == null) {
            //  default current location
            stop_lat = 23.231116;
            stop_long = 77.433592;
        }
        intent.putExtra("school_lat", stop_lat);
        intent.putExtra("school_long", stop_long);
        if (stop_lat == getIntent().getDoubleExtra("school_lat", 23.231116) && stop_long == getIntent().getDoubleExtra("school_long", 77.433592))
            setResult(RESULT_CANCELED, intent);
        else setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);


        return false;
    }

    @Override
    public void onBackPressed() {
        if (stop_lat == null || stop_long == null) {
            //  default current location
            stop_lat = 23.231116;
            stop_long = 77.433592;
        }
        Intent intent = new Intent(this, SchoolInformationActivity.class);
        intent.putExtra("school_lat", stop_lat);
        intent.putExtra("school_long", stop_long);

        if (stop_lat == getIntent().getDoubleExtra("school_lat", 23.231116) && stop_long == getIntent().getDoubleExtra("school_long", 77.433592))
            setResult(RESULT_CANCELED, intent);
        else setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_location_search, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_location_search) onClickGoogleApi();
        return super.onOptionsItemSelected(item);
    }
}
