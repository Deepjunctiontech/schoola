package in.junctiontech.school.schoolinformation;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolDetailsEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

/**
 * Created by JAYDEVI BHADE on 01-05-2017.
 * Updated by Deep on 13-04-2018.
 */

public class SchoolInformationActivity extends AppCompatActivity  {

    private CircleImageView school_logo;
    private MainDatabase mDb ;

    private SharedPreferences sp;
    private Bitmap bitmap;
    private File targetLocation, targetSchoolImageLocation;
    private EditText et_school_information;
    private SchoolDetailsEntity schoolDetails1;

    private ProgressDialog progressbar;
    private boolean isAnyChanges = false;
    private Double school_long, school_lat;
    private Marker st;
    private AppBarLayout app_bar1;
    private CoordinatorLayout snackbar;
    private boolean isImageChanged = false;//false for no change . true for  changed
    private int colorIs;
    String initial_school_information;
    private boolean isLogoChanged;
    private boolean isSchoolImageChanged;
    String whichImage  ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(in.junctiontech.school.R.layout.layout_school_information);

        mDb = MainDatabase.getDatabase(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.saving));
        setColorApp();
        ((CollapsingToolbarLayout) findViewById(R.id.toolbar_layout)).setContentScrimColor(colorIs);

        snackbar = findViewById(R.id.layout_school_information);
        app_bar1 = findViewById(R.id.app_bar1);
        et_school_information = findViewById(R.id.et_school_information);

        school_logo = findViewById(R.id.iv_school_information_logo);


        sp = Prefs.with(this).getSharedPreferences();
        targetLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                sp.getString("organization_name", "") + "_logo" + ".jpg");
        targetSchoolImageLocation = new File(Environment.getExternalStorageDirectory().toString() +
                "/" + Config.FOLDER_NAME + "/",
                sp.getString("organization_name", "") + "_school_image" + ".jpg");


        if (!(Gc.ADMIN.equalsIgnoreCase(Gc.getSharedPreference(Gc.SIGNEDINUSERROLE, this)))) {
            et_school_information.setEnabled(false);
            et_school_information.setFocusable(false);
            app_bar1.setEnabled(false);
            school_logo.setEnabled(false);
        }


        school_logo.setOnClickListener(v -> {
            if (takePermission()) {
                if (schoolDetails1 != null) {
                    whichImage = "logo";
                    CropImage.activity()
                            .setAspectRatio(150,150)
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .start(this);

                } else {
                    whichImage = null;
                    Snackbar.make(snackbar,
                            "School Data not available Try Again !",
                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                }


            }
        });

        app_bar1.setOnClickListener(v -> {
            if (takePermission()) {
                if (schoolDetails1 != null) {
                    whichImage = "schoolimage";
                    CropImage.activity()
                            .setAspectRatio(160,90)
                            .setMaxCropResultSize(1920,1080)
                            .start(this);
//                    startActivityForResult(new Intent(SchoolInformationActivity.this, CropImageActivity.class)
//
//                                    .putExtra("Id", schoolDetails1.Id)
//                                    .putExtra("isSchoolImage", true),
//                            Config.SCHOOL_IMAGE);
//                    overridePendingTransition(R.anim.enter, R.anim.nothing);
                } else {
                    whichImage = null;
                    Snackbar.make(snackbar,
                            "School Data not available Try Again !",
                            Snackbar.LENGTH_LONG).setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                }
            }
        });


        et_school_information.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(getCurrentFocus() == et_school_information &&  !(s.toString().trim().equals(Strings.nullToEmpty(initial_school_information)))) {
                    isAnyChanges = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        mDb.schoolDetailsModel().getSchoolDetailsLive().observe(this, schoolDetailsEntity -> {
            schoolDetails1 = schoolDetailsEntity;
            setValuesInUi();
            displayImages();
//            onMapReady(mMap);
        });
    }

    void setValuesInUi() {
        if (schoolDetails1 != null) {
            Objects.requireNonNull(getSupportActionBar()).setTitle(schoolDetails1.SchoolName);
            initial_school_information = Strings.nullToEmpty(schoolDetails1.schoolDescription);
            et_school_information.setText(schoolDetails1.schoolDescription);

            ((TextView) findViewById(R.id.tv_school_information_board)).setText(
                    getString(R.string.school_board) + " : " + schoolDetails1.Board + "\n" +
                            getString(R.string.affiliated_by) + " : " + schoolDetails1.AffiliatedBy
            );

            ((TextView) findViewById(R.id.tv_school_information_contact_us)).setText(
                    (schoolDetails1.SchoolAddress) + " " +
                            (schoolDetails1.City) + " " +
                            (schoolDetails1.District) + " " +
                            (schoolDetails1.State) + ", " +
                            (schoolDetails1.Country) + ", " +
                            (schoolDetails1.PIN) + " " +
                            "\n\n" +
                            getString(R.string.mobile_number) + " : " +
                            (schoolDetails1.Mobile) + ", " +
                            (schoolDetails1.AlternateMobile) + "\n" +
                            getString(R.string.landline_number) + " : " +
                            (schoolDetails1.Landline) + "\n" +
                            getString(R.string.email_id) + " : " + (schoolDetails1.Email));


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void displayImage() {
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(targetLocation));
            school_logo.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            app_bar1.setBackground(new BitmapDrawable(this.getResources(), BitmapFactory.decodeStream(new FileInputStream(targetSchoolImageLocation))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void displayImages() {
        if(!(Gc.getSharedPreference(Gc.LOGO_URL,this).equalsIgnoreCase("")))
        Glide.with(this)
                .load(Gc.getSharedPreference(Gc.LOGO_URL, this))
                .apply(RequestOptions.placeholderOf(R.drawable.ic_junction))
                .apply(RequestOptions.errorOf(R.drawable.ic_junction))
                .apply(RequestOptions.circleCropTransform())
                .into(school_logo);

        Glide.with(this)
                .load(Gc.getSharedPreference(Gc.SCHOOLIMAGE, this))
                .apply(RequestOptions.placeholderOf(R.drawable.s1))
                .apply(RequestOptions.errorOf(R.drawable.s1))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        app_bar1.setBackground(resource);
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();

        return true;
    }

    @Override
    public void onBackPressed() {
        if (isAnyChanges) {

            android.app.AlertDialog.Builder alertUpdate = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alertUpdate.setTitle(getString(R.string.verification));
            alertUpdate.setIcon(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_alert, null));


            alertUpdate.setPositiveButton(R.string.save, (dialog, which) -> {
                try {
                    saveInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
            alertUpdate.setNegativeButton(R.string.exit, (dialog, which) -> {
                super.onBackPressed();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);

            });
            alertUpdate.setCancelable(false);
            alertUpdate.show();
        }else{
            super.onBackPressed();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    private void saveInfo() throws JSONException {

        progressbar.show();

        final JSONObject param = new JSONObject();
        final JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("Id", schoolDetails1.Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        schoolDetails1.schoolMapLocation = (school_lat + "," + school_long);
        schoolDetails1.schoolDescription = ((et_school_information.getText().toString().trim().replaceAll("'", "+")));

        param.put("schoolDescription",schoolDetails1.schoolDescription);

        param.put("schoolMapLocation", schoolDetails1.schoolMapLocation);
        if (isLogoChanged){
            Bitmap image = ((BitmapDrawable) school_logo.getDrawable()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("Logo", encodedImage);

        }

        if (isSchoolImageChanged){
            Bitmap image = ((BitmapDrawable) app_bar1.getBackground()).getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            param.put("schoolImage", encodedImage);
        }

        final JSONObject param_main = new JSONObject();
        param_main.put("data", param);
        param_main.put("filter", job_filter);

        JSONObject p = new JSONObject();

        p.put("id", schoolDetails1.Id);

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "schoolDetail/schoolDetails/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(), job -> {
            String code = job.optString("code");

            progressbar.cancel();

            switch (code) {
                case Gc.APIRESPONSE200:

                    try {
                        final SchoolDetailsEntity schoolDetailsEntity = new Gson().fromJson(job.getJSONObject("result").optString("schoolDetails"),SchoolDetailsEntity.class);
                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.LOGO_URL, schoolDetailsEntity.Logo);
                        setDefaults.put(Gc.SCHOOLIMAGE,schoolDetailsEntity.schoolImage );
                        Gc.setSharedPreference(setDefaults, SchoolInformationActivity.this);

                        new Thread(() -> {
                            mDb.schoolDetailsModel().insertSchoolDetails(schoolDetailsEntity);
                        }).start();

                        finish();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, SchoolInformationActivity.this);

                    Intent intent = new Intent(SchoolInformationActivity.this, AdminNavigationDrawerNew.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    finish();
                    break;
                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, SchoolInformationActivity.this);
//TODO check if this works alright failure of accessToken

                    Intent intent1 = new Intent(SchoolInformationActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                    default:

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.layout_school_information),R.color.fragment_first_blue);

            }
        }, volleyError -> {
            progressbar.cancel();
            Config.responseVolleyErrorHandler(SchoolInformationActivity.this,volleyError,findViewById(R.id.layout_school_information));


        }) {
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent b) {
        super.onActivityResult(requestCode, resultCode, b);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(b);
            isAnyChanges = true;
            Uri resultUri = result.getUri();

            if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("logo")){
                isLogoChanged = true;

                Glide.with(this)
                        .load(new File(resultUri.getPath())) // Uri of the picture
                        .into(school_logo);

            }else if (Strings.nullToEmpty(whichImage).equalsIgnoreCase("schoolimage")){
                isSchoolImageChanged = true;

                Glide.with(this)
                        .load(new File(resultUri.getPath())) // Uri of the picture
                        .into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                                app_bar1.setBackground(resource);
                            }
                        });
            }else {
                Config.responseSnackBarHandler(getString(R.string.report_not_submitted_try_again),
                        findViewById(R.id.layout_school_information), R.color.fragment_first_blue);
            }


            String message = Objects.requireNonNull(b.getExtras()).getString("message");

            Config.responseSnackBarHandler(message,
                                findViewById(R.id.layout_school_information),R.color.fragment_first_green);
        }

    }


    private boolean locationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                }, Config.LOCATION);
            }


            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {
        //   boolean res = grantResults[0] == PackageManager.PERMISSION_GRANTED;


        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults);
        if (permsRequestCode == Config.IMAGE_LOGO_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
                        //denied
                        Log.e("denied", permissions[0]);
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                            //allowed
                            Log.e("allowed", permissions[0]);

                        } else {
                            //set to never ask again
                            Log.e("set to never ask again", permissions[0]);
                            //do something here.
                            showRationale(getString(R.string.permission_denied), getString(
                                    R.string.permission_denied_external_storage));

                        }
                    }
                }
            }


        }
    }

    private void showRationale(String permission, String permission_denied_location) {
        AlertDialog.Builder alertLocationInfo = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        alertLocationInfo.setTitle(permission);
        alertLocationInfo.setIcon(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_alert, null));
        alertLocationInfo.setMessage(permission_denied_location
                + "\n" + getString(R.string.setting_permission_path_on));
        alertLocationInfo.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, Config.LOCATION);
            }
        });
        alertLocationInfo.setNegativeButton(getString(R.string.deny), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                        .findFragmentById(R.id.map);
//                mapFragment.getMapAsync(SchoolInformationActivity.this);
            }
        });
        alertLocationInfo.setCancelable(false);
        alertLocationInfo.show();
    }

    public boolean takePermission() {

        if (
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        ||
                        ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED
                ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, Config.IMAGE_LOGO_CODE);
            }
            return false;
        } else return true;

    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        // getWindow().setStatusBarColor(colorIs);
        //getWindow().setNavigationBarColor(colorIs);
        ((TextView) findViewById(R.id.tv_school_information)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_school_information_board_title)).setTextColor(colorIs);
        ((TextView) findViewById(R.id.tv_school_information_contact_us_title)).setTextColor(colorIs);

    }
}
