package in.junctiontech.school.admin.userpermission;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;

public class UserSelectionForPermissionActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private RecyclerView recycler_view;
    private String db_name;
    private ProgressDialog progressDialog;
    private ArrayList<MasterEntry> masterEntryPosList = new ArrayList<>();
    private UserPermissionAdapter adapter;
    private boolean logoutAlert;
    private View snackbar;
    private int colorIs;
    private String keyName;
    private int activityID;
    private ArrayList<MobilePermission> permissionList = new ArrayList<>();

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        setContentView(R.layout.fragment_view_all_list);
        setColorApp();
        db_name = sp.getString("organization_name", "Not Found");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        snackbar = findViewById(R.id.fl_fragment_all_list);

        keyName = getIntent().getStringExtra("keyName");
        activityID = getIntent().getIntExtra("activityID",0);
        getSupportActionBar().setSubtitle(keyName);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        //  appTopicList = (ArrayList<AppTopicList>) getIntent().getSerializableExtra("data");
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_list_of_data);
        final SwipeRefreshLayout  swipe_refresh_list =(SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();
        fetchMasterEntryData();
        try {
            getPermissionForUserTypeBasis();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);
        adapter = new UserPermissionAdapter(this, masterEntryPosList, colorIs, keyName,activityID);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    private void fetchMasterEntryData() {
        progressDialog.show();
        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("MasterEntryName", "UserType");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MasterEntryApi.php?databaseName=" + db_name +
                "&data=" + (new JSONObject(param1));
        Log.e("MasterEntryData", url);

        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                url
                //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntryData", s);
                        progressDialog.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MasterEntry obj = (new Gson()).fromJson(s, new TypeToken<MasterEntry>() {
                                }.getType());
                                masterEntryPosList = obj.getResult();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !(UserSelectionForPermissionActivity.this).isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UserSelectionForPermissionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);

                            adapter.updateList(masterEntryPosList, permissionList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MasterEntryData", volleyError.toString());
                progressDialog.dismiss();
                adapter.updateList(masterEntryPosList, permissionList);
                Config.responseVolleyErrorHandler(UserSelectionForPermissionActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                params.put("data", (new JSONObject(param1)).toString());
                //  Log.d("loginjson", new JSONObject(param).toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    private void setPermissionForUserTypeBasis() throws JSONException {

        JSONArray jsonArray = new JSONArray();


        for (MasterEntry masterEntry : masterEntryPosList) {
            JSONObject jobPer = new JSONObject();
            if (masterEntry.getMobilePermission() != null
                    ) {
                MobilePermission mobilePermissionObj = masterEntry.getMobilePermission();
                jobPer.put("UserType", masterEntry.getMasterEntryId());
                jobPer.put("Permission", mobilePermissionObj.getPermission());
                jsonArray.put(jobPer);

            }
        }

        JSONObject jsonObjectMain = new JSONObject();
        jsonObjectMain.put("Type", "userType");
        jsonObjectMain.put("ActivityName", keyName);
        jsonObjectMain.put("ActivityID", activityID);
        jsonObjectMain.put("data", jsonArray);

        if (jsonArray.length() > 0) {
            progressDialog.show();

            String url = sp.getString("HostName", "Not Found")
                    + sp.getString(Config.applicationVersionName, "Not Found")
                    + "MobilepermissionApi.php?databaseName=" + db_name;
            Log.e("MasterEntryData", url);
            Log.e("MasterEntryData", jsonObjectMain.toString());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObjectMain, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    Log.e("POSTResult", jsonObject.toString());
                    progressDialog.dismiss();
                    Config.responseSnackBarHandler(jsonObject.optString("message"), snackbar, (jsonObject.optString("code", "").equalsIgnoreCase("200") ? getResources().getColor(android.R.color.holo_green_dark) : getResources().getColor(android.R.color.holo_red_dark)));


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e("MasterEntryData", volleyError.toString());
                    progressDialog.dismiss();
                    Config.responseVolleyErrorHandler(UserSelectionForPermissionActivity.this, volleyError, snackbar);

                }
            });

            AppRequestQueueController.getInstance(this).cancelRequestByTag("addPermission");
            request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
            AppRequestQueueController.getInstance(this).addToRequestQueue(request, "addPermission");
        } else {
            Config.responseSnackBarHandler(getString(R.string.data_not_available), snackbar);

        }
    }

    private void getPermissionForUserTypeBasis() throws JSONException {
        progressDialog.show();
        JSONObject param = new JSONObject();
        param.put("ActivityID", activityID);
        param.put("ActivityName", keyName);
        param.put("UserId", "null");

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", param);

        String url = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "MobilepermissionApi.php?databaseName=" + db_name +
                "&data=" + (new JSONObject(param1));
        Log.e("keyName", url);


        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                url
                //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MasterEntryData", s);
                        progressDialog.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                MobilePermission obj = (new Gson()).fromJson(s, new TypeToken<MobilePermission>() {
                                }.getType());
                                permissionList = obj.getResult();
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(UserSelectionForPermissionActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502"))
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        adapter.updateList(masterEntryPosList, permissionList);
                        progressDialog.cancel();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MasterEntryData", volleyError.toString());
                progressDialog.dismiss();
                adapter.updateList(masterEntryPosList, permissionList);
                Config.responseVolleyErrorHandler(UserSelectionForPermissionActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_fee_receipt, menu);

        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_save:
                showPasswordVerification();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showPasswordVerification() {
        AlertDialog.Builder alertUpdate = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertUpdate.setTitle(getString(R.string.verification));
        alertUpdate.setIcon(getResources().getDrawable(R.drawable.ic_alert));
        View view_update = getLayoutInflater().inflate(R.layout.layout_password_verification, null);

        final EditText et_name = (EditText) view_update.findViewById(R.id.et_password_verification_userid);
        final EditText et_pwd = (EditText) view_update.findViewById(R.id.et_password_verification_password);

        alertUpdate.setPositiveButton(R.string.next, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_name.getText().toString().trim().length() == 0) {
                    Toast.makeText(UserSelectionForPermissionActivity.this,
                            getString(R.string.section_name_can_not_be_blank), Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        checkUserValidation(et_name.getText().toString(), et_pwd.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        alertUpdate.setNegativeButton(R.string.cancel, null);
        alertUpdate.setView(view_update);
        alertUpdate.setCancelable(false);
        alertUpdate.show();
    }

    private void checkUserValidation(String userId, String pwd) throws JSONException {
        progressDialog.show();

        final JSONObject param = new JSONObject();
        param.put("database_name", sp.getString("organization_name", ""));
        param.put("username", userId);
        param.put("password", pwd);
        param.put("url", "android");

        String loginUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "LoginApi.php";

        Log.e("LoginUrl", loginUrl);

        Log.e("LoginData", param.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, loginUrl, param,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject job) {
                        progressDialog.cancel();

                        Log.e("Response:", job.toString());

                        if (job.optString("code").equalsIgnoreCase("200")) {
                            try {
                                setPermissionForUserTypeBasis();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else Config.responseSnackBarHandler(job.optString("message"), snackbar);

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.cancel();
                        Config.responseVolleyErrorHandler(UserSelectionForPermissionActivity.this, volleyError, snackbar);
                    }
                });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(jsonObjectRequest, "userVerification");


    }


}
