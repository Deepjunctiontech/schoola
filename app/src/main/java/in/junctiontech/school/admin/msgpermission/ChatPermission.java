package in.junctiontech.school.admin.msgpermission;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 09-08-2017.
 */

public class ChatPermission implements Serializable {
    String FromUserType,ToUserType,Permission;
    ChatPermission chatPermission;

    public ChatPermission(String FromUserType, String ToUserType, boolean Permission) {
    this.FromUserType= FromUserType;
    this.ToUserType= ToUserType;
    this.Permission= Permission?"1":"0";
    }

    public ChatPermission getChatPermission() {
        return chatPermission;
    }

    public void setChatPermission(ChatPermission chatPermission) {
        this.chatPermission = chatPermission;
    }

    public String getFromUserType() {
        return FromUserType;
    }

    public void setFromUserType(String fromUserType) {
        FromUserType = fromUserType;
    }

    public String getToUserType() {
        return ToUserType;
    }

    public void setToUserType(String toUserType) {
        ToUserType = toUserType;
    }

    public String getPermission() {
        return Permission;
    }

    public void setPermission(String permission) {
        Permission = permission;
    }

    public ArrayList<ChatPermission> getResult() {
        return result;
    }

    public void setResult(ArrayList<ChatPermission> result) {
        this.result = result;
    }

    ArrayList<ChatPermission> result;

 }
