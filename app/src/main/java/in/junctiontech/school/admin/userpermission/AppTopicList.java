package in.junctiontech.school.admin.userpermission;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by LENEVO on 02-08-2017.
 */

public class AppTopicList implements Serializable {
    String topicName,keyName;
    boolean isSubTopc;
    int activityID;
    ArrayList<AppTopicList> subTopicList;

    public AppTopicList(int activityID, String keyName, boolean isSubTopc) {
        this.activityID = activityID;
        this.keyName = keyName;
        this.isSubTopc = isSubTopc;
    }

    public AppTopicList(int activityID,String keyName, ArrayList<AppTopicList> subTopicList) {
        this.activityID = activityID;
        this.keyName = keyName;
        this.subTopicList = subTopicList;
    }



    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public ArrayList<AppTopicList> getSubTopicList() {
        return subTopicList;
    }

    public void setSubTopicList(ArrayList<AppTopicList> subTopicList) {
        this.subTopicList = subTopicList;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public boolean isSubTopc() {
        return isSubTopc;
    }

    public void setSubTopc(boolean subTopc) {
        isSubTopc = subTopc;
    }
}
