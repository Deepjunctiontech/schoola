package in.junctiontech.school.admin.msgpermission;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;
import in.junctiontech.school.schoolnew.permissions.communication.MessengerPermissionsActivity;

/**
 * Created by JAYDEVI BHADE on 16-08-2017.
 */


public class MsgPermissionAdapter extends RecyclerView.Adapter<MsgPermissionAdapter.ViewHoler> {

    MessengerPermissionsActivity messengerPermissionsActivity;
    ArrayList<MasterEntry> masterEntryPosList = new ArrayList<>();


    public MsgPermissionAdapter(MessengerPermissionsActivity messengerPermissionsActivity,
                                ArrayList<MasterEntry> masterEntryPosList) {
        this.messengerPermissionsActivity = messengerPermissionsActivity;

        this.masterEntryPosList = masterEntryPosList;

    }

    @Override
    public ViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MsgPermissionAdapter.ViewHoler(LayoutInflater.from(messengerPermissionsActivity).inflate(R.layout.activity_messenger_permissions, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHoler holder, int position) {
        holder.rl_msg_permission_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ll_user_type_permission_list.setVisibility( holder.ll_user_type_permission_list.getVisibility() == View.VISIBLE ? View.GONE :
                        View.VISIBLE);
            }
        });
        holder.tv_user_type.setText(masterEntryPosList.get(position).getMasterEntryValue());
        holder.ll_user_type_permission_list.removeAllViews();
        for (final ChatPermission chatPermissionobj : masterEntryPosList.get(position).getChatPermissionArrayList()) {

           View itemMsgPer = LayoutInflater.from(messengerPermissionsActivity).inflate(R.layout.item_msg_permission, null);
           TextView tv_from_user_type = (TextView) itemMsgPer.findViewById(R.id.tv_from_user_type);
           TextView tv_to_user_type = (TextView) itemMsgPer.findViewById(R.id.tv_to_user_type);
           CheckBox ck_from_user_type_to = (CheckBox) itemMsgPer.findViewById(R.id.ck_from_user_type_to);
           tv_from_user_type.setText(chatPermissionobj.getFromUserType());
           tv_to_user_type.setText(chatPermissionobj.getToUserType());
           ck_from_user_type_to.setChecked( chatPermissionobj.getPermission().equalsIgnoreCase("1")?true:false);

           ck_from_user_type_to.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                   chatPermissionobj.setPermission(isChecked ? "1" : "0");
               }
           });

           holder.ll_user_type_permission_list.addView(itemMsgPer);

        }

    }


    @Override
    public int getItemCount() {
        return masterEntryPosList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void updateList(ArrayList<MasterEntry> masterEntryPosList ) {

        this.masterEntryPosList = masterEntryPosList;
        notifyDataSetChanged();
    }

    public class ViewHoler extends RecyclerView.ViewHolder {
        private final RelativeLayout rl_msg_permission_admin;
        private final LinearLayout ll_user_type_permission_list;
        private final TextView tv_user_type;

        public ViewHoler(View itemView) {
            super(itemView);
            tv_user_type = (TextView) itemView.findViewById(R.id.tv_user_type);
            rl_msg_permission_admin = (RelativeLayout) itemView.findViewById(R.id.rl_msg_permission_admin);
            ll_user_type_permission_list = (LinearLayout) itemView.findViewById(R.id.ll_user_type_permission_list);

        }
    }


}
