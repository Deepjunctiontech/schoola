package in.junctiontech.school.admin.userpermission;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;

public class SubTopicListActivity extends AppCompatActivity {
    private RecyclerView recycler_view;
    private ArrayList<AppTopicList> appTopicList = new ArrayList<>();
    private AppTopicListAdapter adapter;
    private String  keyName;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_view_all_list);
        setColorApp();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        keyName = getIntent().getStringExtra("keyName");
        getSupportActionBar().setSubtitle(keyName);
        appTopicList = (ArrayList<AppTopicList>) getIntent().getSerializableExtra("data");
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_list_of_data);
        final SwipeRefreshLayout  swipe_refresh_list =(SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
        setupRecycler();
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);
        adapter = new AppTopicListAdapter(this, appTopicList,false/*For sub topic*/);
        recycler_view.setAdapter(adapter);
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }
}
