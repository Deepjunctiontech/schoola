package in.junctiontech.school.admin.userpermission;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;

public class PermissionsActivity extends AppCompatActivity {

    private RecyclerView recycler_view;
    private ArrayList<AppTopicList> appTopicList = new ArrayList<>();
    private AppTopicListAdapter adapter;
    private SharedPreferences sp;
    private String db_name;
    private View snackbar;
    private ProgressDialog progressDialog;

    private void setColorApp() {

        int colorIs = Config.getAppColor(this, true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));
    }

    ArrayList<String> parentNameList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_all_list);
        sp = Prefs.with(this).getSharedPreferences();
        setColorApp();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db_name = sp.getString("organization_name", "Not Found");
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_list_of_data);
        snackbar = findViewById(R.id.fl_fragment_all_list);
        ((SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list)).setRefreshing(false);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));

        final SwipeRefreshLayout  swipe_refresh_list =(SwipeRefreshLayout) findViewById(R.id.swipe_refresh_list);
        swipe_refresh_list.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_list.setRefreshing(false);
            }
        });
     /*   appTopicList.add(new AppTopicList(getString(R.string.class_text), "createClassSection", new ArrayList<AppTopicList>()));

        ArrayList<AppTopicList> feesSubTopicList = new ArrayList<AppTopicList>();
        feesSubTopicList.add(new AppTopicList(getString(R.string.create_fee_type), "createFeeType", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.class_fee), "classFee", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.fee_account), "feeAccount", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.fee_payment), "feePayment", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.update_student_fee), "updateStudentFee", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.fee_receipt), "feeReceipt", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.transport_fee), "transportFee", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.fee_reminder), "feeReminder", true));
        feesSubTopicList.add(new AppTopicList(getString(R.string.view_due_payment), "viewDuePayment", true));

        appTopicList.add(new AppTopicList(getString(R.string.fee), "manageFee", feesSubTopicList));

        ArrayList<AppTopicList> studentSubTopicList = new ArrayList<AppTopicList>();
        studentSubTopicList.add(new AppTopicList(getString(R.string.create_student), "createStudent", true));
        studentSubTopicList.add(new AppTopicList(getString(R.string.update_student_fee), "updateStudentFee", true));
        studentSubTopicList.add(new AppTopicList(getString(R.string.change_class), "changeClass", true));
        studentSubTopicList.add(new AppTopicList(getString(R.string.promotion_to_next_class), "promoteToNextClass", true));
        studentSubTopicList.add(new AppTopicList(getString(R.string.attendance_entry), "studentAttendanceEntry", true));
        // studentSubTopicList.add(new AppTopicList(  getString(R.string.report_view),"reportView",true));

        appTopicList.add(new AppTopicList(getString(R.string.student), "Student", studentSubTopicList));

        appTopicList.add(new AppTopicList(getString(R.string.subject), "createSubject", new ArrayList<AppTopicList>()));

        ArrayList<AppTopicList> stafftSubTopicList = new ArrayList<AppTopicList>();
        stafftSubTopicList.add(new AppTopicList(getString(R.string.create_staff), "createStaff", true));
        stafftSubTopicList.add(new AppTopicList(getString(R.string.staff_attendance), "staffAttendance", true));

        appTopicList.add(new AppTopicList(getString(R.string.staff), "staff", stafftSubTopicList));


        appTopicList.add(new AppTopicList(getString(R.string.user_list), "userList", new ArrayList<AppTopicList>()));

        ArrayList<AppTopicList> homeworkSubTopicList = new ArrayList<AppTopicList>();
        homeworkSubTopicList.add(new AppTopicList(getString(R.string.homework_report), "homeworkReport", true));
        homeworkSubTopicList.add(new AppTopicList(getString(R.string.homework_entry), "homeworkEntry", true));
        appTopicList.add(new AppTopicList(getString(R.string.homework), "homework", homeworkSubTopicList));

        ArrayList<AppTopicList> attendanceSubTopicList = new ArrayList<>();
        attendanceSubTopicList.add(new AppTopicList(getString(R.string.attendance_report), "attendanceReport", true));
        attendanceSubTopicList.add(new AppTopicList(getString(R.string.attendance_entry), "studentAttendanceEntry", true));
        appTopicList.add(new AppTopicList(getString(R.string.attendance), "attendance", attendanceSubTopicList));

        ArrayList<AppTopicList> examSubTopicList = new ArrayList<AppTopicList>();
        examSubTopicList.add(new AppTopicList(getString(R.string.create_grades), "createGrades", true));
        examSubTopicList.add(new AppTopicList(getString(R.string.grade_marks_setup), "gradeMarksSetup", true));
        examSubTopicList.add(new AppTopicList(getString(R.string.exam_type), "createExamType", true));
        examSubTopicList.add(new AppTopicList(getString(R.string.exam_results), "fillExamResult", true));
        // examSubTopicList.add(new AppTopicList(  getString(R.string.report_view),"reportView",true));
        examSubTopicList.add(new AppTopicList(getString(R.string.exam_result_report), "examResultReport", true));

        appTopicList.add(new AppTopicList(getString(R.string.exam), "exam", examSubTopicList));

        ArrayList<AppTopicList> messengerList = new ArrayList<AppTopicList>();
        messengerList.add(new AppTopicList(getString(R.string.notification), "createNotification", true));
        messengerList.add(new AppTopicList(getString(R.string.sms), "sms", true));
        //  messengerList.add(new AppTopicList(getString(R.string.messager),"messenger",  true));
        appTopicList.add(new AppTopicList(getString(R.string.messager), "messengerActivity", messengerList));

        appTopicList.add(new AppTopicList(getString(R.string.transport_text), "transport", new ArrayList<AppTopicList>()));
        appTopicList.add(new AppTopicList(getString(R.string.gallery), "gallery", new ArrayList<AppTopicList>()));

        ArrayList<AppTopicList> timetableSubTopicList = new ArrayList<AppTopicList>();
        timetableSubTopicList.add(new AppTopicList(getString(R.string.create_slots), "CreateSlotTimeTable", true));
        timetableSubTopicList.add(new AppTopicList(getString(R.string.day_wise), "DayWiseTimeTable", true));
        timetableSubTopicList.add(new AppTopicList(getString(R.string.date_wise), "DateWiseTimeTable", true));
        timetableSubTopicList.add(new AppTopicList(getString(R.string.view_time_table), "viewTimeTable", true));

        appTopicList.add(new AppTopicList(getString(R.string.time_table), "createTimeTable", timetableSubTopicList));

        appTopicList.add(new AppTopicList(getString(R.string.teacher_notes), "teachersNotes", new ArrayList<AppTopicList>()));
        appTopicList.add(new AppTopicList(getString(R.string.online_test), "onlineTest", new ArrayList<AppTopicList>()));
        appTopicList.add(new AppTopicList(getString(R.string.report_queries), "reportBug", new ArrayList<AppTopicList>()));
*/
        setActivityName();

    }

    private void getMobileActivityNameFromServer() {
        progressDialog.show();
        String url = Config.ORGANIZATION_ID_CHECK
                + "MobileActivity.php?databaseName=" + db_name;
        Log.e("url", url);

        StringRequest request = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("MobileActivityName", s);
                        //  progressDialog.cancel();
                        ArrayList<AppTopicList> permissionList;
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200) {
                                sp.edit().putString("activityNameJson", s).commit();
                                setActivityName();
                            } else
                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        progressDialog.cancel();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("MobileActivityName", volleyError.toString());
                progressDialog.dismiss();

                Config.responseVolleyErrorHandler(PermissionsActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
    }

    private void setActivityName() {
        if (sp.getString("activityNameJson", "").equalsIgnoreCase(""))
            getMobileActivityNameFromServer();
        else {
            parentNameList.clear();
            MobileActivityNameModel obj = (new Gson()).fromJson(sp.getString("activityNameJson", ""), new TypeToken<MobileActivityNameModel>() {
            }.getType());
            for (MobileActivityNameModel mblName : obj.getResult()) {
                if (0== mblName.getParent()  )
                    appTopicList.add(new AppTopicList(mblName.getActivityID(), mblName.getActivityName(),filter(obj.getResult(), mblName.getActivityID())));

            /*     if (null== mblName.getParent() )
                    appTopicList.add(new AppTopicList(mblName.getActivityID(), mblName.getActivityName(), new ArrayList<AppTopicList>()));
                else  if (null!= mblName.getParent()&& !parentNameList.contains(mblName.getParent()))
                    appTopicList.add(new AppTopicList(mblName.getActivityID(), mblName.getActivityName(), filter(obj.getResult(), mblName.getParent())));*/
            }
            setupRecycler();
        }
    }

    public ArrayList<AppTopicList> filter(ArrayList<MobileActivityNameModel> models, int query) {

        final ArrayList<AppTopicList> filteredModelList = new ArrayList<>();

        Log.e("query",query+" ritu");

        for (MobileActivityNameModel model : models) {

            if (0 != model.getParent() && model.getParent()==query) {
                Log.e("query",model.getActivityID()+" ritu "+ model.getActivityName());
                filteredModelList.add(new AppTopicList(model.getActivityID(), model.getActivityName(), true));
            }
        }
        return filteredModelList;
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);
        adapter = new AppTopicListAdapter(this, appTopicList, true/*for topic*/);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("name", "Arun Verma");
    }
}
