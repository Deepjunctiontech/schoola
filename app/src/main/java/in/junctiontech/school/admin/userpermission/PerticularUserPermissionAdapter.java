//package in.junctiontech.school.Admin.userpermission;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.res.ColorStateList;
//import android.graphics.Color;
//import android.support.v4.graphics.drawable.DrawableCompat;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.SwitchCompat;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.CompoundButton;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import in.junctiontech.school.FCM.Config;
//import in.junctiontech.school.R;
//import in.junctiontech.school.models.StaffDetail;
//
///**
// * Created by LENEVO on 03-08-2017.
// */
//
//public class PerticularUserPermissionAdapter  extends RecyclerView.Adapter<PerticularUserPermissionAdapter.MyViewHolder> {
//    private final int[]  thumbColors;
//    private ArrayList<StaffDetail> appTopicList;
//    private Context context;
//    private int appColor;
//   private ArrayList<MobilePermission> permissionArrayList;
//
//    public PerticularUserPermissionAdapter(Context context, ArrayList<StaffDetail> appTopicList, int colorIs,
//    ArrayList<MobilePermission> staffDetailArrayList){
//        this.appTopicList = appTopicList;
//        this.context = context;
//        this.appColor = colorIs;
//        this.permissionArrayList = staffDetailArrayList;
//          thumbColors = new int[]{
//                Color.LTGRAY,
//                appColor,
//        };
//
//    }
//
//    public void animate(RecyclerView.ViewHolder viewHolder) {
//        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
//        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
//    }
//
//    @Override
//    public PerticularUserPermissionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_selection, parent, false);
//        return new PerticularUserPermissionAdapter.MyViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final PerticularUserPermissionAdapter.MyViewHolder holder, int position) {
//        StaffDetail obj =appTopicList.get(position);
//        holder.tv_item_name.setText(obj.getStaffName());
//        if (obj.getMobilePermission() != null) {
//            Log.e("MobilePermission",(obj.getMobilePermission()).getPermission());
//            if ((obj.getMobilePermission()).getPermission().equalsIgnoreCase("1"))holder.tv_item_name.setChecked(true);
//            else holder.tv_item_name.setChecked(false);
//        }
//
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    @Override
//    public int getItemCount() {
//        return appTopicList.size();
//    }
//
//    public void updateList(ArrayList<StaffDetail> masterEntryPosList, ArrayList<MobilePermission> staffDetailArrayList) {
//        this.appTopicList =masterEntryPosList;
//        setList(staffDetailArrayList);
//
//    }
//
//    private  void setList(ArrayList<MobilePermission> permissionList) {
//        for (StaffDetail staffDetail : appTopicList) {
//
//            for (MobilePermission perObj : permissionList) {
//                if (null!=perObj.getUserId()&& perObj.getUserId().equalsIgnoreCase(staffDetail.getStaffId())) {
//                    perObj.setPermission("1");
//                    staffDetail.setMobilePermission(perObj);
//                    break;
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        SwitchCompat tv_item_name;
//
//        public MyViewHolder(final View itemView) {
//            super(itemView);
//            (itemView.findViewById(R.id.ck_item_user_selection_name)).setVisibility(View.GONE);
//            (itemView.findViewById(R.id.iv_item_user_next)).setVisibility(View.GONE);
//            tv_item_name = (SwitchCompat) itemView.findViewById(R.id.switch_item_user_name);
//            tv_item_name.setVisibility(View.VISIBLE);
//
//            DrawableCompat.setTintList(DrawableCompat.wrap(tv_item_name.getThumbDrawable()), new ColorStateList( Config.stateOffOn, thumbColors));
//            DrawableCompat.setTintList(DrawableCompat.wrap(tv_item_name.getTrackDrawable()), new ColorStateList( Config.stateOffOn, Config.trackColors));
//
//            tv_item_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (appTopicList.get(getLayoutPosition()).getMobilePermission()==null)
//                        appTopicList.get(getLayoutPosition()).setMobilePermission(new MobilePermission());
//                    appTopicList.get(getLayoutPosition()).getMobilePermission().setPermission(isChecked?"1":"0");
//                }
//            });
//
//          //  Config.setSwitchCompactColor(tv_item_name,appColor);
//            //itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
//
//        }
//    }
//}
//
