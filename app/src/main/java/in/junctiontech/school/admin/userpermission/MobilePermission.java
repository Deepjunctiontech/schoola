package in.junctiontech.school.admin.userpermission;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by LENEVO on 04-08-2017.
 */

public class MobilePermission implements Serializable {
    String MobilePermissionId, UserId, UserType;
    String Permission;
    String ActivityName, AdditionalInfo;
    ArrayList<MobilePermission> result;

    public ArrayList<MobilePermission> getResult() {
        return result;
    }

    public void setResult(ArrayList<MobilePermission> result) {
        this.result = result;
    }

    public String getMobilePermissionId() {
        return MobilePermissionId;
    }

    public void setMobilePermissionId(String mobilePermissionId) {
        MobilePermissionId = mobilePermissionId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getPermission() {
        return Permission;
    }

    public void setPermission(String permission) {
        Permission = permission;
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String activityName) {
        ActivityName = activityName;
    }

    public String getAdditionalInfo() {
        return AdditionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        AdditionalInfo = additionalInfo;
    }
}
