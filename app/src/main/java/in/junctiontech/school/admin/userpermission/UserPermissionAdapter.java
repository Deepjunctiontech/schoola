package in.junctiontech.school.admin.userpermission;

import android.content.Context;
import android.content.res.ColorStateList;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;

import java.util.ArrayList;

import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.models.MasterEntry;

/**
 * Created by LENEVO on 02-08-2017.
 */

public class UserPermissionAdapter extends RecyclerView.Adapter<UserPermissionAdapter.MyViewHolder> {
    private final int  color;
    private ArrayList<MasterEntry> appTopicList;
    private Context context;
    private  String keyName;
    private int activityID;
    private ColorStateList colorStateList;

    public UserPermissionAdapter(Context context, ArrayList<MasterEntry> appTopicList, int color, String keyName,int activityID) {
        this.appTopicList = appTopicList;
        this.context = context;
        this.color = color;
        this.keyName = keyName;
        this.activityID = activityID;
        colorStateList = Config.getCheckBoxColor(color);
    }



    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public UserPermissionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_selection, parent, false);
        return new UserPermissionAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserPermissionAdapter.MyViewHolder holder, int position) {
      MasterEntry obj =appTopicList.get(position);
        holder.tv_item_name.setText(obj.getMasterEntryValue());
        if (obj.getMobilePermission() != null) {
            if ((obj.getMobilePermission()).getPermission().equalsIgnoreCase("1"))holder.tv_item_name.setChecked(true);
            else holder.tv_item_name.setChecked(false);
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return appTopicList.size();
    }

    public void updateList(ArrayList<MasterEntry> masterEntryPosList, ArrayList<MobilePermission> permissionList) {
        this.appTopicList =masterEntryPosList;
        setList(permissionList);

    }
private  void setList(ArrayList<MobilePermission> permissionList) {
    for (MasterEntry masterEntryObj : appTopicList) {

        for (MobilePermission perObj : permissionList) {
            if (perObj.getUserType().equalsIgnoreCase(masterEntryObj.getMasterEntryId())) {
                perObj.setPermission("1");
                masterEntryObj.setMobilePermission(perObj);
                break;
            }
        }
    }
    notifyDataSetChanged();
}


    public class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatCheckBox tv_item_name;


        public MyViewHolder(final View itemView) {
            super(itemView);
            tv_item_name = (AppCompatCheckBox) itemView.findViewById(R.id.ck_item_user_selection_name);
            final ImageView  iv_item_user_next = (ImageView) itemView.findViewById(R.id.iv_item_user_next);

            tv_item_name.setSupportButtonTintList(colorStateList);
            tv_item_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                   iv_item_user_next.setVisibility(isChecked?View.INVISIBLE:View.VISIBLE);
                    if (appTopicList.get(getLayoutPosition()).getMobilePermission()==null)
                        appTopicList.get(getLayoutPosition()).setMobilePermission(new MobilePermission());
                    appTopicList.get(getLayoutPosition()).getMobilePermission().setPermission(isChecked?"1":"0");
                }
            });
           // itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tv_item_name.isChecked()) {
//                        context.startActivity(new Intent(context, ParticularUserPermissionActivity.class)
//                                .putExtra("dataUSerType",appTopicList.get(getLayoutPosition()))
//                        .putExtra("keyName",keyName)
//                        .putExtra("activityID",activityID)
//                        );

                        ((UserSelectionForPermissionActivity) context).overridePendingTransition(R.anim.enter, R.anim.nothing);
                    }

                }
            });
        }
    }

}

