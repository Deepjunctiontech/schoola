package in.junctiontech.school.admin.userpermission;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by LENEVO on 18-08-2017.
 */

public class MobileActivityNameModel implements Serializable {
    String activityName;
    int parent;
    ArrayList<MobileActivityNameModel>result;

    int activityID;
    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public ArrayList<MobileActivityNameModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<MobileActivityNameModel> result) {
        this.result = result;
    }
}
