package in.junctiontech.school.admin.userpermission;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;

import in.junctiontech.school.R;

/**
 * Created by LENEVO on 02-08-2017.
 */

public class AppTopicListAdapter extends RecyclerView.Adapter<AppTopicListAdapter.MyViewHolder> {
    private ArrayList<AppTopicList> appTopicList;
    private Context context;
    private boolean isTopic;


    public AppTopicListAdapter(Context context, ArrayList<AppTopicList> appTopicList, boolean isTopic) {
        this.appTopicList = appTopicList;
        this.context = context;
        this.isTopic = isTopic;

    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public AppTopicListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_permissions, parent, false);
        return new AppTopicListAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final AppTopicListAdapter.MyViewHolder holder, int position) {

        //holder.tv_item_name.setText(isTopic ? appTopicList.get(position).getTopicName() : appTopicList.get(position).getSubTopicName());
        holder.tv_item_name.setText( appTopicList.get(position).getKeyName() );

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return appTopicList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_item_name;


        public MyViewHolder(final View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_permission_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!isTopic){
                        context.startActivity(new Intent(context, UserSelectionForPermissionActivity.class)
                                .putExtra("keyName",appTopicList.get(getLayoutPosition()).getKeyName())
                                .putExtra("activityID",appTopicList.get(getLayoutPosition()).getActivityID())
                        );
                        ((SubTopicListActivity) context).overridePendingTransition(R.anim.enter, R.anim.nothing);
                    }else
                        if (appTopicList.get(getLayoutPosition()).getSubTopicList() != null && appTopicList.get(getLayoutPosition()).getSubTopicList().size() > 0) {
                            context.startActivity(new Intent(context, SubTopicListActivity.class)
                                    .putExtra("data", appTopicList.get(getLayoutPosition()).getSubTopicList()));
                            ((PermissionsActivity) context).overridePendingTransition(R.anim.enter, R.anim.nothing);
                        } else if (appTopicList.get(getLayoutPosition()).getSubTopicList().size() == 0) {
                            context.startActivity(new Intent(context, UserSelectionForPermissionActivity.class)
                                    .putExtra("keyName",appTopicList.get(getLayoutPosition()).getKeyName())
                                    .putExtra("activityID",appTopicList.get(getLayoutPosition()).getActivityID())

                            );
                            ((PermissionsActivity) context).overridePendingTransition(R.anim.enter, R.anim.nothing);
                        }

                }
            });

            itemView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.animator_for_bounce));

        }
    }
}

