package in.junctiontech.school;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.RadioButton;

import java.util.Locale;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.menuActions.LanguageSetup;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;

/**
 * Created by JAYDEVI BHADE on 11/8/2016.
 */

public class AdminLanguageSetting extends AppCompatActivity {

    private RadioButton rb_english,rb_hindi, rb_urdu, rb_marathi, rb_spanish,rb_arabic,rb_french, rb_amharic;
    private SharedPreferences sp;
    private void setColorApp() {

       int colorIs = Config.getAppColor(this,true);
       // getWindow().setStatusBarColor(colorIs);
       // getWindow().setNavigationBarColor(colorIs);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences("APP_LANG", Context.MODE_PRIVATE);
//        sharedPsreferences = getSharedPreferences("APP_LANG", Context.MODE_PRIVATE);
//        LanguageSetup.changeLang(this, sp.getString("app_language", ""));

        setContentView(R.layout.activity_language_selection);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.choose_language);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setColorApp();

        Locale obj = Locale.getDefault();
        rb_english =  findViewById(R.id.rb_english);
        rb_hindi = findViewById(R.id.rb_hindi);
        rb_urdu = findViewById(R.id.rb_urdu);
        rb_marathi = findViewById(R.id.rb_marathi);
        rb_spanish = findViewById(R.id.rb_spanish);
        rb_arabic = findViewById(R.id.rb_arabic);
        rb_french = findViewById(R.id.rb_french);
        rb_amharic = findViewById(R.id.rb_amharic);

        String defaultLang = sp.getString("app_language", "");
       // rb_urdu.setVisibility(View.GONE);
        rb_english.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
            {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "en")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);

                    finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }
            }
//                startActivity(newtext Intent(LanguageSelection.this, Settings.class));
//                finish();
        });
        rb_hindi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "hi")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);

                    finish();

                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }
            }


        });
        rb_urdu.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "ur")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);

                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });
        rb_marathi.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "mr")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });
        rb_spanish.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "es")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });
        rb_arabic.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "ar")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });

        rb_french.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "fr")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });

        rb_amharic.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                if (LanguageSetup.changeLang(AdminLanguageSetting.this, "am")){
                    Intent intent = new Intent(AdminLanguageSetting.this, AdminNavigationDrawerNew.class);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);

                }
            }


        });

        if (("").equals(defaultLang) || defaultLang.equalsIgnoreCase(""))
            defaultLang = obj.getLanguage();

        switch (defaultLang) {
            case "en":
                rb_english.setChecked(true);
                break;
            case "hi":
                rb_hindi.setChecked(true);
                break;
            case "ur":
                rb_urdu.setChecked(true);
                break;
            case "mr":
                rb_marathi.setChecked(true);
                break;
            case "es":
                rb_spanish.setChecked(true);
                break;
            case "ar":
                rb_arabic.setChecked(true);
                break;
            case "fr":
                rb_french.setChecked(true);
                break;
            case "am":
                rb_amharic.setChecked(true);
                break;
            default:
                rb_english.setChecked(true);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        setResult(RESULT_CANCELED, intent);

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AdminNavigationDrawerNew.class);
        setResult(RESULT_CANCELED, intent);

        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
       getApplicationContext().getResources().updateConfiguration(newConfig,
               getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_language_selection);
        super.onConfigurationChanged(newConfig);
    }
}
