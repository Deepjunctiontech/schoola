package in.junctiontech.school.staffattendance;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.DbHandler;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.Prefs;
import in.junctiontech.school.R;
import in.junctiontech.school.models.StaffDetail;
import in.junctiontech.school.schoolnew.DB.MainDatabase;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;

public class StaffAttendanceActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    MainDatabase mDb;

    private Button btn_staff_attendance_select_intime, btn_staff_attendance_select_outtime,
            btn_staff_attendance_select_date, btn_staff_attendance_save;
    private RecyclerView recycler_view_staff_attendance_staff_list;
    private ProgressDialog progressbar;
    private AlertDialog.Builder alert;
    private SharedPreferences sp;
    private Type type;
    private Gson gson;
    private String dbName;
    private ArrayList<StaffDetail> staffListData = new ArrayList<>();
    int flag = 1;
    private StaffAttendanceAdapter adapter;
    private CheckBox ck_staff_attendance_select_all;
    private String convertedDate = "";
    private String staffAttendanceId = "";
    private boolean isDialogShowing = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean logoutAlert = false;

    ProgressBar progressBar;
    //    private RelativeLayout snackbar;
    private int colorIs;

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        //   getWindow().setStatusBarColor(colorIs);
        // getWindow().setNavigationBarColor(colorIs);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(colorIs));

        ((TextView) findViewById(R.id.tv_staff_attendance_select_date)).setTextColor(colorIs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = Prefs.with(this).getSharedPreferences();
        //   LanguageSetup.changeLang(this, sp.getString("app_language", ""));
        setContentView(R.layout.activity_staff_attendance);
//        snackbar =  findViewById(R.id.activity_staff_attendance);
        intViews();
        dbName = sp.getString("organization_name", "");

        mDb = MainDatabase.getDatabase(this);
        progressBar = findViewById(R.id.progressBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setColorApp();
        gson = new Gson();
        type = new TypeToken<StaffDetail>() {
        }.getType();
        progressbar = new ProgressDialog(this);
        progressbar.setCancelable(false);
        progressbar.setMessage(getString(R.string.please_wait));

        alert = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_LIGHT);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogShowing = false;
            }
        });
        alert.setCancelable(false);
//        fetchStaffFromServer();
//        getStrtotimeDate(Config.currentDate.format(new Date()));

        //  convertCurrentDate();
        setupRecycler();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equalsIgnoreCase(Config.INTERNET_AVAILABLE)) {
                    Log.e("internet", "available");
//                    if (staffListData.size() == 0)
//                        fetchStaffFromServer();

//                    getStrtotimeDate(Config.currentDate.format(new Date()));


                }
            }
        };

        fetchAttendance();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/1214782667", this, adContainer);
        }
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.INTERNET_AVAILABLE));
        super.onResume();

        mDb.schoolStaffModel().getAllStaff().observe(this, new Observer<List<SchoolStaffEntity>>() {
            @Override
            public void onChanged(@Nullable List<SchoolStaffEntity> schoolStaffEntities) {
                String jsonObject = new Gson().toJson(schoolStaffEntities);
                staffListData = new Gson().fromJson(jsonObject, new TypeToken<List<StaffDetail>>() {
                }
                        .getType());
                adapter.updateList(staffListData);

            }
        });


    }

    void fetchAttendance() {
        progressBar.setVisibility(View.VISIBLE);
        final JSONObject p = new JSONObject();
        try {
            p.put("Date", btn_staff_attendance_select_date.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        p.put("Frequency", "Monthly");

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "attendance/staffAttendanceEntry/"
                + p;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);
                String code = job.optString("code");
                switch (code) {
                    case Gc.APIRESPONSE200:
                        try {
                            ArrayList<StaffAttendance> staffAttendances = new Gson()
                                    .fromJson(job.getJSONObject("result")
                                                    .optString("staffAttendance"),
                                            new TypeToken<List<StaffAttendance>>() {
                                            }.getType());

                            staffListData.clear();

                            for (StaffAttendance f : staffAttendances) {
                                staffListData.add(new StaffDetail(
                                        f.StaffId,
                                        f.StaffName,
                                        f.StaffPosition,
                                        f.staffProfileImage,
                                        f.Attendance.get(0).AttStatus));
                            }

                            adapter.updateList(staffListData);

                            Config.responseSnackBarHandler(job.optString("message"),
                                    findViewById(R.id.activity_staff_attendance), R.color.fragment_first_green);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StaffAttendanceActivity.this);

                        Intent intent1 = new Intent(StaffAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StaffAttendanceActivity.this);

                        Intent intent2 = new Intent(StaffAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();
                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.activity_staff_attendance), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, error, findViewById(R.id.activity_staff_attendance));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void setupRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view_staff_attendance_staff_list.setLayoutManager(layoutManager);
        adapter = new StaffAttendanceAdapter(this, staffListData, colorIs);
        recycler_view_staff_attendance_staff_list.setAdapter(adapter);


    }

    private void intViews() {
        //  btn_staff_attendance_select_intime = (Button) findViewById(R.id.btn_staff_attendance_select_intime);
        //  btn_staff_attendance_select_outtime = (Button) findViewById(R.id.btn_staff_attendance_select_outtime);
        btn_staff_attendance_select_date = findViewById(R.id.btn_staff_attendance_select_date);
        ck_staff_attendance_select_all = findViewById(R.id.ck_staff_attendance_select_all);
        btn_staff_attendance_save = findViewById(R.id.btn_staff_attendance_save);

        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        btn_staff_attendance_select_date.setText(currentDate.format(new Date()));

        //   recycler_view_staff_attendance_staff_list =(RecyclerView)findViewById(R.id.recycler_view_staff_attendance_staff_list);
        recycler_view_staff_attendance_staff_list = findViewById(R.id.recycler_view_staff_attendance_staff_list);

        btn_staff_attendance_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(StaffAttendanceActivity.this,"save",Toast.LENGTH_LONG).show();
                saveAttendance();
//                if (attendanceList.size() == 0) {
//                    showSnack();
//                }
//                else
//                    convertSatffAttendanceDate("P", attendanceList);
//                AlertDialog.Builder alertAtt = new AlertDialog.Builder(StaffAttendanceActivity.this,
//                        AlertDialog.THEME_HOLO_LIGHT);
//                final RadioGroup rg = new RadioGroup(StaffAttendanceActivity.this);
//                //     final RadioButton rb_admin = newtext RadioButton(SignUpActivity.this);
//                final RadioButton rb_Present = new RadioButton(StaffAttendanceActivity.this);
//                final RadioButton rb_Halfday = new RadioButton(StaffAttendanceActivity.this);
//                final RadioButton rb_Onduty = new RadioButton(StaffAttendanceActivity.this);
//                final RadioButton rb_Holiday = new RadioButton(StaffAttendanceActivity.this);
//
//                final String[] att_status = {""};
//
//                rb_Present.setText(getString(R.string.present));
//                rb_Halfday.setText(R.string.halfday);
//                rb_Onduty.setText(R.string.onduty);
//                rb_Holiday.setText(R.string.holiday);
//
//                rg.addView(rb_Present);
//                rg.addView(rb_Halfday);
//                rg.addView(rb_Onduty);
//                rg.addView(rb_Holiday);
//
//                rg.setPadding(50, 20, 0, 50);
//                rb_Present.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        att_status[0] = "P";
//                    }
//                });
//                rb_Halfday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        att_status[0] = "H";
//                    }
//                });
//                rb_Onduty.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        att_status[0] = "OD";
//                    }
//                });
//                rb_Holiday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        att_status[0] = "HD";
//                    }
//                });
//
//
//                alertAtt.setView(rg);
//                alertAtt.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                alertAtt.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        convertSatffAttendanceDate(att_status[0]);
//                    }
//                });
//                alertAtt.show();

            }
        });
        ck_staff_attendance_select_all.setOnCheckedChangeListener((buttonView, isChecked) -> {
            /*if (adapter != null) {
                adapter.selectAll(isChecked);
                if (isChecked)
                    ck_staff_attendance_select_all.setText(R.string.un_select_all);
                else ck_staff_attendance_select_all.setText(R.string.select_all);
            }*/
            if (adapter != null) {
                if (flag == 1) {
                    adapter.selectAll(flag);
                    ck_staff_attendance_select_all.setText("Half Day All");
                    flag = 2;
                } else if (flag == 2) {
                    adapter.selectAll(flag);
                    flag = 3;
                    ck_staff_attendance_select_all.setText("Absent All");
                } else {
                    adapter.selectAll(flag);
                    flag = 1;
                    ck_staff_attendance_select_all.setText("Present All");
                }
               /* if (isChecked)
                    ck_staff_attendance_select_all.setText(R.string.un_select_all);
                else ck_staff_attendance_select_all.setText(R.string.select_all);*/
            }

        });
//        btn_staff_attendance_select_intime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(StaffAttendanceActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        String am_pm = (selectedHour < 12) ? "AM" : "PM";
//                        selectedHour = (selectedHour < 12) ? selectedHour : selectedHour - 12;
//                        btn_staff_attendance_select_intime.setText(selectedHour + ":" + selectedMinute
//                                + " " + am_pm);
//
//
//                    }
//                }, hour, minute, true);//Yes 24 hour ic_time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//            }
//        });
//
//        btn_staff_attendance_select_outtime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(StaffAttendanceActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        String am_pm = (selectedHour < 12) ? "AM" : "PM";
//                        selectedHour = (selectedHour < 12) ? selectedHour : selectedHour - 12;
//                        btn_staff_attendance_select_outtime.setText(selectedHour + ":" + selectedMinute
//                                + " " + am_pm);
//                    }
//                }, hour, minute, true);//Yes 24 hour ic_time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//            }
//        });

        final Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date

        final DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                String new_day = dayOfMonth + "";
//                if (dayOfMonth < 10)
//                    new_day = "0" + dayOfMonth;
//
//                String new_month = monthOfYear + 1 + "";
////            Toast.makeText(AttendanceEntryFrontPage.this,new_month ,Toast.LENGTH_LONG).show();
//                if ((monthOfYear + 1) < 10)
//                    new_month = "0" + (monthOfYear + 1);
//
////            currentdate.setText("" + new_day + "-" + new_month + "-" + year);
//                btn_staff_attendance_select_date.setText(year + "-" + new_month + "-" + new_day);
                staffAttendanceId = "";
                ck_staff_attendance_select_all.setChecked(false);
//            getStrtotimeDate(year + "-" + new_month + "-" + new_day);

                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(Gc.DATEINPUTFORMAT, Locale.US);
                btn_staff_attendance_select_date.setText(sdf.format(cal.getTime()));
                flag = 1;
                fetchAttendance();
            }
        };
        btn_staff_attendance_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create the DatePickerDialog instance

                DatePickerDialog datePicker = new DatePickerDialog(StaffAttendanceActivity.this, myDateListener,
                        cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));
                datePicker.setCancelable(false);
                datePicker.setTitle(getString(R.string.select_date));
                datePicker.show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        super.onBackPressed();
    }

    public void setFilter(ArrayList<StaffDetail> studentListData) {
        adapter.setFilter(studentListData);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_search, menu);
//
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<StaffDetail> filteredModelList = filter(staffListData, newText);

        setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private ArrayList<StaffDetail> filter(ArrayList<StaffDetail> models, String query) {
        query = query.toLowerCase();
        final ArrayList<StaffDetail> filteredModelList = new ArrayList<>();

        for (StaffDetail model : models) {

            String staffName = model.getStaffName().toLowerCase();
            String positionval = model.getMasterEntryValue().toLowerCase();
            if (staffName.contains(query) || positionval.contains(query)) {
                filteredModelList.add(model);

            }
        }
        return filteredModelList;
    }

    private void fetchStaffFromServer() {
     /*   if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

        progressbar.show();

        String fetchStaffUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffApi.php?databaseName=" + dbName +
                "&action=join";
        Log.e("getStaffData", fetchStaffUrl);
        StringRequest request = new StringRequest(Request.Method.GET,

                fetchStaffUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("getStaffData", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optInt("code") == 200 && !isFinishing()) {
                                StaffDetail obj = gson.fromJson(s, type);
                                staffListData = obj.getResult();
                                adapter.updateList(staffListData);

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(StaffAttendanceActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }
//                            if (staffListData.size() == 0 && !isFinishing()) {
//                                showSnack();
//                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getStaffData", volleyError.toString());
                progressbar.cancel();
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }
    }

    private void getStrtotimeDate(String datestr) {
       /* if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        String dateUrl = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                datestr;
        Log.d("dateUrl", dateUrl);
        StringRequest request = new StringRequest(Request.Method.GET,
                dateUrl
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
                        convertedDate = s;
//                        fetchStaffAttendance(convertedDate);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        //}
    }

    private void fetchStaffAttendance(String date) {


        Map<String, String> param = new LinkedHashMap<String, String>();
        param.put("Date", date);

        final Map<String, JSONObject> param1 = new LinkedHashMap<>();
        param1.put("filter", (new JSONObject(param)));


        String fetchStaffAtt = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffAttendanceApi.php?databaseName=" +
                dbName +
                "&data=" + (new JSONObject(param1));
        Log.e("fetchStaffAttendance", fetchStaffAtt);
        for (StaffDetail obj : staffListData)
            obj.setPresentStatus("A");
        StringRequest request = new StringRequest(Request.Method.GET,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",

                fetchStaffAtt
                //  sp.getString("HostName", "Not Found") + "MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("fetchStaffAttendance", s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.optString("code").equalsIgnoreCase("200")) {

                                staffAttendanceId = jsonObject.optJSONArray("result").optJSONObject(0)
                                        .optString("StaffAttendanceId");
                                String[] attendance = jsonObject.optJSONArray("result").optJSONObject(0).optString("Attendance").split(",");

                                for (int i = 0; i < attendance.length; i++) {

                                    String[] att = attendance[i].split("-");
                                    for (StaffDetail obj : staffListData) {

                                        if (obj.getStaffId().equalsIgnoreCase(att[0])) {
                                            obj.setPresentStatus(att[1]);
                                            Log.e("attID", obj.getStaffId() + obj.getPresentStatus());
                                            break;
                                        }
                                    }
                                }

                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(StaffAttendanceActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            }

                            if (!isFinishing())
                                adapter.updateList(staffListData);

                            progressbar.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("fetchStaffAttendance", volleyError.toString());
                progressbar.cancel();
                if (!isFinishing())
                    adapter.updateList(staffListData);
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);


        //}
    }

    void saveAttendance() {

        progressBar.setVisibility(View.VISIBLE);

        ArrayList<StaffDetail> attendanceList = adapter.getAttendance();

        final JSONArray param = new JSONArray();
        try {
            JSONArray att = new JSONArray();

            for (StaffDetail s : attendanceList) {
                att.put(new JSONObject()
                        .put("StaffId", s.getStaffId())
                        .put("Attendance", s.getPresentStatus())
                );
            }

            param.put(new JSONObject()
                    .put("Date", btn_staff_attendance_select_date.getText().toString())
                    .put("DOLUsername", Gc.getSharedPreference(Gc.SIGNEDINUSERNAME, this))
                    .put("StaffAttendance", att)
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL, this)
                + Gc.ERPAPIVERSION
                + "attendance/staffAttendanceEntry";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject job) {
                progressBar.setVisibility(View.GONE);

                String code = job.optString("code");

                switch (code) {
                    case Gc.APIRESPONSE200:

                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.activity_staff_attendance), R.color.fragment_first_green);

                        break;

                    case Gc.APIRESPONSE401:

                        HashMap<String, String> setDefaults1 = new HashMap<>();
                        setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults1, StaffAttendanceActivity.this);

                        Intent intent1 = new Intent(StaffAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);

                        finish();

                        break;

                    case Gc.APIRESPONSE500:

                        HashMap<String, String> setDefaults = new HashMap<>();
                        setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                        Gc.setSharedPreference(setDefaults, StaffAttendanceActivity.this);

                        Intent intent2 = new Intent(StaffAttendanceActivity.this, AdminNavigationDrawerNew.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);

                        finish();

                        break;

                    default:
                        Config.responseSnackBarHandler(job.optString("message"),
                                findViewById(R.id.activity_staff_attendance), R.color.fragment_first_blue);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, error, findViewById(R.id.activity_staff_attendance));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY", Gc.APPKEY);
                headers.put("ORGKEY", Gc.getSharedPreference(Gc.ERPINSTCODE, getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME, getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID", Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID, getApplicationContext()));
                headers.put("USERTYPE", Gc.getSharedPreference(Gc.USERTYPECODE, getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN, getApplicationContext()));
                headers.put("PLANTYPE", Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return param == null ? null : param.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", param.toString(), "utf-8");
                    return null;
                }
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    public void saveStaffAttendance(String att_status, String attDate, ArrayList<StaffDetail> attendanceList) throws JSONException {


        String finalAttendance = "";
//            for (StaffDetail obj : staffListData) {
//                if (obj.get)
//                finalAttendance = finalAttendance + obj.getStaffId() + "-" + att_status + "-" + attDate + "-1475300715-1475301300,";
//          else                 finalAttendance = finalAttendance + obj.getStaffId() + "-" + "A" + "-" + attDate + "-1475300715-1475301300,";
//
//            }


        for (StaffDetail obj : staffListData) {
            for (StaffDetail objNew : attendanceList) {
                if (objNew.getStaffId().equalsIgnoreCase(obj.getStaffId())) {
                    obj = objNew;
                    break;
                }
            }
            finalAttendance = finalAttendance + obj.getStaffId() + "-" + obj.getPresentStatus() + "-" + attDate + "-1475300715-1475301300,";

        }

        final JSONObject param = new JSONObject();
        param.put("Date", attDate);
        param.put("Attendance", finalAttendance);
        param.put("DOL", attDate);
        param.put("DOLUsername", sp.getString("loggedUserName", ""));


        String insertAttStaff = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffAttendanceApi.php?databaseName=" + dbName;

        Log.d("insertAttStaff", insertAttStaff);
        Log.d("param", param.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                insertAttStaff, param
                ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        DbHandler.longInfo(jsonObject.toString());
                        Log.e("saveAtt", jsonObject.toString());
                        progressbar.cancel();

                        if (jsonObject.optString("code").equalsIgnoreCase("201")) {
                            staffAttendanceId = jsonObject.optString("result");
                            Toast.makeText(StaffAttendanceActivity.this, jsonObject.optString("message"),
                                    Toast.LENGTH_SHORT).show();
                            /*   ck_staff_attendance_select_all.setChecked(false);*/
                        } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                ||
                                jsonObject.optString("code").equalsIgnoreCase("511")) {
                            if (!logoutAlert & !isFinishing()) {
                                Config.responseVolleyHandlerAlert(StaffAttendanceActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                logoutAlert = true;
                            }
                        } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
//                            Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                        } else {

                            alert.setMessage(jsonObject.optString("message"));
                            if (!isFinishing())
                                alert.show();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("saveAtt", volleyError.toString());
                progressbar.cancel();
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);

            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);


    }

    private void convertSatffAttendanceDate(final String att_status, final ArrayList<StaffDetail> attendanceList) {
    /*    if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/
        progressbar.show();
        String convertDate = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "Conversion.php?type=dateToString&date=" +
                btn_staff_attendance_select_date.getText().toString();


        Log.d("convertDate", convertDate);
        StringRequest request = new StringRequest(Request.Method.GET,
                convertDate
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("dateStrResult", s);
//                        if (staffAttendanceId.equalsIgnoreCase(""))
//                            try {
//                                saveStaffAttendance(att_status, s, attendanceList);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        else deleteAttendanceThenSave(att_status, s, attendanceList);


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("dateStrResult", volleyError.toString());
                progressbar.dismiss();
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);

        // }
    }

    private void deleteAttendanceThenSave(final String att_status, final String date, final ArrayList<StaffDetail> attendanceList) {
      /*  if (!Config.checkInternet(this)) {
            if (!isDialogShowing) {
                alert.setMessage(getString(R.string.internet_not_available_please_check_your_internet_connectivity));
                alert.show();
                isDialogShowing = true;
            }

        } else {*/

        progressbar.show();

        JSONObject job_filter = new JSONObject();
        try {
            job_filter.put("StaffAttendanceId", staffAttendanceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Map<String, JSONObject> param_main = new LinkedHashMap<String, JSONObject>();
        param_main.put("filter", job_filter);


        Log.e("deleteResultAtt", new JSONObject(param_main).toString());

        String deleteAtt = sp.getString("HostName", "Not Found")
                + sp.getString(Config.applicationVersionName, "Not Found")
                + "StaffAttendanceApi.php?databaseName=" + dbName +
                "&data=" + (new JSONObject(param_main));
        Log.d("deleteAtt", deleteAtt);
        StringRequest request = new StringRequest(Request.Method.DELETE,
                //  sp.getString("HostName", "Not Found") + "login/login_userCpanel",
                deleteAtt

                //   sp.getString("HostName", "Not Found") +"MasterEntryApi.php?data={\"filter\":{\"MasterEntryName\":\"UserType\"}}"
                ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        DbHandler.longInfo(s);
                        Log.e("deleteResultAtt", s);
                        progressbar.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(s);

                            if (jsonObject.optInt("code") == 200) {
//                                saveStaffAttendance(att_status, date, attendanceList);
                            } else if (jsonObject.optString("code").equalsIgnoreCase("503")
                                    ||
                                    jsonObject.optString("code").equalsIgnoreCase("511")) {
                                if (!logoutAlert & !isFinishing()) {
                                    Config.responseVolleyHandlerAlert(StaffAttendanceActivity.this, jsonObject.optInt("code") + "", jsonObject.optString("message"));
                                    logoutAlert = true;
                                }
                            } else if (jsonObject.optString("code").equalsIgnoreCase("502")) {
//                                Config.responseSnackBarHandler(getString(R.string.maintanance_work_in_progress_please_visit_after_ten_min), snackbar);
                            } else {
                                if (!isFinishing())
                                    alert.setMessage(getString(R.string.failled_to_fill_attendance));
                                alert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("deleteResultAtt", volleyError.toString());
                progressbar.cancel();
//                Config.responseVolleyErrorHandler(StaffAttendanceActivity.this, volleyError, snackbar);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> header = new LinkedHashMap<String, String>();
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<String, String>();
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppRequestQueueController.getInstance(this).addToRequestQueue(request);
        // }
    }

    private void showSnack() {
//        if (staffListData.size() == 0) {
//            Snackbar snackbarObj = Snackbar.make(
//                    snackbar,
//                    getString(R.string.staff_not_available),
//                    Snackbar.LENGTH_LONG).setAction(getString(R.string.create_staff),
//                    new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (!sp.getBoolean(Config.CREATE_STAFF_PERMISSION,false)) {
//                                Toast.makeText(StaffAttendanceActivity.this,"Permission not available  : CREATE STAFF !",Toast.LENGTH_SHORT).show();
//
//                            }else {   startActivity(new Intent(StaffAttendanceActivity.this, CreateStaffActivity.class));
//                            finish();
//                            overridePendingTransition(R.anim.enter, R.anim.nothing);}
//                        }
//                    });
//            snackbarObj.setDuration(5000);
//            snackbarObj.getView().setBackgroundColor(getResources().getColor(R.color.bottomTabSelector));
//            snackbarObj.setActionTextColor(getResources().getColor(android.R.color.holo_red_dark));
//            snackbarObj.show();
//        }
    }

}

class StaffAttendance {
    public String StaffId;
    public String StaffName;
    public String StaffPosition;
    public String staffProfileImage;
    public ArrayList<AttendanceEntry> Attendance;
}

class AttendanceEntry {
    public String Date;
    public String AttStatus;
    public String inTime;
    public String OutTime;
}