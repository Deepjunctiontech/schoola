package in.junctiontech.school.staffattendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.junctiontech.school.R;
import in.junctiontech.school.models.StaffDetail;

/**
 * Created by JAYDEVI BHADE on 10/15/2016.
 */

public class StaffAttendanceAdapter extends RecyclerView.Adapter<StaffAttendanceAdapter.MyViewHolder> {
    private ArrayList<StaffDetail> staffList;
    private Context context;
    private int colorIs;
    private String status = "A";

    public StaffAttendanceAdapter(Context context, ArrayList<StaffDetail> staffList, int colorIs) {
        this.staffList = staffList;
        this.context = context;
        this.colorIs = colorIs;
    }

    @Override
    public StaffAttendanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staff_attendance, parent, false);
        return new StaffAttendanceAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StaffAttendanceAdapter.MyViewHolder holder, final int position) {
        final StaffDetail staffObj = staffList.get(position);

        holder.tv_item_staff_att_name.setText(staffObj.getStaffName().toUpperCase());
        holder.tv_item_staff_att_sub_name.setText(staffObj.getMasterEntryValue());

        if (staffObj.getPresentStatus() != null)
            status = staffObj.getPresentStatus();
        if (status.equalsIgnoreCase("A") || status.equalsIgnoreCase("HD"))
            holder.ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_red_colour);
        else if (status.equalsIgnoreCase("P") || status.equalsIgnoreCase("OD"))
            holder.ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_green_colour);
        else if (status.equalsIgnoreCase("H"))
            holder.ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_yellow_colour);
        else if (status.equalsIgnoreCase("N"))
            holder.ib_item_staff_att_present_status.setBackgroundColor(android.R.drawable.btn_default);

        staffObj.setPresentStatus(status);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return staffList.size();
    }

    public void updateList(ArrayList<StaffDetail> staffList) {
        this.staffList = staffList;
        // Log.e("update", "refresh");
        notifyDataSetChanged();
    }

    public ArrayList<StaffDetail> getAttendance() {return this.staffList;}

    /*public void selectAll(boolean isChecked) {
        if (isChecked)
            this.status = "P";
        else this.status = "A";
        for(int i=0;i<getItemCount();i++)
            staffList.get(i).setPresentStatus(this.status);
        notifyDataSetChanged();
    }*/

    void selectAll(int flag) {
        if (flag == 1) {
            this.status = "P";
        } else if (flag == 2) {
            this.status = "H";
        } else {
            this.status = "A";
        }
        for (int i = 0; i < getItemCount(); i++)
            staffList.get(i).setPresentStatus(this.status);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_staff_att_name, tv_item_staff_att_sub_name;
        private ImageButton ib_item_staff_att_present_status;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_staff_att_name = itemView.findViewById(R.id.tv_item_staff_att_name);
            tv_item_staff_att_name.setTextColor(colorIs);
            tv_item_staff_att_sub_name = itemView.findViewById(R.id.tv_item_staff_att_sub_name);
            ib_item_staff_att_present_status = itemView.findViewById(R.id.ib_item_staff_att_present_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StaffDetail staffObj = staffList.get(getLayoutPosition());
                    if (staffObj.getPresentStatus().equalsIgnoreCase("A")) {
                        status = "H";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_yellow_colour);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("P")) {
                        status = "A";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_red_colour);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("H")) {
                        status = "N";
                        ib_item_staff_att_present_status.setBackgroundResource(android.R.drawable.btn_default);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("N")) {
                        status = "P";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_green_colour);
                    }

                    staffObj.setPresentStatus(status);
                }
            });
            ib_item_staff_att_present_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StaffDetail staffObj = staffList.get(getLayoutPosition());
                    if (staffObj.getPresentStatus().equalsIgnoreCase("A")) {
                        status = "H";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_yellow_colour);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("P")) {
                        status = "A";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_red_colour);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("H")) {
                        status = "N";
                        ib_item_staff_att_present_status.setBackgroundResource(android.R.drawable.btn_default);
                    } else if (staffObj.getPresentStatus().equalsIgnoreCase("N")) {
                        status = "P";
                        ib_item_staff_att_present_status.setBackgroundResource(R.drawable.ic_green_colour);
                    }
                    staffObj.setPresentStatus(status);
                }
            });

        }
    }

    public void setFilter(ArrayList<StaffDetail> staffList) {
        this.staffList = new ArrayList<>();
        this.staffList.addAll(staffList);
        notifyDataSetChanged();
    }
}






