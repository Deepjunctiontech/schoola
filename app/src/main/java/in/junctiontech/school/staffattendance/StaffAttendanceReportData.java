package in.junctiontech.school.staffattendance;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10-11-2017.
 */

public class StaffAttendanceReportData implements Serializable {

    private ArrayList<StaffAttendanceReportData> result;

    private String onDate , attendance, inTime, outTime ;

    public ArrayList<StaffAttendanceReportData> getResult() {
        return result;
    }

    public void setResult(ArrayList<StaffAttendanceReportData> result) {
        this.result = result;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }
}
