package in.junctiontech.school.staffattendance;

import android.graphics.Color;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.junctiontech.school.R;
import in.junctiontech.school.ReportView.ParentViewAttendance;

/**
 * Created by JAYDEVI BHADE on 10-11-2017.
 */

public class StaffAttendanceReportAdapter extends RecyclerView.Adapter<StaffAttendanceReportAdapter.MyViewHolder> {
    private StaffAttendanceReportActivity context;
    private List<Date> monthlyDates;

    private ArrayList<StaffAttendanceReportData> allEvents = new ArrayList<>();

    public StaffAttendanceReportAdapter(StaffAttendanceReportActivity parentViewHomework, List<Date> dayValueInCells, ArrayList<StaffAttendanceReportData> listOfEvents) {
        this.allEvents = listOfEvents;
        this.context = parentViewHomework;
        this.monthlyDates = dayValueInCells;
    }


    @Override
    public StaffAttendanceReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_homework, parent, false);
        return new StaffAttendanceReportAdapter.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final StaffAttendanceReportAdapter.MyViewHolder holder, final int position) {
        if (position < 7) {
            holder.tv_item_calendar_date.setTypeface(Typeface.DEFAULT_BOLD);
            holder.tv_item_calendar_date.setText(String.valueOf(ParentViewAttendance.weekDay[position]));
        } else {
            Date mDate = monthlyDates.get(position);
            final Calendar dateCal = Calendar.getInstance();
            dateCal.setTime(mDate);

            final int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);


            StaffAttendanceReportData status = allEvents.get(position);
            Log.e("RITU_dayValue", status == null ? "noAtten" : status.getAttendance());

            if (status == null || status.getAttendance() == null || status.getAttendance().equalsIgnoreCase("")) {
                holder.tv_item_calendar_date.setBackground(null);
                holder.tv_item_calendar_date.setTextColor(Color.BLACK);
            } else if (status.getAttendance().equalsIgnoreCase("A") || status.getAttendance().equalsIgnoreCase("HD")) {

                holder.tv_item_calendar_date.setBackgroundResource(R.drawable.red_circular_layout);
                holder.tv_item_calendar_date.setTextColor(Color.WHITE);

            } else if (status.getAttendance().equalsIgnoreCase("P") || status.getAttendance().equalsIgnoreCase("OD")) {

                holder.tv_item_calendar_date.setBackgroundResource(R.drawable.green_circular_layout);
                holder.tv_item_calendar_date.setTextColor(Color.WHITE);
            } else if (status.getAttendance().equalsIgnoreCase("H")) {

                holder.tv_item_calendar_date.setBackgroundResource(R.drawable.yellow_circular_layout);
                holder.tv_item_calendar_date.setTextColor(Color.WHITE);
            }

            holder.tv_item_calendar_date.setText(String.valueOf(dayValue));
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return monthlyDates.size();
    }

    public void updateCalendar(List<Date> monthlyDates, ArrayList<StaffAttendanceReportData> allEvents) {
        this.monthlyDates = monthlyDates;
        this.allEvents = allEvents;
        this.notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_calendar_date;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_calendar_date = (TextView) itemView.findViewById(R.id.month_date);
            tv_item_calendar_date.setBackground(null);
            tv_item_calendar_date.setTextColor(Color.BLACK);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StaffAttendanceReportData obj = allEvents.get(getLayoutPosition());
                    if (obj!=null)
                    Toast.makeText(context, "In time : " +obj.getInTime()+
                            "\nOut time : "+obj.getOutTime()
                            , Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}

