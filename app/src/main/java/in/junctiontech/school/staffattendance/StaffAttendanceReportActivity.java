package in.junctiontech.school.staffattendance;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.junctiontech.school.AppRequestQueueController;
import in.junctiontech.school.FCM.Config;
import in.junctiontech.school.R;
import in.junctiontech.school.schoolnew.DB.SchoolStaffEntity;
import in.junctiontech.school.schoolnew.adminpanel.AdminNavigationDrawerNew;
import in.junctiontech.school.schoolnew.common.Gc;
import in.junctiontech.school.schoolnew.model.AttendanceStatus;

public class StaffAttendanceReportActivity extends AppCompatActivity {

    private int colorIs;
    ProgressBar progressBar;

    RecyclerView mRecyclerView;
    CalendarAdapter adapter;

    SchoolStaffEntity staff;

    TextView tv_days_present, tv_days_absent,tv_half_days;

    Button btn_report_month;

    ArrayList<String> dayList = new ArrayList<>();
    HashMap<String, String> monthAtt = new HashMap<>();
    HashMap<String, String> attTime = new HashMap<>();

    String yearMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_attendance_report);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.attendance_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.progressBar);

        tv_days_present = findViewById(R.id.tv_days_present);
        tv_days_absent = findViewById(R.id.tv_days_absent);
        tv_half_days = findViewById(R.id.tv_half_days);
        btn_report_month = findViewById(R.id.btn_report_month);
        mRecyclerView = findViewById(R.id.rv_staff_attendance_report);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 7);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new CalendarAdapter();
        mRecyclerView.setAdapter(adapter);

        String st = getIntent().getStringExtra("staff");
        if(!("".equalsIgnoreCase(Strings.nullToEmpty(st)))) {
            staff = new Gson().fromJson(st, SchoolStaffEntity.class);
            Objects.requireNonNull(getSupportActionBar()).setTitle(staff.StaffName +" : " + getString(R.string.attendance_report));

        }


        String date = new SimpleDateFormat("MM-yyyy", Locale.US).format(new Date());
        yearMonth = new SimpleDateFormat("yyyy-MM", Locale.US).format(new Date());
        String firstdate = "01-"+ date;


        try {
            Date firstdayDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate);

            Calendar c = Calendar.getInstance();
            c.setTime(firstdayDate); // yourdate is an object of type Date

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            int maxDays = c.getActualMaximum(Calendar.DATE);

            buildCalenderForMonth(dayOfWeek, maxDays);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        btn_report_month.setText(new SimpleDateFormat("MMM-yyyy", Locale.US).format(new Date()));

        try {
            fetchAttendenceAndDisplay();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        selectAttendanceDate();

        setColorApp();

        if (!Arrays.asList(Gc.STRING_ARRAY).contains(Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()).toLowerCase()) &&
                Gc.getSharedPreference(Gc.ERPADDVERTISEMENTSTATUS, this).toLowerCase().equalsIgnoreCase("yes")) {
            View adContainer = findViewById(R.id.adMobView);
            Config.adsInitialize("ca-app-pub-1890254643259173/4387740911", this, adContainer);
        }
    }

    void buildCalenderForMonth(int dayOfWeek, int maxDays){
        dayList.clear();
        dayList.add("Sun");
        dayList.add("Mon");
        dayList.add("Tue");
        dayList.add("Wed");
        dayList.add("Thu");
        dayList.add("Fri");
        dayList.add("Sat");
        switch (dayOfWeek){
            case 1:
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 2:
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 3:
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;

            case 4:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 5:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 6:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
            case 7:
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                dayList.add("");
                for (int i =1; i<=maxDays;i++){
                    dayList.add(Integer.toString(i));
                }
                break;
        }
        adapter.notifyDataSetChanged();

    }

    void fetchAttendenceAndDisplay() throws JSONException {

        progressBar.setVisibility(View.VISIBLE);

        final JSONObject p = new JSONObject();
        p.put("StaffId",staff.StaffId);
        p.put("MonthYear",btn_report_month.getText().toString());

        String url = Gc
                .getSharedPreference(Gc.ERPHOSTAPIURL,this)
                + Gc.ERPAPIVERSION
                + "attendance/staffAttendanceEntry/"
                + p
                ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), job -> {
            progressBar.setVisibility(View.GONE);
            String code = job.optString("code");

            switch (code) {
                case Gc.APIRESPONSE200:
                    try{
                        JSONArray attendancejarr = job.getJSONObject("result").getJSONArray("staffAttendance");

                        if(attendancejarr.length()>0) {

                            ArrayList<AttendanceStatus> attendanceMonthly = new Gson()
                                    .fromJson(attendancejarr.getJSONObject(0).getJSONArray("Attendance").toString(),new TypeToken<List<AttendanceStatus>>(){}
                                            .getType());

                            fillTotalAttendance(attendanceMonthly);

                            for (AttendanceStatus a: attendanceMonthly){
                                if (!(a.AttStatus.equalsIgnoreCase("N")))
                                    monthAtt.put(a.Date,a.AttStatus);

                                attTime.put(a.Date,getString(R.string.intime)+" : " + a.inTime + " - "
                                        + getString(R.string.outtime)+" : " + a.OutTime);
                            }
                            if (monthAtt.size()>0){
                                adapter.notifyDataSetChanged();
                                Config.responseSnackBarHandler(job.optString("message"),
                                        findViewById(R.id.top_layout), R.color.fragment_first_green);
                            }else{
                                Config.responseSnackBarHandler(getString(R.string.attendance_not_available),
                                        findViewById(R.id.top_layout), R.color.fragment_first_green);
                            }
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gc.APIRESPONSE204:
                    tv_days_present.setText("0");
                    tv_days_absent.setText("0");
                    tv_half_days.setText("0");
                    Config.responseSnackBarHandler(getString(R.string.attendance_not_available),
                            findViewById(R.id.top_layout), R.color.fragment_first_green);

                    break;

                case Gc.APIRESPONSE401:

                    HashMap<String, String> setDefaults1 = new HashMap<>();
                    setDefaults1.put(Gc.NOTAUTHORIZED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults1, StaffAttendanceReportActivity.this);

                    Intent intent1 = new Intent(StaffAttendanceReportActivity.this, AdminNavigationDrawerNew.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);

                    finish();

                    break;

                case Gc.APIRESPONSE500:

                    HashMap<String, String> setDefaults = new HashMap<>();
                    setDefaults.put(Gc.ISORGANIZATIONDELETED, Gc.TRUE);
                    Gc.setSharedPreference(setDefaults, StaffAttendanceReportActivity.this);

                    Intent intent2 = new Intent(StaffAttendanceReportActivity.this, AdminNavigationDrawerNew.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);

                    finish();

                    break;
                default:
                    Config.responseSnackBarHandler(job.optString("message"),
                            findViewById(R.id.top_layout),R.color.fragment_first_blue);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);

            Config.responseVolleyErrorHandler(StaffAttendanceReportActivity.this,error,findViewById(R.id.top_layout));

        }){
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("APPKEY",Gc.APPKEY);
                headers.put("ORGKEY",Gc.getSharedPreference(Gc.ERPINSTCODE,getApplicationContext()));
                headers.put("DBNAME", Gc.getSharedPreference(Gc.ERPDBNAME,getApplicationContext()));
                headers.put("Content-Type", Gc.CONTENT_TYPE);
                headers.put("DEVICE", Gc.DEVICETYPE);
                headers.put("DEVICEID",Gc.id(getApplicationContext()));
                headers.put("USERID", Gc.getSharedPreference(Gc.USERID,getApplicationContext()));
                headers.put("USERTYPE",Gc.getSharedPreference(Gc.USERTYPECODE,getApplicationContext()));
                headers.put("ACCESSTOKEN", Gc.getSharedPreference(Gc.ACCESSTOKEN,getApplicationContext()));
                headers.put("PLANTYPE",Gc.getSharedPreference(Gc.ERPPLANTYPE, getApplicationContext()));

                return headers;
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 2, 2));
        AppRequestQueueController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void fillTotalAttendance(ArrayList<AttendanceStatus>  attendanceMonthly){
        int present = 0,absent  = 0,leave  = 0,halfday = 0;

        for(AttendanceStatus a: attendanceMonthly){
            switch (a.AttStatus.toUpperCase()){
                case "P":
                    ++present;
                    break;
                case "A":
                    ++absent;
                    break;
                case "H":
                    ++halfday;
                    break;
            }
        }
        tv_days_present.setText(String.valueOf(present));
        tv_days_absent.setText(String.valueOf(absent));
        tv_half_days.setText(String.valueOf(halfday));

    }


    void selectAttendanceDate(){
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "MM-yyyy"; //In which you need put here
//                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            yearMonth = new SimpleDateFormat("yyyy-MM", Locale.US).format(myCalendar.getTime());

            String date1 = sdf.format(myCalendar.getTime());

            String firstdate = "01-"+ date1;

            try {
                Date firstdayDate = new SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(firstdate);

                Calendar c = Calendar.getInstance();
                c.setTime(firstdayDate); // yourdate is an object of type Date

                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                int maxDays = c.getActualMaximum(Calendar.DATE);

                buildCalenderForMonth(dayOfWeek, maxDays);

            } catch (ParseException e) {
                e.printStackTrace();
            }
//                btn_select_month.setText(sdf.format(new Date()));
            btn_report_month.setText(new SimpleDateFormat("MMM-yyyy", Locale.US).format(myCalendar.getTime()));

            try {
                monthAtt.clear();
                fetchAttendenceAndDisplay();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };

        btn_report_month.setOnClickListener(view -> new DatePickerDialog(StaffAttendanceReportActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setColorApp() {

        colorIs = Config.getAppColor(this, true);
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(colorIs));
            (findViewById(R.id.btn_report_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));
            (findViewById(R.id.btn_report_month)).setBackgroundTintList(ColorStateList.valueOf(colorIs));

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder>{

        @NonNull
        @Override
        public CalendarAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calender_date, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CalendarAdapter.MyViewHolder holder, int position) {
            String dateName = dayList.get(position);
            if (position < 7 ){
                holder.tv_cal_date.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                holder.tv_cal_date.setText(dateName);
            }else{
                if (dayList.get(position).equalsIgnoreCase("")){
                    holder.tv_cal_date.setBackground(null);
                    holder.tv_cal_date.setText(null);
                }else {
                    holder.tv_cal_date.setText(dateName);
                    int dateInt = Integer.parseInt(dateName);
                    if (dateInt>0 && dateInt < 10){
                        String key = yearMonth+"-0"+dateInt;
                        if(monthAtt.containsKey(key)){
                            if (monthAtt.get(key).equalsIgnoreCase("P"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("A"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.red_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("H"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.blue_circular_layout));
                            else
                                holder.tv_cal_date.setBackground(null);
                        }else{
                            holder.tv_cal_date.setBackground(null);
                        }
                    }else{
                        String key = yearMonth+"-"+dateInt;
                        if(monthAtt.containsKey(key)){
                            if (monthAtt.get(key).equalsIgnoreCase("P"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.green_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("A"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.red_circular_layout));
                            else if (monthAtt.get(key).equalsIgnoreCase("H"))
                                holder.tv_cal_date.setBackground(getDrawable(R.drawable.blue_circular_layout));
                            else
                                holder.tv_cal_date.setBackground(null);
                        }else{
                            holder.tv_cal_date.setBackground(null);
                        }
                    }

                }
            }
        }

        @Override
        public int getItemCount() {
            return dayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_cal_date;
            public MyViewHolder(View itemView) {
                super(itemView);
                tv_cal_date = itemView.findViewById(R.id.tv_cal_date);
                itemView.setOnClickListener(v -> {
                    if (getAdapterPosition() > 6) {
                        String inTimeOutTimeDate = buildDateFromDigit(dayList.get(getAdapterPosition()));
                        if (!("".equalsIgnoreCase(inTimeOutTimeDate))){
                            if (monthAtt.containsKey(inTimeOutTimeDate) && "P".equalsIgnoreCase(monthAtt.get(inTimeOutTimeDate)))
                            Config.myToast(StaffAttendanceReportActivity.this, "Date", attTime.get(inTimeOutTimeDate), Gravity.CENTER);
                        }
                    }
                });
            }
        }
    }

    String buildDateFromDigit(String calDigit){
        String date = yearMonth ;
        String dateDigit = "";
        if (!("".equalsIgnoreCase(calDigit))){
            int digit = Integer.parseInt(calDigit);

            if (digit >= 1 && digit <=9){
                dateDigit = "0"+ String.valueOf(digit);
            }else{
                dateDigit = String.valueOf(digit);
            }
            date = date +"-"+ dateDigit;
            return date;

        }else{
            return "";
        }
    }

}
