package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10/27/2016.
 */

public class  Subject implements Serializable {
    private ArrayList<Subject> result;
    String SubjectId, Session, SubjectName, SubjectAbb, sectionId, SubjectStatus ,DOE, 	DOL,SectionName="";

    public String getDOL() {
        return DOL;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public ArrayList<Subject> getResult() {
        return result;
    }

    public void setResult(ArrayList<Subject> result) {
        this.result = result;
    }

    public void setDOL(String DOL) {
        this.DOL = DOL;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectAbb() {
        return SubjectAbb;
    }

    public void setSubjectAbb(String subjectAbb) {
        SubjectAbb = subjectAbb;
    }


    public String getSubjectStatus() {
        return SubjectStatus;
    }

    public void setSubjectStatus(String subjectStatus) {
        SubjectStatus = subjectStatus;
    }

    public String getDOE() {
        return DOE;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public void setDOE(String DOE) {

        this.DOE = DOE;
    }

}
