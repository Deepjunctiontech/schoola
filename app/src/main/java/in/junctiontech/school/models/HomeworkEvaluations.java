package in.junctiontech.school.models;

/**
 * Created by JAYDEVI BHADE on 12/26/2016.
 */

public class HomeworkEvaluations {
    String id ,name ,status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HomeworkEvaluations(String name, String id, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }
}
