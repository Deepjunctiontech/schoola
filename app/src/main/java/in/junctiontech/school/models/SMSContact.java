package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 13/1/2017.
 */

public class SMSContact implements Serializable {
    private String id,name,mobileHolderType, mobileHolderNumber="";
    private ArrayList<SMSContact> result;
    private boolean smsSendStatus=true;
    private String totalCount="";
    private boolean deliveryStatus= false;

    public boolean isDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(boolean deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public SMSContact(String id, String name, String mobileHolderType, String mobileHolderNumber) {
        this.id=id;
        this.name= name;
        this.mobileHolderNumber=mobileHolderNumber;
        this.mobileHolderType= mobileHolderType;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileHolderType() {
        return mobileHolderType;
    }

    public void setMobileHolderType(String mobileHolderType) {
        this.mobileHolderType = mobileHolderType;
    }

    public String getMobileHolderNumber() {
        return mobileHolderNumber;
    }

    public void setMobileHolderNumber(String mobileHolderNumber) {
        this.mobileHolderNumber = mobileHolderNumber;
    }

    public ArrayList<SMSContact> getResult() {
        return result;
    }

    public void setResult(ArrayList<SMSContact> result) {
        this.result = result;
    }

    public boolean isSmsSendStatus() {
        return smsSendStatus;
    }

    public void setSmsSendStatus(boolean smsSendStatus) {
        this.smsSendStatus = smsSendStatus;
    }
}
