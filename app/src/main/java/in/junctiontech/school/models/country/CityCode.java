package in.junctiontech.school.models.country;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 13-04-2017.
 */

public class CityCode implements Serializable {
String name ;
    ArrayList<CityCode> result;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CityCode> getResult() {
        return result;
    }

    public void setResult(ArrayList<CityCode> result) {
        this.result = result;
    }
}
