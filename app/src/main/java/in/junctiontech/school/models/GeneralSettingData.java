package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10/5/2016.
 */

public class GeneralSettingData  implements Serializable{
    private String Id, CurrentSession, SchoolStartDate, sessionStartDate,
            SchoolName, SchoolMoto,Logo, SchoolAddress, City, District, PIN,
            State,Country, Mobile, AlternateMobile, Email,Landline, Fax,
            DateOfEstablishment, Board, AffiliatedBy, RegistrationNo, AffiliationNo,
            Terms,            DOE,DOEUsername, DOL, DOLUsername,schoolDescription,schoolMapLocation,
            facebookId,YouTubeId,LinkdinId,gPlusId, schoolImage,themeColor,Website;


    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String getSchoolImage() {
        return schoolImage;
    }

    public void setSchoolImage(String schoolImage) {
        this.schoolImage = schoolImage;
    }

    public String getSchoolDescription() {
        return schoolDescription;
    }

    public void setSchoolDescription(String schoolDescription) {
        this.schoolDescription = schoolDescription;
    }

    public String getSchoolMapLocation() {
        return schoolMapLocation;
    }

    public void setSchoolMapLocation(String schoolMapLocation) {
        this.schoolMapLocation = schoolMapLocation;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getYouTubeId() {
        return YouTubeId;
    }

    public void setYouTubeId(String youTubeId) {
        YouTubeId = youTubeId;
    }

    public String getLinkdinId() {
        return LinkdinId;
    }

    public void setLinkdinId(String linkdinId) {
        LinkdinId = linkdinId;
    }

    public String getgPlusId() {
        return gPlusId;
    }

    public void setgPlusId(String gPlusId) {
        this.gPlusId = gPlusId;
    }

    private String logoPath,code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    private String logoUrl;

    private ArrayList<GeneralSettingData> result;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDOE() {
        return DOE;
    }

    public void setDOE(String DOE) {
        this.DOE = DOE;
    }

    public String getDOEUsername() {
        return DOEUsername;
    }

    public void setDOEUsername(String DOEUsername) {
        this.DOEUsername = DOEUsername;
    }

    public String getDOL() {
        return DOL;
    }

    public void setDOL(String DOL) {
        this.DOL = DOL;
    }

    public String getDOLUsername() {
        return DOLUsername;
    }

    public void setDOLUsername(String DOLUsername) {
        this.DOLUsername = DOLUsername;
    }



    public ArrayList<GeneralSettingData> getResult() {
        return result;
    }

    public void setResult(ArrayList<GeneralSettingData> result) {
        this.result = result;
    }

    public String getCurrentSession() {
        return CurrentSession;
    }

    public void setCurrentSession(String currentSession) {
        CurrentSession = currentSession;
    }

    public String getSchoolStartDate() {
        return SchoolStartDate;
    }

    public void setSchoolStartDate(String schoolStartDate) {
        SchoolStartDate = schoolStartDate;
    }

    public String getSessionStartDate() {
        return sessionStartDate;
    }

    public void setSessionStartDate(String sessionStartDate) {
        this.sessionStartDate = sessionStartDate;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getSchoolMoto() {
        return SchoolMoto;
    }

    public void setSchoolMoto(String schoolMoto) {
        SchoolMoto = schoolMoto;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getSchoolAddress() {
        return SchoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        SchoolAddress = schoolAddress;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getAlternateMobile() {
        return AlternateMobile;
    }

    public void setAlternateMobile(String alternateMobile) {
        AlternateMobile = alternateMobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLandline() {
        return Landline;
    }

    public void setLandline(String landline) {
        Landline = landline;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getDateOfEstablishment() {
        return DateOfEstablishment;
    }

    public void setDateOfEstablishment(String dateOfEstablishment) {
        DateOfEstablishment = dateOfEstablishment;
    }

    public String getBoard() {
        return Board;
    }

    public void setBoard(String board) {
        Board = board;
    }

    public String getAffiliatedBy() {
        return AffiliatedBy;
    }

    public void setAffiliatedBy(String affiliatedBy) {
        AffiliatedBy = affiliatedBy;
    }

    public String getRegistrationNo() {
        return RegistrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        RegistrationNo = registrationNo;
    }

    public String getAffiliationNo() {
        return AffiliationNo;
    }

    public void setAffiliationNo(String affiliationNo) {
        AffiliationNo = affiliationNo;
    }

    public String getTerms() {
        return Terms;
    }

    public void setTerms(String terms) {
        Terms = terms;
    }
}
