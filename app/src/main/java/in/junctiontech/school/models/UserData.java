package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 16/2/2017.
 */

public class UserData implements Serializable {
    String  name, positionName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public UserData(String name, String positionName) {
        this.name = name;
        this.positionName = positionName;
    }
}
