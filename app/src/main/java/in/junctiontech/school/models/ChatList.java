package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 12/29/2016.
 */

public class ChatList implements Serializable{

    int groupId,unReadChatCounter=0;
String  groupName ,groupMembers,groupMembersType, lastChatMessage, lastChatTime;

    public ChatList() {

    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getUnReadChatCounter() {
        return this.unReadChatCounter;
    }

    public void setUnReadChatCounter(int unReadChatCounter) {
        this.unReadChatCounter = unReadChatCounter;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupMembersType() {
        return groupMembersType;
    }

    public void setGroupMembersType(String groupMembersType) {
        this.groupMembersType = groupMembersType;
    }

    public String getLastChatMessage() {
        return lastChatMessage;
    }

    public void setLastChatMessage(String lastChatMessage) {
        this.lastChatMessage = lastChatMessage;
    }

    public String getLastChatTime() {
        return lastChatTime;
    }

    public void setLastChatTime(String lastChatTime) {
        this.lastChatTime = lastChatTime;
    }

    public String getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(String groupMembers) {
        this.groupMembers = groupMembers;
    }

    public ChatList(int groupId, String groupName, String groupMembers, String groupMembersType, String lastChatMessage,
                    String lastChatTime,
                    int unReadChatCounter
                   ) {
        this.groupId = groupId;
        this.unReadChatCounter = unReadChatCounter;
        this.groupName = groupName;
        this.groupMembers = groupMembers;
        this.groupMembersType = groupMembersType;
        this.lastChatMessage = lastChatMessage;
        this.lastChatTime= lastChatTime;
    }
}
