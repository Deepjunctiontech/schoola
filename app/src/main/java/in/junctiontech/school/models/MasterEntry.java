package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

import in.junctiontech.school.admin.msgpermission.ChatPermission;
import in.junctiontech.school.admin.userpermission.MobilePermission;

/**
 * Created by JAYDEVI BHADE on 10/14/2016.
 */

public class MasterEntry implements Serializable {
    private String MasterEntryId;
    private String MasterEntryStatus;
    private String MasterEntryName;
    private String MasterEntryValue;
    private String studentAmount;
    private ArrayList<ChatPermission> chatPermissionArrayList;

    public ArrayList<ChatPermission> getChatPermissionArrayList() {
        return chatPermissionArrayList;
    }

    public void setChatPermissionArrayList(ArrayList<ChatPermission> chatPermissionArrayList) {
        this.chatPermissionArrayList = chatPermissionArrayList;
    }

    private MobilePermission mobilePermission;

    public MobilePermission getMobilePermission() {
        return mobilePermission;
    }

    public void setMobilePermission(MobilePermission mobilePermission) {
        this.mobilePermission = mobilePermission;
    }

    public MasterEntry(String feeId, String studentAmount) {
        this.feeId=feeId;
        this.studentAmount=studentAmount;
    }



    public String getFeeId() {
        return feeId;
    }

    public void setFeeId(String feeId) {
        this.feeId = feeId;
    }

    private  String feeId;

    public MasterEntry(String feeId,String amount, String masterEntryId, String masterEntryValue,
                       String studentAmount) {
        this.feeId = feeId;
        this.amount = amount;
        this.MasterEntryId = masterEntryId;
        this.MasterEntryValue = masterEntryValue;
        this.studentAmount = studentAmount;
    }

    public String getStudentAmount() {
        return studentAmount;
    }

    public void setStudentAmount(String studentAmount) {
        this.studentAmount = studentAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    private String amount;

    public String getMasterEntryId() {
        return MasterEntryId;
    }

    public void setMasterEntryId(String masterEntryId) {
        MasterEntryId = masterEntryId;
    }

    public String getMasterEntryStatus() {
        return MasterEntryStatus;
    }

    public void setMasterEntryStatus(String masterEntryStatus) {
        MasterEntryStatus = masterEntryStatus;
    }

    public String getMasterEntryName() {
        return MasterEntryName;
    }

    public void setMasterEntryName(String masterEntryName) {
        MasterEntryName = masterEntryName;
    }

    public String getMasterEntryValue() {
        return MasterEntryValue;
    }

    public void setMasterEntryValue(String masterEntryValue) {
        MasterEntryValue = masterEntryValue;
    }

    public ArrayList<MasterEntry> getResult() {
        return result;
    }

    public void setResult(ArrayList<MasterEntry> result) {
        this.result = result;
    }

    private ArrayList<MasterEntry> result;
}
