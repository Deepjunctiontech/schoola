package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 11/5/2016.
 */

public class Sibling implements Serializable {
    private String SiblingId, RegistrationId, SName, SDOB,SClass,SSchool,SRemarks;
private ArrayList<Sibling> result;
    public Sibling(String SiblingId, String RegistrationId, String SName, String childDob,
                   String childClass, String schoolName,String remark) {
        this.SiblingId=SiblingId;
        this.RegistrationId=RegistrationId;
        this.SName=SName;
        this.SDOB=childDob;
        this.SClass=childClass;
        this.SSchool=schoolName;
        this.SRemarks= remark;
    }

    public ArrayList<Sibling> getResult() {
        return result;
    }

    public void setResult(ArrayList<Sibling> result) {
        this.result = result;
    }

    public String getSiblingId() {
        return SiblingId;
    }

    public void setSiblingId(String siblingId) {
        SiblingId = siblingId;
    }

    public String getRegistrationId() {
        return RegistrationId;
    }

    public void setRegistrationId(String registrationId) {
        RegistrationId = registrationId;
    }

    public String getSName() {
        return SName;
    }

    public void setSName(String SName) {
        this.SName = SName;
    }

    public String getSDOB() {
        return SDOB;
    }

    public void setSDOB(String SDOB) {
        this.SDOB = SDOB;
    }

    public String getSClass() {
        return SClass;
    }

    public void setSClass(String SClass) {
        this.SClass = SClass;
    }

    public String getSSchool() {
        return SSchool;
    }

    public void setSSchool(String SSchool) {
        this.SSchool = SSchool;
    }

    public String getSRemarks() {
        return SRemarks;
    }

    public void setSRemarks(String SRemarks) {
        this.SRemarks = SRemarks;
    }
}
