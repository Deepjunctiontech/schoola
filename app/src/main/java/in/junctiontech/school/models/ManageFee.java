package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10/18/2016.
 */

public class ManageFee implements Serializable {
    private String FeeId , FeeStatus, Session, SectionId, FeeType,Amount, 	Distance, MasterEntryValue;
private ArrayList<ManageFee> result;

    public String getMasterEntryValue() {
        return MasterEntryValue;
    }

    public void setMasterEntryValue(String masterEntryValue) {
        MasterEntryValue = masterEntryValue;
    }

    public ManageFee(String feeId, String feeStatus, String session,

                     String sectionId, String feeType, String amount,
                     String distance) {
        this.FeeId = feeId;
        this.FeeStatus = feeStatus;
        this.Session= session;
        this.SectionId = sectionId;
        this.FeeType = feeType;
        this.Distance = distance;
        this.Amount = amount;
    }

    public ArrayList<ManageFee> getResult() {
        return result;
    }

    public void setResult(ArrayList<ManageFee> result) {
        this.result = result;
    }

    public String getFeeId() {
        return FeeId;
    }

    public void setFeeId(String feeId) {
        FeeId = feeId;
    }

    public String getFeeStatus() {
        return FeeStatus;
    }

    public void setFeeStatus(String feeStatus) {
        FeeStatus = feeStatus;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getFeeType() {
        return FeeType;
    }

    public void setFeeType(String feeType) {
        FeeType = feeType;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }
}
