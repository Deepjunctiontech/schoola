package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by JAYDEVI BHADE on 9/1/2016.
 */
public class OnlineExamData implements Serializable {
    private String subjectName, examName, examId, examDate, maxMarks, cutoff, examLevel,
            noOfQuestion, examDuration, perQuestionDuration,chapterName,runningStatus="start",
            examEndDate,show_result_onmobile, studentTestOnMobileStartTime;


    private Date studentExamStartTime;

    public String getStudentTestOnMobileStartTime() {
        return this.studentTestOnMobileStartTime;
    }

    public void setStudentTestOnMobileStartTime(String studentTestOnMobileStartTime) {
        this.studentTestOnMobileStartTime = studentTestOnMobileStartTime;
    }

    private ArrayList<OnlineExamData> questionBankList, questionsOptionList;
    private String questionId, question, optionId, answer, answerDescription,
            questionSolution,choiceAnswer;

    public Date getStudentExamStartTime() {
        return studentExamStartTime;
    }

    public void setStudentExamStartTime(Date studentExamStartTime) {
        this.studentExamStartTime = studentExamStartTime;
    }

    public void setChoiceAnswer(String choiceAnswer) {
        this.choiceAnswer = choiceAnswer;
    }

    private ArrayList<String> optionIndex, optionValue;
    private ArrayList<OnlineExamData> result;

    public String getChoiceAnswer() {
        return this.choiceAnswer;
    }

    public ArrayList<OnlineExamData> getResult() {
        return result;
    }

    public void setResult(ArrayList<OnlineExamData> result) {
        this.result = result;
    }

    public String getObtained_marks() {
        return obtained_marks;
    }

    public void setObtained_marks(String obtained_marks) {
        this.obtained_marks = obtained_marks;
    }

    private String correct_ans, wrong_ans,obtained_marks, online_student_status,time_duration,no_of_qus_attemp;

    public String getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(String correct_ans) {
        this.correct_ans = correct_ans;
    }

    public String getWrong_ans() {
        return wrong_ans;
    }

    public String getRunningStatus() {
        return runningStatus;
    }

    public void setRunningStatus(String runningStatus) {
        this.runningStatus = runningStatus;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getExamEndDate() {
        return examEndDate;
    }

    public void setExamEndDate(String examEndDate) {
        this.examEndDate = examEndDate;
    }



    public void setWrong_ans(String wrong_ans) {
        this.wrong_ans = wrong_ans;
    }


    public String getOnline_student_status() {
        return online_student_status;
    }

    public void setOnline_student_status(String online_student_status) {
        this.online_student_status = online_student_status;
    }

    public String getTime_duration() {
        return time_duration;
    }

    public void setTime_duration(String time_duration) {
        this.time_duration = time_duration;
    }

    public String getNo_of_qus_attemp() {
        return no_of_qus_attemp;
    }

    public void setNo_of_qus_attemp(String no_of_qus_attemp) {
        this.no_of_qus_attemp = no_of_qus_attemp;
    }



    public String getSubjectName() {
        return subjectName;
    }

    public OnlineExamData(String subjectName) {
        this.subjectName = subjectName;
    }


    public String getExamName() {
        return examName;

    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getExamDate() {
        return examDate;
    }

    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }

    public String getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(String maxMarks) {
        this.maxMarks = maxMarks;
    }

    public String getCutoff() {
        return cutoff;
    }

    public void setCutoff(String cutoff) {
        this.cutoff = cutoff;
    }

    public String getExamLevel() {
        return examLevel;
    }

    public void setExamLevel(String examLevel) {
        this.examLevel = examLevel;
    }

    public String getNoOfQuestion() {
        return noOfQuestion;
    }

    public void setNoOfQuestion(String noOfQuestion) {
        this.noOfQuestion = noOfQuestion;
    }

    public String getExamDuration() {
        return examDuration;
    }

    public void setExamDuration(String examDuration) {
        this.examDuration = examDuration;
    }

    public String getPerQuestionDuration() {
        return perQuestionDuration;
    }

    public void setPerQuestionDuration(String perQuestionDuration) {
        this.perQuestionDuration = perQuestionDuration;
    }

    public ArrayList<OnlineExamData> getQuestionBankList() {
        return questionBankList;
    }

    public void setQuestionBankList(ArrayList<OnlineExamData> questionBankList) {
        this.questionBankList = questionBankList;
    }

    public ArrayList<OnlineExamData> getQuestionsOptionList() {
        return questionsOptionList;
    }

    public void setQuestionsOptionList(ArrayList<OnlineExamData> questionsOptionList) {
        this.questionsOptionList = questionsOptionList;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public ArrayList<String> getOptionIndex() {
        return optionIndex;
    }

    public void setOptionIndex(ArrayList<String> optionIndex) {
        this.optionIndex = optionIndex;
    }

    public ArrayList<String> getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(ArrayList<String> optionValue) {
        this.optionValue = optionValue;
    }

    public String getAnswer() {

        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerDescription() {
        return answerDescription;
    }

    public void setAnswerDescription(String answerDescription) {
        this.answerDescription = answerDescription;
    }

    public String getQuestionSolution() {
        return questionSolution;
    }

    public void setQuestionSolution(String questionSolution) {
        this.questionSolution = questionSolution;
    }

    public void setSubjectName(String subjectName) {

        this.subjectName = subjectName;

    }

    public String getShow_result_onmobile() {
        return show_result_onmobile;
    }

    public void setShow_result_onmobile(String show_result_onmobile) {
        this.show_result_onmobile = show_result_onmobile;
    }
}
