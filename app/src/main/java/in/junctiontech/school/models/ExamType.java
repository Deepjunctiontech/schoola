package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 15-04-2017.
 */

public class ExamType implements Serializable {
    String Exam_Type,Exam_Status;
    ArrayList<ExamType> result;

    public String getExam_Type() {
        return Exam_Type;
    }

    public void setExam_Type(String exam_Type) {
        Exam_Type = exam_Type;
    }

    public String getExam_Status() {
        return Exam_Status;
    }

    public void setExam_Status(String exam_Status) {
        Exam_Status = exam_Status;
    }

    public ArrayList<ExamType> getResult() {
        return result;
    }

    public void setResult(ArrayList<ExamType> result) {
        this.result = result;
    }
}
