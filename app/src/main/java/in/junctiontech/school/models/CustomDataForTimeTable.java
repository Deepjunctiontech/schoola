package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JUNCTION TECH on 12/29/2015.
 */
public class CustomDataForTimeTable implements Serializable {
    private String classID, className, sectionID, sectionName, subjectID, subjectName, staffID, staffName, work_time;
    private int textColor;

    private String studentID, studentName;
    private boolean selectedInGrp;

    private String homework;

    public CustomDataForTimeTable(String classID, String className, String sectionID, String sectionName,
                                  String subjectID, String subjectName, String staffID, String staffName, String work_time, int textColor) {

        this.classID = classID;
        this.className = className;
        this.sectionID = sectionID;
        this.sectionName = sectionName;
        this.subjectID = subjectID;
        this.subjectName = subjectName;
        this.staffID = staffID;
        this.staffName = staffName;
        this.work_time = work_time;
        this.textColor = textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public void setWork_time(String work_time) {
        this.work_time = work_time;
    }


    public String getClassID() {
        return classID;
    }

    public String getClassName() {
        return className;
    }

    public String getSectionID() {
        return sectionID;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getStaffID() {
        return staffID;
    }

    public String getStaffName() {
        return staffName;
    }

    public String getWork_time() {
        return work_time;
    }


    public CustomDataForTimeTable(String classID, String className, String sectionID, String sectionName,
                                  String studentID, String studentName, boolean selectedInGrp) {
        this.classID = classID;
        this.className = className;
        this.sectionID = sectionID;
        this.sectionName = sectionName;
        this.studentID = studentID;
        this.studentName = studentName;
        this.selectedInGrp = selectedInGrp;
        this.textColor = textColor;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public boolean isSelectedInGrp() {
        return selectedInGrp;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public CustomDataForTimeTable(String homework, String subjectName, String subjectID) {
        this.subjectID = subjectID;
        this.homework = homework;
        this.subjectName = subjectName;
    }

    public void setSelectedInGrp(boolean selectedInGrp) {
        this.selectedInGrp = selectedInGrp;
    }
}
