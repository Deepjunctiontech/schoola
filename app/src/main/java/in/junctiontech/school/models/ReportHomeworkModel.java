package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 30-06-2017.
 */

public class ReportHomeworkModel implements Serializable {
    ArrayList<ReportHomeworkModel>result;
    String ClassId,SubjectId,SectionId,Homework,SubjectName,ClassName,SectionName,Dateofhomework,
            Studentstatus,StudentId;

    public ArrayList<ReportHomeworkModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<ReportHomeworkModel> result) {
        this.result = result;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getHomework() {
        return Homework;
    }

    public void setHomework(String homework) {
        Homework = homework;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getDateofhomework() {
        return Dateofhomework;
    }

    public void setDateofhomework(String dateofhomework) {
        Dateofhomework = dateofhomework;
    }

    public String getStudentstatus() {
        return Studentstatus;
    }

    public void setStudentstatus(String studentstatus) {
        Studentstatus = studentstatus;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }
}
