package in.junctiontech.school.models.country;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 13-04-2017.
 */

public class StateCode implements Serializable {
    String countryId,name, stategeonameId;
    ArrayList<StateCode> result;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStategeonameId() {
        return stategeonameId;
    }

    public void setStategeonameId(String stategeonameId) {
        this.stategeonameId = stategeonameId;
    }

    public ArrayList<StateCode> getResult() {
        return result;
    }

    public void setResult(ArrayList<StateCode> result) {
        this.result = result;
    }
}
