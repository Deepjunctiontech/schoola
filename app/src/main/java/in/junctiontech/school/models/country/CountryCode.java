package in.junctiontech.school.models.country;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE 13-04-2017.
 */

public class CountryCode implements Serializable {
String code ,countryName , countryCode , geonameId;


    ArrayList<CountryCode> result;

    public String getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(String geonameId) {
        this.geonameId = geonameId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ArrayList<CountryCode> getResult() {
        return result;
    }

    public void setResult(ArrayList<CountryCode> result) {
        this.result = result;
    }
}
