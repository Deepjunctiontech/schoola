package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 9/29/2016.
 */

public class NotificationEvent implements Serializable {
    private int Notify_id;
    private String Description;
    private String Link_url;
    private String Status;
    private String Date;

    public NotificationEvent(){

    }
    public ArrayList<NotificationEvent> getResult() {
        return result;
    }

    public void setResult(ArrayList<NotificationEvent> result) {
        this.result = result;
    }

    private ArrayList<NotificationEvent> result;

    public boolean isOnClickValve() {
        return this.onClickValve;
    }

    public void setOnClickValve(boolean onClickValve) {
        this.onClickValve = onClickValve;
    }

    private boolean onClickValve;

    public NotificationEvent(int notifyId, String description, String linkUrl,
                             String status, String date , boolean onClickValve ) {
        this.Notify_id = notifyId;
        this.Description = description;
        this.Link_url = linkUrl;
        this.Status = status;
        this.Date = date;
        this.onClickValve = onClickValve;
    }

    public int getNotifyId() {
        return Notify_id;
    }

    public void setNotifyId(int notifyId) {
        this.Notify_id = notifyId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getLinkUrl() {
        return Link_url;
    }

    public void setLinkUrl(String linkUrl) {
        this.Link_url = linkUrl;
    }



    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }


}
