package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 12/21/2015.
 */
public class StudentData implements Serializable {
    private String studentID, studentName, studentGrade, studentMarks, studentResult, max_marks;
    private String classId, className, sectionId, sectionName, reviewDate, review_sno, review;
    private String teacherID, teacherName, homeworkEvaluation;
    private String description, date, linkUrl, homework, session;
    private String accessionNumber, BookName, AuthorName, Publisher, SubjectId, SubjectName,
            Purpose, Price, doi, dor, userType;

    public String getHomeworkEvaluation() {
        return homeworkEvaluation;
    }

    public void setHomeworkEvaluation(String homeworkEvaluation) {
        this.homeworkEvaluation = homeworkEvaluation;
    }

    public StudentData(String accessionNumber, String bookName, String authorName, String publisher, String subjectId, String subjectName, String purpose, String price, String doi, String dor) {
        this.accessionNumber = accessionNumber;
        this.BookName = bookName;
        this.AuthorName = authorName;
        this.Publisher = publisher;
        this.SubjectId = subjectId;
        this.SubjectName = subjectName;
        this.Purpose = purpose;
        this.Price = price;
        this.doi = doi;
        this.dor = dor;
    }

    public StudentData(String classId, String className, String sectionId,
                       String sectionName, String subjectId, String subjectName,
                       String homework, String dateofhomework, String session) {
        this.className = className;
        this.classId = classId;
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.homework = homework;
        this.SubjectId = subjectId;
        this.SubjectName = subjectName;
        this.doi = dateofhomework;
        this.session = session;
    }

    public StudentData(String subId, String subName, String work, String homeworkEvaluation) {
        this.SubjectId = subId;
        this.SubjectName = subName;
        this.homework = work;
        this.homeworkEvaluation = homeworkEvaluation;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }


    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public String getAuthorName() {
        return AuthorName;
    }

    public void setAuthorName(String authorName) {
        AuthorName = authorName;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getPurpose() {
        return Purpose;
    }

    public void setPurpose(String purpose) {
        Purpose = purpose;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getDor() {
        return dor;
    }

    public void setDor(String dor) {
        this.dor = dor;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public StudentData(String description, String date, String linkUrl) {
        this.description = description;
        this.date = date;
        this.linkUrl = linkUrl;
    }

    public String getStudentID() {
        return studentID;
    }

    public String getMax_marks() {
        return max_marks;
    }

    public void setMax_marks(String max_marks) {
        this.max_marks = max_marks;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getStudentGrade() {
        return studentGrade;
    }

    public String getStudentMarks() {
        return studentMarks;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setStudentGrade(String studentGrade) {
        this.studentGrade = studentGrade;
    }

    public void setStudentMarks(String studentMarks) {
        this.studentMarks = studentMarks;
    }

    public void setStudentResult(String studentResult) {
        this.studentResult = studentResult;
    }

    public String getStudentResult() {
        return studentResult;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        this.SubjectName = subjectName;
    }

    public StudentData(String studentID, String studentName, String studentGrade, String studentMarks, String studentResult, String max_marks) {
        this.studentID = studentID;
        this.studentName = studentName;
        this.studentGrade = studentGrade;
        this.studentMarks = studentMarks;

        this.studentResult = studentResult;
        this.max_marks = max_marks;
    }

    public StudentData(String subjectName,
                       String studentGrade,
                       String studentMarks,
                       String studentResult,
                       String max_marks) {

        this.SubjectName = subjectName;
        this.studentGrade = studentGrade;
        this.studentMarks = studentMarks;
        this.studentResult = studentResult;
        this.max_marks = max_marks;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReview_sno() {
        return review_sno;
    }

    public void setReview_sno(String review_sno) {
        this.review_sno = review_sno;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public StudentData(String studentName, String studentID, String review, String review_sno, String reviewDate, String sectionName, String className) {
        this.studentName = studentName;
        this.studentID = studentID;
        this.review = review;
        this.review_sno = review_sno;
        this.reviewDate = reviewDate;
        this.sectionName = sectionName;
        this.className = className;
    }

    public StudentData(String studentName, String studentID) {
        this.studentName = studentName;
        this.studentID = studentID;
    }
    public StudentData(String studentName, String studentID, String userType,  int f) {
        this.studentName = studentName;
        this.studentID = studentID;
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
