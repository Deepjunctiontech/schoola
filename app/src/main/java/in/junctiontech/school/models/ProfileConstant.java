package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 17/1/2017.
 */

public class ProfileConstant implements Serializable {
    private String userId,userName,positionName,mobile,email,alternateMobile, fatherName, motherName,
            dateOfBirth,dateOfJoining,fatherMobile,motherMobile,presentAddress,permanentAddress,profileImageUrl;

    public ProfileConstant(String id, String name, String positionName, String mobile, String email,
                           String alternateMobile, String fatherName, String motherName, String dateOfBirth,
                           String dateOfJoining, String fatherMobile, String motherMobile, String presentAddress,
                           String permanentAddress,String profileImageUrl) {
        this.userId= id;
        this.userName= name;
        this.positionName= positionName;
        this.mobile= mobile;
        this.email= email;
        this.alternateMobile= alternateMobile;
        this.fatherName= fatherName;
        this.motherName= motherName;
        this.dateOfBirth= dateOfBirth;
        this.dateOfJoining= dateOfJoining;
        this.fatherMobile= fatherMobile;

        this.motherMobile= motherMobile;
        this.presentAddress= presentAddress;
        this.permanentAddress= permanentAddress;
        this.profileImageUrl= profileImageUrl;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getFatherMobile() {
        return fatherMobile;
    }

    public void setFatherMobile(String fatherMobile) {
        this.fatherMobile = fatherMobile;
    }

    public String getMotherMobile() {
        return motherMobile;
    }

    public void setMotherMobile(String motherMobile) {
        this.motherMobile = motherMobile;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlternateMobile() {
        return alternateMobile;
    }

    public void setAlternateMobile(String alternateMobile) {
        this.alternateMobile = alternateMobile;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }
}
