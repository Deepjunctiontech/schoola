package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 9/2/2017.
 */

public class HomeworkAndEvaluationData implements Serializable{
    String ClassId,SubjectId,SectionId, Homework, SubjectName, ClassName, SectionName, Dateofhomework,
            Dateofsubmission,Studentstatus, StudentId, DOEvaluation, Session;
    ArrayList<HomeworkAndEvaluationData> result;

    public HomeworkAndEvaluationData(String classId, String ClassName, String sectionId, String SectionName,
                                     String dateofhomework, String subjectId, String subjectName,
                                     String homework, String studentId) {
        this.ClassId= classId;
        this.ClassName= ClassName;
        this.SectionName= SectionName;
        this.SectionId= sectionId;
        this.Dateofhomework= dateofhomework;
        this.SubjectId= subjectId;
        this.SubjectName= subjectName;
        this.Homework= homework;
        this.StudentId= studentId;
    }

    public ArrayList<HomeworkAndEvaluationData> getResult() {
        return result;
    }

    public void setResult(ArrayList<HomeworkAndEvaluationData> result) {
        this.result = result;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getHomework() {
        return Homework;
    }

    public void setHomework(String homework) {
        Homework = homework;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getDateofhomework() {
        return Dateofhomework;
    }

    public void setDateofhomework(String dateofhomework) {
        Dateofhomework = dateofhomework;
    }

    public String getDateofsubmission() {
        return Dateofsubmission;
    }

    public void setDateofsubmission(String dateofsubmission) {
        Dateofsubmission = dateofsubmission;
    }

    public String getStudentstatus() {
        return Studentstatus;
    }

    public void setStudentstatus(String studentstatus) {
        Studentstatus = studentstatus;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String studentId) {
        StudentId = studentId;
    }

    public String getDOEvaluation() {
        return DOEvaluation;
    }

    public void setDOEvaluation(String DOEvaluation) {
        this.DOEvaluation = DOEvaluation;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }
}
