package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 12/28/2016.
 */

public class ChatData implements Serializable{
    int groupId,msgId;
    String userID, userType, userName,msg ,createdAtMsg, status;
    boolean perDayTagVisibility ;

    public void setPerDayTagVisibility(boolean perDayTagVisibility) {
        this.perDayTagVisibility = perDayTagVisibility;
    }

    public boolean isPerDayTagVisibility() {
        return perDayTagVisibility;

    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCreatedAtMsg() {
        return createdAtMsg;
    }

    public void setCreatedAtMsg(String createdAtMsg) {
        this.createdAtMsg = createdAtMsg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ChatData(int groupId, int msgId, String userID, String userType, String userName,
                    String msg, String createdAtMsg, String status, boolean perDayTagVisibility) {
        this.groupId = groupId;
        this.msgId = msgId;
        this.userID = userID;
        this.userType = userType;
        this.userName = userName;
        this.msg = msg;
        this.createdAtMsg = createdAtMsg;
        this.status = status;
        this.perDayTagVisibility = perDayTagVisibility;
    }
}
