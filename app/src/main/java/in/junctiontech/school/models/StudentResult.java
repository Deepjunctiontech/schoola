package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 24-04-2017.
 */

public class StudentResult implements Serializable {
    ArrayList<StudentResult> result;
    String Exam_Type,subject_Id,subjectName,obtainedMarks,maximumMarks,grade,resultStudent,
    examDate;

    public String getExamDate() {
        return examDate;
    }

    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }

    public ArrayList<StudentResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<StudentResult> result) {
        this.result = result;
    }

    public String getExam_Type() {
        return Exam_Type;
    }

    public void setExam_Type(String exam_Type) {
        Exam_Type = exam_Type;
    }

    public String getSubject_Id() {
        return subject_Id;
    }

    public void setSubject_Id(String subject_Id) {
        this.subject_Id = subject_Id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getObtainedMarks() {
        return obtainedMarks;
    }

    public void setObtainedMarks(String obtainedMarks) {
        this.obtainedMarks = obtainedMarks;
    }

    public String getMaximumMarks() {
        return maximumMarks;
    }

    public void setMaximumMarks(String maximumMarks) {
        this.maximumMarks = maximumMarks;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getResultStudent() {
        return resultStudent;
    }

    public void setResultStudent(String resultStudent) {
        this.resultStudent = resultStudent;
    }
}
