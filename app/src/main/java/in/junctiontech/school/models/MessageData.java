package in.junctiontech.school.models;

import java.io.Serializable;

/**
 * Created by JAYDEVI BHADE on 2/29/2016.
 */
public class MessageData implements Serializable {
    private String groupName, senderID, senderName,
            msg, msg_time, sending_status, s_no, myQuery;
    private String senderHeader, serverGroupId, g_members, g_admin, created_at, feedback, status, month, g_admin_type, g_members_type;
    private boolean visible_invisible;
    private String groupId, receiverId, receiverName, receiverType, noOfMessage, app_msg_s_no;
    private int imageId, backgroundResourceId;


    public String getMyQuery() {
        return myQuery;
    }

    public void setMyQuery(String myQuery) {
        this.myQuery = myQuery;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public String getSenderHeader() {
        return senderHeader;
    }

    public void setSenderHeader(String senderHeader) {
        this.senderHeader = senderHeader;
    }

    public int getBackgroundResourceId() {
        return backgroundResourceId;
    }

    public void setBackgroundResourceId(int backgroundResourceId) {
        this.backgroundResourceId = backgroundResourceId;
    }

    public String getApp_msg_s_no() {
        return app_msg_s_no;
    }

    public void setApp_msg_s_no(String app_msg_s_no) {
        this.app_msg_s_no = app_msg_s_no;
    }


    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getGroupId() {
        return groupId;

    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getNoOfMessage() {
        return noOfMessage;
    }

    public void setNoOfMessage(String noOfMessage) {
        this.noOfMessage = noOfMessage;
    }


    public String getFeedback() {
        return feedback;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public boolean isVisible_invisible() {
        return visible_invisible;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setMsg_time(String msg_time) {
        this.msg_time = msg_time;
    }

    public void setSending_status(String sending_status) {
        this.sending_status = sending_status;
    }

    public String getServerGroupId() {
        return serverGroupId;
    }

    public void setServerGroupId(String serverGroupId) {
        this.serverGroupId = serverGroupId;
    }

    public String getG_members() {
        return g_members;
    }

    public void setG_members(String g_members) {
        this.g_members = g_members;
    }

    public String getG_admin() {
        return g_admin;
    }

    public String getG_members_type() {
        return g_members_type;
    }

    public void setG_members_type(String g_members_type) {
        this.g_members_type = g_members_type;
    }

    public String getG_admin_type() {
        return g_admin_type;
    }

    public void setG_admin_type(String g_admin_type) {
        this.g_admin_type = g_admin_type;
    }

    public void setVisible_invisible(boolean visible_invisible) {
        this.visible_invisible = visible_invisible;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setG_admin(String g_admin) {
        this.g_admin = g_admin;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getGroupName() {
        return groupName;
    }

    public String getSenderID() {
        return senderID;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getS_no() {
        return s_no;
    }

    public void setS_no(String s_no) {
        this.s_no = s_no;
    }

    public String getMsg() {
        return msg;
    }

    public String getMonth() {
        return month;
    }

    public String getMsg_time() {
        return msg_time;
    }

    public String getSending_status() {
        return sending_status;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public MessageData(
            String app_msg_s_no,
            String senderID,
            String senderName,
            String receiverType,
            String msg,
            String msg_time,
            String status,
            int imageId,
            boolean visible_invisible
    ) {

        this.receiverType = receiverType;
        this.app_msg_s_no = app_msg_s_no;
        this.senderID = senderID;
        this.msg = msg;
        this.senderName = senderName;
        this.msg_time = msg_time;
        this.status = status;
        this.imageId = imageId;
        this.visible_invisible = visible_invisible;
    }

    private boolean visibility;
    private String sNoId;

    public String getsNoId() {
        return sNoId;
    }

    public void setsNoId(String sNoId) {
        this.sNoId = sNoId;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public MessageData( String sNoId,
            boolean visibility,String feedback, String created_at, String senderName,
                       String senderID,
                       String status, String month, boolean visible_invisible) {
        this.sNoId = sNoId;
        this.visibility = visibility;
        this.feedback = feedback;
        this.created_at = created_at;
        this.senderName = senderName;
        this.status = status;
        this.month = month;
        this.visible_invisible = visible_invisible;
        this.senderID = senderID;
    }


    public MessageData(String groupId,
                       String groupName,
                       String receiverId,
                       String receiverName,
                       String receiverType,
                       String noOfMessage,
                       String message) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.receiverType = receiverType;
        this.noOfMessage = noOfMessage;
        this.msg = message;

    }


}
