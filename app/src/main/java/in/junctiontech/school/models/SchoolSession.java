package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 11/1/2017.
 */

public class SchoolSession implements Serializable {
  String  currentSession;
    ArrayList<SchoolSession> sessionList= new ArrayList<>();
    String sessionStartDate,sessionEndDate,session, type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(String currentSession) {
        this.currentSession = currentSession;
    }

    public ArrayList<SchoolSession> getSessionList() {
        return sessionList;
    }

    public void setSessionList(ArrayList<SchoolSession> sessionList) {
        this.sessionList = sessionList;
    }

    public String getSessionStartDate() {
        return sessionStartDate;
    }

    public void setSessionStartDate(String sessionStartDate) {
        this.sessionStartDate = sessionStartDate;
    }

    public String getSessionEndDate() {
        return sessionEndDate;
    }

    public void setSessionEndDate(String sessionEndDate) {
        this.sessionEndDate = sessionEndDate;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
