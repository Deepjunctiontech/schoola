package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10/14/2016.
 */

public class StaffDetail implements Serializable {
    private String StaffId;
    private String StaffStatus;
    private String StaffPosition;
    private String StaffName;
    private String StaffMobile;
    private String staffProfileImage;
    private String StaffEmail;
    private String StaffAlternateMobile;
    private String StaffFName;
    private String StaffMName;
    private String StaffDOJ;
    private String StaffDOB;
    private String StaffPresentAddress;
    private String StaffPermanentAddress;
    private String StaffRemarks;
    private String DOE;
    private String DLU;
    private String DOD;
    private String DODUsername;
    public String StaffPositionValue;

    public String getDOE() {
        return DOE;
    }

    public void setDOE(String DOE) {
        this.DOE = DOE;
    }

    public String getDLU() {
        return DLU;
    }

    public void setDLU(String DLU) {
        this.DLU = DLU;
    }

    public String getDOD() {
        return DOD;
    }

    public void setDOD(String DOD) {
        this.DOD = DOD;
    }

    public String getDODUsername() {
        return DODUsername;
    }

    public void setDODUsername(String DODUsername) {
        this.DODUsername = DODUsername;
    }

    public String getStaffPositionValue() {
        return StaffPositionValue;
    }

    public void setStaffPositionValue(String staffPositionValue) {
        StaffPositionValue = staffPositionValue;
    }

    public String getStaffProfileImage() {
        return staffProfileImage;
    }

    public void setStaffProfileImage(String staffProfileImage) {
        this.staffProfileImage = staffProfileImage;
    }

    public String getMasterEntryValue() {
        return MasterEntryValue;
    }

    public void setMasterEntryValue(String masterEntryValue) {
        MasterEntryValue = masterEntryValue;
    }

    private String MasterEntryValue;
    private String presentStatus;

    public String getPresentStatus() {
        return presentStatus;
    }

    public void setPresentStatus(String presentStatus) {
        this.presentStatus = presentStatus;
    }

    private ArrayList<StaffDetail> result;

    public StaffDetail(String staffId,
                       String staffName,
                       String staffPositionValue,
                       String staffProfileImage,
                       String presentStatus){
        this.StaffId = staffId;
        this.StaffName = staffName;
        this.StaffPositionValue = staffPositionValue;
        this.staffProfileImage = staffProfileImage;
        this.presentStatus = presentStatus;

    }

    public StaffDetail(String id,
                       String staffStatus,
                       String staffPosition,
                       String staffName,
                       String staffMobile,
                       String staffEmail,
                       String staffAlternateMobile,
                       String staffFName,
                       String staffMName,
                       String staffDOJ,
                       String staffDOB,
                       String staffPresentAddress,
                       String staffPermanentAddress,
                       String staffRemarks) {
        this.StaffId = id;
        this.StaffStatus = staffStatus;
        this.StaffPosition = staffPosition;
        this.StaffName = staffName;
        this.StaffMobile = staffMobile;
        this.StaffEmail = staffEmail;

        this.StaffAlternateMobile = staffAlternateMobile;
        this.StaffFName = staffFName;
        this.StaffMName = staffMName;
        this.StaffDOJ = staffDOJ;
        this.StaffDOB = staffDOB;
        this.StaffPresentAddress = staffPresentAddress;
        this.StaffPermanentAddress = staffPermanentAddress;
        this.StaffRemarks = staffRemarks;
    }

    public ArrayList<StaffDetail> getResult() {
        return result;
    }

    public void setResult(ArrayList<StaffDetail> result) {
        this.result = result;
    }

    public String getStaffId() {
        return StaffId;
    }

    public void setStaffId(String staffId) {
        StaffId = staffId;
    }

    public String getStaffStatus() {
        return StaffStatus;
    }

    public void setStaffStatus(String staffStatus) {
        StaffStatus = staffStatus;
    }

    public String getStaffPosition() {
        return StaffPosition;
    }

    public void setStaffPosition(String staffPosition) {
        StaffPosition = staffPosition;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public String getStaffMobile() {
        return StaffMobile;
    }

    public void setStaffMobile(String staffMobile) {
        StaffMobile = staffMobile;
    }

    public String getStaffEmail() {
        return StaffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        StaffEmail = staffEmail;
    }

    public String getStaffAlternateMobile() {
        return StaffAlternateMobile;
    }

    public void setStaffAlternateMobile(String staffAlternateMobile) {
        StaffAlternateMobile = staffAlternateMobile;
    }

    public String getStaffFName() {
        return StaffFName;
    }

    public void setStaffFName(String staffFName) {
        StaffFName = staffFName;
    }

    public String getStaffMName() {
        return StaffMName;
    }

    public void setStaffMName(String staffMName) {
        StaffMName = staffMName;
    }

    public String getStaffDOJ() {
        return StaffDOJ;
    }

    public void setStaffDOJ(String staffDOJ) {
        StaffDOJ = staffDOJ;
    }

    public String getStaffDOB() {
        return StaffDOB;
    }

    public void setStaffDOB(String staffDOB) {
        StaffDOB = staffDOB;
    }

    public String getStaffPresentAddress() {
        return StaffPresentAddress;
    }

    public void setStaffPresentAddress(String staffPresentAddress) {
        StaffPresentAddress = staffPresentAddress;
    }

    public String getStaffPermanentAddress() {
        return StaffPermanentAddress;
    }

    public void setStaffPermanentAddress(String staffPermanentAddress) {
        StaffPermanentAddress = staffPermanentAddress;
    }

    public String getStaffRemarks() {
        return StaffRemarks;
    }

    public void setStaffRemarks(String staffRemarks) {
        StaffRemarks = staffRemarks;
    }
}
