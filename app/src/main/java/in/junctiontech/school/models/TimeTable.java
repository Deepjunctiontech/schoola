package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 8/24/2016.
 */
public class TimeTable implements Serializable {
private String slotTime, slotName, staff, subject, chapterName, topicName;
    private ArrayList<TimeTable> chapterData;
    ArrayList<TimeTable>result;

    public ArrayList<TimeTable> getResult() {
        return result;
    }

    public void setResult(ArrayList<TimeTable> result) {
        this.result = result;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public String getSlotName() {
        return slotName;
    }

    public TimeTable(String slotTime, String slotName, String staff, String subject, ArrayList<TimeTable> chapterData) {
        this.slotTime = slotTime;
        this.slotName = slotName;
        this.staff = staff;
        this.subject = subject;
        this.chapterData = chapterData;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getSubject() {
        return subject;
    }

    public TimeTable(String chapterName, String topicName) {
        this.chapterName = chapterName;
        this.topicName = topicName;
    }

    public void setSubject(String subject) {

        this.subject = subject;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public ArrayList<TimeTable> getChapterData() {
        return chapterData;
    }

    public void setChapterData(ArrayList<TimeTable> chapterData) {
        this.chapterData = chapterData;
    }
}
