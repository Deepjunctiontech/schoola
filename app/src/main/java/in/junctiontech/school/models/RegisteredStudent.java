package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 10/19/2016.
 */

public class RegisteredStudent implements Serializable {
private String RegistrationId, AdmissionId, Session,Status,StudentName, FatherName,Mobile,
       MotherName, SectionId, Gender,ClassId,SectionName,ClassName,
        AdmissionNo, ParentsPassword, StudentsPassword;
    private String StudentEmail;
    private String FatherMobile;
    private String FatherDateOfBirth;
    private String FatherEmail;
    private String FatherQualification;
    private String FatherOccupation;
    private String FatherDesignation;
    private String FatherOrganization;
    private String MotherMobile;
    private String MotherDateOfBirth;
    private String MotherEmail;
    private String MotherQualification;
    private String MotherOccupation;
    private String MotherDesignation;
    private String MotherOrganization;
    private String DOB;
    private String Landline;
    private String AlternateMobile;
    private String PresentAddress;
    private String PermanentAddress;
    private String BloodGroup;
    private String Caste;
    private String Category;

    public String getAdmissionId() {
        return AdmissionId;
    }

    public void setAdmissionId(String admissionId) {
        AdmissionId = admissionId;
    }

    private String Nationality;
    private String DateOfTermination;
    private String TerminationReason;
    private String TerminationRemarks;
    private String DOT;
    private String DOR;
    private boolean onClick = false;
    private String  positionName, studentProfileImage,parentProfileImage;

    public String getParentProfileImage() {
        return parentProfileImage;
    }

    public void setParentProfileImage(String parentProfileImage) {
        this.parentProfileImage = parentProfileImage;
    }

    public String getStudentProfileImage() {
        return studentProfileImage;
    }

    public void setStudentProfileImage(String studentProfileImage) {
        this.studentProfileImage = studentProfileImage;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDOR() {
        return DOR;
    }

    public void setDOR(String DOR) {
        this.DOR = DOR;
    }

    public String getStudentEmail() {
        return StudentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        StudentEmail = studentEmail;
    }

    public String getFatherMobile() {
        return FatherMobile;
    }

    public void setFatherMobile(String fatherMobile) {
        FatherMobile = fatherMobile;
    }

    public String getFatherDateOfBirth() {
        return FatherDateOfBirth;
    }

    public void setFatherDateOfBirth(String fatherDateOfBirth) {
        FatherDateOfBirth = fatherDateOfBirth;
    }

    public String getFatherEmail() {
        return FatherEmail;
    }

    public void setFatherEmail(String fatherEmail) {
        FatherEmail = fatherEmail;
    }

    public String getFatherQualification() {
        return FatherQualification;
    }

    public void setFatherQualification(String fatherQualification) {
        FatherQualification = fatherQualification;
    }

    public String getFatherOccupation() {
        return FatherOccupation;
    }

    public void setFatherOccupation(String fatherOccupation) {
        FatherOccupation = fatherOccupation;
    }

    public String getFatherDesignation() {
        return FatherDesignation;
    }

    public void setFatherDesignation(String fatherDesignation) {
        FatherDesignation = fatherDesignation;
    }

    public String getFatherOrganization() {
        return FatherOrganization;
    }

    public void setFatherOrganization(String fatherOrganization) {
        FatherOrganization = fatherOrganization;
    }

    public String getMotherMobile() {
        return MotherMobile;
    }

    public void setMotherMobile(String motherMobile) {
        MotherMobile = motherMobile;
    }

    public String getMotherDateOfBirth() {
        return MotherDateOfBirth;
    }

    public void setMotherDateOfBirth(String motherDateOfBirth) {
        MotherDateOfBirth = motherDateOfBirth;
    }

    public String getMotherEmail() {
        return MotherEmail;
    }

    public void setMotherEmail(String motherEmail) {
        MotherEmail = motherEmail;
    }

    public String getMotherQualification() {
        return MotherQualification;
    }

    public void setMotherQualification(String motherQualification) {
        MotherQualification = motherQualification;
    }

    public String getMotherOccupation() {
        return MotherOccupation;
    }

    public void setMotherOccupation(String motherOccupation) {
        MotherOccupation = motherOccupation;
    }

    public String getMotherDesignation() {
        return MotherDesignation;
    }

    public void setMotherDesignation(String motherDesignation) {
        MotherDesignation = motherDesignation;
    }

    public String getMotherOrganization() {
        return MotherOrganization;
    }

    public void setMotherOrganization(String motherOrganization) {
        MotherOrganization = motherOrganization;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getLandline() {
        return Landline;
    }

    public void setLandline(String landline) {
        Landline = landline;
    }

    public String getAlternateMobile() {
        return AlternateMobile;
    }

    public void setAlternateMobile(String alternateMobile) {
        AlternateMobile = alternateMobile;
    }

    public String getPresentAddress() {
        return PresentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        PresentAddress = presentAddress;
    }

    public String getPermanentAddress() {
        return PermanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        PermanentAddress = permanentAddress;
    }

    public String getBloodGroup() {
        return BloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        BloodGroup = bloodGroup;
    }

    public String getCaste() {
        return Caste;
    }

    public void setCaste(String caste) {
        Caste = caste;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getDateOfTermination() {
        return DateOfTermination;
    }

    public void setDateOfTermination(String dateOfTermination) {
        DateOfTermination = dateOfTermination;
    }

    public String getTerminationReason() {
        return TerminationReason;
    }

    public void setTerminationReason(String terminationReason) {
        TerminationReason = terminationReason;
    }

    public String getTerminationRemarks() {
        return TerminationRemarks;
    }

    public void setTerminationRemarks(String terminationRemarks) {
        TerminationRemarks = terminationRemarks;
    }

    public String getDOT() {
        return DOT;
    }

    public void setDOT(String DOT) {
        this.DOT = DOT;
    }

    public boolean isOnClick() {
        return onClick;
    }

    public void setOnClick(boolean onClick) {
        this.onClick = onClick;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getAdmissionNo() {
        return AdmissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        AdmissionNo = admissionNo;
    }

    public String getParentsPassword() {
        return ParentsPassword;
    }

    public void setParentsPassword(String parentsPassword) {
        ParentsPassword = parentsPassword;
    }

    public String getStudentsPassword() {
        return StudentsPassword;
    }

    public void setStudentsPassword(String studentsPassword) {
        StudentsPassword = studentsPassword;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    private ArrayList<RegisteredStudent> result;

    public ArrayList<RegisteredStudent> getResult() {
        return result;
    }

    public void setResult(ArrayList<RegisteredStudent> result) {
        this.result = result;
    }

    public String getRegistrationId() {

        return RegistrationId;
    }

    public void setRegistrationId(String registrationId) {
        RegistrationId = registrationId;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }
}
