package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 11/3/2016.
 */

public class Qualification implements Serializable {
    String QualificationId, Type, UniqueId, BoardUniversity, degreeName, Year, Marks;
private ArrayList<Qualification> result;

    public ArrayList<Qualification> getResult() {
        return result;
    }

    public void setResult(ArrayList<Qualification> result) {
        this.result = result;
    }

    public Qualification(String qualificaionId, String type, String staffId, String boardOfUniversity,
                         String degreeName, String year, String marks) {
        this.QualificationId = qualificaionId;
        this.Type = type;
        this.UniqueId = staffId;
        this.BoardUniversity = boardOfUniversity;
        this.degreeName = degreeName;
        this.Year = year;
        this.Marks = marks;
    }

    public String getQualificationId() {
        return QualificationId;
    }

    public void setQualificationId(String qualificationId) {
        QualificationId = qualificationId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUniqueId() {
        return UniqueId;
    }

    public void setUniqueId(String uniqueId) {
        UniqueId = uniqueId;
    }

    public String getBoardUniversity() {
        return BoardUniversity;
    }

    public void setBoardUniversity(String boardUniversity) {
        BoardUniversity = boardUniversity;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }
}
