package in.junctiontech.school.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JAYDEVI BHADE on 9/27/2016.
 */

public class CreateClass implements Serializable {
    private String SectionId, SectionName;
    private String classId, className, classSectionName;
private boolean isSelected;
    public CreateClass(String classId, String className, String sectionId, String sectionName) {
        this.classId =classId;
        this.className= className;
        this.SectionId =sectionId;
        this.SectionName= sectionName;
    }

    public CreateClass(String sectionId, String classSectionName, boolean isSelected) {
        this.SectionId = sectionId;
        this.classSectionName = classSectionName;
        this.isSelected = isSelected;
    }

    public CreateClass() {

    }


    public String getClassSectionName() {
        return classSectionName;
    }

    public void setClassSectionName(String classSectionName) {
        this.classSectionName = classSectionName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<CreateClass> getResult() {
        return result;
    }

    public void setResult(ArrayList<CreateClass> result) {
        this.result = result;
    }

    private ArrayList<CreateClass> result;

    public boolean isSectionVisibility() {
        return sectionVisibility;
    }

    public void setSectionVisibility(boolean sectionVisibility) {
        this.sectionVisibility = sectionVisibility;
    }

    private ArrayList<CreateClass> sectionList;
    private boolean sectionVisibility ;

    public CreateClass(String class_id, String class_no, ArrayList<CreateClass> sectionList) {
        this.classId =class_id;
        this.className= class_no;
        this.sectionList = sectionList;

    }

    public String getSectionId() {
        return this.SectionId;
    }

    public void setSectionId(String sectionId) {
        this.SectionId = sectionId;
    }

    public String getSectionName() {
        return this.SectionName;
    }

    public void setSectionName(String sectionName) {
        this.SectionName = sectionName;
    }

    public String getClassId() {
        return this.classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public ArrayList<CreateClass> getSectionList() {
        return sectionList;
    }

    public void setSectionList(ArrayList<CreateClass> sectionList) {
        this.sectionList = sectionList;
    }

    public String getClassName() {
        return this.className;

    }

    public void setClassName(String className) {
        this.className = className;
    }

    public CreateClass(String sectionId, String sectionName) {
        this.SectionId = sectionId;
        this.SectionName = sectionName;

    }
}
