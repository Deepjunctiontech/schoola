package in.junctiontech.school.models;

/**
 * Created by JAYDEVI BHADE on 12/22/2016.
 */

public class Attendance {
    public String studentName , AdmissionID, presentStatus, profileUrl, AdmissionNo;

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAdmissionID() {
        return AdmissionID;
    }

    public void setAdmissionID(String admissionID) {
        this.AdmissionID = admissionID;
    }

    public String getPresentStatus() {
        return presentStatus;
    }

    public void setPresentStatus(String presentStatus) {
        this.presentStatus = presentStatus;
    }

    public String getAdmissionNo() {
        return AdmissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        AdmissionNo = admissionNo;
    }

    public Attendance(String AdmissionID, String studentName, String presentStatus, String profileImage, String admissionNo) {
        this.AdmissionID = AdmissionID;
        this.studentName = studentName;
        this.presentStatus = presentStatus;
        this.profileUrl = profileImage;
        this.AdmissionNo = admissionNo;

    }
}
