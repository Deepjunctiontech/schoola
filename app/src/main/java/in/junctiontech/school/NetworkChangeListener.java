package in.junctiontech.school;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;

import in.junctiontech.school.FCM.Config;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by JAYDEVI BHADE on 7/21/2016.
 */
public class NetworkChangeListener extends BroadcastReceiver {
    private DbHandler db;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
       // final SharedPreferences sp = context.getSharedPreferences("data_auto_send", MODE_PRIVATE);
        final SharedPreferences sp = Prefs.with(context).getSharedPreferences();
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr  .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        db = DbHandler.getInstance(context);

        if (!sp.getString("user_type", "").equalsIgnoreCase("") && db!=null)
        {
            if ((wifi!=null && wifi.isConnected()) || ( mobile!= null &&   mobile.isConnected())) {
                // Do something
                Intent intent1 = new Intent(Config.INTERNET_AVAILABLE);
                intent1.putExtra("is", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent1);

                Log.e("NetworkAvailable ", "Flag No 1");
                //   db.sendDataToServer();

                if (sp.getString("user_type", "").equalsIgnoreCase("Teacher")) try {
                    db.sendFeedback();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    db.sendPendingChats();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (wifi!=null && wifi.isConnected()) {

                    if (sp.getBoolean("wifi_attendance", false))
                        try {
                            db.sendAttendanceToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    if (sp.getBoolean("wifi_homework", false)) {
                        try {
                            db.sendHomeWorkToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            db.sendHomeWorkEvaluationToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (sp.getBoolean("wifi_result", false))
                        try {
                            db.sendExamResultToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    if (sp.getBoolean("autoSendingWifi", false))
                        try {
                            db.sendDataToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                } else if (mobile!= null && mobile.isConnected()) {

                    if (sp.getBoolean("MobileData_attendance", false))
                        try {
                            db.sendAttendanceToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    if (sp.getBoolean("MobileData_homework", false)) {
                        try {
                            db.sendHomeWorkToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            db.sendHomeWorkEvaluationToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (sp.getBoolean("MobileData_result", false))
                        try {
                            db.sendExamResultToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    if (sp.getBoolean("autoSendingMobileData", false))
                        try {
                            db.sendDataToServer();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }


            } else {
                Log.e("NetworkNotAvailable ", "Flag No 1");
            }

            final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
            new PhoneStateListener() {
                @Override
                public void onServiceStateChanged(ServiceState serviceState) {
                    super.onServiceStateChanged(serviceState);

                    if (telephonyManager!=null && telephonyManager.isNetworkRoaming()) {

                        if (sp.getBoolean("roaming_attendance", false))
                            try {
                                db.sendAttendanceToServer();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        if (sp.getBoolean("roaming_homework", false)) {
                            try {
                                db.sendHomeWorkToServer();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                db.sendHomeWorkEvaluationToServer();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (sp.getBoolean("roaming_result", false))
                            try {
                                db.sendExamResultToServer();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        if (sp.getBoolean("autoSendingRoaming", false))
                            try {
                                db.sendDataToServer();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                    } else Log.d("roaming", "no");


                }
            };
        }

    }

    /*public void networkChecks(final String typeReq) {

        final SharedPreferences sp = context.getSharedPreferences("data_auto_send", MODE_PRIVATE);

        ConnectivityManager com = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = com.getAllNetworkInfo();


        for (NetworkInfo ni : networkInfos) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {

                if (ni.isConnected()) {

                    if (typeReq.equalsIgnoreCase("ic_attendance")) {
                        if (sp.getBoolean("wifi_attendance", false))
                            db.sendAttendanceToServer();
                    } else if (typeReq.equalsIgnoreCase("homework")) {
                        if (sp.getBoolean("wifi_homework", false)){
                            db.sendHomeWorkToServer();
                            db.sendHomeWorkEvaluationToServer();
                        }
                    } else if (typeReq.equalsIgnoreCase("result")) {
                        if (sp.getBoolean("wifi_result", false))
                            db.sendExamResultToServer();
                    } else {
                        if (sp.getBoolean("autoSendingWifi", false))
                            db.sendDataToServer();
                    }


                }

            }

            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {

                if (ni.isConnected()) {

                    if (typeReq.equalsIgnoreCase("ic_attendance")) {
                        if (sp.getBoolean("MobileData_attendance", false))
                            db.sendAttendanceToServer();
                    } else if (typeReq.equalsIgnoreCase("homework")) {
                        if (sp.getBoolean("MobileData_homework", false)){
                            db.sendHomeWorkToServer();
                            db.sendHomeWorkEvaluationToServer();
                        }
                    } else if (typeReq.equalsIgnoreCase("result")) {
                        if (sp.getBoolean("MobileData_result", false))
                            db.sendExamResultToServer();
                    } else {
                        if (sp.getBoolean("autoSendingMobileData", false))
                            db.sendDataToServer();
                    }

                }


            }

        }




    }*/
}
